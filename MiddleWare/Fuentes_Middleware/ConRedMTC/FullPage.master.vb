﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class FullPage
    Inherits System.Web.UI.MasterPage

    ' Error handler estándar, redirecciona a página de error
    Protected Sub HandleErrorRedirect(ByVal exception As Exception)

        If Session("Exception") Is Nothing Then
            Session("Exception") = exception
        End If

        Server.ClearError()

        Response.Redirect("~/ErrorPage.aspx")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblDateTime.Text = Now().ToString
    End Sub
End Class

