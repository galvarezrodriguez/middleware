﻿<%@ Page Title="" Language="VB" MasterPageFile="~/FullPage.master" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Inicio de Sesión</title>

    <!-- Toggle -->
    <script type="text/javascript" src="js/toogle.js"></script>    
    <!-- Modal Alert -->
    <script src="js/msgAlert.js" type="text/javascript"></script>

    <!-- MD5 -->
    <script src="js/md5-min.js" type="text/javascript"></script>

    <!-- SHA-1 -->
    <script src="js/sha1-min.js" type="text/javascript"></script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="loginform">
        <div class="title"> <h2>Inicio de Sesión</h2> </div>
        <div class="body">
      	    <label class="log-lab">Usuario:</label>
            <asp:TextBox ID="txtUser" runat="server" class="login-input-user" value="" 
                TabIndex="1" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
      	    <label class="log-lab">Clave:</label>
            <asp:TextBox ID="txtPassword" runat="server" class="login-input-pass" value="" 
                TextMode="Password" TabIndex="2" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
            <asp:Button ID="btnSubmit" runat="server" Text="Entrar" class="button" TabIndex="3" onclientclick="return validateLogin()" />
        </div>
    </div>  
    
    <asp:Panel ID="pnlError" runat="server" Visible="False">    
        <div class="albox warningbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <script type="text/javascript">
        $(function() {
            $("#ctl00_MainContent_txtUser").focus();
        });
    </script>    
    
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
    <!-- Validator -->
    <script src="js/ValidatorLogin.js" type="text/javascript"></script>    
    
</asp:Content>

