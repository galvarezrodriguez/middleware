﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class Settings_AddProductXBusinessModel
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            txtBusinessModelID.Text = objSessionParams.intSelectedBusinessModel
            txtBusinessModelName.Text = objSessionParams.strSelectedBusinessModel

            ddlProducts.Focus()

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdProductsXBusinessModel_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProductsXBusinessModel.RowCommand
        Try

            Select Case e.CommandName

                Case "Delete"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Delete")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    grdProductsXBusinessModel.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    Dim strData As String = "Modelo de Negocio ID.: " & txtBusinessModelID.Text _
                                & ", Código Producto: " & grdProductsXBusinessModel.SelectedDataKey.Values.Item("opc_id") _
                                & ", Producto: " & CType(grdProductsXBusinessModel.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Delete", "Producto Eliminado del Modelo de Negocio. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Producto eliminado del Modelo de Negocio"
                    grdProductsXBusinessModel.SelectedIndex = -1
            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnHidRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHidRefresh.Click
        ddlProducts.DataBind()
    End Sub

    Protected Sub btnAddProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProduct.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        Try
            If dsProductsXBusinessModel.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Producto Asociado al Modelo de Negocio. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Producto asociado al Modelo de Negocio correctamente"
                deactivateForm()
                grdProductsXBusinessModel.DataBind()
                dsProducts.DataBind()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Asociando Producto al Modelo de Negocio. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error asociando Producto al Modelo de Negocio"
            End If

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Private Sub deactivateForm()
        ddlProducts.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
    End Sub

    Private Sub activateForm()
        ddlProducts.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        activateForm()
        ddlProducts.DataBind()
        ddlProducts.Focus()
    End Sub

End Class
