﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Settings_ListBusinessModels
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        Else
            pnlMsg.Visible = False
            pnlError.Visible = False
        End If
    End Sub

    Protected Sub grdBusinessModels_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBusinessModels.RowCommand
        Try

            Select Case e.CommandName

                Case "AdminProducts"
                    grdBusinessModels.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedBusinessModel = grdBusinessModels.SelectedDataKey.Values.Item("id")
                    objSessionParams.strSelectedBusinessModel = CType(grdBusinessModels.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblModelName"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddProductXBusinessModel.aspx", False)

                Case "Update"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    Dim strData As String = "Modelo ID: " & CType(grdBusinessModels.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID2"), Label).Text
                    strData &= ", Modelo Nombre: " & CType(grdBusinessModels.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtBusinessModelGrid"), TextBox).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Modelo de Negocio Actualizado. Datos[ " & strData & " ]", "")
                    pnlMsg.Visible = True
                    pnlError.Visible = False
                    lblMsg.Text = "Modelo de Negocio Modificado"
                    dsBusinessModels.Update()
                    grdBusinessModels.EditIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click
        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        If validateField() Then
            If dsBusinessModels.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Modelo de Negocio Creado. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Modelo de Negocio creado correctamente"
                clearForm()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Modelo de Negocio. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando Modelo de Negocio"
            End If
        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Complete el formulario correctamente"
        End If
    End Sub

    Sub clearForm()
        txtBusinessModel.Text = ""
        txtBusinessModel.Focus()
    End Sub

    Function validateField() As Boolean
        Dim retVal As Boolean

        If txtBusinessModel.Text.Length <= 5 Then
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

End Class
