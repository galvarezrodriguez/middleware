﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListBusinessModels.aspx.vb" Inherits="Settings_ListBusinessModels" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Modelos de Negocio
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
  	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Settings/Manager.aspx">Parametrización</asp:LinkButton>
    </li>
    <li>Listar Modelos de Negocio</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Listar Modelos de Negocio
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite listar los modelos de negocio del sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:Panel ID="pnlInsert" runat="server"
            CssClass="dialogbox" Width="700px">
            <br />
            <b>&nbsp;Nombre Modelo de Negocio:&nbsp;&nbsp; </b>
            <asp:TextBox ID="txtBusinessModel" 
                runat="server" Width="300px" MaxLength="50" CssClass="st-forminput-active" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Button ID="btnInsert" runat="server" CssClass="st-button" 
                Text="Crear" />
            <br />
        </asp:Panel>    
    
        <asp:GridView ID="grdBusinessModels" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsBusinessModels" ForeColor="#333333" Width="700px" 
            CssClass="mGrid" DataKeyNames="id"
            EmptyDataText="No existen Modelos de Negocio creados.">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="Código" SortExpression="id">
                    <ItemTemplate>
                        <asp:Label ID="lblID" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="lblID2" runat="server" Text='<%# Eval("id") %>'></asp:Label>
                    </EditItemTemplate>                    
                </asp:TemplateField>            
                <asp:TemplateField HeaderText="Modelo de Negocio" SortExpression="nombre">
                    <ItemTemplate>
                        <asp:Label ID="lblModelName" runat="server" Text='<%# Bind("nombre") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBusinessModelGrid" runat="server" Text='<%# Bind("nombre") %>' onkeydown = "return (event.keyCode!=13);" Width="300"  MaxLength="50" CssClass="st-forminput-active"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqFieldValidator" runat="server" ControlToValidate="txtBusinessModelGrid" ErrorMessage="Debe digitar un valor correcto."></asp:RequiredFieldValidator>                        
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Modelo de Negocio" />
                        <asp:ImageButton ID="imgAdminProducts" runat="server" CausesValidation="False" 
                            CommandName="AdminProducts" ImageUrl="~/img/icons/16x16/add_products.png" Text="Asociar Productos" 
                            ToolTip="Asociar Productos" CommandArgument='<%# grdBusinessModels.Rows.Count %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:LinkButton ID="lnkButtonUpdate" runat="server" CausesValidation="True" 
                            CommandName="Update" Text="Actualizar" CommandArgument='<%# grdBusinessModels.Rows.Count %>'></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="lnkButtonCancel" runat="server" CausesValidation="False" 
                            CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                    </EditItemTemplate>
                </asp:TemplateField>                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    
	    <asp:SqlDataSource ID="dsBusinessModels" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
            SelectCommand="SELECT id, nombre FROM TipoNivelEntidad WHERE id NOT IN(1)"
            InsertCommand="INSERT INTO TipoNivelEntidad(nombre) VALUES (@nombreNuevo)"             
            UpdateCommand="UPDATE TipoNivelEntidad SET nombre = @nombre WHERE (id = @id)">
            <UpdateParameters>
                <asp:Parameter Name="nombre" />
                <asp:Parameter Name="id" />
            </UpdateParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtBusinessModel" Name="nombreNuevo" PropertyName="Text" Type="String" />
            </InsertParameters>            
        </asp:SqlDataSource>

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>  

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Settings/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

