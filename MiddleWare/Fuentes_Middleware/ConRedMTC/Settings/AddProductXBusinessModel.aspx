﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddProductXBusinessModel.aspx.vb" Inherits="Settings_AddProductXBusinessModel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Productos X Modelo de Negocio
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Settings/Manager.aspx">Parametrización</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkWebModules" runat="server" CssClass="fixed" 
            PostBackUrl="~/Settings/ListBusinessModels.aspx">Listar Modelos de Negocio</asp:LinkButton>
    </li>
    <li>Agregar Productos X Modelo de Negocio</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Productos X Modelo de Negocio
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar Productos a un modelo de negocio del sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Modelo de Negocio</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Modelo de Negocio:</span>	
                <asp:TextBox ID="txtBusinessModelName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Modelo de Negocio." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <asp:TextBox ID="txtBusinessModelID" runat="server" Visible="False"></asp:TextBox>
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Lista de Productos:</span>	
                <asp:DropDownList ID="ddlProducts" runat="server" class="uniform" DataSourceID="dsProducts" 
                    DataTextField="pro_nombre" DataValueField="pro_id" Width="300px" TabIndex="2">
                </asp:DropDownList>
                <asp:Button ID="btnHidRefresh" runat="server" Text="" Visible="false"  />
                <asp:SqlDataSource ID="dsProducts" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="SELECT P.pro_id, P.pro_nombre + ' :: ' + Prov.prov_nombre AS pro_nombre FROM Producto P INNER JOIN Proveedor Prov ON (P.pro_proveedor_id = Prov.prov_id) WHERE P.pro_id NOT IN ( SELECT P.pro_id FROM Matriz_NivelEntidadXProducto MNEP INNER JOIN TipoNivelEntidad TNE ON (MNEP.tipo_nivel_entidad_id = TNE.id AND TNE.id = @tipoNivelEntidad) INNER JOIN Producto P ON (MNEP.producto_id = P.pro_id) INNER JOIN Proveedor PR ON (P.pro_proveedor_id = PR.prov_id))">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtBusinessModelID" Name="tipoNivelEntidad" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <div class="clear"></div>
            </div>
            
            <div class="button-box">
                <asp:Button ID="btnAddProduct" runat="server" Text="Adicionar Producto al Modelo" 
                    CssClass="st-button" TabIndex="3" OnClientClick="return validateAddProductXBusinessModel();"/>
                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="4" Visible="false"  />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Productos Asociados al Modelo de Negocio</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdProductsXBusinessModel" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsProductsXBusinessModel" ForeColor="#333333" Width="800px" 
                CssClass="mGridCenter" DataKeyNames="Producto_ID" 
                EmptyDataText="No existen Productos asociados a este Modelo de Negocio." 
                HorizontalAlign="Center"
                OnRowDeleted="btnHidRefresh_Click">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" SortExpression="Producto_ID">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("Producto_ID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Producto" SortExpression="Producto">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Bind("Producto") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Proveedor" SortExpression="Proveedor">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("Proveedor") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Destino" SortExpression="Destino">
                        <ItemTemplate>
                            <asp:Label ID="lblID4" runat="server" Text='<%# Bind("Destino") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Producto del Modelo de Negocio" CommandArgument='<%# grdProductsXBusinessModel.Rows.Count %>' />                                
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            
	        <asp:SqlDataSource ID="dsProductsXBusinessModel" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>"                 
                SelectCommand="SELECT P.pro_id AS Producto_ID, P.pro_nombre AS Producto, PR.prov_nombre AS Proveedor, PR.prov_isomux_nombre AS Destino FROM Matriz_NivelEntidadXProducto MNEP INNER JOIN TipoNivelEntidad TNE ON (MNEP.tipo_nivel_entidad_id = TNE.id AND TNE.id = @tipoNivelEntidad) INNER JOIN Producto P ON (MNEP.producto_id = P.pro_id) INNER JOIN Proveedor PR ON (P.pro_proveedor_id = PR.prov_id)"
                DeleteCommand="DELETE FROM Matriz_NivelEntidadXProducto WHERE tipo_nivel_entidad_id = @tipo_nivel_entidad_id AND producto_id = @Producto_ID"
                InsertCommand="INSERT INTO Matriz_NivelEntidadXProducto(tipo_nivel_entidad_id, producto_id) VALUES(@tipo_nivel_entidad_id, @producto_id)">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBusinessModelID" Name="tipoNivelEntidad" PropertyName="Text" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:ControlParameter ControlID="txtBusinessModelID" Name="tipo_nivel_entidad_id" PropertyName="Text" Type="String" />
                    <asp:Parameter Name="Producto_ID" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtBusinessModelID" Name="tipo_nivel_entidad_id" 
                        PropertyName="Text" />                    
                    <asp:ControlParameter ControlID="ddlProducts" Name="producto_id" 
                        PropertyName="SelectedValue" />
                </InsertParameters>
            </asp:SqlDataSource> 
            
            <br/>
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Settings/ListBusinessModels.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSettings.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

