﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Index.aspx.vb" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Inicio
    </title>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    PLATAFORMA WEB
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">

    <!-- START Text Box -->
    <div class="grid960">
        <div class="simplebox">
	        <div class="titleh"><h3><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%></h3></div>
            <div class="body">
    	        <p class="padding-in">Esta plataforma web permite la administración de comercios, comisiones, conciliaciones y demás tareas propias de la operación de la Red de Pagos Conred.</p>
            </div>
        </div>
    <div class="clear"></div>   
    </div>
    <!-- END Text Box -->

    <!-- START Text Box -->
    <div class="grid450-left">
        <div class="simplebox">
	        <div class="titleh"><h3>Datos de Conexión:</h3></div>
            <div class="body">
    	        <p class="padding-in">
                    <ul class="statistics">
                    	<li>IP de Conexión:	<p>	<span class="blue"><asp:Label ID="lblIP" runat="server" Text=""></asp:Label></span></p></li>
                    	<li>Fecha de Inicio Sesión:	<p>	<span class="blue"><asp:Label ID="lblLoginInitDate" runat="server" Text=""></asp:Label></span></p></li>
                    </ul>                    <div class="clear"></div>                    <br />    	        </p>
            </div>
        </div>
    </div>
    <!-- END Text Box -->

    <!-- START Text Box -->
    <div class="grid450-right">
        <div class="simplebox">
	        <div class="titleh"><h3>Datos de la Cuenta:</h3></div>
            <div class="body">
    	        <p class="padding-in">
    	        
                    <ul class="statistics">
                    	<li>Nombre Usuario:	<p><span class="blue"><asp:Label ID="lblUserName" runat="server" Text=""></asp:Label></span></p></li>
                    	<li>Perfil:	<p><span class="blue"><asp:Label ID="lblProfile" runat="server" Text=""></asp:Label></span></p></li>
                    	<li>Fecha de Expiración Clave:<p> <span class="blue"><asp:Label ID="lblExpDatetime" runat="server" Text=""></asp:Label></span></p></li>
                    </ul>    	        
    	        
    	            <br />
    	            <div align="center">
    	            
                        <asp:Button ID="btnEditUser" runat="server" Text="Editar Cuenta" 
                            CssClass="st-button" TabIndex="1" />

                        <asp:Button ID="btnChangePassword" runat="server" Text="Cambiar Clave" 
                            CssClass="st-button" TabIndex="2" />
                                	            
                    </div>
                    <div class="clear"></div>
                    <br />
    	        </p>
            </div>
        </div>
    </div>
    <!-- END Text Box -->
    
</asp:Content>

