﻿
function validateAddProductXBusinessModel() {

    var flagField = false;
    var errorCode = 0;
    var strError = '';

    if ($("#ctl00_MainContent_ddlProducts").val() <= 0) {
        $("#ctl00_MainContent_ddlProducts").focus();
        errorCode = 1;
        flagField = false;
    }
    else
        flagField = true;

    if (!flagField) {

        switch (errorCode) {
            case 1: strError = 'Por favor seleccione un producto de la lista'; break;
        }

        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: strError,
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}