﻿function validateEmail(mailValue) {
    //Solo direcciones de correo correctas
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return emailPattern.test(mailValue);
}

function isAlfaNum(dataValue) {

    //Solo Caracteres a-z, A-Z, Dígitos 0-9
    var patron = new RegExp("\\w+$", "i");

    if (!patron.test(dataValue))
        return false;
    else
        return true;
}

function validateTxtConred(strValue) {
    
    //Solo Caracteres a-z, A-Z y Espacio
    var patron = /^[a-zA-ZáéíóúÑñÁÉÍÓÚ ]*$/;

    if (!strValue.search(patron))
        return true;
    else
        return false;
}

function validateDecimal(value) {
    //Solo valores decimales
    var decimalPattern = /^[0-9,]*$/;
    return decimalPattern.test(value);
}

function validateOnlyDigits(value) {
    //Solo valores numéricos
    var digitsPattern = /^[0-9]*$/;
    return digitsPattern.test(value);
}