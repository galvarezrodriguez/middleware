﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:decimal-format name="German" decimal-separator=',' grouping-separator='.'  NaN=''/>
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          .stdPVTblLCell {
          color: DarkBlue;
          font-weight: bold;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 12pt;
          }
          .stdPageHdr {
          color: DarkBlue;
          font-weight: bold;
          font-style:italic;
          font-family:Verdana;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 20pt;
          }
          .gridHeader {
          background-color: #C0C0C0;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          vertical-align:middle;
          text-align:center;
          border: solid thin Black;
          }
          .SearchHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchKey {
          color: DarkBlue;
          font-size: 9pt;
          vertical-align:middle;
          text-align:right;
          font-family:Verdana;
          }
          .SearchValue {
          color: Black;
          font-size: 9pt;
          font-weight: bold;
          vertical-align:middle;
          text-align:left;
          font-family:Verdana;
          }
          .SearchResultHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchResultItem {
          background-color: #8FC9FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .SearchResultAltItem {
          background-color: #CCE6FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE>
          <TR>
            <TD class="stdPageHdr" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/ReportName"/>
            </TD>
          </TR>
          <TR  class="stdPVTblLCell">
            <TD>
              Encabezado
            </TD>
          </TR>
          <TR>
            <TD> </TD>
          </TR>
          <TR>
            <TD colspan="1" class="SearchKey">Comercio:</TD>
            <TD class="SearchValue" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/Merchant"/>
            </TD>
          </TR>
          <TR>
            <TD colspan="1" class="SearchKey">Período:</TD>
            <TD class="SearchValue" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/Period"/>
            </TD>
          </TR>
          <TR>
            <TD></TD>
          </TR>
          <TR class="SearchResultHeader">
            <TD colspan="2">Detalle del Reporte</TD>
          </TR>
          <TR>
            <TD> </TD>
          </TR>
          <TR>
            <TD class="gridHeader">
              Fecha
            </TD>
            <TD class="gridHeader">
              Comercio ID
            </TD>
            <TD class="gridHeader">
              Terminal
            </TD>
            <TD class="gridHeader">
              Producto
            </TD>
            <TD class="gridHeader">
              Ref. Cliente
            </TD>
            <TD class="gridHeader">
              Auth. ID Operador
            </TD>
            <TD class="gridHeader">
              Auth. ID P38
            </TD>
            <TD class="gridHeader">
              Saldo Inicial
            </TD>
            <TD class="gridHeader">
              Valor
            </TD>
            <TD class="gridHeader">
              Saldo Final
            </TD>
          </TR>
          <xsl:for-each select="NewDataSet/Table">
            <xsl:choose>
              <xsl:when test="position() mod 2 = 1">
                <TR>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_fecha_inicial"/>
                  </TD>
                  <TD class="SearchResultItem">
                    '<xsl:value-of select="tra_comercio_id"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_terminal_id"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="pro_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    '<xsl:value-of select="tra_referencia_cliente_1"/>
                  </TD>
                  <TD class="SearchResultItem">
                    '<xsl:value-of select="tra_autorizacion_ope_id"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="tra_auth_id_p38"/>
                  </TD>                  
                  <TD class="SearchResultItem">
                    <xsl:value-of select="format-number(tra_comercio_saldo_inicial,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="format-number(tra_valor,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="format-number(tra_comercio_saldo_final,'#.###,00', 'German')"/>
                  </TD>
                </TR>
              </xsl:when>
              <xsl:otherwise>
                <TR>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_fecha_inicial"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    '<xsl:value-of select="tra_comercio_id"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_terminal_id"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="pro_nombre"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    '<xsl:value-of select="tra_referencia_cliente_1"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    '<xsl:value-of select="tra_autorizacion_ope_id"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="tra_auth_id_p38"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="format-number(tra_comercio_saldo_inicial,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="format-number(tra_valor,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="format-number(tra_comercio_saldo_final,'#.###,00', 'German')"/>
                  </TD>
                </TR>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>