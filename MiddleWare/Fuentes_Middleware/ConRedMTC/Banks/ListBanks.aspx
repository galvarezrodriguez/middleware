﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListBanks.aspx.vb" Inherits="Banks_ListBanks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Administración de Bancos
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Banks/Manager.aspx">Bancos</asp:LinkButton>
    </li>
    <li>Admin. Bancos</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Administración de Bancos
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite administrar los bancos del sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:Panel ID="pnlInsert" runat="server"
            CssClass="dialogbox" Width="800px">
            <br />
            <b>&nbsp;Nombre de Banco:&nbsp;&nbsp; </b>
            <asp:TextBox ID="txtBank" onkeydown = "return (event.keyCode!=13);"
                runat="server" Width="300px" MaxLength="50" CssClass="st-forminput-active"></asp:TextBox>
            &nbsp;&nbsp;
            <asp:Button ID="btnInsert" runat="server" CssClass="st-button" 
                Text="Crear" />
            <br />            
        </asp:Panel>    
    
        <asp:GridView ID="grdBanks" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsBanks" ForeColor="#333333" Width="800px" 
            CssClass="mGrid" DataKeyNames="ban_id" 
            EmptyDataText="No existen Bancos creados.">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                    SortExpression="ban_id">
                    <ItemTemplate>
                        <asp:Label ID="lblID1" runat="server" Text='<%# Bind("ban_id") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Label ID="lblID2" runat="server" Text='<%# Eval("ban_id") %>'></asp:Label>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Nombre" SortExpression="ban_nombre">
                    <ItemTemplate>
                        <asp:Label ID="lblID3" runat="server" Text='<%# Bind("ban_nombre") %>'></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="txtBankGrid" runat="server" Text='<%# Bind("ban_nombre") %>' onkeydown = "return (event.keyCode!=13);" Width="300"  MaxLength="50" CssClass="st-forminput-active"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="reqFieldValidator" runat="server" ControlToValidate="txtBankGrid" ErrorMessage="Debe digitar un valor correcto."></asp:RequiredFieldValidator>                        
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Banco" />
                        <asp:ImageButton ID="imgAccounts" runat="server" CausesValidation="False" 
                            CommandName="AddAccounts" CommandArgument='<%# grdBanks.Rows.Count %>' ImageUrl="~/img/icons/16x16/add_account.png" Text="Agregar Cuentas" 
                            ToolTip="Agregar Cuentas" />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:LinkButton ID="lnkButtonUpdate" runat="server" CausesValidation="True" 
                            CommandName="Update" Text="Actualizar" CommandArgument='<%# grdBanks.Rows.Count %>'></asp:LinkButton>
                        &nbsp;<asp:LinkButton ID="lnkButtonCancel" runat="server" CausesValidation="False" 
                            CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                    </EditItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    
	    <asp:SqlDataSource ID="dsBanks" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
            SelectCommand="SELECT ban_id, ban_nombre FROM Banco" 
            InsertCommand="INSERT INTO Banco(ban_nombre) VALUES (@nombreNuevo)" 
            UpdateCommand="UPDATE Banco SET ban_nombre = @ban_nombre WHERE (ban_id = @ban_id)">
            <UpdateParameters>
                <asp:Parameter Name="ban_nombre" />
                <asp:Parameter Name="ban_id" />
            </UpdateParameters>
            <InsertParameters>
                <asp:ControlParameter ControlID="txtBank" Name="nombreNuevo" PropertyName="Text" Type="String" />
            </InsertParameters>
        </asp:SqlDataSource>

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

        <asp:Panel ID="pnlError" runat="server" Visible="False">    
            <div class="albox errorbox">
                <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel>                       

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Banks/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

