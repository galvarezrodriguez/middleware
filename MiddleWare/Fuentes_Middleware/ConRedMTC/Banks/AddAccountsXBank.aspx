﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddAccountsXBank.aspx.vb" Inherits="Banks_AddAccountsXBank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Cuentas X Banco
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Banks/Manager.aspx">Bancos</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkProfiles" runat="server" CssClass="fixed" 
            PostBackUrl="~/Banks/ListBanks.aspx">Admin. Bancos</asp:LinkButton>
    </li>
    <li>Agregar Cuentas X Banco</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Cuentas X Banco
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar cuentas x banco al sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Banco</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre del Banco:</span>	
                <asp:TextBox ID="txtBankName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Banco." Enabled="False" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <asp:TextBox ID="txtBankID" runat="server" Visible="False"></asp:TextBox>                    
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext"><b>Datos de la Cuenta</b></span>	
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Tipo de Cuenta:</span>	                
                <asp:DropDownList ID="ddlAccountsType" class="uniform" runat="server" 
                    DataSourceID="dsAccountType" DataTextField="nombre" 
                    DataValueField="id" ToolTip="Seleccione el Tipo de Cuenta." TabIndex="2">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsAccountType" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="SELECT -1 AS id, '   ' AS nombre UNION ALL SELECT * FROM TipoCuenta">                                       
                </asp:SqlDataSource>                
                <div class="clear"></div>
            </div>            

            <div class="st-form-line">	
                <span class="st-labeltext">Número de Cuenta:</span>	
                <asp:TextBox ID="txtAccountNumber" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="3" MaxLength="100" 
                    ToolTip="Número de Cuenta." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>

            <div class="st-form-line">	
                <span class="st-labeltext">Descripción:</span>	
                <asp:TextBox ID="txtAccountDescription" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="4" MaxLength="100" 
                    ToolTip="Descripción de la Cuenta." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>
            
            <div class="button-box">
                <asp:Button ID="btnAddAccount" runat="server" Text="Adicionar Cuenta" 
                    CssClass="st-button" TabIndex="5" OnClientClick="return validateAddAccount();" />
                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="5" Visible="false"  />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Cuentas creadas en el&nbsp; banco</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdAccountsXBank" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsAccountsXBank" ForeColor="#333333" Width="800px" 
                CssClass="mGridCenter" DataKeyNames="cue_id" 
                EmptyDataText="No existen Cuentas creadas en este Banco." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="cue_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("cue_id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Número de Cuenta" SortExpression="cue_numero">
                        <ItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Bind("cue_numero") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tipo de Cuenta" SortExpression="TipoCuenta">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("TipoCuenta") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Observación" SortExpression="cue_observacion">
                        <ItemTemplate>
                            <asp:Label ID="lblID4" runat="server" Text='<%# Bind("cue_observacion") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar" 
                                ToolTip="Eliminar Cuenta del Banco" CommandArgument='<%# grdAccountsXBank.Rows.Count %>' />                        
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            
	        <asp:SqlDataSource ID="dsAccountsXBank" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                SelectCommand="SELECT CB.cue_id, CB.cue_numero, TC.nombre AS TipoCuenta, CB.cue_observacion
                FROM CuentaBanco CB
	                INNER JOIN TipoCuenta TC ON (TC.id = CB.cue_tipo_cuenta_id)
	                INNER JOIN Banco B ON (B.ban_id = CB.cue_banco_id)
                WHERE CB.cue_banco_id = @cue_banco_id
                ORDER BY 1"
                InsertCommand="INSERT INTO CuentaBanco(cue_numero, cue_tipo_cuenta_id, cue_banco_id, cue_observacion) VALUES (@cue_numero, @cue_tipo_cuenta_id, @cue_banco_id, @cue_observacion)" 
                DeleteCommand="DELETE FROM CuentaBanco WHERE cue_id = @cue_id">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtBankID" Name="cue_banco_id" 
                        PropertyName="Text" />
                </SelectParameters>                
                <DeleteParameters>
                    <asp:Parameter Name="cue_id" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtAccountNumber" Name="cue_numero" 
                        PropertyName="Text" />                    
                    <asp:ControlParameter ControlID="ddlAccountsType" Name="cue_tipo_cuenta_id" 
                        PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtBankID" Name="cue_banco_id" 
                        PropertyName="Text" />                        
                    <asp:ControlParameter ControlID="txtAccountDescription" Name="cue_observacion" 
                        PropertyName="Text" />                        
                </InsertParameters>                
                
            </asp:SqlDataSource> 
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Banks/ListBanks.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorBank.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

