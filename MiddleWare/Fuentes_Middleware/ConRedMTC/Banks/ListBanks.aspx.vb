﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Banks_ListBanks
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            clearForm()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnInsert_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInsert.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        If validateField() Then
            If dsBanks.Insert() >= 1 Then
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Banco Creado. Datos[ " & getFormDataLog() & " ]", "")
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Banco creado correctamente"
                clearForm()
            Else
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Banco. Datos[ " & getFormDataLog() & " ]", "")
                pnlMsg.Visible = False
                pnlError.Visible = True
                lblError.Text = "Error creando banco"
            End If
        Else
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Complete el formulario correctamente"
        End If

    End Sub

    Function validateField() As Boolean
        Dim retVal As Boolean

        If txtBank.Text.Length <= 5 Then
            retVal = False
        Else
            retVal = True
        End If

        Return retVal

    End Function

    Sub clearForm()
        txtBank.Text = ""
        txtBank.Focus()
    End Sub

    Protected Sub grdBanks_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdBanks.RowCommand
        Try

            Select Case e.CommandName

                Case "AddAccounts"
                    grdBanks.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedBank = grdBanks.SelectedDataKey.Values.Item("ban_id")
                    objSessionParams.strSelectedBank = CType(grdBanks.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblID3"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddAccountsXBank.aspx", False)

                Case "Update"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    Dim strData As String = "Nuevo Nombre: " & CType(grdBanks.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtBankGrid"), TextBox).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Nombre del Banco Actualizado. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Nombre de Banco Modificado"
                    dsBanks.Update()

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

End Class
