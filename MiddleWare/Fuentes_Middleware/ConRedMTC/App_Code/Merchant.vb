﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace Conred.Merchants

    Public Class Merchant

        Private strConnString As String
        Private strMerchantID As String
        Private intMerchantType As Integer
        Private decPctGain As Decimal
        Private decBalance As Decimal
        Private intMerchantStatus As Integer
        Private dateActivationDate As Date
        Private boolAdminLocalBalance As Boolean

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        Public Sub New(ByVal strConnString As String, ByVal strMerchantID As String, ByVal intMerchantType As Integer, ByVal boolAdminLocalBalance As Boolean, ByVal decPctGain As Decimal)
            Me.strConnString = strConnString
            Me.strMerchantID = strMerchantID
            Me.intMerchantType = intMerchantType
            Me.boolAdminLocalBalance = boolAdminLocalBalance
            Me.decPctGain = decPctGain
        End Sub

        ''' <summary>
        ''' Código del Comercio
        ''' </summary>
        Public Property MerchantID() As String
            Get
                Return strMerchantID
            End Get
            Set(ByVal value As String)
                strMerchantID = value
            End Set
        End Property

        ''' <summary>
        ''' Tipo de Comercio
        ''' </summary>
        Public Property MerchantType() As Integer
            Get
                Return intMerchantType
            End Get
            Set(ByVal value As Integer)
                intMerchantType = value
            End Set
        End Property

        ''' <summary>
        ''' Administra Cupo Local?
        ''' </summary>
        Public Property AdminLocalBalance() As Boolean
            Get
                Return boolAdminLocalBalance
            End Get
            Set(ByVal value As Boolean)
                boolAdminLocalBalance = value
            End Set
        End Property

        ''' <summary>
        ''' Porcentaje de Ganancia
        ''' </summary>
        Public Property MerchantPctGain() As Decimal
            Get
                Return decPctGain
            End Get
            Set(ByVal value As Decimal)
                decPctGain = value
            End Set
        End Property

        ''' <summary>
        ''' Saldo del Comercio
        ''' </summary>
        Public Property MerchantBalance() As Decimal
            Get
                Return decBalance
            End Get
            Set(ByVal value As Decimal)
                decBalance = value
            End Set
        End Property

        ''' <summary>
        ''' Estado del Comercio
        ''' </summary>
        Public Property MerchantStatus() As Integer
            Get
                Return intMerchantStatus
            End Get
            Set(ByVal value As Integer)
                intMerchantStatus = value
            End Set
        End Property

        ''' <summary>
        ''' Fecha de Activación del Comercio
        ''' </summary>
        Public Property MerchantActivationDate() As DateTime
            Get
                Return dateActivationDate
            End Get
            Set(ByVal value As DateTime)
                dateActivationDate = value
            End Set
        End Property

        ''' <summary>
        ''' Retornar datos del Comercio
        ''' </summary>
        Public Sub getMerchantData(ByVal merchantID As String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosComercio"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("merchantID", merchantID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strMerchantID = merchantID
                        intMerchantType = results.GetInt64(0)
                        decPctGain = results.GetDecimal(1)
                        intMerchantStatus = results.GetInt64(2)
                        decBalance = results.GetDecimal(3)
                        dateActivationDate = results.GetDateTime(4)
                        boolAdminLocalBalance = results.GetBoolean(5)
                    End While
                Else
                    strMerchantID = ""
                    intMerchantType = -1
                    decPctGain = 0
                    intMerchantStatus = -1
                    decBalance = 0
                    dateActivationDate = Now
                End If

            Catch ex As Exception

            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If

                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Activar Comercio JNTS en Conred
        ''' </summary>
        Public Function activate() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean
            Dim intRet As Integer

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActivarComercioJNTS"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("merchantID", strMerchantID))
                command.Parameters.Add(New SqlParameter("merchantType", intMerchantType))
                command.Parameters.Add(New SqlParameter("merchantAdminLocalBalance", boolAdminLocalBalance))
                command.Parameters.Add(New SqlParameter("merchantPctGain", decPctGain))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        intRet = results.GetInt32(0)
                    End While
                Else
                    intRet = 0
                End If

                If intRet = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Editar Comercio
        ''' </summary>
        Public Function editMerchant() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarComercio"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("merchantID", strMerchantID))
                command.Parameters.Add(New SqlParameter("entityType", intMerchantType))
                command.Parameters.Add(New SqlParameter("merchantAdminLocalBalance", boolAdminLocalBalance))
                command.Parameters.Add(New SqlParameter("merchantPctGain", decPctGain))
                command.Parameters.Add(New SqlParameter("merchantStatus", intMerchantStatus))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Movimiento de Saldo Comercio
        ''' </summary>
        Public Function applyMovementMerchant(ByVal intMovementType As Integer, ByVal decAmount As Decimal, ByVal intLogID As Long, _
                                                ByVal intAccountBank As Integer, ByVal strDescription As String, _
                                                ByRef decNewBalance As Decimal) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webModificarSaldoComercio"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("merchantID", strMerchantID))
                command.Parameters.Add(New SqlParameter("movementType", intMovementType))
                command.Parameters.Add(New SqlParameter("movementAmount", decAmount))
                command.Parameters.Add(New SqlParameter("movementLog", intLogID))
                command.Parameters.Add(New SqlParameter("movementAccountBank", intAccountBank))
                command.Parameters.Add(New SqlParameter("movementDescription", strDescription))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                        decNewBalance = results.GetDecimal(1)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Validar Retiros de Saldo al comercio
        ''' </summary>
        Public Function validateNegativeMovement(ByVal decAmount As Decimal, ByVal intMovementType As Integer) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean
            Dim intRet As Integer

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarMovimientoSaldoComercio"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("merchantID", strMerchantID))
                command.Parameters.Add(New SqlParameter("merchantMovementType", intMovementType))
                command.Parameters.Add(New SqlParameter("merchantMovementAmount", decAmount))

                'Ejecutar SP
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        intRet = results.GetInt32(0)
                    End While
                Else
                    intRet = 0
                End If

                If intRet = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Obtiene el Saldo del Comercio
        ''' </summary>
        Public Function getBalanceMerchant() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webObtenerSaldoComercio"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("merchantID", strMerchantID))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        decBalance = results.GetDecimal(0)
                    End While
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
