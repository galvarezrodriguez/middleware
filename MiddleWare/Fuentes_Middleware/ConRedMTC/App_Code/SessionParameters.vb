﻿Imports Microsoft.VisualBasic

Namespace Conred.Sessions

    Public Class SessionParameters

        Public strSelectedMenu As String
        Public strSelectedOption As String
        Public intPasswordErrorCode As Integer
        Public strUserLogin As String
        Public intSelectedProfile As Integer
        Public strSelectedProfile As String
        Public strMerchantID As String
        Public intSelectedBank As Integer
        Public strSelectedBank As String
        Public intSelectedSupplier As Integer
        Public strSelectedSupplier As String
        Public intSelectedModule As Integer
        Public strSelectedModule As String
        Public intSelectedBusinessModel As Integer
        Public strSelectedBusinessModel As String
        Public intSelectedProduct As Integer
        Public strSelectedProduct As String

        'Reports
        Public intReportType As Integer
        Public strReportName As String
        Public dateStartDate As Date
        Public dateEndDate As Date
        Public strMerchantName As String
        Public intSelectedUserID As Integer
        Public strSelectedUserName As String

    End Class

End Namespace
