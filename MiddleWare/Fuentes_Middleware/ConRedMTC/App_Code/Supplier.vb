﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace Conred.Suppliers

    Public Class Supplier

        Private strConnString As String
        Private intSupplierID As Integer
        Private strName As String
        Private strDescription As String
        Private strIsomuxName As String

        Public Sub New(ByVal strConnString As String)
            Me.strConnString = strConnString
        End Sub

        ''' <summary>
        ''' Nombre del Proveedor
        ''' </summary>
        Public Property SupplierName() As String
            Get
                Return strName
            End Get
            Set(ByVal value As String)
                strName = value
            End Set
        End Property

        ''' <summary>
        ''' Descripción del Proveedor
        ''' </summary>
        Public Property supplierDescription() As String
            Get
                Return strDescription
            End Get
            Set(ByVal value As String)
                strDescription = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre del Formateador Destino para el Proveedor
        ''' </summary>
        Public Property IsomuxName() As String
            Get
                Return strIsomuxName
            End Get
            Set(ByVal value As String)
                strIsomuxName = value
            End Set
        End Property

        ''' <summary>
        ''' Código del Proveedor
        ''' </summary>
        Public Property SupplierID() As Integer
            Get
                Return intSupplierID
            End Get
            Set(ByVal value As Integer)
                intSupplierID = value
            End Set
        End Property

        ''' <summary>
        ''' Crear Proveedor
        ''' </summary>
        Public Function createSupplier() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierName", strName))
                command.Parameters.Add(New SqlParameter("supplierDesc", strDescription))
                command.Parameters.Add(New SqlParameter("supplierIsomuxName", strIsomuxName))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Compra de Cupo de Proveedor
        ''' </summary>
        Public Function applyMovementSupplier(ByVal decAmount As Decimal, ByVal intLogID As Long, _
                                                ByVal intAccountBank As Integer, ByVal strDescription As String, _
                                                ByVal decCurrentBalance As Decimal, ByVal decFinalBalance As Decimal) As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webGrabarCompraCupoProveedor"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("supplierID", intSupplierID))
                command.Parameters.Add(New SqlParameter("movementAmount", decAmount))
                command.Parameters.Add(New SqlParameter("movementCurrentBalance", decCurrentBalance))
                command.Parameters.Add(New SqlParameter("movementNewBalance", decFinalBalance))
                command.Parameters.Add(New SqlParameter("movementLog", intLogID))
                command.Parameters.Add(New SqlParameter("movementAccountBank", intAccountBank))
                command.Parameters.Add(New SqlParameter("movementDescription", strDescription))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

    End Class

End Namespace
