﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.VisualBasic

Namespace Conred.Users

    Public Class User

        Private strConnString As String
        Private intUserID As Integer
        Private strLoginName As String
        Private strDisplayName As String
        Private strEmail As String
        Private strPasswordHash As String
        Private intProfile As Integer
        Private intStatus As Integer

        Public Sub New(ByVal strConnString As String, ByVal intUserID As Integer)
            Me.strConnString = strConnString
            Me.intUserID = intUserID
        End Sub

        ''' <summary>
        ''' Login del usuario
        ''' </summary>
        Public Property LoginName() As String
            Get
                Return strLoginName
            End Get
            Set(ByVal value As String)
                strLoginName = value
            End Set
        End Property

        ''' <summary>
        ''' Nombre a mostrar del usuario
        ''' </summary>
        Public Property DisplayName() As String
            Get
                Return strDisplayName
            End Get
            Set(ByVal value As String)
                strDisplayName = value
            End Set
        End Property

        ''' <summary>
        ''' Correo Electrónico del usuario
        ''' </summary>
        Public Property Email() As String
            Get
                Return strEmail
            End Get
            Set(ByVal value As String)
                strEmail = value
            End Set
        End Property

        ''' <summary>
        ''' Perfil del usuario
        ''' </summary>
        Public Property Profile() As Integer
            Get
                Return intProfile
            End Get
            Set(ByVal value As Integer)
                intProfile = value
            End Set
        End Property

        ''' <summary>
        ''' Estado del usuario
        ''' </summary>
        Public Property Status() As Integer
            Get
                Return intStatus
            End Get
            Set(ByVal value As Integer)
                intStatus = value
            End Set
        End Property

        ''' <summary>
        ''' Password del usuario
        ''' </summary>
        Public Property Password() As String
            Get
                Return strPasswordHash
            End Get
            Set(ByVal value As String)
                strPasswordHash = value
            End Set
        End Property

        ''' <summary>
        ''' Retornar datos del Usuario
        ''' </summary>
        Public Sub getUserData()
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strLoginName = results.GetString(0)
                        strDisplayName = results.GetString(1)
                        strEmail = results.GetString(2)
                        intStatus = results.GetInt64(3)
                        intProfile = results.GetInt64(4)
                    End While
                Else
                    strLoginName = ""
                    strDisplayName = ""
                    strEmail = ""
                    intStatus = -1
                    intProfile = -1
                End If

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Retornar datos del Usuario
        ''' </summary>
        Public Sub getUserData(ByVal loginID As String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webConsultarDatosUsuarioPorLogin"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", loginID))

                'Leer Resultado
                results = command.ExecuteReader()
                If results.HasRows Then
                    While results.Read()
                        strLoginName = results.GetString(0)
                        strDisplayName = results.GetString(1)
                        strEmail = results.GetString(2)
                        intStatus = results.GetInt64(3)
                        intProfile = results.GetInt64(4)
                    End While
                Else
                    strLoginName = ""
                    strDisplayName = ""
                    strEmail = ""
                    intStatus = -1
                    intProfile = -1
                End If

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

        ''' <summary>
        ''' Grabar Datos del Usuario
        ''' </summary>
        Public Function saveUserData() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webGrabarDatosUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))
                command.Parameters.Add(New SqlParameter("userName", strDisplayName))
                command.Parameters.Add(New SqlParameter("userEmail", strEmail))

                'Ejecutar SP
                command.ExecuteNonQuery()

                retVal = True

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Actualizar Clave del Usuario
        ''' </summary>
        Public Function updateUserPassword(ByVal currentPassword As String, ByVal newPassword As String) As Integer
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Integer

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCambiarClave"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", intUserID))
                command.Parameters.Add(New SqlParameter("actualHASH", currentPassword))
                command.Parameters.Add(New SqlParameter("newHASH", newPassword))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = 0
                End If

                retVal = status

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Crear Usuario
        ''' </summary>
        Public Function createUser() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCrearUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userName", strDisplayName))
                command.Parameters.Add(New SqlParameter("userID", strLoginName))
                command.Parameters.Add(New SqlParameter("userEmail", strEmail))
                command.Parameters.Add(New SqlParameter("userHash", strPasswordHash))
                command.Parameters.Add(New SqlParameter("userProfileID", intProfile))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar data reader por Default
                If Not (results Is Nothing) Then
                    results.Close()
                End If
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Editar Usuario
        ''' </summary>
        Public Function editUser() As Boolean
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()
            Dim results As SqlDataReader
            Dim status As Integer
            Dim retVal As Boolean

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webActualizarUsuario"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userName", strDisplayName))
                command.Parameters.Add(New SqlParameter("userID", strLoginName))
                command.Parameters.Add(New SqlParameter("userEmail", strEmail))
                command.Parameters.Add(New SqlParameter("userHash", strPasswordHash))
                command.Parameters.Add(New SqlParameter("userProfileID", intProfile))
                command.Parameters.Add(New SqlParameter("userStatusID", intStatus))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = False
                End If

                If status = 1 Then
                    retVal = True
                Else
                    retVal = False
                End If

            Catch ex As Exception
                retVal = False
            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

            Return retVal

        End Function

        ''' <summary>
        ''' Cerrar Sesión WEB de un Usuario Específico
        ''' </summary>
        Public Sub closeUserSession(ByVal UserLogin As String)
            Dim connection As New SqlConnection(strConnString)
            Dim command As New SqlCommand()

            Try
                'Abrir Conexion
                connection.Open()

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webCerrarSesionWebSeguridad"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("UserLogin", UserLogin))

                'Leer Resultado
                command.ExecuteNonQuery()

            Catch ex As Exception

            Finally
                'Cerrar conexion por Default
                If Not (connection Is Nothing) Then
                    connection.Close()
                End If
            End Try

        End Sub

    End Class

End Namespace
