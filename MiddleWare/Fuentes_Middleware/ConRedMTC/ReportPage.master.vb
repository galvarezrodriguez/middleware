﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Security

Partial Class ReportPage
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        On Error Resume Next

        Dim objAccessToken As AccessToken = Session("AccessToken")

        lblUserName.Text = objAccessToken.DisplayName
        lblDateTime.Text = Now().ToString

        If Not Page.IsPostBack Then

        End If
    End Sub

    Protected Sub lnkLogo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLogo.Click
        'Ir a Inicio
        Session("SelectedMenu") = ""
        Session("SelectedOption") = ""

        Response.Redirect("~/Index.aspx")
    End Sub
End Class

