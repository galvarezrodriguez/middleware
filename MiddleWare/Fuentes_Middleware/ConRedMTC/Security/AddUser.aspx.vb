﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Security_AddUser
    Inherits Conred.Web.BasePage

    Private ReadOnly SEPARATOR As String = " "
    Private ReadOnly SEPARATOR_LOGIN As String = "."

    Public userObj As User

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Set New Data
        userObj.DisplayName = txtUserName.Text
        userObj.Email = txtEmail.Text
        userObj.LoginName = txtLogin.Text
        userObj.Password = txtPassword.Text
        userObj.Profile = ddlUserProfile.SelectedValue

        'Save Data
        If userObj.createUser() Then
            'New User Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Usuario Creado. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Usuario creado correctamente."
            clearForm()
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Usuario. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error creando usuario."
        End If

    End Sub

    Private Sub clearForm()
        txtUserName.Text = ""
        txtEmail.Text = ""
        txtLogin.Text = ""
        txtPassword.Text = ""
        ddlUserProfile.SelectedValue = -1
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtUserName.Focus()
        End If
    End Sub

    Protected Sub btnValidateUserName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnValidateUserName.Click

        'Validar Campo Nombre de Usuario
        If txtUserName.Text.Length < 10 Or txtUserName.Text.Trim().Split(SEPARATOR).Length <= 1 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Ingrese el campo Nombre Completo correctamente. Mínimo un(1) Nombre y un(1) Apellido."
            Exit Sub
        End If

        'Set Login
        txtLogin.Text = getUserLogin()

        If txtLogin.Text.Trim().Length <= 0 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Login Ya registrado, Digite otro nombre."
        Else
            pnlError.Visible = False
            txtPassword.Focus()
        End If

    End Sub

    Function changeNonStandardChars(ByVal data As String) As String
        Dim retVal As String = ""

        data = data.ToLower().Replace("á", "a")
        data = data.ToLower().Replace("é", "e")
        data = data.ToLower().Replace("í", "i")
        data = data.ToLower().Replace("ó", "o")
        data = data.ToLower().Replace("ú", "u")
        data = data.ToLower().Replace("ñ", "n")

        retVal = data

        Return retVal
    End Function

    Function getUserLogin() As String
        Dim connection As New SqlConnection(ConnectionString())
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim retVal As String = ""
        Dim status As Integer = 0
        Dim i As Integer = 0
        Dim tmpUserName() As String = changeNonStandardChars(txtUserName.Text).Trim().Split(SEPARATOR)
        Dim posibleUserName As String = ""

        Try
            'Abrir Conexion
            connection.Open()

            For i = 1 To tmpUserName(0).Length
                If tmpUserName.Length = 2 Then
                    posibleUserName = tmpUserName(0).Substring(0, i) & SEPARATOR_LOGIN & tmpUserName(1)
                ElseIf tmpUserName.Length = 3 Then
                    posibleUserName = tmpUserName(0).Substring(0, i) & tmpUserName(1).Substring(0, 1) & SEPARATOR_LOGIN & tmpUserName(2)
                ElseIf tmpUserName.Length = 4 Then
                    posibleUserName = tmpUserName(0).Substring(0, i) & tmpUserName(1).Substring(0, 1) & tmpUserName(2).Substring(0, 1) & SEPARATOR_LOGIN & tmpUserName(3)
                End If

                'Verify Login vs. DB

                command.Connection = connection
                command.CommandType = CommandType.StoredProcedure
                command.CommandText = "sp_webValidarLoginName"
                command.Parameters.Clear()
                command.Parameters.Add(New SqlParameter("userID", posibleUserName))

                'Ejecutar SP
                results = command.ExecuteReader()

                If results.HasRows Then
                    While results.Read()
                        status = results.GetInt32(0)
                    End While
                Else
                    retVal = ""
                End If

                If status = 0 Then
                    'Login Disponible
                    retVal = posibleUserName
                    Exit For
                Else
                    retVal = ""
                End If

                results.Close()
                results = Nothing

            Next i

            connection.Close()
        Catch ex As Exception
            retVal = ""
            HandleErrorRedirect(ex)
        End Try

        Return retVal

    End Function

End Class
