﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Security_EditUser
    Inherits Conred.Web.BasePage

    Private ReadOnly SEPARATOR As String = " "
    Private ReadOnly SEPARATOR_LOGIN As String = "."

    Public userObj As User

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Set New Data
        userObj.DisplayName = txtUserName.Text
        userObj.Email = txtEmail.Text
        userObj.LoginName = txtLogin.Text
        userObj.Profile = ddlUserProfile.SelectedValue
        userObj.Status = ddlUserStatus.SelectedValue

        If chkNewPassword.Checked = True Then
            userObj.Password = txtPassword.Text
        Else
            userObj.Password = ""
        End If

        'Update Data
        If userObj.editUser() Then
            'User Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Usuario Editado. Datos[ " & getFormDataLog() & " ]", "")
            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Usuario editado correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error Editando Usuario. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando usuario."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If objSessionParams.strUserLogin <> "" Then
                'Edit Mode
                userObj = New User(strConnectionString, objAccessToken.UserID)
                userObj.getUserData(objSessionParams.strUserLogin)

                'Set Data
                txtUserName.Text = userObj.DisplayName
                txtEmail.Text = userObj.Email
                txtLogin.Text = userObj.LoginName
                txtPassword.Text = userObj.Password
                ddlUserProfile.SelectedValue = userObj.Profile
                ddlUserStatus.SelectedValue = userObj.Status

            End If

            txtUserName.Focus()
        End If
    End Sub

    Protected Sub chkNewPassword_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNewPassword.CheckedChanged

        If chkNewPassword.Checked = True Then
            txtPassword.Enabled = True
            txtPassword.Focus()
        Else
            txtPassword.Enabled = False
        End If

    End Sub
End Class
