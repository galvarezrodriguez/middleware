﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Security_ListUsers
    Inherits Conred.Web.BasePage

    Public userObj As User

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        End If
    End Sub

    Protected Sub grdUsers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdUsers.RowCommand
        Try

            Select Case e.CommandName

                Case "EditUser"
                    grdUsers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.strUserLogin = grdUsers.SelectedDataKey.Values.Item("usu_login")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditUser.aspx", False)

                Case "LogoutUser"
                    grdUsers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    lblUserName.Text = grdUsers.SelectedDataKey.Values.Item("usu_nombre")
                    lblLogin.Text = grdUsers.SelectedDataKey.Values.Item("usu_login")

                    grdUsers.SelectedIndex = -1

                    modalPopupExt.Show()

                Case Else
                    objSessionParams.strUserLogin = ""
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    grdUsers.SelectedIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnLogoutUser_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogoutUser.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "CloseSession")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        userObj = New User(strConnectionString, objAccessToken.UserID)

        'Close Session
        userObj.closeUserSession(lblLogin.Text.Trim())

        'Session Closed OK
        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "CloseSession", "Sesión Web de Usuario Cerrada. Datos[ " & lblLogin.Text.Trim() & " ]", "")

        pnlMsg.Visible = True
        lblMsg.Text = "Sesión Web de Usuario cerrada correctamente."

    End Sub
End Class
