﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListUsers.aspx.vb" Inherits="Security_ListUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Usuarios
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Security/Manager.aspx">Seguridad</asp:LinkButton>
    </li>
    <li>Listado de Usuarios</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Listado de Usuarios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite ver el listado de usuarios del sistema administrador web:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
        <asp:GridView ID="grdUsers" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsListUsers" ForeColor="#333333" Width="100%" 
            CssClass="mGrid" DataKeyNames="usu_login,usu_nombre" 
            EmptyDataText="No existen Usuarios creados.">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="usu_nombre" HeaderText="Nombre de Usuario" 
                    SortExpression="usu_nombre" />
                <asp:BoundField DataField="usu_email" HeaderText="Correo Electrónico" 
                    SortExpression="usu_email" />
                <asp:BoundField DataField="usu_login" HeaderText="Login" 
                    SortExpression="usu_login" />
                <asp:BoundField DataField="perf_nombre" HeaderText="Perfil" 
                    SortExpression="perf_nombre" />
                <asp:BoundField DataField="descripcion" HeaderText="Estado" 
                    SortExpression="descripcion" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditUser" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Usuario" CommandArgument='<%# grdUsers.Rows.Count %>' />
                        <asp:ImageButton ID="imgLogout" runat="server" CausesValidation="False" 
                            CommandName="LogoutUser" ImageUrl="~/img/icons/16x16/lock_open.png" Text="Cerrar Sesión Web" 
                            ToolTip="Cerrar Sesión Web del Usuario" CommandArgument='<%# grdUsers.Rows.Count %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    
	    <asp:SqlDataSource ID="dsListUsers" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
            SelectCommand="sp_webConsultarUsuarios" SelectCommandType="StoredProcedure"></asp:SqlDataSource>

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Security/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>
    
    <asp:Panel ID="pnlPopupLogout" runat="server" CssClass="modalPopup">
        <asp:Label ID="lblTitulo" runat="server" 
            Text="Confirma que desea cerrar la sesión web?" Font-Bold="True" 
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <b>Nombre de Usuario:</b>&nbsp;&nbsp;<asp:Label ID="lblUserName" runat="server" Text="Label"></asp:Label>
        <br />
        <b>Login de Usuario:</b>&nbsp;&nbsp;<asp:Label ID="lblLogin" runat="server" Text="Label"></asp:Label>
        <br />
        <br />        
        <asp:Button ID="btnLogoutUser" runat="server" Text="Aceptar" CssClass="st-button" />
        <br />

        <asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" 
            Font-Size="Small">Cancelar</asp:LinkButton>
    </asp:Panel>
    
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupLogout">
    </ajaxtoolkit:ModalPopupExtender>
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

