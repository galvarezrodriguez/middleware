﻿Imports Conred.Security
Imports Conred.Sessions

Partial Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim objAccessToken As AccessToken = Session("AccessToken")
        Dim objSessionParams As SessionParameters = Session("SessionParameters")

        If Not objAccessToken Is Nothing Then
            objAccessToken.Validate("Session", "End")
            objAccessToken.closeSession()
            objAccessToken.Active = False
        End If

        If Not objAccessToken Is Nothing Then
            'Limpiar Datos Session
            objSessionParams.strSelectedMenu = ""
            objSessionParams.strSelectedOption = ""
            objSessionParams.strMerchantID = ""
            objSessionParams.strMerchantName = ""
            objSessionParams.strReportName = ""
            objSessionParams.strSelectedBank = ""
            objSessionParams.strSelectedProfile = ""
            objSessionParams.strSelectedSupplier = ""
            objSessionParams.strSelectedUserName = ""
            objSessionParams.strUserLogin = ""
            objSessionParams.intPasswordErrorCode = -1
            objSessionParams.intSelectedProfile = -1
            objSessionParams.intReportType = -1
            objSessionParams.intSelectedBank = -1
            objSessionParams.intSelectedSupplier = -1
            objSessionParams.intSelectedUserID = -1
        End If

        Session.Clear()
        Session.Abandon()

        Response.Cookies.Add(New HttpCookie("ASP.NET_SessionId", ""))
        Response.Redirect("Login.aspx?exit=-1")

    End Sub
End Class
