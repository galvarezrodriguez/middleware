﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddAgreementXProduct.aspx.vb" Inherits="Suppliers_AddAgreementXProduct" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Convenios X Producto
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/Manager.aspx">Proveedores</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkSuppliers" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/ListSuppliers.aspx">Listado de Proveedores</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkProducts" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/AddProductsXSupplier.aspx">Productos X Proveedor</asp:LinkButton>
    </li>    
    <li>Agregar Convenios X Producto</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Convenios X Producto
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar convenios x productos de recaudo:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Producto</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre del Producto:</span>	
                <asp:TextBox ID="txtProductName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Producto." Enabled="False"></asp:TextBox>
                <asp:TextBox ID="txtProductID" runat="server" Visible="False"></asp:TextBox>                    
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext"><b>Datos del Convenio</b></span>	
                <div class="clear"></div>
            </div>        

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Nombre:</span>	
                            <asp:TextBox ID="txtAgreementName" CssClass="st-forminput" style="width:310px" 
                                runat="server" TabIndex="1" MaxLength="100" 
                                ToolTip="Nombre del Convenio." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>                      
                    </td>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Código EAN:</span>	
                            <asp:TextBox ID="txtEANCode" CssClass="st-forminput" style="width:210px" 
                                runat="server" TabIndex="2" MaxLength="20" 
                                ToolTip="Código EAN del producto." 
                                onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>                     
                    </td>
                </tr>
                
                <tr>
                    <td colspan=2>
                        <div class="st-form-noline">

                            <asp:CheckBox ID="chkPartialAmount" CssClass="uniform" runat="server" 
                                Text="Valor Parcial?" AutoPostBack="True" 
                                ToolTip="El Código de Barras contiene el Valor a Pagar?" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="chkDoubleRef" CssClass="uniform" runat="server" 
                                Text="Ref. Doble?" AutoPostBack="True" 
                                ToolTip="El Código de Barras contiene una segunda referencia de Pago?" />
                        
                            <br /><br />
                        
                            <table>
                                <tr>
                                    <td style="width: 300px">
                                        <span class="st-labeltext">Pos. Inicial Ref1:</span>	                
                                        <asp:TextBox ID="txtInitPosRef1" CssClass="st-forminput" style="width:40px" 
                                            runat="server" TabIndex="3" MaxLength="2" 
                                            ToolTip="Posición Inicial Referencia1." onkeydown = "return (event.keyCode!=13);">0</asp:TextBox>  
                                        <br />
                                        <span class="st-labeltext">Longitud Ref1:</span>	                
                                        <asp:TextBox ID="txtLenRef1" CssClass="st-forminput" style="width:40px" 
                                            runat="server" TabIndex="4" MaxLength="2" 
                                            ToolTip="Longitud Referencia1." onkeydown = "return (event.keyCode!=13);">0</asp:TextBox>                                    
                                    </td>
                                    <td style="width: 300px">
                                        <span class="st-labeltext">Pos. Inicial Valor:</span>	                
                                        <asp:TextBox ID="txtInitPosAmount" CssClass="st-forminput" style="width:40px" 
                                            runat="server" TabIndex="5" MaxLength="2" 
                                            ToolTip="Posición Inicial del Valor." onkeydown = "return (event.keyCode!=13);">0</asp:TextBox>  
                                        <br />
                                        <span class="st-labeltext">Longitud Valor:</span>	                
                                        <asp:TextBox ID="txtLenAmount" CssClass="st-forminput" style="width:40px" 
                                            runat="server" TabIndex="6" MaxLength="2" 
                                            ToolTip="Longitud del Valor." onkeydown = "return (event.keyCode!=13);" 
                                            ValidationGroup="0">0</asp:TextBox>
                                    </td>
                                    <td style="width: 300px">
                                        <span class="st-labeltext">Pos. Inicial Ref2:</span>	                
                                        <asp:TextBox ID="txtInitPosRef2" CssClass="st-forminput" style="width:40px" 
                                            runat="server" TabIndex="7" MaxLength="2" 
                                            ToolTip="Posición Inicial Referencia2." 
                                            onkeydown = "return (event.keyCode!=13);" Enabled="False">0</asp:TextBox>  
                                        <br />
                                        <span class="st-labeltext">Longitud Ref2:</span>	                
                                        <asp:TextBox ID="txtLenRef2" CssClass="st-forminput" style="width:40px" 
                                            runat="server" TabIndex="8" MaxLength="2" 
                                            ToolTip="Longitud Referencia2." onkeydown = "return (event.keyCode!=13);" 
                                            Enabled="False">0</asp:TextBox>                                    
                                    </td>
                                    <td>
                                        <asp:Button ID="btnTestBarcode" runat="server" Text="Probar Lectura" 
                                            CssClass="st-button" TabIndex="9" />                                        
                                    </td>
                                </tr>
                            </table>
                        </div>                 
                    </td>
                </tr>
            </table>
             
            <div class="button-box">
                <asp:Button ID="btnAddAgreement" runat="server" Text="Adicionar Convenio" 
                    CssClass="st-button" TabIndex="10" OnClientClick="return validateAddAgreement();" />
                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="11" Visible="false" />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Convenios creados en el producto</h3>
        </div>
        
        <p style="padding-left: 20px">
            EAN Convenio: 
            <asp:TextBox ID="txtEANAgreementFilter" runat="server" TabIndex="1" MaxLength="20" CssClass="st-forminput-active"></asp:TextBox>
            <asp:Button ID="btnFilter" runat="server" Text="Filtrar" CssClass="st-button" TabIndex="2" />                
        </p>
        
        <div class="body">         
            <asp:GridView ID="grdAgreementsXProduct" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsAgreementsXProduct" ForeColor="#333333" Width="99%" 
                CssClass="mGridCenter" DataKeyNames="conv_id" 
                EmptyDataText="No existen Convenios creados para este producto." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="conv_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("conv_id") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Eval("conv_id") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" SortExpression="conv_nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("conv_nombre") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNameGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="50" Text='<%# Bind("conv_nombre") %>' Width="180" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>                            
                        </EditItemTemplate>           
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="EAN" InsertVisible="False" 
                        SortExpression="conv_codigo_EAN">
                        <ItemTemplate>
                            <asp:Label ID="lblID3" runat="server" Text='<%# Bind("conv_codigo_EAN") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>                        
                            <asp:TextBox ID="txtEANCodeGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="20" Text='<%# Bind("conv_codigo_EAN") %>' Width="100" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Cód. Parcial?" SortExpression="conv_cod_parcial">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Bind("conv_cod_parcial") %>' Enabled="false"/>                            
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkActiveGrid" runat="server" Checked='<%# Bind("conv_cod_parcial") %>' Enabled="true" />
                        </EditItemTemplate>                        
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ref. Doble?" SortExpression="conv_ref_doble">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDoubleRef" runat="server" Checked='<%# Bind("conv_ref_doble") %>' Enabled="false"/>                            
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkDoubleRefGrid" runat="server" Checked='<%# Bind("conv_ref_doble") %>' Enabled="true" />
                        </EditItemTemplate>                        
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Pos. Ini Ref1" SortExpression="conv_pos_ini_ref1">
                        <ItemTemplate>
                            <asp:Label ID="lblInitPosRef1" runat="server" Text='<%# Bind("conv_pos_ini_ref1") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInitPosRef1Grid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("conv_pos_ini_ref1") %>' onkeydown = "return (event.keyCode!=13);" Width="15"></asp:TextBox>                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Long. Ref1" SortExpression="conv_longitud_ref1">
                        <ItemTemplate>
                            <asp:Label ID="lblLenRef1" runat="server" Text='<%# Bind("conv_longitud_ref1") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLenRef1Grid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("conv_longitud_ref1") %>' onkeydown = "return (event.keyCode!=13);" Width="15"></asp:TextBox>                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pos. Ini Valor" SortExpression="conv_pos_ini_valor">
                        <ItemTemplate>
                            <asp:Label ID="lblInitPosAmount" runat="server" Text='<%# Bind("conv_pos_ini_valor") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInitPosAmountGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("conv_pos_ini_valor") %>' onkeydown = "return (event.keyCode!=13);" Width="15"></asp:TextBox>                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Long. Valor" SortExpression="conv_longitud_valor">
                        <ItemTemplate>
                            <asp:Label ID="lblLenAmount" runat="server" Text='<%# Bind("conv_longitud_valor") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLenAmountGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("conv_longitud_valor") %>' onkeydown = "return (event.keyCode!=13);" Width="15"></asp:TextBox>                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Pos. Ini Ref2" SortExpression="conv_pos_ini_ref2">
                        <ItemTemplate>
                            <asp:Label ID="lblInitPosRef2" runat="server" Text='<%# Bind("conv_pos_ini_ref2") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtInitPosRef2Grid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("conv_pos_ini_ref2") %>' onkeydown = "return (event.keyCode!=13);" Width="15"></asp:TextBox>                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Long. Ref2" SortExpression="conv_longitud_ref2">
                        <ItemTemplate>
                            <asp:Label ID="lblLenRef2" runat="server" Text='<%# Bind("conv_longitud_ref2") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtLenRef2Grid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("conv_longitud_ref2") %>' onkeydown = "return (event.keyCode!=13);" Width="15"></asp:TextBox>                            
                        </EditItemTemplate>
                        <ItemStyle HorizontalAlign="Center" Width="50px" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                                CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                                ToolTip="Editar Convenio" CommandArgument='<%# grdAgreementsXProduct.Rows.Count %>' />
                            <asp:ImageButton ID="imgDelete" runat="server" CausesValidation="False" 
                                CommandName="Delete" ImageUrl="~/img/icons/16x16/delete.png" Text="Eliminar Convenio" 
                                ToolTip="Eliminar Convenio" CommandArgument='<%# grdAgreementsXProduct.Rows.Count %>' />                                
                            <asp:ImageButton ID="imgTestBarcode" runat="server" CausesValidation="False" 
                                CommandName="TestBarcode" ImageUrl="~/img/icons/16x16/bar_code.png" Text="Prueba de Lectura Código de Barras" 
                                ToolTip="Prueba de Lectura Código de Barras" CommandArgument='<%# grdAgreementsXProduct.Rows.Count %>' />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkButtonUpdate" runat="server" CausesValidation="True" 
                                CommandArgument="<%# grdAgreementsXProduct.Rows.Count %>" CommandName="Update" 
                                Text="Actualizar"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="lnkButtonCancel" runat="server" CausesValidation="False" 
                                CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            
	        <asp:SqlDataSource ID="dsAgreementsXProduct" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                SelectCommand="SELECT conv_id, conv_nombre, conv_codigo_EAN, conv_cod_parcial, conv_ref_doble, conv_pos_ini_ref1, conv_longitud_ref1, conv_pos_ini_valor, conv_longitud_valor, conv_pos_ini_ref2, conv_longitud_ref2 FROM ConvenioXProducto WHERE conv_producto_id = @conv_producto_id AND (conv_codigo_EAN=@conv_codigo_EAN OR @conv_codigo_EAN='-1') ORDER BY 1"
                InsertCommand="INSERT INTO ConvenioXProducto(conv_nombre, conv_codigo_EAN, conv_cod_parcial, conv_ref_doble, conv_pos_ini_ref1, conv_longitud_ref1, conv_pos_ini_valor, conv_longitud_valor, conv_pos_ini_ref2, conv_longitud_ref2, conv_producto_id) 
                                VALUES(@conv_nombre, @conv_codigo_EAN, @conv_cod_parcial, @conv_ref_doble, @conv_pos_ini_ref1, @conv_longitud_ref1, @conv_pos_ini_valor, @conv_longitud_valor, @conv_pos_ini_ref2, @conv_longitud_ref2, @conv_producto_id)"
                UpdateCommand="UPDATE ConvenioXProducto SET conv_nombre=@conv_nombre, conv_codigo_EAN=@conv_codigo_EAN, conv_cod_parcial=@conv_cod_parcial, conv_ref_doble=@conv_ref_doble, conv_pos_ini_ref1=@conv_pos_ini_ref1,
                                conv_longitud_ref1=@conv_longitud_ref1, conv_pos_ini_valor=@conv_pos_ini_valor, conv_longitud_valor=@conv_longitud_valor, conv_pos_ini_ref2=@conv_pos_ini_ref2, conv_longitud_ref2=@conv_longitud_ref2 WHERE conv_id=@conv_id"
                DeleteCommand="DELETE ConvenioXProducto WHERE conv_id = @conv_id">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtProductID" Name="conv_producto_id" PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtEANAgreementFilter" Name="conv_codigo_EAN" PropertyName="Text" DefaultValue="-1" />                    
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="conv_id" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="conv_nombre" />
                    <asp:Parameter Name="conv_codigo_EAN" />
                    <asp:Parameter Name="conv_cod_parcial"/>
                    <asp:Parameter Name="conv_ref_doble" />
                    <asp:Parameter Name="conv_pos_ini_ref1" />
                    <asp:Parameter Name="conv_longitud_ref1" />                    
                    <asp:Parameter Name="conv_pos_ini_valor" />
                    <asp:Parameter Name="conv_longitud_valor" />
                    <asp:Parameter Name="conv_pos_ini_ref2" />
                    <asp:Parameter Name="conv_longitud_ref2" />
                    <asp:Parameter Name="conv_id" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtAgreementName" Name="conv_nombre" 
                        PropertyName="Text" />                    
                    <asp:ControlParameter ControlID="txtEANCode" Name="conv_codigo_EAN" 
                        PropertyName="Text" />
                    <asp:ControlParameter ControlID="chkPartialAmount" Name="conv_cod_parcial" 
                        PropertyName="Checked" />                        
                    <asp:ControlParameter ControlID="chkDoubleRef" Name="conv_ref_doble" 
                        PropertyName="Checked" />
                    <asp:ControlParameter ControlID="txtInitPosRef1" Name="conv_pos_ini_ref1" 
                        PropertyName="Text" />                        
                    <asp:ControlParameter ControlID="txtLenRef1" Name="conv_longitud_ref1" 
                        PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtInitPosAmount" Name="conv_pos_ini_valor" 
                        PropertyName="Text" />                        
                    <asp:ControlParameter ControlID="txtLenAmount" Name="conv_longitud_valor" 
                        PropertyName="Text" />
                    <asp:ControlParameter ControlID="txtInitPosRef2" Name="conv_pos_ini_ref2" 
                        PropertyName="Text" />                        
                    <asp:ControlParameter ControlID="txtLenRef2" Name="conv_longitud_ref2" 
                        PropertyName="Text" />                        
                    <asp:ControlParameter ControlID="txtProductID" Name="conv_producto_id" 
                        PropertyName="Text" />
                </InsertParameters>
            </asp:SqlDataSource> 
            
            <br />
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Suppliers/AddProductsXSupplier.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>    
    
    <asp:Panel ID="pnlPopupTestBarcode" runat="server" CssClass="modalPopupTestBarcode">
        <asp:Label ID="lblTitulo" runat="server" 
            Text="Prueba de Lectura Código de Barras:" Font-Bold="True" 
            Font-Size="Medium"></asp:Label>
        <hr />
        <br />
        
        <asp:Label ID="lblAgreementName" runat="server" Text="Convenio:" Font-Bold="True" Font-Size="Small"></asp:Label>        
        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblName" runat="server" Font-Size="Small"></asp:Label>
        
        <br />
        <br />
        <asp:CheckBox ID="chk1" Text="Valor Parcial?" CssClass="uniform" runat="server" Enabled="False" />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <asp:CheckBox ID="chk2" Text="Ref. Doble?" CssClass="uniform" runat="server" Enabled="False" />        
        
        <br />
        <br />
        
        <table style="width: 500px;" align="center">
            <tr>
                <td>
                    <b>Pos. Inicial Ref1:</b>
                </td>
                <td>
                    <asp:Label ID="lblTest1" runat="server" Text="Label"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td>
                    <b>Pos. Inicial Valor:</b>
                </td>
                <td>
                    <asp:Label ID="lblTest3" runat="server" Text="Label"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>                
                <td>
                    <b>Pos. Inicial Ref2:</b>
                </td>
                <td>
                    <asp:Label ID="lblTest5" runat="server" Text="Label"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>                
            </tr>
            <tr>
                <td>
                    <b>Longitud Ref1:</b>
                </td>
                <td>
                    <asp:Label ID="lblTest2" runat="server" Text="Label"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>
                <td>
                    <b>Longitud Valor:</b>                    
                </td>
                <td>
                    <asp:Label ID="lblTest4" runat="server" Text="Label"></asp:Label> 
                    &nbsp;&nbsp;&nbsp;&nbsp;               
                </td>
                <td>
                    <b>Longitud Ref2:</b>
                </td>
                <td>
                    <asp:Label ID="lblTest6" runat="server" Text="Label"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                </td>
            </tr>            
        </table>

        <br />
        <b>BarCode:</b> 
        <asp:TextBox ID="txtBarcode" runat="server" Width="400px" onkeydown = "return validateKey(event)"></asp:TextBox>        
        <br />
        <br />
        
        <div id="pnlUnpack1" style="border: solid 1px black; visibility: hidden">
            <div id="pnlUnpack2" style="padding-left: 10px;">
            
            </div>
        </div>

        <br />
        <asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" Font-Size="Small">Cerrar</asp:LinkButton>
    </asp:Panel>
    
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupTestBarcode">
    </ajaxtoolkit:ModalPopupExtender>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorAgreement.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

