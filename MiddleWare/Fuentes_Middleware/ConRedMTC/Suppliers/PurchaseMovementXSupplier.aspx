﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="PurchaseMovementXSupplier.aspx.vb" Inherits="Suppliers_PurchaseMovementXSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Compra de Cupo Proveedor
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/Manager.aspx">Proveedores</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/ListSuppliers.aspx">Lista de Proveedores</asp:LinkButton>
    </li>
    <li>Compra de Cupo Proveedor</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Compra de Cupo Proveedor
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite realizar compras de cupo transaccional hacia los proveedores:</p>
    
    <br />

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>   
    
    <asp:Panel ID="pnlError" runat="server" Visible="False">    
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Proveedor</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Proveedor:</span>	
                <asp:TextBox ID="txtSupplierName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Proveedor" 
                    onkeydown = "return (event.keyCode!=13);" Enabled="False" Font-Size="11pt"></asp:TextBox>
                <asp:TextBox ID="txtSupplierID" runat="server" Visible="False"></asp:TextBox>                    
                <div class="clear"></div>
            </div>  
        
            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Saldo Actual Proveedor $:</span>	
                            <asp:TextBox ID="txtCurrentBalance" CssClass="st-forminput" style="width:210px" 
                                runat="server" TabIndex="1" MaxLength="10" 
                                ToolTip="Saldo Actual." 
                                onkeydown = "return (event.keyCode!=13);" Font-Size="11pt"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="clear"></div>
                        </div>                    
                    </td>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Saldo Final Proveedor $:</span>	
                            <asp:TextBox ID="txtFinalBalance" CssClass="st-forminput" style="width:200px" 
                                runat="server" TabIndex="1" MaxLength="10" 
                                ToolTip="Saldo Final." 
                                onkeydown = "return (event.keyCode!=13);" Font-Size="11pt"></asp:TextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <div class="clear"></div>
                        </div>                    
                    </td>                    
                </tr>    
                <tr>
                    <td>
                        <div class="st-form-noline">	
                            <span class="st-labeltext">Cuenta:</span>	
                            <asp:DropDownList ID="ddlBankAccount" class="uniform" runat="server" 
                                DataSourceID="dsAccounts" DataTextField="nombre_cuenta" 
                                DataValueField="cue_id" 
                                ToolTip="Seleccione la cuenta para el movimiento." TabIndex="2">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsAccounts" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" SelectCommand="SELECT -1 AS cue_id, '   ' AS nombre_cuenta UNION ALL SELECT CB.cue_id, B.ban_nombre + ' - ' + TC.nombre + ' - ' + CB.cue_numero AS nombre_cuenta
                                FROM CuentaBanco CB INNER JOIN TipoCuenta TC ON (TC.id = CB.cue_tipo_cuenta_id) INNER JOIN Banco B ON (B.ban_id = CB.cue_banco_id)"></asp:SqlDataSource>
                            <div class="clear"></div>
                        </div>                    
                    </td>
                    <td>
                        <div class="st-form-noline">	
                            <span class="st-labeltext">Valor Compra $:</span>&nbsp;<asp:TextBox 
                                ID="txtMovementAmount" CssClass="st-forminput" style="width:200px" 
                                runat="server" TabIndex="3" MaxLength="10" 
                                ToolTip="Monto del Movimiento" 
                                onkeydown = "return (event.keyCode!=13);" 
                                onpaste="javascript:return false;" Font-Bold="True" ForeColor="Red" 
                                Font-Size="13pt"></asp:TextBox>
                            <span>
                            </span>
                            <div class="clear"></div>
                        </div>                     
                    </td>
                </tr>            
            </table>
            
            <div class="st-form-line">	
                <span class="st-labeltext">Observación:</span>	
                <asp:TextBox ID="txtDescription" CssClass="st-forminput" style="width:510px" 
                    runat="server" ToolTip="Descripción del movimiento." 
                    TabIndex="4" onkeydown = "return (event.keyCode!=13);" MaxLength="250"></asp:TextBox>                
                <div class="clear"></div>
            </div>
            
            <div class="button-box">
                <asp:Button ID="btnApplyMovement" runat="server" Text="Aplicar" 
                    CssClass="st-button" TabIndex="5" 
                    onclientclick="return validateApplyMovement()" />
                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="6" Visible="false" />
            </div>
            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Últimas compras realizadas</h3>
        </div>
        <div class="body">    
            <br />     
    
                <asp:GridView ID="grdLastPurchases" runat="server" 
                    AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                    DataSourceID="dsLastPurchases" ForeColor="#333333" 
                    CssClass="mGridCenter" 
                    EmptyDataText="No existen compras de cupo para el Proveedor seleccionado." 
                    HorizontalAlign="Center" PageSize="5">
                    <RowStyle BackColor="White" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="comp_fecha" HeaderText="Fecha" 
                            SortExpression="comp_fecha" >
                        </asp:BoundField>
                        <asp:BoundField DataField="comp_saldo_inicial" HeaderText="Saldo Inicial" 
                            SortExpression="comp_saldo_inicial" DataFormatString="{0:C2}">
                        </asp:BoundField>
                        <asp:BoundField DataField="comp_valor" HeaderText="Valor" 
                            SortExpression="comp_valor" DataFormatString="{0:C2}">
                        </asp:BoundField>                    
                        <asp:BoundField DataField="comp_saldo_final" HeaderText="Saldo Final" 
                            SortExpression="comp_saldo_final" DataFormatString="{0:C2}">
                        </asp:BoundField>                    
                        <asp:BoundField DataField="Cuenta" 
                            HeaderText="Cuenta" SortExpression="Cuenta" ReadOnly="True" >
                        </asp:BoundField>
                        <asp:BoundField DataField="comp_observacion" 
                            HeaderText="Observación" SortExpression="comp_observacion" >
                        </asp:BoundField>
                        <asp:BoundField DataField="usu_nombre" 
                            HeaderText="Usuario" SortExpression="usu_nombre" >
                        </asp:BoundField>
                        
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                        CssClass="pgr" Font-Underline="False" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                        BorderWidth="1px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView>            
                
                <asp:SqlDataSource ID="dsLastPurchases" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="sp_webConsultarComprasXProveedor" 
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtSupplierID" Name="supplierID" 
                                    PropertyName="Text" />
                    </SelectParameters>
                </asp:SqlDataSource>     
        </div>
    </div>    
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Suppliers/ListSuppliers.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>
    
    <asp:Panel ID="pnlPopupMovement" runat="server" CssClass="modalPopupMovement">
        <asp:Label ID="lblTitulo" runat="server" 
            Text="Confirma que desea realizar el siguiente movimiento?" Font-Bold="True" 
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <table class="mGridCenter">
            <tr>
                <td><b>Proveedor:</b></td>
                <td><asp:Label ID="lblSupplierName" runat="server" Text="Label" Font-Bold="True" Font-Size="12pt"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Saldo Actual:</b></td>
                <td><asp:Label ID="lblCurrentAmount" runat="server" Text="Label" Font-Bold="True" Font-Size="12pt"></asp:Label></td>
            </tr>            
            <tr>
                <td><b>Saldo Actual:</b></td>
                <td><asp:Label ID="lblFinalAmount" runat="server" Text="Label" Font-Bold="True" Font-Size="12pt"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Cuenta:</b></td>
                <td><asp:Label ID="lblAccount" runat="server" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Valor Compra:</b></td>
                <td><asp:Label ID="lblAmount" runat="server" Text="Label" Font-Bold="True" Font-Size="14pt" ForeColor="Red"></asp:Label></td>
            </tr>                        
        </table>
        
        <br />
        
        <table>
            <tr>
                <td><asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" Font-Size="Small">Cancelar</asp:LinkButton></td>
                <td style="width: 400px; text-align:right;"><asp:Button ID="btnApplyMovementPopup" runat="server" Text="Aceptar" CssClass="st-button" /></td>
            </tr>
        </table>        
        
    </asp:Panel>
    
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupMovement">
    </ajaxtoolkit:ModalPopupExtender>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSupplier.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

