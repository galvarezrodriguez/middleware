﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddProductsXSupplier.aspx.vb" Inherits="Suppliers_AddProductsXSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Productos X Proveedor
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/Manager.aspx">Proveedores</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkProfiles" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/ListSuppliers.aspx">Listado de Proveedores</asp:LinkButton>
    </li>
    <li>Agregar Productos X Proveedor</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Productos X Proveedor
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar productos x proveedor al sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Proveedor</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre del Proveedor:</span>	
                <asp:TextBox ID="txtSupplierName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Nombre del Proveedor." Enabled="False"></asp:TextBox>
                <asp:TextBox ID="txtSupplierID" runat="server" Visible="False"></asp:TextBox>                    
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext"><b>Datos del Producto</b></span>	
                <div class="clear"></div>
            </div>        

            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Nombre:</span>	
                            <asp:TextBox ID="txtProductName" CssClass="st-forminput" style="width:310px" 
                                runat="server" TabIndex="2" MaxLength="100" 
                                ToolTip="Nombre del Producto." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>                      
                    </td>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Comisión Global: (%)</span>	
                            <asp:TextBox ID="txtGlobalPctGain" CssClass="st-forminput" style="width:60px" 
                                runat="server" TabIndex="3" MaxLength="5"
                                ToolTip="Comisión global del producto." onkeydown = "return (event.keyCode!=13);">0,0</asp:TextBox>
                            <div class="clear"></div>
                        </div>                     
                    </td>
                </tr>
                
                <tr>
                    <td>
                        <div class="st-form-noline">	
                            <span class="st-labeltext">Tipo de Producto:</span>	                
                            <asp:DropDownList ID="ddlProductsType" class="uniform" runat="server" 
                                DataSourceID="dsProductType" DataTextField="nombre" 
                                DataValueField="id" ToolTip="Seleccione el Tipo de Producto." TabIndex="4">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsProductType" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                                SelectCommand="SELECT -1 AS id, '   ' AS nombre UNION ALL SELECT * FROM TipoProducto">
                            </asp:SqlDataSource>  
                            <br />
                            <div class="clear"></div>
                        </div>                 
                    </td>
                    <td>
                        <div class="st-form-noline">	
                            <span class="st-labeltext">Código EAN:</span>	
                            <asp:TextBox ID="txtEANCode" CssClass="st-forminput" style="width:210px" 
                                runat="server" TabIndex="5" MaxLength="100" 
                                ToolTip="Código EAN del producto." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                            <div class="clear"></div>
                        </div>                
                    </td>            
                </tr>
            </table>
             
            <div class="button-box">
                <asp:Button ID="btnAddProduct" runat="server" Text="Adicionar Producto" 
                    CssClass="st-button" TabIndex="6" OnClientClick="return validateAddProduct();" />
                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="7" Visible="false"  />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Productos creados en el proveedor</h3>
        </div>
        <div class="body">    
            <br />            
            <asp:GridView ID="grdProductsXSupplier" runat="server" AllowPaging="True" 
                AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsProductsXSupplier" ForeColor="#333333" Width="800px" 
                CssClass="mGridCenter" DataKeyNames="pro_id" 
                EmptyDataText="No existen Productos creados para este proveedor." 
                HorizontalAlign="Center">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:TemplateField HeaderText="Código" InsertVisible="False" 
                        SortExpression="pro_id">
                        <ItemTemplate>
                            <asp:Label ID="lblID1" runat="server" Text='<%# Bind("pro_id") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:Label ID="lblID2" runat="server" Text='<%# Eval("pro_id") %>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Nombre" SortExpression="pro_nombre">
                        <ItemTemplate>
                            <asp:Label ID="lblName" runat="server" Text='<%# Bind("pro_nombre") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtNameGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="50" Text='<%# Bind("pro_nombre") %>' Width="120" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>                            
                        </EditItemTemplate>           
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Estado" SortExpression="Estado">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Estado") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlStatus" runat="server" class="uniform" 
                                SelectedValue='<%# Bind("Estado_ID") %>' DataSourceID="dsStatus" 
                                DataTextField="nombre" DataValueField="id">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsStatus" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                                SelectCommand="SELECT id, nombre FROM EstadoProducto"></asp:SqlDataSource>
                        </EditItemTemplate>                        
                    </asp:TemplateField>                    
                    <asp:TemplateField HeaderText="Comisión Global" SortExpression="pro_comision_global">
                        <ItemTemplate>
                            <asp:Label ID="lblPctGain" runat="server" Text='<%# Bind("pro_comision_global") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPctGainGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="5" Text='<%# Bind("pro_comision_global") %>' onkeydown = "return (event.keyCode!=13);" Width="40"></asp:TextBox>                            
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tipo Producto" SortExpression="TipoProducto">
                        <ItemTemplate>
                            <asp:Label ID="lblProductType" runat="server" Text='<%# Bind("TipoProducto") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="ddlProductType" runat="server" class="uniform" 
                                SelectedValue='<%# Bind("TipoProducto_ID") %>' DataSourceID="dsProductType" 
                                DataTextField="nombre" DataValueField="id">
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="dsProductType" runat="server" 
                                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                                SelectCommand="SELECT id, nombre FROM TipoProducto"></asp:SqlDataSource>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Código EAN" SortExpression="pro_cod_EAN">
                        <ItemTemplate>
                            <asp:Label ID="lblEANCode" runat="server" Text='<%# Bind("pro_cod_EAN") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtEANCodeGrid" runat="server" CssClass="st-forminput-active" 
                                MaxLength="40" Text='<%# Bind("pro_cod_EAN") %>' onkeydown = "return (event.keyCode!=13);" Width="120"></asp:TextBox>                            
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>                        
                            <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                                CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                                ToolTip="Editar Producto" CommandArgument='<%# grdProductsXSupplier.Rows.Count %>' />
                                
                            <asp:ImageButton ID="imgAgreements" runat="server" CausesValidation="False" 
                                CommandName="adminAgreements" ImageUrl="~/img/icons/16x16/add_agreements.png" Text="Administrar Convenios" 
                                ToolTip="Administrar Convenios" CommandArgument='<%# grdProductsXSupplier.Rows.Count %>' />                                
                                
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:LinkButton ID="lnkButtonUpdate" runat="server" CausesValidation="True" 
                                CommandArgument="<%# grdProductsXSupplier.Rows.Count %>" CommandName="Update" 
                                Text="Actualizar"></asp:LinkButton>
                            &nbsp;<asp:LinkButton ID="lnkButtonCancel" runat="server" CausesValidation="False" 
                                CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                        </EditItemTemplate>                        
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            
	        <asp:SqlDataSource ID="dsProductsXSupplier" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                SelectCommand="SELECT P.pro_id, P.pro_nombre, EP.nombre AS Estado, EP.id AS Estado_ID, CAST(P.pro_comision_global AS NUMERIC(4,2)) AS pro_comision_global, TP.nombre AS TipoProducto, TP.id AS TipoProducto_ID, P.pro_cod_EAN FROM Producto P INNER JOIN EstadoProducto EP ON (P.pro_estado_producto_id = EP.id) INNER JOIN TipoProducto TP ON (P.pro_tipo_producto_id = TP.id) WHERE pro_proveedor_id = @pro_proveedor_id ORDER BY 1"
                InsertCommand="INSERT INTO Producto(pro_nombre, pro_estado_producto_id, pro_comision_global, pro_tipo_producto_id, pro_proveedor_id, pro_cod_EAN) VALUES(@pro_nombre, 1, @pro_comision_global, @pro_tipo_producto_id, @pro_proveedor_id, @pro_cod_EAN)"
                UpdateCommand="UPDATE Producto SET pro_nombre = @pro_nombre, pro_estado_producto_id = @Estado_ID, pro_comision_global = CAST(REPLACE(@pro_comision_global, ',', '.') AS NUMERIC(4,2)), pro_tipo_producto_id = @TipoProducto_ID, pro_cod_EAN = @pro_cod_EAN WHERE pro_id = @pro_id">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtSupplierID" Name="pro_proveedor_id" 
                        PropertyName="Text" />
                </SelectParameters>                
                <UpdateParameters>
                    <asp:Parameter Name="pro_nombre" />
                    <asp:Parameter Name="Estado_ID" />
                    <asp:Parameter Name="pro_comision_global"/>
                    <asp:Parameter Name="TipoProducto_ID" />
                    <asp:Parameter Name="pro_cod_EAN" />
                    <asp:Parameter Name="pro_id" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtProductName" Name="pro_nombre" 
                        PropertyName="Text" />                    
                    <asp:ControlParameter ControlID="txtGlobalPctGain" DbType="Decimal" Name="pro_comision_global" 
                        PropertyName="Text" />
                    <asp:ControlParameter ControlID="ddlProductsType" Name="pro_tipo_producto_id" 
                        PropertyName="SelectedValue" />                        
                    <asp:ControlParameter ControlID="txtSupplierID" Name="pro_proveedor_id" 
                        PropertyName="Text" />                        
                    <asp:ControlParameter ControlID="txtEANCode" Name="pro_cod_EAN" 
                        PropertyName="Text" />
                </InsertParameters>
            </asp:SqlDataSource> 
            
            <br />
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Suppliers/ListSuppliers.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSupplier.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

