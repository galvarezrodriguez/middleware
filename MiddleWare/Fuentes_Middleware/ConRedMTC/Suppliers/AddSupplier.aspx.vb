﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Suppliers

Partial Class Suppliers_AddSupplier
    Inherits Conred.Web.BasePage

    Public supplierObj As Supplier

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        supplierObj = New Supplier(strConnectionString)

        'Set New Data
        supplierObj.SupplierName = txtSupplierName.Text
        supplierObj.supplierDescription = txtDesc.Text
        supplierObj.IsomuxName = ddlSupplierDest.SelectedValue

        'Save Data
        If supplierObj.createSupplier() Then
            'New Supplier Created OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Proveedor Creado. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Proveedor creado correctamente."
            clearForm()
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Proveedor. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error creando proveedor."
        End If

    End Sub

    Private Sub clearForm()
        txtSupplierName.Text = ""
        txtDesc.Text = ""
        ddlSupplierDest.SelectedIndex = 0
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtSupplierName.Focus()
        End If
    End Sub

End Class
