﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Suppliers_AddProductsXSupplier
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            txtSupplierID.Text = objSessionParams.intSelectedSupplier
            txtSupplierName.Text = objSessionParams.strSelectedSupplier

            ddlProductsType.Focus()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub grdProductsXSupplier_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdProductsXSupplier.RowCommand
        Try

            Select Case e.CommandName

                Case "Update"

                    If Not validateUpdateProduct(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument))) Then
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "Complete correctamente el formulario"
                    Else
                        'Validar Acceso a Función
                        Try
                            objAccessToken.Validate(getCurrentPage(), "Update")
                        Catch ex As Exception
                            HandleErrorRedirect(ex)
                        End Try

                        Dim strData As String = "Nombre: " & CType(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtNameGrid"), TextBox).Text
                        strData &= ", Estado: " & CType(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("ddlStatus"), DropDownList).SelectedValue
                        strData &= ", Comisión: " & CType(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtPctGainGrid"), TextBox).Text
                        strData &= ", Tipo Producto: " & CType(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("ddlProductType"), DropDownList).SelectedValue
                        strData &= ", Código EAN: " & CType(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtEANCodeGrid"), TextBox).Text

                        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Producto Actualizado. Datos[ " & strData & " ]", "")

                        pnlError.Visible = False
                        pnlMsg.Visible = True
                        lblMsg.Text = "Producto Modificado"
                        dsProductsXSupplier.Update()
                        grdProductsXSupplier.SelectedIndex = -1

                    End If

                Case "adminAgreements"
                    grdProductsXSupplier.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedProduct = grdProductsXSupplier.SelectedDataKey.Values.Item("pro_id")
                    objSessionParams.strSelectedProduct = CType(grdProductsXSupplier.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblName"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddAgreementXProduct.aspx", False)

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Private Function validateUpdateProduct(ByVal grdRow As GridViewRow) As Boolean

        Dim retVal As Boolean = False

        Dim txtNameGridObj As TextBox = CType(grdRow.Cells(0).FindControl("txtNameGrid"), TextBox)
        Dim txtPctGainGridObj As TextBox = CType(grdRow.Cells(0).FindControl("txtPctGainGrid"), TextBox)
        Dim txtEANCodeGridObj As TextBox = CType(grdRow.Cells(0).FindControl("txtEANCodeGrid"), TextBox)

        If txtNameGridObj.Text.Length < 3 Then
            retVal = False
        ElseIf txtEANCodeGridObj.Text.Length < 5 Then
            retVal = False
        Else

            Dim intRegexPattern As New System.Text.RegularExpressions.Regex("^[0-9,]*$")

            If Not intRegexPattern.IsMatch(txtPctGainGridObj.Text) Then
                retVal = False
            Else
                retVal = True
            End If

        End If

        Return retVal
    End Function

    Protected Sub btnAddProduct_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProduct.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        If dsProductsXSupplier.Insert() >= 1 Then
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Producto creado en el proveedor. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = False
            pnlMsg.Visible = True
            lblMsg.Text = "Producto creado correctamente en el proveedor"
            deactivateForm()
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Creando Producto en el Proveedor. Datos[ " & getFormDataLog() & " ]", "")
            pnlMsg.Visible = False
            pnlError.Visible = True
            lblError.Text = "Error creando producto en el Proveedor"
        End If

    End Sub

    Private Sub deactivateForm()
        ddlProductsType.Enabled = False
        txtProductName.Enabled = False
        txtGlobalPctGain.Enabled = False
        txtEANCode.Enabled = False
        btnAddProduct.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
    End Sub

    Private Sub activateForm()
        ddlProductsType.SelectedIndex = -1
        ddlProductsType.Enabled = True
        txtProductName.Enabled = True
        txtProductName.Text = ""
        txtGlobalPctGain.Enabled = True
        txtGlobalPctGain.Text = ""
        txtEANCode.Enabled = True
        txtEANCode.Text = ""
        btnAddProduct.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        activateForm()
        ddlProductsType.Focus()
    End Sub

    Protected Sub dsProductsXSupplier_Updating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.SqlDataSourceCommandEventArgs) Handles dsProductsXSupplier.Updating
        If pnlError.Visible = True Then
            e.Cancel = True
        End If
    End Sub


    Protected Sub grdProductsXSupplier_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdProductsXSupplier.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            If e.Row.RowState <> 5 And e.Row.RowState <> 4 Then
                Dim agreements As ImageButton = CType(e.Row.Cells(6).Controls(3), ImageButton)
                Dim productID As Label = CType(e.Row.FindControl("lblID1"), Label)

                If productID.Text = "200" Then    'Only Bill Payment Product
                    CType(e.Row.Cells(6).Controls(3), ImageButton).Visible = True
                Else
                    CType(e.Row.Cells(6).Controls(3), ImageButton).Visible = False
                End If

            End If
        End If
    End Sub
End Class
