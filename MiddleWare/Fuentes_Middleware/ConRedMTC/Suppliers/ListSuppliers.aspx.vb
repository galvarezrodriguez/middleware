﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Suppliers_ListSuppliers
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        End If
    End Sub

    Protected Sub grdSuppliers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdSuppliers.RowCommand
        Try

            Select e.CommandName

                Case "Update"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    Dim strData As String

                    strData = "Nombre: " & CType(grdSuppliers.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtSupplierName"), TextBox).Text
                    strData &= ", Descripción: " & CType(grdSuppliers.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtSupplierDesc"), TextBox).Text
                    strData &= ", Nombre Isomux: " & CType(grdSuppliers.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("ddlSupplierDest"), DropDownList).SelectedValue

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Proveedor Actualizado. Datos[ " & strData & " ]", "")

                    pnlMsg.Visible = True
                    lblMsg.Text = "Proveedor Modificado."

                    dsListSuppliers.Update()

                Case "AddProducts"
                    grdSuppliers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedSupplier = grdSuppliers.SelectedDataKey.Values.Item("prov_id")
                    objSessionParams.strSelectedSupplier = CType(grdSuppliers.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblSuppierName"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("AddProductsXSupplier.aspx", False)

                Case "PurchaseMovement"
                    grdSuppliers.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.intSelectedSupplier = grdSuppliers.SelectedDataKey.Values.Item("prov_id")
                    objSessionParams.strSelectedSupplier = CType(grdSuppliers.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblSuppierName"), Label).Text

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("PurchaseMovementXSupplier.aspx", False)

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub grdSuppliers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSuppliers.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            If e.Row.RowState <> 5 And e.Row.RowState <> 4 Then
                Dim edit As ImageButton = CType(e.Row.Cells(4).Controls(1), ImageButton)
                Dim products As ImageButton = CType(e.Row.Cells(4).Controls(3), ImageButton)
                Dim addNo As ImageButton = CType(e.Row.Cells(4).Controls(5), ImageButton)

                CType(e.Row.Cells(4).Controls(1), ImageButton).Visible = objAccessToken.Peek(getCurrentPage(), "Update")
                CType(e.Row.Cells(4).Controls(3), ImageButton).Visible = objAccessToken.Peek("Suppliers/AddProductsXSupplier.aspx", "Save")
                CType(e.Row.Cells(4).Controls(5), ImageButton).Visible = objAccessToken.Peek("Suppliers/PurchaseMovementXSupplier.aspx", "Save")
            End If

        End If
    End Sub
End Class
