﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AddSupplier.aspx.vb" Inherits="Suppliers_AddSupplier" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Agregar Proveedor
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
    
    <script src="../js/md5-min.js" type="text/javascript"></script>

    <script src="../js/sha1-min.js" type="text/javascript"></script>    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Suppliers/Manager.aspx">Proveedores</asp:LinkButton>
    </li>
    <li>Agregar Proveedor</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Agregar Proveedores
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite agregar proveedores al sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Proveedor</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre Proveedor:</span>	
                <asp:TextBox ID="txtSupplierName" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Digite el nombre del Proveedor." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Descripción:</span>	
                <asp:TextBox ID="txtDesc" CssClass="st-forminput" style="width:510px" 
                    runat="server" ToolTip="Digite descripción del Proveedor." 
                    TabIndex="2" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>        
            
            <div class="st-form-line">	
                <span class="st-labeltext">Nombre ISOMux:</span>	                
                <asp:DropDownList ID="ddlSupplierDest" class="uniform" runat="server" 
                    DataSourceID="dsSupplierDest" DataTextField="dato" 
                    DataValueField="dato" ToolTip="Seleccione el destino para el Proveedor." TabIndex="3">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsSupplierDest" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="SELECT ' ' AS dato UNION ALL SELECT dato FROM ConfiguracionFormateador WHERE nombre_parametro = 'isomuxName'"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>            
            
            <div class="button-box">
                <asp:Button ID="btnSave" runat="server" Text="Crear Proveedor" 
                    CssClass="st-button" TabIndex="4" onclientclick="return validateAddSupplier()" />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Suppliers/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorSupplier.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

