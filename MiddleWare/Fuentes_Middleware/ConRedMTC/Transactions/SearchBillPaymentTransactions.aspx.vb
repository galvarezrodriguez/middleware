﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Transactions_SearchBillPaymentTransactions
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtPaymentRef.Focus()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strFilter As String = ""

        grdSearchTransactions.Visible = True

        If txtPaymentRef.Text.Length > 0 Then
            strFilter = txtPaymentRef.Text.Trim()
        ElseIf txtAuthIDP38.Text.Length > 0 Then
            strFilter = txtAuthIDP38.Text.Trim()
        ElseIf txtBillAmount.Text.Length > 0 Then
            strFilter = txtBillAmount.Text.Trim()
        ElseIf txtSTAN.Text.Length > 0 Then
            strFilter = txtSTAN.Text.Trim()
        End If

        dsSearchTransactions.SelectParameters("filterTxt").DefaultValue = strFilter
        dsSearchTransactions.DataBind()
    End Sub
End Class
