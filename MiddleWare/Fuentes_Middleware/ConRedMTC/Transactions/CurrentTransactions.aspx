﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="CurrentTransactions.aspx.vb" Inherits="Transactions_CurrentTransactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Vista de Transacciones Actuales
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Transactions/Manager.aspx">Transacciones</asp:LinkButton>
    </li>
    <li>Vista de Transacciones</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Vista de Transacciones
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite ver el listado de transacciones actuales del sistema(10 Minutos Atrás):</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

        <asp:ScriptManager ID="scrManager" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="updPanelTrx" runat="server">
        <ContentTemplate>

            <asp:LinkButton ID="lnkRefresh" runat="server"><asp:Image ID="imgRefresh" runat="server" ImageUrl="../img/icons/16x16/refresh.png" />&nbsp;Actualizar Vista</asp:LinkButton>
        
            <asp:GridView ID="grdCurrentTransactions" runat="server" 
                AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                DataSourceID="dsCurrentTransactions" ForeColor="#333333" 
                CssClass="mGridCenter" 
                EmptyDataText="No existen transacciones actuales." 
                HorizontalAlign="Center" PageSize="25">
                <RowStyle BackColor="White" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="tra_fecha_inicial" HeaderText="Fecha" 
                        SortExpression="tra_fecha_inicial" ReadOnly="True" >
                    <ItemStyle Width="140px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_tipo_mensaje_req" HeaderText="Tipo Msg. Req." 
                        SortExpression="tra_tipo_mensaje_req">
                    <ItemStyle Width="40px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_tipo_mensaje_resp" HeaderText="Tipo Msg. Resp." 
                        SortExpression="tra_tipo_mensaje_resp">
                    <ItemStyle Width="40px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_codigo_proceso" HeaderText="Cód. Proc." 
                        SortExpression="tra_codigo_proceso">
                    <ItemStyle Width="50px" />
                    </asp:BoundField>                    
                    <asp:BoundField DataField="tra_codigo_respuesta" HeaderText="Cód. Rta." 
                        SortExpression="tra_codigo_respuesta">
                    <ItemStyle Width="30px" />
                    </asp:BoundField>                    
                    <asp:BoundField DataField="tra_comercio_id" 
                        HeaderText="Comercio" SortExpression="tra_comercio_id" >
                    <ItemStyle Width="80px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_terminal_id" 
                        HeaderText="Terminal" SortExpression="tra_terminal_id" >
                    <ItemStyle Width="60px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_stan" 
                        HeaderText="STAN" SortExpression="tra_stan" ReadOnly="True" >
                    <ItemStyle Width="50px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_referencia_cliente_1" HeaderText="Ref. Cliente" 
                        SortExpression="tra_referencia_cliente_1">
                    <ItemStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_valor" HeaderText="Valor" 
                        SortExpression="tra_valor" >
                    <ItemStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_auth_id_p38" HeaderText="Auth. Id P38" 
                        SortExpression="tra_auth_id_p38" ReadOnly="True">
                    <ItemStyle Width="50px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="tra_autorizacion_ope_id" 
                        HeaderText="Auth. ID Operador" ReadOnly="True" 
                        SortExpression="tra_autorizacion_ope_id">
                    <ItemStyle Width="80px" />
                    </asp:BoundField>                        
                    <asp:BoundField DataField="tra_datos_adicionales" 
                        HeaderText="Cód. de Barras" ReadOnly="True" 
                        SortExpression="tra_datos_adicionales">
                    <ItemStyle Width="110px" />
                    </asp:BoundField>                          
                    <asp:BoundField DataField="mensaje" HeaderText="Mensaje Final" 
                        SortExpression="mensaje">
                    <ItemStyle Width="110px" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                    CssClass="pgr" Font-Underline="False" />
                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                    BorderWidth="1px" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>            
            
            <asp:SqlDataSource ID="dsCurrentTransactions" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                SelectCommand="sp_webConsultarTransaccionesVista" 
                SelectCommandType="StoredProcedure">
            </asp:SqlDataSource>
            
        </ContentTemplate>
        </asp:UpdatePanel>

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Transactions/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

