﻿Imports System.Data
Imports System.Data.SqlClient


Partial Class Transactions_getStatsData
    Inherits Conred.Web.BasePage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            getDBStats()
        End If

    End Sub

    Private Sub getDBStats()

        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim dataFooter As New ArrayList
        Dim dataBody As New ArrayList
        Dim maxPeak As Integer = 0

        Try
            'Abrir Conexion
            connection.Open()

            command.Connection = connection
            command.CommandType = CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarEstadisticasMonitor"
            command.Parameters.Clear()

            'Leer Resultado
            results = command.ExecuteReader()
            If results.HasRows Then
                While results.Read()
                    dataFooter.Add(results.GetString(0) & ":" & results.GetString(1))
                    dataBody.Add(results.GetInt32(2).ToString)

                    'Save Max. Peak
                    If Integer.Parse(results.GetInt32(2).ToString) > maxPeak Then
                        maxPeak = Integer.Parse(results.GetInt32(2).ToString)
                    End If

                End While
            End If

        Catch ex As Exception

        Finally
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try

        createHTMLData(dataFooter, dataBody, maxPeak)

    End Sub

    Private Sub createHTMLData(ByVal dataFooter As ArrayList, ByVal dataBody As ArrayList, ByVal maxPeak As Integer)

        If dataFooter.Count > 0 Then

            'Peak Counter
            Response.Write("<div style='position: absolute; top: 200px; left: 30px;'> <b>Máx. " & maxPeak.ToString & "<b/> </div>")

            'Create Output HTML for Stats
            Response.Write("<div class='chart'><table id='data' style='visibility:hidden;'><tfoot><tr>")


            For Each dataStr As String In dataFooter
                Response.Write("<th>" & dataStr & "</th>")
            Next

            Response.Write("</tr></tfoot>")
            Response.Write("<tbody><tr>")

            For Each dataStr As String In dataBody
                Response.Write("<td>" & dataStr & "</td>")
            Next

            Response.Write("</tr></tbody></table><div id='holder'></div><br /><br /></div>")
        Else
            Response.Write("<br /><br />Sin Transacciones Registradas<br /><br /><br />")
        End If

    End Sub

End Class
