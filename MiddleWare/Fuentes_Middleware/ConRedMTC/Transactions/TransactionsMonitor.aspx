﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TransactionsMonitor.aspx.vb" Inherits="Transactions_TransactionsMonitor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Monitor de Transacciones
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>

	<script type="text/javascript" src="../js/jquery.wysiwyg.js"></script>

	<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>    
    
    <!-- raphael base and raphael charts -->
	<script type="text/javascript" src="../js/raphael.js"></script>
	<script type="text/javascript" src="../js/analytics.js"></script>
	<script type="text/javascript" src="../js/popup.js"></script>    
    
    <script type="text/javascript" src="../js/fullcalendar.min.js"></script>    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Transactions/Manager.aspx">Transacciones</asp:LinkButton>
    </li>
    <li>Monitor de Transacciones</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Monitor de Transacciones
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite ver el comportamiento transaccional de las operaciones en la plataforma Conred:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">

	    <div class="titleh">
        <h3>Cantidad de Transacciones por Minuto (30 Min. Atrás)</h3>
        <div class="shortcuts-icons">        
            <a class="shortcut tips" title="Refrescar Vista" href="javascript:refreshStats();"><img src="../img/icons/shortcut/refresh.png" width="25" height="25" alt="icon" /></a>
        </div>
        </div>

        <div class="body">
        </div>                <div id="infoRefresh" style="text-align: right;">Actualiza cada 60 Segundos. [ <b>Restantes 60</b> ]</div>
    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Transactions/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

	<script type="text/javascript" src="../js/StatsMonitor.js"></script>    

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

