﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="SearchBillPaymentTransactions.aspx.vb" Inherits="Transactions_SearchBillPaymentTransactions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Búsqueda de Transacciones Pago de Facturas
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Transactions/Manager.aspx">Transacciones</asp:LinkButton>
    </li>
    <li>Búsqueda de Transacciones Pago de Facturas</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Búsqueda de Transacciones Pago de Facturas
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite buscar transacciones de Pago de Facturas del día actual:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
	    <div class="titleh">
    	    <h3>Opciones de Búsqueda</h3>
        </div>
            
        <div class="body">
        
            <table width="100%">
                <tr>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Ref. de Pago:</span>	
                            <asp:TextBox ID="txtPaymentRef" CssClass="st-forminput" style="width:200px" 
                                runat="server" TabIndex="1" MaxLength="20" 
                                ToolTip="Digite referencia de Pago." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                        </div>                    
                    </td>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Autorización ID-P38:</span>	
                            <asp:TextBox ID="txtAuthIDP38" CssClass="st-forminput" style="width:80px" 
                                runat="server" TabIndex="2" MaxLength="6" 
                                ToolTip="Digite número de autorización P38." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                        </div>                    
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">Valor Factura:</span>	
                            <asp:TextBox ID="txtBillAmount" CssClass="st-forminput" style="width:100px" 
                                runat="server" TabIndex="3" MaxLength="20" 
                                ToolTip="Digite valor de la Factura." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                        </div>                    
                    </td>
                    <td>
                        <div class="st-form-line">	
                            <span class="st-labeltext">STAN:</span>	
                            <asp:TextBox ID="txtSTAN" CssClass="st-forminput" style="width:80px" 
                                runat="server" TabIndex="4" MaxLength="6" 
                                ToolTip="Digite STAN." onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                        </div>                    
                    </td>
                </tr>
            </table>

            <div class="button-box">
                <asp:Button ID="btnSearch" runat="server" Text="Buscar Transacción" 
                    CssClass="st-button" TabIndex="5" 
                    onclientclick="return validateSearchBillPayment()" />
            </div>            
            
        </div>     
    
        <asp:GridView ID="grdSearchTransactions" runat="server" 
            AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsSearchTransactions" ForeColor="#333333" 
            CssClass="mGridCenter" 
            EmptyDataText="No existen transacciones del día con los parámetros ingresados." 
            HorizontalAlign="Center" PageSize="5" Visible="False">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="tra_fecha_inicial" HeaderText="Fecha" 
                    SortExpression="tra_fecha_inicial" ReadOnly="True" >
                <ItemStyle Width="140px" />
                </asp:BoundField>
                <asp:BoundField DataField="tra_tipo_mensaje_req" HeaderText="Tipo Msg." 
                    SortExpression="tra_tipo_mensaje_req">
                <ItemStyle Width="40px" />
                </asp:BoundField>
                <asp:BoundField DataField="tra_codigo_proceso" HeaderText="Cód. Proc." 
                    SortExpression="tra_codigo_proceso">
                <ItemStyle Width="50px" />
                </asp:BoundField>                    
                <asp:BoundField DataField="tra_codigo_respuesta" HeaderText="Cód. Rta." 
                    SortExpression="tra_codigo_respuesta">
                <ItemStyle Width="30px" />
                </asp:BoundField>                    
                <asp:BoundField DataField="tra_comercio_id" 
                    HeaderText="Comercio" SortExpression="tra_comercio_id" >
                </asp:BoundField>
                <asp:BoundField DataField="tra_terminal_id" 
                    HeaderText="Terminal" SortExpression="tra_terminal_id" >
                </asp:BoundField>
                <asp:BoundField DataField="tra_stan" 
                    HeaderText="STAN" SortExpression="tra_stan" ReadOnly="True" >
                </asp:BoundField>
                <asp:BoundField DataField="tra_referencia_cliente_1" HeaderText="Ref. Cliente" 
                    SortExpression="tra_referencia_cliente_1" />
                <asp:BoundField DataField="tra_valor" HeaderText="Valor" 
                    SortExpression="tra_valor" >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="tra_auth_id_p38" HeaderText="Auth. Id P38" 
                    SortExpression="tra_auth_id_p38" ReadOnly="True">                
                <ItemStyle Width="50px" />
                </asp:BoundField>                     
                <asp:BoundField DataField="tra_datos_adicionales" 
                    HeaderText="Código de Barras" ReadOnly="True" 
                    SortExpression="tra_datos_adicionales" />
                <asp:BoundField DataField="mensaje" HeaderText="Mensaje Final" 
                    SortExpression="mensaje" />
                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                BorderWidth="1px" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>            
        
        <asp:SqlDataSource ID="dsSearchTransactions" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
            SelectCommand="sp_webBuscarTransaccionesPagoFacturasVista" 
            SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter DefaultValue="-1" Name="filterTxt" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource> 

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Transactions/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <!-- Validator -->
    <script src="../js/ValidatorSearchTrx.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

