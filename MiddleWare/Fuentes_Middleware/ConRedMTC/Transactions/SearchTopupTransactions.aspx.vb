﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Transactions_SearchTopupTransactions
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtMobileNumber.Focus()
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strFilter As String = ""

        grdSearchTransactions.Visible = True

        If txtMobileNumber.Text.Length > 0 Then
            strFilter = txtMobileNumber.Text.Trim()
        ElseIf txtAuthIDP38.Text.Length > 0 Then
            strFilter = txtAuthIDP38.Text.Trim()
        ElseIf txtAuthIDOpe.Text.Length > 0 Then
            strFilter = txtAuthIDOpe.Text.Trim()
        ElseIf txtSTAN.Text.Length > 0 Then
            strFilter = txtSTAN.Text.Trim()
        End If

        dsSearchTransactions.SelectParameters("filterTxt").DefaultValue = strFilter
        dsSearchTransactions.DataBind()
    End Sub
End Class
