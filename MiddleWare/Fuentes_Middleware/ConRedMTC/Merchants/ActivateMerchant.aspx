﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ActivateMerchant.aspx.vb" Inherits="Merchants_ActivateMerchant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Activar Comercio
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>  
	
	<script type="text/javascript" src="../js/jquery.wysiwyg.js"></script>
	
	<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>
	
    <script type="text/javascript" src="../js/fullcalendar.min.js"></script>

	<script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>   
		   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/Manager.aspx">Comercios</asp:LinkButton>
    </li>
    <li>Activar Comercio</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Activar Comercios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite activar comercios en el sistema administrador web:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Comercio</h3>
        </div>
        <div class="body">

            <div class="st-form-line">	
                <span class="st-labeltext">Comercios JNTS:</span>	

                <asp:DropDownList ID="ddlMerchants" class="uniform" runat="server" 
                    DataSourceID="dsMerchants" DataTextField="Descripcion" 
                    DataValueField="IDCOMERCIO" 
                    ToolTip="Seleccione el tipo de negocio." TabIndex="3" AutoPostBack="True">
                </asp:DropDownList>
                
                <asp:TextBox ID="txtFilter" CssClass="st-forminput" TabIndex="1" style="width:80px" runat="server"></asp:TextBox> 
                
                <asp:Button ID="btnFilter" runat="server" Text="Consultar" CssClass="st-button" TabIndex="2"/>
                
                <asp:SqlDataSource ID="dsMerchants" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="sp_webConsultarComerciosParaActivar" 
                    SelectCommandType="StoredProcedure"></asp:SqlDataSource>

                <br /><br />
                <asp:GridView ID="grdTerminals" runat="server" 
                    AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                    DataSourceID="dsTerminals" ForeColor="#333333" 
                    CssClass="mGridCenter" 
                    EmptyDataText="No existen terminales asociadas a este Comercio." 
                    HorizontalAlign="Center" PageSize="5" Visible="False" Width="500px">
                    <RowStyle BackColor="White" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="TERMINAL_ID" HeaderText="Terminal ID" 
                            SortExpression="TERMINAL_ID" >
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Activo?" SortExpression="ACTIVO">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActive" Enabled=false Checked=<%# Bind("ACTIVO") %> runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción" 
                            SortExpression="DESCRIPCION">
                        </asp:BoundField>                    
                        
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                        CssClass="pgr" Font-Underline="False" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                        BorderWidth="1px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView> 
                    
                <asp:SqlDataSource ID="dsTerminals" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="sp_webConsultarTerminalesXComercio" 
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlMerchants" DefaultValue="-1" 
                            Name="filterMerchant" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Tipo Negocio:</span>	
                <asp:DropDownList ID="ddlEntityType" class="uniform" runat="server" 
                    DataSourceID="dsEntityType" DataTextField="nombre" 
                    DataValueField="id" 
                    ToolTip="Seleccione el tipo de negocio." TabIndex="4">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsEntityType" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS nombre
                        UNION ALL
                        SELECT * FROM TipoNivelEntidad WHERE id NOT IN (1)"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>   
               
            <div class="st-form-line">
                <asp:CheckBox ID="chkAdminLocalBalance" runat="server" 
                    Text="Administrar Cupo Localmente?" AutoPostBack="True" />
            </div>
            
            <asp:Panel ID="pnlPctGain" runat="server" Visible="False">
                <div class="st-form-line">	
                    <span class="st-labeltext">Comisión Prepago:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; %</span>	
                    <asp:TextBox ID="txtPctGain" CssClass="st-forminput" style="width:60px" 
                        runat="server" TabIndex="5" MaxLength="5" 
                        ToolTip="Digite la comisión del Comercio." 
                        onkeydown = "return (event.keyCode!=13);">0,0</asp:TextBox>                               
                    <div class="clear"></div>
                </div>
            </asp:Panel>
            
            <div class="button-box">
                <asp:Button ID="btnActivate" runat="server" Text="Activar Comercio" 
                    CssClass="st-button" TabIndex="6" 
                    onclientclick="return validateActivateMerchant()" />

                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="7" Visible="false"/>
                    
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Merchants/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorMerchant.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

