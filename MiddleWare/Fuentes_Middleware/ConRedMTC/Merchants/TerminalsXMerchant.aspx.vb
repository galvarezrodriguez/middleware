﻿Imports System.Data
Imports System.Data.SqlClient

Partial Class Merchants_TerminalsXMerchant
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If objSessionParams.strMerchantID <> "" Then

                'Set Data
                txtMerchantID.Text = objSessionParams.strMerchantID

                btnRefreshTerminals.Visible = objAccessToken.Peek(getCurrentPage(), "Update")

            End If
        End If
    End Sub

    Protected Sub grdTerminalsXMerchant_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdTerminalsXMerchant.RowCommand
        Try

            Select Case e.CommandName

                Case "Update"
                    'Validar Acceso a Función
                    Try
                        objAccessToken.Validate(getCurrentPage(), "Update")
                    Catch ex As Exception
                        HandleErrorRedirect(ex)
                    End Try

                    Dim strData As String = "Terminal ID: " & CType(grdTerminalsXMerchant.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("lblTerminalID2"), Label).Text
                    strData &= ", Activo: " & CType(grdTerminalsXMerchant.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("chkActiveUpd"), CheckBox).Checked
                    strData &= ", Descripción: " & CType(grdTerminalsXMerchant.Rows(Convert.ToInt32(e.CommandArgument)).Cells(0).FindControl("txtDescription"), TextBox).Text

                    objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Terminal Actualizada. Datos[ " & strData & " ]", "")

                    pnlError.Visible = False
                    pnlMsg.Visible = True
                    lblMsg.Text = "Terminal Actualizada"
                    dsTerminalsXMerchant.Update()
                    grdTerminalsXMerchant.SelectedIndex = -1

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try
    End Sub

    Protected Sub btnRefreshTerminals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefreshTerminals.Click
        lblMerchantID.Text = txtMerchantID.Text
        modalPopupExt.Show()
        grdTerminals.DataBind()
        dsTerminals.DataBind()
    End Sub

    Protected Sub btnUpdateTerminalsPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateTerminalsPopup.Click
        'sp_webAgregarTerminalesXComercioJNTS

        Dim conn As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim resInsert As Integer = 0

        Try

            'Conectarse
            conn.Open()

            'Armar el comando
            command.Connection = conn
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webAgregarTerminalesXComercioJNTS"

            command.Parameters.Add(New SqlParameter("@merchantID", txtMerchantID.Text.Trim()))

            'Recuperar los resultados
            results = command.ExecuteReader()

            While results.Read()
                resInsert = results.GetInt32(0)
            End While

        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            'Cerrar conexion por Default
            If Not (conn Is Nothing) Then
                conn.Close()
            End If
        End Try

        If resInsert > 0 Then
            pnlError.Visible = False
            pnlMsg.Visible = True
            lblMsg.Text = "Terminales Agregadas Correctamente"

            dsTerminalsXMerchant.DataBind()
            grdTerminalsXMerchant.DataBind()

        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error Agregando Terminales"
        End If

    End Sub

    Protected Sub grdSuppliers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTerminalsXMerchant.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            If e.Row.RowState <> 5 And e.Row.RowState <> 4 Then
                Dim edit As ImageButton = CType(e.Row.Cells(3).Controls(1), ImageButton)

                CType(e.Row.Cells(3).Controls(1), ImageButton).Visible = objAccessToken.Peek(getCurrentPage(), "Update")
            End If

        End If
    End Sub

End Class
