﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Merchants

Partial Class Merchants_EditMerchant
    Inherits Conred.Web.BasePage

    Public objMerchant As Merchant

    Protected Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Update")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        objMerchant = New Merchant(strConnectionString)

        ''Set New Data
        objMerchant.MerchantID = txtMerchantID.Text
        objMerchant.MerchantType = ddlEntityType.SelectedValue
        objMerchant.MerchantStatus = ddlMerchantStatus.SelectedValue
        objMerchant.AdminLocalBalance = chkAdminLocalBalance.Checked
        objMerchant.MerchantPctGain = Decimal.Parse(txtPctGain.Text.Trim().Replace(".", ","))

        ''Update Data
        If objMerchant.editMerchant() Then
            'User Edited OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Comercio Editado. Datos[ " & getFormDataLog() & " ]", "")
            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Comercio editado correctamente."
        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Update", "Error Editando Comercio. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error editando comercio."
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            If objSessionParams.strMerchantID <> "" Then
                'Edit Mode
                objMerchant = New Merchant(strConnectionString)
                objMerchant.getMerchantData(objSessionParams.strMerchantID)

                'Set Data
                txtMerchantID.Text = objSessionParams.strMerchantID
                txtBalance.Text = "$ " & Format(Double.Parse(objMerchant.MerchantBalance), "###,##0.00")
                txtActivationDate.Text = objMerchant.MerchantActivationDate.ToString("dd/MM/yyyy")
                txtPctGain.Text = objMerchant.MerchantPctGain
                ddlEntityType.SelectedValue = objMerchant.MerchantType
                ddlMerchantStatus.SelectedValue = objMerchant.MerchantStatus
                chkAdminLocalBalance.Checked = objMerchant.AdminLocalBalance

                If objMerchant.AdminLocalBalance = True Then
                    pnlPctGain.Visible = True
                    txtPctGain.Focus()
                Else
                    pnlPctGain.Visible = False
                End If

            End If
        End If
    End Sub

    Protected Sub chkAdminLocalBalance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAdminLocalBalance.CheckedChanged
        If chkAdminLocalBalance.Checked Then
            pnlPctGain.Visible = True
            txtPctGain.Focus()
        Else
            pnlPctGain.Visible = False
        End If
    End Sub
End Class
