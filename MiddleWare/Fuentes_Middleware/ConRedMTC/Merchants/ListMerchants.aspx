﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ListMerchants.aspx.vb" Inherits="Merchants_ListMerchants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Listar Comercios
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/Manager.aspx">Comercios</asp:LinkButton>
    </li>
    <li>Listado de Comercios</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Listado de Comercios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite ver el listado de comercios del sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
    
		<!-- Toggle Div -->
        <div class="dialogbox">
            <p>
                Comercio ID: 
                <asp:TextBox ID="txtComercioIDFilter" runat="server" TabIndex="1" MaxLength="20" CssClass="st-forminput-active"></asp:TextBox>
                <asp:Button ID="btnFilter" runat="server" Text="Filtrar" CssClass="st-button" TabIndex="2" />                
            </p>
        </div>    
    
        <asp:GridView ID="grdMerchants" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" 
            DataSourceID="dsListMerchants" ForeColor="#333333" Width="100%" 
            CssClass="mGrid" DataKeyNames="ent_comercio_JNTS, ent_tipo_nivel_entidad_id, ent_maneja_saldo" 
            EmptyDataText="No existen Comercios Activados." PageSize="50">
            <RowStyle BackColor="White" ForeColor="White" />
            <Columns>
                <asp:BoundField DataField="ent_comercio_JNTS" HeaderText="Código Comercio" 
                    SortExpression="ent_comercio_JNTS" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="tipo" HeaderText="Tipo de Comercio" 
                    SortExpression="tipo" />
                <asp:BoundField DataField="ent_maneja_saldo" HeaderText="Maneja Saldo Local?" 
                    SortExpression="ent_maneja_saldo" >                    
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>                    
                <asp:BoundField DataField="ent_comision_global" HeaderText="Comisión (%)" 
                    SortExpression="ent_comision_global" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="ent_saldo" HeaderText="Saldo Actual" 
                    SortExpression="ent_saldo" DataFormatString="{0:C2}" />
                <asp:BoundField DataField="fecha_activacion" HeaderText="Fecha Activación Conred" 
                    SortExpression="fecha_activacion" >
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField DataField="estado" HeaderText="Estado" 
                    SortExpression="estado" />
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                            CommandName="EditMerchant" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                            ToolTip="Editar Comercio" CommandArgument='<%# grdMerchants.Rows.Count %>' />
                        <asp:ImageButton ID="imgAccountMovement" runat="server" CausesValidation="False" 
                            CommandName="AccountMovementMerchant" ImageUrl="~/img/icons/16x16/money.png" Text="Movimiento de Saldo para el Comercio" 
                            ToolTip="Movimiento de Saldo para el Comercio" CommandArgument='<%# grdMerchants.Rows.Count %>' />
                        <asp:ImageButton ID="imgTerminals" runat="server" CausesValidation="False" 
                            CommandName="TerminalsXMerchant" ImageUrl="~/img/icons/16x16/pos.png" Text="Terminales Asignadas en el Comercio" 
                            ToolTip="Terminales Asignadas en el Comercio" CommandArgument='<%# grdMerchants.Rows.Count %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                
            </Columns>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                CssClass="pgr" Font-Underline="False" />
            <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#999999" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    
	    <asp:SqlDataSource ID="dsListMerchants" runat="server" 
            ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
            SelectCommand="sp_webConsultarComercios" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:Parameter Name="filterMerchant" Type="String" DefaultValue="-1" />
            </SelectParameters>
        </asp:SqlDataSource>

        <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
            <div class="albox succesbox">
                <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                <a href="#" class="close tips" title="Cerrar">Cerrar</a>
            </div>
        </asp:Panel> 

    </div>
   
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Merchants/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>
    
    <asp:Panel ID="pnlPopupMsg" runat="server" CssClass="modalPopup">
        <asp:Label ID="lblTitulo" runat="server" 
            Text="Error:" Font-Bold="True" 
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <asp:Label ID="lblPopupMsg" runat="server" Text="Este comercio no permite movimientos de saldo debido a su Configuración. (Maneja Saldo Local?)"></asp:Label>
        <br />
        <br />
        <asp:Button ID="btnAceptMsg" runat="server" Text="Aceptar" CssClass="st-button" />
        <br />

    </asp:Panel>
    
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnAceptMsg" TargetControlID="btnPopUp" PopupControlID="pnlPopupMsg">
    </ajaxtoolkit:ModalPopupExtender>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

