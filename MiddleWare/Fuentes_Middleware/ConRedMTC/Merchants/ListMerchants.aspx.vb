﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Merchants

Partial Class Merchants_ListMerchants
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtComercioIDFilter.Text = ""
            txtComercioIDFilter.Focus()
        End If
    End Sub

    Protected Sub grdMerchants_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles grdMerchants.RowCommand
        Try

            Select Case e.CommandName

                Case "EditMerchant"
                    grdMerchants.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.strMerchantID = grdMerchants.SelectedDataKey.Values.Item("ent_comercio_JNTS")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("EditMerchant.aspx", False)

                Case "AccountMovementMerchant"

                    Dim adminLocalBalance As String

                    grdMerchants.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    adminLocalBalance = grdMerchants.SelectedDataKey.Values.Item("ent_maneja_saldo")

                    If adminLocalBalance.ToLower() = "sí" Then
                        objSessionParams.strMerchantID = grdMerchants.SelectedDataKey.Values.Item("ent_comercio_JNTS")

                        'Set Data into Session
                        Session("SessionParameters") = objSessionParams

                        Response.Redirect("AccountMovementMerchant.aspx", False)
                    Else
                        'Mov. de Saldo NO Permitido
                        grdMerchants.SelectedIndex = -1
                        modalPopupExt.Show()
                    End If

                Case "TerminalsXMerchant"
                    grdMerchants.SelectedIndex = Convert.ToInt32(e.CommandArgument)
                    objSessionParams.strMerchantID = grdMerchants.SelectedDataKey.Values.Item("ent_comercio_JNTS")

                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

                    Response.Redirect("TerminalsXMerchant.aspx", False)

                Case Else
                    objSessionParams.strMerchantID = ""
                    'Set Data into Session
                    Session("SessionParameters") = objSessionParams

            End Select

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click

        Dim strFilter As String = ""

        If txtComercioIDFilter.Text = "" Then
            strFilter = "-1"
        Else
            strFilter = txtComercioIDFilter.Text
        End If

        dsListMerchants.SelectParameters("filterMerchant").DefaultValue = strFilter
        grdMerchants.DataBind()

    End Sub

    Protected Sub grdSuppliers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdMerchants.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            If e.Row.RowState <> 5 And e.Row.RowState <> 4 Then
                CType(e.Row.Cells(7).Controls(1), ImageButton).Visible = objAccessToken.Peek("Merchants/EditMerchant.aspx", "Load")
                CType(e.Row.Cells(7).Controls(3), ImageButton).Visible = objAccessToken.Peek("Merchants/AccountMovementMerchant.aspx", "Load")
                CType(e.Row.Cells(7).Controls(5), ImageButton).Visible = objAccessToken.Peek("Merchants/TerminalsXMerchant.aspx", "Load")
            End If

        End If
    End Sub

End Class
