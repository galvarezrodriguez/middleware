﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="AccountMovementMerchant.aspx.vb" Inherits="Merchants_AccountMovementMerchant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Movimiento de Saldo Comercio
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkMerchants" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/Manager.aspx">Comercios</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkListMerchants" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/ListMerchants.aspx">Listar Comercios</asp:LinkButton>
    </li>    
    <li>Movimiento de Saldo Comercio</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Movimiento de Saldo Comercios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite realizar un movimiento de cuenta al Comercio:</p>
    
    <br />

    <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
        <div class="albox succesbox">
            <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>   
    
    <asp:Panel ID="pnlError" runat="server" Visible="False">    
        <div class="albox errorbox">
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
            <a href="#" class="close tips" title="Cerrar">Cerrar</a>
        </div>
    </asp:Panel>
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Comercio</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Código:</span>	
                <asp:TextBox ID="txtMerchantID" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Código de Comercio" 
                    onkeydown = "return (event.keyCode!=13);" Enabled="False" Font-Size="11pt"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Saldo $:</span>	
                <asp:TextBox ID="txtCurrentBalance" CssClass="st-forminput" style="width:210px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Saldo Actual." 
                    onkeydown = "return (event.keyCode!=13);" Enabled="False" Font-Size="11pt"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>         
        
            <div class="st-form-line">	
                <span class="st-labeltext">Tipo Movimiento:</span>	
                <asp:DropDownList ID="ddlMovementType" class="uniform" runat="server" 
                    DataSourceID="dsMovementType" DataTextField="nombre" 
                    DataValueField="id" 
                    ToolTip="Seleccione el tipo de movimiento." TabIndex="2">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsMovementType" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" SelectCommand="SELECT -1 AS id, '   ' AS nombre UNION ALL SELECT * FROM TipoMovimientoCuenta WHERE id NOT IN(3)"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>        

            <div class="st-form-line">	
                <span class="st-labeltext">Cuenta:</span>	
                <asp:DropDownList ID="ddlBankAccount" class="uniform" runat="server" 
                    DataSourceID="dsAccounts" DataTextField="nombre_cuenta" 
                    DataValueField="cue_id" 
                    ToolTip="Seleccione la cuenta para el movimiento." TabIndex="2">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsAccounts" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" SelectCommand="SELECT -1 AS cue_id, '   ' AS nombre_cuenta UNION ALL SELECT CB.cue_id, B.ban_nombre + ' - ' + TC.nombre + ' - ' + CB.cue_numero AS nombre_cuenta
                    FROM CuentaBanco CB INNER JOIN TipoCuenta TC ON (TC.id = CB.cue_tipo_cuenta_id) INNER JOIN Banco B ON (B.ban_id = CB.cue_banco_id)"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>
                 
            <div class="st-form-line">	
                <span class="st-labeltext">Valor $:</span>&nbsp;<asp:TextBox 
                    ID="txtMovementAmount" CssClass="st-forminput" style="width:200px" 
                    runat="server" TabIndex="3" MaxLength="10" 
                    ToolTip="Monto del Movimiento" 
                    onkeydown = "return (event.keyCode!=13);" 
                    onpaste="javascript:return false;" Font-Bold="True" ForeColor="Red" 
                    Font-Size="13pt"></asp:TextBox>
                <span>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblNewAmount" runat="server" Font-Bold="True" Font-Size="13pt" 
                        ForeColor="Red" Text=""></asp:Label>
                </span>
                <div class="clear"></div>
            </div> 
            
            <div class="st-form-line">	
                <span class="st-labeltext">Observación:</span>	
                <asp:TextBox ID="txtDescription" CssClass="st-forminput" style="width:510px" 
                    runat="server" ToolTip="Descripción del movimiento." 
                    TabIndex="4" onkeydown = "return (event.keyCode!=13);" MaxLength="250"></asp:TextBox>                
                <div class="clear"></div>
            </div>           
            
            <div class="button-box">
                <asp:Button ID="btnApplyMovement" runat="server" Text="Aplicar" 
                    CssClass="st-button" TabIndex="5" 
                    onclientclick="return validateApplyMovement()" />
                <asp:Button ID="btnFinalize" runat="server" Text="Finalizar" 
                    CssClass="st-button" TabIndex="6" Visible="false" />
            </div>
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Merchants/ListMerchants.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
    <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
    </ajaxtoolkit:ToolkitScriptManager>
    
    <asp:Panel ID="pnlPopupMovement" runat="server" CssClass="modalPopupMovement">
        <asp:Label ID="lblTitulo" runat="server" 
            Text="Confirma que desea realizar el siguiente movimiento?" Font-Bold="True" 
            Font-Size="Medium"></asp:Label>
        <br />
        <br />
        <table class="mGridCenter">
            <tr>
                <td><b>Comercio:</b></td>
                <td><asp:Label ID="lblMerchantID" runat="server" Text="Label" Font-Bold="True" Font-Size="12pt"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Saldo Actual:</b></td>
                <td><asp:Label ID="lblCurrentAmount" runat="server" Text="Label" Font-Bold="True" Font-Size="12pt"></asp:Label></td>
            </tr>            
            <tr>
                <td><b>Tipo Movimiento:</b></td>
                <td><asp:Label ID="lblMovement" runat="server" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Cuenta:</b></td>
                <td><asp:Label ID="lblAccount" runat="server" Text="Label"></asp:Label></td>
            </tr>
            <tr>
                <td><b>Valor:</b></td>
                <td><asp:Label ID="lblAmount" runat="server" Text="Label" Font-Bold="True" Font-Size="14pt" ForeColor="Red"></asp:Label></td>
            </tr>                        
        </table>
        
        <br />
        
        <table>
            <tr>
                <td><asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" Font-Size="Small">Cancelar</asp:LinkButton></td>
                <td style="width: 400px; text-align:right;"><asp:Button ID="btnApplyMovementPopup" runat="server" Text="Aceptar" CssClass="st-button" /></td>
            </tr>
        </table>        
        
    </asp:Panel>
    
    <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
    
    <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupMovement">
    </ajaxtoolkit:ModalPopupExtender>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorMerchant.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

