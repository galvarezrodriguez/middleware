﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Merchants

Partial Class Merchants_AccountMovementMerchant
    Inherits Conred.Web.BasePage

    Public objMerchant As Merchant

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If objSessionParams.strMerchantID <> "" Then
                'Set Data
                txtMerchantID.Text = objSessionParams.strMerchantID

                objMerchant = New Merchant(strConnectionString)
                'Get Balance
                objMerchant.MerchantID = txtMerchantID.Text

                If objMerchant.getBalanceMerchant() Then
                    txtCurrentBalance.Text = Format(Double.Parse(objMerchant.MerchantBalance), "###,##0.00")
                Else
                    txtCurrentBalance.Text = "Error"
                End If

            End If
        Else
            lblNewAmount.Text = ""
            pnlMsg.Visible = False
            pnlError.Visible = False
        End If
    End Sub

    Protected Sub btnApplyMovement_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApplyMovement.Click

        lblMerchantID.Text = txtMerchantID.Text
        lblCurrentAmount.Text = "$ " & txtCurrentBalance.Text
        lblMovement.Text = ddlMovementType.SelectedItem.Text
        lblAccount.Text = ddlBankAccount.SelectedItem.Text
        lblAmount.Text = "$ " & Format(Double.Parse(txtMovementAmount.Text.Trim()), "###,##0.00")

        modalPopupExt.Show()
    End Sub

    Protected Sub btnApplyMovementPopup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApplyMovementPopup.Click

        Dim LogID As Long = 0

        'Validar Acceso a Función
        Try
            LogID = objAccessToken.ValidateLogID(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        objMerchant = New Merchant(strConnectionString)

        'Set New Data
        objMerchant.MerchantID = txtMerchantID.Text

        'Validate Negative Movements
        If ddlMovementType.SelectedValue <> 2 And ddlMovementType.SelectedValue <> 4 Then
            doMovement(LogID)
        Else
            If objMerchant.validateNegativeMovement(Decimal.Parse(txtMovementAmount.Text.Trim()), ddlMovementType.SelectedValue) Then
                doMovement(LogID)
            Else
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error aplicando movimiento de saldo al comercio. El comercio No presenta saldo suficiente para realizar el proceso."
            End If
        End If

    End Sub

    Private Sub doMovement(ByVal LogID As Long)
        Dim NewBalance As Decimal

        'Save Movement
        If objMerchant.applyMovementMerchant(ddlMovementType.SelectedValue, Decimal.Parse(txtMovementAmount.Text.Trim()), _
                    LogID, ddlBankAccount.SelectedValue, txtDescription.Text.Trim(), NewBalance) Then
            'Movement OK
            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Movimiento de Saldo Aplicado correctamente."
            lblNewAmount.Text = "Nuevo Saldo: $ " & Format(Double.Parse(NewBalance), "###,##0.00")
            deactivateForm()
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error aplicando movimiento de saldo al comercio."
        End If
    End Sub

    Private Sub deactivateForm()
        ddlMovementType.Enabled = False
        ddlBankAccount.Enabled = False
        txtMovementAmount.Enabled = False
        txtDescription.Enabled = False
        btnApplyMovement.Enabled = False
        btnFinalize.Visible = True
        btnFinalize.Enabled = True
        txtMovementAmount.ForeColor = Drawing.Color.Black
        txtMovementAmount.Font.Bold = False
    End Sub

    Private Sub activateForm()

        ddlMovementType.Enabled = True
        ddlMovementType.SelectedIndex = -1
        ddlBankAccount.Enabled = True
        ddlBankAccount.SelectedIndex = -1
        txtMovementAmount.Enabled = True
        txtMovementAmount.Text = ""
        txtDescription.Enabled = True
        txtDescription.Text = ""
        btnApplyMovement.Enabled = True
        btnApplyMovement.Enabled = True
        btnFinalize.Visible = False
        btnFinalize.Enabled = False
        txtMovementAmount.ForeColor = Drawing.Color.Red
        txtMovementAmount.Font.Bold = True
        lblNewAmount.Text = ""

        objMerchant = New Merchant(strConnectionString)
        'Get Balance
        objMerchant.MerchantID = txtMerchantID.Text

        If objMerchant.getBalanceMerchant() Then
            txtCurrentBalance.Text = Format(Double.Parse(objMerchant.MerchantBalance), "###,##0.00")
        Else
            txtCurrentBalance.Text = "Error"
        End If

    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        activateForm()
    End Sub
End Class
