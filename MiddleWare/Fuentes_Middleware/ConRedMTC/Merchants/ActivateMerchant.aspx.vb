﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users
Imports Conred.Merchants

Partial Class Merchants_ActivateMerchant
    Inherits Conred.Web.BasePage

    Public objMerchant As Merchant

    Protected Sub btnActivate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActivate.Click

        'Validar Acceso a Función
        Try
            objAccessToken.Validate(getCurrentPage(), "Save")
        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

        If grdTerminals.Visible = False Or grdTerminals.Rows.Count <= 0 Then
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "No existen terminales asociadas a este comercio."
            Exit Sub
        End If

        'Validate Percentage Gain Value
        If pnlPctGain.Visible = False Then
            objMerchant = New Merchant(strConnectionString, ddlMerchants.SelectedValue, ddlEntityType.SelectedValue, chkAdminLocalBalance.Checked, 0)
        Else
            objMerchant = New Merchant(strConnectionString, ddlMerchants.SelectedValue, ddlEntityType.SelectedValue, chkAdminLocalBalance.Checked, Decimal.Parse(txtPctGain.Text.Trim().Replace(".", ",")))
        End If

        'Save Data
        If objMerchant.activate() Then
            'New Merchant Activates OK
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Comercio Activado. Datos[ " & getFormDataLog() & " ]", "")

            pnlMsg.Visible = True
            pnlError.Visible = False
            lblMsg.Text = "Comercio creado correctamente."

            ddlMerchants.Enabled = False
            grdTerminals.Enabled = False
            txtPctGain.Enabled = False
            ddlEntityType.Enabled = False
            btnFinalize.Visible = True
            btnActivate.Enabled = False
            chkAdminLocalBalance.Enabled = False
            txtFilter.Enabled = False
            btnFilter.Enabled = False

        Else
            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Save", "Error Activando Comercio. Datos[ " & getFormDataLog() & " ]", "")
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Error creando comercio."
        End If

    End Sub

    Private Sub clearForm()
        pnlError.Visible = False
        pnlMsg.Visible = False
        pnlPctGain.Enabled = True
        pnlPctGain.Visible = False
        txtFilter.Text = ""
        txtFilter.Focus()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            clearForm()

            txtFilter.Focus()

        End If
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        clearForm()
        btnFinalize.Visible = False
        btnActivate.Enabled = True
        grdTerminals.Enabled = True
        ddlMerchants.Enabled = True
        ddlEntityType.Enabled = True
        chkAdminLocalBalance.Checked = False
        chkAdminLocalBalance.Enabled = True
        grdTerminals.Visible = False
        txtFilter.Enabled = True
        txtFilter.Text = ""
        txtFilter.Focus()
        btnFilter.Enabled = True

        ddlMerchants.DataBind()
        ddlEntityType.SelectedIndex = 0

    End Sub

    Protected Sub ddlMerchants_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMerchants.SelectedIndexChanged
        grdTerminals.Visible = True
        txtFilter.Text = ""
        pnlError.Visible = False
        pnlMsg.Visible = False
    End Sub

    Protected Sub chkAdminLocalBalance_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAdminLocalBalance.CheckedChanged
        If chkAdminLocalBalance.Checked Then
            pnlPctGain.Visible = True
            txtPctGain.Text = "0,0"
            txtPctGain.Focus()
        Else
            txtPctGain.Text = "0,0"
            pnlPctGain.Visible = False
        End If
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        If txtFilter.Text.Trim().Length = 10 Then
            If Not ddlMerchants.Items.FindByValue(txtFilter.Text.Trim()) Is Nothing Then
                ddlMerchants.SelectedValue = txtFilter.Text.Trim()
                grdTerminals.Visible = True
                pnlError.Visible = False
                pnlMsg.Visible = False
            Else
                grdTerminals.Visible = False
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Comercio no encontrado"
            End If
        Else
            grdTerminals.Visible = False
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "Código de comercio erróneo"
        End If
    End Sub
End Class
