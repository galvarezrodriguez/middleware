﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EditMerchant.aspx.vb" Inherits="Merchants_EditMerchant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Editar Comercio
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkMerchants" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/Manager.aspx">Comercios</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkMerchantList" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/ListMerchants.aspx">Listar Comercios</asp:LinkButton>
    </li>        
    <li>Editar Comercio</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Editar Comercios
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite editar los comercios del sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Comercio</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Código:</span>	
                <asp:TextBox ID="txtMerchantID" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Digite nombres y apellidos del usuario." 
                    onkeydown = "return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <div class="st-form-line">	
                <span class="st-labeltext">Saldo:</span>	
                <asp:TextBox ID="txtBalance" CssClass="st-forminput" style="width:510px" 
                    runat="server" Enabled="False" ToolTip="Login para entrada al sistema." 
                    TabIndex="2" onkeydown = "return (event.keyCode!=13);"></asp:TextBox>
                <div class="clear"></div>
            </div>        
                 
            <div class="st-form-line">	
                <span class="st-labeltext">Fecha Activación:</span>&nbsp;<asp:TextBox 
                    ID="txtActivationDate" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="3" MaxLength="20" 
                    ToolTip="Digite la nueva clave de acceso para el usuario." Enabled="False" 
                    onkeydown = "return (event.keyCode!=13);" 
                    onpaste="javascript:return false;"></asp:TextBox>
                <div class="clear"></div>
            </div> 
            
            <div class="st-form-line">	
                <span class="st-labeltext">Tipo Negocio:</span>	
                <asp:DropDownList ID="ddlEntityType" class="uniform" runat="server" 
                    DataSourceID="dsEntityType" DataTextField="nombre" 
                    DataValueField="id" 
                    ToolTip="Seleccione el tipo de negocio." TabIndex="4">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsEntityType" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" SelectCommand="SELECT -1 AS id, '       ' AS nombre
                        UNION ALL
                        SELECT * FROM TipoNivelEntidad WHERE id NOT IN (1)"></asp:SqlDataSource>
                <div class="clear"></div>
            </div>           
            
            <div class="st-form-line">
                <asp:CheckBox ID="chkAdminLocalBalance" runat="server" 
                    Text="Administrar Cupo Localmente?" AutoPostBack="True" />
            </div>
            
            <asp:Panel ID="pnlPctGain" runat="server" Visible="False">
                <div class="st-form-line">	
                    <span class="st-labeltext">Comisión Prepago:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; %</span>	
                    <asp:TextBox ID="txtPctGain" CssClass="st-forminput" style="width:60px" 
                        runat="server" TabIndex="5" MaxLength="5" 
                        ToolTip="Digite la comisión del Comercio." 
                        onkeydown = "return (event.keyCode!=13);">0,0</asp:TextBox>                               
                    <div class="clear"></div>
                </div>            
            </asp:Panel>
            
            <div class="st-form-line">	
                <span class="st-labeltext">Estado del Comercio:</span>	                
                <asp:DropDownList ID="ddlMerchantStatus" class="uniform" runat="server" 
                    DataSourceID="dsMerchantStatus" DataTextField="nombre" 
                    DataValueField="id" Width="200px" 
                    ToolTip="Seleccione el estado del comercio." TabIndex="6">
                </asp:DropDownList>
                <asp:SqlDataSource ID="dsMerchantStatus" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" SelectCommand="SELECT -1 AS id, '    ' AS nombre
                        UNION ALL
                        SELECT * from EstadoEntidad "></asp:SqlDataSource>
                <div class="clear"></div>
            </div>             
            
            <div class="button-box">
                <asp:Button ID="btnEdit" runat="server" Text="Editar Comercio" 
                    CssClass="st-button" TabIndex="7" 
                    onclientclick="return validateEditMerchant()" />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Merchants/ListMerchants.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorMerchant.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

