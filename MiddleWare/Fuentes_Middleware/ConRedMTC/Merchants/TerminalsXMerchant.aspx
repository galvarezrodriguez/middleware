﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="TerminalsXMerchant.aspx.vb" Inherits="Merchants_TerminalsXMerchant" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Terminales X Comercio
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkMerchants" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/Manager.aspx">Comercios</asp:LinkButton>
    </li>
    <li>
        <asp:LinkButton ID="lnkMerchantList" runat="server" CssClass="fixed" 
            PostBackUrl="~/Merchants/ListMerchants.aspx">Listar Comercios</asp:LinkButton>
    </li>    
    <li>Terminales X Comercio</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Terminales X Comercio
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite listar las terminales asociadas a un comercio:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Comercio</h3>
        </div>
        <div class="body">
        
            <div class="st-form-line">	
                <span class="st-labeltext">Código:</span>	
                <asp:TextBox ID="txtMerchantID" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Digite nombres y apellidos del usuario." 
                    onkeydown = "return (event.keyCode!=13);" Enabled="False"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="clear"></div>
            </div>        
        
            <!-- START SIMPLE FORM -->
            <div class="simplebox grid960">
	            <div class="titleh">
    	            <h3>Terminales asociadas a este comercio</h3>
                </div>
                <div class="body">    
                    <br />            
            
                    <asp:GridView ID="grdTerminalsXMerchant" runat="server" 
                        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                        DataSourceID="dsTerminalsXMerchant" ForeColor="#333333" 
                        CssClass="mGridCenter" 
                        EmptyDataText="No existen terminales asociadas a este Comercio." 
                        HorizontalAlign="Center" PageSize="5" Width="700px" 
                        DataKeyNames="pto_numero">
                        <RowStyle BackColor="White" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="Terminal ID" SortExpression="pto_numero">
                                <ItemTemplate>
                                    <asp:Label ID="lblTerminalID" runat="server" Text='<%# Bind("pto_numero") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Label ID="lblTerminalID2" runat="server" Text='<%# Bind("pto_numero") %>'></asp:Label>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Activa?" SortExpression="estado">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkActive" Enabled=false Checked=<%# Bind("estado") %> runat="server" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                                <EditItemTemplate>
                                    <asp:CheckBox ID="chkActiveUpd" Checked=<%# Bind("estado") %> runat="server" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Descripción" SortExpression="pto_observacion">
                                <ItemTemplate>
                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("pto_observacion") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescription" runat="server" CssClass="st-forminput-active"
                                    onkeydown = "return (event.keyCode!=13);" Width="250px" MaxLength="50"
                                    Text='<%# Bind("pto_observacion") %>'></asp:TextBox>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>                        
                                    <asp:ImageButton ID="imgEdit" runat="server" CausesValidation="False" 
                                        CommandName="Edit" ImageUrl="~/img/icons/16x16/edit.png" Text="Editar" 
                                        ToolTip="Editar Terminal" CommandArgument='<%# grdTerminalsXMerchant.Rows.Count %>' />                        
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lnkButtonUpdate" runat="server" CausesValidation="True" 
                                        CommandArgument="<%# grdTerminalsXMerchant.Rows.Count %>" CommandName="Update" 
                                        Text="Actualizar"></asp:LinkButton>
                                    &nbsp;<asp:LinkButton ID="lnkButtonCancel" runat="server" CausesValidation="False" 
                                        CommandName="Cancel" Text="Cancelar"></asp:LinkButton>
                                </EditItemTemplate>                        
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                            CssClass="pgr" Font-Underline="False" />
                        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                            BorderWidth="1px" />
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    </asp:GridView>                    
                    
                    <asp:SqlDataSource ID="dsTerminalsXMerchant" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                        SelectCommand="sp_webConsultarTerminalesXComercioConred" 
                        SelectCommandType="StoredProcedure"
                        UpdateCommand="UPDATE PuntoVenta SET pto_estado_punto_venta_id = (CASE @estado WHEN 1 THEN 1 ELSE 2 END), pto_observacion = @pto_observacion WHERE (pto_numero = @pto_numero)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="txtMerchantID" DefaultValue="-1" 
                                Name="filterMerchant" PropertyName="Text" Type="String" />
                        </SelectParameters>
                        <UpdateParameters>
                            <asp:Parameter Name="pto_numero" />
                            <asp:Parameter Name="estado" />
                            <asp:Parameter Name="pto_observacion" />                            
                        </UpdateParameters>                        
                    </asp:SqlDataSource>                    
                    <br />

                </div>
                <br />                
                <div class="padding-left10">
                    <asp:Button ID="btnRefreshTerminals" runat="server" Text="Nuevas Terminales JNTS" 
                        CssClass="st-button" TabIndex="1" />
                </div> 
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
            <ajaxtoolkit:ToolkitScriptManager ID="scrMgrAsp" runat="server">
            </ajaxtoolkit:ToolkitScriptManager>
            
            <asp:Panel ID="pnlPopupTerminals" runat="server" CssClass="modalPopupMovement">
                <asp:Label ID="lblTitulo" runat="server" 
                    Text="Nuevas Terminales Asociadas en JNTS:" Font-Bold="True" 
                    Font-Size="Medium"></asp:Label>
                <br />
                <br />
                <b>Comercio:</b>&nbsp;&nbsp;<asp:Label ID="lblMerchantID" runat="server" Text="Label"></asp:Label>
                <br />
                <br />
                <asp:GridView ID="grdTerminals" runat="server" 
                    AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
                    DataSourceID="dsTerminals" ForeColor="#333333" 
                    CssClass="mGridCenter" 
                    EmptyDataText="No existen Nuevas terminales en JNTS asociadas a este Comercio." 
                    HorizontalAlign="Center" PageSize="5" Width="450px">
                    <RowStyle BackColor="White" ForeColor="White" />
                    <Columns>
                        <asp:BoundField DataField="TERMINAL_ID" HeaderText="Terminal ID" 
                            SortExpression="TERMINAL_ID" >
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Activo?" SortExpression="ACTIVO">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkActive" Enabled=false Checked=<%# Bind("ACTIVO") %> runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción" 
                            SortExpression="DESCRIPCION">
                        </asp:BoundField>                    
                        
                    </Columns>
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
                        CssClass="pgr" Font-Underline="False" />
                    <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
                        BorderWidth="1px" />
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                </asp:GridView> 
                    
                <asp:SqlDataSource ID="dsTerminals" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                    SelectCommand="sp_webConsultarTerminalesXComercioJNTS" 
                    SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="txtMerchantID" DefaultValue="-1" 
                            Name="filterMerchant" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>                
                
                <br />
                
                <table>
                    <tr>
                        <td><asp:LinkButton ID="lnkCancel" runat="server" Font-Bold="True" Font-Size="Small">Cancelar</asp:LinkButton></td>
                        <td style="width: 400px; text-align:right;"><asp:Button ID="btnUpdateTerminalsPopup" runat="server" Text="Agregar" CssClass="st-button" /></td>
                    </tr>
                </table>        
                
            </asp:Panel>
            
            <asp:Button ID="btnPopUp" runat="server" Style="display: none" Text="Invisible Button" />
            
            <ajaxtoolkit:ModalPopupExtender ID="modalPopupExt" runat="server" BackgroundCssClass="modalBackground"
                CancelControlID="lnkCancel" TargetControlID="btnPopUp" PopupControlID="pnlPopupTerminals">
            </ajaxtoolkit:ModalPopupExtender>             
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Merchants/ListMerchants.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorMerchant.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

