﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Conciliations_GenerateFileBillPaymentMovilred
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtDate.Text = Now.ToString("MM/dd/yyyy")
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnGenerateMovilredFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateMovilredFile.Click

        Dim connection As SqlConnection
        Dim command As New SqlCommand()
        Dim results As SqlDataReader
        Dim fileConc As StreamWriter
        Dim filePath As String
        Dim fileName As String
        Dim counter As Integer
        Dim fechaTxt As String
        Dim fecTokens As String
        Dim monto As Long
        Dim strLog As String = ""

        Try

            'Validar Acceso a Función
            Try
                objAccessToken.Validate(getCurrentPage(), "Create")
            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

            connection = New SqlConnection(strConnectionString)

            'Abrir Conexion
            connection.Open()

            fechaTxt = txtDate.Text.Trim()

            'FORMATO: MM/DD/AAAA
            fecTokens = fechaTxt.Split("/")(1) & fechaTxt.Split("/")(0) & fechaTxt.Split("/")(2).Substring(2)

            filePath = ConfigurationSettings.AppSettings.Get("ConciliationsFolder") & "DETALLERECAUDO" & "CARVAJAL" & fecTokens & ".txt"
            fileName = "DETALLERECAUDO" & "CARVAJAL" & fecTokens & ".txt"

            fileConc = File.CreateText(filePath)

            'TRANSACCIONES DE MOVILRED - PAGOS DE FACTURA REGISTRADAS EN CONRED
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarTrxMovilredPagoFacturas"
            command.Parameters.Add(New SqlParameter("@fechaConsultaTxt", fechaTxt))

            'Recuperar los resultados
            results = command.ExecuteReader()

            Dim lineStr As String

            'Verificar si hay Transacciones para procesar
            counter = 0
            monto = 0

            Do While results.Read()
                lineStr = ""
                lineStr = results.GetString(0) & ";" & results.GetString(1) & ";" & results.GetString(2) & ";" & _
                          results.GetString(3) & ";" & results.GetString(4) & ";" & results.GetString(5) & ";" & _
                          results.GetString(6) & ";" & results.GetString(7) & ";" & results.GetString(8) & ";" & _
                          results.GetString(9) & ";" & results.GetString(10) & ";" & results.GetString(11) & ";" & _
                          results.GetString(12) & ";"

                fileConc.WriteLine(lineStr)

                counter += 1
                monto += Long.Parse(results.GetString(4))  'Valor del Pago

            Loop

            If counter >= 1 Then
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Archivo Generado Correctamente"

                txtDate.Enabled = False
                btnGenerateMovilredFile.Enabled = False
                txtPath.Text = fileName
                pnlDownload.Visible = True
                txtPath.Visible = True
                btnDownload.Visible = True

                strLog = "Fecha de Conciliación = " & fechaTxt & " ; Núm. de Trx = " & counter & " ; Valor Total = $" & monto
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Create", "Proceso de Generación Archivo Movilred Pago Facturas Realizado. Datos[ " & strLog & " ]", "")

            Else
                pnlDownload.Visible = False
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "No hay transacciones de Pago de Facturas Movilred para la fecha seleccionada"

                strLog = "No hay transacciones de Movilred para la fecha: " & fechaTxt
                objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Create", "Proceso de Generación Archivo Movilred Pago Facturas Realizado. Datos[ " & strLog & " ]", "")

            End If

        Catch Ex As Exception
            HandleErrorRedirect(Ex)
        Finally
            'Cerrar el fichero 
            If Not (fileConc Is Nothing) Then
                fileConc.Close()
            End If
            'Cerrar DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If
            'Cerrar conexion por Default
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        Response.Redirect("Dload.aspx?fileName=" & txtPath.Text)
    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        pnlError.Visible = False
        pnlMsg.Visible = False
        txtDate.Enabled = True
        btnGenerateMovilredFile.Enabled = True
        txtPath.Text = ""
        pnlDownload.Visible = False
    End Sub
End Class
