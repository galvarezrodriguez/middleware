﻿Imports System.IO
Imports System.Data
Imports System.Data.SqlClient

Partial Class Conciliations_LoadFileTopupMovilred
    Inherits Conred.Web.BasePage

    Partial Class dataReg
        Public tra_operator As String
        Public tra_date As String
        Public tra_time As String
        Public tra_id As String
        Public tra_auth As String
        Public tra_amount As String
        Public tra_mobile_phone As String
        Public tra_externalReference As String
        Public tra_total_time As String
        Public Sub clear()
            tra_operator = ""
            tra_date = ""
            tra_time = ""
            tra_id = ""
            tra_auth = ""
            tra_amount = ""
            tra_mobile_phone = ""
            tra_externalReference = ""
            tra_total_time = ""
        End Sub
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Protected Sub btnMovilredConciliation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMovilredConciliation.Click

        Dim resProcess As Integer
        Dim line As Integer = 0
        Dim errorMessages As String = ""
        Dim strLog As String = ""

        Try

            resProcess = processMovilredFile(txtFileName.Text, line, errorMessages)

            If resProcess = 1 Or resProcess = 2 Then
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Diferencias Encontradas. Por favor revisar los Datos y generar el Archivo."
            ElseIf resProcess = 0 Then
                pnlError.Visible = False
                pnlMsg.Visible = True
                lblMsg.Text = "Archivo Procesado. No se encontraron diferencias."
            ElseIf resProcess = 9 Then
                'Syntax Error in file
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Error Procesando Archivo de Conciliación Movilred. Por favor revisar su estructura en la línea: " & line & " :: " & errorMessages
            End If

            btnMovilredConciliation.Enabled = False
            btnFinalize.Visible = True

            If pnlDifferences.Visible = False And pnlError.Visible = True Then
                strLog = "Fecha = " & txtFileDate.Text & ". " & lblError.Text
            ElseIf pnlDifferences.Visible = True And pnlError.Visible = False Then
                strLog = "Fecha = " & txtFileDate.Text & ". Diferencias Conred->Movilred: Nro. Trx=" & lblCounter1.Text & "; Valor= $" & lblAmount1.Text _
                            & ".  ::  Diferencias Movilred->:Conred: Nro. Trx=" & lblCounter2.Text & "; Valor= $" & lblAmount2.Text
            ElseIf pnlDifferences.Visible = False And pnlMsg.Visible = True Then
                strLog = "Fecha = " & txtFileDate.Text & ". " & lblMsg.Text
            End If

            objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Create", "Proceso de Conciliación Movilred Realizado. Datos[ " & strLog & " ]", "")

        Catch ex As Exception
            HandleErrorRedirect(ex)
        End Try

    End Sub

    Function processMovilredFile(ByVal fileName As String, ByRef line As Integer, ByRef errorMessages As String) As Integer

        Dim idx As Integer
        Dim resultDiff As Integer
        Dim counter, amount As Long
        Dim counter2, amount2 As Long
        Dim tokens() As String
        Dim auxTokens() As String
        Dim fileDate As String = txtFileDate.Text
        Dim fileConc As New System.IO.StreamReader(ConfigurationSettings.AppSettings.Get("ConciliationsFolder") & fileName)

        Try

            counter = 0
            amount = 0
            counter2 = 0
            amount2 = 0
            idx = 0
            resultDiff = False
            While fileConc.EndOfStream <> True
                ReDim Preserve tokens(idx)
                ReDim Preserve auxTokens(idx)
                auxTokens(idx) = fileConc.ReadLine()
                If auxTokens(idx).Length >= 13 Then
                    tokens(idx) = auxTokens(idx)
                    idx = idx + 1
                End If
            End While

            'Close File
            fileConc.Close()

            If idx > 1 Then
                'Delete Temp Tables Before Start the Process
                DeleteDBConciTemp()

                'Insert Records into Movilred Temp Table
                If insertDBMovilredTemp(tokens, idx, line, errorMessages) Then

                    'Insert Records from Local Conred DB
                    insertDBConredTemp(fileDate)

                    'Compare Temp Tables

                    'First Step
                    If doDiffConred_Movilred(counter, amount) = False Then
                        resultDiff = 0
                    Else
                        resultDiff = 1
                    End If

                    lblCounter1.Text = Format(counter, "###,##0")
                    lblAmount1.Text = "$ " + Format(amount, "###,###,##0")

                    'Second Step
                    If doDiffMovilred_Conred(counter2, amount2) = False Then
                        resultDiff += 0
                        btnDownloadDiffFile.Enabled = False
                        btnDownloadDiffFile.Visible = False
                    Else
                        resultDiff += 1

                        'Activate Download Button
                        btnDownloadDiffFile.Enabled = True
                        btnDownloadDiffFile.Visible = True

                    End If

                    lblCounter2.Text = Format(counter2, "###,##0")
                    lblAmount2.Text = "$ " + Format(amount2, "###,###,##0")

                    If resultDiff > 0 Then
                        pnlDifferences.Visible = True
                    Else
                        pnlDifferences.Visible = False
                    End If

                Else
                    resultDiff = 9 'Wrong Syntax in Input File
                End If
            End If

            Return resultDiff

        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            'Close File
            If Not fileConc Is Nothing Then
                fileConc.Close()
            End If
            'Delete File
            Try                
                File.Delete(ConfigurationSettings.AppSettings.Get("ConciliationsFolder") & fileName)
            Catch ex As Exception
            End Try
        End Try

    End Function

    Private Function doDiffMovilred_Conred(ByRef counter As Long, ByRef amount As Long) As Boolean
        Dim valRet As Boolean
        Dim connection As SqlConnection = Nothing
        Dim command As New SqlCommand()
        Dim results As SqlDataReader = Nothing
        Dim fileInsert As StreamWriter = Nothing
        Dim fileDiffName As String = txtFileName.Text.Split(".")(0) & "_DIFF." & txtFileName.Text.Split(".")(1)
        Dim filePath = ConfigurationSettings.AppSettings.Get("ConciliationsFolder") & fileDiffName

        Try
            valRet = False

            'Open File
            fileInsert = File.CreateText(filePath)

            connection = New SqlConnection(strConnectionString)

            'Open Connection
            connection.Open()

            'Transactions in Movilred and Not In Conred
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webDiffTrxMovilredConred"

            'Get Results
            results = command.ExecuteReader()

            'Check for diff. records
            Do While results.Read()
                valRet = True   'Differences Found
                counter += 1
                amount += Long.Parse(results.GetString(6))

                'CARVAJAL;COMCEL;2012-09-25;09:09:13;0579578918;APROBADA;00;30767334;2000;3144001024;177278;153;
                'Create Diff. File for Movilred
                Dim SQL As String = "CARVAJAL" & ";" & results.GetString(1) & ";" & results.GetString(2).Replace("/", "-") _
                                     & ";" & results.GetString(3) & ";" & results.GetValue(4).ToString _
                                     & ";" & "APROBADA" & ";" & "00" & ";" & results.GetValue(5).ToString _
                                     & ";" & results.GetString(6) & ";" & results.GetString(7) _
                                     & ";" & results.GetValue(8).ToString & ";" & results.GetString(9) & ";"
                fileInsert.WriteLine(SQL)
            Loop

            If valRet = True Then
                txtDiffFileName.Text = fileDiffName
            End If

            Return valRet
        Catch ex As Exception
            Return valRet
        Finally
            'Close File Connection 
            If Not (fileInsert Is Nothing) Then
                fileInsert.Close()
            End If

            'Close DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Close DB Connection
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Function

    Private Function doDiffConred_Movilred(ByRef counter As Long, ByRef amount As Long) As Boolean
        Dim valRet As Boolean
        Dim connection As SqlConnection = Nothing
        Dim command As New SqlCommand()
        Dim results As SqlDataReader = Nothing

        Try
            valRet = False

            connection = New SqlConnection(strConnectionString)

            'Open Connection
            connection.Open()

            'Transactions in Conred and Not In Movilred
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webDiffTrxConredMovilred"

            'Get Results
            results = command.ExecuteReader()

            'Check for diff. records
            Do While results.Read()
                valRet = True   'Differences Found
                counter += 1
                amount += Long.Parse(results.GetString(5))
            Loop

            Return valRet
        Catch ex As Exception
            Return valRet
        Finally
            'Close DataReader
            If Not (results Is Nothing) Then
                results.Close()
            End If

            'Close DB Connection
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Function

    Private Sub insertDBConredTemp(ByVal fileDate As String)
        Dim connection As SqlConnection = Nothing
        Dim command As New SqlCommand()

        Try

            connection = New SqlConnection(strConnectionString)

            'Open Connection
            connection.Open()

            'Call SP
            command.Connection = connection
            command.CommandType = Data.CommandType.StoredProcedure
            command.CommandText = "sp_webConsultarTrxMovilredxDia"
            command.Parameters.Add(New SqlParameter("@concDate", Date.Parse(fileDate)))

            'Execute SP
            command.ExecuteNonQuery()
        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            'Close DB Connection
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Private Function insertDBMovilredTemp(ByRef tokens() As String, ByVal idx As Integer, ByRef line As Integer, ByRef errorMessages As String) As Boolean

        'Insert Line By Line - Bulk Method
        Dim connection As SqlConnection = Nothing
        Dim command As New SqlCommand()
        Dim SQL As String
        Dim i As Integer
        Dim oneReg As New dataReg()
        Dim flagRet As Boolean
        Dim fileInsert As StreamWriter = Nothing
        Dim filePath = ConfigurationSettings.AppSettings.Get("ConciliationsFolder") & "Movilred_Insert.csv"
        Dim fileBulkPath = ConfigurationSettings.AppSettings.Get("ConciliationsBulkFolder") & "Movilred_Insert.csv"
        Dim fileDate As String = txtFileDate.Text

        Try

            'Open File
            fileInsert = File.CreateText(filePath)

            connection = New SqlConnection(strConnectionString)

            'Open DB Connection
            connection.Open()

            'Set SQL Command
            command.Connection = connection
            flagRet = False

            For i = 0 To idx - 1
                oneReg.clear()
                line = line + 1
                If separateFields(tokens(i), oneReg) Then
                    flagRet = True
                    'Build SQL String
                    If oneReg.tra_id <> "" Then
                        If CDate(fileDate) <> CDate(oneReg.tra_date) Then
                            errorMessages = "La fecha en el nombre del archivo no coincide con la fecha de las transacciones"
                            Return False
                        Else
                            If Convert.ToInt32(oneReg.tra_auth) <> 0 Then
                                'CARVAJAL;COMCEL;2012-09-25;09:09:13;0579578918;APROBADA;00;30767334;2000;3144001024;177278;153;
                                SQL = "1" & "," & oneReg.tra_operator & "," & oneReg.tra_date & "," & oneReg.tra_time & "," & oneReg.tra_id _
                                & "," & oneReg.tra_auth & "," & oneReg.tra_amount & "," & oneReg.tra_mobile_phone & "," & oneReg.tra_externalReference _
                                & "," & oneReg.tra_total_time

                                fileInsert.WriteLine(SQL)
                            End If
                        End If
                    End If
                Else
                    errorMessages = "Error Formato de Campos"
                    flagRet = False
                    Exit For
                End If
            Next i

            'Close File
            fileInsert.Close()

            If flagRet Then
                'Call SP for SQL Bulking
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webCargarRegistrosMovilred"
                command.Parameters.Add(New SqlParameter("@path", fileBulkPath))
                command.ExecuteNonQuery()
            End If

            Return flagRet

        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            'Close DB Connection
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
            'Close File Connection 
            If Not (fileInsert Is Nothing) Then
                fileInsert.Close()
            End If
            'Delete File
            Try
                File.Delete(filePath)
            Catch ex As Exception
            End Try
        End Try

    End Function

    Private Function separateFields(ByVal dataStr As String, ByRef oneReg As dataReg) As Boolean

        Dim fields() = dataStr.Split(";")

        If fields.Length >= 13 Then
            Dim dateInFile = fields(2)
            Dim dateTokens = dateInFile.Split("-")
            Dim timeInFile = fields(3)

            If fields(5).ToString.ToUpper = "APROBADA" Then
                If IsNumeric(fields(4)) And IsNumeric(fields(7)) And IsNumeric(fields(8)) And _
                    IsNumeric(fields(9)) And IsNumeric(fields(10)) Then

                    If Not validationDate(dateInFile, timeInFile) Then
                        Return False
                    End If

                    oneReg.tra_operator = fields(1)
                    oneReg.tra_date = dateTokens(1) & "/" & dateTokens(2) & "/" & dateTokens(0)
                    oneReg.tra_time = timeInFile
                    oneReg.tra_id = fields(4)
                    oneReg.tra_auth = fields(7)
                    oneReg.tra_amount = fields(8)
                    oneReg.tra_mobile_phone = fields(9)
                    oneReg.tra_externalReference = fields(10)
                    oneReg.tra_total_time = fields(11)

                Else : Return False
                End If
            End If
            Return True
        Else : Return False
        End If

        Return False

    End Function

    Function validationDate(ByVal strDate As String, ByVal strTime As String) As Boolean
        Try
            Dim dateT As Date = CDate(strDate & " " & strTime)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub DeleteDBConciTemp()
        Dim connection As SqlConnection = Nothing
        Dim command As New SqlCommand()

        Try
            connection = New SqlConnection(strConnectionString)

            'Open Connection
            connection.Open()

            'Delete Table 1
            command.CommandText = "TRUNCATE TABLE ConcMovilredINTemp"
            command.Connection = connection
            command.ExecuteNonQuery()

            'Delete Table 2
            command.CommandText = "TRUNCATE TABLE ConcConredINTemp"
            command.ExecuteNonQuery()

        Finally
            'Close Connection
            If Not (connection Is Nothing) Then
                connection.Close()
            End If
        End Try
    End Sub

    Protected Sub btnMovilredUploadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMovilredUploadFile.Click

        Dim alertMessage = ""
        Dim fileStream As System.IO.StreamReader = Nothing
        Dim dateConc As Date = Nothing
        Dim fileName As String = ""
        Dim tokens() As String
        Dim idx As Integer = 0
        Dim flagError As Boolean = False
        Dim fields As Integer = 0
        Dim contentFileDate() As String = Nothing

        Try

            'Validar Acceso a Función
            Try
                objAccessToken.Validate(getCurrentPage(), "Create")
            Catch ex As Exception
                HandleErrorRedirect(ex)
            End Try

            If fileMovilredUpload.FileName <> "" Then

                fileName = fileMovilredUpload.FileName

                If fileMovilredUpload.FileName.ToLower().Contains("detalleventascarvajal") Then
                    If fileMovilredUpload.PostedFile.ContentType = "text/plain" Then
                        fileMovilredUpload.SaveAs(ConfigurationSettings.AppSettings.Get("ConciliationsFolder") + fileMovilredUpload.FileName)
                        fileStream = New System.IO.StreamReader(ConfigurationSettings.AppSettings.Get("ConciliationsFolder") & fileMovilredUpload.FileName)

                        While fileStream.EndOfStream <> True
                            ReDim Preserve tokens(idx)
                            tokens(idx) = fileStream.ReadLine()
                            If tokens(idx).Length > 10 Then
                                fields = tokens(idx).Split(";").Length
                                contentFileDate = tokens(idx).Split(";")
                                If dateConc = Nothing Then
                                    Try
                                        dateConc = contentFileDate(2)
                                        txtFileDate.Text = dateConc.ToShortDateString.ToString
                                        txtFileDate.Visible = True
                                        lblFileDate.Visible = True
                                        flagError = False
                                    Catch ex As Exception
                                        pnlError.Visible = True
                                        pnlMsg.Visible = False
                                        lblError.Text = "Error en el formato del archivo Movilred, la fecha en el nombre del archivo no es válida."
                                        Exit Sub
                                    End Try
                                End If
                                'Validate Length of each line in the file
                                If fields < 13 Then
                                    flagError = True
                                    Exit While
                                End If
                            End If
                            idx = idx + 1
                        End While

                        If flagError Then
                            pnlError.Visible = True
                            pnlMsg.Visible = False
                            lblError.Text = "Seleccione un archivo de conciliación correcto para Movilred"
                        Else
                            txtFileName.Text = fileMovilredUpload.FileName
                            btnMovilredConciliation.Visible = True
                            btnMovilredUploadFile.Enabled = False
                            fileMovilredUpload.Enabled = False
                            pnlError.Visible = False
                        End If
                    Else
                        pnlError.Visible = True
                        pnlMsg.Visible = False
                        lblError.Text = "Seleccione un archivo de conciliación correcto para Movilred"
                    End If
                Else
                    pnlError.Visible = True
                    pnlMsg.Visible = False
                    lblError.Text = "Seleccione un archivo de conciliación correcto para Movilred"
                End If
            Else
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "Seleccione un archivo de conciliación correcto para Movilred"
            End If
        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            'Close the file
            If Not (fileStream Is Nothing) Then
                fileStream.Close()
            End If
        End Try

    End Sub

    Protected Sub btnFinalize_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinalize.Click
        lblFileDate.Visible = False
        txtFileDate.Text = ""
        txtFileDate.Visible = False
        txtFileName.Text = ""
        txtFileName.Visible = False
        txtDiffFileName.Visible = False
        txtDiffFileName.Text = ""

        btnMovilredUploadFile.Enabled = True
        btnMovilredConciliation.Enabled = True
        btnMovilredConciliation.Visible = False

        btnDownloadDiffFile.Enabled = True
        btnDownloadDiffFile.Visible = False

        btnFinalize.Enabled = True
        btnFinalize.Visible = False

        fileMovilredUpload.Enabled = True

        pnlDifferences.Visible = False

    End Sub

    Protected Sub btnDownloadDiffFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadDiffFile.Click
        Response.Redirect("Dload.aspx?fileName=" & txtDiffFileName.Text)
    End Sub
End Class
