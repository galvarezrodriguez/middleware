﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="GenerateFileTopupMovilred.aspx.vb" Inherits="Conciliations_GenerateFileTopupMovilred" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Generación de Archivo Conciliación Recargas Movilred
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     

    <script type="text/javascript" src="../js/jquery.wysiwyg.js"></script>

	<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>

    <script type="text/javascript" src="../js/fullcalendar.min.js"></script>

	<script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Conciliations/Manager.aspx">Conciliaciones</asp:LinkButton>
    </li>
    <li>Generación de Archivo Conciliación Recargas Movilred</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Generación de Archivo Conciliación Recargas Movilred
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite generar el archivo de conciliación para Venta de Recargas Movilred:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Generación Archivo de Conciliación</h3>
        </div>
        <div class="body">
            
          	<div class="st-form-line">	
                <span><b>Fecha de Transacciones:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                <asp:TextBox ID="txtDate" CSSClass="datepicker-input" 
                    onkeydown = "return false;" runat="server" Font-Bold="True" Width="100px" 
                    ToolTip="Fecha de Transacciones" TabIndex="1"></asp:TextBox>
                &nbsp;
                &nbsp;
                &nbsp;
                &nbsp;
                <asp:Button ID="btnGenerateMovilredFile" runat="server" Text="1. Generar Archivo" 
                    CssClass="st-button" TabIndex="2" />
                <div class="clear"></div> 
            </div>
            
            <asp:Panel ID="pnlDownload" runat="server" Visible="False">
          	    <div class="st-form-line">	
                    <span><b>Archivo:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                    <asp:TextBox ID="txtPath" CSSClass="st-forminput" 
                        runat="server" Font-Bold="True" Width="300px" 
                        ToolTip="Archivo de Descarga" TabIndex="1" Enabled="False"></asp:TextBox>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <asp:Button ID="btnDownload" runat="server" Text="2. Descargar" 
                        CssClass="st-button" TabIndex="2" />
                    <asp:Button ID="btnFinalize" runat="server" CssClass="st-button" TabIndex="3" 
                        Text="3. Finalizar" />
                    <div class="clear"></div> 
                </div>            
            </asp:Panel>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Conciliations/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <script type="text/javascript" src="../js/ValidatorConciliation.js"></script>   
    
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

