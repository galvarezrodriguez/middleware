﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="LoadFileTopupMovilred.aspx.vb" Inherits="Conciliations_LoadFileTopupMovilred" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Cargue de Archivo Conciliación Movilred
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     

    <script type="text/javascript" src="../js/jquery.wysiwyg.js"></script>

	<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>

    <script type="text/javascript" src="../js/fullcalendar.min.js"></script>

	<script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Conciliations/Manager.aspx">Conciliaciones</asp:LinkButton>
    </li>
    <li>Cargue de Archivo Conciliación Movilred</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Cargue de Archivo Conciliación Movilred
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite cargar el archivo de conciliación para venta de recargas Movilred:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Cargue Archivo de Conciliación</h3>
        </div>
        <div class="body">
            
          	<div class="st-form-line">	
                <span><b>Archivo Movilred:&nbsp;<asp:FileUpload ID="fileMovilredUpload" runat="server" TabIndex="1" /></b></span>
                &nbsp;&nbsp;&nbsp;&nbsp;                
                <asp:Button ID="btnMovilredUploadFile" runat="server" Text="1. Cargar Archivo" CssClass="st-button" TabIndex="2" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblFileDate" runat="server" Text="Fecha del Archivo:" Visible="false"></asp:Label>
                <asp:TextBox ID="txtFileDate" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                <asp:TextBox ID="txtFileName" runat="server" Visible="False"></asp:TextBox>
                <asp:TextBox ID="txtDiffFileName" runat="server" Visible="False"></asp:TextBox>
                <div class="clear"></div> 
            </div>
            <div class="st-form-line">                
                <asp:Button ID="btnMovilredConciliation" runat="server" Text="2. Realizar Conciliación" 
                    CssClass="st-button" TabIndex="3" Visible="false" />                

                &nbsp;&nbsp;&nbsp;&nbsp;
                    
                <asp:Button ID="btnDownloadDiffFile" runat="server" Text="3. Descargar Archivo Diff." 
                    CssClass="st-button" TabIndex="4" Visible="false" />
                    
                &nbsp;&nbsp;&nbsp;&nbsp;
                    
                <asp:Button ID="btnFinalize" runat="server" Text="4. Finalizar" 
                    CssClass="st-button" TabIndex="5" Visible="false" />
                    
                <div class="clear"></div> 
            </div>           
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
            <br />
                            
            <asp:Panel ID="pnlDifferences" runat="server" Visible="False">    
                <div class="grid450-left">
                    <div class="simplebox">
                        <div class="titleh"><h3>Diferencias Encontradas[Conred &rArr; Movilred]:</h3></div>
                        <div class="body">
                            <p class="padding-in">
                                <ul class="statistics">
            	                    <li>Número de Transacciones: <p> <span class="blue"><asp:Label ID="lblCounter1" 
                                            runat="server" Font-Size="12pt"></asp:Label></span></p></li>
            	                    <li>Valor de Diferencias:   <p> <span class="blue"><asp:Label ID="lblAmount1" 
                                            runat="server" Font-Size="12pt"></asp:Label></span></p></li>
                                </ul>
                                <p>
                                </p>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="grid450-right">
                    <div class="simplebox">
                        <div class="titleh"><h3>Diferencias Encontradas[Movilred &rArr; Conred]:</h3></div>
                        <div class="body">
                            <p class="padding-in">
                                <ul class="statistics">
            	                    <li>Número de Transacciones: <p> <span class="red"><asp:Label ID="lblCounter2" 
                                            runat="server" Font-Size="12pt"></asp:Label></span></p></li>
            	                    <li>Valor de Diferencias:   <p> <span class="red"><asp:Label ID="lblAmount2" 
                                            runat="server" Font-Size="12pt"></asp:Label></span></p></li>
                                </ul>
                                <p>
                                </p>
                            </p>
                        </div>
                    </div>
                </div>            
            </asp:Panel>              
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Conciliations/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">

    <script type="text/javascript" src="../js/ValidatorConciliation.js"></script>   
    
    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

