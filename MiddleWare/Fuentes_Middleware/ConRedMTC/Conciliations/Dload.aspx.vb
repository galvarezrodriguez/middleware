
Partial Class Conciliations_Dload
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try

            Dim fileName = Request.Params("fileName")

            Dim filePath As String = "C:\\Conciliaciones\\" & fileName
            Response.Clear()
            Response.AddHeader("content-disposition", "attachment;filename=" & fileName)
            Response.Charset = ""
            Response.ContentType = "text/plain"
            Response.WriteFile(filePath)
            Response.End()

        Catch exptn As Exception
            Throw
        End Try
    End Sub
End Class
