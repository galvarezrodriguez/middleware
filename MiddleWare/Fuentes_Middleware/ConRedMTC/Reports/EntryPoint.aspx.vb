﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Users

Partial Class Reports_EntryPoint
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtStartDate.Text = Now.ToString("MM/dd/yyyy")
            txtEndDate.Text = Now.ToString("MM/dd/yyyy")
            'txtStartDate.Text = Now.ToString("dd/MM/yyyy")
            'txtEndDate.Text = Now.ToString("dd/MM/yyyy")
            txtReportType.Text = getReportName()
            setFormControls()
        Else
            pnlError.Visible = False
            pnlMsg.Visible = False
        End If
    End Sub

    Private Function getReportName() As String

        Dim retVal As String = ""

        Select Case objSessionParams.intReportType

            Case 46
                retVal = "Ventas Detalladas X Comercios"

            Case 47
                retVal = "Movimiento de Cuenta X Comercios"

            Case 48
                retVal = "Resumen de Ventas X Día"

            Case 54
                retVal = "Log de Auditoría"

            Case 80
                retVal = "Compras de Cupo X Proveedor"

        End Select

        Return retVal

    End Function

    Private Sub setFormControls()
        Select Case objSessionParams.intReportType

            Case 46,47
                pnlMerchants.Visible = True
                pnlUsers.Visible = False
                pnlSuppliers.Visible = False

            Case 48
                pnlMerchants.Visible = False
                pnlUsers.Visible = False
                pnlSuppliers.Visible = False

            Case 54
                pnlMerchants.Visible = False
                pnlUsers.Visible = True
                pnlSuppliers.Visible = False

            Case 80
                pnlSuppliers.Visible = True
                pnlMerchants.Visible = False
                pnlUsers.Visible = False

        End Select
    End Sub

    Protected Sub btnGenerateReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerateReport.Click
        Dim startDate As Date
        Dim endDate As Date

        startDate = Date.Parse(txtStartDate.Text)
        endDate = Date.Parse(txtEndDate.Text)

        'Validate Data
        If startDate <= endDate Then
            'Calculate Days Difference

            If DateDiff(DateInterval.Day, startDate, endDate) < 31 Then
                'Generate Report
                objSessionParams.dateStartDate = startDate
                objSessionParams.dateEndDate = endDate
                objSessionParams.strReportName = txtReportType.Text

                If pnlMerchants.Visible = True Then
                    objSessionParams.strMerchantID = ddlMerchants.SelectedValue
                    objSessionParams.strMerchantName = ddlMerchants.SelectedItem.Text
                Else
                    objSessionParams.strMerchantID = ""
                    objSessionParams.strMerchantName = ""
                End If

                If pnlUsers.Visible = True Then
                    objSessionParams.intSelectedUserID = ddlUsers.SelectedValue
                    objSessionParams.strSelectedUserName = ddlUsers.SelectedItem.Text
                Else
                    objSessionParams.intSelectedUserID = 0
                    objSessionParams.strSelectedUserName = ""
                End If

                If pnlSuppliers.Visible = True Then
                    objSessionParams.intSelectedSupplier = ddlSuppliers.SelectedValue
                    objSessionParams.strSelectedSupplier = ddlSuppliers.SelectedItem.Text
                Else
                    objSessionParams.intSelectedSupplier = 0
                    objSessionParams.strSelectedSupplier = ""
                End If

                'Set Data into Session
                Session("SessionParameters") = objSessionParams

                'Loading Window
                Dim url As String = "ReportViewer.aspx"
                Response.Redirect("~/Reports/Loading.Aspx?Page=" & url, "False")
            Else
                pnlError.Visible = True
                pnlMsg.Visible = False
                lblError.Text = "El rango del reporte no debe superar los 30 días."
            End If
        Else
            pnlError.Visible = True
            pnlMsg.Visible = False
            lblError.Text = "La fecha inicial debe ser menor que la fecha final."
        End If

    End Sub
End Class
