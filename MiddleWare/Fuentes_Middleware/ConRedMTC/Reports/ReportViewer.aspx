﻿<%@ Page Title="" Language="VB" MasterPageFile="~/ReportPage.master" AutoEventWireup="false" CodeFile="ReportViewer.aspx.vb" Inherits="Reports_ReportViewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Visor de Reportes
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <div style="text-align: center;">
            <asp:Label ID="lblReportName" runat="server" Text="Label" Font-Bold="True" Font-Size="14pt"></asp:Label>
            <br />
            <asp:Label ID="lblOptionName" runat="server" Text="Label" Font-Bold="True" Font-Size="11pt"></asp:Label><asp:Label ID="lblOptionValue" runat="server" Text="Label" Font-Size="11pt"></asp:Label>
            <br />
            <asp:Label ID="lblDate1" runat="server" Text="Fecha Inicial:" Font-Bold="True" Font-Size="11pt"></asp:Label>
            &nbsp;&nbsp;
            <asp:Label ID="lblStartDate" runat="server" Text="Label" Font-Size="10pt"></asp:Label>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblDate2" runat="server" Text="Fecha Final:" Font-Bold="True" Font-Size="11pt"></asp:Label>
            &nbsp;&nbsp;            
            <asp:Label ID="lblEndDate" runat="server" Text="Label" Font-Size="10pt"></asp:Label>
    </div>
        
    <br />
    <asp:LinkButton ID="lnkExportExcel" runat="server">Exportar a Excel <asp:Image ID="imgExcel" runat="server" ImageUrl="~/img/excel.gif" /> </asp:LinkButton>
    <br />
    <br />
    <asp:GridView ID="grdAccountMovementXMerchant" runat="server" 
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
        DataSourceID="dsAccountMovementXMerchant" ForeColor="#333333" 
        CssClass="mGridCenter" 
        EmptyDataText="No existen movimientos de cuenta del comercio para el período dado." 
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="mov_fecha" HeaderText="Fecha" 
                SortExpression="mov_fecha" />        
            <asp:BoundField DataField="ent_comercio_JNTS" HeaderText="Comercio" 
                SortExpression="ent_comercio_JNTS" />
            <asp:BoundField DataField="nombre" HeaderText="Tipo Movimiento" 
                SortExpression="nombre" />
            <asp:BoundField DataField="ban_nombre" HeaderText="Banco" 
                SortExpression="ban_nombre" />
            <asp:BoundField DataField="cue_numero" HeaderText="Nro. Cuenta" 
                SortExpression="cue_numero" />
            <asp:BoundField DataField="mov_entidad_saldo_inicial" DataFormatString="{0:C2}" 
                HeaderText="Saldo Inicial" SortExpression="mov_entidad_saldo_inicial" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="mov_valor" DataFormatString="{0:C2}" 
                HeaderText="Valor" SortExpression="mov_valor" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="mov_entidad_saldo_final" DataFormatString="{0:C2}" 
                HeaderText="Saldo Final" SortExpression="mov_entidad_saldo_final" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="mov_observacion" HeaderText="Observación" 
                SortExpression="mov_observacion" />
            <asp:BoundField DataField="usu_nombre" HeaderText="Usuario Web" 
                SortExpression="usu_nombre" />
            
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>            
    
    <asp:SqlDataSource ID="dsAccountMovementXMerchant" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
        SelectCommand="sp_webReporteMovimientoCuentaComercio" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="merchantID" Type="String" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource> 

    <asp:GridView ID="grdSalesXMerchant" runat="server" 
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
        DataSourceID="dsSalesXMerchant" ForeColor="#333333" 
        CssClass="mGridCenter" 
        EmptyDataText="No existen ventas registradas del comercio para el período dado." 
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha_inicial" HeaderText="Fecha" 
                SortExpression="tra_fecha_inicial" />
            <asp:BoundField DataField="tra_comercio_id" HeaderText="Comercio" 
                SortExpression="tra_comercio_id" />
            <asp:BoundField DataField="tra_terminal_id" HeaderText="Terminal" 
                SortExpression="tra_terminal_id" />
            <asp:BoundField DataField="pro_nombre" HeaderText="Producto" 
                SortExpression="pro_nombre" />
            <asp:BoundField DataField="tra_referencia_cliente_1" HeaderText="Ref. Cliente" 
                SortExpression="tra_referencia_cliente_1" />
            <asp:BoundField DataField="tra_auth_id_p38" HeaderText="Auth. ID P38" 
                SortExpression="tra_auth_id_p38" />
            <asp:BoundField DataField="tra_autorizacion_ope_id" 
                HeaderText="Auth. Id Operador" SortExpression="tra_autorizacion_ope_id" />
            <asp:BoundField DataField="tra_comercio_saldo_inicial" DataFormatString="{0:C2}" 
                HeaderText="Saldo Inicial" SortExpression="tra_comercio_saldo_inicial" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="tra_valor" DataFormatString="{0:C2}" 
                HeaderText="Valor" SortExpression="tra_valor" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="tra_comercio_saldo_final" HeaderText="Saldo final" 
                SortExpression="tra_comercio_saldo_final" DataFormatString="{0:C2}" >            
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>            
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>            
    
    <asp:SqlDataSource ID="dsSalesXMerchant" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
        SelectCommand="sp_webReporteVentasComercio" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="merchantID" Type="String" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource> 

    <asp:GridView ID="grdSalesXDay" runat="server" 
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
        DataSourceID="dsSalesXDay" ForeColor="#333333" 
        CssClass="mGridCenter" 
        EmptyDataText="No existen ventas registradas para el período dado." 
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="tra_fecha" HeaderText="Fecha" 
                SortExpression="tra_fecha" ReadOnly="True" />
            <asp:BoundField DataField="ventas_comcel_directo" HeaderText="Ventas Comcel-Directo" 
                SortExpression="ventas_comcel_directo" DataFormatString="{0:C2}" 
                ReadOnly="True" Visible="False" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="ventas_tigo_directo" HeaderText="Ventas Tigo-Directo" 
                SortExpression="ventas_tigo_directo" DataFormatString="{0:C2}" 
                ReadOnly="True" Visible="False" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="ventas_comcel_movilred" HeaderText="Ventas Comcel-Movilred" 
                SortExpression="ventas_comcel_movilred" DataFormatString="{0:C2}" 
                ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="ventas_tigo_movilred" 
                HeaderText="Ventas Tigo-Movilred" SortExpression="ventas_tigo_movilred" 
                DataFormatString="{0:C2}" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="ventas_movistar_movilred" DataFormatString="{0:C2}" 
                HeaderText="Ventas Movistar-Movilred" 
                SortExpression="ventas_movistar_movilred" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="ventas_otros_recargas_movilred" DataFormatString="{0:C2}" 
                HeaderText="Ventas Otros-Operadores-Movilred" 
                SortExpression="ventas_otros_recargas_movilred" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="tra_total_recargas" DataFormatString="{0:C2}" 
                HeaderText="Total Recargas" SortExpression="tra_total_recargas" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>            
            <asp:BoundField DataField="recaudo_facturas_movilred" DataFormatString="{0:C2}" 
                HeaderText="Recaudo Facturas-Movilred" 
                SortExpression="recaudo_facturas_movilred" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="tra_total_recaudos" DataFormatString="{0:C2}" 
                HeaderText="Total Recaudos" SortExpression="tra_total_recaudos" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>            
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>            
    
    <asp:SqlDataSource ID="dsSalesXDay" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
        SelectCommand="sp_webReporteResumenVentas" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource> 

    <asp:GridView ID="grdLogXUser" runat="server" 
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
        DataSourceID="dsLogXUser" ForeColor="#333333" 
        CssClass="mGridCenter" 
        EmptyDataText="No existen acciones registradas del usuario para el período dado." 
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="log_fecha" HeaderText="Fecha" 
                SortExpression="log_fecha" >
            </asp:BoundField>
            <asp:BoundField DataField="usu_login" HeaderText="Login" 
                SortExpression="usu_login" >
            </asp:BoundField>
            <asp:BoundField DataField="usu_nombre" HeaderText="Nombre Usuario" 
                SortExpression="usu_nombre" >            
            </asp:BoundField>
            <asp:BoundField DataField="Objeto" HeaderText="Objeto del Sistema" 
                SortExpression="Objeto" >
            </asp:BoundField>
            <asp:BoundField DataField="Metodo" 
                HeaderText="Método" SortExpression="Metodo" >
            </asp:BoundField>
            <asp:BoundField DataField="TipoMensaje" 
                HeaderText="Tipo de Mensaje" 
                SortExpression="TipoMensaje" >
            </asp:BoundField>
            <asp:BoundField DataField="log_mensaje1" 
                HeaderText="Info. Adicional" SortExpression="log_mensaje1" >
            </asp:BoundField>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>            
    
    <asp:SqlDataSource ID="dsLogXUser" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
        SelectCommand="sp_webReporteAuditoria" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="userID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource> 

    <asp:GridView ID="grdPurchaseXSupplier" runat="server" 
        AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" 
        DataSourceID="dsPurchaseXSupplier" ForeColor="#333333" 
        CssClass="mGridCenter" 
        EmptyDataText="No existen compras de cupo x proveedor para el período dado." 
        HorizontalAlign="Center" PageSize="25" Visible="False">
        <RowStyle BackColor="White" ForeColor="White" />
        <Columns>
            <asp:BoundField DataField="fecha" HeaderText="Fecha" 
                SortExpression="fecha" ReadOnly="True" >
            </asp:BoundField>
            <asp:BoundField DataField="prov_nombre" HeaderText="Proveedor" 
                SortExpression="prov_nombre" ReadOnly="True" >
            </asp:BoundField>
            <asp:BoundField DataField="ban_nombre" HeaderText="Banco" 
                SortExpression="ban_nombre" ReadOnly="True" >            
            </asp:BoundField>
            <asp:BoundField DataField="cue_numero" HeaderText="Número Cuenta" 
                SortExpression="cue_numero" ReadOnly="True" >
            </asp:BoundField>
            <asp:BoundField DataField="comp_saldo_inicial" 
                HeaderText="Proveedor Saldo Inicial" SortExpression="comp_saldo_inicial" 
                DataFormatString="{0:C2}" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="comp_valor" 
                HeaderText="Valor Compra" 
                SortExpression="comp_valor" DataFormatString="{0:C2}" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="comp_saldo_final" 
                HeaderText="Proveedor Saldo Final" SortExpression="comp_saldo_final" 
                DataFormatString="{0:C2}" ReadOnly="True" >
            <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>            
            <asp:BoundField DataField="comp_observacion" HeaderText="Observación" 
                ReadOnly="True" SortExpression="comp_observacion" />
            <asp:BoundField DataField="usu_nombre" HeaderText="Usuario" ReadOnly="True" 
                SortExpression="usu_nombre" />
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Center" 
            CssClass="pgr" Font-Underline="False" />
        <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#E5E5E5" BorderColor="#666666" BorderStyle="Solid" 
            BorderWidth="1px" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>            
    
    <asp:SqlDataSource ID="dsPurchaseXSupplier" runat="server" 
        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
        SelectCommand="sp_webReporteComprasXProveedor" 
        SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:Parameter Name="supplierID" Type="Int64" />
            <asp:Parameter Name="startDate" Type="DateTime" />
            <asp:Parameter Name="endDate" Type="DateTime" />
        </SelectParameters>
    </asp:SqlDataSource> 

    <br />

    <asp:HyperLink ID="lnkBack" runat="server" NavigateUrl="~/Reports/EntryPoint.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>
</asp:Content>

