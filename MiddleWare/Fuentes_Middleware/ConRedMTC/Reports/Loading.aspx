﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Loading.aspx.vb" Inherits="Reports_Loading" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

	<HEAD>
		<title><%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Cargando Reporte</title>
		<script language="javascript">	
		    var iLoopCounter = 1;
		    var iMaxLoop = 5;
		    var iIntervalId;
    		
		    function BeginPageLoad() {
			    location.href = "<%= Request.QueryString("Page")%>";
		    }	
		</script>
	</HEAD>
	<body onload="BeginPageLoad()">
		<form id="Form1" method="post" runat="server">
            <table border="0" cellpadding="0" cellspacing="0" width="99%" height="99%" class="mGridCenter" align="center" valign="middle">
		        <tr>
			        <td align="center" valign="middle">
                        <asp:Image ID="imgLoading" runat="server" ImageUrl="~/img/proccesing.gif" ImageAlign="Middle"/>
                    </td>
		        </tr>
	        </table>
	    </form>
	</body>
</HTML>


