﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="EntryPoint.aspx.vb" Inherits="Reports_EntryPoint" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <title>
        <%=ConfigurationSettings.AppSettings.Get("AppWebName") & " v" & ConfigurationSettings.AppSettings.Get("VersionAppWeb")%> :: Generación de Reportes
    </title>
    
    <script type="text/javascript" src="../js/toogle.js"></script>
    
    <script type="text/javascript" src="../js/jquery.tipsy.js"></script>
    
    <script type="text/javascript" src="../js/jquery-settings.js"></script>

    <script type="text/javascript" src="../js/msgAlert.js"></script>   

	<script type="text/javascript" src="../js/jquery.uniform.min.js"></script>     

    <script type="text/javascript" src="../js/jquery.wysiwyg.js"></script>

	<script type="text/javascript" src="../js/jquery.tablesorter.min.js"></script>

    <script type="text/javascript" src="../js/fullcalendar.min.js"></script>

	<script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>   
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Path" Runat="Server">
    <li>
        <asp:LinkButton ID="lnkSecurity" runat="server" CssClass="fixed" 
            PostBackUrl="~/Reports/Manager.aspx">Reportes</asp:LinkButton>
    </li>
    <li>Generación de Reportes</li>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" Runat="Server">
    Generación de Reportes
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" Runat="Server">
    <p>Este módulo le permite generar reportes del sistema:</p>
    
    <br />
    
    <!-- START SIMPLE FORM -->
    <div class="simplebox grid960">
	    <div class="titleh">
    	    <h3>Datos del Reporte</h3>
        </div>
        <div class="body">
            
          	<div class="st-form-line" style="text-align: center;">	
            	<span><b>Fecha Inicial:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                <asp:TextBox ID="txtStartDate" CSSClass="datepicker-input" 
                    onkeydown = "return false;" runat="server" Font-Bold="True" Width="100px" 
                    ToolTip="Fecha Inicial" TabIndex="1"></asp:TextBox>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            	<span><b>Fecha Final:&nbsp;&nbsp;&nbsp;&nbsp;</b></span>
                <asp:TextBox ID="txtEndDate" CSSClass="datepicker-input" 
                    onkeydown = "return false;" runat="server" Font-Bold="True" Width="100px" 
                    ToolTip="Fecha Inicial" TabIndex="2"></asp:TextBox>
          		<div class="clear"></div> 
            </div>

            <div class="st-form-line">	    
                <span class="st-labeltext"><b>Tipo de Reporte:</b></span>	                
                
                <asp:TextBox ID="txtReportType" CssClass="st-forminput" style="width:510px" 
                    runat="server" TabIndex="1" MaxLength="100" 
                    ToolTip="Tipo de Reporte." onkeydown = "return (event.keyCode!=13);" 
                    Enabled="False"></asp:TextBox>
            </div>
            
            <asp:Panel ID="pnlMerchants" runat="server" Visible="false">
                <div class="st-form-line">	    
                    <span class="st-labeltext"><b>Comercios:</b></span>	                
                    <asp:DropDownList ID="ddlMerchants" class="uniform" runat="server" 
                        DataSourceID="dsMerchants" DataTextField="Descripcion" 
                        DataValueField="IDCOMERCIO" Width="200px" 
                        ToolTip="Seleccione el Comercio para el reporte." TabIndex="1">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsMerchants" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                        SelectCommand="sp_webConsultarComerciosParaReporte" 
                        SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>            
            </asp:Panel>
            
            <asp:Panel ID="pnlUsers" runat="server" Visible="false">
                <div class="st-form-line">	    
                    <span class="st-labeltext"><b>Usuarios:</b></span>	                
                    <asp:DropDownList ID="ddlUsers" class="uniform" runat="server" 
                        DataSourceID="dsUsers" DataTextField="usu_nombre" 
                        DataValueField="usu_id" Width="200px" 
                        ToolTip="Seleccione el Usuario para el reporte." TabIndex="1">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsUsers" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                        SelectCommand="SELECT -1 AS usu_id, 'Todos los Usuarios' AS usu_nombre
                        UNION ALL
                        SELECT usu_id, usu_login + ' - ' + usu_nombre AS usu_nombre FROM Usuario "></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>            
            </asp:Panel>            
            
            <asp:Panel ID="pnlSuppliers" runat="server" Visible="false">
                <div class="st-form-line">	    
                    <span class="st-labeltext"><b>Proveedores:</b></span>	                
                    <asp:DropDownList ID="ddlSuppliers" class="uniform" runat="server" 
                        DataSourceID="dsSuppliers" DataTextField="prov_nombre" 
                        DataValueField="prov_id" Width="200px" 
                        ToolTip="Seleccione el Proveedor para el reporte." TabIndex="1">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="dsSuppliers" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:ConredConnectionString %>" 
                        SelectCommand="SELECT -1 AS prov_id, 'Todos los Proveedores' AS prov_nombre
                        UNION ALL
                        SELECT prov_id, prov_nombre FROM proveedor"></asp:SqlDataSource>
                    <div class="clear"></div>
                </div>            
            </asp:Panel>            

            <div class="button-box">
                <asp:Button ID="btnGenerateReport" runat="server" Text="Generar Reporte" 
                    CssClass="st-button" TabIndex="4" 
                    onclientclick="return validateGenerateReport()" />
            </div>
            
            <asp:Panel ID="pnlMsg" runat="server" Visible="False">    
                <div class="albox succesbox">
                    <asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>   
            
            <asp:Panel ID="pnlError" runat="server" Visible="False">    
                <div class="albox errorbox">
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <a href="#" class="close tips" title="Cerrar">Cerrar</a>
                </div>
            </asp:Panel>                       
            
        </div>
    </div>
    
    <asp:HyperLink ID="lnkBack" runat="server" 
        NavigateUrl="~/Reports/Manager.aspx">&lt;&lt;Volver a la página anterior</asp:HyperLink>    
    
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="jsOutput" Runat="Server">
    
    <!-- Validator -->
    <script src="../js/ValidatorReport.js" type="text/javascript"></script>

    <script src="../js/ValidatorUtils.js" type="text/javascript"></script>

    <asp:Literal ID="ltrScript" runat="server"></asp:Literal>
    
</asp:Content>

