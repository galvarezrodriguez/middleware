﻿Imports System.Data
Imports System.Data.SqlClient
Imports Conred.Reports
Imports System.Xml
Imports System.IO

Partial Class Reports_ReportViewer
    Inherits Conred.Web.BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            lblReportName.Text = objSessionParams.strReportName
            lblStartDate.Text = objSessionParams.dateStartDate
            lblEndDate.Text = objSessionParams.dateEndDate

            doReport()

        End If

    End Sub

    Private Sub doReport()

        Dim strReportData As String = ""

        Select Case objSessionParams.intReportType

            Case 46
                'Ventas X Comercios
                lblOptionName.Text = "Comercio: "
                lblOptionValue.Text = objSessionParams.strMerchantName
                dsSalesXMerchant.SelectParameters("merchantID").DefaultValue = objSessionParams.strMerchantID
                dsSalesXMerchant.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsSalesXMerchant.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdSalesXMerchant.Visible = True
                grdAccountMovementXMerchant.Visible = False
                grdSalesXDay.Visible = False
                grdLogXUser.Visible = False
                grdPurchaseXSupplier.Visible = False

            Case 47
                'Movimiento de Cuenta X Comercios
                lblOptionName.Text = "Comercio: "
                lblOptionValue.Text = objSessionParams.strMerchantName
                dsAccountMovementXMerchant.SelectParameters("merchantID").DefaultValue = objSessionParams.strMerchantID
                dsAccountMovementXMerchant.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsAccountMovementXMerchant.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdAccountMovementXMerchant.Visible = True
                grdSalesXMerchant.Visible = False
                grdSalesXDay.Visible = False
                grdLogXUser.Visible = False
                grdPurchaseXSupplier.Visible = False

            Case 48
                'Resumen de Ventas X Día
                lblOptionName.Text = ""
                lblOptionValue.Text = ""
                dsSalesXDay.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsSalesXDay.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdSalesXDay.Visible = True
                grdAccountMovementXMerchant.Visible = False
                grdSalesXMerchant.Visible = False
                grdLogXUser.Visible = False
                grdPurchaseXSupplier.Visible = False

            Case 54
                'Reporte Log X Usuario
                lblOptionName.Text = "Usuario: "
                lblOptionValue.Text = objSessionParams.strSelectedUserName
                dsLogXUser.SelectParameters("userID").DefaultValue = objSessionParams.intSelectedUserID
                dsLogXUser.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsLogXUser.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdLogXUser.Visible = True
                grdSalesXDay.Visible = False
                grdAccountMovementXMerchant.Visible = False
                grdSalesXMerchant.Visible = False
                grdPurchaseXSupplier.Visible = False

            Case 80
                'Reporte Compras X Proveedor
                lblOptionName.Text = "Proveedor: "
                lblOptionValue.Text = objSessionParams.strSelectedSupplier
                dsPurchaseXSupplier.SelectParameters("supplierID").DefaultValue = objSessionParams.intSelectedSupplier
                dsPurchaseXSupplier.SelectParameters("startDate").DefaultValue = objSessionParams.dateStartDate
                dsPurchaseXSupplier.SelectParameters("endDate").DefaultValue = objSessionParams.dateEndDate
                grdPurchaseXSupplier.Visible = True
                grdLogXUser.Visible = False
                grdSalesXDay.Visible = False
                grdAccountMovementXMerchant.Visible = False
                grdSalesXMerchant.Visible = False

        End Select

        strReportData = "Reporte: " & objSessionParams.strReportName & ", " & lblOptionName.Text & _
                        lblOptionValue.Text & ", Fecha Inicial: " & objSessionParams.dateStartDate & _
                        ", Fecha Fin: " & objSessionParams.dateEndDate

        'Save Log
        objAccessToken.LogMessageByObjectMethod(getCurrentPage(), "Load", "Reporte Generado. Datos[ " & strReportData & " ]", "")

    End Sub

    Protected Sub lnkExportExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExportExcel.Click
        Dim strExcelFile As String
        Dim XMLDoc As System.Xml.XmlDataDocument
        Dim objNewNode As System.Xml.XmlNode
        Dim objFrstNode As System.Xml.XmlNode
        Dim connection As New SqlConnection(strConnectionString)
        Dim command As New SqlCommand()
        Dim result As SqlDataReader
        Dim da As New SqlDataAdapter(command)
        Dim dataset As New Data.DataSet
        Dim objExport As New ExcelExport

        'Init Actions
        objExport.TempFolder = ConfigurationSettings.AppSettings.Get("TempReportFolder")
        objExport.XSLStyleSheetFolder = ConfigurationSettings.AppSettings.Get("XSLReportFolder")
        objExport.CleanUpTemporaryFiles()

        Try
            'Ventas X Comercios
            If objSessionParams.intReportType = 46 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteVentasComercio"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@merchantID", objSessionParams.strMerchantID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" _
                                        & "<Merchant>" & objSessionParams.strMerchantName & "</" & "Merchant>" & _
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "VentasXComercios.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)

            End If

            'Movimiento de Cuenta X Comercios
            If objSessionParams.intReportType = 47 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteMovimientoCuentaComercio"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@merchantID", objSessionParams.strMerchantID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" _
                                        & "<Merchant>" & objSessionParams.strMerchantName & "</" & "Merchant>" & _
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "MvtoCuentaXComercios.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Resumen de Ventas X Día
            If objSessionParams.intReportType = 48 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteResumenVentas"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" _
                                        & "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "VentasXDia.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte Log X Usuario
            If objSessionParams.intReportType = 54 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteAuditoria"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@userID", objSessionParams.intSelectedUserID))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" _
                                        & "<User>" & objSessionParams.strSelectedUserName & "</" & "User>" & _
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "LogAuditoria.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

            'Reporte Compras X Proveedor
            If objSessionParams.intReportType = 80 Then
                command.Connection = connection
                command.CommandType = Data.CommandType.StoredProcedure
                command.CommandText = "sp_webReporteComprasXProveedor"
                command.Connection.Open()
                command.Parameters.Add(New SqlParameter("@supplierID", objSessionParams.intSelectedSupplier))
                command.Parameters.Add(New SqlParameter("@startDate", objSessionParams.dateStartDate))
                command.Parameters.Add(New SqlParameter("@endDate", objSessionParams.dateEndDate))

                'Execute Command
                result = command.ExecuteReader()
                command.Connection.Close()
                da.SelectCommand = command

                'Fill dataset
                da.Fill(dataset, "Table")

                'Create the XML Data Document from the dataset.
                XMLDoc = New XmlDataDocument(dataset)

                If XMLDoc.DocumentElement Is Nothing Then
                    Exit Sub
                End If

                'Add Header Information
                objNewNode = XMLDoc.CreateElement("HeaderDetails")
                objNewNode.InnerXml = "<ReportName>" & objSessionParams.strReportName & "</ReportName>" _
                                        & "<Supplier>" & objSessionParams.strSelectedSupplier & "</" & "Supplier>" & _
                                        "<Period>" & objSessionParams.dateStartDate.ToString("dd/MM/yyy") & " hasta: " & objSessionParams.dateEndDate.ToString("dd/MM/yyy") & "</Period>"
                XMLDoc.DataSet.EnforceConstraints = False
                objFrstNode = XMLDoc.DocumentElement.FirstChild
                XMLDoc.DocumentElement.InsertBefore(objNewNode, objFrstNode)

                'Generate Excel File
                strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "ComprasXProveedor.xsl")

                'Send Excel File To Client
                objExport.SendExcelToClient(strExcelFile)
            End If

        Catch ex As Threading.ThreadAbortException
            'Do nothing
        Catch ex As Exception
            HandleErrorRedirect(ex)
        Finally
            If Not connection Is Nothing Then
                connection.Close()
            End If
        End Try

    End Sub
End Class
