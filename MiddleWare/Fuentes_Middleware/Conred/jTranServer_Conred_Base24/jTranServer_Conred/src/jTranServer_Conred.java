/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin David Beltrán Duque
 */

//Java Imports
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Properties;

//jPOS Imports
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;

//DBPool Imports
import snaq.db.*;

//Conred Imports
import com.conred.fixedData.jTranServerFixedData;
import com.conred.jPOSConredChannel.BASE24TCPChannel_Movilred;
import org.jpos.iso.packager.BASE24Packager;
import com.conred.utils.Utils;
import com.conred.logUtilities.LogWriter;
import com.conred.logUtilities.DaemonLogger;
import com.conred.jPOSConredChannel.ISOServer_Conred;
import com.conred.jPOSConredChannel.ISO87B_ConredPackager;
import com.conred.enumerations.*;
import com.conred.DAL.*;

public class jTranServer_Conred implements ISORequestListener {

  //Loggers jTranServer
  public static Logger loggerServer                         = null;
  public static LogWriter logWriterServer                   = null;
  
  //Log4java Logger
  private static org.apache.log4j.Logger logjTranServer     = org.apache.log4j.Logger.getLogger(jTranServer_Conred.class);
 
  //jTranServer Channel & Server
  public static ServerChannel jTranServerChannel            = null;  
  public static ISOServer_Conred jTranServer                = null;
  
  //Init Variables jTranServer
  public static boolean flagServerXMLlog                    = false;
  public static boolean flagServerTXTlog                    = false;
  public static String initParameters[]                     = new String[jTranServerInitParameters.END_FIELD.ordinal()];
  
  //DB Pool
  public static ConnectionPool conDBPool                    = null;
  
  //ISOMux for Each Product
  /*Comcel*/
  public static ISOChannel channelISOMux_Comcel             = null;
  public static ISOMUX gatewayISOMux_Comcel                 = null;
  public static int totalTimeout_Comcel                     = 0;
  public static Logger loggerGateway_Comcel                 = null;
  public static LogWriter logWriterGateway_Comcel           = null;

  /*Tigo*/
  public static ISOChannel channelISOMux_Tigo               = null;
  public static ISOMUX gatewayISOMux_Tigo                   = null;
  public static int totalTimeout_Tigo                       = 0;
  public static Logger loggerGateway_Tigo                   = null;
  public static LogWriter logWriterGateway_Tigo             = null;
  
  /*Movilred*/
  public static ISOChannel channelISOMux_Movilred           = null;
  public static ISOMUX gatewayISOMux_Movilred               = null;
  public static int totalTimeout_Movilred                   = 0;
  public static Logger loggerGateway_Movilred               = null;
  public static LogWriter logWriterGateway_Movilred         = null;

  private static int minThreadPool                          = 0;
  private static int maxThreadPool                          = 0;  
  public static byte[] BASE64_HEADER                        = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
  
  ////Process Pool
  protected static ThreadPool pool                          = null;  
  
  public jTranServer_Conred () {
        super();
  }
  
  public boolean process (ISOSource source, ISOMsg m) {
        if (pool == null) 
            pool = new ThreadPool (minThreadPool,maxThreadPool);

        pool.execute (new Process (source, m));
        return true;
  }  

  private static void logMessageID(String Data, int tcpPortID){
      logjTranServer.info(jTranServer_Conred.class.getName() + " [" + tcpPortID + "] : " + Data + ".");
  }
  
  private static void logMessageID(String Data, String trxID){
      logjTranServer.info(jTranServer_Conred.class.getName() + " [" + trxID + "] : " + Data + ".");
  }  
  
  protected void finalize() throws Throwable  {
      super.finalize();
      System.runFinalization();
      logMessageID("Servidor jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION + " Detenido", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
  } 

  public static void main (String[] args) throws Exception {
      boolean flagInit        = false;
      initializer initObj     = new initializer();
      Properties propObj      = new Properties();
      File propFile           = new File(jTranServerFixedData.JTRANSERVER_CONFIG_FILE);
      
      if (!propFile.exists()){
          //Create Log4j Logger
          org.apache.log4j.PropertyConfigurator.configure(jTranServerFixedData.JTRANSERVER_CONFIG_FILE);
          logMessageID("Archivo de Propiedades No Encontrado, Nombre: " + jTranServerFixedData.JTRANSERVER_CONFIG_FILE + " - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION, 0);
          System.out.println("Archivo de Propiedades No Encontrado, Nombre: " + jTranServerFixedData.JTRANSERVER_CONFIG_FILE + " - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION);
          System.exit(-1);
          return;   ///Init Error No Properties File
      }

      try {
          propObj.load(new FileInputStream(jTranServerFixedData.JTRANSERVER_CONFIG_FILE));
      } catch(Exception e) {
          //Create Log4j Logger
          org.apache.log4j.PropertyConfigurator.configure(jTranServerFixedData.JTRANSERVER_CONFIG_FILE);
          logMessageID("Error Leyendo Archivo de Propiedades - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION, 0);
          System.out.println("Error Leyendo Archivo de Propiedades - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION);
          System.exit(-1);
          return;   ///Error Reading Properties File
      }

      //Check Init Data via Properties File
      if (propObj.getProperty(jTranServerFixedData.DATABASE_URL_KEY) == null
              || propObj.getProperty(jTranServerFixedData.DATABASE_USER_KEY) == null
              || propObj.getProperty(jTranServerFixedData.DATABASE_PASSWORD_KEY) == null){
          System.out.println("Algunas Propiedades No Encontradas, Revisar Archivo de Propiedades - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION);
          System.exit(-1);
          return;   ///Init Data Empty
      }
          
      //Get Init. Parameters from DB
      flagInit = initObj.getInitParameters(initParameters, propObj.getProperty(jTranServerFixedData.DATABASE_DRIVER_JDBC_KEY), propObj.getProperty(jTranServerFixedData.DATABASE_URL_KEY), propObj.getProperty(jTranServerFixedData.DATABASE_USER_KEY), propObj.getProperty(jTranServerFixedData.DATABASE_PASSWORD_KEY));
        
      if (!flagInit){
          //Create Log4j Logger
          org.apache.log4j.PropertyConfigurator.configure(jTranServerFixedData.JTRANSERVER_CONFIG_FILE);          
          logMessageID("Error Conectando al Servidor de Base de Datos para Inicilización - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION, 0);
          System.out.println("Error Conectando al Servidor de Base de Datos para Inicilización - jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION);
          System.exit(-1);
          return;   ///Error Init DB
      }

      SimpleDateFormat formatlogger = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
        
      //Init Parameters
      minThreadPool = Integer.parseInt(initParameters[jTranServerInitParameters.MIN_THREAD_POOL.ordinal()]);
      maxThreadPool = Integer.parseInt(initParameters[jTranServerInitParameters.MAX_THREAD_POOL.ordinal()]);

      ///Init Process Pool
      pool = new ThreadPool (minThreadPool,maxThreadPool);       
      
      ///Use Server XML Logger or Not
      if (initParameters[jTranServerInitParameters.LOCAL_XML_LOG.ordinal()].equals("1"))
          flagServerXMLlog = true;
      else
          flagServerXMLlog = false;

      ///Use Server TEXT-PLAIN Logger or Not
      if (initParameters[jTranServerInitParameters.LOCAL_TXT_LOG.ordinal()].equals("1"))
          flagServerTXTlog = true;
      else
          flagServerTXTlog = false;
      
      if (flagServerXMLlog){
          ///Create Log Writers - Server
          loggerServer = new Logger();
          logWriterServer = new LogWriter();
          logWriterServer.setLogger(loggerServer, "JTRANSERVER TOPUP SERVER");
          
          /*Comcel*/
          loggerGateway_Comcel = new Logger(); 
          logWriterGateway_Comcel = new LogWriter();
          logWriterGateway_Comcel.setLogger(loggerGateway_Comcel, "COMCEL ISOMUX");

          /*Tigo*/
          loggerGateway_Tigo = new Logger(); 
          logWriterGateway_Tigo = new LogWriter();
          logWriterGateway_Tigo.setLogger(loggerGateway_Tigo, "TIGO ISOMUX");

          /*Movilred*/
          loggerGateway_Movilred = new Logger(); 
          logWriterGateway_Movilred = new LogWriter();
          logWriterGateway_Movilred.setLogger(loggerGateway_Movilred, "MOVILRED ISOMUX");
          
      }
               
      ///Check if the log folder is created
      if (flagServerXMLlog){
          File folderLogs=new File(jTranServerFixedData.LOGS_PATH); 
          folderLogs.mkdir();
          folderLogs = null;

          folderLogs=new File(jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH); 
          folderLogs.mkdir();
          folderLogs = null;
          
          ///Create Logger Files - Server
          loggerServer.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Server_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          
          ///Create Logger Files - Formatters          
          //loggerGateway_Avantel.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Avantel_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          loggerGateway_Comcel.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Comcel_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          loggerGateway_Tigo.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Tigo_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          //loggerGateway_UffMovil.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_UffMovil_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          loggerGateway_Movilred.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Movilred_" + formatlogger.format(new java.util.Date()) + ".xml") ));          
          
      }    

      //Init Server Channel - Base64
      jTranServerChannel = new BASE24TCPChannel_Movilred(new BASE24Packager());
      jTranServer = new ISOServer_Conred(Integer.valueOf(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]), jTranServerChannel, new ThreadPool(Integer.parseInt(initParameters[jTranServerInitParameters.MIN_THREAD_POOL.ordinal()]),Integer.parseInt(initParameters[jTranServerInitParameters.MAX_THREAD_POOL.ordinal()])), Integer.valueOf(initParameters[jTranServerInitParameters.TOTALTIMEOUT_JTRANSERVER.ordinal()]));
      ///Set Base64 Header
      ((BaseChannel)jTranServerChannel).setHeader( BASE64_HEADER );  
      
      if (flagServerXMLlog)
          jTranServer.setLogger (loggerServer, "JTRANSERVER CONRED SERVER");
        
      jTranServer.addISORequestListener (new jTranServer_Conred());
      
      //Start Server
      new Thread (jTranServer).start ();                   
      
      //Start Cron Loggers
      if (flagServerXMLlog)
          new DaemonLogger(loggerServer, loggerGateway_Comcel, loggerGateway_Tigo, loggerGateway_Movilred);
      
      if (flagServerTXTlog){
          File folderLogs=new File(jTranServerFixedData.LOGS_PATH); 
          folderLogs.mkdir();
          folderLogs = null;
          
          folderLogs=new File(jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.TXT_LOG_PATH); 
          folderLogs.mkdir();
          folderLogs = null;          
          
          //Create Log4j Logger
          org.apache.log4j.PropertyConfigurator.configure(jTranServerFixedData.JTRANSERVER_CONFIG_FILE);
          logMessageID("Servidor jTranServer " + jTranServerFixedData.JTRANSERVER_VERSION + " Iniciado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
          logMessageID("Puerto TCP de Atención Servidor " + initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()], Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
      }      
      
      //Init DBPool
      conDBPool = new ConnectionPool("jTranServerDBPool", Integer.parseInt(initParameters[jTranServerInitParameters.MIN_THREAD_POOL.ordinal()]), Integer.parseInt(initParameters[jTranServerInitParameters.MAX_THREAD_POOL.ordinal()]), 0/*Max. Connections*/, jTranServerFixedData.DBPOOL_IDLE_TIMEOUT/*IDLE Time in Seconds*/, propObj.getProperty(jTranServerFixedData.DATABASE_URL_KEY), propObj.getProperty(jTranServerFixedData.DATABASE_USER_KEY), propObj.getProperty(jTranServerFixedData.DATABASE_PASSWORD_KEY));
      
      //Start DBPool
      try {
          conDBPool.setCaching(false, false, false);
          conDBPool.init(Integer.parseInt(initParameters[jTranServerInitParameters.MIN_THREAD_POOL.ordinal()]));
      }catch(Exception e){ }      
      
      //Init Comcel Formatter
      channelISOMux_Comcel = new NACChannel(initParameters[jTranServerInitParameters.IP_COMCEL_FORMATTER.ordinal()], Integer.valueOf(initParameters[jTranServerInitParameters.PORT_COMCEL_FORMATTER.ordinal()]), new ISO87B_ConredPackager(), null);
      gatewayISOMux_Comcel = new ISOMUX (channelISOMux_Comcel);
      gatewayISOMux_Comcel.setName(initParameters[jTranServerInitParameters.ISOMUX_NAME_COMCEL_FORMATTER.ordinal()]);
      totalTimeout_Comcel  = Integer.valueOf(initParameters[jTranServerInitParameters.TOTALTIMEOUT_COMCEL_FORMATTER.ordinal()]);
              
      if (flagServerXMLlog)
          ((BaseChannel)channelISOMux_Comcel).setLogger (loggerGateway_Comcel, "COMCEL ISOMUX");
      
      //Start ISOMux Comcel
      new Thread (gatewayISOMux_Comcel).start();

      //Init Tigo Formatter
      channelISOMux_Tigo = new NACChannel(initParameters[jTranServerInitParameters.IP_TIGO_FORMATTER.ordinal()], Integer.valueOf(initParameters[jTranServerInitParameters.PORT_TIGO_FORMATTER.ordinal()]), new ISO87B_ConredPackager(), null);
      gatewayISOMux_Tigo = new ISOMUX (channelISOMux_Tigo);
      gatewayISOMux_Tigo.setName(initParameters[jTranServerInitParameters.ISOMUX_NAME_TIGO_FORMATTER.ordinal()]);
      totalTimeout_Tigo  = Integer.valueOf(initParameters[jTranServerInitParameters.TOTALTIMEOUT_TIGO_FORMATTER.ordinal()]);
              
      if (flagServerXMLlog)
          ((BaseChannel)channelISOMux_Tigo).setLogger (loggerGateway_Tigo, "TIGO ISOMUX");
      
      //Start ISOMux Tigo
      new Thread (gatewayISOMux_Tigo).start();      
      
      //Init Movilred Formatter
      channelISOMux_Movilred = new BASE24TCPChannel_Movilred(initParameters[jTranServerInitParameters.IP_MOVILRED_FORMATTER.ordinal()], Integer.valueOf(initParameters[jTranServerInitParameters.PORT_MOVILRED_FORMATTER.ordinal()]), new BASE24Packager());
      gatewayISOMux_Movilred = new ISOMUX (channelISOMux_Movilred){
                                                        /*Set unique key for every Message*/
                                                        protected String getKey (ISOMsg m) {
                                                            String retKey = "";
                                                            try{
                                                                ///Outgoing / Incoming, Admin. Messages
                                                                if (m.getMTI().equals(jTranServerFixedData.ADMIN_MSG_TYPE_REQ) 
                                                                        || m.getMTI().equals(jTranServerFixedData.ADMIN_MSG_TYPE_RESP)){
                                                                    retKey = m.getString(ISOFields.STAN.ordinal());
                                                                }
                                                                else {
                                                                    ///Sale/Financial Messages
                                                                    retKey = m.getString(ISOFields.RRN.ordinal());
                                                                }
                                                            }catch(Exception ex){}                                  
                                                            return retKey;
                                                        }
                                                      };
      ///Set Base64 Header
      ((BaseChannel)channelISOMux_Movilred).setHeader( BASE64_HEADER );
      gatewayISOMux_Movilred.setName(initParameters[jTranServerInitParameters.ISOMUX_NAME_MOVILRED_FORMATTER.ordinal()]);
      totalTimeout_Movilred  = Integer.valueOf(initParameters[jTranServerInitParameters.TOTALTIMEOUT_MOVILRED_FORMATTER.ordinal()]);
              
      if (flagServerXMLlog)
          ((BaseChannel)channelISOMux_Movilred).setLogger (loggerGateway_Movilred, "MOVILRED ISOMUX");
      
      //Start ISOMux Movilred
      new Thread (gatewayISOMux_Movilred).start();
      
      ///Sleep Some Time
      Thread.sleep(1000);
      
      //Verify ISOMux Status
      if(flagServerTXTlog){
          
          if (gatewayISOMux_Comcel.isConnected())
              logMessageID("ISOMux Comcel Conectado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
          else
              logMessageID("ISOMux Comcel NO Conectado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
          
          if (gatewayISOMux_Tigo.isConnected())
              logMessageID("ISOMux Tigo Conectado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
          else
              logMessageID("ISOMux Tigo NO Conectado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));
          
          if (gatewayISOMux_Movilred.isConnected())
              logMessageID("ISOMux Movilred Conectado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));          
          else
              logMessageID("ISOMux Movilred NO Conectado", Integer.parseInt(initParameters[jTranServerInitParameters.LOCAL_PORT.ordinal()]));          
      }
      
  }

protected class Process implements Runnable {
    
    ISOSource source;
    ISOMsg m;
    
    Process (ISOSource source, ISOMsg m) {
        super();
        this.source = source;
        this.m = m;
    }
    
    private int processAdminMessageMovilred(ISOMsg m){
        long startProcessingTime          = 0;
        long endProcessingTime            = 0;
        int totalProcessingTime           = 0;
        preProcessTrxAdmin spPreprocess   = new preProcessTrxAdmin();
        String paramsPreprocess[]         = new String[preProcessTrxAdminParameters.END_FIELD.ordinal()];
        postProcessTrxAdmin spPostprocess = new postProcessTrxAdmin();
        String paramsPostprocess[]        = new String[postProcessTrxAdminParameters.END_FIELD.ordinal()];      
        ISORequest request                = null;
        ISOMsg responseMsg                = null;
        ISOMsg sendMsg                    = null;
        statusAdmin sAdmin                = new statusAdmin();

        ///Start Time Counter
        startProcessingTime = System.currentTimeMillis();

        try{
            //Set Parameters - Call SP
            Utils.packSPParamsPreprocessTrxAdmin(paramsPreprocess, m, jTranServerFixedData.ADMIN_MSG_CONRED_TO_MOVILRED);

            //Call SP Preprocess
            spPreprocess.preProccesTrx(paramsPreprocess, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()));

            logMessageID("Trx. ID " + paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()] + " ISO " + paramsPreprocess[preProcessTrxAdminParameters.MSG_TYPE_REQ.ordinal()] + " STAN " + paramsPreprocess[preProcessTrxAdminParameters.STAN.ordinal()], m.getString(ISOFields.STAN.ordinal()));

            //Eval Output Params                  
            if (Integer.parseInt(paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()]) > jTranServerFixedData.TRANSMIT_TRX_REJECTED_INT){
                  //DB OK, Route Trx. to Movilred
                  sendMsg = (ISOMsg)m.clone();
                  int channelID = Integer.parseInt( m.getString(ISOFields.PROCCESING_CODE.ordinal()).substring(4) );

                  if (gatewayISOMux_Movilred.isConnected()){
                      ///Send & Wait For Response
                      request = new ISORequest(sendMsg);             
                      gatewayISOMux_Movilred.queue(request);

                      logMessageID("Mensaje Administrativo Enviado a Formateador Movilred. Trx. ID: " + paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()] + " - STAN: " + paramsPreprocess[preProcessTrxAdminParameters.STAN.ordinal()], m.getString(ISOFields.STAN.ordinal()));

                      responseMsg = request.getResponse(totalTimeout_Movilred);

                      if (responseMsg!=null){
                          if (responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()).equals(jTranServerFixedData.RSP_CODE_OK)){
                              logMessageID("Mensaje Administrativo Autorizado por Movilred", m.getString(ISOFields.STAN.ordinal()));
                              m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_OK);

                              //Check for Admin. Message Response
                              if (responseMsg.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.LOGOFF_CODE)){
                                  sAdmin.setConnectionStatus(jTranServerFixedData.LOGOFF, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()), jTranServerFixedData.CHANNEL_MOVILRED, channelID);
                              } else if (responseMsg.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.LOGON_CODE)){
                                  sAdmin.setConnectionStatus(jTranServerFixedData.LOGON, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()), jTranServerFixedData.CHANNEL_MOVILRED, channelID);
                              }

                          } else {
                              logMessageID("Mensaje Administrativo NO Autorizado por Movilred. Rsp. Code: " + responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()), m.getString(ISOFields.STAN.ordinal()));
                              m.set(ISOFields.RESPONSE_CODE.ordinal(), responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()));
                          }                        
                      } else {
                          logMessageID("Timeout alcanzado Formateador Movilred", m.getString(ISOFields.STAN.ordinal()));
                      }
                  } else {
                      logMessageID("Formateador Movilred NO Conectado", m.getString(ISOFields.STAN.ordinal()));

                      m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);                    

                  }                            
            } else {
                //DB ERROR, 
                logMessageID("Error en BD - Transacción Administrativa", m.getString(ISOFields.STAN.ordinal()));

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

            }          

            ///Set Message Type
            m.setResponseMTI();          

            //Set Parameters - Call SP
            Utils.packSPParamsPostprocessTrxAdmin(paramsPostprocess, m, paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()].trim());

            //Call SP PostProcess
            spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()));          

        }catch(Exception e){
            logMessageID("Error Formato de Campos Mensaje Administrativo", m.getString(ISOFields.STAN.ordinal()));
        }

        ///Get Proccesing Time
        endProcessingTime = System.currentTimeMillis();                    

        totalProcessingTime = (int)(endProcessingTime-startProcessingTime);

        logMessageID("Mensaje Administrativo Procesado en " + totalProcessingTime + " Miliseg", m.getString(ISOFields.STAN.ordinal()));

        return totalProcessingTime;

    }  

    private int processAdminMessageGeneral(ISOMsg m){
        long startProcessingTime          = 0;
        long endProcessingTime            = 0;
        int totalProcessingTime           = 0;
        preProcessTrxAdmin spPreprocess   = new preProcessTrxAdmin();
        String paramsPreprocess[]         = new String[preProcessTrxAdminParameters.END_FIELD.ordinal()];
        postProcessTrxAdmin spPostprocess = new postProcessTrxAdmin();
        String paramsPostprocess[]        = new String[postProcessTrxAdminParameters.END_FIELD.ordinal()];
        String tipoTrxAdmin               = "";
        statusAdmin sAdmin                = new statusAdmin();      

        ///Start Time Counter
        startProcessingTime = System.currentTimeMillis();

        try{

            if (m.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.LOGON_CODE)){
                tipoTrxAdmin = "LOGON";
                logMessageID("Transacción de " + tipoTrxAdmin + " Recibida", m.getString(ISOFields.STAN.ordinal()));
            } else if (m.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.LOGOFF_CODE)){
                tipoTrxAdmin = "LOGOFF";
                logMessageID("Transacción de " + tipoTrxAdmin + " Recibida", m.getString(ISOFields.STAN.ordinal()));
            } else if (m.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.ECHO_TEST_CODE)){
                tipoTrxAdmin = "ECHO TEST";
                logMessageID("Transacción de " + tipoTrxAdmin + " Recibida", m.getString(ISOFields.STAN.ordinal()));
            }

            //Set Parameters - Call SP
            Utils.packSPParamsPreprocessTrxAdmin(paramsPreprocess, m, jTranServerFixedData.ADMIN_MSG_JNTS_TO_CONRED);

            //Call SP Preprocess
            spPreprocess.preProccesTrx(paramsPreprocess, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()));

            logMessageID("Trx. ID " + paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()] + " ISO " + paramsPreprocess[preProcessTrxAdminParameters.MSG_TYPE_REQ.ordinal()] + " STAN " + paramsPreprocess[preProcessTrxAdminParameters.STAN.ordinal()], m.getString(ISOFields.STAN.ordinal()));

            //Eval Output Params                  
            if (Integer.parseInt(paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()]) > jTranServerFixedData.TRANSMIT_TRX_REJECTED_INT){
                  //DB OK

                logMessageID("Transacción de " + tipoTrxAdmin + " Autorizada", m.getString(ISOFields.STAN.ordinal()));

                if (m.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.LOGOFF_CODE)){
                    sAdmin.setConnectionStatus(jTranServerFixedData.LOGOFF, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()), jTranServerFixedData.CHANNEL_CONRED, 0);
                } else if (m.getString(ISOFields.FIELD70.ordinal()).equals(jTranServerFixedData.LOGON_CODE)){
                    sAdmin.setConnectionStatus(jTranServerFixedData.LOGON, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()), jTranServerFixedData.CHANNEL_CONRED, 0);
                }              

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_OK);              

            } else {
                //DB ERROR, 
                logMessageID("Error en BD - Transacción de " + tipoTrxAdmin, m.getString(ISOFields.STAN.ordinal()));

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

            }          

            ///Set Message Type
            m.setResponseMTI();          

            //Set Parameters - Call SP
            Utils.packSPParamsPostprocessTrxAdmin(paramsPostprocess, m, paramsPreprocess[preProcessTrxAdminParameters.TRX_ID.ordinal()].trim());

            //Call SP PostProcess
            spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.STAN.ordinal()));          

        }catch(Exception e){
            logMessageID("Error Formato de Campos Mensaje Administrativo de " + tipoTrxAdmin, m.getString(ISOFields.STAN.ordinal()));
        }

        ///Get Proccesing Time
        endProcessingTime = System.currentTimeMillis();                    

        totalProcessingTime = (int)(endProcessingTime-startProcessingTime);

        logMessageID("Mensaje de " + tipoTrxAdmin + " Procesado en " + totalProcessingTime + " Miliseg", m.getString(ISOFields.STAN.ordinal()));

        return totalProcessingTime;

    } 

    private int processTopupSale(ISOMsg m){
        SimpleDateFormat datetimeRsp  = new SimpleDateFormat("ddMMHHmmss");
        ISORequest request            = null;
        ISOMsg responseMsg            = null;
        ISOMsg sendMsg                = new ISOMsg();
        String isomuxName             = "";
        long startProcessingTime      = 0;
        long endProcessingTime        = 0;
        int totalProcessingTime       = 0;
        boolean retransmitOperator    = false;
        boolean ISOMuxConnected       = false;
        preProcess spPreprocess       = new preProcess();
        String paramsPreprocess[]     = new String[preProcessParameters.END_FIELD.ordinal()];
        postProcess spPostprocess     = new postProcess();
        String paramsPostprocess[]    = new String[postProcessParameters.END_FIELD.ordinal()];
        String RRN_DB                 = "";
        String codErrorStatus         = "";

        try{

            ///Start Time Counter
            startProcessingTime = System.currentTimeMillis();

            try{

                ///Pack Send Message Data
                sendMsg = (ISOMsg)m.clone();

                //Set Parameters - Call SP
                Utils.packSPParamsPreprocess(paramsPreprocess, m);

                //Call SP Preprocess
                spPreprocess.preProccesTrx(paramsPreprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                RRN_DB = paramsPreprocess[preProcessParameters.TRX_ID.ordinal()].trim();

                logMessageID("Trx. ID " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + " ISO " + paramsPreprocess[preProcessParameters.MSG_TYPE_REQ.ordinal()] + " Proc. Code " + paramsPreprocess[preProcessParameters.PROC_CODE.ordinal()] + " Com. " + paramsPreprocess[preProcessParameters.MERCHANT_ID.ordinal()] + " Ter. " + paramsPreprocess[preProcessParameters.TERMINAL_ID.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                //Eval Output Params                  
                if (paramsPreprocess[preProcessParameters.ROUTE_TRX.ordinal()].equals(jTranServerFixedData.TRANSMIT_TRX_OK)
                        && paramsPreprocess[preProcessParameters.RSP_CODE.ordinal()].equals(jTranServerFixedData.JTRANSERVER_TRX_OK)){
                    //DB OK, Transmit Trx. To Operator
                    retransmitOperator = true;

                    //Get ISOMUx Name, where to route the topup transaction?
                    isomuxName = paramsPreprocess[preProcessParameters.ISOMUX_NAME.ordinal()];

                    //Set Status Code
                    codErrorStatus = paramsPreprocess[preProcessParameters.RSP_CODE.ordinal()];

                } else {
                    //DB Error, Return response to Client
                    retransmitOperator = false;

                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

                    ///Pack MSG Error, Set Error Code
                    codErrorStatus = paramsPreprocess[preProcessParameters.RSP_CODE.ordinal()];
                }
            }catch(Exception e){
                ///Format Error
                retransmitOperator = false;

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

                ///Pack MSG Error, Set Error Code
                codErrorStatus = jTranServerFixedData.JTRANSERVER_ERR_MSG_OUT;
            }
            
            if (retransmitOperator == true){

                if ( isomuxName.trim().toLowerCase().equals(gatewayISOMux_Comcel.getName().trim().toLowerCase()) ){
                    if (gatewayISOMux_Comcel.isConnected()){
                        ISOMuxConnected = true;
                        ///Send & Wait For Response
                        request = new ISORequest(sendMsg);
                        gatewayISOMux_Comcel.queue(request);

                        logMessageID("Venta de Recarga Enviada a Formateador Comcel. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                        responseMsg = request.getResponse(totalTimeout_Comcel);
                    } else {
                        //Iso Mux Not Connected
                        ISOMuxConnected = false;

                        logMessageID("Formateador Comcel NO Conectado", m.getString(ISOFields.RRN.ordinal()));

                    }
                } else if ( isomuxName.trim().toLowerCase().equals(gatewayISOMux_Movilred.getName().trim().toLowerCase()) ){
                    if (gatewayISOMux_Movilred.isConnected()){                  
                        ///Send & Wait For Response
                        request = new ISORequest(sendMsg);             
                        gatewayISOMux_Movilred.queue(request);

                        logMessageID("Venta de Recarga Enviada a Formateador Movilred. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                        responseMsg = request.getResponse(totalTimeout_Movilred);
                    } else {
                        //Iso Mux Not Connected
                        ISOMuxConnected = false;

                        logMessageID("Formateador Movilred NO Conectado", m.getString(ISOFields.RRN.ordinal()));

                    }
                } else if ( isomuxName.trim().toLowerCase().equals(gatewayISOMux_Tigo.getName().trim().toLowerCase()) ){
                    if (gatewayISOMux_Tigo.isConnected()){                  
                        ///Send & Wait For Response
                        request = new ISORequest(sendMsg);             
                        gatewayISOMux_Tigo.queue(request);

                        logMessageID("Venta de Recarga Enviada a Formateador Tigo. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                        responseMsg = request.getResponse(totalTimeout_Tigo);
                    } else {
                        //Iso Mux Not Connected
                        ISOMuxConnected = false;

                        logMessageID("Formateador Tigo NO Conectado", m.getString(ISOFields.RRN.ordinal()));

                    }
                }

                ///Check for Response
                if (responseMsg != null) {
                    //Get Response Data
                    
                    if (responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()).equals(jTranServerFixedData.RSP_CODE_OK)){
                        ///TopUp Authorized
                        logMessageID("Recarga Autorizada. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()] + ", Auth. ID_P38: " + responseMsg.getString(ISOFields.AUTH_ID.ordinal()) + ", Auth. ID Operador: " + responseMsg.getString(ISOFields.FIELD63.ordinal()).substring(54, 74), m.getString(ISOFields.RRN.ordinal()));
                    } else {
                        ///TopUp Not Authorized
                        logMessageID("Recarga NO Autorizada. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));
                    }
                    
                    //Set Parameters - Call SP
                    Utils.packSPParamsPostprocess(paramsPostprocess, m, responseMsg, responseMsg.getString(ISOFields.FIELD59.ordinal()), RRN_DB);

                    //Remove Field 59
                    if (responseMsg.hasField(ISOFields.FIELD59.ordinal()))
                        responseMsg.unset(ISOFields.FIELD59.ordinal());
                    
                    //Call SP PostProcess
                    spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                    //Set Response Mesg. Fields
                    m.setMTI(responseMsg.getMTI());
                    m.set(ISOFields.DATETIME.ordinal(), responseMsg.getString(ISOFields.DATETIME.ordinal()));
                    m.set(ISOFields.TIME.ordinal(), responseMsg.getString(ISOFields.TIME.ordinal()));
                    m.set(ISOFields.AUTH_ID.ordinal(), responseMsg.getString(ISOFields.AUTH_ID.ordinal()));
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()));
                    
                } else {
                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);

                    if (ISOMuxConnected == true){
                        //Set Parameters - Call SP
                        Utils.packSPParamsPostprocess(paramsPostprocess, m, sendMsg, jTranServerFixedData.JTRANSERVER_ERR_TIMEOUT, RRN_DB);

                        logMessageID("Tiempo de Espera Agotado. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                    } else {                     
                        //Set Parameters - Call SP
                        Utils.packSPParamsPostprocess(paramsPostprocess, m, sendMsg, jTranServerFixedData.JTRANSERVER_ERR_CONNECT, RRN_DB);
                    }
                    
                    //Call SP PostProcess
                    spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                }
            } else {
                //Local Error

                logMessageID("Transacción NO Transmitida a Autorizador. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Celular: " + paramsPreprocess[preProcessParameters.REF_CUSTOMER_1.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()] + ", Cód. Estado: " + codErrorStatus, m.getString(ISOFields.RRN.ordinal()));

                if (!codErrorStatus.equals(jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT)){
                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

                    //Set Parameters - Call SP
                    Utils.packSPParamsErrorPostprocess(paramsPostprocess, RRN_DB, codErrorStatus);

                    //Call SP PostProcess
                    spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                } else {
                    //DB Connecting Error
                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);
                }
            }      
        }catch(Exception e){
            ///General Error
            try{

                logMessageID(jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT, m.getString(ISOFields.RRN.ordinal()));

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);

            }catch(Exception ex){ }
        }

        try{            
            if (retransmitOperator == false){
                ///Set Response Message Type
                m.setResponseMTI();          

                ///Set Response Trx. DateTime
                m.set(ISOFields.DATETIME.ordinal(), datetimeRsp.format(new java.util.Date()) );            
            }
        }catch(Exception e){ }

        ///Get Proccesing Time
        endProcessingTime = System.currentTimeMillis();                    

        totalProcessingTime = (int)(endProcessingTime-startProcessingTime);

        logMessageID("Venta de Recarga Procesada en " + totalProcessingTime + " Miliseg", m.getString(ISOFields.RRN.ordinal()));

        return totalProcessingTime;

    }     
    
    private int processBillPayment(ISOMsg m){
        SimpleDateFormat datetimeRsp  = new SimpleDateFormat("ddMMHHmmss");
        ISORequest request            = null;
        ISOMsg responseMsg            = null;
        ISOMsg sendMsg                = new ISOMsg();
        String isomuxName             = "";
        long startProcessingTime      = 0;
        long endProcessingTime        = 0;
        int totalProcessingTime       = 0;
        boolean retransmitOperator    = false;
        boolean ISOMuxConnected       = false;
        preProcess spPreprocess       = new preProcess();
        String paramsPreprocess[]     = new String[preProcessParameters.END_FIELD.ordinal()];
        postProcess spPostprocess     = new postProcess();
        String paramsPostprocess[]    = new String[postProcessParameters.END_FIELD.ordinal()];
        String RRN_DB                 = "";
        String codErrorStatus         = "";

        try{

            ///Start Time Counter
            startProcessingTime = System.currentTimeMillis();

            try{

                ///Pack Send Message Data
                sendMsg = (ISOMsg)m.clone();

                //Set Parameters - Call SP
                Utils.packSPParamsPreprocess(paramsPreprocess, m);

                //Call SP Preprocess
                spPreprocess.preProccesTrx(paramsPreprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                RRN_DB = paramsPreprocess[preProcessParameters.TRX_ID.ordinal()].trim();

                logMessageID("Trx. ID " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + " ISO " + paramsPreprocess[preProcessParameters.MSG_TYPE_REQ.ordinal()] + " Proc. Code " + paramsPreprocess[preProcessParameters.PROC_CODE.ordinal()] + " Com. " + paramsPreprocess[preProcessParameters.MERCHANT_ID.ordinal()] + " Ter. " + paramsPreprocess[preProcessParameters.TERMINAL_ID.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                //Eval Output Params                  
                if (paramsPreprocess[preProcessParameters.ROUTE_TRX.ordinal()].equals(jTranServerFixedData.TRANSMIT_TRX_OK)
                        && paramsPreprocess[preProcessParameters.RSP_CODE.ordinal()].equals(jTranServerFixedData.JTRANSERVER_TRX_OK)){
                    //DB OK, Transmit Trx. To Movilred
                    retransmitOperator = true;

                    //Get ISOMUx Name, where to route the Bill Payment transaction?
                    isomuxName = paramsPreprocess[preProcessParameters.ISOMUX_NAME.ordinal()];

                    //Set Status Code
                    codErrorStatus = paramsPreprocess[preProcessParameters.RSP_CODE.ordinal()];

                } else {
                    //DB Error, Return response to Client
                    retransmitOperator = false;

                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

                    ///Pack MSG Error, Set Error Code
                    codErrorStatus = paramsPreprocess[preProcessParameters.RSP_CODE.ordinal()];
                }
            }catch(Exception e){
                ///Format Error
                retransmitOperator = false;

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

                ///Pack MSG Error, Set Error Code
                codErrorStatus = jTranServerFixedData.JTRANSERVER_ERR_MSG_OUT;
            }
            
            if (retransmitOperator == true){

                if ( isomuxName.trim().toLowerCase().equals(gatewayISOMux_Movilred.getName().trim().toLowerCase()) ){
                    if (gatewayISOMux_Movilred.isConnected()){
                        
                        //Remove Field 59                       
                        sendMsg.unset(ISOFields.FIELD59.ordinal());                        
                        
                        ///Send & Wait For Response
                        request = new ISORequest(sendMsg);             
                        gatewayISOMux_Movilred.queue(request);

                        logMessageID("Pago de Factura Enviado a Formateador Movilred. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Cód. de Barras: " + paramsPreprocess[preProcessParameters.ADD_DATA.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                        responseMsg = request.getResponse(totalTimeout_Movilred);
                    } else {
                        //Iso Mux Not Connected
                        ISOMuxConnected = false;

                        logMessageID("Formateador Movilred NO Conectado", m.getString(ISOFields.RRN.ordinal()));

                    }
                }
                
                ///Check for Response
                if (responseMsg != null) {
                    //Get Response Data
                    
                    if (responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()).equals(jTranServerFixedData.RSP_CODE_OK)){
                        ///Bill Payment Authorized
                        logMessageID("Pago de Factura Autorizado. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Cód. de Barras: " + paramsPreprocess[preProcessParameters.ADD_DATA.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()] + ", Auth. ID_P38: " + responseMsg.getString(ISOFields.AUTH_ID.ordinal()), m.getString(ISOFields.RRN.ordinal()));
                    } else {
                        ///Bill Payment Not Authorized
                        logMessageID("Pago de Factura NO Autorizado. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Cód. de Barras: " + paramsPreprocess[preProcessParameters.ADD_DATA.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));
                    }
                    
                    //Set Parameters - Call SP
                    Utils.packSPParamsPostprocess(paramsPostprocess, m, responseMsg, responseMsg.getString(ISOFields.FIELD59.ordinal()), RRN_DB);

                    //Remove Field 59
                    if (responseMsg.hasField(ISOFields.FIELD59.ordinal()))
                        responseMsg.unset(ISOFields.FIELD59.ordinal());
                    
                    //Call SP PostProcess
                    spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                    //Set Response Mesg. Fields
                    m.setMTI(responseMsg.getMTI());
                    m.set(ISOFields.DATETIME.ordinal(), responseMsg.getString(ISOFields.DATETIME.ordinal()));
                    m.set(ISOFields.TIME.ordinal(), responseMsg.getString(ISOFields.TIME.ordinal()));
                    m.set(ISOFields.AUTH_ID.ordinal(), responseMsg.getString(ISOFields.AUTH_ID.ordinal()));
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), responseMsg.getString(ISOFields.RESPONSE_CODE.ordinal()));
                    
                } else {
                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);

                    if (ISOMuxConnected == true){
                        //Set Parameters - Call SP
                        Utils.packSPParamsPostprocess(paramsPostprocess, m, sendMsg, jTranServerFixedData.JTRANSERVER_ERR_TIMEOUT, RRN_DB);

                        logMessageID("Tiempo de Espera Agotado. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Cód. de Barras: " + paramsPreprocess[preProcessParameters.ADD_DATA.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()], m.getString(ISOFields.RRN.ordinal()));

                    } else {                     
                        //Set Parameters - Call SP
                        Utils.packSPParamsPostprocess(paramsPostprocess, m, sendMsg, jTranServerFixedData.JTRANSERVER_ERR_CONNECT, RRN_DB);
                    }
                    
                    //Call SP PostProcess
                    spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                }
            } else {
                //Local Error

                logMessageID("Transacción NO Transmitida a Autorizador. Trx. ID: " + paramsPreprocess[preProcessParameters.TRX_ID.ordinal()] + ", Cód. de Barras: " + paramsPreprocess[preProcessParameters.ADD_DATA.ordinal()] + ", Monto:" + paramsPreprocess[preProcessParameters.AMOUNT.ordinal()] + ", Cód. Estado: " + codErrorStatus, m.getString(ISOFields.RRN.ordinal()));

                if (!codErrorStatus.equals(jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT)){
                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);              

                    //Set Parameters - Call SP
                    Utils.packSPParamsErrorPostprocess(paramsPostprocess, RRN_DB, codErrorStatus);

                    //Call SP PostProcess
                    spPostprocess.postProccesTrx(paramsPostprocess, conDBPool, logjTranServer, m.getString(ISOFields.RRN.ordinal()));

                } else {
                    //DB Connecting Error
                    ///Set Rsp. Code
                    m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);
                }
            }      
        }catch(Exception e){
            ///General Error
            try{

                logMessageID(jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT, m.getString(ISOFields.RRN.ordinal()));

                ///Set Rsp. Code
                m.set(ISOFields.RESPONSE_CODE.ordinal(), jTranServerFixedData.RSP_CODE_ERROR);

            }catch(Exception ex){ }
        }

        try{            
            if (retransmitOperator == false){
                ///Set Response Message Type
                m.setResponseMTI();          

                ///Set Response Trx. DateTime
                m.set(ISOFields.DATETIME.ordinal(), datetimeRsp.format(new java.util.Date()) );            
            }
        }catch(Exception e){ }

        ///Get Proccesing Time
        endProcessingTime = System.currentTimeMillis();                    

        totalProcessingTime = (int)(endProcessingTime-startProcessingTime);

        logMessageID("Pago de Factura Procesado en " + totalProcessingTime + " Miliseg", m.getString(ISOFields.RRN.ordinal()));

        return totalProcessingTime;

    }         
    
    public void run () {
        String ipSource   = "0.0.0.0";
        int portLocal     = 0;

        try{
            ///Get Connection Data
            ipSource  = String.valueOf(((BaseChannel)m.getSource()).getSocket().getInetAddress().getHostAddress());
            portLocal = ((BaseChannel)m.getSource()).getSocket().getPort();
        } catch(Exception e){ }

        try{

            ///Log TCP Connection Data
            logMessageID("Mensaje Recibido del Host JNTS - " + ipSource + ":" + portLocal + " [ " + Utils.separateHEXData(ISOUtil.zeropad(Integer.toHexString( m.getHeader().length + m.pack().length + 2 ).toUpperCase(), 4)) + new String(m.getHeader()) + new String(m.pack()) + "]", (m.hasField(ISOFields.RRN.ordinal())) ? m.getString(ISOFields.RRN.ordinal()) : m.getString(ISOFields.STAN.ordinal()));            

            ///Log DB Pool Resources Info
            logMessageID("DBPool Info: In Use=" + conDBPool.getCheckedOut() + ", Free=" + conDBPool.getFreeCount() + ", Total Size: " + conDBPool.getSize(), (m.hasField(ISOFields.RRN.ordinal())) ? m.getString(ISOFields.RRN.ordinal()) : m.getString(ISOFields.STAN.ordinal()));

            ///Log ISOServer Resources Info
            logMessageID("ISOServer Info: Threads Available=" + jTranServer.getPoolSize() + ", Max. Threads=" + jTranServer.getMaxPoolSize() + ", Current Connections=" + (jTranServer.getMaxPoolSize()-jTranServer.getPoolSize()) , (m.hasField(ISOFields.RRN.ordinal())) ? m.getString(ISOFields.RRN.ordinal()) : m.getString(ISOFields.STAN.ordinal()));
            
            //Admin. Messages
            if (m.getMTI().equals(jTranServerFixedData.ADMIN_MSG_TYPE_REQ)){

                //Check for Own Movilred Admin. Messages
                if (m.hasField(ISOFields.PROCCESING_CODE.ordinal()) && 
                        (m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_MOVILRED) ||
                            m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_MOVILRED1) ||
                            m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_MOVILRED2) ||
                            m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_MOVILRED3))){
                    processAdminMessageMovilred(m);
                } else {
                    //JNTS And Conred Web Admin. Mesages
                    processAdminMessageGeneral(m);
                }

                logMessageID("Enviando Respuesta al Host JNTS - " + ipSource + ":" + portLocal + " [ " + Utils.separateHEXData( ISOUtil.zeropad(Integer.toHexString( m.getHeader().length + m.pack().length + 2 ).toUpperCase(), 4)) + new String(m.getHeader()) + new String(m.pack()) + "]", (m.hasField(ISOFields.RRN.ordinal())) ? m.getString(ISOFields.RRN.ordinal()) : m.getString(ISOFields.STAN.ordinal()));

                ///Send Response to JNTS
                source.send(m);                
                
            } else {
                //Sales/Financial Messages

                if (m.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_REQ) &&
                        m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_TOPUP_SALE)){

                    //TopUp Sales
                    processTopupSale(m);                    
                    
                } else if (m.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_REQ) &&
                        m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_BILL_PAYMENT)){

                    //Bill Payment
                    processBillPayment(m);                    
                    
                }
                
                logMessageID("Enviando Respuesta al Host JNTS - " + ipSource + ":" + portLocal + " [ " + Utils.separateHEXData( ISOUtil.zeropad(Integer.toHexString( m.getHeader().length + m.pack().length + 2 ).toUpperCase(), 4)) + new String(m.getHeader()) + new String(m.pack()) + "]", (m.hasField(ISOFields.RRN.ordinal())) ? m.getString(ISOFields.RRN.ordinal()) : m.getString(ISOFields.STAN.ordinal()));

                //check for Local errors
                if (m.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_REQ))
                    m.setResponseMTI();
                
                ///Send Response to JNTS
                source.send(m);                
                
            }

        } catch( Exception e ){
            //Error Sending Response to Client
            logMessageID("Error Enviando Respuesta al Host JNTS - " + ipSource + ":" + portLocal, (m.hasField(ISOFields.RRN.ordinal())) ? m.getString(ISOFields.RRN.ordinal()) : m.getString(ISOFields.STAN.ordinal()));
        }
    }    
    
}    
  
}
