/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.DAL;

import java.sql.*; 
import java.math.BigDecimal;
import com.conred.enumerations.postProcessTrxAdminParameters;
import com.conred.fixedData.jTranServerFixedData;
import snaq.db.*;
import snaq.util.*;

/**
 *
 * @author Elkin Beltrán
 */
public class postProcessTrxAdmin {

    private String dbConnectError   = "socket write error";
    private String dbTimeoutError   = "la consulta ha superado el tiempo de espera";    
    
    private void logMessageID(String Data, org.apache.log4j.Logger logjTranServer,
            String trxID){
        logjTranServer.info(postProcessTrxAdmin.class.getName() + " [" + trxID + "] : " + Data + ".");
    }    
    
    
    public void postProccesTrx(String[] params, ConnectionPool conDBPool,
            org.apache.log4j.Logger logjTranServer, String trxID){
        Connection con          = null;
        CallableStatement cStmt = null;
        String sqlCallerSP      = "{call " + jTranServerFixedData.POSTPROCESS_TRX_ADMIN_SP_NAME + "(";
        int i                   = 0;
        String sqlTxtLog        = "";
        
        //Try to Connect to DB Server
        try{
            con = conDBPool.getConnection(jTranServerFixedData.DBPOOL_CONNECTION_TIMEOUT);            
        } catch(Exception ex){
            //Connecting Error
            con = null;
        }           
        
        try {
            if (con != null) { 
                //Set the SQL Sentence
                for (i=0; i < postProcessTrxAdminParameters.END_FIELD.ordinal() - 1; i++)
                    sqlCallerSP = sqlCallerSP + "?, ";
                sqlCallerSP += "?)}";
                cStmt = con.prepareCall(sqlCallerSP);

                sqlTxtLog = sqlCallerSP;

                //Set Input Parameters
                cStmt.setString(postProcessTrxAdminParameters.TRX_ID.ordinal() + 1         , params[postProcessTrxAdminParameters.TRX_ID.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessTrxAdminParameters.TRX_ID.ordinal()] + "'");
                cStmt.setString(postProcessTrxAdminParameters.MSG_TYPE_RESP.ordinal() + 1  , params[postProcessTrxAdminParameters.MSG_TYPE_RESP.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessTrxAdminParameters.MSG_TYPE_RESP.ordinal()] + "'");
                cStmt.setString(postProcessTrxAdminParameters.RSP_CODE.ordinal() + 1       , params[postProcessTrxAdminParameters.RSP_CODE.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessTrxAdminParameters.RSP_CODE.ordinal()] + "'");
                
                try {

                    logMessageID(sqlTxtLog, logjTranServer, trxID);
                    
                    //Execute SP
                    cStmt.execute();

                } catch(SQLException sqle){
                    if (sqle.getMessage().toLowerCase().trim().contains(dbConnectError.toLowerCase().trim())){
                        //Handle Error Connecting DB Server
                        //Set OutPut Parameters
                        logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                        
                    }  else if (sqle.getMessage().toLowerCase().trim().contains(dbTimeoutError.toLowerCase().trim())){
                        //Handle Error Timeout
                        //Set OutPut Parameters
                        
                        logMessageID("Tiempo de Espera Agotado", logjTranServer, trxID);
                        
                    } else {
                        //Handle General Error
                        //Set OutPut Parameters
                        
                        logMessageID("Error General Servidor BD", logjTranServer, trxID);
                        
                    }  
                }
            } else {
                //Handle Error Connecting DB Server
                //Set OutPut Parameters                
                logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);                
            }
        } catch(Exception e){
            logMessageID("Error General Servidor BD", logjTranServer, trxID);
        } finally {
            try { 
                if (cStmt != null)
                  cStmt.close();
                cStmt = null;
            } catch (SQLException e) { }

	    try { 
                if (con != null)
                  con.close();
                con = null;
            } catch (SQLException e) { } 
        }
    }
    
}
