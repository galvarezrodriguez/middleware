/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.utils;

//jPOS Imports
import com.conred.enumerations.postProcessParameters;
import com.conred.enumerations.preProcessParameters;
import com.conred.enumerations.preProcessTrxAdminParameters;
import com.conred.enumerations.postProcessTrxAdminParameters;
import com.conred.enumerations.ISOFields;
import org.jpos.iso.*;
import org.jpos.util.*;

//Conred Imports
import com.conred.fixedData.jTranServerFixedData;

/**
 *
 * @author Elkin Beltrán
 */
public class Utils {

    public static String amountFormat(String amount) {
      String amountFormatted = "";
      String[] pieces = new String[5];
      int lenA = amount.length();
      int i = 0;

      if (lenA <= 3) {
          amountFormatted = amount;
      } else {
          while (amount.length() > 3) {
              lenA = amount.length();
              pieces[i] = amount.substring(lenA - 3, lenA);
              amount = amount.substring(0, lenA - 3);
              i++;
          }
          if (amount.length() > 0) {
              pieces[i] = amount;
          }

          ///Compact Everything
          while (i >= 0) {
              if (i != 0) {
                  amountFormatted += pieces[i] + ".";
              } else {
                  amountFormatted += pieces[i];
              }
              i--;
          }
      }
      return amountFormatted;
    }    
    
    public static void packSPParamsPreprocessTrxAdmin(String params[], ISOMsg m, int type){

      try{       

        //Admin. Messages
        if (type == jTranServerFixedData.ADMIN_MSG_JNTS_TO_CONRED){
            params[preProcessTrxAdminParameters.SOURCE_INTERFACE.ordinal()] = "JNTS";
            params[preProcessTrxAdminParameters.DEST_INTERFACE.ordinal()] = "CONRED";  
        } else if (type == jTranServerFixedData.ADMIN_MSG_CONRED_TO_MOVILRED){
            params[preProcessTrxAdminParameters.SOURCE_INTERFACE.ordinal()] = "CONRED";
            params[preProcessTrxAdminParameters.DEST_INTERFACE.ordinal()] = "MOVILRED";            
        }
        
        params[preProcessTrxAdminParameters.MSG_TYPE_REQ.ordinal()] = m.getMTI();
        params[preProcessTrxAdminParameters.FIELD_70.ordinal()] = m.getString(ISOFields.FIELD70.ordinal());
        params[preProcessTrxAdminParameters.STAN.ordinal()] = m.getString(ISOFields.STAN.ordinal());        
        
      } catch(Exception e){ }

    }    
    
    public static void packSPParamsPostprocessTrxAdmin(String params[], ISOMsg m, String trxID){

      try{       

        //Admin. Messages       
        params[postProcessTrxAdminParameters.TRX_ID.ordinal()]           = trxID;
        params[postProcessTrxAdminParameters.MSG_TYPE_RESP.ordinal()]    = m.getMTI();
        params[postProcessTrxAdminParameters.RSP_CODE.ordinal()]         = m.getString(ISOFields.RESPONSE_CODE.ordinal());
        
      } catch(Exception e){ }

    }    
    
    public static void packSPParamsPreprocess(String params[], ISOMsg m){

      try{       

          if (m.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_REQ) && 
                  m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_TOPUP_SALE)){
              //TopUp Sale
              params[preProcessParameters.MSG_TYPE_REQ.ordinal()]   = m.getMTI();
              params[preProcessParameters.ORIGIN_INTERFACE.ordinal()]   = jTranServerFixedData.ORIGIN_INTERFACE_NAME.toUpperCase();
              params[preProcessParameters.PROC_CODE.ordinal()]      = m.getString(ISOFields.PROCCESING_CODE.ordinal());
              params[preProcessParameters.STAN.ordinal()]           = m.getString(ISOFields.STAN.ordinal());
              params[preProcessParameters.RRN.ordinal()]            = m.getString(ISOFields.RRN.ordinal());
              params[preProcessParameters.TERMINAL_ID.ordinal()]    = m.getString(ISOFields.TERMINAL_ID.ordinal()).trim();
              params[preProcessParameters.MERCHANT_ID.ordinal()]    = m.getString(ISOFields.FIELD48.ordinal()).substring(0, 19).trim();
              //EAN CODE             
              params[preProcessParameters.PRODUCT_EAN.ordinal()]    = m.getString(ISOFields.FIELD63.ordinal()).substring(22, 41).trim();
              params[preProcessParameters.REF_CUSTOMER_1.ordinal()] = m.getString(ISOFields.FIELD63.ordinal()).substring(41, 56).trim();
              params[preProcessParameters.ADD_DATA.ordinal()]       = "";
              params[preProcessParameters.AMOUNT.ordinal()]         = String.valueOf(Integer.parseInt(m.getString(ISOFields.AMOUNT.ordinal()).trim()) / 100);
              params[preProcessParameters.IP.ordinal()]             = String.valueOf(((BaseChannel)m.getSource()).getSocket().getInetAddress().getHostAddress());
              params[preProcessParameters.PORT.ordinal()]           = String.valueOf(((BaseChannel)m.getSource()).getSocket().getPort());              

          } else if (m.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_REQ) && 
                  m.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_BILL_PAYMENT)){
              //Bill Payment
              params[preProcessParameters.MSG_TYPE_REQ.ordinal()]   = m.getMTI();
              params[preProcessParameters.ORIGIN_INTERFACE.ordinal()]   = jTranServerFixedData.ORIGIN_INTERFACE_NAME.toUpperCase();
              params[preProcessParameters.PROC_CODE.ordinal()]      = m.getString(ISOFields.PROCCESING_CODE.ordinal());
              params[preProcessParameters.STAN.ordinal()]           = m.getString(ISOFields.STAN.ordinal());
              params[preProcessParameters.RRN.ordinal()]            = m.getString(ISOFields.RRN.ordinal());
              params[preProcessParameters.TERMINAL_ID.ordinal()]    = m.getString(ISOFields.TERMINAL_ID.ordinal()).trim();
              params[preProcessParameters.MERCHANT_ID.ordinal()]    = m.getString(ISOFields.FIELD48.ordinal()).substring(0, 19).trim();
              //EAN CODE             
              params[preProcessParameters.PRODUCT_EAN.ordinal()]    = m.getString(ISOFields.FIELD59.ordinal());
              params[preProcessParameters.REF_CUSTOMER_1.ordinal()] = "";
              //BAR CODE
              params[preProcessParameters.ADD_DATA.ordinal()]       = getBillPaymentToken(m.getString(ISOFields.FIELD63.ordinal())); 
              params[preProcessParameters.AMOUNT.ordinal()]         = String.valueOf(Integer.parseInt(m.getString(ISOFields.AMOUNT.ordinal()).trim()) / 100);
              params[preProcessParameters.IP.ordinal()]             = String.valueOf(((BaseChannel)m.getSource()).getSocket().getInetAddress().getHostAddress());
              params[preProcessParameters.PORT.ordinal()]           = String.valueOf(((BaseChannel)m.getSource()).getSocket().getPort());              

          }          
          
      } catch(Exception e){ }

    }

    public static String getBillPaymentToken(String Data){        
        String retVal = "";
        int lenToken = 0;
        
        lenToken = Integer.parseInt(Data.substring(16, 21));
        
        retVal = Data.substring(22, (22+lenToken));
        
        return retVal;
    }
    
    public static void packSPParamsPostprocess(String params[], ISOMsg req, ISOMsg resp, String msgLocal, String trxID){

      try{

          if (!resp.hasField( ISOFields.RESPONSE_CODE.ordinal() ))
              req.setResponseMTI();
          
          if ((req.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_REQ) ||
                  req.getMTI().equals(jTranServerFixedData.MSG_TYPE_SALE_RESP_OK)) ){ 
              //TopUp Sale
              if (req.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_TOPUP_SALE)){                
                if (resp.hasField(ISOFields.AUTH_ID.ordinal())){
                    params[postProcessParameters.TRX_ID.ordinal()]             = trxID;                    
                    params[postProcessParameters.MSG_TYPE_RESP.ordinal()]      = resp.getMTI();
                    params[postProcessParameters.AUTH_ID.ordinal()]            = resp.getString(ISOFields.AUTH_ID.ordinal());
                    params[postProcessParameters.OPERATOR_AUTH_ID.ordinal()]   = resp.getString(ISOFields.FIELD63.ordinal()).substring(54, 74);
                    params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()] = getMsgStatus(msgLocal, req, resp);
                    params[postProcessParameters.RSP_CODE_ISO.ordinal()]       = (resp.hasField(ISOFields.RESPONSE_CODE.ordinal())) ? resp.getString(ISOFields.RESPONSE_CODE.ordinal()) : req.getString(ISOFields.RESPONSE_CODE.ordinal());
                } else {
                    params[postProcessParameters.TRX_ID.ordinal()]             = trxID;
                    params[postProcessParameters.MSG_TYPE_RESP.ordinal()]      = (resp.hasField( ISOFields.RESPONSE_CODE.ordinal()))? (resp.getString(ISOFields.RESPONSE_CODE.ordinal()).equals( jTranServerFixedData.RSP_CODE_ERRORTO )) ? null : resp.getMTI() : req.getMTI();
                    params[postProcessParameters.AUTH_ID.ordinal()]            = jTranServerFixedData.AUTH_ID_ERROR;
                    params[postProcessParameters.OPERATOR_AUTH_ID.ordinal()]   = jTranServerFixedData.AUTH_ID_ERROR;
                    params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()] = getMsgStatus(msgLocal, req, resp);
                    params[postProcessParameters.RSP_CODE_ISO.ordinal()]       = (resp.hasField(ISOFields.RESPONSE_CODE.ordinal())) ? resp.getString(ISOFields.RESPONSE_CODE.ordinal()) : req.getString(ISOFields.RESPONSE_CODE.ordinal());
                }
              } 
              //Bill Payment
              else if (req.getString(ISOFields.PROCCESING_CODE.ordinal()).equals(jTranServerFixedData.PROC_CODE_BILL_PAYMENT)){                
                if (resp.hasField(ISOFields.AUTH_ID.ordinal())){
                    params[postProcessParameters.TRX_ID.ordinal()]             = trxID;
                    params[postProcessParameters.MSG_TYPE_RESP.ordinal()]      = resp.getMTI();
                    params[postProcessParameters.AUTH_ID.ordinal()]            = resp.getString(ISOFields.AUTH_ID.ordinal());
                    params[postProcessParameters.OPERATOR_AUTH_ID.ordinal()]   = "";
                    params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()] = getMsgStatus(msgLocal, req, resp);
                    params[postProcessParameters.RSP_CODE_ISO.ordinal()]       = (resp.hasField(ISOFields.RESPONSE_CODE.ordinal())) ? resp.getString(ISOFields.RESPONSE_CODE.ordinal()) : req.getString(ISOFields.RESPONSE_CODE.ordinal());
                } else {
                    params[postProcessParameters.TRX_ID.ordinal()]             = trxID;
                    params[postProcessParameters.MSG_TYPE_RESP.ordinal()]      = (resp.hasField( ISOFields.RESPONSE_CODE.ordinal()))? (resp.getString(ISOFields.RESPONSE_CODE.ordinal()).equals( jTranServerFixedData.RSP_CODE_ERRORTO )) ? null : resp.getMTI() : req.getMTI();
                    params[postProcessParameters.AUTH_ID.ordinal()]            = jTranServerFixedData.AUTH_ID_ERROR;
                    params[postProcessParameters.OPERATOR_AUTH_ID.ordinal()]   = jTranServerFixedData.AUTH_ID_ERROR;
                    params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()] = getMsgStatus(msgLocal, req, resp);
                    params[postProcessParameters.RSP_CODE_ISO.ordinal()]       = (resp.hasField(ISOFields.RESPONSE_CODE.ordinal())) ? resp.getString(ISOFields.RESPONSE_CODE.ordinal()) : req.getString(ISOFields.RESPONSE_CODE.ordinal());
                }
              }
          }

      } catch(Exception e){ }

    }  

    private static String getMsgStatus(String msgLocal, ISOMsg req, ISOMsg resp){
        
        String retVal = "";
        String rspCode = "";
        
        if (msgLocal.equals("")){

            ///Define Final Status According Rsp. Code
            rspCode = (resp.hasField(ISOFields.RESPONSE_CODE.ordinal())) ? resp.getString(ISOFields.RESPONSE_CODE.ordinal()) : req.getString(ISOFields.RESPONSE_CODE.ordinal());
            
            if (rspCode.equals(jTranServerFixedData.RSP_CODE_ERRORTO))
                retVal = jTranServerFixedData.JTRANSERVER_ERR_TIMEOUT;
            else
                retVal = jTranServerFixedData.JTRANSERVER_ERR_GENERAL;
            
        } else {
            retVal = msgLocal;
        }
        
        return retVal;
        
    }    
    
    public static void packSPParamsErrorPostprocess(String params[], String RRN_Local, String msgLocal){

      try{
          params[postProcessParameters.TRX_ID.ordinal()]             = RRN_Local;
          params[postProcessParameters.AUTH_ID.ordinal()]            = jTranServerFixedData.AUTH_ID_ERROR;
          params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()] = msgLocal;
          params[postProcessParameters.RSP_CODE_ISO.ordinal()]       = jTranServerFixedData.RSP_CODE_ERROR;
      } catch(Exception e){ }

    }        
    
    public static String packISOField63(String authCodeFormatter, String statusCode, String message){
      String retCad = "";
      try{
          retCad = ISOUtil.padleft(String.valueOf(authCodeFormatter.trim().length()),2,'0') + authCodeFormatter.trim();

          //Prevent Null Assignment
          if (statusCode.trim().length() <= 0){
              statusCode = jTranServerFixedData.JTRANSERVER_ERR_GENERAL;
              message = jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT;
          }

          retCad += ISOUtil.padleft(String.valueOf(statusCode.trim().length()),2,'0') + statusCode.trim();
          retCad += ISOUtil.padleft(String.valueOf(message.trim().length()),2,'0') + message.trim();

      }catch(Exception e){}

      return retCad;
    }        

    public static String[] unpackISOField63(String data){
      String retTokens[] = new String[jTranServerFixedData.MAX_RSP_TOKENS_FIELD63];

      int idx = 0;
      int len = 0;
      int offset = 0;
      String subData = "";

      try{      
          while(offset < data.length()){
              len = Integer.valueOf(data.substring(offset, offset + 2));            
              offset += 2;
              subData = data.substring(offset, offset + len );            
              offset += len;

              retTokens[idx] = subData.trim();

              idx++;
          }      
      } catch(Exception e){
          for(int j=0; j< retTokens.length; j++)
              retTokens[j] = "";
      }

      return retTokens;
    }        
    
    public static String separateHEXData(String data){
      String retVal = "";
      int i = 0;

      while(i < data.length()){
          retVal += data.substring(i, i+2) + "h ";

          i += 2;
      }

      return retVal;

    }

    public static byte[] swapTPDU(byte tpdu[]){
      byte TPDUtmp[] = new byte[tpdu.length];

      TPDUtmp[0] = tpdu[0];
      TPDUtmp[1] = tpdu[3];
      TPDUtmp[2] = tpdu[4];
      TPDUtmp[3] = tpdu[1];
      TPDUtmp[4] = tpdu[2];      

      return TPDUtmp;
    }
    
    
}
