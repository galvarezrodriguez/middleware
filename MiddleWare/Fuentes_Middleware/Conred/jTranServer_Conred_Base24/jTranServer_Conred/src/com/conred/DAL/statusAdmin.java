/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.DAL;

import java.sql.*;
import com.conred.fixedData.jTranServerFixedData;
import snaq.db.*;
import snaq.util.*;

/**
 *
 * @author Elkin Beltrán
 */
public class statusAdmin {

    private String dbConnectError   = "socket write error";
    private String dbTimeoutError   = "la consulta ha superado el tiempo de espera";
    
    private void logMessageID(String Data, org.apache.log4j.Logger logjTranServer,
            String trxID){
        logjTranServer.info(statusAdmin.class.getName() + " [" + trxID + "] : " + Data + ".");
    }    
    
    public boolean setConnectionStatus(String status, ConnectionPool conDBPool,
            org.apache.log4j.Logger logjTranServer, String trxID, int type, int channelID){
        
        Connection con          = null;
        CallableStatement cs    = null;
        String sqlCallerSP      = "";
        String sqlTxtLog        = "";
        boolean retVal          = false;
        
        //Try to Connect to DB Server
        try{
            
            if (type == jTranServerFixedData.CHANNEL_CONRED){
                sqlCallerSP = "{call " + jTranServerFixedData.STATUS_JNTS_SP_NAME + "(?)}";
            } else if (type == jTranServerFixedData.CHANNEL_MOVILRED){
                sqlCallerSP = "{call " + jTranServerFixedData.STATUS_MOVILRED_SP_NAME + "(?, ?)}";
            }
            
            sqlTxtLog = sqlCallerSP;
            
            con = conDBPool.getConnection(jTranServerFixedData.DBPOOL_CONNECTION_TIMEOUT);            
        } catch(Exception ex){
            //Connecting Error
            con = null;
        }         
        
        try {            
            if (con != null) {
                //Set the SQL Sentence
                cs = con.prepareCall(sqlCallerSP);

                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + status + "'");
                
                //Set Input Parameters
                cs.setString(1, status);     ///Estado a Actualizar
                cs.setInt(2, channelID);     ///Canal a Actualizar

                try {
                    
                    //Set Query Timeout
                    cs.setQueryTimeout(jTranServerFixedData.QUERY_TIMEOUT);                    
                    
                    logMessageID(sqlTxtLog, logjTranServer, trxID);
                    
                    cs.execute();

                    retVal = true;
                    
                }catch(SQLException sqle){
                    
                    if (sqle.getMessage().toLowerCase().trim().contains(dbConnectError.toLowerCase().trim())){
                        //Handle Error Connecting DB Server
                        //Set OutPut Parameters
                        logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                        
                    }  else if (sqle.getMessage().toLowerCase().trim().contains(dbTimeoutError.toLowerCase().trim())){
                        //Handle Error Timeout
                        //Set OutPut Parameters
                        logMessageID("Tiempo de Espera Agotado", logjTranServer, trxID);
                        
                    } else {
                        //Handle General Error
                        //Set OutPut Parameters
                        logMessageID("Error General Servidor BD", logjTranServer, trxID);
                        
                    }
                    
                    retVal = false;
                }                
            } else {
                //Handle Error Connecting DB Server
                //Set OutPut Parameters               
                logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                retVal = false;
            }                
        } catch (Exception e) {
            logMessageID("Error General Servidor BD", logjTranServer, trxID);            
            retVal = false;
        } finally{
            try{
                cs.close();
            } catch(Exception e){}
            
            try{
                con.close();
            } catch(Exception e){}
        }
        
        return retVal;
        
    }
    
}
