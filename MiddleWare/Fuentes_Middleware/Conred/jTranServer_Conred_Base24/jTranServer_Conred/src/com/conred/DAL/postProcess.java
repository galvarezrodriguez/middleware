/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.DAL;

import com.conred.enumerations.postProcessParameters;
import com.conred.fixedData.jTranServerFixedData;
import java.sql.*; 
import snaq.db.*;
import snaq.util.*;

/**
 *
 * @author Elkin Beltrán
 */
public class postProcess {
    
    private String dbConnectError   = "socket write error";
    private String dbTimeoutError   = "la consulta ha superado el tiempo de espera";    
    
    private void logMessageID(String Data, org.apache.log4j.Logger logjTranServer,
            String trxID){
        logjTranServer.info(postProcess.class.getName() + " [" + trxID + "] : " + Data + ".");
    }          
    
    public void postProccesTrx(String[] params, ConnectionPool conDBPool,
            org.apache.log4j.Logger logjTranServer, String trxID){
        Connection con          = null;
        ResultSet rs            = null;
        CallableStatement cStmt = null;
        String sqlCallerSP      = "{call " + jTranServerFixedData.POSTPROCESS_SP_NAME + "(";
        int i                   = 0;
        String sqlTxtLog        = "";
        
        //Try to Connect to DB Server
        try{
            con = conDBPool.getConnection(jTranServerFixedData.DBPOOL_CONNECTION_TIMEOUT);        
        } catch(Exception ex){
            //Connecting Error
            con = null;
        }        
        
        try {
            if (con != null) {
                //Set the SQL Sentence
                for (i=0; i < postProcessParameters.END_FIELD.ordinal() - 1; i++)
                    sqlCallerSP = sqlCallerSP + "?, ";
                sqlCallerSP += "?)}";
                cStmt = con.prepareCall(sqlCallerSP);
                
                sqlTxtLog = sqlCallerSP;
                
                //Set Input Parameters
                cStmt.setLong       (postProcessParameters.TRX_ID.ordinal() + 1             , Long.parseLong(params[postProcessParameters.TRX_ID.ordinal()]));
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", params[postProcessParameters.TRX_ID.ordinal()]);
                cStmt.setString       (postProcessParameters.MSG_TYPE_RESP.ordinal() + 1             , params[postProcessParameters.MSG_TYPE_RESP.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessParameters.MSG_TYPE_RESP.ordinal()] + "'");
                cStmt.setString     (postProcessParameters.AUTH_ID.ordinal() + 1            , params[postProcessParameters.AUTH_ID.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessParameters.AUTH_ID.ordinal()] + "'");
                cStmt.setString     (postProcessParameters.OPERATOR_AUTH_ID.ordinal() + 1   , params[postProcessParameters.OPERATOR_AUTH_ID.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessParameters.OPERATOR_AUTH_ID.ordinal()] + "'");                                
                cStmt.setString     (postProcessParameters.RSP_CODE_FORMATTER.ordinal() + 1 , params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessParameters.RSP_CODE_FORMATTER.ordinal()] + "'");
                cStmt.setString     (postProcessParameters.RSP_CODE_ISO.ordinal() + 1 , params[postProcessParameters.RSP_CODE_ISO.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[postProcessParameters.RSP_CODE_ISO.ordinal()] + "'");                
                
                //Set Output Parameters
                cStmt.registerOutParameter(postProcessParameters.RSP_CODE.ordinal() + 1     , java.sql.Types.VARCHAR);
                cStmt.registerOutParameter(postProcessParameters.RSP_MSG.ordinal() + 1      , java.sql.Types.VARCHAR);
                cStmt.registerOutParameter(postProcessParameters.BALANCE.ordinal() + 1      , java.sql.Types.DECIMAL);
                
                try {
                    
                    //Set Query Timeout
                    cStmt.setQueryTimeout(jTranServerFixedData.QUERY_TIMEOUT);
                    
                    logMessageID(sqlTxtLog, logjTranServer, trxID);
                    
                    //Execute SP
                    cStmt.execute();

                    //Read Output Parameters
                    params[postProcessParameters.RSP_CODE.ordinal()]     = cStmt.getObject(postProcessParameters.RSP_CODE.ordinal() + 1).toString();
                    params[postProcessParameters.RSP_MSG.ordinal()]      = cStmt.getObject(postProcessParameters.RSP_MSG.ordinal() + 1).toString();
                    params[postProcessParameters.BALANCE.ordinal()]      = String.valueOf(cStmt.getBigDecimal(postProcessParameters.BALANCE.ordinal() + 1).longValue());
                
                } catch(SQLException sqle){
                    if (sqle.getMessage().toLowerCase().trim().contains(dbConnectError.toLowerCase().trim())){
                        //Handle Error Connecting DB Server
                        //Set OutPut Parameters
                        params[postProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT;
                        params[postProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT_TXT;
                        params[postProcessParameters.BALANCE.ordinal()]      = jTranServerFixedData.BALANCE_ERROR;   
                        
                        logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                        
                    }  else if (sqle.getMessage().toLowerCase().trim().contains(dbTimeoutError.toLowerCase().trim())){
                        //Handle Error Timeout
                        //Set OutPut Parameters
                        params[postProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_DB_TIMEOUT;
                        params[postProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_DB_TIMEOUT_TXT;
                        params[postProcessParameters.BALANCE.ordinal()]      = jTranServerFixedData.BALANCE_ERROR;                        
                        
                        logMessageID("Tiempo de Espera Agotado", logjTranServer, trxID);
                        
                    } else {
                        //Handle General Error
                        //Set OutPut Parameters
                        params[postProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_GENERAL;
                        params[postProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT;
                        params[postProcessParameters.BALANCE.ordinal()]      = jTranServerFixedData.BALANCE_ERROR;
                        
                        logMessageID("Error General Servidor BD", logjTranServer, trxID);                        
                        
                    }                    
                }                
                
            } else {
                //Handle Error Connecting DB Server
                //Set OutPut Parameters
                params[postProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT;
                params[postProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT_TXT;
                params[postProcessParameters.BALANCE.ordinal()]      = jTranServerFixedData.BALANCE_ERROR;                  
                
                logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                
            }
        } catch(Exception e){
            //Handle General Error
            //Set OutPut Parameters
            params[postProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_GENERAL;
            params[postProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT;
            params[postProcessParameters.BALANCE.ordinal()]      = jTranServerFixedData.BALANCE_ERROR;                    
            
            logMessageID("Error General Servidor BD", logjTranServer, trxID);            
            
        } finally {
            try { 
                if (rs != null)
                  rs.close();
                rs = null;
            } catch (SQLException e) { }
          
            try { 
                if (cStmt != null)
                  cStmt.close();
                cStmt = null;
            } catch (SQLException e) { }

	    try { 
                if (con != null)
                  con.close();
                con = null;
            } catch (SQLException e) { } 
        }
    }
    
}
