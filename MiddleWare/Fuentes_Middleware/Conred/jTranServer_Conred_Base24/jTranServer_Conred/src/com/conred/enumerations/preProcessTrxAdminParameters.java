/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.enumerations;

/**
 *
 * @author Elkin Beltrán
 */

public enum preProcessTrxAdminParameters {
    SOURCE_INTERFACE,
    DEST_INTERFACE,
    MSG_TYPE_REQ,
    FIELD_70,
    STAN,
    TRX_ID,
    END_FIELD
}
