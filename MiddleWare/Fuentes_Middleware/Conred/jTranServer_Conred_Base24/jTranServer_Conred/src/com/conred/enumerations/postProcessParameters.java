/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.enumerations;

/**
 *
 * @author Elkin Beltrán
 */

public enum postProcessParameters {
    TRX_ID,
    MSG_TYPE_RESP,
    AUTH_ID,
    OPERATOR_AUTH_ID,
    RSP_CODE_FORMATTER,
    RSP_CODE_ISO,    
    RSP_CODE,
    RSP_MSG,
    BALANCE,
    END_FIELD
}
