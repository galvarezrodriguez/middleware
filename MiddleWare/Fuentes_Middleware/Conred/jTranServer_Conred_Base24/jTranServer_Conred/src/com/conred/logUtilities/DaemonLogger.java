package com.conred.logUtilities;

/*
 * DaemonLogger.java
 *
 * Created on 27 de noviembre de 2007, 04:28 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin David Beltrán Duque
 */
import com.conred.fixedData.jTranServerFixedData;
import java.io.*;
import java.text.SimpleDateFormat;
import org.jpos.iso.*;
import org.jpos.util.*;
import java.util.*;

public class DaemonLogger  {
  
  private final static long fONCE_PER_DAY = 1000*60*60*24;  
  private final static int fONE_DAY = 1;
  private final static int fONE_AM = 1;
  private final static int fZERO_MINUTES = 0;
  public static Timer timer;
  
  //Server
  private static Logger logServer;
  
  //Formatters
  //private static Logger loggerGateway_Avantel;
  private static Logger loggerGateway_Comcel;
  private static Logger loggerGateway_Tigo;
  //private static Logger loggerGateway_UffMovil;
  private static Logger loggerGateway_Movilred;
  
  public DaemonLogger ( Logger Server, Logger Gateway_Comcel, Logger Gateway_Tigo, 
          Logger Gateway_Movilred){
    this.logServer = Server;
    //this.loggerGateway_Avantel = Gateway_Avantel;
    this.loggerGateway_Comcel = Gateway_Comcel;
    this.loggerGateway_Tigo = Gateway_Tigo;
    //this.loggerGateway_UffMovil = Gateway_UffMovil;
    this.loggerGateway_Movilred = Gateway_Movilred;
    
    timer = new Timer( ) ;
    timer.scheduleAtFixedRate(new changeLogger(), getTomorrowMorning1am(), fONCE_PER_DAY);       
  }

  private static Date getTomorrowMorning1am(){
    Calendar tomorrow = new GregorianCalendar();
    tomorrow.add(Calendar.DATE, fONE_DAY);
    Calendar result = new GregorianCalendar(
      tomorrow.get(Calendar.YEAR),
      tomorrow.get(Calendar.MONTH),
      tomorrow.get(Calendar.DATE),
      fONE_AM,
      fZERO_MINUTES
    );
    return result.getTime();
  }  

  class changeLogger extends TimerTask  {     
    public void run ( )  {
      SimpleDateFormat formatlogger = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
      try{         
          
          logServer.removeAllListeners();
          //loggerGateway_Avantel.removeAllListeners();
          loggerGateway_Comcel.removeAllListeners();
          loggerGateway_Tigo.removeAllListeners();
          //loggerGateway_UffMovil.removeAllListeners();
          //loggerGateway_MovistarSolidda.removeAllListeners();
                   
          ///Crear nuevos Stream de Salidas para Logs
          logServer.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Server_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          //loggerGateway_Avantel.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Avantel_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          loggerGateway_Comcel.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Comcel_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          loggerGateway_Tigo.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Tigo_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          //loggerGateway_UffMovil.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_UffMovil_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          loggerGateway_Movilred.addListener (new SimpleLogListener ( new PrintStream (jTranServerFixedData.LOGS_PATH + jTranServerFixedData.FOLDER_SEPARATOR + jTranServerFixedData.XML_LOG_PATH + jTranServerFixedData.FOLDER_SEPARATOR + "Gateway_Movilred_" + formatlogger.format(new java.util.Date()) + ".xml") ));
          
      }catch(Exception e){
          e.printStackTrace();
      }
    }
  }    
  
}
