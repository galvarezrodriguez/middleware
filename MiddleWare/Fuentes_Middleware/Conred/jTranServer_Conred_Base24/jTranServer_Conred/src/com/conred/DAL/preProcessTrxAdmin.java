/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.DAL;

import java.sql.*; 
import java.math.BigDecimal;
import com.conred.enumerations.preProcessTrxAdminParameters;
import com.conred.fixedData.jTranServerFixedData;
import snaq.db.*;
import snaq.util.*;

/**
 *
 * @author Elkin Beltrán
 */
public class preProcessTrxAdmin {
    
    private String dbConnectError   = "socket write error";
    private String dbTimeoutError   = "la consulta ha superado el tiempo de espera";    
    
    private void logMessageID(String Data, org.apache.log4j.Logger logjTranServer,
            String trxID){
        logjTranServer.info(preProcessTrxAdmin.class.getName() + " [" + trxID + "] : " + Data + ".");
    }    
    
    public void preProccesTrx(String[] params, ConnectionPool conDBPool,
            org.apache.log4j.Logger logjTranServer, String trxID){
        Connection con          = null;
        CallableStatement cStmt = null;
        String sqlCallerSP      = "{call " + jTranServerFixedData.PREPROCESS_TRX_ADMIN_SP_NAME + "(";
        int i                   = 0;
        String sqlTxtLog        = "";
        
        //Try to Connect to DB Server
        try{
            con = conDBPool.getConnection(jTranServerFixedData.DBPOOL_CONNECTION_TIMEOUT);            
        } catch(Exception ex){
            //Connecting Error
            con = null;
        }         
        
        try {
            if (con != null) {            
                //Set the SQL Sentence
                for (i=0; i < preProcessTrxAdminParameters.END_FIELD.ordinal() - 1; i++)
                    sqlCallerSP = sqlCallerSP + "?, ";
                sqlCallerSP += "?)}";
                cStmt = con.prepareCall(sqlCallerSP);

                sqlTxtLog = sqlCallerSP;

                //Set Input Parameters
                cStmt.setString(preProcessTrxAdminParameters.SOURCE_INTERFACE.ordinal() + 1 , params[preProcessTrxAdminParameters.SOURCE_INTERFACE.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessTrxAdminParameters.SOURCE_INTERFACE.ordinal()] + "'");
                cStmt.setString(preProcessTrxAdminParameters.DEST_INTERFACE.ordinal() + 1   , params[preProcessTrxAdminParameters.DEST_INTERFACE.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessTrxAdminParameters.DEST_INTERFACE.ordinal()] + "'");
                cStmt.setString(preProcessTrxAdminParameters.MSG_TYPE_REQ.ordinal() + 1     , params[preProcessTrxAdminParameters.MSG_TYPE_REQ.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessTrxAdminParameters.MSG_TYPE_REQ.ordinal()] + "'");
                cStmt.setString(preProcessTrxAdminParameters.FIELD_70.ordinal() + 1         , params[preProcessTrxAdminParameters.FIELD_70.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessTrxAdminParameters.FIELD_70.ordinal()] + "'");
                cStmt.setString(preProcessTrxAdminParameters.STAN.ordinal() + 1             , params[preProcessTrxAdminParameters.STAN.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessTrxAdminParameters.STAN.ordinal()] + "'");

                //Set Output Parameters
                cStmt.registerOutParameter(preProcessTrxAdminParameters.TRX_ID.ordinal() + 1 , java.sql.Types.BIGINT);

                try {

                    //Set Query Timeout
                    cStmt.setQueryTimeout(jTranServerFixedData.QUERY_TIMEOUT);
                    
                    logMessageID(sqlTxtLog, logjTranServer, trxID);
                    
                    //Execute SP
                    cStmt.execute();

                    //Read Output Parameters
                    params[preProcessTrxAdminParameters.TRX_ID.ordinal()] = cStmt.getObject(preProcessTrxAdminParameters.TRX_ID.ordinal() + 1).toString();

                } catch(SQLException sqle){
                    if (sqle.getMessage().toLowerCase().trim().contains(dbConnectError.toLowerCase().trim())){
                        //Handle Error Connecting DB Server
                        //Set OutPut Parameters
                        params[preProcessTrxAdminParameters.TRX_ID.ordinal()] = jTranServerFixedData.TRX_ID_ERROR;
                        
                        logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                        
                    }  else if (sqle.getMessage().toLowerCase().trim().contains(dbTimeoutError.toLowerCase().trim())){
                        //Handle Error Timeout
                        //Set OutPut Parameters
                        params[preProcessTrxAdminParameters.TRX_ID.ordinal()] = jTranServerFixedData.TRX_ID_ERROR;                       
                        
                        logMessageID("Tiempo de Espera Agotado", logjTranServer, trxID);
                        
                    } else {
                        //Handle General Error
                        //Set OutPut Parameters
                        params[preProcessTrxAdminParameters.TRX_ID.ordinal()] = jTranServerFixedData.TRX_ID_ERROR;
                        
                        logMessageID("Error General Servidor BD", logjTranServer, trxID);
                        
                    }                     
                    
                }
            } else {
                //Handle Error Connecting DB Server
                //Set OutPut Parameters
                params[preProcessTrxAdminParameters.TRX_ID.ordinal()] = jTranServerFixedData.TRX_ID_ERROR;   
                
                logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                
            }
        } catch(Exception e){
            params[preProcessTrxAdminParameters.TRX_ID.ordinal()] = jTranServerFixedData.TRX_ID_ERROR;
            
            logMessageID("Error General Servidor BD", logjTranServer, trxID);
            
        } finally {
            try { 
                if (cStmt != null)
                  cStmt.close();
                cStmt = null;
            } catch (SQLException e) { }

	    try { 
                if (con != null)
                  con.close();
                con = null;
            } catch (SQLException e) { } 
        }
    }
    
}
