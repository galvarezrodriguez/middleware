/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.enumerations;

/**
 *
 * @author Elkin Beltrán
 */

public enum preProcessParameters {
    MSG_TYPE_REQ,
    ORIGIN_INTERFACE,
    PROC_CODE,
    STAN,
    RRN,
    TERMINAL_ID,
    MERCHANT_ID,
    PRODUCT_EAN,
    REF_CUSTOMER_1,
    ADD_DATA,
    AMOUNT,
    IP,
    PORT,
    ROUTE_TRX,
    RSP_CODE,
    RSP_MSG,
    TRX_ID,
    ISOMUX_NAME,
    END_FIELD
}
