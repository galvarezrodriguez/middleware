/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.enumerations;

/**
 *
 * @author Elkin Beltrán
 */

public enum postProcessTrxAdminParameters {
    TRX_ID,
    MSG_TYPE_RESP,
    RSP_CODE,
    END_FIELD
}
