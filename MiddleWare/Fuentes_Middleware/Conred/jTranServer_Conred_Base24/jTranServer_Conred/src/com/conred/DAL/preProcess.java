/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.DAL;

import com.conred.enumerations.preProcessParameters;
import com.conred.fixedData.jTranServerFixedData;
import java.math.BigDecimal;
import java.sql.*; 
import snaq.db.*;
import snaq.util.*;

/**
 *
 * @author Elkin Beltrán
 */
public class preProcess {

    private String dbConnectError   = "socket write error";
    private String dbTimeoutError   = "la consulta ha superado el tiempo de espera";
    
    private void logMessageID(String Data, org.apache.log4j.Logger logjTranServer,
            String trxID){
        logjTranServer.info(preProcess.class.getName() + " [" + trxID + "] : " + Data + ".");
    }    
    
    public void preProccesTrx(String[] params, ConnectionPool conDBPool,
            org.apache.log4j.Logger logjTranServer, String trxID){
        Connection con          = null;
        CallableStatement cStmt = null;
        String sqlCallerSP      = "{call " + jTranServerFixedData.PREPROCESS_SP_NAME + "(";
        int i                   = 0;
        String sqlTxtLog        = "";
        
        //Try to Connect to DB Server
        try{
            con = conDBPool.getConnection(jTranServerFixedData.DBPOOL_CONNECTION_TIMEOUT);            
        } catch(Exception ex){
            //Connecting Error
            con = null;
        }        
        
        try {
            if (con != null) {
                //Set the SQL Sentence
                for (i=0; i < preProcessParameters.END_FIELD.ordinal() - 1; i++)
                    sqlCallerSP = sqlCallerSP + "?, ";
                sqlCallerSP += "?)}";
                cStmt = con.prepareCall(sqlCallerSP);
                
                sqlTxtLog = sqlCallerSP;
                
                //Set Input Parameters
                cStmt.setString     (preProcessParameters.MSG_TYPE_REQ.ordinal() + 1        , params[preProcessParameters.MSG_TYPE_REQ.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.MSG_TYPE_REQ.ordinal()] + "'");
                cStmt.setString     (preProcessParameters.ORIGIN_INTERFACE.ordinal() + 1        , params[preProcessParameters.ORIGIN_INTERFACE.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.ORIGIN_INTERFACE.ordinal()] + "'");                
                cStmt.setString     (preProcessParameters.PROC_CODE.ordinal() + 1       , params[preProcessParameters.PROC_CODE.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.PROC_CODE.ordinal()] + "'");
                cStmt.setInt        (preProcessParameters.STAN.ordinal() + 1            , Integer.parseInt(params[preProcessParameters.STAN.ordinal()]));
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", params[preProcessParameters.STAN.ordinal()]);
                cStmt.setString     (preProcessParameters.RRN.ordinal() + 1             , params[preProcessParameters.RRN.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.RRN.ordinal()] + "'");                
                cStmt.setString     (preProcessParameters.TERMINAL_ID.ordinal() + 1     , params[preProcessParameters.TERMINAL_ID.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.TERMINAL_ID.ordinal()] + "'");
                cStmt.setString     (preProcessParameters.MERCHANT_ID.ordinal() + 1     , params[preProcessParameters.MERCHANT_ID.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.MERCHANT_ID.ordinal()] + "'");
                cStmt.setString     (preProcessParameters.PRODUCT_EAN.ordinal() + 1      , params[preProcessParameters.PRODUCT_EAN.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.PRODUCT_EAN.ordinal()] + "'");
                cStmt.setString     (preProcessParameters.REF_CUSTOMER_1.ordinal() + 1  , params[preProcessParameters.REF_CUSTOMER_1.ordinal()]);               
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.REF_CUSTOMER_1.ordinal()] + "'");
                cStmt.setString     (preProcessParameters.ADD_DATA.ordinal() + 1  , params[preProcessParameters.ADD_DATA.ordinal()]);               
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.ADD_DATA.ordinal()] + "'");                
                cStmt.setBigDecimal (preProcessParameters.AMOUNT.ordinal() + 1          , BigDecimal.valueOf(Long.parseLong(params[preProcessParameters.AMOUNT.ordinal()])));
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", params[preProcessParameters.AMOUNT.ordinal()]);
                cStmt.setString     (preProcessParameters.IP.ordinal() + 1              , params[preProcessParameters.IP.ordinal()]);
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", "'" + params[preProcessParameters.IP.ordinal()] + "'");
                cStmt.setInt        (preProcessParameters.PORT.ordinal() + 1            , Integer.parseInt(params[preProcessParameters.PORT.ordinal()]));
                sqlTxtLog = sqlTxtLog.replaceFirst("\\?", params[preProcessParameters.PORT.ordinal()]);
                
                //Set Output Parameters
                cStmt.registerOutParameter(preProcessParameters.ROUTE_TRX.ordinal() + 1     , java.sql.Types.SMALLINT);
                cStmt.registerOutParameter(preProcessParameters.RSP_CODE.ordinal() + 1      , java.sql.Types.VARCHAR);
                cStmt.registerOutParameter(preProcessParameters.RSP_MSG.ordinal() + 1       , java.sql.Types.VARCHAR);
                cStmt.registerOutParameter(preProcessParameters.TRX_ID.ordinal() + 1        , java.sql.Types.BIGINT);
                cStmt.registerOutParameter(preProcessParameters.ISOMUX_NAME.ordinal() + 1   , java.sql.Types.VARCHAR);
                
                try {
                                        
                    //Set Query Timeout
                    cStmt.setQueryTimeout(jTranServerFixedData.QUERY_TIMEOUT);
                    
                    logMessageID(sqlTxtLog, logjTranServer, trxID);
                    
                    //Execute SP
                    cStmt.execute();

                    //Read Output Parameters
                    params[preProcessParameters.ROUTE_TRX.ordinal()]    = cStmt.getObject(preProcessParameters.ROUTE_TRX.ordinal() + 1).toString();
                    params[preProcessParameters.RSP_CODE.ordinal()]     = cStmt.getObject(preProcessParameters.RSP_CODE.ordinal() + 1).toString();
                    params[preProcessParameters.RSP_MSG.ordinal()]      = cStmt.getObject(preProcessParameters.RSP_MSG.ordinal() + 1).toString();
                    params[preProcessParameters.TRX_ID.ordinal()]       = cStmt.getObject(preProcessParameters.TRX_ID.ordinal() + 1).toString();
                    params[preProcessParameters.ISOMUX_NAME.ordinal()]  = cStmt.getObject(preProcessParameters.ISOMUX_NAME.ordinal() + 1).toString();
                    
                } catch(SQLException sqle){
                    
                    if (sqle.getMessage().toLowerCase().trim().contains(dbConnectError.toLowerCase().trim())){
                        //Handle Error Connecting DB Server
                        //Set OutPut Parameters
                        params[preProcessParameters.ROUTE_TRX.ordinal()]    = jTranServerFixedData.TRANSMIT_TRX_REJECTED;
                        params[preProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT;
                        params[preProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT_TXT;
                        params[preProcessParameters.TRX_ID.ordinal()]       = jTranServerFixedData.TRX_ID_ERROR;   
                        params[preProcessParameters.ISOMUX_NAME.ordinal()]  = "";
                        
                        logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                        
                    }  else if (sqle.getMessage().toLowerCase().trim().contains(dbTimeoutError.toLowerCase().trim())){
                        //Handle Error Timeout
                        //Set OutPut Parameters
                        params[preProcessParameters.ROUTE_TRX.ordinal()]    = jTranServerFixedData.TRANSMIT_TRX_REJECTED;
                        params[preProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_DB_TIMEOUT;
                        params[preProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_DB_TIMEOUT_TXT;
                        params[preProcessParameters.TRX_ID.ordinal()]       = jTranServerFixedData.TRX_ID_ERROR;
                        params[preProcessParameters.ISOMUX_NAME.ordinal()]  = "";
                        
                        logMessageID("Tiempo de Espera Agotado", logjTranServer, trxID);
                        
                    } else {
                        //Handle General Error
                        //Set OutPut Parameters
                        params[preProcessParameters.ROUTE_TRX.ordinal()]    = jTranServerFixedData.TRANSMIT_TRX_REJECTED;
                        params[preProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_GENERAL;
                        params[preProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT;
                        params[preProcessParameters.TRX_ID.ordinal()]       = jTranServerFixedData.TRX_ID_ERROR;
                        params[preProcessParameters.ISOMUX_NAME.ordinal()]  = "";
                        
                        logMessageID("Error General Servidor BD", logjTranServer, trxID);
                        
                    }                    
                }
                
            } else {
                //Handle Error Connecting DB Server
                //Set OutPut Parameters
                params[preProcessParameters.ROUTE_TRX.ordinal()]    = jTranServerFixedData.TRANSMIT_TRX_REJECTED;
                params[preProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT;
                params[preProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_DB_CONNECT_TXT;
                params[preProcessParameters.TRX_ID.ordinal()]       = jTranServerFixedData.TRX_ID_ERROR;
                params[preProcessParameters.ISOMUX_NAME.ordinal()]  = "";
                
                logMessageID("Error Conectando Servidor BD", logjTranServer, trxID);
                
            }
        } catch(Exception e){
            //Handle General Error
            //Set OutPut Parameters
            params[preProcessParameters.ROUTE_TRX.ordinal()]    = jTranServerFixedData.TRANSMIT_TRX_REJECTED;
            params[preProcessParameters.RSP_CODE.ordinal()]     = jTranServerFixedData.JTRANSERVER_ERR_GENERAL;
            params[preProcessParameters.RSP_MSG.ordinal()]      = jTranServerFixedData.JTRANSERVER_ERR_GENERAL_TXT;
            params[preProcessParameters.TRX_ID.ordinal()]       = jTranServerFixedData.TRX_ID_ERROR;
            params[preProcessParameters.ISOMUX_NAME.ordinal()]  = "";
            
            logMessageID("Error General Servidor BD", logjTranServer, trxID);
            
        } finally {
            try { 
                if (cStmt != null)
                  cStmt.close();
                cStmt = null;
            } catch (SQLException e) { }

	    try { 
                if (con != null)
                  con.close();
                con = null;
            } catch (SQLException e) { } 
        }
    }
    
}
