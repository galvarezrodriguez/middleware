/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.DAL;

import java.sql.*;
import com.conred.fixedData.jTranServerFixedData;
import com.conred.enumerations.jTranServerInitParameters;

/**
 *
 * @author Elkin Beltrán
 */
public class initializer {

    public boolean getInitParameters(String initParams[], String DRIVER_JDBC, String URL_DB, String USER_DB, String PASSWORD_DB){
        
        Connection conn         = null;
        CallableStatement cs    = null;
        ResultSet rs            = null;
        boolean retVal          = false;
        
        try {            
            Class.forName( DRIVER_JDBC );
            
            conn = DriverManager.getConnection(URL_DB,USER_DB, PASSWORD_DB);
            
            ///Preparar llamado al SP Inicializador de Parámetros
            cs = conn.prepareCall("{call dbo." + jTranServerFixedData.INIT_SP_NAME + "(?)}");
            
            ///Parámetros
            cs.setInt(1, jTranServerFixedData.INSTANCE_ID);     ///Código de Instancia del Servidor
            
            rs = cs.executeQuery();
            
            // Se establecen los parámetros de Inicialización
            while (rs.next()) {
                if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.LOCAL_PORT.toLowerCase()))
                    initParams[jTranServerInitParameters.LOCAL_PORT.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.LOCAL_TXT_LOG.toLowerCase()) )
                    initParams[jTranServerInitParameters.LOCAL_TXT_LOG.ordinal()] = rs.getString(2); 
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.LOCAL_XML_LOG.toLowerCase()) )
                    initParams[jTranServerInitParameters.LOCAL_XML_LOG.ordinal()] = rs.getString(2); 
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.MIN_THREAD_POOL.toLowerCase()) )
                    initParams[jTranServerInitParameters.MIN_THREAD_POOL.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.MAX_THREAD_POOL.toLowerCase()) )
                    initParams[jTranServerInitParameters.MAX_THREAD_POOL.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.TOTALTIMEOUT_JTRANSERVER.toLowerCase()) )
                    initParams[jTranServerInitParameters.TOTALTIMEOUT_JTRANSERVER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.IP_COMCEL_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.IP_COMCEL_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.PORT_COMCEL_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.PORT_COMCEL_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.TOTALTIMEOUT_COMCEL_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.TOTALTIMEOUT_COMCEL_FORMATTER.ordinal()] = rs.getString(2);                
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.ISOMUX_NAME_COMCEL_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.ISOMUX_NAME_COMCEL_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.IP_TIGO_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.IP_TIGO_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.PORT_TIGO_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.PORT_TIGO_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.TOTALTIMEOUT_TIGO_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.TOTALTIMEOUT_TIGO_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.ISOMUX_NAME_TIGO_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.ISOMUX_NAME_TIGO_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.IP_MOVILRED_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.IP_MOVILRED_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.PORT_MOVILRED_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.PORT_MOVILRED_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.TOTALTIMEOUT_MOVILRED_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.TOTALTIMEOUT_MOVILRED_FORMATTER.ordinal()] = rs.getString(2);
                else if ( rs.getString(1).trim().toLowerCase().equals(jTranServerFixedData.ISOMUX_NAME_MOVILRED_FORMATTER.toLowerCase()) )
                    initParams[jTranServerInitParameters.ISOMUX_NAME_MOVILRED_FORMATTER.ordinal()] = rs.getString(2);                
            }
            
            retVal = true;
            
        } catch (Exception e) {
            retVal = false;
        } finally{
            try{
                cs.close();
            } catch(Exception e){}
            
            try{
                rs.close();
            } catch(Exception e){}
            
            try{
                conn.close();
            } catch(Exception e){}
        }
        
        return retVal;
        
    }
    
}
