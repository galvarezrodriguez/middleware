/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.clients;

import com.conred.jPOSConredChannel.BASE24TCPChannel_Movilred;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.packager.*;
import java.text.SimpleDateFormat;

/**
 *
 * @author Elkin Beltrán
 */
public class MovilredBase24_EchoTestClient {
    public static void main(String args[]) {  
        byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};

        Logger logger = new Logger();
        logger.addListener (new SimpleLogListener (System.out));

        for(int i=0; i<100; i++){
            ISOChannel channel = null;
            try{
                channel = new BASE24TCPChannel_Movilred ("127.0.0.1", 25030, new BASE24Packager());        
                ((LogSource)channel).setLogger (logger, "EmuladorPOS-channel");
                ((BaseChannel)channel).setHeader(BASE64_HEADER);
                ((BaseChannel)channel).setTimeout(60000);   ////60 segundos de espera
                channel.connect ();   

                ///Movilred Echo Test
                ISOMsg m = new ISOMsg ();
                m.setMTI ("0800");
                m.set(3, "990000"); 
                m.set (7, ISODate.getDateTime (new java.util.Date()));
                m.set(11, "528445");
                //m.set(41, "00000001");          ///TERMINAL ID
                //m.set(42, "000000000000001");   ///COMERCIO ID        
                //m.set(48, "1.01");              ///APP. VERSION

                m.set(70, "301");       //Echo Test

                channel.send (m);
                ISOMsg r = channel.receive ();
                channel.disconnect ();
                channel = null;

            } catch(Exception e){
                e.printStackTrace();
            }            
        }
        
    }
}
