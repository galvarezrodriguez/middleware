/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.fixedData;

/**
 *
 * @author Elkin Beltrán
 */
public class jTranServerFixedData {
    public static final String DATABASE_DRIVER_JDBC_KEY                 = "jTranServer_Conred.databaseDRIVER_JDBC";
    public static final String DATABASE_URL_KEY                         = "jTranServer_Conred.databaseURL";
    public static final String DATABASE_USER_KEY                        = "jTranServer_Conred.databaseUSER";    
    public static final String DATABASE_PASSWORD_KEY                    = "jTranServer_Conred.databasePASSWORD";
    public static final String INIT_SP_NAME                             = "sp_mtcConsultarParametrosServidor";
    public static final String PREPROCESS_TRX_ADMIN_SP_NAME             = "sp_mtcPreprocesarTrxAdministrativa";
    public static final String POSTPROCESS_TRX_ADMIN_SP_NAME            = "sp_mtcPostprocesarTrxAdministrativa";  
    public static final String STATUS_MOVILRED_SP_NAME                  = "sp_mtcActualizarEstadoMovilred";
    public static final String STATUS_JNTS_SP_NAME                      = "sp_mtcActualizarEstadoJNTS";
    public static final int INSTANCE_ID                                 = 0;
    public static final int MAX_RSP_TOKENS_FIELD63                      = 2;
    public static final boolean THIRD_PARTY_COMPONENT                   = false;
    public static final long DBPOOL_CONNECTION_TIMEOUT                  = 5000;     //5 Seconds To Get Connection
    public static final int QUERY_TIMEOUT                               = 10;       //10 Seconds To Process SP
    public static final String PREPROCESS_SP_NAME                       = "sp_mtcPreProcesar";
    public static final String POSTPROCESS_SP_NAME                      = "sp_mtcPostProcesar";
    public static final String TRANSMIT_TRX_REJECTED                    = "0";
    public static final int TRANSMIT_TRX_REJECTED_INT                   = 0;
    public static final String TRANSMIT_TRX_OK                          = "1";
    public static final String BALANCE_DECIMALS                         = "00";
    public static final String TRX_ID_ERROR                             = "-1";
    public static final String BALANCE_ERROR                            = "0";
    public static final String JTRANSERVER_VERSION                      = "v1.0";
    public static final int JTRANSERVER_RELAX_TIME                      = 2000;
    public static final int DBPOOL_IDLE_TIMEOUT                         = 60*60;  ///In Seconds - 0 Unlimited
        
    //Log Files Path
    public static final String LOGS_PATH                                = "logs";
    public static final String XML_LOG_PATH                             = "xml";
    public static final String TXT_LOG_PATH                             = "text-plain";
    public static final String FOLDER_SEPARATOR                         = "/";
    
    //Init Properties File
    public static final String JTRANSERVER_CONFIG_FILE                  = "./jTranServer.properties";
    
    //Init. Parameters
    public static final String LOCAL_PORT                               = "LocalPort";
    public static final String LOCAL_TXT_LOG                            = "LocalTextLog";
    public static final String LOCAL_XML_LOG                            = "LocalXMLLog";
    public static final String MIN_THREAD_POOL                          = "minThreadPool";
    public static final String MAX_THREAD_POOL                          = "maxThreadPool";
    public static final String TOTALTIMEOUT_JTRANSERVER                 = "TotalTimeoutJTranServer";
    public static final String IP_COMCEL_FORMATTER                      = "IPComcelFormatter";
    public static final String PORT_COMCEL_FORMATTER                    = "PortComcelFormatter";
    public static final String TOTALTIMEOUT_COMCEL_FORMATTER            = "totalTimeoutComcelFormatter";
    public static final String ISOMUX_NAME_COMCEL_FORMATTER             = "isomuxNameComcelFormatter";
    public static final String IP_TIGO_FORMATTER                        = "IPTigoFormatter";
    public static final String PORT_TIGO_FORMATTER                      = "PortTigoFormatter";
    public static final String TOTALTIMEOUT_TIGO_FORMATTER              = "totalTimeoutTigoFormatter";
    public static final String ISOMUX_NAME_TIGO_FORMATTER               = "isomuxNameTigoFormatter";
    public static final String IP_MOVILRED_FORMATTER                    = "IPMovilredFormatter";
    public static final String PORT_MOVILRED_FORMATTER                  = "PortMovilredFormatter";
    public static final String TOTALTIMEOUT_MOVILRED_FORMATTER          = "totalTimeoutMovilredFormatter";    
    public static final String ISOMUX_NAME_MOVILRED_FORMATTER           = "isomuxNameMovilredFormatter";    
    
    ///Error Messages
    public static final String JTRANSERVER_ERR_GENERAL_TXT              = "Error General Plataforma";
    public static final String JTRANSERVER_ERR_DB_TIMEOUT_TXT           = "Timeout Base de Datos";
    public static final String JTRANSERVER_ERR_DB_CONNECT_TXT           = "Error Conectando Base de Datos";
    public static final String JTRANSERVER_ERR_TIMEOUT_TXT              = "Timeout Formateador";
    public static final String JTRANSERVER_ERR_CONNECT_TXT              = "Formateador Desconectado";
    
    ///TPDU
    public static byte[] TPDU                                           = {0x00, 0x00, 0x00, 0x00, 0x00};
    
    ///Trx. States
    public static final String JTRANSERVER_TRX_OK                       = "JTS000000";
    public static final String JTRANSERVER_ERR_CONNECT                  = "JTS990000";
    public static final String JTRANSERVER_ERR_TIMEOUT                  = "JTS980000";
    public static final String JTRANSERVER_ERR_MSG_OUT                  = "JTS970000";
    public static final String JTRANSERVER_ERR_AMOUNT                   = "JTS960000";
    public static final String JTRANSERVER_ERR_GENERAL                  = "JTS999000";
    public static final String JTRANSERVER_ERR_DB_TIMEOUT               = "JTS700000";
    public static final String JTRANSERVER_ERR_DB_CONNECT               = "JTS710000";
    public static final String RSP_CODE_OK                              = "00";
    public static final String RSP_CODE_ERROR                           = "99";
    public static final String RSP_CODE_ERRORTO                         = "A3"; //Timeout    
    public static final String AUTH_ID_ERROR                            = "0";
    
    ///Operators ID
    public static final int OPERATOR_COMCEL                             = 100;
    public static final int OPERATOR_MOVISTAR                           = 110;
    public static final int OPERATOR_TIGO                               = 120;
    public static final int OPERATOR_AVANTEL                            = 130;
    public static final int OPERATOR_UFF_MOVIL                          = 140;
    
    ///TCP Connection Status
    public static final String TCP_DOWN                                 = "TCPDOWN";
    public static final String TCP_UP                                   = "TCPUP";
    
    ///Msg. Admin. Status
    public static final String LOGOFF                                   = "LOGOFF";
    public static final String LOGON                                    = "LOGON";

    ///Other Static Data
    public static final String ADMIN_MSG_TYPE_REQ                       = "0800";
    public static final String ADMIN_MSG_TYPE_RESP                      = "0810";
    public static final String ECHO_TEST_CODE                           = "301";
    public static final String LOGON_CODE                               = "001";    
    public static final String LOGOFF_CODE                              = "002";
    public static final String PROC_CODE_MOVILRED                       = "990000";
    public static final String PROC_CODE_MOVILRED1                      = "990001";
    public static final String PROC_CODE_MOVILRED2                      = "990002";
    public static final String PROC_CODE_MOVILRED3                      = "990003";
    
    ///Admin Message Destination Type
    public static final int ADMIN_MSG_JNTS_TO_CONRED                    = 1;
    public static final int ADMIN_MSG_CONRED_TO_MOVILRED                = 2;
    
    ///Channel Type
    public static final int CHANNEL_CONRED                              = 1;
    public static final int CHANNEL_MOVILRED                            = 2;
    
    ///Transactions Types & Proccesing Codes
    public static final String MSG_TYPE_SALE_REQ                        = "0200";
    public static final String MSG_TYPE_SALE_RESP_OK                    = "0210";
    public static final String MSG_TYPE_SALE_RESP_ERROR                 = "9210";    
    public static final String PROC_CODE_TOPUP_SALE                     = "380000";
    public static final String PROC_CODE_BILL_PAYMENT                   = "150000";
    
    ///Interface Origin Name
    public static final String ORIGIN_INTERFACE_NAME                    = "MIDDLEWARE";
}
