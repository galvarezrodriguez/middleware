/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.clients;

import com.conred.jPOSConredChannel.BASE24TCPChannel_Movilred;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.packager.*;

/**
 *
 * @author Elkin Beltrán
 */
public class JNTSBase24_TopupClient {
    public static void main(String args[]) {  
        byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
        ISOChannel channel;   

        Logger logger = new Logger();
        logger.addListener (new SimpleLogListener (System.out));
        
        try{
            channel = new BASE24TCPChannel_Movilred ("127.0.0.1", 25030, new BASE24Packager());        
            ((LogSource)channel).setLogger (logger, "EmuladorJNTS-channel");
            ((BaseChannel)channel).setHeader(BASE64_HEADER);
            ((BaseChannel)channel).setTimeout(60000);   ////60 segundos de espera
            channel.connect ();   

            ///JNTS TOPUP
            ISOMsg m = new ISOMsg ();
            m.setMTI ("0200");
            m.set (3, "380000");
            m.set (4, "1000" + "00");
            m.set (7, ISODate.getDateTime (new java.util.Date()));
            m.set (11, "100005");
            m.set (12, ISODate.getTime (new java.util.Date()));
            m.set (13, ISODate.getDate (new java.util.Date()));
            m.set (17, ISODate.getDate (new java.util.Date()));
            m.set (22, "010");
            m.set (32, "10000000005");
            m.set (35, "94000096000022=4912101               ");
            m.set (37, "100000000005");
            m.set (41, ISOUtil.strpad("96000022", 16));
            m.set (42, ISOUtil.strpad("", 15));
            m.set (43, "SUPERMERCADO SAS      1100100BOGOTACUNCO");
            m.set (48, "0525002355         00250001");
            m.set (49, "170");
            m.set (60, "0005FINA+0000000");
            m.set (61, "0000    00000000000");
            ///EAN POR PRODUCTO Y CELULAR A RECARGAR
            //COMCEL=770717532280 ; TIGO=770213800506 ; MOVISTAR=770717696219
            m.set (63, "& 0000200058! P000034 770717532280            3112956578P ");
            m.set (100, "10000000000");
            m.set (128, "A0CD4B0100000000");
            
            channel.send (m);
            ISOMsg r = channel.receive ();
            channel.disconnect ();
            channel = null;
            
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
