/*
 * TestPOS.java
 *
 * Created on 22 de noviembre de 2007, 11:41 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin David Beltrán Duque @Conred
 */

import java.util.*;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;
import org.jpos.iso.packager.*;
import org.com.jposChannel.*;
import org.jpos.core.Sequencer;
import org.jpos.core.VolatileSequencer;
import org.com.LVList.LVList;

import org.com.utilities.*;

public class TestPOS {
        
    public static Sequencer seq = new VolatileSequencer();
    
    public static void main (String[] args) throws Exception {       
        
        if (args.length < 3){
            System.out.println("USO CORRECTO:\n" + "jTestPOS.exe 1 2 3\n"
                    + "DONDE:\n" + "1: Numero de peticiones a enviar.\n"
                    + "2: Tiempo de espera(milisegundos) entre envios.\n"
                    + "3: Tipo Peticion(1-Ventas, 2-Echo Test, 3-Consulta Saldo).");
            return;
        }
        
        int id=1;
        int max = Integer.valueOf(args[0]);
        long waitTime = Long.valueOf(args[1]);
        int tipo = Integer.valueOf(args[2]);        
        
        seq.set("traceno",(int)(Math.random() * 50000 + 1)); ///Trace Seed
            
        while(true){
        
            if (tipo == 1){ ///Ventas            
                for (int y=0; y<max; y++){
                    sendTestSale sT = new sendTestSale(id, y);
                    new Thread(sT).start();
                    id++;
                    if (id >= 3)
                          id = 1;                
                    Thread.sleep(20);
                }
            }
            else if (tipo == 2){    ///echo test
                for (int y=0; y<max; y++){
                    sendTestEcho sTE = new sendTestEcho(y);
                    new Thread(sTE).start();
                    Thread.sleep(20);
                }            
            }
            else if (tipo == 3){    ///Consulta saldo
                for (int y=0; y<max; y++){
                    sendTestQuery sTQ = new sendTestQuery(y);
                    new Thread(sTQ).start();
                    Thread.sleep(20);
                }            
            }            
            Thread.sleep(waitTime);
            
        }
    }
    
private static class sendTestSale implements Runnable{
    private int idOPE;
    private int id;
    
    public sendTestSale(int idOPE, int id){
        this.idOPE = idOPE;
        this.id = id;
    }
    
    public void run(){
        ISOChannel channel = null;
        try{
        
            ISOMsg m = new ISOMsg ();
            byte[] TPDU = {0x60, 0x00, 0x02, 0x00, 0x00};
            
            channel = new NACChannel ("127.0.0.1", 6663, new ISO_ePrepaywarePackager(), TPDU);
            ((BaseChannel)channel).setTimeout(60000);   ////60 segundos de espera
            channel.connect ();               
            
            m.setMTI("0200");
            m.set(4,  "1000" + "00");   

            ///STAN cambiarlo para cada transacción    
            m.set (new ISOField (11,ISOUtil.zeropad(new Integer(seq.get("traceno")).toString(),6)));

            ///Fecha y hora
            ISODateTime isoDateTime = null; 
            isoDateTime = new ISODateTime(ISODateTime.formatTime.LOCAL);
            m.set (12, isoDateTime.getTime());
            m.set (13, isoDateTime.getDate());            
            
            m.set (37, "000000" + m.getString(11));
            m.set (41, "57000109");             
            
            LVList list = new LVList(6);
            
            if (this.idOPE == 1){
                m.set(3, "606000");     ////Para Comcel
                list.setValue(0, "1".getBytes());   ///movistar
                list.setValue(2, "3112956578".getBytes());
            } else if (this.idOPE == 2){
                m.set(3, "000016");     ////Para Movistar
                list.setValue(0, "2".getBytes());   ///movistar
                list.setValue(2, "3157107720".getBytes());      
            } else if (this.idOPE == 3){
                m.set (3,  "304000");   ////Para Tigo
                list.setValue(0, "3".getBytes());   ///movistar
                list.setValue(2, "3011234567".getBytes());      
            }
            
            list.setValue(1, "9999".getBytes());  //General para cualquier valor
            list.setValue(3, "307140".getBytes());
            list.setValue(4, "".getBytes());   ///Comcel                
            list.setValue(5, "".getBytes());
            byte[] rp = list.pack();
            byte[] field60 = new byte[rp.length+1];
            System.arraycopy(rp, 0, field60, 0, rp.length);
            m.set (60, field60);  
            
            System.out.println("VENTA RECARGA[" + this.id + "]: " + m);
            
            if (channel.isConnected()){
                ///Enviar transaccion si está conectado
                try{
                    channel.send(m);
                }catch(Exception p){
                    ///No se estableció conexion con el servidor
                    System.out.println("ERROR ENVIANDO TRAMA VENTA RECARGA[" + this.id + "]: " + m);    
                    return;
                }
                
                try{
                    ISOMsg r = channel.receive();
                    System.out.println("VENTA RECARGA[" + this.id + "]: " + r);
                }catch(Exception io){
                    System.out.println("TIMEOUT DE LECTURA AGOTADO VENTA RECARGA[" + this.id + "]: " + m);    
                }
            }else{
                System.out.println("NO SE ESTABLECIO CONEXION VENTA RECARGA[" + this.id + "]: " + m);
            }                                   
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if (channel!= null)
                    channel.disconnect();
            }catch(Exception s){
                s.printStackTrace();
            }
            channel = null;        
        }
    }
}
    
private static class sendTestEcho implements Runnable{
    
    private int id;
    
    public sendTestEcho(int id){
        this.id = id;
    }
    
    public void run(){
        ISOChannel channel = null;
        try{
        
            ISOMsg m = new ISOMsg ();
            byte[] TPDU = {0x60, 0x00, 0x01, 0x00, 0x00};
            
            //channel = new NACChannel ("127.0.0.1", 6663, new ISO_ePrepaywarePackager(), TPDU);
            channel = new NACChannel ("194.168.1.48", 8888, new ISO_ePrepaywarePackager(), TPDU);
            ((BaseChannel)channel).setTimeout(60000);   ////60 segundos de espera
            channel.connect ();               
            
            m.setMTI ("0800");
            m.set (3, "990000");
            
            ///STAN cambiarlo para cada transacción    
            m.set (new ISOField (11,ISOUtil.zeropad(new Integer(seq.get("traceno")).toString(),6)));
            
            m.set (41, "00000001");              

            System.out.println("ECHO TEST[" + this.id + "]: " + m);            
            
            if (channel.isConnected()){
                ///Enviar transaccion si está conectado
                try{
                    channel.send(m);
                }catch(Exception p){
                    ///No se estableció conexion con el servidor
                    System.out.println("ERROR ENVIANDO TRAMA ECHO TEST[" + this.id + "]: " + m);    
                    return;
                }
                
                try{
                    ISOMsg r = channel.receive();
                    System.out.println("ECHO TEST[" + this.id + "]: " + r);
                }catch(Exception io){
                    System.out.println("TIMEOUT DE LECTURA AGOTADO ECHO TEST[" + this.id + "]: " + m);    
                }
            }else{
                System.out.println("NO SE ESTABLECIO CONEXION ECHO TEST[" + this.id + "]: " + m);
            }                
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try{
                if (channel!= null)
                    channel.disconnect();
            }catch(Exception s){
                s.printStackTrace();
            }
            channel = null;        
        }
    }
    
}   

private static class sendTestQuery implements Runnable{

    private int id;
    
    public sendTestQuery(int id){
        this.id = id;
    }
    
    public void run(){
        ISOChannel channel = null;
        try{        
            ISOMsg m = new ISOMsg ();
            byte[] TPDU = {0x60, 0x00, 0x01, 0x00, 0x00};
            
            channel = new NACChannel ("127.0.0.1", 6663, new ISO_ePrepaywarePackager(), TPDU);
            ((BaseChannel)channel).setTimeout(60000);   ////60 segundos de espera
            channel.connect ();               
            
            m.setMTI ("0800");
            m.set (3, "300300");
            
            ///STAN cambiarlo para cada transacción    
            m.set (new ISOField (11,ISOUtil.zeropad(new Integer(seq.get("traceno")).toString(),6)));
            
            m.set (41, "57000109");              

            System.out.println("CONSULTA SALDO[" + this.id + "]: " + m);            
            
            if (channel.isConnected()){
                ///Enviar transaccion si está conectado
                try{
                    channel.send(m);
                }catch(Exception p){
                    ///No se estableció conexion con el servidor
                    System.out.println("ERROR ENVIANDO TRAMA CONSULTA SALDO[" + this.id + "]: " + m);    
                    return;
                }
                
                try{
                    ISOMsg r = channel.receive();
                    System.out.println("CONSULTA SALDO[" + this.id + "]: " + r);
                }catch(Exception io){
                    System.out.println("TIMEOUT DE LECTURA AGOTADO CONSULTA SALDO[" + this.id + "]: " + m);    
                }
            }else{
                System.out.println("NO SE ESTABLECIO CONEXION CONSULTA SALDO[" + this.id + "]: " + m);
            }            
        }catch(Exception e){
            e.printStackTrace();
        } finally{
            try{
                if (channel!= null)
                    channel.disconnect();
            }catch(Exception s){
                s.printStackTrace();
            }
            channel = null;                    
        }
    }
    
}   

}