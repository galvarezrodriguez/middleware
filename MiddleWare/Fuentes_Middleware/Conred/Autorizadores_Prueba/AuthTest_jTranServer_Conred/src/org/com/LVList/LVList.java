package org.com.LVList;

/*
 * LVList.java
 *
 * Created on 15 de febrero de 2008, 10:58
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin Beltran
 */
public class LVList {
    
    private final int maxMsg        = 50;
    private int numMsg;
    private LVMsg[] tmsg            = new LVMsg[maxMsg];
    
    /** Creates a new instance of LVList */
    public LVList(int numMsg) {
        this.numMsg = numMsg;
        initList();
    }
    
    public LVList()
    {
        
    }
    
    private void initList(){       
        for (int i=0; i<numMsg; i++)
            tmsg[i] = new LVMsg();        
    }
    
    public void setValue(int index, byte[] value){
        //Prevent Array OverFlow
        if (index < numMsg){
            tmsg[index].setValue(value);
        }
    }
    
    public byte[] getValue(int index){
        //Prevent Array OverFlow
        if (index < numMsg){
            return tmsg[index].getValue();
        }
        return null;
    }    

    public byte[] getLV(int index){
        //Prevent Array OverFlow
        if (index < numMsg){
            return tmsg[index].getLV();
        }
        return null;
    }        
    
    public byte[] pack(){
        int maxLength   = 0;
        int offset      = 0;
        byte[] packer   = null;
        
        ///Query Length
        for (int i=0; i<numMsg; i++){
            if (tmsg[i].getLint() > 0)
                maxLength += tmsg[i].getLint() + 1;
        }
        packer = new byte[maxLength];
        
        for (int i=0; i<numMsg; i++){
            if (tmsg[i].getLint() > 0){
                System.arraycopy(tmsg[i].getLV(), 0, packer, offset, tmsg[i].getLint() + 1);
                offset += tmsg[i].getLint() + 1;
            }
        }
        
        if (packer.length > 0)
            return packer;
        else
            return null;
    }    
    
    public boolean unpack(byte[] data, int startIndexBuffer, String[] parametros, int startIndexParameters)
    {
        try
        {
            int pointerBuffer = startIndexBuffer;
            int pointerParameters = startIndexParameters;
            int len = 0;
            int offset = startIndexBuffer;
            while(pointerBuffer < data.length){
                len = (int)data[pointerBuffer];
                byte[] tmp = new byte[len];
                System.arraycopy(data, offset+1, tmp, 0, len);
                parametros[pointerParameters] = new String(tmp);
                pointerParameters += 1;
                pointerBuffer += len + 1;
                offset += len + 1;
                tmp = null;
            }  
            return true;
        }catch (Exception e)
        {
            return false;
        }
    }
}
