package org.com.LVList;


/**
 * @author Elkin Beltran
 */
public class LVMsg {

    private byte[] value;

    /**
     * empty constructor
     */
    public LVMsg() {

    }

    /**
     * constructs a LV Message from value
     * @param value 
     */
    public LVMsg(byte[] value) {
        this.value = value;
    }

    /*
     * @return value 
     */
    public byte[] getValue() {
        return value;
    }

    /**
     * @param value of LV Message
     */
    public void setValue(byte[] newValue) {
        this.value = newValue;
    }

    /*
     * @return length + value of the LV Message
     */
    public byte[] getLV() {
        byte[] bLen = getL();
        if (value != null) {
            int tLength = bLen.length + value.length;
            byte[] out = new byte[tLength];
            System.arraycopy(bLen,  0, out,           0, bLen.length );
            System.arraycopy(value, 0, out, bLen.length, value.length);            
            return out;
        } else {//Length can be 0
            int tLength = bLen.length;
            byte[] out = new byte[tLength];
            System.arraycopy(bLen, 0, out, 0, bLen.length);
            return out;
        }
    }

    /**
     * Value less than 255 can be encoded in single byte and multiple bytes are
     * required for length bigger than 255
     * 
     * @return encoded length
     */
    public byte[] getL() {

        if (value == null)
            return new byte[1];
        
        int len = getLint();

        /* If value less than 255 can be encoded on one byte */
        if (len <= 255) {
            byte[] rBytes = new byte[1];
            rBytes[0] = (byte) value.length;
            return rBytes;
        }
        else { ///Maximun two bytes to be encoded
            byte[] rBytes = new byte[2];
            rBytes[0] = (byte) (value.length / 256);
            rBytes[1] = (byte) (value.length % 256);            
            return rBytes;            
        }
    }
    
    /*
     * @return integer length of the value
     */
    public int getLint() {
        if (value != null)
            return value.length;
        return 0;
    }    
}

