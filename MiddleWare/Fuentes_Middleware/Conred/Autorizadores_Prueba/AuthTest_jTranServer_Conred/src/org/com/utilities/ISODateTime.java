package org.com.utilities;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author francisco
 */

import java.util.*;

public class ISODateTime {

       /*
     * LOCAL -> Local Time 
     * GMT -> Greenwich Mean Time
     */
    public enum formatTime
    {
        LOCAL,
        GMT       
    }
    
    private Calendar TIMEDate;
    
    
    /*
     * Constructor 
     * format -> Greenwich Mean Time or Local Time
     */
    public ISODateTime(formatTime format)
    {
        if (format == format.GMT)
        {
            TIMEDate = Calendar.getInstance(new SimpleTimeZone(0, "GMT"));
        }else if (format == format.LOCAL)
        {
            TIMEDate = Calendar.getInstance(new SimpleTimeZone(TimeZone.getDefault().getRawOffset(), "GMT"));
        }
    }
    
    /*
     * getDate 
     * return Date (DDMM) - Field 12 ISO8583
     */
    public String getDate()
    {
        ////ELKIN LE QUITO LA SUMA DE 1 AL DIA - DAY_OF_MONTH
        return padleft(Integer.toString(TIMEDate.get(Calendar.MONTH) + 1), 2, '0') + 
               padleft(Integer.toString(TIMEDate.get(Calendar.DAY_OF_MONTH)), 2, '0');
        ////FIN CAMBIO ELKIN       
    }
    
    /*
     * getTime 
     * return Date (HHMMSS) - Field 13 ISO8583
     */
    public String getTime()
    {                
        return padleft(Integer.toString(TIMEDate.get(Calendar.HOUR_OF_DAY)), 2, '0') + 
               padleft(Integer.toString(TIMEDate.get(Calendar.MINUTE)), 2, '0') + 
               padleft(Integer.toString(TIMEDate.get(Calendar.SECOND)), 2, '0');
    }   
    
    /*
     * padleft 
     * s -> String
     * len -> Lenght to format
     * c -> char (pad) for instance '0', ' ', etc.
     * return String with padLeft, for example:
     * padLeft ("5", 2, '0') return "05"
     */
    private String padleft(String s, int len, char c)
    {
        s = s.trim();
        if (s.length() > len)
            return null;
        StringBuilder d = new StringBuilder (len);
        int fill = len - s.length();
        while (fill --> 0)      
           d.append(c);            
        d.append(s);
        return d.toString();
    } 
    
}
