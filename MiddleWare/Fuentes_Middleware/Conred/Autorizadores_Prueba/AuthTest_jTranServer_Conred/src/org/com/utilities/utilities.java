package org.com.utilities;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author francisco
 */
public class utilities {
    
    
        /**
     * pad to the left
     * @param s - original string
     * @param len - desired len
     * @param c - padding char
     * @return padded string
     */
    public String padleft(String s, int len, char c)
    {
        s = s.trim();
        if (s.length() > len)
            return null;
        StringBuffer d = new StringBuffer (len);
        int fill = len - s.length();
        while (fill --> 0)      
           d.append(c);            
        d.append(s);
        return d.toString();
    } 
    
        
        /**
     * pad to the left
     * @param s - original string
     * @param len - desired len
     * @param c - padding char
     * @return padded string
     */
    public String padRight(String s, int len, char c)
    {
        s = s.trim();
        if (s.length() > len)
            return null;
        StringBuffer d = new StringBuffer (len);
        int fill = len - s.length();
        d.append(s);
        while (fill-- > 0)      
           d.append(c);       
        return d.toString();
    } 


}
