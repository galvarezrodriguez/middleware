/*
 * authTestCOMCEL.java
 *
 * Created on 22 de noviembre de 2007, 11:41 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin David Beltran Duque
 */

import java.io.*;
import java.util.Random;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;
import org.jpos.iso.packager.*;

import org.jpos.core.Sequencer;
import org.jpos.core.VolatileSequencer;

import org.com.jposChannel.*;

public class authTestCOMCEL implements ISORequestListener {
    
    private static int initVal;
    private static Sequencer seq = new VolatileSequencer();
    
    private static int i=0;
    
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(authTestCOMCEL.class);
    
    public authTestCOMCEL() {
        super();
    }
    
    public boolean process (ISOSource source, ISOMsg m) {

        String tcpData = "";
        
        try {            
            //Auth ID
            //ISOUtil.zeropad(new Integer(seq.get ("traceno")).toString(),6)
            ///El comcelChannel se puede cambiar por el canal del iso server utilizado en el main
            System.out.println("IP DEL CLIENTE    :::" + ((comcelChannel)m.getSource()).getSocket().getInetAddress().toString().substring(1));
            System.out.println("PUERTO DE ATENCIÓN:::" + ((comcelChannel)m.getSource()).getSocket().getPort());
            
            tcpData = ((comcelChannel)m.getSource()).getSocket().getInetAddress().toString().substring(1) + ":" + ((comcelChannel)m.getSource()).getSocket().getPort();
            
            log.info("Conexión Establecida." + " - " + tcpData);
            
            String cadRet = m.getString(0);
            String[] sep = cadRet.split("\\|"); 
            
            log.info("Data Recibida." + " - " + tcpData + " - " + cadRet);
            
            if (sep[4].equals("9999999999")){
                cadRet += "055" + "|" + "\n";
            }
            else if (sep[4].equals("8888888888")){
                cadRet += "077" + "|" + "\n";
            }
            else if (sep[4].equals("7777777777")){
                cadRet += "015" + "|" + "\n";
            }     
            else if (sep[4].equals("6666666666")){   ///TIMEOUT
                ///Timeout
            }
            else if (sep[4].substring(0,1).equals("3")){
                if (sep[4].substring(0,3).equals("310") || sep[4].substring(0,3).equals("311") || sep[4].substring(0,3).equals("312") || sep[4].substring(0,3).equals("313") || sep[4].substring(0,3).equals("314") ){
                    
                    try{
                        if (sep[4].trim().length() != 10){
                            cadRet += "009" + "|" + "\n";    
                        } else if (Long.valueOf(sep[4].trim()) > 0 ){
                            //Número Correcto
                            cadRet += "000" + "|" + ISOUtil.zeropad(new Integer(seq.get ("traceno")).toString(),7) + "\n";
                        }
                    }catch(Exception e){
                        cadRet += "009" + "|" + "\n";
                    }

                    //cadRet += "015" + "|" + "\n";                    
                    
                }    
                else{
                    //cadRet += "009" + "|" + "\n";
                    cadRet += "009" + "|Error Plataforma Prepago COMCEL|" + "\n";
                }
            } else if (sep[4].trim().length() != 10){
                cadRet += "009" + "|" + "\n";   //Celular Inválido
            } else{
                cadRet += "009" + "|" + "\n";   //Celular Inválido
            }
            
            System.out.println(" Auth ID " + i + " - Valor: $" + sep[7]);
            m.set(0, cadRet);
            
            //if (i == 1 || i == 2 )
                //Thread.sleep(2000);        ///Timeout de 12 segundos, luego sí manda la respuesta
            
            if (sep[4].equals("3113333333")){
                //Respuesta Cercana al Timeout, APROBADA
                if (i < 3 ){
                    //No response
                } else {
                    i = 0;
                    source.send (m);
                }
            } else if (sep[4].equals("3118888888")){
                //Respuesta Cercana al Timeout, NO APROBADA
                if (i < 4 ){
                    //No response
                } else {
                    i = 0;
                    source.send (m);
                }
            } else {
                ///Esperar Tiempo Randómico para Pruebas en Campo
                //Thread.sleep(new Random().nextInt(4000));                
                log.info("Data Enviada." + " - " + tcpData + " - " + cadRet.replace("\n", ""));
                source.send (m);
            }            
            
            i++;
            
        } catch (ISOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return true;
    }
    
    protected void finalize() throws Throwable  {
        super.finalize();
        System.runFinalization();
    }    
    
    public static void main (String[] args) throws Exception {      
                
        Logger logger = new Logger ();
        logger.addListener (new SimpleLogListener(System.out));
        ServerChannel channel = new comcelChannel(new comcelPackager(), null);
       
        ((LogSource)channel).setLogger (logger, "CANAL - AUTORIZADOR");
        ISOServer2 server = new ISOServer2 (6666, channel, null);
        //ISOServer2 server = new ISOServer2 (21, channel, null);
        server.setLogger (logger, "AUTORIZADOR");
        server.addISORequestListener (new authTestCOMCEL());
        new Thread (server).start ();
 
        initVal = (int)(Math.random() * 50000 + 1);
        
        seq.set("traceno",initVal); ///Trace Seed
     
        //Ejemplo Log4java
        org.apache.log4j.PropertyConfigurator.configure("./log4j.properties");
        log.info("Autorizador de Pruebas COMCEL iniciado.");
        log.info("Atendiendo Transacciones Puerto TCP 6666.");
    }
}