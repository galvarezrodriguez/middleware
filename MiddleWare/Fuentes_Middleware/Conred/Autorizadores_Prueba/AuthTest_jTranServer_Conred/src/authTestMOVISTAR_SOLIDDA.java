/*
 * authTestTigo.java
 *
 * Created on 22 de noviembre de 2007, 11:41 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin David Beltrán Duque
 */

import java.io.*;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;
import org.jpos.iso.packager.*; 

import org.jpos.core.Sequencer;
import org.jpos.core.VolatileSequencer;
import org.com.jposChannel.*;

public class authTestMOVISTAR_SOLIDDA implements ISORequestListener {
    
    private static int initVal;
    private static Sequencer seq = new VolatileSequencer();
    
    public authTestMOVISTAR_SOLIDDA() {
        super();
    }
    public boolean process (ISOSource source, ISOMsg m) {
        
        try {
            m.setResponseMTI ();
            
            if (m.getMTI().equals("0810")){
                m.set(39, "00");
            }
            else{
                
                String tokens_Field35[] = m.getString(35).split("\\|");
                
                if (tokens_Field35[1].trim().equals("3157777777")){
                    ///Trx. Error
                    m.set(39,"99");
                    m.set(63, "0|0|22|SALDO INSUFICIENTE|14|PIN0000000000000|170|196.00|110908|0|0");
                    source.send (m);
                } else if (tokens_Field35[1].trim().equals("3155555555")){
                    ///Timeout Error

                } else {
                    ///Trx. OK
                    m.set(39,"00");
                    m.set(37, ISOUtil.zeropad(new Integer(seq.get ("traceno")).toString(),6));
                    source.send (m);
                }
            }            
            
        } catch (ISOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } 
        return true;
    }
    
    protected void finalize() throws Throwable  {
        super.finalize();
        System.runFinalization();
    }    
    
    public static void main (String[] args) throws Exception {      
        Logger logger = new Logger();
        logger.addListener (new SimpleLogListener (System.out));
        ServerChannel channel = new ASCIIChannel_ePrepayware(new ISO87A_ePrepaywarePackager());
       
        ((LogSource)channel).setLogger (logger, "CANAL - AUTORIZADOR");
        ISOServer server = new ISOServer (6670, channel, null);
        server.setLogger (logger, "AUTORIZADOR MOVISTAR/SOLIDDA");
        server.addISORequestListener (new authTestMOVISTAR_SOLIDDA());
        new Thread (server).start ();
 
        initVal = (int)(Math.random()*50000 + 1);
        
        seq.set("traceno",initVal); ///Trace Seed
        
    }
    
}