package com.conred.configFile;
/*
 * Configurator.java
 *
 * Created on 22 de noviembre de 2007, 11:10 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

/**
 *
 * @author Elkin David Beltrn Duque
 */

import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory; 
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException; 
import java.io.File;
import java.io.IOException;
import java.io.*;
import java.util.StringTokenizer;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Configurator {

    Document document; 
    org.w3c.dom.Node domNode;
    
    public Configurator(String xmlFile) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.parse( new File(xmlFile) ); ////cambiar xmlFile por fileNameTMP
           
        }catch(ParserConfigurationException e){ e.printStackTrace();}
        catch(SAXException m){ m.printStackTrace();}
        catch(IOException n){ n.printStackTrace();}
        domNode = document;
    }

    public String getValue(String path) {
        String resultado = null;
        StringTokenizer mipath = new StringTokenizer(path, "/");
        Node nodo = domNode; 
        while (nodo != null && mipath.hasMoreTokens() ) {
            NodeList nl = nodo.getChildNodes();
            String token = mipath.nextToken();
            nodo=null; 
            for ( int i=0;i<nl.getLength();i++){
                Node sigNodo =nl.item(i);
                String name = sigNodo.getNodeName();
                if ( name.equals(token)){
                    nodo=sigNodo; 
                    i=nl.getLength();
                }
            }
        }
        if (nodo!=null){
            resultado = nodo.getFirstChild().getNodeValue(); 
        }
        return resultado;
    }
}
