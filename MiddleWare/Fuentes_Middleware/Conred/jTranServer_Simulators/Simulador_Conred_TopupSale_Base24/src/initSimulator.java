/*
 * initSimulator.java
 *
 * Created on 11 de octubre de 2008, 8:41
 */
import com.conred.configFile.Configurator;
import com.conred.jPOSConredChannel.BASE24TCPChannel_Conred;
import com.conred.jPOSConredChannel.ISOMUX_Conred;
import com.conred.logUtilities.LogWriter;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;
import org.jpos.iso.packager.*;
import org.jdom.*;
import org.jdom.output.*;
import org.jdom.output.XMLOutputter;
import java.io.*;
import org.jpos.core.Sequencer;
import org.jpos.core.VolatileSequencer;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.event.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

///User Interface
import java.awt.*;
import javax.swing.*;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import java.util.*;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.awt.event.*;

/**
 *
 * @author  Elkin Beltran
 */
public class initSimulator extends javax.swing.JFrame {
    
    static Configurator config                  = new Configurator("Config.xml");
    static int stan                             = 0;
    static int MAX_FILAS                        = 500;
    static Sequencer seq                        = new VolatileSequencer();
    static int cons_env                         = 0;
    aboutForm aForm                             = new aboutForm(this, true);
    static boolean flagBusy                     = false;
    
    //ISOMUX Channel Conred
    public static ISOChannel channelConredMUX       = null;
    public static ISOMUX_Conred gatewayISOMuxConred = null;    
    
    //Loggers
    public static Logger loggerGateway              = null; 
    public static LogWriter logWriterGateway        = null;  
    
    public static byte[] BASE64_HEADER              = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};    
    
    public static String conred_ip                  = "";
    public static String conred_port                = "";
    
    /** Creates new form initSimulator2 */
    public initSimulator() {
        
        ///Change Look And Feel
        try{
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (Exception e) { e.printStackTrace(); }                
        
        initComponents();
        
        /* Sin Licenciamiento
        if (!checkLicense()){
            JOptionPane.showMessageDialog(this, "Número de Ejecuciones Excedidas.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);
            System.exit(0);        
        }
        */
        
        ///Set the new data      
        String[] ipTot = config.getValue("config/destIP").split("\\.");
        ip1TXT.setText(ipTot[0]);
        ip2TXT.setText(ipTot[1]);
        ip3TXT.setText(ipTot[2]);
        ip4TXT.setText(ipTot[3]);
        
        ip1TXT.setPreferredSize(new Dimension(26, 17));
        ip2TXT.setPreferredSize(new Dimension(26, 17));
        ip3TXT.setPreferredSize(new Dimension(26, 17));
        ip4TXT.setPreferredSize(new Dimension(26, 17));
        
        portTXT.setText(config.getValue("config/destPort"));
        merchantTXT.setText(config.getValue("config/merchant"));
        terminalTXT.setText(config.getValue("config/terminal"));
        serialTXT.setText(config.getValue("config/serial"));
        
        stan = Integer.valueOf(config.getValue("config/stan"));
        seq.set("traceno",stan);     ///Trace Seed
        
        ///Set Table Model
        tablaDatos.getColumnModel().getColumn(0).setPreferredWidth(70);
        tablaDatos.getColumnModel().getColumn(1).setPreferredWidth(40);
        tablaDatos.getColumnModel().getColumn(2).setPreferredWidth(60);
        tablaDatos.getColumnModel().getColumn(3).setPreferredWidth(415);
        tablaDatos.getColumnModel().getColumn(4).setPreferredWidth(415);
        
        setListeners();
        
        setSelection(ip1TXT);
        
        ///Init. Loggers
        try{
            
            loggerGateway = new Logger(); 
            
            ///Create Log Writers
            logWriterGateway = new LogWriter();
            logWriterGateway.setLogger(loggerGateway, "JNTS CHANNEL");            

            SimpleDateFormat formatlogger = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");                
            loggerGateway.addListener (new SimpleLogListener ( new PrintStream ("logs/SimulatorJNTS_" + formatlogger.format(new java.util.Date()) + ".xml") ));        
        }catch(Exception e) {
            e.printStackTrace();
        }        

        ///Start ISOMUX JNTS
        try{

            conred_ip   = ip1TXT.getText().trim() + "." + ip2TXT.getText().trim() + "." + ip3TXT.getText().trim() + "." + ip4TXT.getText().trim();
            conred_port = portTXT.getText().trim();

            channelConredMUX = new BASE24TCPChannel_Conred(conred_ip, Integer.valueOf(conred_port), new BASE24Packager());
            gatewayISOMuxConred = new ISOMUX_Conred(channelConredMUX, isomuxLBL){
                                                            /*Set unique key for every Message*/
                                                            protected String getKey (ISOMsg m) {
                                                                String retKey = "";
                                                                try{
                                                                    ///Outgoing / Incoming, Admin. Messages
                                                                    if (m.getMTI().equals("0800") 
                                                                            || m.getMTI().equals("0810")){
                                                                        retKey = m.getString(11);
                                                                    }
                                                                    else {
                                                                        ///Sale/Financial Messages
                                                                        retKey = m.getString(37);
                                                                    }
                                                                }catch(Exception ex){}                                  
                                                                return retKey;
                                                            }
                                                          };            

            ///Set ISOMux Logger
            ((BaseChannel)channelConredMUX).setLogger(loggerGateway, "JNTS CHANNEL");

            ///Set Header
            ((BaseChannel)channelConredMUX).setHeader( BASE64_HEADER );

            ///Start ISOMux Thread
            new Thread (gatewayISOMuxConred).start();                                                          
                                                          
        }catch(Exception e) {
            e.printStackTrace();
        }        
        
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFileChooser1 = new javax.swing.JFileChooser();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        portTXT = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        ip1TXT = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        ip2TXT = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        ip3TXT = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        ip4TXT = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaDatos = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        serialTXT = new javax.swing.JTextField();
        merchantTXT = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        terminalTXT = new javax.swing.JTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        enviadasLBL = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        recibidasLBL = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        statusLBL = new javax.swing.JLabel();
        isomuxLBL = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        jMenuItem3 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JSeparator();
        jMenuItem6 = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenuItem9 = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JSeparator();
        jMenu3 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Simulador de Transacciones Conred v1.0 - Ventas de Tiempo Aire");
        setBackground(new java.awt.Color(0, 0, 0));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true), "Datos del Servidor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 255, 51)));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel1.setForeground(new java.awt.Color(51, 255, 51));
        jLabel1.setText("IP Servidor:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel2.setForeground(new java.awt.Color(51, 255, 51));
        jLabel2.setText("Puerto TCP:");

        portTXT.setBackground(new java.awt.Color(0, 0, 0));
        portTXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        portTXT.setForeground(new java.awt.Color(51, 255, 51));
        portTXT.setText("6665");
        portTXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));

        jPanel3.setBackground(new java.awt.Color(0, 0, 0));

        ip1TXT.setBackground(new java.awt.Color(0, 0, 0));
        ip1TXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        ip1TXT.setForeground(new java.awt.Color(51, 255, 51));
        ip1TXT.setText("127");
        ip1TXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));
        ip1TXT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ip1TXTActionPerformed(evt);
            }
        });
        ip1TXT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ip1TXTFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                ip1TXTFocusLost(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel3.setForeground(new java.awt.Color(51, 255, 51));
        jLabel3.setText(".");
        jLabel3.setIconTextGap(0);
        jLabel3.setMaximumSize(new java.awt.Dimension(2, 15));

        ip2TXT.setBackground(new java.awt.Color(0, 0, 0));
        ip2TXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        ip2TXT.setForeground(new java.awt.Color(51, 255, 51));
        ip2TXT.setText("127");
        ip2TXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));
        ip2TXT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ip2TXTFocusGained(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel4.setForeground(new java.awt.Color(51, 255, 51));
        jLabel4.setText(".");

        ip3TXT.setBackground(new java.awt.Color(0, 0, 0));
        ip3TXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        ip3TXT.setForeground(new java.awt.Color(51, 255, 51));
        ip3TXT.setText("127");
        ip3TXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));
        ip3TXT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ip3TXTFocusGained(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel5.setForeground(new java.awt.Color(51, 255, 51));
        jLabel5.setText(".");

        ip4TXT.setBackground(new java.awt.Color(0, 0, 0));
        ip4TXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        ip4TXT.setForeground(new java.awt.Color(51, 255, 51));
        ip4TXT.setText("127");
        ip4TXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));
        ip4TXT.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                ip4TXTFocusGained(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(ip1TXT)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ip2TXT)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ip3TXT)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ip4TXT))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ip1TXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ip2TXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ip3TXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(ip4TXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(portTXT, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(portTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setBackground(new java.awt.Color(0, 0, 0));
        jScrollPane1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        tablaDatos.setBackground(new java.awt.Color(0, 0, 0));
        tablaDatos.setForeground(new java.awt.Color(51, 255, 51));
        tablaDatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Celular", "Valor", "ID Ope.", "Trama Enviada", "Trama Recibida", "Tiempo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, true, true, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablaDatos.setToolTipText("Datos a Enviar por el Simulador.");
        tablaDatos.setGridColor(new java.awt.Color(51, 255, 51));
        jScrollPane1.setViewportView(tablaDatos);

        jPanel5.setBackground(new java.awt.Color(0, 0, 0));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true), "Datos del Cliente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 255, 51)));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel6.setForeground(new java.awt.Color(51, 255, 51));
        jLabel6.setText("Comercio Id:");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel7.setForeground(new java.awt.Color(51, 255, 51));
        jLabel7.setText("App. Version:");

        serialTXT.setBackground(new java.awt.Color(0, 0, 0));
        serialTXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        serialTXT.setForeground(new java.awt.Color(51, 255, 51));
        serialTXT.setText("6665");
        serialTXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));

        merchantTXT.setBackground(new java.awt.Color(0, 0, 0));
        merchantTXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        merchantTXT.setForeground(new java.awt.Color(51, 255, 51));
        merchantTXT.setText("0518824875");
        merchantTXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel9.setForeground(new java.awt.Color(51, 255, 51));
        jLabel9.setText("Terminal Id:");

        terminalTXT.setBackground(new java.awt.Color(0, 0, 0));
        terminalTXT.setFont(new java.awt.Font("Tahoma", 1, 12));
        terminalTXT.setForeground(new java.awt.Color(51, 255, 51));
        terminalTXT.setText("25468975");
        terminalTXT.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(merchantTXT, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(terminalTXT, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(serialTXT, javax.swing.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(merchantTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(terminalTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(serialTXT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel6.setBackground(new java.awt.Color(0, 0, 0));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(51, 255, 51), 1, true), "Resultado Simulador", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 255, 51)));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel10.setForeground(new java.awt.Color(51, 255, 51));
        jLabel10.setText("TX. Enviadas:");

        enviadasLBL.setFont(new java.awt.Font("Tahoma", 1, 36));
        enviadasLBL.setForeground(new java.awt.Color(51, 255, 51));
        enviadasLBL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        enviadasLBL.setText("0");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 12));
        jLabel13.setForeground(new java.awt.Color(51, 255, 51));
        jLabel13.setText("Tx. Recibidas:");

        recibidasLBL.setFont(new java.awt.Font("Tahoma", 1, 36));
        recibidasLBL.setForeground(new java.awt.Color(51, 255, 51));
        recibidasLBL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        recibidasLBL.setText("0");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(enviadasLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(recibidasLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(recibidasLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(enviadasLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel13)
                        .addGap(11, 11, 11))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1047, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 414, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(0, 0, 0));

        statusLBL.setFont(new java.awt.Font("Tahoma", 0, 12));
        statusLBL.setForeground(new java.awt.Color(51, 255, 51));
        statusLBL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/procesandoStatic.gif"))); // NOI18N
        statusLBL.setText("Simulador Iniciado.");

        isomuxLBL.setForeground(new java.awt.Color(51, 255, 51));
        isomuxLBL.setIcon(new javax.swing.ImageIcon(getClass().getResource("/stop.png"))); // NOI18N
        isomuxLBL.setText("Estado ISOMUX");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(statusLBL)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 777, Short.MAX_VALUE)
                .addComponent(isomuxLBL, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(statusLBL)
                .addComponent(isomuxLBL))
        );

        jMenu1.setMnemonic('a');
        jMenu1.setText("Archivo");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Salir");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setMnemonic('p');
        jMenu2.setText("Procesos");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_R, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Llenar Datos Randómicos");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem2);

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Agregar Fila");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Borrar Datos Tabla");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);
        jMenu2.add(jSeparator1);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setFont(new java.awt.Font("Tahoma", 1, 11));
        jMenuItem3.setText("Iniciar Stress");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);
        jMenu2.add(jSeparator2);

        jMenuItem6.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem6.setText("Guardar Resultados");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem6);
        jMenu2.add(jSeparator3);

        jMenuBar1.add(jMenu2);

        jMenu4.setText("Msg. Administrativos");

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem7.setText("Conectar por Logon");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem7);

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_D, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setText("Desconectar por Logoff");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem8);

        jMenuItem9.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_T, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem9.setText("Enviar Echo Test");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem9);
        jMenu4.add(jSeparator4);

        jMenuBar1.add(jMenu4);

        jMenu3.setText("Acerca de...");
        jMenu3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu3MousePressed(evt);
            }
        });
        jMenu3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu3ActionPerformed(evt);
            }
        });
        jMenuBar1.add(jMenu3);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        saveConfig();
        incrementLicense();
        System.exit(0);
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void ip1TXTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ip1TXTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ip1TXTActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        setRandomicData();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        addRow();
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        
    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        saveConfig();
        incrementLicense();
    }//GEN-LAST:event_formWindowClosing

    private void ip1TXTFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ip1TXTFocusLost

    }//GEN-LAST:event_ip1TXTFocusLost

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed

        if (gatewayISOMuxConred.isConnected()){
            
            if (isomuxLBL.getText().trim().toLowerCase().equals("logon")){
                enviadasLBL.setText("0");
                recibidasLBL.setText("0");
                startStress();            
            } else {
                JOptionPane.showMessageDialog(this, "Debe estar conectado por Logon para enviar Ventas", "Simulador de Transacciones Conred v1.0", JOptionPane.ERROR_MESSAGE);            
            }
        }
        else {
            JOptionPane.showMessageDialog(this, "ISOMUX No Conectado, Verifique conexión", "Simulador de Transacciones Conred v1.0", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void ip2TXTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ip2TXTFocusGained
        setSelection(ip2TXT);
    }//GEN-LAST:event_ip2TXTFocusGained

    private void ip1TXTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ip1TXTFocusGained
        setSelection(ip1TXT);
    }//GEN-LAST:event_ip1TXTFocusGained

    private void ip3TXTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ip3TXTFocusGained
        setSelection(ip3TXT);
    }//GEN-LAST:event_ip3TXTFocusGained

    private void ip4TXTFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_ip4TXTFocusGained
        setSelection(ip4TXT);
    }//GEN-LAST:event_ip4TXTFocusGained

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        DefaultTableModel model = (DefaultTableModel)tablaDatos.getModel();        
        model.setRowCount(0);        
        addRow();
        enviadasLBL.setText("0");
        recibidasLBL.setText("0");                
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        saveCSVData();
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    private void jMenu3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu3ActionPerformed
        aForm.setLocationWindows();
        aForm.setVisible(true);
    }//GEN-LAST:event_jMenu3ActionPerformed

    private void jMenu3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu3MousePressed
        aForm.setLocationWindows();
        aForm.setVisible(true);
    }//GEN-LAST:event_jMenu3MousePressed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        
        if (gatewayISOMuxConred.isConnected()){
            sendLogon();
        }
        else {
            JOptionPane.showMessageDialog(this, "ISOMUX No Conectado, Verifique conexión", "Simulador de Transacciones Conred v1.0", JOptionPane.ERROR_MESSAGE);
        }        
        
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed

        if (gatewayISOMuxConred.isConnected()){
            sendLogoff();
        }
        else {
            JOptionPane.showMessageDialog(this, "ISOMUX No Conectado, Verifique conexión", "Simulador de Transacciones Conred v1.0", JOptionPane.ERROR_MESSAGE);
        }        
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed

        if (gatewayISOMuxConred.isConnected()){
            sendEchoTest();
        }
        else {
            JOptionPane.showMessageDialog(this, "ISOMUX No Conectado, Verifique conexión", "Simulador de Transacciones Conred v1.0", JOptionPane.ERROR_MESSAGE);
        }                
        
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void setSelection(javax.swing.JTextField obj){
        obj.setSelectionStart(0); 
        obj.setSelectionEnd(obj.getText().length());
    }
    /**
     * setListeners
     * Establece los listeners para las cajas de texto del formulario.
     */
    private void setListeners(){
        ip1TXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n' || caracter == '.'){
                        setSelection(ip2TXT);
                        ip2TXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (ip1TXT.getCaret().getDot() != ip1TXT.getCaret().getMark()){
                        ip1TXT.setText("");
                        return;
                    }                    
                    else if (ip1TXT.getText().length() >= 3){
                        e.consume();                    
                        setSelection(ip2TXT);
                        ip2TXT.requestFocus();
                    }
                    else if (ip1TXT.getText().length() == 2 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(ip2TXT);
                        ip2TXT.requestFocus();
                    }
                }
            }
        });        
        ip2TXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n' || caracter == '.'){
                        setSelection(ip3TXT);
                        ip3TXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (ip2TXT.getCaret().getDot() != ip2TXT.getCaret().getMark()){
                        ip2TXT.setText("");
                        return;
                    }                    
                    else if (ip2TXT.getText().length() >= 3){                        
                        e.consume(); 
                        setSelection(ip3TXT);
                        ip3TXT.requestFocus();
                    }
                    else if (ip2TXT.getText().length() == 2 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(ip3TXT);
                        ip3TXT.requestFocus();
                    }
                }
            }
        });
        ip3TXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n' || caracter == '.'){
                        setSelection(ip4TXT);
                        ip4TXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (ip3TXT.getCaret().getDot() != ip3TXT.getCaret().getMark()){
                        ip3TXT.setText("");
                        return;
                    }                    
                    else if (ip3TXT.getText().length() >= 3){                        
                        e.consume();                    
                        setSelection(ip4TXT);
                        ip4TXT.requestFocus();
                    }
                    else if (ip3TXT.getText().length() == 2 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(ip4TXT);
                        ip4TXT.requestFocus();
                    }
                }
            }
        });
        ip4TXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n' || caracter == '.'){
                        setSelection(portTXT);
                        portTXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (ip4TXT.getCaret().getDot() != ip4TXT.getCaret().getMark()){
                        ip4TXT.setText("");
                        return;
                    }                    
                    else if (ip4TXT.getText().length() >= 3){                        
                        e.consume();                    
                        setSelection(portTXT);
                        portTXT.requestFocus();
                    }
                    else if (ip4TXT.getText().length() == 2 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(portTXT);
                        portTXT.requestFocus();
                    }
                }
            }
        });        
        portTXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n'){
                        setSelection(merchantTXT);
                        merchantTXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (portTXT.getCaret().getDot() != portTXT.getCaret().getMark()){
                        portTXT.setText("");
                        return;
                    }                    
                    else if (portTXT.getText().length() >= 5){                        
                        e.consume();                    
                        setSelection(merchantTXT);
                        merchantTXT.requestFocus();
                    }
                    else if (portTXT.getText().length() == 4 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(merchantTXT);
                        merchantTXT.requestFocus();
                    }
                }
            }
        });
        merchantTXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n'){
                        setSelection(terminalTXT);
                        terminalTXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (merchantTXT.getCaret().getDot() != merchantTXT.getCaret().getMark()){
                        merchantTXT.setText("");
                        return;
                    }                    
                    else if (merchantTXT.getText().length() >= 10){                        
                        e.consume();                    
                        setSelection(terminalTXT);
                        terminalTXT.requestFocus();
                    }
                    else if (merchantTXT.getText().length() == 9 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(terminalTXT);
                        terminalTXT.requestFocus();
                    }
                }
            }
        });
        terminalTXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n'){
                        setSelection(serialTXT);
                        serialTXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (terminalTXT.getCaret().getDot() != terminalTXT.getCaret().getMark()){
                        terminalTXT.setText("");
                        return;
                    }                    
                    else if (terminalTXT.getText().length() >= 8){                        
                        e.consume();
                        setSelection(serialTXT);
                        serialTXT.requestFocus();
                    }
                    else if (terminalTXT.getText().length() == 7 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(serialTXT);
                        serialTXT.requestFocus();
                    }
                }
            }
        });       
        serialTXT.addKeyListener(new KeyAdapter(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    if (caracter == '\n'){
                        setSelection(ip1TXT);
                        ip1TXT.requestFocus();
                    }
                    e.consume();
                }
                else{                   
                    if (serialTXT.getCaret().getDot() != serialTXT.getCaret().getMark()){
                        serialTXT.setText("");
                        return;
                    }                    
                    else if (serialTXT.getText().length() >= 16){                        
                        e.consume();
                        setSelection(ip1TXT);
                        ip1TXT.requestFocus();
                    }
                    else if (serialTXT.getText().length() == 15 && (caracter != KeyEvent.VK_BACK_SPACE)){
                        setSelection(ip1TXT);
                        ip1TXT.requestFocus();
                    }
                }
            }
        });                
        JTextField celular = new TextCellEditer();
        celular.addKeyListener(new KeyListener(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    e.consume();
                }
                else if (((JTextField)e.getSource()).getCaret().getDot() != ((JTextField)e.getSource()).getCaret().getMark()){
                    ((JTextField)e.getSource()).setText("");
                    return;
                }                
                else if ( ((JTextField)e.getSource()).getText().length() >= 10){
                    e.consume();                    
                }                
            }
            public void keyReleased(KeyEvent e){}
            public void keyPressed(KeyEvent e){}
        });
        JTextField valor = new TextCellEditer();
        valor.addKeyListener(new KeyListener(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    e.consume();
                }
                else if (((JTextField)e.getSource()).getCaret().getDot() != ((JTextField)e.getSource()).getCaret().getMark()){
                    ((JTextField)e.getSource()).setText("");
                    return;
                }                
                else if ( ((JTextField)e.getSource()).getText().length() >= 5){
                    e.consume();                    
                }                
            }
            public void keyReleased(KeyEvent e){}
            public void keyPressed(KeyEvent e){}
        });        
        JTextField idOpe = new TextCellEditer();
        idOpe.addKeyListener(new KeyListener(){
            public void keyTyped(KeyEvent e){
                char caracter = e.getKeyChar();
                if(((caracter < '0') || (caracter > '9')) && (caracter != KeyEvent.VK_BACK_SPACE)){
                    e.consume();
                }
                else if (((JTextField)e.getSource()).getCaret().getDot() != ((JTextField)e.getSource()).getCaret().getMark()){
                    ((JTextField)e.getSource()).setText("");
                    return;
                }                
                else if ( ((JTextField)e.getSource()).getText().length() >= 3){
                    ((JTextField)e.getSource()).setText(((JTextField)e.getSource()).getText().substring(0, 3));
                    e.consume();                    
                }                
            }
            public void keyReleased(KeyEvent e){}
            public void keyPressed(KeyEvent e){}
        });
               
        DefaultCellEditor celularCell = new DefaultCellEditor(celular);
        tablaDatos.getColumn("Celular").setCellEditor(celularCell);
        
        DefaultCellEditor valorCell = new DefaultCellEditor(valor);
        tablaDatos.getColumn("Valor").setCellEditor(valorCell);

        String[] operadores = {"COMC", "MOVI", "TIGO"};
        TableColumn col = tablaDatos.getColumnModel().getColumn(2);
        col.setCellEditor(new MyComboBoxEditor(operadores));
        col.setCellRenderer(new MyComboBoxRenderer(operadores));        
        
    }    

private class TextCellEditer extends JTextField {
    public TextCellEditer () {
        addAncestorListener( new AncestorListener(){
            public void ancestorAdded(AncestorEvent e){
                requestFocus();
            }
            public void ancestorMoved(AncestorEvent e){}
            public void ancestorRemoved(AncestorEvent e){}
        });
    }
}    

public class MyComboBoxRenderer extends JComboBox implements TableCellRenderer {
    public MyComboBoxRenderer(String[] items) {
        super(items);
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        if (isSelected) {
            setForeground(table.getSelectionForeground());
            super.setBackground(table.getSelectionBackground());
        } else {
            setForeground(table.getForeground());
            setBackground(table.getBackground());
        }

        // Select the current value
        setSelectedItem(value);
        return this;
    }
}
    
public class MyComboBoxEditor extends DefaultCellEditor {
    public MyComboBoxEditor(String[] items) {
        super(new JComboBox(items));
    }
}

    private boolean checkLicense(){
        RegQuery rq = new RegQuery();
        
        int sumExec = rq.getSoftwareKey();
        
        if (sumExec == 0){
            //Set the registry Key for the firts time
            int state = rq.setSoftwareKey(1);
            
            if (state == 1){
                JOptionPane.showMessageDialog(this, "Número de Ejecuciones Restantes: 24", "Simulador de Transacciones Conred v1.0", JOptionPane.INFORMATION_MESSAGE);
                return true;
            }            
        }
        else if (sumExec >= 25){
            return false;    
        }
        JOptionPane.showMessageDialog(this, "Número de Ejecuciones Restantes: " + (25-sumExec), "Simulador de Transacciones Conred v1.0", JOptionPane.INFORMATION_MESSAGE);
        return true;
    }

    private boolean incrementLicense(){
        RegQuery rq = new RegQuery();
        
        int sumExec = rq.getSoftwareKey();
        
        sumExec++;
        
        //Set the registry Key Add
        int state = rq.setSoftwareKey(sumExec);

        if (state == 1){
            return true;
        }
        return false;
    }
    
    private int generateRandomOpeId(){
        double rndOpe = 9;  ///Centinela
      
        while(rndOpe > 3)
            rndOpe = Math.random() * 10;
        
        return (int)rndOpe;    
    }

    private int generateRandomAmount(){
        double rndAmt = 99;  ///Centinela
      
        while(rndAmt>15)
            rndAmt = Math.random() * 100;
        
        return (int)rndAmt;    
    }    
    
    private void setRandomicData(){
        //100: Comcel , 110: Movistar , 120: Tigo
        String[] operadores = {"100", "110", "120"};
        String[] operadoresN = {"COMC", "MOVI", "TIGO"};
        String[] prefijos = {"310", "315", "301"};  ///Dinamicos
        //String[] prefijos = {"355", "355", "355"};  ///Estaticos Comcel Error
        //String[] prefijos = {"310", "311", "312"};  ///Estaticos SOLO Comcel
        
        String[] montos = {"1000", "2000", "5000", "3000", "5000", "1000", "2000", "5000", "1000", "3000", "1000", "3000", "2000", "1000", "10000"}; //Dinamicos
        //String[] montos = {"5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000", "5000"}; //Estaticos 1000
        
        String celular = "";
        String operador = "";
        String valor = "";
        
        int rndOpe = 0;        
        int rndAmt = 0;
        int idx = 0;
        double cel = 0;
        
        String str = JOptionPane.showInputDialog(this, "Digite Número de Filas a insertar ( < " + MAX_FILAS + " ) : ", "Simulador de Transacciones Conred v1.0", 1);        

        if(str == null){
            JOptionPane.showMessageDialog(this, "Debe digitar un número de Filas a insertar.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);
            return;
        }      

        DefaultTableModel model = (DefaultTableModel)tablaDatos.getModel();        
        model.setRowCount(0);        
        
        enviadasLBL.setText("0");
        recibidasLBL.setText("0");                
        
        int filasT=0;
        try{       
            filasT = Integer.valueOf(str);
        }catch(Exception e){
            JOptionPane.showMessageDialog(this, "Debe digitar un valor correcto de filas.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);            
            return;        
        }
        
        if (filasT <= 0 || filasT > MAX_FILAS){
            JOptionPane.showMessageDialog(this, "Debe digitar un número de Filas menores a 100.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);            
            return;
        }
        
        for (idx=0; idx<filasT; idx++){
            do{
                rndOpe = generateRandomOpeId(); //Dinamico            
            }while( rndOpe > 3 ); ///NO TIGO NI UFF
            
            //rndOpe = 0; //Estatico comcel
            //rndOpe = 1; //Estatico movistar
            //rndOpe = 2; //Estatico tigo
            //rndOpe = 3;  //Estatico Avantel
           
            rndAmt = generateRandomAmount();

            operador = operadoresN[rndOpe];
            
            cel = Math.random() * 5000000;   ///Randomico
            
            //cel = 5593220;                    ///Estático Avantel
            //cel = 9999999;                    ///Estático Tigo
            //cel = 7690112;                      ///Fijo Pruebas Tigo
            
            if (rndOpe == 0){ //Movistar
                //cel = 1000200;
                //cel = 2222222;
            }
            
            try{
                celular = prefijos[rndOpe] + ISOUtil.zeropad(String.valueOf((int)cel),7);
            }catch(Exception e){}            
            
            //valor = "1000";
            valor = montos[rndAmt];     //Dinámico

            Vector columnas=new Vector();       
            columnas.add(0,celular);
            columnas.add(1,valor);
            columnas.add(2,operador);
            columnas.add(3,"");
            columnas.add(4,"");
            columnas.add(5,"");
            model.addRow(columnas);   
        }
    }
    
    private void addRow(){
        DefaultTableModel model = (DefaultTableModel)tablaDatos.getModel();
        
        if (model.getRowCount() < MAX_FILAS){
            Vector columnas=new Vector();       
            columnas.add(0,"");
            columnas.add(1,"");
            columnas.add(2,null);
            columnas.add(3,"");
            columnas.add(4,"");
            columnas.add(5,"");
            model.addRow(columnas);
        }
        else{
            JOptionPane.showMessageDialog(this, "Número Máximo de Filas Alcanzado (Max = 100).", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);  
        }
    }
    
    private void saveConfig(){
        //Create a root element named config
        Element root = new Element("config");
        Format format = Format.getPrettyFormat();
        format.setEncoding("windows-1252");
        
        ///Default Values
        if (ip1TXT.getText().trim().equals(""))
            ip1TXT.setText("127");
        
        if (ip2TXT.getText().trim().equals(""))
            ip2TXT.setText("0");

        if (ip3TXT.getText().trim().equals(""))
            ip3TXT.setText("0");        

        if (ip4TXT.getText().trim().equals(""))
            ip4TXT.setText("1");        
        
        if (portTXT.getText().trim().equals(""))
            portTXT.setText("1001");

        if (merchantTXT.getText().trim().equals(""))
            merchantTXT.setText("1");
        
        if (serialTXT.getText().trim().equals(""))
            serialTXT.setText("0000000010145584");

        if (terminalTXT.getText().trim().equals(""))
            terminalTXT.setText("1");

        double f = 0;        
        if (stan == 0){
            f = Math.random() * 50000;        
            stan = (int)f;
        }
        else{
            stan = seq.get("traceno");        
        }
        
        //add configuration properties
        root.addContent(new Element("destIP").setText(ip1TXT.getText() + "." + ip2TXT.getText() + "." + ip3TXT.getText() + "." + ip4TXT.getText()));
        root.addContent(new Element("destPort").setText(portTXT.getText()));
        root.addContent(new Element("merchant").setText(merchantTXT.getText()));
        root.addContent(new Element("terminal").setText(terminalTXT.getText()));
        root.addContent(new Element("serial").setText(serialTXT.getText()));
        root.addContent(new Element("stan").setText(String.valueOf(stan)));
        
        Document doc=new Document(root);//Create document

        try{
          XMLOutputter out=new XMLOutputter(format); 

          FileOutputStream file=new FileOutputStream("Config.xml");
          out.output(doc,file);
          file.flush();
          file.close();          
        }catch(Exception e){e.printStackTrace();}        
    }
    
    private boolean validateDataTable(){
        DefaultTableModel model = (DefaultTableModel)tablaDatos.getModel();
        int i=0;
        String col1 = "";
        String col2 = "";
        String col3 = "";
        
        for (i=0; i<model.getRowCount(); i++){
            col1 = (String)tablaDatos.getValueAt(i, 0);
            col2 = (String)tablaDatos.getValueAt(i, 1);
            col3 = (String)tablaDatos.getValueAt(i, 2);
            
            if (col1 == null || col2 == null || col3 == null || 
                 col1.length() != 10 || Integer.valueOf(col2) < 100 || Integer.valueOf(col2) > 50000 ||
                  (!col3.equals("COMC") && !col3.equals("MOVI") && !col3.equals("TIGO"))
                    ){
                return false;
            }
        }
        return true;        
    } 

    private boolean validateOneByteIP(JTextField obj){
        String ipS = obj.getText().trim();
        int ipT = 0;
        
        if (ipS.trim().equals(""))
            ipT = 0;
        else
            ipT = Integer.valueOf(ipS);
        
        if (ipT > 255 || (ipS.substring(0, 1).equals("0") && ipS.length() > 1)){
            return false;
        }
        else if (obj.equals(ip1TXT)){
            if (ipT <= 0)
                return false;
        }        
        return true;        
    }
    
    private boolean validateIP(){
        if (!validateOneByteIP(ip1TXT) || !validateOneByteIP(ip2TXT) || !validateOneByteIP(ip3TXT) || !validateOneByteIP(ip4TXT))
            return false;
        return true;
    }
    
    private boolean validateAditionalData(){        
        if (merchantTXT.getText().trim().equals("") || serialTXT.getText().trim().equals("") || terminalTXT.getText().trim().equals("") )
            return false;        
        return true;
    }
    
    private void startStress(){
        sender s = new sender();
        new Thread(s).start();
    }
    
    private boolean validateResultData(){
        DefaultTableModel model = (DefaultTableModel)tablaDatos.getModel();
        int i=0;
        String col4 = "";
        String col5 = "";
        
        if (enviadasLBL.getText().trim().equals("0") || recibidasLBL.getText().trim().equals("0"))
            return false;
        
        for (i=0; i<model.getRowCount(); i++){
            col4 = (String)tablaDatos.getValueAt(i, 3);
            col5 = (String)tablaDatos.getValueAt(i, 4);
            
            if (col4 == null || col5 == null){
                return false;
            }
        }
        return true;        
    }    
    
    private void saveCSVData(){    
        if (!validateResultData()){
            JOptionPane.showMessageDialog(this, "No hay datos para guardar.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);            
            return;
        }
        JFileChooser jf = new JFileChooser();       
        jf.setFileFilter(new FileFilter() {
           public String getDescription() { return "Archivo Separado por Comas(*.csv)"; }
           public boolean accept(File f) {
              if(f.isDirectory()) return true;
              if(f.getName().endsWith(".csv")) return true;
              return false;
            }
        });
        int opt = jf.showSaveDialog(this);

        if (opt == 0){
            ///Aceptó guardar
            boolean flag = false;
            FileWriter arch = null;
            try{                
                String nomArc = jf.getSelectedFile().getAbsolutePath();
                
                if (nomArc.indexOf(".") < 0){
                    ///Archivo sin Extensión
                    nomArc += ".csv";
                }
                
                File fA = new File(nomArc);
                arch = new FileWriter(fA, true);
                
                String linea = "CELULAR;VALOR;ID. OPE; TRAMA ENVIADA; TRAMA RECIBIDA;TIEMPO;\r\n";
                arch.write(linea);
                
                for(int i=0; i<tablaDatos.getModel().getRowCount(); i++){
                    linea = "";
                    linea = (String)tablaDatos.getValueAt(i, 0) + ";";
                    linea += (String)tablaDatos.getValueAt(i, 1) + ";";
                    linea += (String)tablaDatos.getValueAt(i, 2) + ";";
                    linea += (String)tablaDatos.getValueAt(i, 3) + ";";
                    linea += (String)tablaDatos.getValueAt(i, 4) + ";";
                    linea += (String)tablaDatos.getValueAt(i, 5) + ";";
                    linea += "\r\n";
                    arch.write(linea);
                }                
                flag = true;
            }catch(Exception e){
                e.printStackTrace();
                flag = false;
            }
            finally{
                try{
                    arch.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
                
                if (flag)
                    JOptionPane.showMessageDialog(this, "Archivo Guardado Correctamente.", "Simulador de Transacciones Conred v1.0", JOptionPane.INFORMATION_MESSAGE);
                else
                    JOptionPane.showMessageDialog(this, "Error Guardando el Archivo.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);
            }
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {               
                initSimulator simulator = new initSimulator();
                simulator.setIconImage(new ImageIcon(getClass().getResource("icoStress.png")).getImage());
                Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                int width = simulator.getSize().width;
                int height = simulator.getSize().height;
                int x = (dim.width - width) / 2;
                int y = (dim.height - height) / 2;
                simulator.setLocation(x, y);
                simulator.setVisible(true);                                
            }
        });
    }
    
    public void runTest(int id, int filas){
        try{

            int stanTx      = 0;
            ISOChannel emulador = null;
            String ip       = "";
            int port        = 0;
            String celular  = "";
            String valor    = "";
            String operador = "";
            String merchant = "";
            String serial   = "";
            String terminal = "";
            byte[] TPDU = {0x60, 0x00, 0x01, 0x00, 0x00};
            SimpleDateFormat formatField7 = new SimpleDateFormat("ddMMyyyyHH");    
            SimpleDateFormat formatField12 = new SimpleDateFormat("HHmmss");
            SimpleDateFormat formatField13 = new SimpleDateFormat("MMdd");
            java.util.Date Dnow = new java.util.Date();                
                    
            synchronized (jPanel2){
                stanTx      = seq.get("traceno");
                ip          = ip1TXT.getText().trim() + "." + ip2TXT.getText().trim() + "." + ip3TXT.getText().trim() + "." + ip4TXT.getText().trim();
                port        = Integer.valueOf(portTXT.getText().trim());
                emulador    = new NACChannel(ip, port, new ISO87BPackager(), TPDU);
            }

            synchronized (jPanel5){
                merchant    = merchantTXT.getText().trim();
                serial      = serialTXT.getText().trim();
                terminal    = terminalTXT.getText().trim();
            }
            
            synchronized (tablaDatos){
                celular     = ((String)tablaDatos.getValueAt(id, 0)).trim();
                valor       = ((String)tablaDatos.getValueAt(id, 1)).trim();
                operador    = ((String)tablaDatos.getValueAt(id, 2)).trim();
                
                if (operador.equals("MOVI"))
                    operador = "44";
                else if (operador.equals("COMC"))
                    operador = "45";
                else if (operador.equals("TIGO"))
                    operador = "46";                
                else if (operador.equals("AVAN"))
                    operador = "48";
            }

            ((BaseChannel)emulador).setTimeout(130000);
            
            emulador.connect ();
            
            ISOMsg m = new ISOMsg ();
            
            ///Armar Mensaje ISO 8583
            m.setMTI ("0200");      //FIJO
            m.set(3, "003003");     //FIJO
            m.set(4, valor + "00");    //VALOR
            m.set(7, formatField7.format(Dnow));    //FECHA HORA: ddmmaaaaHH
            m.set(11, ISOUtil.zeropad(String.valueOf(stanTx), 6) ); //Stan
            m.set(12, formatField12.format(Dnow));  //Hora
            m.set(13, formatField13.format(Dnow));  //Fecha
            m.set(24, "001");       //FIJO
            m.set(41, ISOUtil.zeropad(terminal, 8));  ///TERMINAL ID 
            m.set(42, "075" + ISOUtil.zeropad(merchant, 12));   ///COMERCIO ID
            //TProd[2]  , 44: Movistar , 45: Comcel , 46: Tigo
            m.set(48, "075" + "01" + operador + "00");  ///TEmisor[3] + VTrama[2] + TProd[2] + Trx[2] 
            m.set(61, ISOUtil.zeropad(serial, 16) + "PC" + "PC" + "75.02");  //N/S Terminal[16] + Marca[2] + Tipo[2] + Vers[5]
            m.set(62, ISOUtil.zeropad(celular, 10));  //NumTel[10] , formato JDRCI
            
            synchronized(tablaDatos){
                tablaDatos.setValueAt(
                        "[MTI]:" + m.getMTI() +
                        "  [3]:" + m.getString(3) +
                        "  [4]:" + valor +
                        "  [41]:" + m.getString(41) +
                        "  [42]:" + m.getString(42) +
                        "  [48]:" + m.getString(48) + 
                        "  [61]:" + m.getString(61) + 
                        "  [62]:" + m.getString(62) 
                        , id, 3);
            }

            synchronized(jPanel6){            
                enviadasLBL.setText(String.valueOf(Integer.valueOf(enviadasLBL.getText()) + 1));
            }
            
            synchronized(jPanel4){
                statusLBL.setText("Enviando Transacción " + id);                
            }
            java.util.Date t1, t2;
            t1 = new java.util.Date(); 
                    
            emulador.send (m);

            ISOMsg response = emulador.receive();
                
            String resT = response.getString(63);   ///Mensaje de Respuesta        

            t2 = new java.util.Date(); 
            
            long totTiempo = t2.getTime() - t1.getTime();
            float real_time = (float)totTiempo / 1000;            
            
            synchronized(tablaDatos){
                tablaDatos.setValueAt(resT, id, 4);
                tablaDatos.setValueAt( real_time + " " + "Seg.", id, 5);
            }

            synchronized(jPanel6){                
                if (flagBusy)
                    jPanel6.wait();
                flagBusy = true;
                recibidasLBL.setText(String.valueOf(Integer.valueOf(recibidasLBL.getText()) + 1));
                flagBusy = false;
                jPanel6.notifyAll();
            }
            
            synchronized(jPanel4){
                statusLBL.setText("Recibiendo Transacción " + id);
            }            
     
            emulador.disconnect();           
        }catch(Exception e){
            
            //Error de respuesta
            synchronized(this){
                tablaDatos.setValueAt("Servidor Inactivo", id, 4);
                statusLBL.setText("Recibiendo Transacción " + id);
            }            
            
            synchronized(jPanel6){                
                try{
                    if (flagBusy)
                        jPanel6.wait();
                }catch(Exception ep){}
                flagBusy = true;
                recibidasLBL.setText(String.valueOf(Integer.valueOf(recibidasLBL.getText()) + 1));
                flagBusy = false;
                jPanel6.notifyAll();
            }
            
        }
        finally{
            synchronized(this){
                cons_env++;
                if (id + 1 >= tablaDatos.getModel().getRowCount()){
                    synchronized(jPanel4){
                        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
                        statusLBL.setText("Proceso Finalizado");    
                    }                    
                }
                else if (cons_env >= filas){
                    synchronized(jPanel6){
                        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
                        statusLBL.setText("Proceso Finalizado");    
                    }                    
                } 
            }        
        }
    }    
    
public class sender implements Runnable{

    public void run(){
        
        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesando.gif")));
        statusLBL.setText("Enviando transacciones");
        
        DefaultTableModel model = (DefaultTableModel)tablaDatos.getModel();
        int filas = model.getRowCount();
        
        if (!validateDataTable()){
            JOptionPane.showMessageDialog(null, "Tabla de Datos incompleta o con valores erróneos. Por favor revisela!!!.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);                    
            statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
            statusLBL.setText("Datos Erróneos");
            return;
        }
        
        if (filas <= 0){
            JOptionPane.showMessageDialog(null, "Debe ingrsar al menos una fila de datos!!!.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);        
            statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
            statusLBL.setText("Datos Erróneos");            
            return;                                    
        }

        if (!validateIP()){
            JOptionPane.showMessageDialog(null, "Ip inválida. Por favor revise los datos!!!.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);                
            statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
            statusLBL.setText("Datos Erróneos");            
            return;
        }
        
        if (Integer.valueOf(portTXT.getText().trim())<=0 || Integer.valueOf(portTXT.getText().trim())>=65535 ){
            JOptionPane.showMessageDialog(null, "Puerto inválido. Por favor revise los datos!!!.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);                            
            statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
            statusLBL.setText("Datos Erróneos");            
            return;
        }
        
        if (!validateAditionalData()){
            JOptionPane.showMessageDialog(null, "Datos del Cliente inválidos. Por favor revise estos datos!!!.", "Simulador de Transacciones Conred v1.0", JOptionPane.WARNING_MESSAGE);                        
            statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
            statusLBL.setText("Datos Erróneos");
            return;
        }
        
        statusLBL.setText("Procesando Solicitudes...");
        
        ///Todo Ok               
        int i=0;

        boolean flagSimul = true;   ///Simultáneas Por Default
        
        int seleccion = JOptionPane.showOptionDialog(
           null,
           "Tipo de Simulación: ", 
           "Seleccione Tipo de Simulación",
           JOptionPane.YES_NO_CANCEL_OPTION,
           JOptionPane.QUESTION_MESSAGE,
           null,    // null para icono por defecto.
           new Object[] { "Simultáneo", "Secuencial" },   // null para YES, NO y CANCEL
           "Simultáneo");

        if (seleccion == 0)
            flagSimul = true;
        else
            flagSimul = false;
        
        if (flagSimul){
            ///Enviar Transacciones Simultáneas
            for(i=0; i<filas; i++){
                launcher lT = new launcher(i,filas);
                new Thread(lT).start();
                try{
                    //Thread.sleep(500);       ///50 Milisegundos de Espera
                }catch(Exception e){
                    e.printStackTrace();
                }
            }    
        } else {
            ///Enviar Transacciones Una a Una
            for(i=0; i<filas; i++){
                
                doStep(i, filas);
                
                try{
                    Thread.sleep(500);       ///50 Milisegundos de Espera entre Transacción
                }catch(Exception e){ }                
                
            }
        }

    }

}    

    public void sendLogon(){
        try{

            int stanTx      = 0;
            byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
            
            synchronized (jPanel5){
                stanTx      = seq.get("traceno");
            }

            ISOMsg m = new ISOMsg ();

            ///Armar Mensaje ISO 8583
            m.setPackager(new BASE24Packager());
            m.setHeader(BASE64_HEADER);
            m.setMTI ("0800");      //FIJO
            m.set (7, ISODate.getDateTime (new java.util.Date()));
            m.set (11, ISOUtil.zeropad(String.valueOf(stanTx), 6) ); //STAN
            m.set (70, "001");          //LOGON

            synchronized(jPanel4){
                statusLBL.setText("Enviando Logon");
            }
            
            java.util.Date t1, t2;
            t1 = new java.util.Date(); 

            ///Enviar Logon a través del ISOMUX Conred
            ISORequest request = new ISORequest(m);
            gatewayISOMuxConred.queue(request);                     
            ISOMsg response = request.getResponse(30000);

            String resT = "";
            
            if (response != null){
                if (response.getString(39).equals("00")){
                    resT = "Logon Autorizado";
                    isomuxLBL.setText("LOGON");
                } else {
                    resT = "Logon NO Autorizado";
                }
            } else {
                resT = "Timeout Alcanzado";
            }
            
            t2 = new java.util.Date(); 

            long totTiempo = t2.getTime() - t1.getTime();
            float real_time = (float)totTiempo / 1000;            
          
            synchronized(jPanel4){
                statusLBL.setText(resT + ". Procesado en " + real_time + " Segs.");
            }                        
            
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public void sendLogoff(){
        try{

            int stanTx      = 0;
            byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
            
            synchronized (jPanel5){
                stanTx      = seq.get("traceno");
            }

            ISOMsg m = new ISOMsg ();

            ///Armar Mensaje ISO 8583
            m.setPackager(new BASE24Packager());
            m.setHeader(BASE64_HEADER);
            m.setMTI ("0800");      //FIJO
            m.set (7, ISODate.getDateTime (new java.util.Date()));
            m.set (11, ISOUtil.zeropad(String.valueOf(stanTx), 6) ); //STAN
            m.set (70, "002");          //LOGOFF

            synchronized(jPanel4){
                statusLBL.setText("Enviando Logoff");
            }
            
            java.util.Date t1, t2;
            t1 = new java.util.Date(); 

            ///Enviar Logon a través del ISOMUX Conred
            ISORequest request = new ISORequest(m);
            gatewayISOMuxConred.queue(request);                     
            ISOMsg response = request.getResponse(30000);

            String resT = "";
            
            if (response != null){
                if (response.getString(39).equals("00")){
                    resT = "Logoff Autorizado";
                    isomuxLBL.setText("LOGOFF");
                } else {
                    resT = "Logoff NO Autorizado";
                }
            } else {
                resT = "Timeout Alcanzado";
            }
            
            t2 = new java.util.Date(); 

            long totTiempo = t2.getTime() - t1.getTime();
            float real_time = (float)totTiempo / 1000;            
          
            synchronized(jPanel4){
                statusLBL.setText(resT + ". Procesado en " + real_time + " Segs.");
            }                        
            
        }catch(Exception e){
            e.printStackTrace();
        }

    }    

    public void sendEchoTest(){
        try{

            int stanTx      = 0;
            byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
            
            synchronized (jPanel5){
                stanTx      = seq.get("traceno");
            }

            ISOMsg m = new ISOMsg ();

            ///Armar Mensaje ISO 8583
            m.setPackager(new BASE24Packager());
            m.setHeader(BASE64_HEADER);
            m.setMTI ("0800");      //FIJO
            m.set (7, ISODate.getDateTime (new java.util.Date()));
            m.set (11, ISOUtil.zeropad(String.valueOf(stanTx), 6) ); //STAN
            m.set (70, "301");          //ECHO TEST

            synchronized(jPanel4){
                statusLBL.setText("Enviando Echo Test");
            }
            
            java.util.Date t1, t2;
            t1 = new java.util.Date(); 

            ///Enviar Logon a través del ISOMUX Conred
            ISORequest request = new ISORequest(m);
            gatewayISOMuxConred.queue(request);                     
            ISOMsg response = request.getResponse(30000);

            String resT = "";
            
            if (response != null){
                if (response.getString(39).equals("00")){
                    resT = "Echo Test Autorizado";
                } else {
                    resT = "Echo Test NO Autorizado";
                }
            } else {
                resT = "Timeout Alcanzado";
            }
            
            t2 = new java.util.Date(); 

            long totTiempo = t2.getTime() - t1.getTime();
            float real_time = (float)totTiempo / 1000;            
          
            synchronized(jPanel4){
                statusLBL.setText(resT + ". Procesado en " + real_time + " Segs.");
            }                        
            
        }catch(Exception e){
            e.printStackTrace();
        }

    }        
    
    public void doStep(int id, int filas){
        try{

            int stanTx      = 0;
            String ip       = "";
            String celular  = "";
            String valor    = "";
            String operador = "";
            String merchant = "";
            String terminal = "";
            byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
            
            synchronized (jPanel5){
                stanTx      = seq.get("traceno");
                merchant    = merchantTXT.getText().trim();
                terminal    = terminalTXT.getText().trim();
            }

            synchronized (tablaDatos){
                celular     = ((String)tablaDatos.getValueAt(id, 0)).trim();
                valor       = ((String)tablaDatos.getValueAt(id, 1)).trim();
                operador    = ((String)tablaDatos.getValueAt(id, 2)).trim();

                if (operador.equals("COMC"))
                    operador = "770717532280";
                else if (operador.equals("MOVI"))
                    operador = "770717696219";
                else if (operador.equals("TIGO"))
                    operador = "770213800506";

            }

            ISOMsg m = new ISOMsg ();

            ///Armar Mensaje ISO 8583
            m.setPackager(new BASE24Packager());
            m.setHeader(BASE64_HEADER);
            m.setMTI ("0200");      //FIJO
            m.set(3, "380000");     //FIJO
            m.set(4, valor + "00");    //VALOR
            m.set (7, ISODate.getDateTime (new java.util.Date()));
            m.set(11, ISOUtil.zeropad(String.valueOf(stanTx), 6) ); //STAN
            m.set (12, ISODate.getTime (new java.util.Date()));
            m.set (13, ISODate.getDate (new java.util.Date()));
            m.set (17, ISODate.getDate (new java.util.Date()));
            m.set (22, "010");          //FIJO
            m.set (32, "10000000005");  //FIJO
            m.set (35, "940000" + ISOUtil.strpad(terminal, 8) + "=4912101               ");    //FIJO
            m.set (37, ISOUtil.zeropad(String.valueOf(stanTx), 12));    //RRN
            m.set (41, ISOUtil.strpad(terminal, 16));
            m.set (42, ISOUtil.strpad("", 15));
            m.set (43, "SUPERMERCADO SAS      1100100BOGOTACUNCO");            
            m.set (48, ISOUtil.strpad(merchant, 10) + "         00250001");
            m.set (49, "170");                  //FIJO
            m.set (60, "0005FINA+0000000");     //FIJO           
            m.set (61, "0000    00000000000");  //FIJO
            ///EAN POR PRODUCTO Y CELULAR A RECARGAR
            //COMCEL=770717532280 ; TIGO=770213800506 ; MOVISTAR=770717696219
            m.set (63, "& 0000200058! P000034 " + ISOUtil.strpad(operador, 19) + ISOUtil.padleft(celular, 15, ' ') + "P ");
            m.set (100, "10000000000");         //FIJO
            m.set (128, "A0CD4B0100000000");    //FIJO - SIN MAC

            synchronized(tablaDatos){
                tablaDatos.setValueAt(
                        "[MTI]:" + m.getMTI() +
                        "  [3]:" + m.getString(3) +
                        "  [4]:" + valor +
                        "  [11]:" + m.getString(11) +
                        "  [37]:" + m.getString(37) +
                        "  [41]:" + m.getString(41).trim() +
                        "  [48]:" + m.getString(48) + 
                        "  [63]:" + m.getString(63)
                        , id, 3);
            }

            synchronized(jPanel6){            
                enviadasLBL.setText(String.valueOf(Integer.valueOf(enviadasLBL.getText()) + 1));
            }

            synchronized(jPanel4){
                statusLBL.setText("Enviando Transacción " + id);                
            }
            java.util.Date t1, t2;
            t1 = new java.util.Date(); 

            ///Enviar Venta a través del ISOMUX Conred
            ISORequest request = new ISORequest(m);
            gatewayISOMuxConred.queue(request);                     
            ISOMsg response = request.getResponse(30000);

            String resT = "";
            
            if (response != null){
                resT =  "[MTI]:" + response.getMTI() +
                        "  [3]:" + response.getString(3) +
                        "  [11]:" + response.getString(11) +
                        "  [37]:" + response.getString(37) +
                        "  [38]:" + response.getString(38) +
                        "  [39]:" + response.getString(39) +
                        "  [41]:" + response.getString(41).trim() +
                        "  [48]:" + response.getString(48) +
                        "  [63]:" + response.getString(63);                
            } else {
                resT = "Timeout Alcanzado";
            }
            
            t2 = new java.util.Date(); 

            long totTiempo = t2.getTime() - t1.getTime();
            float real_time = (float)totTiempo / 1000;            

            synchronized(tablaDatos){
                tablaDatos.setValueAt(resT, id, 4);
                tablaDatos.setValueAt( real_time + " " + "Seg.", id, 5);
            }

            synchronized(jPanel6){                
                if (flagBusy)
                    jPanel6.wait();
                flagBusy = true;
                recibidasLBL.setText(String.valueOf(Integer.valueOf(recibidasLBL.getText()) + 1));
                flagBusy = false;
                jPanel6.notifyAll();
            }

            synchronized(jPanel4){
                statusLBL.setText("Recibiendo Transacción " + id);
            }            
          
        }catch(Exception e){

            e.printStackTrace();

            //Error de respuesta
            synchronized(this){
                tablaDatos.setValueAt("Servidor Inactivo", id, 4);
                tablaDatos.setValueAt("-", id, 5);
                statusLBL.setText("Recibiendo Transacción " + id);
            }            

            synchronized(jPanel6){                
                try{
                    if (flagBusy)
                        jPanel6.wait();
                }catch(Exception ep){}
                flagBusy = true;
                recibidasLBL.setText(String.valueOf(Integer.valueOf(recibidasLBL.getText()) + 1));
                flagBusy = false;
                jPanel6.notifyAll();
            }

        }
        finally{
            synchronized(this){
                cons_env++;
                if (id + 1 >= tablaDatos.getModel().getRowCount()){
                    synchronized(jPanel4){
                        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
                        statusLBL.setText("Proceso Finalizado");    
                    }                    
                }
                else if (cons_env >= filas){
                    synchronized(jPanel6){
                        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
                        statusLBL.setText("Proceso Finalizado");    
                    }                    
                } 
            }        
        }
    }


public class launcher implements Runnable{
    
    private int id;
    private int filas;
    
    public launcher(int id, int filas){
        this.id = id;
        this.filas = filas;
    }
    
    public void run(){
        try{

            int stanTx      = 0;
            String ip       = "";
            String celular  = "";
            String valor    = "";
            String operador = "";
            String merchant = "";
            String terminal = "";
            byte[] BASE64_HEADER = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
                    
            synchronized (jPanel5){
                stanTx      = seq.get("traceno");
                merchant    = merchantTXT.getText().trim();
                terminal    = terminalTXT.getText().trim();
            }
            
            synchronized (tablaDatos){
                celular     = ((String)tablaDatos.getValueAt(id, 0)).trim();
                valor       = ((String)tablaDatos.getValueAt(id, 1)).trim();
                operador    = ((String)tablaDatos.getValueAt(id, 2)).trim();

                if (operador.equals("COMC"))
                    operador = "770717532280";
                else if (operador.equals("MOVI"))
                    operador = "770717696219";
                else if (operador.equals("TIGO"))
                    operador = "770213800506";

            }

            ISOMsg m = new ISOMsg ();

            ///Armar Mensaje ISO 8583
            m.setPackager(new BASE24Packager());
            m.setHeader(BASE64_HEADER);
            m.setMTI ("0200");      //FIJO
            m.set(3, "380000");     //FIJO
            m.set(4, valor + "00");    //VALOR
            m.set (7, ISODate.getDateTime (new java.util.Date()));
            m.set(11, ISOUtil.zeropad(String.valueOf(stanTx), 6) ); //STAN
            m.set (12, ISODate.getTime (new java.util.Date()));
            m.set (13, ISODate.getDate (new java.util.Date()));
            m.set (17, ISODate.getDate (new java.util.Date()));
            m.set (22, "010");          //FIJO
            m.set (32, "10000000005");  //FIJO
            m.set (35, "940000" + ISOUtil.strpad(terminal, 8) + "=4912101               ");    //FIJO
            m.set (37, ISOUtil.zeropad(String.valueOf(stanTx), 12));    //RRN
            m.set (41, ISOUtil.strpad(terminal, 16));
            m.set (42, ISOUtil.strpad("", 15));
            m.set (43, "SUPERMERCADO SAS      1100100BOGOTACUNCO");            
            m.set (48, ISOUtil.strpad(merchant, 10) + "         00250001");
            m.set (49, "170");                  //FIJO
            m.set (60, "0005FINA+0000000");     //FIJO
            m.set (61, "0000    00000000000");  //FIJO
            ///EAN POR PRODUCTO Y CELULAR A RECARGAR
            //COMCEL=770717532280 ; TIGO=770213800506 ; MOVISTAR=770717696219
            m.set (63, "& 0000200058! P000034 " + ISOUtil.strpad(operador, 19) + ISOUtil.padleft(celular, 15, ' ') + "P ");
            m.set (100, "10000000000");         //FIJO
            m.set (128, "A0CD4B0100000000");    //FIJO - SIN MAC
            
            synchronized(tablaDatos){
                tablaDatos.setValueAt(
                        "[MTI]:" + m.getMTI() +
                        "  [3]:" + m.getString(3) +
                        "  [4]:" + valor +
                        "  [11]:" + m.getString(11) +
                        "  [37]:" + m.getString(37) +
                        "  [41]:" + m.getString(41).trim() +
                        "  [48]:" + m.getString(48) + 
                        "  [63]:" + m.getString(63)
                        , id, 3);
            }

            synchronized(jPanel6){            
                enviadasLBL.setText(String.valueOf(Integer.valueOf(enviadasLBL.getText()) + 1));
            }

            synchronized(jPanel4){
                statusLBL.setText("Enviando Transacción " + id);                
            }
            java.util.Date t1, t2;
            t1 = new java.util.Date(); 
                    
            ///Enviar Venta a través del ISOMUX Conred
            ISORequest request = new ISORequest(m);
            gatewayISOMuxConred.queue(request);                     
            ISOMsg response = request.getResponse(30000);

            String resT = "";
            
            if (response != null){
                resT =  "[MTI]:" + response.getMTI() +
                        "  [3]:" + response.getString(3) +
                        "  [11]:" + response.getString(11) +
                        "  [37]:" + response.getString(37) +                        
                        "  [38]:" + response.getString(38) +
                        "  [39]:" + response.getString(39) +
                        "  [41]:" + response.getString(41).trim() +
                        "  [48]:" + response.getString(48) +
                        "  [63]:" + response.getString(63);                
            } else {
                resT = "Timeout Alcanzado";
            }
            
            t2 = new java.util.Date(); 
            
            long totTiempo = t2.getTime() - t1.getTime();
            float real_time = (float)totTiempo / 1000;            
            
            synchronized(tablaDatos){
                tablaDatos.setValueAt(resT, this.id, 4);
                tablaDatos.setValueAt( real_time + " " + "Seg.", this.id, 5);
            }

            synchronized(jPanel6){                
                if (flagBusy)
                    jPanel6.wait();
                flagBusy = true;
                recibidasLBL.setText(String.valueOf(Integer.valueOf(recibidasLBL.getText()) + 1));
                flagBusy = false;
                jPanel6.notifyAll();
            }
            
            synchronized(jPanel4){
                statusLBL.setText("Recibiendo Transacción " + this.id);
            }            
     
        }catch(Exception e){
            
            e.printStackTrace();
            
            //Error de respuesta
            synchronized(this){
                tablaDatos.setValueAt("Servidor Inactivo", this.id, 4);
                tablaDatos.setValueAt("-", this.id, 5);
                statusLBL.setText("Recibiendo Transacción " + this.id);
            }            
            
            synchronized(jPanel6){                
                try{
                    if (flagBusy)
                        jPanel6.wait();
                }catch(Exception ep){}
                flagBusy = true;
                recibidasLBL.setText(String.valueOf(Integer.valueOf(recibidasLBL.getText()) + 1));
                flagBusy = false;
                jPanel6.notifyAll();
            }
            
        }
        finally{
            synchronized(this){
                cons_env++;
                if (this.id + 1 >= tablaDatos.getModel().getRowCount()){
                    synchronized(jPanel4){
                        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
                        statusLBL.setText("Proceso Finalizado");    
                    }                    
                }
                else if (cons_env >= this.filas){
                    synchronized(jPanel6){
                        statusLBL.setIcon(new ImageIcon(getClass().getResource("procesandoStatic.gif")));
                        statusLBL.setText("Proceso Finalizado");    
                    }                    
                } 
            }        
        }
    }
}    

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel enviadasLBL;
    private javax.swing.JTextField ip1TXT;
    private javax.swing.JTextField ip2TXT;
    private javax.swing.JTextField ip3TXT;
    private javax.swing.JTextField ip4TXT;
    private javax.swing.JLabel isomuxLBL;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    private javax.swing.JMenuItem jMenuItem7;
    private javax.swing.JMenuItem jMenuItem8;
    private javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JTextField merchantTXT;
    private javax.swing.JTextField portTXT;
    private javax.swing.JLabel recibidasLBL;
    private javax.swing.JTextField serialTXT;
    private javax.swing.JLabel statusLBL;
    private javax.swing.JTable tablaDatos;
    private javax.swing.JTextField terminalTXT;
    // End of variables declaration//GEN-END:variables
    
}
