import java.io.*;

public class RegQuery {

  private static final String REGQUERY_UTIL = "REG QUERY ";  
  private static final String REGADD_UTIL = "REG ADD ";
  private static final String REGDWORD_TOKEN = "REG_DWORD";

  private static final String SOFTWARE_KEY_QUERY = REGQUERY_UTIL +
    "\"HKLM\\Software\\regEdit\" /v remoteApp";

  private static final String SOFTWARE_KEY_ADD = REGADD_UTIL +
    "\"HKLM\\Software\\regEdit\" /v remoteApp /t REG_DWORD /d ";  
  
  public static int getSoftwareKey() {
    try {
      Process process = Runtime.getRuntime().exec(SOFTWARE_KEY_QUERY);
      StreamReader reader = new StreamReader(process.getInputStream());

      reader.start();
      process.waitFor();
      reader.join();

      String result = reader.getResult();
      int p = result.indexOf(REGDWORD_TOKEN);

      if (p == -1)
         return 0;

      // Number of executions
      String temp = result.substring(p + REGDWORD_TOKEN.length()).trim();
      return (Integer.parseInt(temp.substring("0x".length()), 16) );
    }
    catch (Exception e) {
      return 0;
    }
  }

  public static int setSoftwareKey(int number) {
    try {
      Process process = Runtime.getRuntime().exec(SOFTWARE_KEY_ADD + number + " /f");
      StreamReader reader = new StreamReader(process.getInputStream());

      reader.start();
      process.waitFor();
      reader.join();

      String result = reader.getResult();
      int p = result.indexOf("correctamente");  ///Español

      if (p == -1){
         p = result.indexOf("successfully");    ///Ingles
         if (p == -1)
            return 0;       ///Error
      }
      // Set number 1 of execution
      return 1; ///Correct
    }
    catch (Exception e) {
      return 0;
    }
  }
  
  static class StreamReader extends Thread {
    private InputStream is;
    private StringWriter sw;

    StreamReader(InputStream is) {
      this.is = is;
      sw = new StringWriter();
    }

    public void run() {
      try {
        int c;
        while ((c = is.read()) != -1)
          sw.write(c);
        }
        catch (IOException e) { ; }
      }

    String getResult() {
      return sw.toString();
    }
  }
  
}
