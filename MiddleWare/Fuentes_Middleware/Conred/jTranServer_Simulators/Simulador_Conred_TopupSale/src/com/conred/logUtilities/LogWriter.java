package com.conred.logUtilities;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.jpos.util.LogSource;
import org.jpos.util.Logger;

/**
 *
 * @author Elkin David Beltr�n Duque
 */
public class LogWriter implements LogSource{

    protected Logger logger = null;
    protected String realm = null;
    
    public void setLogger(Logger logger, String realm){
        this.logger = logger;
        this.realm  = realm;
    }
    
    public String getRealm(){
        return realm;
    }
    
    public Logger getLogger(){
        return logger;
    }
    
}
