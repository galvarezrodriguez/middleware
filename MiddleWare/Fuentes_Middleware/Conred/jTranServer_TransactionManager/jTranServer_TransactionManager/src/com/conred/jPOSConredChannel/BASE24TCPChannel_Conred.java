/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.conred.jPOSConredChannel;

import java.io.IOException;
import java.net.ServerSocket;
import org.jpos.iso.BaseChannel;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.ISOUtil;
import org.jpos.util.LogEvent;
import org.jpos.util.Logger;


public class BASE24TCPChannel_Conred extends BaseChannel {
    /**
     * Public constructor (used by Class.forName("...").newInstance())
     */
    public BASE24TCPChannel_Conred () {
        super();
    }
    /**
     * Construct client ISOChannel
     * @param host  server TCP Address
     * @param port  server port number
     * @param p     an ISOPackager
     * @see ISOPackager
     */
    public BASE24TCPChannel_Conred (String host, int port, ISOPackager p) {
        super(host, port, p);
    }
    /**
     * Construct server ISOChannel
     * @param p     an ISOPackager
     * @see ISOPackager
     * @exception IOException
     */
    public BASE24TCPChannel_Conred (ISOPackager p) throws IOException {
        super(p);
    }
    /**
     * constructs a server ISOChannel associated with a Server Socket
     * @param p     an ISOPackager
     * @param serverSocket where to accept a connection
     * @exception IOException
     * @see ISOPackager
     */
    public BASE24TCPChannel_Conred (ISOPackager p, ServerSocket serverSocket) 
        throws IOException
    {
        super(p, serverSocket);
    }
    /**
     * @param m the Message to send (in this case it is unused)
     * @param len   message len (ignored)
     * @exception IOException
     */
    protected void sendMessageTrailler(ISOMsg m, int len) throws IOException {
        //serverOut.write (3);
    }
    protected void sendMessageLength(int len) throws IOException {
        //len++;  // one byte trailler
        len +=2;  //Para JNTS
        
        serverOut.write (len >> 8);
        serverOut.write (len);
    }
    protected int getMessageLength() throws IOException, ISOException {
        int l = 0;
        byte[] b = new byte[2];
        Logger.log (new LogEvent (this, "get-message-length"));
        while (l == 0) {
            serverIn.readFully(b,0,2);  ///Método Bloqueante, Elkin
            l = ((((int)b[0])&0xFF) << 8) | (((int)b[1])&0xFF);
            if (l == 0) {
                serverOut.write(b);
                serverOut.flush();
            }
        }
        Logger.log (new LogEvent (this, "got-message-length", Integer.toString(l-2)));
        //return l - 1;   // trailler length
        return l - 2;   // trailler length - Sin Trailer
    }
    protected void getMessageTrailler() throws IOException {
        /*
        Logger.log (new LogEvent (this, "get-message-trailler"));
        byte[] b = new byte[1];
        serverIn.readFully(b,0,1);
        Logger.log (new LogEvent (this, "got-message-trailler", ISOUtil.hexString(b)));
        */
    }
    
}



