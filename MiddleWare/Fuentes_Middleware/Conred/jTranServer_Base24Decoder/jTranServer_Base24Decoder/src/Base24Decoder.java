/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.conred.jPOSConredChannel.ISOMsg_Conred;
import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;
import org.jpos.iso.packager.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

/**
 *
 * @author Elkin Beltrán
 */
public class Base24Decoder {
    public static void main(String args[]) {
        /*
         //Pruebas
         args = null;
         args = new String[2];
         //args[0] = "30 38 30 30 38 32 32 30 30 30 30 30 30 30 30 30 30 30 30 30 30 34 30 30 30 30 30 30 30 30 30 30 30 30 30 30 30 39 30 38 31 30 33 31 32 34 33 31 32 32 35 36 33 30 31";
         args[0] = "ISO0260000130800822000000000000004000000000000000821083106006843301";
         args[1] = "12";
        */ 
        
        if (args.length != 2){
            System.out.println("Error General Decoder Base24.");
            return;
        }
        
        try{            
            ISOMsg_Conred decoderMsg   = new ISOMsg_Conred();
            PrintStream ps      = null;
            BufferedReader br   = null;
            String contentTxt   = "";
            String lineTxt      = "";
            int lenHeader       = 0;
            String headerB24    = null;
            byte arrMsg[]       = null;
            
            lenHeader = Integer.parseInt(args[1]);  //Longitud Header Base24
            //byte arrMsg[] = ISOUtil.hex2byte(args[0].replace(" ", ""));   //Separado con Espacios y Caracter en modo Hex            
            arrMsg = args[0].substring(lenHeader).getBytes();           //Trama Base24 Normal con caracteres ASCII
            headerB24 = args[0].substring(0, lenHeader);                //Header Base24 con caracteres ASCII
            
            ///Crear Archivo de Salida
            try{
                ps = new PrintStream(new FileOutputStream("salida.txt"));
                decoderMsg.setPackager(new BASE24Packager());
                decoderMsg.unpack(arrMsg);
                decoderMsg.setHeader(headerB24);
                decoderMsg.dump(ps, "");                
            } catch(Exception ex){
                System.out.println("Error Trama Base24 de Entrada, Verifique datos.");
            } finally{
                try{
                    ps.close();
                }catch(Exception e){ }
            }
            
            //Leer Archivo Creado Anteriormente
            try {
                br = new BufferedReader (new FileReader (new File("salida.txt")));
                lineTxt = br.readLine();
                while (lineTxt != null) {
                    contentTxt += lineTxt + "\r\n";
                    lineTxt = br.readLine();
                }                
            } catch (Exception e) {
                System.out.println("Error General Decoder ISO.");
            } finally{
                try{
                    br.close();
                }catch(Exception e){ }
            }           
            
            //Imprimir Salida
            System.out.println(contentTxt.replace("<", "#"));
            
        } catch(Exception e){
            System.out.println("Error General Decoder ISO.");
        }
    }
}
