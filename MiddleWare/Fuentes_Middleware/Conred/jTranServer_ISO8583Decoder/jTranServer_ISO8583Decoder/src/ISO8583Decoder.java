/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.jpos.iso.*;
import org.jpos.util.*;
import org.jpos.iso.channel.*;
import org.jpos.iso.packager.*;
import com.conred.jPOSConredChannel.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

/**
 *
 * @author Elkin Beltrán
 */
public class ISO8583Decoder {
    public static void main(String args[]) {

        ///Prueba
        /*
            args = null;
            args = new String[1];
            args[0] = "02 00 32 38 05 00 20 C0 00 02 38 00 00 00 00 00 30 00 00 10 07 11 21 20 00 64 54 11 21 20 10 07 00 21 00 01 37 94 00 00 64 90 00 10 D4 91 21 01 60 60 60 60 60 60 60 00 36 34 39 30 30 30 31 30 30 35 34 34 30 30 30 32 33 33 20 20 20 20 20 00 32 37 37 30 37 31 37 36 39 36 32 31 39 20 20 20 20 20 20 20 20 20 20 33 31 35 30 33 36 38 34 36 39";
        */
        
        if (args.length != 1){
            System.out.println("Error General Decoder ISO.");
            return;
        }
        
        try{            
            ISOMsg_Conred decoderMsg   = new ISOMsg_Conred();
            PrintStream ps      = null;
            BufferedReader br   = null;
            String contentTxt   = "";
            String lineTxt      = "";

            byte arrMsg[] = ISOUtil.hex2byte(args[0].replace(" ", ""));
            
            ///Crear Archivo de Salida
            try{
                ps = new PrintStream(new FileOutputStream("salida.txt"));
                decoderMsg.setPackager(new ISO87B_ConredPackager());
                decoderMsg.unpack(arrMsg);
                decoderMsg.dump(ps, "");                
            } catch(org.jpos.iso.ISOException ex){
                System.out.println("Error Trama ISO 8583 de Entrada, Verifique datos.");
            } finally{
                try{
                    ps.close();
                }catch(Exception e){ }
            }
            
            //Leer Archivo Creado Anteriormente
            try {
                br = new BufferedReader (new FileReader (new File("salida.txt")));
                lineTxt = br.readLine();
                while (lineTxt != null) {
                    contentTxt += lineTxt + "\r\n";
                    lineTxt = br.readLine();
                }                
            } catch (Exception e) {
                System.out.println("Error General Decoder ISO.");
            } finally{
                try{
                    br.close();
                }catch(Exception e){ }
            }           
            
            //Imprimir Salida
            System.out.println(contentTxt.replace("<", "#"));
            
        } catch(Exception e){
            System.out.println("Error General Decoder ISO.");
        }
    }
}
