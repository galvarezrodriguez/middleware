/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import org.jpos.iso.*;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.packager.*;

/**
 *
 * @author Elkin Beltrán
 */
public class TransactionTester {

    private enum FIELDS{
        IP,
        PORT,
        MSG_TYPE,
        PROC_CODE,
        STAN,
        TIME,
        DATE,
        TERMINALID,
        END_FIELD1,
        RSP_CODE,
        RESPONSE_MSG,
        PROC_TIME,
        END_FIELD2
    }
    
    public static void main(String[] args){

        byte[] TPDU                 = {0x60, 0x00, 0x01, 0x00, 0x00};
        ISOChannel channel          = null;
        ISOMsg sendMsg              = null;
        ISOMsg recvMsg              = null;
        int timeout                 = 30000;
        long startProcessingTime    = 0;
        long endProcessingTime      = 0;
        int totalProcessingTime     = 0;
        String[] responseTokens     = new String[FIELDS.END_FIELD2.ordinal()];
        String errMsg               = "";
        String SEPARATOR            = "|";
        String responseCad          = "";
        
        //Prueba
            /*
            args        = null;
            args        = new String[FIELDS.END_FIELD1.ordinal()];
            args[FIELDS.IP.ordinal()]           = "127.0.0.1";
            args[FIELDS.PORT.ordinal()]         = "25030";
            args[FIELDS.MSG_TYPE.ordinal()]     = "0800";
            args[FIELDS.PROC_CODE.ordinal()]     = "990000";
            args[FIELDS.STAN.ordinal()]         = "456790";
            args[FIELDS.TIME.ordinal()]         = "092554";
            args[FIELDS.DATE.ordinal()]         = "1009";
            args[FIELDS.TERMINALID.ordinal()]   = "25000001";
            */
        //Fin Prueba
        
        //Check Params
        if (args.length != FIELDS.END_FIELD1.ordinal()){
            System.out.println("Error Parámetros");
            return;   ///Error Params
        }        
        
        ///Start Time Counter
        startProcessingTime = System.currentTimeMillis();        
        
        try{
            channel = new NACChannel(args[FIELDS.IP.ordinal()], Integer.parseInt(args[FIELDS.PORT.ordinal()]), new ISO87BPackager(), TPDU);
            ((BaseChannel)channel).setTimeout(timeout);   ////30 segundos de espera
            channel.connect (); 

            ///Conred Echo Test
            sendMsg  = new ISOMsg ();
            sendMsg.setMTI( args[FIELDS.MSG_TYPE.ordinal()]);       //Se Recibe Como Parám, pero se setea Fijo (PHP)
            sendMsg.set(3 , args[FIELDS.PROC_CODE.ordinal()]);      //Se Recibe Como Parám, pero se setea Fijo (PHP)
            sendMsg.set(11, args[FIELDS.STAN.ordinal()]);           //Se Recibe Como Parám.
            sendMsg.set(12, args[FIELDS.TIME.ordinal()]);           //hhMMss, Se Recibe Como Parám
            sendMsg.set(13, args[FIELDS.DATE.ordinal()]);           //ddmm, Se Recibe Como Parám            
            sendMsg.set(41, args[FIELDS.TERMINALID.ordinal()]);      //Se Recibe Como Parám            

            channel.send(sendMsg);
            recvMsg = channel.receive ();

        } catch(org.jpos.iso.ISOException e1){
            errMsg = "Servidor No Activo";
        } catch(java.net.SocketTimeoutException e2){
            errMsg = "Tiempo de Espera Agotado";
        } catch(java.net.SocketException e3){            
            errMsg = "Conexión Reseteada por Servidor";
        } catch(Exception en){
            errMsg = "Error general";
        } finally {
            try{
                channel.disconnect ();
                channel = null;
            } catch(Exception e){ }
        }
        
        ///Get Proccesing Time
        endProcessingTime = System.currentTimeMillis();                    
      
        totalProcessingTime = (int)(endProcessingTime-startProcessingTime);
        
        ///Verificar Mensaje Recibido para emitir respuesta        
        if (recvMsg != null){
            try{
                responseTokens[FIELDS.MSG_TYPE.ordinal()]       = recvMsg.getMTI();
                responseTokens[FIELDS.PROC_CODE.ordinal()]      = recvMsg.getString(3);
                responseTokens[FIELDS.STAN.ordinal()]           = recvMsg.getString(11);
                responseTokens[FIELDS.TIME.ordinal()]           = recvMsg.getString(12);
                responseTokens[FIELDS.DATE.ordinal()]           = recvMsg.getString(13);
                responseTokens[FIELDS.TERMINALID.ordinal()]     = recvMsg.getString(41);
                //Código y Mensaje de Respuesta
                responseTokens[FIELDS.RSP_CODE.ordinal()]       = recvMsg.getString(39);
                responseTokens[FIELDS.RESPONSE_MSG.ordinal()]   = "Echo Test Correcto";
            }catch(Exception ex){ }
            
        } else {
                responseTokens[FIELDS.MSG_TYPE.ordinal()]       = args[FIELDS.MSG_TYPE.ordinal()];
                responseTokens[FIELDS.PROC_CODE.ordinal()]      = args[FIELDS.PROC_CODE.ordinal()];
                responseTokens[FIELDS.STAN.ordinal()]           = args[FIELDS.STAN.ordinal()];
                responseTokens[FIELDS.TIME.ordinal()]           = args[FIELDS.TIME.ordinal()];
                responseTokens[FIELDS.DATE.ordinal()]           = args[FIELDS.DATE.ordinal()];      
                responseTokens[FIELDS.TERMINALID.ordinal()]     = args[FIELDS.TERMINALID.ordinal()];
                //Código y Mensaje de Respuesta
                responseTokens[FIELDS.RSP_CODE.ordinal()]       = "99";
                responseTokens[FIELDS.RESPONSE_MSG.ordinal()]   = errMsg;
        }
        
        //Tiempo de Procesamiento
        responseTokens[FIELDS.PROC_TIME.ordinal()]              = String.valueOf(totalProcessingTime);        
        
        //Imprimir Respuesta
        responseCad = responseTokens[FIELDS.MSG_TYPE.ordinal()]         + SEPARATOR +
                        responseTokens[FIELDS.PROC_CODE.ordinal()]      + SEPARATOR +
                        responseTokens[FIELDS.STAN.ordinal()]           + SEPARATOR +
                        responseTokens[FIELDS.TIME.ordinal()]           + SEPARATOR +
                        responseTokens[FIELDS.DATE.ordinal()]           + SEPARATOR +
                        responseTokens[FIELDS.TERMINALID.ordinal()]     + SEPARATOR +
                        responseTokens[FIELDS.RSP_CODE.ordinal()]       + SEPARATOR +
                        responseTokens[FIELDS.RESPONSE_MSG.ordinal()]   + SEPARATOR +
                        responseTokens[FIELDS.PROC_TIME.ordinal()];

        System.out.print(responseCad);
        
    }
    
}
