/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;

/**
 *
 * @author Elkin Beltrán
 */
public class DBTester {
    
    /*
     * Este programa debe ejecutarse enviando los siguientes parámetros:
     * [0] -> JDBC Driver
     * [1] -> URL de Conexión
     * [2] -> Usuario
     * [3] -> Password
     * 
     * La salida del programa será:
     * OK|1
     * ERROR|Descripción del Error
     * 
     */    
    public static void main(String[] args){

        if (args.length != 4){
            System.out.println("ERROR|" + "Este programa debe ejecutarse: DBTester [Driver, URL, User, Password]");
            return;
        }

        /*
        String args1[] = new String[4];
        
        args1[0]="com.microsoft.sqlserver.jdbc.SQLServerDriver";
        args1[1]="jdbc:sqlserver://ASNCO-ELKBELBU\\SQLEXPRESS;database=Conred;integratedSecurity=false";
        args1[2]="sa";
        args1[3]="admin2012*";
        */
        
        doDBConn(args);

    }
    
    private static void doDBConn(String[] args){
        Connection conn         = null;
        String retVal           = "";
        
        try {            
            Class.forName( args[0] );
            
            conn = DriverManager.getConnection(args[1], args[2], args[3]);
            
            retVal = "OK|0";
            
        } catch (Exception e) {
            retVal = "ERROR|" + e.getMessage();
        } finally{
            try{
                conn.close();
            } catch(Exception e){}
        }
        
        System.out.println(retVal);
        
    }
      
}
