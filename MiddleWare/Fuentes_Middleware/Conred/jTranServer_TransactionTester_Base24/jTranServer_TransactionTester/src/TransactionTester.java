/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.conred.jPOSConredChannel.BASE24TCPChannel_Conred;
import org.jpos.iso.*;
import org.jpos.iso.packager.BASE24Packager;

/**
 *
 * @author Elkin Beltrán
 */
public class TransactionTester {

    private enum FIELDS{
        IP,
        PORT,
        MSG_TYPE,
        DATETIME,
        STAN,
        FIELD70,
        END_FIELD1,
        RSP_CODE,
        RESPONSE_MSG,
        PROC_TIME,
        END_FIELD2
    }
    
    public static void main(String[] args){

        byte[] BASE64_HEADER        = {'I', 'S', 'O', 0x30, 0x32, 0x36, 0x30, 0x30, 0x30, 0x30, 0x31, 0x33};
        ISOChannel channel          = null;
        ISOMsg sendMsg              = null;
        ISOMsg recvMsg              = null;
        int timeout                 = 30000;
        long startProcessingTime    = 0;
        long endProcessingTime      = 0;
        int totalProcessingTime     = 0;
        String[] responseTokens     = new String[FIELDS.END_FIELD2.ordinal()];
        String errMsg               = "";
        String SEPARATOR            = "|";
        String responseCad          = "";
        
        //Prueba
            /*
            args        = null;
            args        = new String[FIELDS.END_FIELD1.ordinal()];
            args[FIELDS.IP.ordinal()]           = "127.0.0.1";
            args[FIELDS.PORT.ordinal()]         = "6660";
            args[FIELDS.MSG_TYPE.ordinal()]     = "0800";
            args[FIELDS.DATETIME.ordinal()]     = "0703092500";
            args[FIELDS.STAN.ordinal()]         = "456790";
            args[FIELDS.FIELD70.ordinal()]      = "301";
            */
        //Fin Prueba
        
        //Check Params
        if (args.length != FIELDS.END_FIELD1.ordinal()){
            System.out.println("Error Parámetros");
            return;   ///Error Params
        }        
        
        ///Start Time Counter
        startProcessingTime = System.currentTimeMillis();        
        
        try{
            channel = new BASE24TCPChannel_Conred(args[FIELDS.IP.ordinal()], Integer.parseInt(args[FIELDS.PORT.ordinal()]), new BASE24Packager());        
            ((BaseChannel)channel).setTimeout(timeout);   ////30 segundos de espera
            ((BaseChannel)channel).setHeader(BASE64_HEADER);            
            channel.connect ();   

            ///Conred Echo Test
            sendMsg  = new ISOMsg ();
            sendMsg.setMTI( args[FIELDS.MSG_TYPE.ordinal()]);       //Se Recibe Como Parám, pero se setea Fijo (PHP)
            sendMsg.set(7,  args[FIELDS.DATETIME.ordinal()]);       //ddmmyyyyhhMMss, Se Recibe Como Parám
            sendMsg.set(11, args[FIELDS.STAN.ordinal()]);           //Se Recibe Como Parám.
            sendMsg.set(70, args[FIELDS.FIELD70.ordinal()]);        //FIELD70, Se Recibe Como Parám.

            channel.send(sendMsg);
            recvMsg = channel.receive ();

        } catch(org.jpos.iso.ISOException e1){
            errMsg = "Servidor No Activo";
        } catch(java.net.SocketTimeoutException e2){
            errMsg = "Tiempo de Espera Agotado";
        } catch(java.net.SocketException e3){            
            errMsg = "Conexión Reseteada por Servidor";
        } catch(Exception en){
            errMsg = "Error general";
        } finally {
            try{
                channel.disconnect ();
                channel = null;
            } catch(Exception e){ }
        }
        
        ///Get Proccesing Time
        endProcessingTime = System.currentTimeMillis();                    
      
        totalProcessingTime = (int)(endProcessingTime-startProcessingTime);
        
        ///Verificar Mensaje Recibido para emitir respuesta        
        if (recvMsg != null){
            try{
                responseTokens[FIELDS.MSG_TYPE.ordinal()]       = recvMsg.getMTI();
                responseTokens[FIELDS.DATETIME.ordinal()]       = recvMsg.getString(7);
                responseTokens[FIELDS.STAN.ordinal()]           = recvMsg.getString(11);
                responseTokens[FIELDS.FIELD70.ordinal()]        = recvMsg.getString(70);
                //Código y Mensaje de Respuesta
                responseTokens[FIELDS.RSP_CODE.ordinal()]       = recvMsg.getString(39);
                responseTokens[FIELDS.RESPONSE_MSG.ordinal()]   = "Echo Test Correcto";
            }catch(Exception ex){ }
            
        } else {
                responseTokens[FIELDS.MSG_TYPE.ordinal()]       = args[FIELDS.MSG_TYPE.ordinal()];
                responseTokens[FIELDS.DATETIME.ordinal()]       = args[FIELDS.DATETIME.ordinal()];
                responseTokens[FIELDS.STAN.ordinal()]           = args[FIELDS.STAN.ordinal()];
                responseTokens[FIELDS.FIELD70.ordinal()]        = args[FIELDS.FIELD70.ordinal()];
                //Código y Mensaje de Respuesta
                responseTokens[FIELDS.RSP_CODE.ordinal()]       = "99";
                responseTokens[FIELDS.RESPONSE_MSG.ordinal()]   = errMsg;
        }
        
        //Tiempo de Procesamiento
        responseTokens[FIELDS.PROC_TIME.ordinal()]              = String.valueOf(totalProcessingTime);        
        
        //Imprimir Respuesta
        responseCad = responseTokens[FIELDS.MSG_TYPE.ordinal()]         + SEPARATOR +
                        responseTokens[FIELDS.DATETIME.ordinal()]       + SEPARATOR +
                        responseTokens[FIELDS.STAN.ordinal()]           + SEPARATOR +
                        responseTokens[FIELDS.FIELD70.ordinal()]        + SEPARATOR +
                        responseTokens[FIELDS.RSP_CODE.ordinal()]       + SEPARATOR +
                        responseTokens[FIELDS.RESPONSE_MSG.ordinal()]   + SEPARATOR +
                        responseTokens[FIELDS.PROC_TIME.ordinal()];

        System.out.print(responseCad);
        
    }
    
}
