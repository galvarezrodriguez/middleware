﻿
function validateActivateMerchant() {

    var flagField = false;

    if ($("#ctl00_MainContent_ddlMerchants").val() <= 0) {
        $("#ctl00_MainContent_ddlMerchants").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_ddlEntityType").val() <= 0) {
        $("#ctl00_MainContent_ddlEntityType").focus();
        flagField = false;
    }
    else if (typeof $("#ctl00_MainContent_txtPctGain").val() != "undefined" && !validateDecimal($("#ctl00_MainContent_txtPctGain").val())) {
        $("#ctl00_MainContent_txtPctGain").focus();
        flagField = false;
    }    
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateEditMerchant() {

    var flagField = false;

    if ($("#ctl00_MainContent_ddlEntityType").val() <= 0) {
        $("#ctl00_MainContent_ddlEntityType").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_ddlMerchantStatus").val() <= 0) {
        $("#ctl00_MainContent_ddlMerchantStatus").focus();
        flagField = false;
    }
    else if (typeof $("#ctl00_MainContent_txtPctGain").val() != "undefined" && !validateDecimal($("#ctl00_MainContent_txtPctGain").val())) {
        $("#ctl00_MainContent_txtPctGain").focus();
        flagField = false;
    }
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateApplyMovement() {

    var flagField = false;

    if ($("#ctl00_MainContent_ddlMovementType").val() <= 0) {
        $("#ctl00_MainContent_ddlMovementType").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_ddlBankAccount").val() <= 0) {
        $("#ctl00_MainContent_ddlBankAccount").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_txtMovementAmount").val().length <= 0
            || $("#ctl00_MainContent_txtMovementAmount").val().substring(0,1) == '0'
            || !validateOnlyDigits($("#ctl00_MainContent_txtMovementAmount").val())) {
        $("#ctl00_MainContent_txtMovementAmount").focus();
        flagField = false;
    }
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}
