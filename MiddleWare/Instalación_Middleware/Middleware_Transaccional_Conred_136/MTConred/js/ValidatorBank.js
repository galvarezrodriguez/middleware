﻿
function validateAddAccount() {

    var flagField = false;
    var errorCode = 0;
    var strError = '';

    if ($("#ctl00_MainContent_txtAccountNumber").val().length <= 5 || !validateOnlyDigits($("#ctl00_MainContent_txtAccountNumber").val())) {
        $("#ctl00_MainContent_txtAccountNumber").focus();
        errorCode = 1;
        flagField = false;
    }
    else if ($("#ctl00_MainContent_ddlAccountsType").val() <= 0) {
        $("#ctl00_MainContent_ddlAccountsType").focus();
        errorCode = 2;
        flagField = false;
    }
    else
        flagField = true;

    if (!flagField) {

        switch (errorCode) {
            case 1: strError = 'Por favor digite un número de cuenta correcto'; break;
            case 2: strError = 'Por favor seleccione un Tipo de Cuenta'; break;
        }

        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: strError,
            callback: function() {
                $.msgAlert.close();

                if ($("#ctl00_MainContent_txtAccountNumber").val().length <= 0)
                    $("#ctl00_MainContent_txtAccountNumber").focus();

            }
        });
    }

    return flagField;

}