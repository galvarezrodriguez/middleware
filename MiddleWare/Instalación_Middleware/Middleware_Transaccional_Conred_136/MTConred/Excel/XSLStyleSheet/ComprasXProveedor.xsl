﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:decimal-format name="German" decimal-separator=',' grouping-separator='.'  NaN=''/>
  <xsl:template match="/">
    <HTML>
      <HEAD>
        <STYLE>
          .stdPVTblLCell {
          color: DarkBlue;
          font-weight: bold;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 12pt;
          }
          .stdPageHdr {
          color: DarkBlue;
          font-weight: bold;
          font-style:italic;
          font-family:Verdana;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 70%;
          font-size: 20pt;
          }
          .gridHeader {
          background-color: #C0C0C0;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          vertical-align:middle;
          text-align:center;
          border: solid thin Black;
          }
          .SearchHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchKey {
          color: DarkBlue;
          font-size: 9pt;
          vertical-align:middle;
          text-align:right;
          font-family:Verdana;
          }
          .SearchValue {
          color: Black;
          font-size: 9pt;
          font-weight: bold;
          vertical-align:middle;
          text-align:left;
          font-family:Verdana;
          }
          .SearchResultHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchResultItem {
          background-color: #8FC9FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .SearchResultAltItem {
          background-color: #CCE6FF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE>
          <TR>
            <TD class="stdPageHdr" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/ReportName"/>
            </TD>
          </TR>
          <TR  class="stdPVTblLCell">
            <TD>
              Encabezado
            </TD>
          </TR>
          <TR>
            <TD> </TD>
          </TR>
          <TR>
            <TD colspan="1" class="SearchKey">Proveedor:</TD>
            <TD class="SearchValue" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/Supplier"/>
            </TD>
          </TR>
          <TR>
            <TD colspan="1" class="SearchKey">Período:</TD>
            <TD class="SearchValue" colspan="6">
              <xsl:value-of select="NewDataSet/HeaderDetails/Period"/>
            </TD>
          </TR>
          <TR>
            <TD></TD>
          </TR>
          <TR class="SearchResultHeader">
            <TD colspan="2">Detalle del Reporte</TD>
          </TR>
          <TR>
            <TD> </TD>
          </TR>
          <TR>
            <TD class="gridHeader">
              Fecha
            </TD>
            <TD class="gridHeader">
              Proveedor
            </TD>
            <TD class="gridHeader">
              Banco
            </TD>
            <TD class="gridHeader">
              Número de Cuenta
            </TD>
            <TD class="gridHeader">
              Proveedor Saldo Inicial
            </TD>
            <TD class="gridHeader">
              Valor Compra
            </TD>
            <TD class="gridHeader">
              Proveedor Saldo Final
            </TD>
            <TD class="gridHeader">
              Observación
            </TD>
            <TD class="gridHeader">
              Usuario Web
            </TD>
          </TR>
          <xsl:for-each select="NewDataSet/Table">
            <xsl:choose>
              <xsl:when test="position() mod 2 = 1">
                <TR>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="fecha"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="prov_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="ban_nombre"/>
                  </TD>
                  <TD class="SearchResultItem">
                    '<xsl:value-of select="cue_numero"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="format-number(comp_saldo_inicial,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="format-number(comp_valor,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="format-number(comp_saldo_final,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="comp_observacion"/>
                  </TD>
                  <TD class="SearchResultItem">
                    <xsl:value-of select="usu_nombre"/>
                  </TD>
                </TR>
              </xsl:when>
              <xsl:otherwise>
                <TR>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="fecha"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="prov_nombre"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="ban_nombre"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    '<xsl:value-of select="cue_numero"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="format-number(comp_saldo_inicial,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="format-number(comp_valor,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="format-number(comp_saldo_final,'#.###,00', 'German')"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="comp_observacion"/>
                  </TD>
                  <TD class="SearchResultAltItem">
                    <xsl:value-of select="usu_nombre"/>
                  </TD>
                </TR>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>