USE [Conred]
GO
/****** Object:  UserDefinedFunction [dbo].[ObtenerMensajeISOMapeado]    Script Date: 12/03/2012 09:38:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ObtenerMensajeISOMapeado]
(
	@CodigoRespuesta AS VARCHAR(10),
	@CodigoRespuestaISO AS VARCHAR(10)
)
RETURNS VARCHAR(2)
AS
BEGIN

	DECLARE @ret_val VARCHAR(2)

	--BUSCAR EL RSP. CODE MAPEADO SI ES UNA RESPUESTA DE MOVILRED U OTRO FORMATEADOR
	
	--MOVILRED
	IF SUBSTRING(@CodigoRespuesta, 1, 3) = 'MOV'
	BEGIN
		SELECT TOP 1 @ret_val = COD_RESPUESTA_ISO FROM MENSAJEESTADO WHERE COD_ESTADO = @CodigoRespuesta
		
		IF @ret_val IS NULL 
			SET @ret_val = 'C0' ---GENERAL ERROR
		
	END
	ELSE
	BEGIN
		--MENSAJES SERVIDOR LOCAL
		SET @ret_val = @CodigoRespuestaISO
	END

	RETURN @ret_val

END
GO
/****** Object:  UserDefinedFunction [dbo].[ObtenerMensaje]    Script Date: 12/03/2012 09:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ObtenerMensaje]
(
	@codRespuesta AS VARCHAR(10)
)
RETURNS VARCHAR(100)
AS
BEGIN

	DECLARE @ret_val VARCHAR(100)

	SELECT TOP 1 @ret_val = MENSAJE FROM MENSAJEESTADO WHERE COD_ESTADO = @codRespuesta

	RETURN (CASE WHEN @ret_val IS NULL THEN 'Mensaje No Encontrado' ELSE @ret_val END)

END
GO
/****** Object:  UserDefinedFunction [dbo].[SepararCamposCodigoBarras]    Script Date: 12/03/2012 09:38:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[SepararCamposCodigoBarras]
(
	@barCode VARCHAR(100),
	@prodCode INT
)
RETURNS @subcampos TABLE 
(
	referencia1 VARCHAR(30),
	referencia2 VARCHAR(30)
)
AS
BEGIN

	DECLARE
		@referencia1 VARCHAR(30),
		@referencia2 VARCHAR(30),
		@EAN_Code VARCHAR(30),
		@ref_doble BIT,
		@pos_ini_ref1 SMALLINT,
		@longitud_ref1 SMALLINT,
		@pos_ini_ref2 SMALLINT,
		@longitud_ref2 SMALLINT
		
	SELECT @EAN_Code = SUBSTRING(@barCode,4,13)
	
	SELECT @ref_doble=conv_ref_doble, @pos_ini_ref1=conv_pos_ini_ref1, @longitud_ref1=conv_longitud_ref1, @pos_ini_ref2=conv_pos_ini_ref2, @longitud_ref2=conv_longitud_ref2 FROM ConvenioXProducto WHERE conv_codigo_EAN=@EAN_Code AND conv_producto_id=@prodCode

	IF @ref_doble = 0
	BEGIN
		--SOLO REFERENCIA 1
		SET @referencia1 = SUBSTRING(@barCode,@pos_ini_ref1,@longitud_ref1)
		SET @referencia2 = ''
	END
	ELSE
	BEGIN
		--REFERENCIA 1 Y 2
		SET @referencia1 = SUBSTRING(@barCode,@pos_ini_ref1,@longitud_ref1)	
		SET @referencia2 = SUBSTRING(@barCode,@pos_ini_ref2,@longitud_ref2)
	END

	INSERT INTO @subcampos
		(referencia1, referencia2)
	VALUES (@referencia1, @referencia2)
		
	RETURN 
END
GO
/****** Object:  UserDefinedFunction [dbo].[ObtenerSaldo]    Script Date: 12/03/2012 09:38:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ObtenerSaldo]
(
	@comercioID AS VARCHAR(20),
	@transaccional AS BIT
)
RETURNS MONEY
AS
BEGIN

	DECLARE @ret_val MONEY
	DECLARE @monto_pendiente MONEY

	IF @transaccional = 1	/*TRANSACCIONAL*/

		SELECT @monto_pendiente=ISNULL(SUM(TRA_VALOR), 0) FROM TRANSACCION WITH(NOLOCK)
		WHERE	TRA_TIPO_MENSAJE_REQ = '0200' 
				AND TRA_FECHA_FIN IS NULL 
				AND TRA_CODIGO_ESTADO_FINAL IS NULL 
				AND TRA_COMERCIO_ID = @comercioID

	ELSE IF @transaccional = 0	/*NO TRANSACCIONAL*/

		SET @monto_pendiente = 0

	SELECT @ret_val = ( (SELECT ENT_SALDO FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = @comercioID) - @monto_pendiente)

	RETURN (CASE WHEN @ret_val IS NULL THEN 0 ELSE @ret_val END)

END
GO
/****** Object:  UserDefinedFunction [dbo].[ValidarSaldo]    Script Date: 12/03/2012 09:38:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[ValidarSaldo]
(
	@comercioID AS VARCHAR(20),
	@valor AS MONEY
)
RETURNS BIT
AS
BEGIN

	DECLARE @ret_val SMALLINT
	DECLARE @adminLocalBalance BIT

	--Obtener Validación de Administración Saldo
	SELECT @adminLocalBalance = ent_maneja_saldo FROM Entidad WHERE ent_comercio_JNTS = @comercioID

	IF @adminLocalBalance = 1
	BEGIN 
		--Maneja Saldo Local
		IF DBO.ObtenerSaldo(@comercioID, 1 /*TRANSACCIONAL*/) >= @valor	
			SET @ret_val = 1
		ELSE
			SET @ret_val = 0
	END
	ELSE
		--No Administrar Saldo
		SET @ret_val = 1
	
	RETURN @ret_val

END
GO
