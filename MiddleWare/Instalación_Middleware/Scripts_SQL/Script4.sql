USE [Conred]
GO
/****** Object:  StoredProcedure [dbo].[sp_utilsGenerarTextoRandomico]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_utilsGenerarTextoRandomico]  
	@randomString VARCHAR(100) OUTPUT
AS
BEGIN

	SET NOCOUNT ON

	DECLARE @sLength TINYINT
	DECLARE @counter TINYINT
	DECLARE @nextChar CHAR(1)
	DECLARE @nextCharINT TINYINT
	SET @counter = 1
	SET @randomString = ''
	SET @sLength = 20

	WHILE @counter <= @sLength
	BEGIN
	  SELECT @nextCharINT = ASCII(CHAR(ROUND(RAND() * 93 + 33, 0)))
	  --SOLO LETRAS(MIN/MAY) Y N�MEROS
      IF (@nextCharINT >= 65 AND @nextCharINT <= 90) OR (@nextCharINT >= 97 AND @nextCharINT <= 122) OR (@nextCharINT >= 48 AND @nextCharINT <= 57)
		BEGIN
		  SELECT @randomString = @randomString + CHAR(@nextCharINT)
		  SET @counter = @counter + 1
		END
	END

	/*PAD LEFT AND RIGHT*/
	SET @randomString = SUBSTRING(CAST(RAND() AS VARCHAR(50)),3, 10) + @randomString + SUBSTRING(CAST(RAND() AS VARCHAR(50)),3, 10)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTransaccionesNroRegistros]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTransaccionesNroRegistros](
	@BuscarPor VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @SQL AS NVARCHAR(1000)
	DECLARE @FILTERBYTEXT AS NVARCHAR(250)

	--FILTER BY
	SET @BuscarPor = '''%' + @BuscarPor + '%'''
	SET @FILTERBYTEXT = ' ( tra_id LIKE ' + @BuscarPor + ' OR tra_referencia_cliente_1 LIKE ' + @BuscarPor + ' OR tra_autorizacion_ope_id LIKE ' + @BuscarPor + ' OR tra_auth_id_p38 LIKE ' + @BuscarPor + ' OR tra_comercio_id LIKE ' + @BuscarPor + ' OR tra_stan LIKE ' + @BuscarPor + ')'

	SELECT @SQL = 'SELECT COUNT(*) AS COUNTER_DB FROM Transaccion t WITH(NOLOCK) INNER JOIN MensajeEstado me ON (t.tra_codigo_estado_final = me.cod_estado) WHERE ' + @FILTERBYTEXT + ' AND (t.tra_fecha_inicial BETWEEN DATEADD(MINUTE,-10,GETDATE()) AND GETDATE())'

	--PRINT @SQL

	EXEC sp_executesql @SQL

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTransacciones]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTransacciones](
	@Pagina INT,
	@RegistrosporPagina INT,
	@OrdenarporColumnaID INT,
	@TipoOrdenamiento VARCHAR(10),	/*ASC � DESC*/
	@BuscarPor VARCHAR(100)
) AS
BEGIN

	DECLARE @SQL AS NVARCHAR(1500)
	DECLARE @ORDERBYCOLUMN AS NVARCHAR(50)
	DECLARE @FILTERBYTEXT AS NVARCHAR(250)

	SET NOCOUNT ON;

	DECLARE @PaginaNueva INT
	DECLARE @Comodin INT

	SET @PaginaNueva = CAST(@Pagina AS INT) - 1 

	IF @PaginaNueva > 0
		SET @Comodin = 1
	ELSE 
		SET @Comodin = 0

	--ORDER BY
	SELECT @ORDERBYCOLUMN = (
		CASE CAST((CAST(@OrdenarporColumnaID AS INT) + 1) AS VARCHAR(10))
			WHEN '1' THEN 'tra_fecha_inicial'
			WHEN '2' THEN 'tra_tipo_mensaje_req'
			WHEN '3' THEN 'tra_tipo_mensaje_resp'
			WHEN '4' THEN 'tra_codigo_proceso'
			WHEN '5' THEN 'tra_codigo_respuesta'
			WHEN '6' THEN 'tra_comercio_id'			
			WHEN '7' THEN 'tra_stan'
			WHEN '8' THEN 'tra_referencia_cliente_1'
			WHEN '9' THEN 'tra_auth_id_p38'
			WHEN '10' THEN 'tra_autorizacion_ope_id'
			WHEN '11' THEN 'tra_datos_adicionales'
			WHEN '12' THEN 'mensaje'
		END
	)

	--FILTER BY
	SET @BuscarPor = '''%' + @BuscarPor + '%'''
	SET @FILTERBYTEXT = ' ( tra_referencia_cliente_1 LIKE ' + @BuscarPor + ' OR tra_auth_id_p38 LIKE ' + @BuscarPor + ' OR tra_comercio_id LIKE ' + @BuscarPor + ' OR tra_autorizacion_ope_id LIKE ' + @BuscarPor + ' OR tra_stan LIKE ' + @BuscarPor + ')'

	SELECT @SQL =
		'SELECT tra_fecha_inicial, tra_tipo_mensaje_req, tra_tipo_mensaje_resp, tra_codigo_proceso, tra_codigo_respuesta, tra_comercio_id, tra_stan, tra_referencia_cliente_1, 
			tra_auth_id_p38, tra_autorizacion_ope_id, tra_datos_adicionales, mensaje FROM (
			SELECT 
				ROW_NUMBER() Over(Order by ' + @ORDERBYCOLUMN + ' ' + @TipoOrdenamiento + ') As RowNum,
				CONVERT(VARCHAR, tra_fecha_inicial, 113) AS tra_fecha_inicial, 
				tra_tipo_mensaje_req, 
				tra_tipo_mensaje_resp,
				tra_codigo_proceso, 
				tra_codigo_respuesta, 
				tra_comercio_id,
				CAST(tra_stan AS VARCHAR) AS tra_stan, 				
				tra_referencia_cliente_1, 
				ISNULL(tra_auth_id_p38, '''') AS tra_auth_id_p38, 
				CAST(( CASE WHEN CAST(tra_autorizacion_ope_id AS VARCHAR(30))='''' THEN NULL ELSE tra_autorizacion_ope_id END) AS NUMERIC(20,0)) AS tra_autorizacion_ope_id, 
				CASE WHEN LEN(tra_datos_adicionales) > 20 THEN SUBSTRING(tra_datos_adicionales,1,20) + ''...'' ELSE tra_datos_adicionales END AS tra_datos_adicionales,	
				me.mensaje 
			FROM Transaccion t WITH(NOLOCK)
			LEFT OUTER JOIN MensajeEstado me ON (t.tra_codigo_estado_final = me.cod_estado)
			WHERE ' + @FILTERBYTEXT + ' AND (t.tra_fecha_inicial BETWEEN DATEADD(MINUTE,-10,GETDATE()) AND GETDATE())' + 
		') AS ResultadoPaginado
		WHERE RowNum BETWEEN ' + CAST( ((@PaginaNueva * CAST(@RegistrosporPagina AS INT)) + @Comodin) AS VARCHAR(50)) + ' AND ' + CAST((@Pagina * CAST(@RegistrosporPagina AS INT)) AS VARCHAR(50)) 

	--PRINT @SQL

	EXEC sp_executesql @SQL

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCerrarSesionWeb]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCerrarSesionWeb](
	@loginID AS INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE LoginInfo
	SET
		login_fecha_fin = GETDATE()
	WHERE 
		login_id = @loginID

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCargarRegistrosMovilred]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCargarRegistrosMovilred] 
(	
	@path varchar(80)
)
AS

TRUNCATE TABLE ConcMovilredINTemp

EXEC('
BULK INSERT ConcMovilredINTemp 
   FROM ''' + @path + '''
   WITH 
      (
         FIELDTERMINATOR ='','',
         ROWTERMINATOR =''\n''
      )
')
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTerminalesXComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTerminalesXComercio](
	@filterMerchant VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TERMINAL_ID, ACTIVO /*(CASE WHEN ACTIVO=1 THEN 1 ELSE 2 END) AS ACTIVO*/, DESCRIPCION 
	FROM assenda.dbo.TERMINALES_ASSENDA  
	WHERE COMERCIO_ID = @filterMerchant
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarProveedores]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarProveedores]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		P.prov_id,
		P.prov_nombre,
		P.prov_descripcion,
		P.prov_isomux_nombre
	FROM Proveedor P

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarEstadoFormateadorMovilred]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarEstadoFormateadorMovilred](
	@formatterName VARCHAR(20),
	@channelID SMALLINT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS VARCHAR(20)

	IF NOT EXISTS(SELECT * FROM EstadoCanalMovilred WHERE id_canal = @channelID)
		SET @status = 'ERROR'
	ELSE
	BEGIN
		SELECT @status = estado FROM EstadoCanalMovilred WHERE id_canal = @channelID
	END

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarEstadoFormateadorJNTS]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarEstadoFormateadorJNTS](
	@formatterName VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS VARCHAR(20)

	IF NOT EXISTS(SELECT * FROM EstadoCanalJNTS)
		SET @status = 'ERROR'
	ELSE
	BEGIN
		SELECT @status = estado FROM EstadoCanalJNTS
	END

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webContarErroresClave]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webContarErroresClave](
	@userID INT
)
AS
BEGIN

	DECLARE @errorCounter AS INT

	SELECT @errorCounter = COUNT(*) FROM LoginInfo WHERE login_tipo_id = 3 AND login_usuario_id = @userID AND login_fecha_inicio BETWEEN DATEADD(MINUTE,-30,GETDATE()) AND GETDATE()

	SELECT @errorCounter AS ERRORCOUNTER

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webDiffTrxMovilredConred]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webDiffTrxMovilredConred] 
AS
BEGIN

	/*TRANSACCIONES POR AUTH_ID QUE ESTAN EN MOVILRED Y NO EN CONRED*/

	SELECT * FROM ConcMovilredINTemp WHERE tra_ExternalReference not in (
		SELECT tra_id FROM  ConcConredINTemp
	)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webDiffTrxConredMovilred]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webDiffTrxConredMovilred] 
AS
BEGIN

	/*TRANSACCIONES POR AUTH_ID QUE ESTAN EN CONRED Y NO EN MOVILRED*/

	SELECT * FROM ConcConredINTemp WHERE tra_id not in (
		SELECT tra_ExternalReference FROM  ConcMovilredINTemp
	)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCrearProveedor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearProveedor](
	@supplierName VARCHAR(50),
	@supplierDesc VARCHAR(100),
	@supplierIsomuxName VARCHAR(50)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		INSERT INTO Proveedor(prov_nombre, prov_descripcion, prov_isomux_nombre)
			VALUES (@supplierName, @supplierDesc, @supplierIsomuxName)

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcPostprocesarTrxAdministrativa]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcPostprocesarTrxAdministrativa](
	/*PAR�METROS DE ENTRADA*/
	@trxID AS BIGINT,
	@tipoMsjResp AS VARCHAR(5),
	@codigoRespuesta AS VARCHAR(2)		
)
AS
BEGIN

	BEGIN TRANSACTION

	BEGIN TRY

		UPDATE TransaccionAdministrativa
			SET tra_fecha_fin=GETDATE(), 
				tra_tipo_mensaje_resp=@tipoMsjResp, 
				tra_cod_respuesta=@codigoRespuesta
		WHERE 
			tra_id = @trxID
		
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
	
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;	
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcPostProcesarReverso]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcPostProcesarReverso](
	/*PAR�METROS DE ENTRADA*/
	@trxID AS BIGINT, 
	@tipoMensajeResp AS VARCHAR(10),	
	@authID AS VARCHAR(6),
	@codRespuestaFormateador AS VARCHAR(10),
	@codRespuestaISO AS VARCHAR(2),
	/*PAR�METROS DE SALIDA*/
	@codRespuesta AS VARCHAR(10) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES*/
	DECLARE @retorno_sp AS SMALLINT
	DECLARE @comercio_id AS VARCHAR(20)
	DECLARE @valor_venta AS MONEY
	DECLARE @tipoMensaje AS VARCHAR(10)
	DECLARE @codigoProcesamiento AS VARCHAR(10)
	
	BEGIN TRANSACTION;

	BEGIN TRY
		/*L�GICA DEL NEGOCIO*/
		
		/*ACTUALIZAR REGISTRO DE TRANSACCION X REVERSO*/
		UPDATE TransaccionXReverso
		SET		tra_fecha_fin				= GETDATE(),
				tra_tipo_mensaje_resp		= @tipoMensajeResp, 
				tra_codigo_respuesta		= @codRespuestaISO,
				tra_auth_id_p38				= (CASE WHEN @authID='0' THEN NULL ELSE @authID END),
				tra_codigo_estado_final		= @codRespuestaFormateador
		WHERE tra_id = @trxID

		/*PAR�METROS DE SALIDA OK*/
		SELECT @codRespuesta = 'JTS000000'
		
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
	
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @codRespuesta = 'JTS940000'

		PRINT(ERROR_MESSAGE())

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcConsultarParametrosServidor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcConsultarParametrosServidor](
	@codigoInstancia smallint
)
AS
BEGIN

	---RETORNA LA LISTA DE PAR�METROS PARA INICIALIZACI�N DEL SERVIDOR SEG�N INSTANCIA
	SELECT nombre_parametro, dato FROM ConfiguracionServidor WHERE codigo_instancia = @codigoInstancia

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcConsultarParametrosFormateador]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcConsultarParametrosFormateador](
	@nombreFormateador VARCHAR(100)
)
AS
BEGIN

	---RETORNA LA LISTA DE PAR�METROS PARA INICIALIZACI�N DEL FORMATEADOR
	SELECT nombre_parametro, dato FROM ConfiguracionFormateador WHERE nombre_formateador = @nombreFormateador

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcActualizarEstadoMovilred]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcActualizarEstadoMovilred](
	/*PAR�METROS DE ENTRADA*/
	@estado AS VARCHAR(20),
	@canalID AS INT
)
AS
BEGIN

	IF EXISTS(SELECT * FROM EstadoCanalMovilred WHERE id_canal = @canalID)
	BEGIN
		UPDATE EstadoCanalMovilred
			SET estado = @estado
		WHERE id_canal = @canalID
	END
	ELSE
	BEGIN
		INSERT INTO EstadoCanalMovilred(id_canal, estado)
		VALUES(@canalID, @estado)
	END	

	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcActualizarEstadoJNTS]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcActualizarEstadoJNTS](
	/*PAR�METROS DE ENTRADA*/
	@estado AS VARCHAR(20)
)
AS
BEGIN

	IF EXISTS(SELECT * FROM EstadoCanalJNTS)
	BEGIN
		UPDATE EstadoCanalJNTS
			SET estado = @estado
	END
	ELSE
	BEGIN
		INSERT INTO EstadoCanalJNTS(estado)
		VALUES(@estado)
	END	

	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcPreprocesarTrxAdministrativa]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcPreprocesarTrxAdministrativa](
	/*PAR�METROS DE ENTRADA*/
	@interfazOrigen AS VARCHAR(20),
	@interfazDestino AS VARCHAR(20),
	@tipoMsjReq AS VARCHAR(5),
	@stan AS VARCHAR(6),
	@terminalID AS VARCHAR(10),
	@campoP70 AS VARCHAR(5),
	/*PAR�METROS DE SALIDA*/
	@trxID AS BIGINT OUTPUT	
)
AS
BEGIN

	BEGIN TRANSACTION

	BEGIN TRY

		INSERT INTO TransaccionAdministrativa(tra_fecha_ini, tra_interfaz_origen, tra_interfaz_destino, 
												tra_tipo_mensaje_req, tra_campo_p70, tra_stan, tra_terminal_id)
		VALUES(GETDATE(), @interfazOrigen, @interfazDestino, @tipoMsjReq, @campoP70, @stan, @terminalID)	

		SELECT @trxID = SCOPE_IDENTITY()
		
		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		PRINT(ERROR_MESSAGE())
	
		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1
	
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

	END CATCH;	
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webActualizarParametrosServidor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webActualizarParametrosServidor](
	@instanciaID				VARCHAR(10),
    @LocalPort					VARCHAR(10),
    @LocalTextLog				VARCHAR(10),
    @LocalXMLLog				VARCHAR(10),
    @minThreadPool				VARCHAR(10),
    @maxThreadPool				VARCHAR(10),
    @TotalTimeoutJTranServer	VARCHAR(10),
    @maxAttempsConnection		VARCHAR(10),
    @minSaleAmount				VARCHAR(10),
    @maxSaleAmount				VARCHAR(10)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS INT

	/*Actualizar Par�metros x Formateador*/
	UPDATE ConfiguracionServidor SET dato = @LocalPort WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'LocalPort'
	UPDATE ConfiguracionServidor SET dato = @LocalTextLog WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'LocalTextLog'
	UPDATE ConfiguracionServidor SET dato = @LocalXMLLog WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'LocalXMLLog'
	UPDATE ConfiguracionServidor SET dato = @minThreadPool WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'minThreadPool'
	UPDATE ConfiguracionServidor SET dato = @maxThreadPool WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'maxThreadPool'
	UPDATE ConfiguracionServidor SET dato = @TotalTimeoutJTranServer WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'TotalTimeoutJTranServer'
	UPDATE ConfiguracionServidor SET dato = @maxAttempsConnection WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'maxAttempsConnection'
	UPDATE ConfiguracionServidor SET dato = @minSaleAmount WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'minSaleAmount'
	UPDATE ConfiguracionServidor SET dato = @maxSaleAmount WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'maxSaleAmount'	

	SET @status = 1	/* OK */

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webActualizarParametrosFormateador]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webActualizarParametrosFormateador](
	@nombreFormateador	 VARCHAR(100),
    @LocalPort           VARCHAR(10),
    @LocalTextLog        VARCHAR(10),
    @LocalXMLLog         VARCHAR(10),
    @minThreadPool       VARCHAR(10),
    @maxThreadPool       VARCHAR(10),
    @destinationIP1      VARCHAR(20),
    @destinationPort1    VARCHAR(10),
    @destinationTimeout1 VARCHAR(10),
    /*Datos Extra Movilred*/
    @nitMovilred		 VARCHAR(20),
    @userMovilred		 VARCHAR(20),
    @passwordMovilred    VARCHAR(20),
    @destinationIP2      VARCHAR(20),
    @destinationPort2    VARCHAR(10),
    @destinationTimeout2 VARCHAR(10),    
    @destinationIP3      VARCHAR(20),
    @destinationPort3    VARCHAR(10),
    @destinationTimeout3 VARCHAR(10)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS INT

	/*Actualizar Par�metros x Formateador[Datos Generales]*/
	UPDATE ConfiguracionFormateador SET dato = @LocalPort WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'LocalPort'
	UPDATE ConfiguracionFormateador SET dato = @LocalTextLog WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'LocalTextLog'
	UPDATE ConfiguracionFormateador SET dato = @LocalXMLLog WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'LocalXMLLog'
	UPDATE ConfiguracionFormateador SET dato = @minThreadPool WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'minThreadPool'
	UPDATE ConfiguracionFormateador SET dato = @maxThreadPool WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'maxThreadPool'
	UPDATE ConfiguracionFormateador SET dato = @destinationIP1 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationIP1'
	UPDATE ConfiguracionFormateador SET dato = @destinationPort1 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationPort1'
	UPDATE ConfiguracionFormateador SET dato = @destinationTimeout1 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationTimeout1'

	/*Actualizaci�n de Par�metros en Config. de Servidor seg�n Formateador - Instancia 0*/
	DECLARE @instanciaID SMALLINT
	SET @instanciaID = 0

	IF @nombreFormateador = 'COMCEL_FORMATTER'
	BEGIN
		UPDATE ConfiguracionServidor SET dato = @LocalPort WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'PortComcelFormatter'
		UPDATE ConfiguracionServidor SET dato = CAST((CAST(@destinationTimeout1 AS BIGINT) + 1000) AS VARCHAR) WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'totalTimeoutComcelFormatter'	
	END
	ELSE IF @nombreFormateador = 'MOVILRED_FORMATTER'
	BEGIN
		/*Datos Extra Movilred*/
		UPDATE ConfiguracionFormateador SET dato = @nitMovilred WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'NIT'
		UPDATE ConfiguracionFormateador SET dato = @userMovilred WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'user'
		UPDATE ConfiguracionFormateador SET dato = @passwordMovilred WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'password'		
		UPDATE ConfiguracionFormateador SET dato = @destinationIP2 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationIP2'
		UPDATE ConfiguracionFormateador SET dato = @destinationPort2 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationPort2'
		UPDATE ConfiguracionFormateador SET dato = @destinationTimeout2 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationTimeout2'		
		UPDATE ConfiguracionFormateador SET dato = @destinationIP3 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationIP3'
		UPDATE ConfiguracionFormateador SET dato = @destinationPort3 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationPort3'
		UPDATE ConfiguracionFormateador SET dato = @destinationTimeout3 WHERE nombre_formateador = @nombreFormateador AND nombre_parametro = 'destinationTimeout3'		
		
		UPDATE ConfiguracionServidor SET dato = @LocalPort WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'PortMovilredFormatter'	
		UPDATE ConfiguracionServidor SET dato = CAST((CAST(@destinationTimeout1 AS BIGINT) + 1000) AS VARCHAR) WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'totalTimeoutMovilredFormatter'	
	END
	ELSE IF @nombreFormateador = 'TIGO_FORMATTER'
	BEGIN
		UPDATE ConfiguracionServidor SET dato = @LocalPort WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'PortTigoFormatter'	
		UPDATE ConfiguracionServidor SET dato = CAST((CAST(@destinationTimeout1 AS BIGINT) + 1000) AS VARCHAR) WHERE codigo_instancia = @instanciaID AND nombre_parametro = 'totalTimeoutTigoFormatter'	
	END	

	SET @status = 1	/* OK */

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webReporteVentasComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteVentasComercio](
	@merchantID AS VARCHAR(20),
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		T.tra_fecha_inicial,
		T.tra_comercio_id,
		T.tra_terminal_id,
		P.pro_nombre,
		T.tra_referencia_cliente_1,
		CAST(( CASE WHEN CAST(tra_autorizacion_ope_id AS VARCHAR(30))='' THEN NULL ELSE tra_autorizacion_ope_id END) AS NUMERIC(20,0)) AS tra_autorizacion_ope_id, 
		T.tra_auth_id_p38,
		T.tra_comercio_saldo_inicial,
		T.tra_valor,
		T.tra_comercio_saldo_final 
	FROM Transaccion T WITH(NOLOCK)
		LEFT OUTER JOIN Producto P ON (T.tra_producto_id = P.pro_id)
	WHERE (T.tra_comercio_id = @merchantID OR @merchantID = '-1') AND
		T.tra_fecha_inicial >= @startDate AND T.tra_fecha_inicial <= @endDateReport
		AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_respuesta = '00'
	ORDER BY 1
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webReporteResumenVentas]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteResumenVentas](
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	DECLARE @tmpTable AS TABLE(tra_fecha VARCHAR(20), /*ventas_comcel_directo MONEY, ventas_tigo_directo MONEY,*/ ventas_comcel_movilred MONEY, ventas_tigo_movilred MONEY, ventas_movistar_movilred MONEY, ventas_otros_recargas_movilred MONEY, tra_total_recargas MONEY, recaudo_facturas_movilred MONEY, tra_total_recaudos MONEY)

	INSERT INTO @tmpTable
		SELECT 
			CONVERT(VARCHAR, T.tra_fecha_inicial, 103) AS tra_fecha,
			--SUM(CASE WHEN T.tra_producto_id = 100 THEN T.tra_valor ELSE 0 END) AS ventas_comcel_directo,
			--SUM(CASE WHEN T.tra_producto_id = 110 THEN T.tra_valor ELSE 0 END) AS ventas_tigo_directo,
			SUM(CASE WHEN T.tra_producto_id = 120 THEN T.tra_valor ELSE 0 END) AS ventas_comcel_movilred,
			SUM(CASE WHEN T.tra_producto_id = 130 THEN T.tra_valor ELSE 0 END) AS ventas_tigo_movilred,
			SUM(CASE WHEN T.tra_producto_id = 140 THEN T.tra_valor ELSE 0 END) AS ventas_movistar_movilred,			
			SUM(CASE WHEN T.tra_producto_id IN (150, 160, 170) THEN T.tra_valor ELSE 0 END) AS ventas_otros_recargas_movilred,			
			SUM(CASE WHEN T.tra_producto_id IN (120, 130, 140, 150, 160, 170, 180) THEN T.tra_valor ELSE 0 END) AS tra_total_recargas,
			SUM(CASE WHEN T.tra_producto_id = 200 THEN T.tra_valor ELSE 0 END) AS recaudo_facturas_movilred,
			SUM(CASE WHEN T.tra_producto_id IN (200) THEN T.tra_valor ELSE 0 END) AS tra_total_recaudos
		FROM Transaccion T WITH(NOLOCK)
		WHERE T.tra_fecha_inicial >= @startDate AND T.tra_fecha_inicial <= @endDateReport
			AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_respuesta = '00'
		GROUP BY CONVERT(VARCHAR, T.tra_fecha_inicial, 103)
		
	--Select Final
	SELECT * FROM @tmpTable
	
	UNION ALL
	
	SELECT NULL, /*NULL, NULL,*/ NULL, NULL, NULL, NULL, SUM(T.tra_total_recargas), NULL, SUM(T.tra_total_recaudos) FROM @tmpTable T
			
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webBuscarTransaccionesRecargaVista]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webBuscarTransaccionesRecargaVista](
	@filterTxt AS VARCHAR(50)
) AS
BEGIN

	SET DATEFORMAT DMY

	SELECT 
		CONVERT(VARCHAR, tra_fecha_inicial, 103) + ' ' + CONVERT(VARCHAR, tra_fecha_inicial, 114) AS tra_fecha_inicial, 
		tra_tipo_mensaje_req, 
		tra_codigo_proceso, 
		tra_codigo_respuesta, 
		tra_comercio_id,
		tra_terminal_id,
		CAST(tra_stan AS VARCHAR) AS tra_stan, 				
		tra_referencia_cliente_1, 
		CAST(tra_valor AS INTEGER) AS tra_valor,
		tra_auth_id_p38, 
		tra_autorizacion_ope_id, 
		me.mensaje 
	FROM Transaccion t WITH(NOLOCK)
	INNER JOIN MensajeEstado me ON (t.tra_codigo_estado_final = me.cod_estado)
	WHERE (t.tra_referencia_cliente_1 LIKE @filterTxt OR t.tra_auth_id_p38 LIKE @filterTxt 
			OR t.tra_autorizacion_ope_id LIKE @filterTxt OR CAST(t.tra_stan AS VARCHAR(10)) LIKE @filterTxt) AND
			t.tra_fecha_inicial > CAST(CONVERT(VARCHAR, GETDATE(), 103) AS DATETIME) AND
			t.tra_codigo_proceso = '380000'	--SOLO RECARGAS
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webBuscarTransaccionesPagoFacturasVista]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webBuscarTransaccionesPagoFacturasVista](
	@filterTxt AS VARCHAR(50)
) AS
BEGIN

	SELECT 
		CONVERT(VARCHAR, tra_fecha_inicial, 103) + ' ' + CONVERT(VARCHAR, tra_fecha_inicial, 114) AS tra_fecha_inicial, 
		tra_tipo_mensaje_req, 
		tra_codigo_proceso, 
		tra_codigo_respuesta, 
		tra_comercio_id,
		tra_terminal_id,
		CAST(tra_stan AS VARCHAR) AS tra_stan, 				
		tra_referencia_cliente_1, 
		CAST(tra_valor AS INTEGER) AS tra_valor,
		tra_auth_id_p38, 
		CASE WHEN LEN(tra_datos_adicionales) > 20 THEN SUBSTRING(tra_datos_adicionales,1,20) + '...' ELSE tra_datos_adicionales END AS tra_datos_adicionales,
		me.mensaje 
	FROM Transaccion t WITH(NOLOCK)
	INNER JOIN MensajeEstado me ON (t.tra_codigo_estado_final = me.cod_estado)
	WHERE (t.tra_referencia_cliente_1 LIKE @filterTxt OR t.tra_auth_id_p38 LIKE @filterTxt 
			OR CAST(CAST(tra_valor AS NUMERIC(18,0)) AS VARCHAR(20)) LIKE @filterTxt OR CAST(t.tra_stan AS VARCHAR(10)) LIKE @filterTxt) AND
			t.tra_fecha_inicial > CAST(CONVERT(VARCHAR, GETDATE(), 103) AS DATETIME) AND
			t.tra_codigo_proceso = '150000'	--SOLO PAGO DE FACTURAS
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCrearOpcion]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearOpcion](
	@optionTitle VARCHAR(100),
	@optionURL VARCHAR(100),
	@optionIMGURL VARCHAR(100),
	@optionType INT,
	@optionParentID INT
)
AS
BEGIN

	DECLARE @optionOrder AS INT

	IF @optionType = 1	
		SELECT @optionOrder = MAX(opc_orden) + 1 FROM Opcion WHERE opc_tipo_opcion_id = @optionType AND opc_opcion_padre_id IS NULL
	ELSE IF @optionType = 2
		SELECT @optionOrder = MAX(opc_orden) + 1 FROM Opcion WHERE opc_tipo_opcion_id = @optionType AND opc_opcion_padre_id = @optionParentID
	 
	IF @optionOrder IS NULL
		SET @optionOrder = 1
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		INSERT INTO Opcion(opc_titulo, opc_url, opc_img_url, opc_tipo_opcion_id, opc_opcion_padre_id, opc_orden)
			VALUES (@optionTitle, @optionURL, @optionIMGURL, @optionType, @optionParentID, @optionOrder)

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTrxMovilredxDia]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTrxMovilredxDia](
	@concDate DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @conDateFinal DATETIME
	SET @conDateFinal = DATEADD(DAY, 1,@concDate)
	
	INSERT INTO ConcConredINTemp		
		SELECT 
			T.tra_id,
			T.tra_referencia_cliente_1,
			CONVERT(VARCHAR, T.tra_fecha_fin , 103),
			CONVERT(VARCHAR, T.tra_fecha_fin , 108),
			CAST(CAST(T.tra_valor AS INT) AS VARCHAR),
			P.pro_nombre 
		FROM Transaccion T WITH(NOLOCK)
			INNER JOIN Producto P ON (T.tra_producto_id = P.pro_id)
		WHERE T.tra_fecha_inicial > @concDate AND T.tra_fecha_fin < @conDateFinal
			AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_proceso = '380000' 
			AND T.tra_codigo_respuesta = '00'
				
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTrxMovilredRecargas]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTrxMovilredRecargas](
	@fechaConsultaTxt AS VARCHAR(10)
)
AS
BEGIN

	SET NOCOUNT ON;
	SET DATEFORMAT MDY
	
	DECLARE @fechaConsultaIni AS DATETIME
	DECLARE @fechaConsultaFin AS DATETIME
	
	SET @fechaConsultaIni = CAST(@fechaConsultaTxt AS DATETIME)
	SET @fechaConsultaFin = DATEADD(DAY,1,@fechaConsultaIni)

	SELECT 
		'CARVAJAL',
		P.pro_nombre,
		REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 102), '.', '-'),
		REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 108), '.', '-'),
		ISNULL(CAST(T.tra_auth_id_p38 AS VARCHAR(12)), '0'),
		CASE WHEN T.tra_codigo_respuestaB24 = '00' THEN 'APROBADA' ELSE 'RECHAZADA' END,
		CAST(T.tra_codigo_respuestaB24 AS VARCHAR(2)),
		ISNULL(CAST(CAST(T.tra_autorizacion_ope_id AS NUMERIC(20,0)) AS VARCHAR(20)), '0'),
		CAST(CAST(T.tra_valor AS INT) AS VARCHAR),
		T.tra_referencia_cliente_1,
		CAST(T.tra_rrn AS VARCHAR(12)),
		CAST(DATEDIFF(ms, T.tra_fecha_inicial, T.tra_fecha_fin) AS VARCHAR)
	FROM Transaccion T WITH(NOLOCK)
		INNER JOIN Producto P ON (T.tra_producto_id = P.pro_id)
	WHERE T.tra_fecha_inicial > @fechaConsultaIni AND T.tra_fecha_fin < @fechaConsultaFin
		AND T.tra_tipo_mensaje_req = '0200' --AND T.tra_codigo_respuesta = '00'
		AND T.tra_codigo_proceso = '380000'	--SOLO RECARGAS
	ORDER BY T.tra_fecha_inicial

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTransaccionesVista]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTransaccionesVista] AS
BEGIN

	SELECT 
		CONVERT(VARCHAR, tra_fecha_inicial, 103) + ' ' + CONVERT(VARCHAR, tra_fecha_inicial, 114) AS tra_fecha_inicial, 
		tra_tipo_mensaje_req, 
		tra_tipo_mensaje_resp, 
		tra_codigo_proceso, 
		tra_codigo_respuesta, 
		tra_comercio_id,
		tra_terminal_id,
		CAST(tra_stan AS VARCHAR) AS tra_stan, 				
		tra_referencia_cliente_1, 
		CAST(tra_valor AS INTEGER) AS tra_valor,
		tra_auth_id_p38, 
		CAST(( CASE WHEN CAST(tra_autorizacion_ope_id AS VARCHAR(30))='' THEN NULL ELSE tra_autorizacion_ope_id END) AS NUMERIC(20,0)) AS tra_autorizacion_ope_id, 
		CASE WHEN LEN(tra_datos_adicionales) > 20 THEN SUBSTRING(tra_datos_adicionales,1,20) + '...' ELSE tra_datos_adicionales END AS tra_datos_adicionales,
		me.mensaje 
	FROM Transaccion t WITH(NOLOCK)
	LEFT OUTER JOIN MensajeEstado me ON (t.tra_codigo_estado_final = me.cod_estado)
	WHERE (t.tra_fecha_inicial BETWEEN DATEADD(MINUTE,-10,GETDATE()) AND GETDATE())
	ORDER BY tra_fecha_inicial DESC

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarEstadisticasTrx]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarEstadisticasTrx](
	@type INT
)
AS
BEGIN

	DECLARE
		@STATS1 AS INT, @STATS2 AS INT, @STATS3 AS INT, @STATS4 AS INT, @STATS5 AS MONEY, @STATS6 AS INT, @STATS7 AS MONEY

	SELECT
		@STATS1 = 0, @STATS2 = 0, @STATS3 = 0, @STATS4 = 0, @STATS5 = 0, @STATS6 = 0, @STATS7 = 0

	SET NOCOUNT ON;
	
	--NRO. TOTAL DE TRX. VENTA
	SELECT @STATS1 = COUNT(*) FROM Transaccion T WITH(NOLOCK)
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_proceso = '380000'

	--NRO. TOTAL DE TRX. CONSULTAS DE SALDO
	SELECT @STATS2 = COUNT(*) FROM Transaccion T WITH(NOLOCK) 
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0800' AND T.tra_codigo_proceso = '380000'

	--NRO. TOTAL DE TRX. ECHO TEST
	SELECT @STATS3 = COUNT(*) FROM Transaccion T WITH(NOLOCK)
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0800' AND T.tra_codigo_proceso = '990000'

	--NRO. TOTAL DE TRX. VENTA - APROBADAS
	SELECT @STATS4 = COUNT(*) FROM Transaccion T WITH(NOLOCK) 
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_proceso = '380000' AND T.tra_codigo_respuesta = '00'

	--VALOR TOTAL DE TRX. VENTA - APROBADAS
	SELECT @STATS5 = SUM(T.tra_valor) FROM Transaccion T WITH(NOLOCK) 
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_proceso = '380000' AND T.tra_codigo_respuesta = '00'

	--NRO. TOTAL DE TRX. VENTA - NO AUTORIZADAS
	SELECT @STATS6 = COUNT(*) FROM Transaccion T WITH(NOLOCK)
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_proceso = '380000' AND T.tra_codigo_respuesta <> '00'

	--VALOR TOTAL DE TRX. VENTA - NO AUTORIZADAS
	SELECT @STATS7 = SUM(T.tra_valor) FROM Transaccion T WITH(NOLOCK) 
	WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, GETDATE(), 103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_proceso = '380000' AND T.tra_codigo_respuesta <> '00'

	SELECT
		(CASE WHEN @STATS1 IS NULL THEN 0 ELSE @STATS1 END) AS STATS1,
		(CASE WHEN @STATS2 IS NULL THEN 0 ELSE @STATS2 END) AS STATS2,
		(CASE WHEN @STATS3 IS NULL THEN 0 ELSE @STATS3 END) AS STATS3,
		(CASE WHEN @STATS4 IS NULL THEN 0 ELSE @STATS4 END) AS STATS4,
		REPLACE(CAST((CASE WHEN @STATS5 IS NULL THEN 0 ELSE @STATS5 END) AS VARCHAR), '.00', '') AS STATS5,
		(CASE WHEN @STATS6 IS NULL THEN 0 ELSE @STATS6 END) AS STATS6,
		REPLACE(CAST((CASE WHEN @STATS7 IS NULL THEN 0 ELSE @STATS7 END) AS VARCHAR), '.00', '') AS STATS7
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarEstadisticasMonitor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarEstadisticasMonitor]
AS
BEGIN

	---NRO. DE TRANSACCIONES DE VENTA 30 MINUTOS ATR�S
	SELECT TOP 31
		RIGHT(REPLICATE('0',2) + CAST(DATEPART(HOUR, T.TRA_FECHA_INICIAL) AS VARCHAR),2) AS HORA, RIGHT(REPLICATE('0',2) + CAST(DATEPART(MINUTE, T.TRA_FECHA_INICIAL) AS VARCHAR),2) AS MINUTO,
		COUNT(*) AS TOTAL_TRX 
	FROM TRANSACCION T WITH(NOLOCK)
		RIGHT OUTER JOIN TIPOMENSAJEISO TM ON (TM.TIPO_MENSAJE = T.TRA_TIPO_MENSAJE_REQ AND TM.CODIGO_PROCESO = T.TRA_CODIGO_PROCESO AND T.tra_tipo_mensaje_req = '0200')
	WHERE T.TRA_FECHA_INICIAL > CAST(CAST(DATEADD(MINUTE, -30, GETDATE()) AS VARCHAR) AS DATETIME)
	GROUP BY DATEPART(HOUR, T.TRA_FECHA_INICIAL), DATEPART(MINUTE, T.TRA_FECHA_INICIAL)
	ORDER BY 1, 2
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarEstadisticas]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarEstadisticas](
	@type INT
)
AS
BEGIN

	DECLARE @TMP TABLE(FECHA VARCHAR(50), TIPO_TRX VARCHAR(50), TOTAL_TRX INT)
	DECLARE @TMP_TIPOS TABLE(FECHA VARCHAR(50), TIPO_TRX VARCHAR(50), TOTAL_TRX INT)
	DECLARE @titulo VARCHAR(100)

	SET NOCOUNT ON;
	
	IF @type = 1
	BEGIN
		---ESTAD�STICAS NUMERO DE VENTAS APROBADAS POR OPERADOR
		SELECT PRO_NOMBRE, TOTAL_TRX
		FROM (
			SELECT P.PRO_ID, P.PRO_NOMBRE, COUNT(*) AS TOTAL_TRX 
			FROM TRANSACCION T WITH(NOLOCK)
				RIGHT OUTER JOIN PRODUCTO P ON (P.PRO_ID = T.TRA_PRODUCTO_ID AND T.TRA_TIPO_MENSAJE_REQ = '0200' AND T.TRA_CODIGO_PROCESO = '380000' AND T.TRA_CODIGO_RESPUESTA = '00' AND T.TRA_FECHA_INICIAL > CONVERT(VARCHAR, GETDATE(), 103))
			GROUP BY P.PRO_ID, P.PRO_NOMBRE
		) RES WHERE PRO_ID IN (100, 110, 120, 130, 140)
	END
	ELSE IF @type = 2
	BEGIN
		---ESTAD�STICAS NUMERO DE VENTAS NO APROBADAS POR OPERADOR
		SELECT PRO_NOMBRE, TOTAL_TRX
		FROM (
			SELECT P.PRO_ID, P.PRO_NOMBRE, COUNT(*) AS TOTAL_TRX 
			FROM TRANSACCION T WITH(NOLOCK)
				RIGHT OUTER JOIN PRODUCTO P ON (P.PRO_ID = T.TRA_PRODUCTO_ID AND T.TRA_TIPO_MENSAJE_REQ = '0200' AND T.TRA_CODIGO_PROCESO = '380000' AND T.TRA_CODIGO_RESPUESTA <> '00' AND T.TRA_FECHA_INICIAL > CONVERT(VARCHAR, GETDATE(), 103))
			GROUP BY P.PRO_ID, P.PRO_NOMBRE
		) RES WHERE PRO_ID IN (100, 110, 120, 130, 140)
	END
	ELSE IF @type = 3
	BEGIN
		INSERT INTO @TMP
			---ESTAD�STICAS NUMERO DE TRANSACCIONES POR TIPO - AUTORIZADAS [5 horas atr�s]
			SELECT CONVERT(CHAR(13),T.TRA_FECHA_INICIAL,120) + ':00' AS FECHA, 
				(CASE TM.TIPO_MENSAJE
					WHEN '0200' THEN CASE TM.CODIGO_PROCESO WHEN '380000' THEN 'Ventas' END
					WHEN '0800' THEN 
									CASE TM.CODIGO_PROCESO WHEN '000001' THEN 'Consultas de Saldo'
															WHEN '990000' THEN 'Echo Tests' END
				 END) AS TIPO_TRX,
				COUNT(*) AS TOTAL_TRX 
			FROM TRANSACCION T WITH(NOLOCK)
				RIGHT OUTER JOIN TIPOMENSAJEISO TM ON (TM.TIPO_MENSAJE = T.TRA_TIPO_MENSAJE_REQ AND TM.CODIGO_PROCESO = T.TRA_CODIGO_PROCESO AND T.TRA_CODIGO_RESPUESTA = '00')
			WHERE T.TRA_FECHA_INICIAL > CAST(CAST(DATEADD(hh, -5, DATEADD(mi, -1 * DATEPART(mi, GETDATE()), GETDATE())) AS VARCHAR) AS DATETIME)
			GROUP BY CONVERT(CHAR(13),T.TRA_FECHA_INICIAL,120), TM.TIPO_MENSAJE, TM.CODIGO_PROCESO
			ORDER BY 1

		--PRODUCTO CARTEASIANO CON TITULOS
		DECLARE titulos_cursor CURSOR FOR 
			SELECT DISTINCT(FECHA) FROM @TMP

		OPEN titulos_cursor

		FETCH NEXT FROM titulos_cursor 
			INTO @titulo

		WHILE @@FETCH_STATUS = 0
		BEGIN
			---TIPOS DE TRANSACCIONES FIJAS
			INSERT INTO @TMP_TIPOS VALUES(@titulo, 'Ventas', 0)
			INSERT INTO @TMP_TIPOS VALUES(@titulo, 'Consultas de Saldo', 0)
			INSERT INTO @TMP_TIPOS VALUES(@titulo, 'Echo Tests', 0)

			FETCH NEXT FROM titulos_cursor 
				INTO @titulo
		END

		CLOSE titulos_cursor
		DEALLOCATE titulos_cursor

		---SELECT FINAL
		SELECT * FROM
		(
			SELECT * FROM @TMP

			UNION ALL

			SELECT T1.*
			FROM @TMP_TIPOS T1
			LEFT JOIN @TMP T2 ON (T1.FECHA = T2.FECHA AND T1.TIPO_TRX = T2.TIPO_TRX) 
			WHERE T2.FECHA IS NULL AND T2.TIPO_TRX IS NULL
		) RES
		ORDER BY 1
	END
	ELSE IF @type = 4
	BEGIN		
		INSERT INTO @TMP
			---ESTAD�STICAS NUMERO DE TRANSACCIONES POR TIPO - ERR�NEAS [5 horas atr�s]
			SELECT CONVERT(CHAR(13),T.TRA_FECHA_INICIAL,120) + ':00' AS FECHA, 
				(CASE TM.TIPO_MENSAJE
					WHEN '0200' THEN CASE TM.CODIGO_PROCESO WHEN '380000' THEN 'Ventas' END
					WHEN '0800' THEN 
									CASE TM.CODIGO_PROCESO WHEN '000001' THEN 'Consultas de Saldo'
															WHEN '990000' THEN 'Echo Tests' END
				 END) AS TIPO_TRX,
				COUNT(*) AS TOTAL_TRX 
			FROM TRANSACCION T WITH(NOLOCK)
				RIGHT OUTER JOIN TIPOMENSAJEISO TM ON (TM.TIPO_MENSAJE = T.TRA_TIPO_MENSAJE_REQ AND TM.CODIGO_PROCESO = T.TRA_CODIGO_PROCESO AND T.TRA_CODIGO_RESPUESTA <> '00')
			WHERE T.TRA_FECHA_INICIAL > CAST(CAST(DATEADD(hh, -5, DATEADD(mi, -1 * DATEPART(mi, GETDATE()), GETDATE())) AS VARCHAR) AS DATETIME)
			GROUP BY CONVERT(CHAR(13),T.TRA_FECHA_INICIAL,120), TM.TIPO_MENSAJE, TM.CODIGO_PROCESO
			ORDER BY 1

		--PRODUCTO CARTEASIANO CON LOS TITULOS
		DECLARE titulos_cursor CURSOR FOR 
			SELECT DISTINCT(FECHA) FROM @TMP

		OPEN titulos_cursor

		FETCH NEXT FROM titulos_cursor 
			INTO @titulo

		WHILE @@FETCH_STATUS = 0
		BEGIN
			---TIPOS DE TRANSACCIONES FIJAS
			INSERT INTO @TMP_TIPOS VALUES(@titulo, 'Ventas', 0)
			INSERT INTO @TMP_TIPOS VALUES(@titulo, 'Consultas de Saldo', 0)
			INSERT INTO @TMP_TIPOS VALUES(@titulo, 'Echo Tests', 0)

			FETCH NEXT FROM titulos_cursor 
				INTO @titulo
		END

		CLOSE titulos_cursor
		DEALLOCATE titulos_cursor

		---SELECT FINAL
		SELECT * FROM
		(
			SELECT * FROM @TMP

			UNION ALL

			SELECT T1.*
			FROM @TMP_TIPOS T1
			LEFT JOIN @TMP T2 ON (T1.FECHA = T2.FECHA AND T1.TIPO_TRX = T2.TIPO_TRX) 
			WHERE T2.FECHA IS NULL AND T2.TIPO_TRX IS NULL
		) RES
		ORDER BY 1
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcConsultarReversosPendientes]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcConsultarReversosPendientes]
AS
BEGIN

	SELECT TOP 50 * FROM (

		---BUSCAR REVERSOS PARA EL MONITOR - 1. (REVERSOS H2H, QUE NO HAYAN SIDO PROCESADOS CORRECTAMENTE EN TABLA REVERSOS)
		SELECT T.tra_id, T.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T.tra_tipo_mensaje_req, T.tra_tipo_mensaje_resp, T.tra_codigo_proceso, T.tra_codigo_respuesta,
			T.tra_stan, T.tra_rrn, T.tra_codigo_estado_final, T.tra_terminal_id, T.tra_comercio_id, CAST(T.tra_valor AS NUMERIC(18,0)) AS tra_valor, T.tra_referencia_cliente_1, T.tra_referencia_cliente_2, T.tra_datos_adicionales,
			T.tra_producto_id
		FROM Transaccion T 
			INNER JOIN (SELECT DISTINCT(tra_id_original) FROM TransaccionXReverso WHERE tra_codigo_respuesta <> '00' AND tra_id_original NOT IN (SELECT tra_id_original FROM TransaccionXReverso WHERE tra_codigo_respuesta = '00' )) AS TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_codigo_respuesta='68' AND T.tra_tipo_mensaje_req='0200' AND T.tra_codigo_proceso IN ('150000') AND
			T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103)

		UNION ALL

		---BUSCAR REVERSOS PARA EL MONITOR - 2. (REVERSOS H2H, QUE NI SIQUIERA EXISTAN EN TABLA REVERSOS)
		SELECT T.tra_id, T.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T.tra_tipo_mensaje_req, T.tra_tipo_mensaje_resp, T.tra_codigo_proceso, T.tra_codigo_respuesta,
			T.tra_stan, T.tra_rrn, T.tra_codigo_estado_final, T.tra_terminal_id, T.tra_comercio_id, CAST(T.tra_valor AS NUMERIC(18,0)) AS tra_valor, T.tra_referencia_cliente_1, T.tra_referencia_cliente_2, T.tra_datos_adicionales,
			T.tra_producto_id
		FROM Transaccion T 
			LEFT OUTER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_codigo_respuesta='68' AND T.tra_tipo_mensaje_req='0200' AND T.tra_codigo_proceso IN ('150000') AND
			T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND TXR.tra_id IS NULL

		UNION ALL

		---BUSCAR REVERSOS PARA EL MONITOR - 3. (REVERSOS DESDE LOS POS, QUE NO HAYAN SIDO PROCESADOS CORRECTAMENTE EN TABLA REVERSOS)
		SELECT T.tra_id, T.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T.tra_tipo_mensaje_req, T.tra_tipo_mensaje_resp, T.tra_codigo_proceso, T.tra_codigo_respuesta,
			T.tra_stan, T.tra_rrn, T.tra_codigo_estado_final, T.tra_terminal_id, T.tra_comercio_id, CAST(T.tra_valor AS NUMERIC(18,0)) AS tra_valor, T.tra_referencia_cliente_1, T.tra_referencia_cliente_2, T.tra_datos_adicionales,
			T.tra_producto_id
		FROM Transaccion T 
			INNER JOIN (SELECT DISTINCT(tra_id_original) FROM TransaccionXReverso WHERE tra_codigo_respuesta <> '00' AND tra_id_original NOT IN (SELECT tra_id_original FROM TransaccionXReverso WHERE tra_codigo_respuesta = '00' )) AS TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_codigo_respuesta='B3' AND T.tra_tipo_mensaje_req='0420' AND T.tra_codigo_proceso IN ('150000') AND
			T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103)

		UNION ALL

		---BUSCAR REVERSOS PARA EL MONITOR - 4. (REVERSOS DESDE LOS POS, QUE NI SIQUIERA EXISTAN EN TABLA REVERSOS)
		SELECT T.tra_id, T.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T.tra_tipo_mensaje_req, T.tra_tipo_mensaje_resp, T.tra_codigo_proceso, T.tra_codigo_respuesta,
			T.tra_stan, T.tra_rrn, T.tra_codigo_estado_final, T.tra_terminal_id, T.tra_comercio_id, CAST(T.tra_valor AS NUMERIC(18,0)) AS tra_valor, T.tra_referencia_cliente_1, T.tra_referencia_cliente_2, T.tra_datos_adicionales,
			T.tra_producto_id
		FROM Transaccion T 
			LEFT OUTER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_codigo_respuesta='B3' AND T.tra_tipo_mensaje_req='0420' AND T.tra_codigo_proceso IN ('150000') AND
			T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND TXR.tra_id IS NULL

	) AS RES
	ORDER BY 1 ASC

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webGrabarCompraCupoProveedor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webGrabarCompraCupoProveedor](
	@supplierID BIGINT,
	@movementAmount MONEY,
	@movementCurrentBalance MONEY,
	@movementNewBalance MONEY,
	@movementLog INT,
	@movementAccountBank INT,
	@movementDescription VARCHAR(250)
)
AS
BEGIN

	SET NOCOUNT ON;
	
BEGIN TRANSACTION

	BEGIN TRY	
	
		INSERT INTO Compra(comp_fecha, comp_proveedor_id, comp_saldo_inicial, comp_saldo_final, comp_valor, comp_observacion, comp_cuenta_banco_id, comp_log_id)
			VALUES (GETDATE(), @supplierID, @movementCurrentBalance, @movementNewBalance, @movementAmount, @movementDescription, @movementAccountBank, @movementLog)

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webEliminarModuloWebXPerfil]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webEliminarModuloWebXPerfil](
	@profileID INT,
	@opc_id INT
)
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ELIMINAR MODULO WEB
		DELETE FROM PerfilXOpcion
		WHERE perf_id = @profileID AND opc_id = @opc_id

		--ELIMINAR OPCIONES
		DELETE FROM PerfilXOpcion
		WHERE perf_id = @profileID AND opc_id IN (SELECT opc_id FROM Opcion WHERE opc_opcion_padre_id = @opc_id)

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webAgregarModuloWebXPerfil]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webAgregarModuloWebXPerfil](
	@profileID INT,
	@webModuleID INT
)
AS
BEGIN
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		--ADICIONAR MODULO WEB
		INSERT INTO PerfilXOpcion(perf_id, opc_id)
			VALUES (@profileID, @webModuleID)

		--ADICIONAR OPCIONES
		INSERT INTO PerfilXOpcion(perf_id, opc_id)
			SELECT @profileID, opc_id FROM Opcion WHERE opc_opcion_padre_id = @webModuleID

		--ADICIONAR METODO LOAD PARA EL M�DULO WEB		
		DECLARE @functionID AS INT	
		DECLARE @TMP AS TABLE(opc_url VARCHAR(100))
		
		INSERT INTO @TMP
			SELECT opc_url FROM Opcion WHERE opc_id=@webModuleID
			UNION ALL
			SELECT opc_url FROM Opcion WHERE opc_opcion_padre_id=@webModuleID
				
		DECLARE cFunctions CURSOR FOR
			SELECT func_id FROM Funcion WHERE func_objeto_id IN (SELECT O.id FROM Objeto O INNER JOIN @TMP T ON (T.opc_url = O.nombre)) AND func_metodo_id = 1
			
		OPEN cFunctions

		FETCH cFunctions INTO @functionID

		WHILE (@@FETCH_STATUS = 0 )
		BEGIN
			IF NOT EXISTS(SELECT * FROM PerfilXFuncion WHERE perf_id = @profileID AND func_id = @functionID)
			BEGIN
				INSERT INTO PerfilXFuncion
					VALUES(@profileID,@functionID)		
			END
			
			FETCH cFunctions INTO @functionID
		END
		CLOSE cFunctions
		DEALLOCATE cFunctions		
		
		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webActivarOpcionWebXPerfil]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webActivarOpcionWebXPerfil](
	@profileID INT,
	@optionID INT,
	@active BIT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		IF @active = 1
		BEGIN 
			--INSERTAR
			IF NOT EXISTS(SELECT * FROM PerfilXOpcion WHERE perf_id = @profileID AND opc_id = @optionID)
			BEGIN
				INSERT INTO PerfilXOpcion(perf_id, opc_id)
					VALUES (@profileID, @optionID)
			END
			
		END
		ELSE
		BEGIN
			--ELIMINAR
			DELETE FROM PerfilXOpcion WHERE perf_id = @profileID AND opc_id = @optionID
		END		
		
		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcProcesarTrxFueraTiempo]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcProcesarTrxFueraTiempo](
	/*PAR�METROS DE ENTRADA*/
	@tipoMensajeResp AS VARCHAR(10),	
	@interfazOrigen AS VARCHAR(20),
	@codigoProcesamiento AS VARCHAR(10),
	@authID AS VARCHAR(6),
	@operadorAuthID AS VARCHAR(20),
	@codRespuestaB24 AS VARCHAR(2),
	@stan AS INT, 
	@rrn AS VARCHAR(20),
	@ptoVentaID AS VARCHAR(20),
	@comercioID AS VARCHAR(20), 
	@refCliente1 AS VARCHAR(50), 
	@datosAdicionales AS VARCHAR(100),
	@valor AS MONEY, 
	@IP AS VARCHAR(20), 
	@puertoTCP AS INT,
	/*PAR�METROS DE SALIDA*/
	@trxID BIGINT OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES LOCALES*/
	DECLARE @refCliente2 AS VARCHAR(50)
	DECLARE @interfazDestino AS VARCHAR(20)
	DECLARE @codigoEstadoFinal AS VARCHAR(10)
	DECLARE @productoID_BD AS SMALLINT
	DECLARE @codRespuestaISO AS VARCHAR(2)
	
	BEGIN TRANSACTION

	SET @interfazDestino = 'MIDDLEWARE'
	SET @codRespuestaISO = 'A4' --C�DIGO FIJO PARA IDENTIFICAR TRANSACCI�N RECIBIDA FUERA DE TIEMPO

	BEGIN TRY

		/*VENTA DE RECARGAS A CELULAR*/
		IF (@tipoMensajeResp = '0210' AND @codigoProcesamiento = '380000')
		BEGIN 
			SELECT @productoID_BD = NULL, @codigoEstadoFinal = 'MOV0000A4'
		END
		/*RECAUDO DE FACTURAS*/
		ELSE IF (@tipoMensajeResp = '0210' AND @codigoProcesamiento = '150000')		
		BEGIN

			SET @productoID_BD = 200	/*C�DIGO FIJO PARA MOVILRED RECAUDO DE FACTURAS*/
			SET @codigoEstadoFinal = 'MOV1000A4'

			--SEPARAR CAMPOS DEL C�DIGO DE BARRAS
			SELECT @refCliente1=referencia1, @refCliente2=referencia2 FROM dbo.[SepararCamposCodigoBarras](@datosAdicionales, @productoID_BD)

		END			
		
		/*REGISTRAR TRANSACCI�N FUERA DE TIEMPO*/
		INSERT INTO Transaccion(tra_fecha_inicial, tra_tipo_mensaje_resp, tra_interfaz_origen, tra_interfaz_destino, tra_codigo_respuesta, tra_codigo_respuestaB24, tra_codigo_proceso, tra_stan, tra_rrn, 
								tra_autorizacion_ope_id, tra_auth_id_p38, tra_codigo_estado_final, tra_terminal_id, tra_comercio_id,tra_referencia_cliente_1, tra_referencia_cliente_2, tra_datos_adicionales, 
								tra_valor, tra_ip_cliente, tra_puerto_atencion)	
		VALUES (GETDATE(), @tipoMensajeResp, @interfazOrigen, @interfazDestino, @codRespuestaISO, @codRespuestaB24, @codigoProcesamiento, @stan, @rrn, 
				@operadorAuthID, @authID, @codigoEstadoFinal, @ptoVentaID, LTRIM(RTRIM(@comercioID)), @refCliente1, @refCliente2, @datosAdicionales, @valor, @IP, @puertoTCP )

		SELECT @trxID = SCOPE_IDENTITY()

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarDatosComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarDatosComercio](
	@merchantID VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT ent_tipo_nivel_entidad_id, 
			CAST(ent_comision_global AS NUMERIC(4,2)) AS ent_comision_global, 
			ent_estado_entidad_id, ent_saldo, 
			ent_fecha_activacion,
			ent_maneja_saldo 
	FROM Entidad WHERE ent_comercio_JNTS = @merchantID

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarComerciosParaReporte]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarComerciosParaReporte]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT '-1' AS IDCOMERCIO, 'Todos los Comercios' AS Descripcion
	
	UNION ALL
	
	SELECT 
		C.IDCOMERCIO, SUBSTRING(CAST(C.IDCOMERCIO AS VARCHAR(20)) + '-' + C.NOMBRE + '-' + C.RAZONSOCIAL, 0, 65) AS Descripcion
	FROM Entidad E
		INNER JOIN assenda.dbo.COMERCIOS C ON (C.IDCOMERCIO = E.ent_comercio_JNTS COLLATE SQL_Latin1_General_CP1_CI_AS)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarComerciosParaActivar]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarComerciosParaActivar]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT '-1' AS IDCOMERCIO, '                ' AS Descripcion
	
	UNION ALL
	
	SELECT C.IDCOMERCIO, SUBSTRING(CAST(C.IDCOMERCIO AS VARCHAR(20)) + '-' + C.NOMBRE + '-' + C.RAZONSOCIAL, 0, 65) AS Descripcion
	FROM assenda.dbo.COMERCIOS C
		LEFT OUTER JOIN Entidad E ON (C.IDCOMERCIO = E.ent_comercio_JNTS COLLATE SQL_Latin1_General_CP1_CI_AS)
	WHERE C.ESTADO = 0 AND E.ent_id IS NULL

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarComercios]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarComercios](
	@filterMerchant VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT E.ent_comercio_JNTS, 
			TNE.nombre AS tipo, 
			CASE WHEN E.ent_maneja_saldo = 1 THEN 'S�' ELSE 'No' END AS ent_maneja_saldo,
			EE.nombre AS estado, 
			CAST(E.ent_comision_global AS NUMERIC(4,2)) AS ent_comision_global, 
			E.ent_saldo, 
			CONVERT(VARCHAR, E.ent_fecha_activacion,103) AS fecha_activacion,
			E.ent_tipo_nivel_entidad_id 
	FROM Entidad E
		INNER JOIN TipoNivelEntidad TNE ON (TNE.id = E.ent_tipo_nivel_entidad_id)
		INNER JOIN EstadoEntidad EE ON (EE.id = E.ent_estado_entidad_id)
	WHERE E.ent_id > 0 AND (E.ent_comercio_JNTS LIKE '%' + @filterMerchant + '%' OR @filterMerchant = '-1')
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcPreProcesarReverso]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcPreProcesarReverso](
	/*PAR�METROS DE ENTRADA*/
	@tipoMensajeReq AS VARCHAR(10),	
	@interfazOrigen AS VARCHAR(12),
	@codigoProcesamiento AS VARCHAR(10),
	@stan AS INT, 
	@rrn AS VARCHAR(20),
	@ptoVentaID AS VARCHAR(20),
	@comercioID AS VARCHAR(20), 
	@productoCodigo AS VARCHAR(20), 
	@refCliente1 AS VARCHAR(50), 
	@refCliente2 AS VARCHAR(50), 
	@datosAdicionales AS VARCHAR(100),
	@valor AS MONEY, 
	@IP AS VARCHAR(20), 
	@puertoTCP AS INT,
	@trxIDOriginal AS BIGINT,
	/*PAR�METROS DE SALIDA*/
	@trxID AS BIGINT OUTPUT
)
AS
BEGIN

	/* VARIABLES LOCALES */
	DECLARE @interfazDestino AS VARCHAR(50)


	BEGIN TRANSACTION

	BEGIN TRY

		/*OBTENER COD. DE PRODUCTO E ISOMUX_NAME SEG�N EAN*/
		SELECT 
			@interfazDestino = PR.prov_isomux_nombre
		FROM Matriz_NivelEntidadXProducto MNEP
			INNER JOIN Producto P ON (MNEP.producto_id = P.pro_id)
			INNER JOIN Proveedor PR ON (P.pro_proveedor_id = PR.prov_id)
			INNER JOIN TipoNivelEntidad TNE ON (MNEP.tipo_nivel_entidad_id = TNE.id)
			INNER JOIN Entidad E ON (E.ent_tipo_nivel_entidad_id = TNE.id)
		WHERE
			E.ent_comercio_JNTS = LTRIM(RTRIM(@comercioID)) AND P.pro_id = LTRIM(RTRIM(@productoCodigo))		
			
		/*REGISTRAR TRANSACCI�N X REVERSO*/
		INSERT INTO TransaccionXReverso(tra_fecha_inicial, tra_tipo_mensaje_req, tra_interfaz_origen, tra_interfaz_destino, tra_codigo_proceso, tra_stan, tra_rrn, tra_terminal_id, tra_comercio_id,
								tra_valor, tra_referencia_cliente_1, tra_referencia_cliente_2, tra_datos_adicionales, tra_producto_id, tra_ip_destino, tra_puerto_atencion_destino, tra_id_original)	
		VALUES (GETDATE(), @tipoMensajeReq, @interfazOrigen, @interfazDestino, @codigoProcesamiento, @stan, @rrn, @ptoVentaID, LTRIM(RTRIM(@comercioID)),
				@valor, @refCliente1, @refCliente2, @datosAdicionales, @productoCodigo, @IP, @puertoTCP, @trxIDOriginal )

		SELECT @trxID = SCOPE_IDENTITY()

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webActualizarComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webActualizarComercio](
	@merchantID VARCHAR(20),
	@entityType INT,
	@merchantAdminLocalBalance AS BIT,
	@merchantPctGain NUMERIC(4,2),
	@merchantStatus INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		DECLARE @newPctGain AS NUMERIC(4,2)
		
		IF @merchantAdminLocalBalance = 1
			SET @newPctGain = @merchantPctGain
		ELSE
			SET @newPctGain = 0
	
		UPDATE Entidad
		SET 
			ent_tipo_nivel_entidad_id = @entityType, 
			ent_maneja_saldo = @merchantAdminLocalBalance,
			ent_comision_global = @newPctGain, 
			ent_estado_entidad_id = @merchantStatus
		WHERE
			ent_comercio_JNTS = @merchantID

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webObtenerSaldoComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webObtenerSaldoComercio](
	@merchantID VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;
		
	SELECT ent_saldo FROM Entidad WHERE ent_comercio_JNTS= @merchantID

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webValidarMovimientoSaldoComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarMovimientoSaldoComercio](
	@merchantID VARCHAR(20),
	@merchantMovementType INT,
	@merchantMovementAmount MONEY
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @currentBalance AS MONEY
	DECLARE @gainAmount AS MONEY
	DECLARE @pctGain AS NUMERIC(4,2)
	
	SELECT @currentBalance=ent_saldo, @pctGain=ent_comision_global FROM Entidad WHERE ent_comercio_JNTS= @merchantID	

	IF @merchantMovementType = 2	/*CON COMISI�N*/
		SET @gainAmount = (@merchantMovementAmount * (@pctGain/100))
	ELSE IF @merchantMovementType = 4	/*SIN COMISI�N*/
		SET @gainAmount = 0

	IF 	(@currentBalance - @merchantMovementAmount - @gainAmount) >= 0
		SELECT 1 /*PERMITIR*/
	ELSE
		SELECT 0 /*NO PERMITIR, SIN SALDO*/

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webValidarLoginName]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarLoginName](
	@UserID AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT COUNT(*) AS LOGINCOUNTER FROM Usuario WHERE usu_login = @UserID
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webValidarLogin]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarLogin](
	@userID VARCHAR(150),
	@passwordID VARCHAR(150),
	@IP VARCHAR(20),
	@sessionID VARCHAR(50),
	@webEntryType INT	/*1) Administraci�n Transaccional(PHP-Admin) , 2) Usuarios WEB (ASP .Net)*/
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @loginID AS INT
	DECLARE @loginType AS INT
	DECLARE @userID_DB AS INT
	DECLARE @userIDInfo_DB AS INT
	DECLARE @userType AS INT
	DECLARE @userName AS VARCHAR(150)
	DECLARE @entityName AS VARCHAR(150)
	DECLARE @expDatetime AS DATETIME
	DECLARE @errorCounter AS INT
	DECLARE @userStatus AS INT

	IF EXISTS(SELECT * FROM Usuario U WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID)
	BEGIN
		--Nombre de Usuario Existe

		--Validar si existen Sesiones Activas de ese usuario para la fecha actual
		IF NOT EXISTS(SELECT * FROM LoginInfo 
						WHERE login_usuario_id = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID) AND CONVERT(VARCHAR, login_fecha_inicio, 103) = CONVERT(VARCHAR, GETDATE(), 103) AND
								login_tipo_id = 1 AND login_fecha_fin IS NULL
			)
		BEGIN
			--Validar Password
			IF EXISTS(SELECT * FROM Usuario U WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID AND U.usu_hash = @passwordID)
			BEGIN
				--Clave Correcta
				
				IF @webEntryType = 1	/*Administraci�n Transaccional(PHP-Admin)*/
				BEGIN
					SELECT @userID_DB = U.usu_id, @userType = U.usu_perfil_id, @userName = U.usu_nombre, @entityName = P.perf_nombre, @expDatetime = U.usu_fecha_exp_clave, @userStatus = U.usu_estado_usuario_id
					FROM Usuario U
						INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
					WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID AND U.usu_hash = @passwordID AND U.usu_perfil_id = 6			
				END
				ELSE IF @webEntryType = 2	/*Usuarios WEB (ASP .Net)*/
				BEGIN
					SELECT @userID_DB = U.usu_id, @userType = U.usu_perfil_id, @userName = U.usu_nombre, @entityName = P.perf_nombre, @expDatetime = U.usu_fecha_exp_clave, @userStatus = U.usu_estado_usuario_id
					FROM Usuario U
						INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
					WHERE U.usu_login COLLATE Latin1_General_CS_AS = @userID AND U.usu_hash = @passwordID AND U.usu_perfil_id <> 6			
				END

				IF NOT @userID_DB IS NULL
				BEGIN
					--VALIDAR ESTADO DE USUARIO
					IF @userStatus = 1
					BEGIN
						--ACTIVADO
						SELECT @loginType = 1, @userIDInfo_DB = @userID_DB				
					END
					ELSE
					BEGIN
						--DESACTIVADO
						SELECT @loginType = 6, @userIDInfo_DB = @userID_DB
					END
				END
				ELSE
				BEGIN
					SELECT @loginType = 5, @userID_DB = -1, @userIDInfo_DB = -1, @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE()
				END
			
			END
			ELSE
			BEGIN
			
				--VALIDAR ESTADO
				SELECT @userStatus = U.usu_estado_usuario_id
				FROM Usuario U
				WHERE U.usu_login = @userID
			
				IF @userStatus = 1
				BEGIN
					--CLAVE ERR�NEA
					SELECT @loginType = 3, @userID_DB = -1, @userIDInfo_DB = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login = @userID), @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE()
				END
				ELSE
				BEGIN
					--DESACTIVADO
					SELECT @loginType = 6, @userID_DB = -1, @userIDInfo_DB = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login = @userID), @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE()	
				END		
			END		
		END
		ELSE
		BEGIN
			--Sesiones Activas Encontradas
			SELECT @loginType = 4, @userID_DB = -1, @userIDInfo_DB = (SELECT U.usu_id FROM Usuario U WHERE U.usu_login = @userID), @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE()			
		END
	END
	ELSE
	BEGIN
		SELECT @loginType = 2, @userID_DB = -1, @userIDInfo_DB = -1, @userType=-1, @userName = '', @entityName = '', @expDatetime = GETDATE()
	END

	--Datos de la Sesion
	INSERT INTO LoginInfo(login_fecha_inicio, login_tipo_id, login_usuario_id, login_ip, login_session_id)
		VALUES (GETDATE(), @loginType, @userIDInfo_DB, @IP, @sessionID)					

	SELECT @loginID = SCOPE_IDENTITY()

	--Validar Nro. de Intentos Erroneos, Si Mayor a 3 Desactivar Usuario
	IF @loginType = 3
	BEGIN
		SELECT @errorCounter = COUNT(*) FROM LoginInfo WHERE login_tipo_id = 3 AND login_usuario_id = @userIDInfo_DB AND login_fecha_inicio BETWEEN DATEADD(MINUTE,-30,GETDATE()) AND GETDATE()
		
		IF @errorCounter >= 3
		BEGIN
			UPDATE Usuario
			SET usu_estado_usuario_id = 2
			WHERE usu_id = @userIDInfo_DB
		END
		
	END 

	--Select Final
	SELECT @loginID AS LOGINID, @userIDInfo_DB AS USERID, @userType AS USERTYPE, @userName AS USERNAME, @entityName AS ENTITYNAME, @expDatetime AS EXPPASSWORDDATETIME, @loginType AS ERRORCODE

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webGrabarDatosUsuario]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webGrabarDatosUsuario](
	@userID INT, /*User ID Logeado*/
	@userName VARCHAR(200),
	@userEmail VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	UPDATE Usuario
	SET usu_nombre = @userName, usu_email = @userEmail
	WHERE usu_id = @userID	

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webValidarSoloAccesoObjetoPorMetodo]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarSoloAccesoObjetoPorMetodo](
	@UserID AS INT,
	@Object AS VARCHAR(100),
	@Method AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	IF EXISTS(SELECT *
				FROM Usuario U
					INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
					INNER JOIN PerfilXFuncion PXF ON (P.perf_id = PXF.perf_id)
					INNER JOIN Funcion F ON (F.func_id = PXF.func_id)
					INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
					INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
				WHERE 
					U.usu_id = @UserID AND
					O.nombre = @Object AND
					M.nombre = @Method)
		
		SELECT 1
	
	ELSE
	
		SELECT 0
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webAjustarSaldoComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webAjustarSaldoComercio](
	/*PAR�METROS DE ENTRADA*/
	@merchantID AS VARCHAR(20),
	@amount AS MONEY,
	/*PAR�METROS DE SALIDA*/
	@newBalance AS MONEY OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES*/
	DECLARE @ret_val MONEY
	DECLARE @intentos TINYINT

	--MODIFICAR EL SALDO ACTUAL CON EL VALOR ENVIADO

	/*PARA CAPTURAR INTERBLOQUEOS Y PROCESARLOS - TRANSACCIONALIDAD SIMULT�NEA SOBRE EL MISMO COMERCIO*/
	SET @intentos = 1
	WHILE @intentos <= 5
	BEGIN
	  BEGIN TRANSACTION
	  BEGIN TRY
		UPDATE ENTIDAD 
			SET ENT_SALDO = (ENT_SALDO + @amount)
			WHERE ENT_COMERCIO_JNTS = @merchantID
		COMMIT
		BREAK
	  END TRY
	  BEGIN CATCH
		ROLLBACK
		SET @intentos = @intentos + 1
		CONTINUE
	  END CATCH;			
	END

	--CONSULTAR SALDO ACTUAL
	SELECT @newBalance = DBO.[obtenerSaldo](@merchantID, 0 /*NO TRANSACCIONAL*/)

	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webAgregarTerminalesXComercioJNTS]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webAgregarTerminalesXComercioJNTS](
	@merchantID VARCHAR(20)
)
AS
BEGIN

	DECLARE @merchantINT AS INT

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	

		SELECT @merchantINT = ent_id FROM Entidad WHERE ent_comercio_JNTS = @merchantID

		COMMIT TRANSACTION

		--Traer Terminales del JNTS Asociadas al Comercio, Insertarlas en BD Conred
		INSERT INTO PuntoVenta(pto_numero, pto_tipo_punto_venta_id, pto_numero_serie, pto_estado_punto_venta_id, pto_observacion, pto_version_software, pto_tipo_comunicacion_id, pto_modelo, pto_imei) 
			SELECT TERMINAL_ID, 3/*POS*/, '-', (CASE WHEN ACTIVO=1 THEN 1 ELSE 0 END) AS ACTIVO, DESCRIPCION, '-', 1/*GPRS*/, '-', '-'
			FROM assenda.dbo.TERMINALES_ASSENDA 
			WHERE COMERCIO_ID = @merchantID
				AND TERMINAL_ID COLLATE SQL_Latin1_General_CP1_CI_AS NOT IN (SELECT pto_numero FROM PuntoVenta PV INNER JOIN PuntoVentaXEntidad PVE ON (PV.pto_id = PVE.pto_id AND PVE.ent_id = @merchantINT))
		
		--Asociar las terminales al Comercio en BD Conred
		INSERT INTO PuntoVentaXEntidad(pto_id, ent_id, numero_asignacion, fecha_inicial)
			SELECT pto_id, @merchantINT, 1/*Asignaci�n 1*/, GETDATE() FROM PuntoVenta WHERE pto_numero COLLATE SQL_Latin1_General_CP1_CI_AS IN (
				SELECT TERMINAL_ID
				FROM assenda.dbo.TERMINALES_ASSENDA 
				WHERE COMERCIO_ID = @merchantID	
					AND TERMINAL_ID COLLATE SQL_Latin1_General_CP1_CI_AS NOT IN (SELECT pto_numero FROM PuntoVenta PV INNER JOIN PuntoVentaXEntidad PVE ON (PV.pto_id = PVE.pto_id AND PVE.ent_id = @merchantINT))
			)

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webActivarComercioJNTS]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webActivarComercioJNTS](
	@merchantID VARCHAR(20),
	@merchantType BIGINT,
	@merchantAdminLocalBalance BIT,
	@merchantPctGain NUMERIC(4,2)
)
AS
BEGIN

	DECLARE @merchantINT AS INT

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	

		INSERT INTO Entidad
		VALUES (@merchantID, @merchantType, 1/*Estado*/, @merchantAdminLocalBalance, @merchantPctGain, 1/*Fijo*/,0/*Saldo Inicial*/, NULL, 1/*Rubro*/, GETDATE())

		SET @merchantINT = SCOPE_IDENTITY()

		COMMIT TRANSACTION

		--Traer Terminales del JNTS Asociadas al Comercio, Insertarlas en BD Conred
		INSERT INTO PuntoVenta(pto_numero, pto_tipo_punto_venta_id, pto_numero_serie, pto_estado_punto_venta_id, pto_observacion, pto_version_software, pto_tipo_comunicacion_id, pto_modelo, pto_imei) 
			SELECT TERMINAL_ID, 3/*POS*/, '-', (CASE WHEN ACTIVO=1 THEN 1 ELSE 0 END) AS ACTIVO, DESCRIPCION, '-', 1/*GPRS*/, '-', '-'
			FROM assenda.dbo.TERMINALES_ASSENDA 
			WHERE COMERCIO_ID = @merchantID
		
		--Asociar las terminales al Comercio en BD Conred
		INSERT INTO PuntoVentaXEntidad(pto_id, ent_id, numero_asignacion, fecha_inicial)
			SELECT pto_id, @merchantINT, 1/*Asignaci�n 1*/, GETDATE() FROM PuntoVenta WHERE pto_numero COLLATE SQL_Latin1_General_CP1_CI_AS IN (
				SELECT TERMINAL_ID
				FROM assenda.dbo.TERMINALES_ASSENDA 
				WHERE COMERCIO_ID = @merchantID	
			)

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcProcesarInicializacion]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcProcesarInicializacion](
	/*PAR�METROS DE ENTRADA*/
	@tipoMensajeReq AS VARCHAR(10),
	@tipoMensajeResp AS VARCHAR(10),
	@codigoProcesamientoReq AS VARCHAR(10),
	@stan AS INT, 
	@ptoVentaID AS VARCHAR(20),
	@IP AS VARCHAR(20), 
	@puertoTCP AS INT,
	/*PAR�METROS DE SALIDA*/
	@trxID AS BIGINT OUTPUT,
	@codRespuestaISO VARCHAR(2) OUTPUT,
	@codigoProcesamientoResp AS VARCHAR(10) OUTPUT,
	@tablaIDResp AS VARCHAR(2) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES LOCALES*/
	DECLARE @validacion_local AS SMALLINT
	DECLARE @codRespuesta AS VARCHAR(10)
	DECLARE @interfazOrigen AS VARCHAR(20)
	DECLARE @interfazDestino AS VARCHAR(20)
	DECLARE @comercioID AS VARCHAR(20)
	DECLARE @tablaIndexReg AS SMALLINT
	DECLARE @tablaTotalReg AS SMALLINT
	--VARIABLE TIPO TABLA PARA RETORNAR DATOS DE CONFIGURACI�N
	DECLARE @tablaConf AS TABLE(F1 VARCHAR(30), F2 VARCHAR(30), F3 VARCHAR(30), F4 VARCHAR(30), F5 VARCHAR(30), F6 VARCHAR(30),
								F7 VARCHAR(30), F8 VARCHAR(30), F9 VARCHAR(30), F10 VARCHAR(30), F11 VARCHAR(30), F12 VARCHAR(30))
	
	SET NOCOUNT ON	
	
	BEGIN TRANSACTION

	SELECT @interfazOrigen = 'CLIENTE', @interfazDestino = 'MIDDLEWARE'

	BEGIN TRY
		/*INICIO VALIDACIONES DEL NEGOCIO*/

		--VALIDAR SI EXISTE UN COMERCIO ASOCIADO A LA TERMINAL
		SELECT @comercioID = E.ent_comercio_JNTS
		FROM Entidad E
			INNER JOIN PuntoVentaXEntidad PVE ON (E.ent_id = PVE.ent_id)
			INNER JOIN PuntoVenta PV ON (PVE.pto_id = PV.pto_id)
		WHERE PV.pto_numero = @ptoVentaID

		IF NOT EXISTS(SELECT * FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID)		--EXISTE PUNTO DE VENTA?

			SELECT @codRespuesta = 'JTS000002', @validacion_local = 0, @codRespuestaISO = 'N1'

		ELSE IF NOT EXISTS(SELECT * FROM PuntoVentaXEntidad WHERE PTO_ID = (SELECT PTO_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) AND ENT_ID = (SELECT ent_id FROM ENTIDAD WHERE ent_comercio_JNTS = @comercioID) AND FECHA_FINAL IS NULL)	--PUNTO DE VENTA RELACIONADO AL COMERCIO

			SELECT @codRespuesta = 'JTS000003', @validacion_local = 0, @codRespuestaISO = 'N2'

		ELSE IF (SELECT ENT_ESTADO_ENTIDAD_ID FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID))) > 1	--COMERCIO ACTIVO?

			SELECT @codRespuesta = 'JTS000004', @validacion_local = 0, @codRespuestaISO = 'N3'

		ELSE IF (SELECT PTO_ESTADO_PUNTO_VENTA_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) > 1	--PUNTO DE VENTA ACTIVO?

			SELECT @codRespuesta = 'JTS000005', @validacion_local = 0, @codRespuestaISO = 'N4'
		ELSE
			--TODO OK, CONTINUAR CON TRANSACCI�N
			SELECT @codRespuesta = 'JTS000000', @validacion_local = 1, @codRespuestaISO = '00'

		/*FIN VALIDACIONES DEL NEGOCIO*/

		IF @validacion_local = 1
		BEGIN
			/*	REGLAS DE NEGOCIO CUMPLIDAS, @validacion_local = 1	*/

			IF @codigoProcesamientoReq = '930000'
			BEGIN 			
				--PRIMERA TABLA DE INICIALIZACION = CONFIGURACI�N
				DECLARE @TICKET_DATA AS TABLE(T1 VARCHAR(2))
				DECLARE @COMERCIO_DATA AS TABLE(C1 VARCHAR(30), C2 VARCHAR(30), C3 VARCHAR(30))
				DECLARE @CONEXION_DATA AS TABLE(CO1 VARCHAR(30), CO2 VARCHAR(30), CO3 VARCHAR(30))
				DECLARE @PARAMS_DATA AS TABLE(PAR1 VARCHAR(30), PAR2 VARCHAR(30), PAR3 VARCHAR(30), PAR4 VARCHAR(30), PAR5 VARCHAR(30))
				
				--VARIABLES DE SALIDA
				SELECT @tablaIDResp = '01', @tablaIndexReg = 1, @tablaTotalReg = 1, @codigoProcesamientoResp = CAST( (CAST(@codigoProcesamientoReq AS INT) + 1) AS VARCHAR)

				--C�DIGO DE TICKET				
				INSERT @TICKET_DATA
					SELECT CASE WHEN ent_tipo_nivel_entidad_id <> 3 THEN '1' ELSE '2' END FROM Entidad WHERE ent_comercio_JNTS = @comercioID
	
				---DATOS DEL COMERCIO
				INSERT @COMERCIO_DATA
					SELECT 
						RIGHT(REPLICATE('0',10) + CAST(C.IDCOMERCIO AS VARCHAR(10)),10) AS IDCOMERCIO,
						LEFT(CAST(C.NOMBRE AS VARCHAR(23)) + REPLICATE(' ', 23),23) AS NOMBRE,
						LEFT(CAST(C.DIRECCION AS VARCHAR(23)) + REPLICATE(' ', 23),23) AS DIRECCION
					FROM assenda.dbo.COMERCIOS C
						INNER JOIN assenda.dbo.TERMINALES_ASSENDA TA ON (C.IDCOMERCIO = TA.COMERCIO_ID)
						INNER JOIN Conred.dbo.Entidad E ON (C.IDCOMERCIO COLLATE SQL_Latin1_General_CP1_CI_AS = E.ent_comercio_JNTS)
					WHERE TA.TERMINAL_ID = @ptoVentaID
					
				---DATOS DE CONECTIVIDAD
				INSERT @CONEXION_DATA
					SELECT
						A.TEL_PRIMARIO, 
						A.TEL_SECUNDARIO,
						A.IP_PRIMARIA
					FROM assenda.dbo.TERMINALES_ASSENDA TA
					INNER JOIN assenda.dbo.TIPOS_TERMINALES TT ON (TA.TIPO_TERMINAL_ID = TT.ID)
					INNER JOIN assenda.dbo.ADQUIRENTE A ON (TT.ADQUIRENTE_ID = A.ID_ADQUIRENTE)
					WHERE TA.TERMINAL_ID = @ptoVentaID
					
				---OTROS DATOS PARAMETRIZABLES POR CONRED		
				INSERT INTO @PARAMS_DATA
					SELECT
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'LocalPort') AS PUERTO,
						(SELECT CAST((CAST(CS.dato AS INT) / 1000 ) AS VARCHAR(10)) FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'TotalTimeoutJTranServer') AS TIMEOUT_RECARGAS,
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'maxAttempsConnection') AS INTENTOS_CONEXION,
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'minSaleAmount') AS VALOR_VENTA_MINIMO,
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'maxSaleAmount') AS VALOR_VENTA_MAXIMO
				
				---RESULTADO FINAL PARA LA INICIALIZACI�N
				INSERT INTO @tablaConf
					SELECT 
					(SELECT T1 FROM @TICKET_DATA),
					(SELECT C1 FROM @COMERCIO_DATA),
					(SELECT C2 FROM @COMERCIO_DATA),
					(SELECT C3 FROM @COMERCIO_DATA),
					(SELECT CO1 FROM @CONEXION_DATA),
					(SELECT CO2 FROM @CONEXION_DATA),
					(SELECT CO3 FROM @CONEXION_DATA),
					(SELECT PAR1 FROM @PARAMS_DATA),
					(SELECT PAR2 FROM @PARAMS_DATA),
					(SELECT PAR3 FROM @PARAMS_DATA),
					(SELECT PAR4 FROM @PARAMS_DATA),
					(SELECT PAR5 FROM @PARAMS_DATA)
				
				SELECT * FROM @tablaConf
				
			END
			ELSE IF @codigoProcesamientoReq = '930001'
			BEGIN
				--ENVIAR DATOS DE LA SIGUIENTE TABLA DE INICIALIZACION DE ACUERDO A LA �LTIMA TABLE_ID ENVIADA
				DECLARE @LastTableID AS VARCHAR(2)
				DECLARE @LastRecIndex AS SMALLINT
				DECLARE @LastTablaTotalRec AS SMALLINT
				DECLARE @NextRecIndex AS SMALLINT
				DECLARE @MaxNextIndexXTable AS SMALLINT
				DECLARE @NextTableID AS VARCHAR(2)

				SELECT TOP 1
					@LastTableID		= TI.tra_tabla_inicializacion_id,
					@LastRecIndex		= TI.tra_index_registro_enviado,
					@LastTablaTotalRec	= TI.tra_max_registros_tabla					
				FROM TransaccionInicializacion TI
				WHERE TI.tra_terminal_id = @ptoVentaID AND TI.tra_cod_respuesta = '00' AND TI.tra_tabla_inicializacion_id NOT IN ('ZZ', 'XX')
				ORDER BY TI.tra_fecha DESC

				--VERIFICAR SI HACEN FALTA REGISTROS A ENVIAR POR LA �LTIMA TABLA
				IF @LastRecIndex >= @LastTablaTotalRec
				BEGIN
					--�LTIMA TABLA FINALIZADA, CONTINUAR CON LA PR�XIMA
					--CALCULAR SIGUIENTE TABLA A ENVIAR CON BASE EN LA ANTERIOR
					SET @NextTableID = RIGHT(REPLICATE('0',2) + CAST((CAST(@LastTableID AS SMALLINT) + 1) AS VARCHAR(2)),2)
					
					--INICIAR DESDE EL PRIMER REGISTRO
					SET @NextRecIndex = 1
				END
				ELSE
				BEGIN
					--CONTINUAR ENVIANDO REGISTROS DE LA �LTIMA TABLA
					SET @NextTableID = @LastTableID
					
					--CONTINUAR CON EL �LTIMO REGISTRO ENVIADO M�S 1
					SET @NextRecIndex = @LastRecIndex + 1				
				END

				--VALIDAR SI EXISTE LA TABLA A ENVIAR
				IF EXISTS(SELECT * FROM TablaInicializacion WHERE tab_id = @NextTableID)
				BEGIN
				
					--OBTENER EL M�XIMO DE REPETICIONES DE LA TABLA A ENVIAR
					SELECT @MaxNextIndexXTable = tab_max_repeticion FROM TablaInicializacion WHERE tab_id = @NextTableID				
				
					IF @NextTableID = '02'
					BEGIN
						--PRODUCTOS DE RECARGA
						
						--ESTABLECER EL TOTAL DE REGISTROS DE LA TABLA
						SELECT @tablaTotalReg = COUNT(*)
						FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 1/*SOLO RECARGAS*/ 
																				
						--ESTABLECER EL �LTIMO ID DE REGISTRO ENVIADO
						SELECT @tablaIndexReg = MAX(Row) FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 1/*SOLO RECARGAS*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						IF @tablaIndexReg IS NULL
							SET @tablaIndexReg = -1
												
						--SELECT FINAL PRODUCTOS DE RECARGA
						SELECT * FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 1/*SOLO RECARGAS*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						--VALIDAR FIN DE ENV�O REGISTROS, VERSION 1 SOLO RECARGAS
						IF @tablaIndexReg < @tablaTotalReg
							SET @codigoProcesamientoResp = '930001'	--FALTAN M�S REGISTROS
						ELSE
							SET @codigoProcesamientoResp = '930000'	--TERMIN� EL PROCESO						
						
						/*
						--VALIDAR FIN DE ENV�O REGISTROS
						IF @tablaIndexReg < @tablaTotalReg
							SET @codigoProcesamientoResp = '930001'	--FALTAN M�S REGISTROS
						ELSE
							SET @codigoProcesamientoResp = '930001'	--TERMIN� EL PROCESO, CONTINUAR TABLA 03
						*/
					END
					ELSE IF @NextTableID = '03'
					BEGIN
						--PRODUCTOS DE PSP
						
						--ESTABLECER EL TOTAL DE REGISTROS DE LA TABLA
						SELECT @tablaTotalReg = COUNT(*)
						FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 2/*SOLO PSP*/ 
																				
						--ESTABLECER EL �LTIMO ID DE REGISTRO ENVIADO
						SELECT @tablaIndexReg = MAX(Row) FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 2/*SOLO PSP*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						IF @tablaIndexReg IS NULL
							SET @tablaIndexReg = -1
												
						--SELECT FINAL PRODUCTOS DE RECARGA
						SELECT * FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 2/*SOLO PSP*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						--VALIDAR FIN DE ENV�O REGISTROS
						IF @tablaIndexReg < @tablaTotalReg
							SET @codigoProcesamientoResp = '930001'	--FALTAN M�S REGISTROS
						ELSE
							SET @codigoProcesamientoResp = '930000'	--TERMIN� EL PROCESO

					END
					ELSE IF @NextTableID = '03'
					BEGIN
						--PRODUCTOS DE GIROS
						SELECT 3
					END	
					
					--VARIABLES DE SALIDA
					SELECT @tablaIDResp = @NextTableID
									
				END
				ELSE 
				BEGIN
				
					SELECT '', '', '', '', '', '', '', '', '', '', '', ''
				
					--SIGUIENTE TABLA NO EXISTE, FIN DEL PROCESO
					--VARIABLES DE SALIDA
					SELECT @tablaIDResp = 'ZZ', @tablaIndexReg = 0, @codigoProcesamientoResp = '930000'					
				END
			END
			ELSE
			BEGIN
				SELECT '', '', '', '', '', '', '', '', '', '', '', ''
				
				--ERROR DE PROCESSING CODE
				SELECT @tablaIDResp = 'XX', @tablaIndexReg = 0, @codigoProcesamientoResp = @codigoProcesamientoReq
			END
		END
		ELSE
		BEGIN
		
			SELECT '', '', '', '', '', '', '', '', '', '', '', ''
			
			--REGLAS DE NEGOCIO NO CUMPLIDAS
			SELECT @tablaIDResp = '01', @tablaIndexReg = 0, @tablaTotalReg = 1, @codigoProcesamientoResp = CAST( (CAST(@codigoProcesamientoReq AS INT) + 1) AS VARCHAR)		
		END

		/*REGISTRAR TRANSACCI�N*/
		INSERT INTO TransaccionInicializacion(tra_fecha, tra_interfaz_origen, tra_interfaz_destino, tra_tipo_mensaje_req, tra_tipo_mensaje_resp, tra_codigo_proceso_req, tra_codigo_proceso_resp, tra_stan, tra_terminal_id, tra_cod_respuesta, 
								tra_codigo_estado_final, tra_tabla_inicializacion_id, tra_index_registro_enviado, tra_max_registros_tabla, tra_ip_cliente, tra_puerto_atencion)	
		VALUES (GETDATE(), @interfazOrigen, @interfazDestino, @tipoMensajeReq, @tipoMensajeResp, @codigoProcesamientoReq, @codigoProcesamientoResp, @stan, @ptoVentaID, @codRespuestaISO, @codRespuesta,
				@tablaIDResp, @tablaIndexReg, @tablaTotalReg, @IP, @puertoTCP )

		SELECT @trxID = SCOPE_IDENTITY()

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1, @codRespuesta = 'JTS930000'

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcDisminuirSaldo]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcDisminuirSaldo](
	/*PAR�METROS DE ENTRADA*/
	@comercioID AS VARCHAR(20),
	@valor AS MONEY,
	/*PAR�METROS DE SALIDA*/
	@nuevoSaldo AS MONEY OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES*/
	DECLARE @ret_val MONEY
	DECLARE @intentos TINYINT

	--RESTAR AL SALDO ACTUAL EL VALOR DE LA VENTA

	/*PARA CAPTURAR INTERBLOQUEOS Y PROCESARLOS - TRANSACCIONALIDAD SIMULT�NEA SOBRE EL MISMO COMERCIO*/
	SET @intentos = 1
	WHILE @intentos <= 5
	BEGIN
	  BEGIN TRANSACTION
	  BEGIN TRY
		UPDATE ENTIDAD 
			SET ENT_SALDO = ENT_SALDO - @valor
			WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID))
					AND ENT_MANEJA_SALDO = 1 /*SALDO ADMINISTRADO LOCALMENTE*/
		COMMIT
		BREAK
	  END TRY
	  BEGIN CATCH
		ROLLBACK
		SET @intentos = @intentos + 1
		CONTINUE
	  END CATCH;			
	END

	--CONSULTAR SALDO ACTUAL
	SELECT @nuevoSaldo = DBO.[obtenerSaldo](@comercioID, 0 /*NO TRANSACCIONAL*/)

	RETURN 0

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCerrarSesionWebSeguridad]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCerrarSesionWebSeguridad](
	@UserLogin AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;
		
	UPDATE LoginInfo
	SET login_fecha_fin = GETDATE()
	FROM LoginInfo LI
		INNER JOIN Usuario U ON (U.usu_id = LI.login_usuario_id)
	WHERE U.usu_login = @UserLogin AND CONVERT(VARCHAR, LI.login_fecha_inicio, 103) = CONVERT(VARCHAR, GETDATE(), 103) 
		AND LI.login_tipo_id = 1

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webActualizarUsuario]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webActualizarUsuario](
	@userName VARCHAR(200),
	@userID VARCHAR(100),
	@userEmail VARCHAR(100),
	@userHash VARCHAR(100),
	@userProfileID INT,
	@userStatusID INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		IF @userHash = '' 
		BEGIN
			--EDICI�N SIN NUEVA CLAVE
			UPDATE Usuario
			SET 
				usu_nombre = @userName, 
				usu_email = @userEmail, 
				usu_perfil_id = @userProfileID, 
				usu_estado_usuario_id = @userStatusID
			WHERE
				usu_login = @userID		
		END
		ELSE
		BEGIN
			--EDICI�N CON NUEVA CLAVE
			UPDATE Usuario
			SET 
				usu_nombre = @userName, 
				usu_email = @userEmail, 
				usu_hash = @userHash, 
				usu_perfil_id = @userProfileID, 
				usu_estado_usuario_id = @userStatusID,
				usu_fecha_exp_clave = DATEADD(MONTH, 0, GETDATE())
			WHERE
				usu_login = @userID				
		END	

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarDatosUsuarioPorLogin]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarDatosUsuarioPorLogin](
	@userID VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT U.usu_login, U.usu_nombre, U.usu_email, U.usu_estado_usuario_id, U.usu_perfil_id FROM Usuario U WHERE U.usu_login = @userID

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarDatosUsuario]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarDatosUsuario](
	@userID INT /*User ID Logeado*/
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT U.usu_login, U.usu_nombre, U.usu_email, U.usu_estado_usuario_id, U.usu_perfil_id FROM Usuario U WHERE usu_id = @userID

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarOpcionesSubmenu]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarOpcionesSubmenu](
	@userID INT,
	@menuID INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		CAST(O.OPC_ID AS VARCHAR) AS OPC_ID, 
		O.OPC_TITULO, 
		O.OPC_URL, 
		O.OPC_IMG_URL 
	FROM PERFILXOPCION PXO 
		INNER JOIN OPCION O ON (O.OPC_ID = PXO.OPC_ID) 
		INNER JOIN PERFIL P ON (P.PERF_ID = PXO.PERF_ID) 
		INNER JOIN USUARIO U ON (U.USU_PERFIL_ID = P.PERF_ID) 
	WHERE U.USU_ID = @userID AND O.OPC_OPCION_PADRE_ID = @menuID AND O.OPC_TIPO_OPCION_ID = 2 
	ORDER BY O.opc_orden
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarModulosMenu]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarModulosMenu](
	@userID INT
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		CAST(O.OPC_ID AS VARCHAR) AS OPC_ID, 
		O.OPC_TITULO, 
		O.OPC_URL, 
		O.OPC_IMG_URL 
	FROM PERFILXOPCION PXO 
		INNER JOIN OPCION O ON (O.OPC_ID = PXO.OPC_ID) 
		INNER JOIN PERFIL P ON (P.PERF_ID = PXO.PERF_ID) 
		INNER JOIN USUARIO U ON (U.USU_PERFIL_ID = P.PERF_ID) 
	WHERE U.USU_ID = @userID AND O.OPC_TIPO_OPCION_ID = 1

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTerminalesXComercioJNTS]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTerminalesXComercioJNTS](
	@filterMerchant VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TERMINAL_ID, ACTIVO, DESCRIPCION 
	FROM assenda.dbo.TERMINALES_ASSENDA  
	WHERE COMERCIO_ID = @filterMerchant
		AND TERMINAL_ID COLLATE SQL_Latin1_General_CP1_CI_AS NOT IN (SELECT pto_numero FROM PuntoVenta PV INNER JOIN PuntoVentaXEntidad PVE ON (PV.pto_id = PVE.pto_id) INNER JOIN Entidad E ON (PVE.ent_id = E.ent_id AND E.ent_comercio_JNTS=@filterMerchant))
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarTerminalesXComercioConred]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarTerminalesXComercioConred](
	@filterMerchant VARCHAR(20)
)
AS
BEGIN

	SET NOCOUNT ON;

	SELECT PV.pto_numero, (CASE PV.pto_estado_punto_venta_id WHEN 1 THEN 1 WHEN 2 THEN 0 END) AS estado, PV.pto_observacion
	FROM PuntoVentaXEntidad PVE
		INNER JOIN PuntoVenta PV ON (PVE.pto_id = PV.pto_id)
		INNER JOIN Entidad E ON (PVE.ent_id = E.ent_id)
	WHERE E.ent_comercio_JNTS = @filterMerchant
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCrearUsuario]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCrearUsuario](
	@userName VARCHAR(200),
	@userID VARCHAR(100),
	@userEmail VARCHAR(100),
	@userHash VARCHAR(100),
	@userProfileID INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	BEGIN TRANSACTION

	BEGIN TRY	
	
		INSERT INTO Usuario(usu_nombre, usu_email, usu_login, usu_hash, usu_perfil_id, usu_estado_usuario_id, usu_entidad_id, usu_fecha_exp_clave)
			VALUES (@userName, @userEmail, @userID, @userHash, @userProfileID, 1/*Activo*/, 0/*Endidad Default*/, DATEADD(MONTH, 0, GETDATE()) )

		COMMIT TRANSACTION

		SELECT 1 /*OK*/

	END TRY
	BEGIN CATCH
		
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

		SELECT 0 /*ERROR*/

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarUsuarios]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarUsuarios]
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		U.usu_nombre,
		U.usu_email,
		U.usu_login,
		P.perf_nombre,
		EU.descripcion 
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN EstadoUsuario EU ON (U.usu_estado_usuario_id = EU.id)
	WHERE U.usu_perfil_id NOT IN (5)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webContinuarProcesoRestauracionClave]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webContinuarProcesoRestauracionClave](
	@codActivacion VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @status AS INT
	DECLARE @restID AS INT
	DECLARE @rndTxt AS VARCHAR(100)

	SET @rndTxt = ''

	--VERIFICAR QUE EXISTA UN PREPROCESO PARA REACTIVACI�N DE CLAVE
	SELECT @restID = rest_id FROM UsuarioxRestauracionClave WHERE rest_rand = @codActivacion AND rest_estado = 1

	IF @restID IS NOT NULL
	BEGIN

		EXEC [sp_utilsGenerarTextoRandomico] @rndTxt OUTPUT

		/*ESTABLECER OTRO RAND�MICO Y DEVOLVERLO*/
		UPDATE UsuarioxRestauracionClave
			SET rest_rand = @rndTxt
		WHERE rest_id = @restID

		SET @status = 1	/*OK*/
	END
	ELSE
		SET @status = 0	/*ERROR*/ 

	SELECT @status AS STATUS, @rndTxt AS RNDTXT

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webContarPaginasMensajes]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webContarPaginasMensajes](
	@userID INT, /*User ID Logeado*/
	@itemsXPagina NUMERIC(6,2) /*Mensajes por p�gina*/
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @max_page AS NUMERIC(10,2)
	DECLARE @max_page_tmp AS NUMERIC(10,2)
	DECLARE @max_page_final AS INT

	IF @itemsXPagina < (SELECT COUNT(*) FROM Mensaje M WHERE M.men_usuario_id_para = @userID)
	BEGIN
		SELECT @max_page = COUNT(*)/@itemsXPagina FROM Mensaje M WHERE M.men_usuario_id_para = @userID
		SELECT @max_page_tmp = ((@max_page / CAST(@max_page AS INT)) - 1)
		
		IF @max_page_tmp > 0
			SET @max_page_final = CAST(@max_page AS INT) + 1
		ELSE
			SET @max_page_final = CAST(@max_page AS INT)
		
		SELECT @max_page_final AS TOTAL_PAGES
		
	END
	ELSE
	BEGIN
		SELECT COUNT(*) AS TOTAL_PAGES FROM Mensaje M WHERE M.men_usuario_id_para = @userID
	END

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webContarMensajesNoLeidos]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webContarMensajesNoLeidos](
	@userID INT /*User ID Logeado*/
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT 
		COUNT(*) AS TOTAL
	FROM Mensaje M
	WHERE M.men_usuario_id_para = @userID AND M.men_fecha_lectura IS NULL

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarComprasXProveedor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarComprasXProveedor](
	@supplierID AS BIGINT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	SELECT TOP 5
		C.comp_fecha,
		C.comp_saldo_inicial,
		C.comp_valor,
		C.comp_saldo_final,
		B.ban_nombre + '-' + CB.cue_numero + '-' + TC.nombre AS Cuenta,
		C.comp_observacion,
		U.usu_nombre 
	FROM Compra C
		INNER JOIN CuentaBanco CB ON (C.comp_cuenta_banco_id = CB.cue_id)
		INNER JOIN Banco B ON (CB.cue_banco_id = B.ban_id)
		INNER JOIN TipoCuenta TC ON (CB.cue_tipo_cuenta_id = TC.id)
		INNER JOIN [Log] L ON (C.comp_log_id = L.log_id)
		INNER JOIN Usuario U ON (L.log_usuario_id = U.usu_id)
	WHERE comp_proveedor_id = @supplierID
	ORDER BY 1 DESC

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webConsultarMensajes]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webConsultarMensajes](
	@userID INT, /*User ID Logeado*/
	@currentPage INT,
	@itemsXPage INT
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @currentPage2 INT
	DECLARE @comodin INT

	SET @currentPage2 = CAST(@currentPage AS INT) - 1 

	IF @currentPage2 > 0
		SET @comodin = 1
	ELSE 
		SET @comodin = 0	

	SELECT id, fecha_envio, mensaje_de, texto, nivel, leido FROM (
		SELECT 
			ROW_NUMBER() Over(Order by M.men_fecha_envio DESC) As RowNum,
			M.men_id AS id,
			replace(replace(convert(varchar, M.men_fecha_envio, 100), 'AM', ' am'), 'PM', ' pm') AS fecha_envio,
			U.usu_nombre AS mensaje_de,
			M.men_texto AS texto,
			T.descripcion AS nivel,
			(CASE WHEN M.men_fecha_lectura IS NULL THEN 0 ELSE 1 END) AS leido
		FROM Mensaje M
			INNER JOIN TipoNivelMensaje T ON (M.men_tipo_nivel_mensaje_id = T.id)
			INNER JOIN Usuario U ON (M.men_usuario_id_de = U.usu_id)
		WHERE M.men_usuario_id_para = @userID
	) AS RESULTADO
	WHERE RowNum BETWEEN (@currentPage2 * @itemsXPage + @Comodin) AND (@currentPage * @itemsXPage)

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webCambiarClave]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webCambiarClave](
	@userID INT,
	@actualHASH VARCHAR(150),
	@newHASH VARCHAR(150)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS INT

	IF NOT EXISTS(SELECT * FROM Usuario U WHERE U.usu_id = @userID AND U.usu_hash = @actualHASH)
		SET @status = 0	/*Error Clave Actual Err�nea*/ 
	ELSE
		BEGIN
			/*Validar que No sea la misma clave de 4 cambios anteriores*/
			IF EXISTS(SELECT * FROM (SELECT TOP 4 usu_hash_antiguo FROM UsuarioHistoricoClaves WHERE usu_id = @userID ORDER BY id DESC) AS TMP WHERE TMP.usu_hash_antiguo = @newHASH)
			BEGIN
				SET @status = 2	/* ERROR CLAVES ANTERIORES SIMILARES */
			END
			ELSE
			BEGIN
				/*Clave Actual Correcta, Cambiarla y Actualizar Fecha de Expiraci�n a 2 Meses de la Fecha Actual*/
				UPDATE Usuario
					SET usu_hash = @newHASH,
						usu_fecha_exp_clave = DATEADD(MONTH, 2, GETDATE())	--2 Meses a Partir de Fecha Actual
				WHERE usu_id = @userID AND usu_hash = @actualHASH

				/*Insertar Hist�rico de Clave*/
				INSERT INTO UsuarioHistoricoClaves(fecha, usu_id, usu_hash_antiguo)
					VALUES(GETDATE(), @userID, @newHASH)

				SET @status = 1	/* OK */			
			END

		END

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcPreProcesar]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcPreProcesar](
	/*PAR�METROS DE ENTRADA*/
	@tipoMensajeReq AS VARCHAR(10),	
	@interfazOrigen AS VARCHAR(12),
	@codigoProcesamiento AS VARCHAR(10),
	@stan AS INT, 
	@rrn AS VARCHAR(20),
	@ptoVentaID AS VARCHAR(20),
	@comercioID AS VARCHAR(20), 
	@productoEAN AS VARCHAR(20), 
	@refCliente1 AS VARCHAR(50), 
	@datosAdicionales AS VARCHAR(100),
	@valor AS MONEY, 
	@IP AS VARCHAR(20), 
	@puertoTCP AS INT,
	/*PAR�METROS DE SALIDA*/
	@rutearTrx AS SMALLINT OUTPUT, 
	@codRespuesta AS VARCHAR(10) OUTPUT, 
	@msjRespuesta AS VARCHAR(500) OUTPUT, 
	@trxID AS BIGINT OUTPUT,
	@isoMuxName AS VARCHAR(50) OUTPUT,	--A donde rutear la trx. con base en parametrizaci�n tabla: Matriz_NivelEntidadXProducto
	@codRespuestaISO VARCHAR(2) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES LOCALES*/
	DECLARE @tra_comercio_saldo AS MONEY
	DECLARE @validacion_local AS SMALLINT	/*1-SI , 0-NO*/
	DECLARE @productoID_BD AS BIGINT
	DECLARE @refCliente2 AS VARCHAR(50)
	DECLARE @interfazDestino AS VARCHAR(20)
	
	BEGIN TRANSACTION

	SET @interfazDestino = ''

	BEGIN TRY
		/*INICIO VALIDACIONES DEL NEGOCIO*/

		IF NOT EXISTS(SELECT * FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID)))	--EXISTE COMERCIO?

			SELECT @codRespuesta = 'JTS000001', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = '11'

		ELSE IF NOT EXISTS(SELECT * FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID)		--EXISTE PUNTO DE VENTA?

			SELECT @codRespuesta = 'JTS000002', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N1'

		ELSE IF NOT EXISTS(SELECT * FROM PuntoVentaXEntidad WHERE PTO_ID = (SELECT PTO_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) AND ENT_ID = (SELECT ent_id FROM ENTIDAD WHERE ent_comercio_JNTS = @comercioID) AND FECHA_FINAL IS NULL)	--PUNTO DE VENTA RELACIONADO AL COMERCIO

			SELECT @codRespuesta = 'JTS000003', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N2'

		ELSE IF (SELECT ENT_ESTADO_ENTIDAD_ID FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID))) > 1	--COMERCIO ACTIVO?

			SELECT @codRespuesta = 'JTS000004', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N3'

		ELSE IF (SELECT PTO_ESTADO_PUNTO_VENTA_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) > 1	--PUNTO DE VENTA ACTIVO?

			SELECT @codRespuesta = 'JTS000005', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N4'

		ELSE IF DBO.[ValidarSaldo](LTRIM(RTRIM(@comercioID)), @valor) = 0	--COMERCIO CON SALDO?

			SELECT @codRespuesta = 'JTS000006', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'P3'

		ELSE
			--TODO OK, CONTINUAR CON TRANSACCI�N
			SELECT @codRespuesta = 'JTS000000', @validacion_local = 1, @codRespuestaISO = NULL, @tra_comercio_saldo = DBO.[ObtenerSaldo](@comercioID, 0 /*NO TRANSACCIONAL*/)

		/*FIN VALIDACIONES DEL NEGOCIO*/

		IF @validacion_local = 1
		BEGIN
			/*	REGLAS DE NEGOCIO CUMPLIDAS, @validacion_local = 1	*/

			/*VENTA DE RECARGAS A CELULAR*/
			IF (@tipoMensajeReq = '0200' AND @codigoProcesamiento = '380000')
				/*RECAUDO DE FACTURAS*/
				OR (@tipoMensajeReq = '0200' AND @codigoProcesamiento = '150000')
			BEGIN

				/*OBTENER COD. DE PRODUCTO E ISOMUX_NAME SEG�N EAN*/
				SELECT 
					@productoID_BD = MNEP.producto_id,
					@isoMuxName = PR.prov_isomux_nombre
				FROM Matriz_NivelEntidadXProducto MNEP
					INNER JOIN Producto P ON (MNEP.producto_id = P.pro_id)
					INNER JOIN Proveedor PR ON (P.pro_proveedor_id = PR.prov_id)
					INNER JOIN TipoNivelEntidad TNE ON (MNEP.tipo_nivel_entidad_id = TNE.id)
					INNER JOIN Entidad E ON (E.ent_tipo_nivel_entidad_id = TNE.id)
				WHERE
					E.ent_comercio_JNTS = LTRIM(RTRIM(@comercioID)) AND P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))

				/*VALIDAR EL C�DIGO DE PRODUCTO*/
				IF @productoID_BD IS NULL	--EXISTE PRODUCTO?
					SELECT @rutearTrx = 0, @interfazDestino = '', @isoMuxName = '', @codRespuesta = 'JTS000007', @codRespuestaISO = '12'
				ELSE
					SELECT @rutearTrx = 1, @interfazDestino = @isoMuxName, @codRespuestaISO = '00'
				
				/*ACCIONES ESPEC�FICAS POR PRODUCTO*/
				IF @rutearTrx = 1 
				BEGIN
					--RECAUDO DE FACTURAS
					IF @tipoMensajeReq = '0200' AND @codigoProcesamiento = '150000'
					BEGIN
					
						IF LEN(@datosAdicionales) > 16
						BEGIN 
							
							IF SUBSTRING(@datosAdicionales, 1, 3) = '415'	--ID INICIAL C�DIGO DE BARRAS COLOMBIA
							BEGIN
							
								IF EXISTS(SELECT * FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD)
								BEGIN
									--SEPARAR CAMPOS DEL C�DIGO DE BARRAS
									SELECT @refCliente1=referencia1, @refCliente2=referencia2 FROM dbo.[SepararCamposCodigoBarras](@datosAdicionales, @productoID_BD)
								END
								ELSE
								BEGIN
									--CONVENIO NO ACTIVO
									SELECT @rutearTrx = 0, @codRespuesta = 'JTS000011', @codRespuestaISO = 'N7'									
								END
							END
							ELSE
							BEGIN 
								--ID INICIAL C�DIGO DE BARRAS ERR�NEO
								SELECT @rutearTrx = 0, @codRespuesta = 'JTS000010', @codRespuestaISO = 'N6'
							END								
						END 
						ELSE
						BEGIN
							--LONGITUD C�DIGO DE BARRAS ERR�NEA
							SELECT @rutearTrx = 0, @codRespuesta = 'JTS000009', @codRespuestaISO = 'N5'
						END

					END				
				END

				/*PAR�METROS DE SALIDA OK*/
				SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)

			END			
			ELSE IF (@tipoMensajeReq = '0100' AND @codigoProcesamiento = '370000')
			BEGIN
				/*CONSULTA DE TRANSACCI�N RECARGA*/
				
				DECLARE 
					@tra_codigo_respuestaConsulta AS VARCHAR(5),
					@tra_rrnConsulta AS VARCHAR(20),
					@tra_autorizacion_ope_idConsulta AS VARCHAR(20),
					@tra_auth_id_p38Consulta AS VARCHAR(10),
					@tra_idConsulta AS BIGINT
				
				SELECT @rutearTrx = 0, @isoMuxName = 'MIDDLEWARE'				
				SET @interfazDestino = @isoMuxName
				
				SELECT
					@tra_idConsulta = tra_id,
					@tra_codigo_respuestaConsulta = tra_codigo_respuesta,
					@tra_rrnConsulta = ISNULL(tra_rrn, ''),
					@tra_autorizacion_ope_idConsulta = ISNULL(tra_autorizacion_ope_id, ''),
					@tra_auth_id_p38Consulta = ISNULL(tra_auth_id_p38, '')					
				FROM Transaccion WITH(NOLOCK)
				WHERE 
					tra_comercio_id = @comercioID AND
					tra_terminal_id = @ptoVentaID AND
					tra_valor = @valor AND
					tra_stan = SUBSTRING(@datosAdicionales, 5, 10) AND
					CONVERT(VARCHAR, tra_fecha_inicial, 112) = CAST(YEAR(GETDATE()) AS VARCHAR(4)) + SUBSTRING(@datosAdicionales, 1, 4)
				
				IF NOT @tra_idConsulta IS NULL
				BEGIN
					--Consulta OK, Retornar Par�metros
					SELECT @codRespuestaISO = @tra_codigo_respuestaConsulta, 
							@msjRespuesta = RIGHT(REPLICATE('0',12) + CAST( @tra_rrnConsulta AS VARCHAR(12)),12) +
											RIGHT(REPLICATE('0',6) + CAST( @tra_auth_id_p38Consulta AS VARCHAR(6)),6) +
											RIGHT(REPLICATE('0',20) + CAST( @tra_autorizacion_ope_idConsulta AS VARCHAR(20)),20), 
							@codRespuesta = 'JTS600000'
				END
				ELSE
				BEGIN
					--Consulta Err�nea
					SELECT @codRespuestaISO = 'N8', @msjRespuesta = '', @codRespuesta = 'JTS600001'
				END
												
				/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
				SELECT 
					@productoID_BD = P.pro_id 
				FROM Producto P 
				WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))
				
			END	
			ELSE IF (@tipoMensajeReq = '0100' AND @codigoProcesamiento = '360000')
			BEGIN
				/*CONSULTA DE PAR�METROS DE LECTURA X CONVENIO*/
					
				SELECT @rutearTrx = 0, @isoMuxName = 'MIDDLEWARE'				
				SET @interfazDestino = @isoMuxName					
					
				/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
				SELECT 
					@productoID_BD = P.pro_id 
				FROM Producto P 
				WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))					
					
				/*VALIDAR EL C�DIGO DE PRODUCTO*/
				IF @productoID_BD IS NULL	--EXISTE PRODUCTO?
					SELECT @rutearTrx = 0, @codRespuesta = 'JTS000007', @codRespuestaISO = '12'
				ELSE
					SELECT @rutearTrx = 0, @codRespuestaISO = '00'					
					
				IF @codRespuestaISO = '00'
				BEGIN	
					
					--SEPARAR C�DIGO DE EAN DEL BARCODE ENVIADO
					IF LEN(@datosAdicionales) > 16
					BEGIN 
						
						IF SUBSTRING(@datosAdicionales, 1, 3) = '415'	--ID INICIAL C�DIGO DE BARRAS COLOMBIA
						BEGIN
						
							IF EXISTS(SELECT * FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD)
							BEGIN
								--DEVOLVER PAR�METROS DE LECTURA DEL C�DIGO DE BARRAS PARA EL CONVENIO, EN LA VARIABLE MENSAJE DE RESPUESTA
								SELECT 
									@msjRespuesta = 
									CAST(CxP.conv_cod_parcial AS VARCHAR)  +
									CAST(CxP.conv_ref_doble AS VARCHAR) + 
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_pos_ini_ref1 AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_longitud_ref1 AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_pos_ini_valor AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_longitud_valor AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_pos_ini_ref2 AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_longitud_ref2 AS VARCHAR),3)
								FROM ConvenioXProducto CxP
								WHERE conv_codigo_EAN = SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD								
								
								SET @codRespuesta = 'JTS500000'
								
							END
							ELSE
							BEGIN
								--CONVENIO NO ACTIVO
								SELECT @rutearTrx = 0, @codRespuesta = 'JTS000011', @codRespuestaISO = 'N7'									
							END
						END
						ELSE
						BEGIN 
							--ID INICIAL C�DIGO DE BARRAS ERR�NEO
							SELECT @rutearTrx = 0, @codRespuesta = 'JTS000010', @codRespuestaISO = 'N6'
						END								
					END 
					ELSE
					BEGIN
						--LONGITUD C�DIGO DE BARRAS ERR�NEA
						SELECT @rutearTrx = 0, @codRespuesta = 'JTS000009', @codRespuestaISO = 'N5'
					END
				END
			END					
			ELSE
			BEGIN
				/*TIPO DE TRANSACCI�N INEXISTENTE*/
				SELECT @rutearTrx = 0, @codRespuesta = 'JTS000012', @codRespuestaISO = '12'
				SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)
			END
			
		END
		ELSE
		BEGIN
			/*	REGLAS DE NEGOCIO NO CUMPLIDAS, @validacion_local = 0	*/
			SELECT @rutearTrx = 0, @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta), @isoMuxName = ''
			
			/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
			SELECT 
				@productoID_BD = P.pro_id 
			FROM Producto P 
			WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))
			
		END

		/*REGISTRAR TRANSACCI�N*/
		INSERT INTO Transaccion(tra_fecha_inicial, tra_tipo_mensaje_req, tra_interfaz_origen, tra_interfaz_destino, tra_codigo_respuesta, tra_codigo_proceso, tra_stan, tra_rrn, tra_terminal_id, tra_comercio_id,
								tra_comercio_saldo_inicial, tra_referencia_cliente_1, tra_referencia_cliente_2, tra_datos_adicionales, tra_valor, tra_producto_id, tra_ip_cliente, tra_puerto_atencion)	
		VALUES (GETDATE(), @tipoMensajeReq, @interfazOrigen, @interfazDestino, @codRespuestaISO, @codigoProcesamiento, @stan, @rrn, @ptoVentaID, LTRIM(RTRIM(@comercioID)),
				@tra_comercio_saldo, @refCliente1, @refCliente2, @datosAdicionales, @valor, @productoID_BD, @IP, @puertoTCP )

		SELECT @trxID = SCOPE_IDENTITY()

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1, @codRespuesta = 'JTS950000', @rutearTrx = 0, @isoMuxName = ''
		SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcPostProcesar]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcPostProcesar](
	/*PAR�METROS DE ENTRADA*/
	@trxID AS BIGINT, 
	@tipoMensajeResp AS VARCHAR(10),	
	@authID AS VARCHAR(6),
	@operadorAuthID AS VARCHAR(20),
	@codRespuestaFormateador AS VARCHAR(10),
	@codRespuestaISO AS VARCHAR(2),
	/*PAR�METROS DE SALIDA*/
	@codRespuesta AS VARCHAR(10) OUTPUT, 
	@msjRespuesta AS VARCHAR(500) OUTPUT, 
	@saldoComercio AS MONEY OUTPUT,
	@codRespuestaISOReal AS VARCHAR(2) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES*/
	DECLARE @retorno_sp AS SMALLINT
	DECLARE @comercio_id AS VARCHAR(20)
	DECLARE @valor_venta AS MONEY
	DECLARE @tipoMensaje AS VARCHAR(10)
	DECLARE @codigoProcesamiento AS VARCHAR(10)
	
	BEGIN TRANSACTION;

	BEGIN TRY
		/*L�GICA DEL NEGOCIO*/

		--CONSULTAR DATOS DE LA TRANSACCI�N EN CURSO
		SELECT 
			@comercio_id			= TRA_COMERCIO_ID,
			@valor_venta			= TRA_VALOR,
			@tipoMensaje			= TRA_TIPO_MENSAJE_REQ,
			@codigoProcesamiento	= TRA_CODIGO_PROCESO
		FROM TRANSACCION WITH(NOLOCK)	--NO BLOQUEANTE
		WHERE TRA_ID = @trxID
		

		/*VENTA DE RECARGAS A CELULAR*/
		IF @tipoMensaje = '0200' AND @codigoProcesamiento = '380000'
		BEGIN

			---MANEJO LOCAL DE SALDO
			--VENTA EXITOSA
			IF @authID <> '0' AND @codRespuestaISO = '00'
			BEGIN

				--DISMINUIR SALDO DEL COMERCIO
				EXEC @retorno_sp=DBO.[sp_mtcDisminuirSaldo] @comercio_id, @valor_venta, @saldoComercio OUTPUT

			END
			ELSE
			BEGIN
				--VENTA NO EXITOSA

				--CONSULTAR SALDO ACTUAL
				SET @saldoComercio = DBO.[ObtenerSaldo](@comercio_id, 0 /*NO TRANSACCIONAL*/)
			END
			
			--SOLO GATEWAY SIN MANEJO DE SALDO
			SET @saldoComercio = DBO.[ObtenerSaldo](@comercio_id, 0 /*NO TRANSACCIONAL*/)			
			
		END
		ELSE
		BEGIN
			--CONSULTAR SALDO ACTUAL
			SET @saldoComercio = 0
		END
		
		/*OBTENER RSP. CODE REAL, MAPEADO*/
		SELECT @codRespuestaISOReal = dbo.[ObtenerMensajeISOMapeado](@codRespuestaFormateador, @codRespuestaISO)

		/*ACTUALIZAR REGISTRO DE TRANSACCION*/
		UPDATE Transaccion
		SET		tra_fecha_fin				= GETDATE(),
				tra_tipo_mensaje_resp		= @tipoMensajeResp, 
				tra_codigo_respuesta		= @codRespuestaISOReal,	/*Respuesta Enviada al POS*/
				tra_codigo_respuestaB24		= @codRespuestaISO,		/*Respuesta Obtenida del Servidor Externo*/
				tra_auth_id_p38				= (CASE WHEN @authID='0' THEN NULL ELSE @authID END),
				tra_autorizacion_ope_id		= (CASE WHEN @operadorAuthID='0' THEN NULL ELSE @operadorAuthID END),
				tra_codigo_estado_final		= @codRespuestaFormateador,
				tra_comercio_saldo_final	= @saldoComercio,
				tra_rrn						= RIGHT(REPLICATE('0',12) + CAST(tra_id AS VARCHAR(12)),12)
		WHERE tra_id = @trxID

		/*PAR�METROS DE SALIDA OK*/
		SELECT @codRespuesta = 'JTS000000', @msjRespuesta = DBO.[ObtenerMensaje](@codRespuestaFormateador)
		IF @msjRespuesta IS NULL 
			SET @msjRespuesta = 'Mensaje ''' + @codRespuesta + ''' No Encontrado'

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
	
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @codRespuesta = 'JTS940000', @saldoComercio = 0
		SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)

		PRINT(ERROR_MESSAGE())

	END CATCH;

END
GO
/****** Object:  StoredProcedure [dbo].[sp_mtcEnviarMensajeUsuario]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_mtcEnviarMensajeUsuario](
	@UserIDFrom AS INT,
	@MsgLevelID AS INT,
	@Message AS VARCHAR(200)
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO Mensaje (men_fecha_envio, men_usuario_id_de, men_usuario_id_para, men_tipo_nivel_mensaje_id, men_texto)	
		SELECT GETDATE(), @UserIDFrom, U.usu_id, @MsgLevelID, @Message FROM Usuario U WHERE U.usu_perfil_id = 6 /*Solo Ususarios Administradores Transaccionales*/
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webValidarObjetoPorMetodoLogID]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarObjetoPorMetodoLogID](
	@UserID AS INT,
	@Object AS VARCHAR(100),
	@Method AS VARCHAR(100),
	@SessionID AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FuncID AS INT
	DECLARE @LogID AS BIGINT

	SELECT @FuncID = F.func_id
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN PerfilXFuncion PXF ON (P.perf_id = PXF.perf_id)
		INNER JOIN Funcion F ON (F.func_id = PXF.func_id)
		INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
		INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
	WHERE 
		U.usu_id = @UserID AND
		O.nombre = @Object AND
		M.nombre = @Method 

	IF @FuncID IS NOT NULL
	BEGIN
		INSERT INTO Log
			(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
		VALUES
			(@UserID, GETDATE(), @FuncID, 5, '', '', @SessionID)
						
		SELECT @LogID = SCOPE_IDENTITY()
			
		SELECT @FuncID AS FuncID, @LogID AS LogID
	END
	ELSE
	BEGIN
		SELECT @FuncID = F.func_id
		FROM Funcion F 
			INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
			INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
		WHERE 
			O.nombre = @Object AND
			M.nombre = @Method 		
			
		IF @FuncID IS NOT NULL		
		BEGIN
			INSERT INTO Log
				(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
			VALUES
				(@UserID, GETDATE(), @FuncID, 3, '', '', @SessionID)
				
			SELECT @LogID = SCOPE_IDENTITY()				
				
			SELECT NULL AS FuncID, @LogID AS LogID		
		END
		ELSE
		BEGIN
			INSERT INTO Log
				(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
			VALUES
				(@UserID, GETDATE(), @FuncID, 3, 'Invalid Object.Method (' + ISNULL(@Object, '') + '.' + ISNULL(@Method, '') + ')', '', @SessionID)
			
			SELECT @LogID = SCOPE_IDENTITY()			
				
			SELECT NULL AS FuncID, @LogID AS LogID
		END
				
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webValidarObjetoPorMetodo]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webValidarObjetoPorMetodo](
	@UserID AS INT,
	@Object AS VARCHAR(100),
	@Method AS VARCHAR(100),
	@SessionID AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FuncID AS INT

	SELECT @FuncID = F.func_id
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN PerfilXFuncion PXF ON (P.perf_id = PXF.perf_id)
		INNER JOIN Funcion F ON (F.func_id = PXF.func_id)
		INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
		INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
	WHERE 
		U.usu_id = @UserID AND
		O.nombre = @Object AND
		M.nombre = @Method 

	IF @FuncID IS NOT NULL
	BEGIN
		INSERT INTO Log
			(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
		VALUES
			(@UserID, GETDATE(), @FuncID, 5, '', '', @SessionID)
			
		SELECT @FuncID AS FuncID
	END
	ELSE
	BEGIN
		SELECT @FuncID = F.func_id
		FROM Funcion F 
			INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
			INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
		WHERE 
			O.nombre = @Object AND
			M.nombre = @Method 		
			
		IF @FuncID IS NOT NULL		
		BEGIN
			INSERT INTO Log
				(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
			VALUES
				(@UserID, GETDATE(), @FuncID, 3, 'Access Denied Object.Method (' + ISNULL(@Object, '') + '.' + ISNULL(@Method, '') + ')', '', @SessionID)
				
			SELECT NULL AS FuncID			
		END
		ELSE
		BEGIN
			INSERT INTO Log
				(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
			VALUES
				(@UserID, GETDATE(), @FuncID, 3, 'Invalid Object.Method (' + ISNULL(@Object, '') + '.' + ISNULL(@Method, '') + ')', '', @SessionID)
				
			SELECT NULL AS FuncID
		END
				
	END
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webMarcarMensajeLeido]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webMarcarMensajeLeido](
	@userID INT /*User ID Logeado*/,
	@messageID INT
)
AS
BEGIN
	DECLARE @status AS INT

	SET NOCOUNT ON;
	
	UPDATE Mensaje
	SET
		men_fecha_lectura = GETDATE()
	WHERE men_usuario_id_para = @userID AND men_id = @messageID

	SET @status = 1	/* OK */

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webInsertarMensajeXObjetoMetodoLog]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webInsertarMensajeXObjetoMetodoLog](
	@UserID AS INT,
	@Object AS VARCHAR(100),
	@Method AS VARCHAR(100),	
	@Message1 AS VARCHAR(250),
	@Message2 AS VARCHAR(250),
	@SessionID AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @FuncID AS INT

	SELECT @FuncID = F.func_id
	FROM Usuario U
		INNER JOIN Perfil P ON (U.usu_perfil_id = P.perf_id)
		INNER JOIN PerfilXFuncion PXF ON (P.perf_id = PXF.perf_id)
		INNER JOIN Funcion F ON (F.func_id = PXF.func_id)
		INNER JOIN Objeto O ON (O.id = F.func_objeto_id)
		INNER JOIN Metodo M ON (M.id = F.func_metodo_id)
	WHERE 
		U.usu_id = @UserID AND
		O.nombre = @Object AND
		M.nombre = @Method 

	INSERT INTO Log
		(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
	VALUES
		(@UserID, GETDATE(), @FuncID, 4, @Message1, @Message2, @SessionID)
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webInsertarMensajeInfoLog]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webInsertarMensajeInfoLog](
	@UserID AS INT,
	@LogTypeID AS INT,
	@Message1 AS VARCHAR(250),
	@Message2 AS VARCHAR(250),
	@SessionID AS VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO Log
		(log_usuario_id, log_fecha, log_funcion_id, log_tipo_log_id ,log_mensaje1, log_mensaje2, log_sessionID)
	VALUES
		(@UserID, GETDATE(), 5, @LogTypeID, @Message1, @Message2, @SessionID)
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webIniciarProcesoRestauracionClave]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webIniciarProcesoRestauracionClave](
	@userID VARCHAR(150),
	@correoID VARCHAR(150)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @userID_DB AS INT
	DECLARE @status AS INT
	DECLARE @rndTxt AS VARCHAR(100)

	SET @rndTxt = ''

	/*VERIFICAR DATOS CONTRA TABLA USUARIOS*/
	SELECT @userID_DB = U.usu_id FROM Usuario U
	WHERE U.usu_login = @userID AND U.usu_email = @correoID

	IF @userID_DB IS NOT NULL
	BEGIN
		--USUARIO EXISTE INSERTAR EN TABLA, SI NO HAY OTRO PROCESO PENDIENTE
		IF NOT EXISTS( SELECT * FROM UsuarioxRestauracionClave WHERE rest_usuario_id = @userID_DB AND rest_estado = 1 )
		BEGIN

			EXEC [sp_utilsGenerarTextoRandomico] @rndTxt OUTPUT

			INSERT INTO UsuarioxRestauracionClave(rest_usuario_id, rest_fecha, rest_rand, rest_estado)
					VALUES ( @userID_DB, GETDATE(), @rndTxt, 1 )

			SET @status = 1	/*OK*/
		END
		ELSE
			SET @status = 0	/*ERROR*/ 
		
	END
	ELSE
		SET @status = 0	/*ERROR*/ 

	SELECT @status AS STATUS, @rndTxt AS RNDTXT

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webTerminarProcesoRestauracionClave]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webTerminarProcesoRestauracionClave](
	@codActivacion VARCHAR(100),
	@passwordID VARCHAR(100)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @userID_DB AS INT
	DECLARE @status AS INT
	DECLARE @restID AS INT

	--VERIFICAR QUE EXISTA UN PREPROCESO PARA REACTIVACI�N DE CLAVE
	SELECT @restID = rest_id, @userID_DB = rest_usuario_id FROM UsuarioxRestauracionClave WHERE rest_rand = @codActivacion AND rest_estado = 1

	IF @restID IS NOT NULL AND @userID_DB IS NOT NULL
	BEGIN

		/*ACTUALIZAR ESTADO DEL PROCESO DE RESTAURACION*/
		UPDATE UsuarioxRestauracionClave
			SET rest_estado = 0
		WHERE rest_id = @restID

		/*ACTUALIZAR CLAVE DEL USUARIO*/

		UPDATE Usuario
			SET usu_hash = @passwordID
		WHERE usu_id = @userID_DB

		SET @status = 1	/*OK*/
	END
	ELSE
		SET @status = 0	/*ERROR*/ 

	SELECT @status AS STATUS

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webReporteComprasXProveedor]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteComprasXProveedor](
	@supplierID AS INT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	DECLARE @tmpTable AS TABLE(fecha DATETIME, prov_nombre VARCHAR(100), ban_nombre VARCHAR(100), cue_numero VARCHAR(100), comp_saldo_inicial MONEY, comp_valor MONEY, comp_saldo_final MONEY, comp_observacion VARCHAR(200), usu_nombre VARCHAR(100))

	INSERT INTO @tmpTable
		SELECT 
			C.comp_fecha AS fecha,
			P.prov_nombre,
			B.ban_nombre,
			CB.cue_numero,
			C.comp_saldo_inicial,
			C.comp_valor,
			C.comp_saldo_final,
			C.comp_observacion,
			U.usu_nombre 
		FROM Compra C
			INNER JOIN Proveedor P ON (C.comp_proveedor_id = P.prov_id)
			INNER JOIN CuentaBanco CB ON (C.comp_cuenta_banco_id = CB.cue_id)
			INNER JOIN Banco B ON (CB.cue_banco_id = B.ban_id)			
			INNER JOIN Log L ON (C.comp_log_id = L.log_id)
			INNER JOIN Usuario U ON (L.log_usuario_id = U.usu_id)			
		WHERE C.comp_fecha >= @startDate AND C.comp_fecha <= @endDateReport
			AND (C.comp_proveedor_id = @supplierID OR @supplierID = -1)
				
	--Select Final
	SELECT * FROM @tmpTable
	
	UNION ALL
	
	SELECT NULL, NULL, NULL, NULL, NULL, SUM(T.comp_valor), NULL, NULL, NULL FROM @tmpTable T
			
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webReporteAuditoria]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteAuditoria](
	@userID AS BIGINT,
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;

	DECLARE @endDateReport AS DATETIME	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)

	SELECT 
		L.log_fecha,
		U.usu_login,
		U.usu_nombre,
		O.nombre AS Objeto,
		M.nombre AS Metodo,
		TL.descripcion AS TipoMensaje,
		L.log_mensaje1
	FROM [Log] L
		INNER JOIN Usuario U ON (L.log_usuario_id = U.usu_id)
		INNER JOIN Funcion F ON (L.log_funcion_id = F.func_id)
		INNER JOIN Objeto O ON (F.func_objeto_id = O.id)
		INNER JOIN Metodo M ON (F.func_metodo_id = M.id)
		INNER JOIN TipoLog TL ON (L.log_tipo_log_id = TL.id)
	WHERE L.log_fecha >= @startDate AND L.log_fecha <= @endDateReport
		AND (L.log_usuario_id = @userID OR @userID = -1)
	ORDER BY 1
	
END
GO
/****** Object:  StoredProcedure [dbo].[sp_webReporteMovimientoCuentaComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webReporteMovimientoCuentaComercio](
	@merchantID AS VARCHAR(20),
	@startDate AS DATETIME,
	@endDate AS DATETIME
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @entityID AS INT
	DECLARE @endDateReport AS DATETIME	
	
	SET @endDateReport = DATEADD(DAY, 1, @endDate)		
	SELECT @entityID=ent_id FROM Entidad WHERE ent_comercio_JNTS= @merchantID
	
	SELECT 
		MC.mov_fecha,
		E.ent_comercio_JNTS,
		TMC.nombre,
		B.ban_nombre,
		CB.cue_numero,
		MC.mov_entidad_saldo_inicial,
		MC.mov_valor,
		MC.mov_entidad_saldo_final,
		MC.mov_observacion,
		U.usu_nombre 
	FROM MovimientoCuenta MC
		INNER JOIN Entidad E ON (MC.mov_entidad_id = E.ent_id)
		INNER JOIN TipoMovimientoCuenta TMC ON (MC.mov_tipo_movimiento_cuenta_id = TMC.id)
		LEFT OUTER JOIN CuentaBanco CB ON (MC.mov_cuenta_banco_id = CB.cue_id)
		LEFT OUTER JOIN Banco B ON (CB.cue_banco_id = B.ban_id)
		INNER JOIN Log L ON (MC.mov_log_id = L.log_id)
		INNER JOIN Usuario U ON (L.log_usuario_id = U.usu_id)
	WHERE (MC.mov_entidad_id = @entityID OR @entityID IS NULL) AND
		MC.mov_fecha >= @startDate AND MC.mov_fecha <= @endDateReport
	ORDER BY 1

END
GO
/****** Object:  StoredProcedure [dbo].[sp_webModificarSaldoComercio]    Script Date: 12/03/2012 09:39:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_webModificarSaldoComercio](
	@merchantID VARCHAR(20),
	@movementType INT,
	@movementAmount MONEY,
	@movementLog INT,
	@movementAccountBank INT,
	@movementDescription VARCHAR(250)
)
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @status AS INT
	DECLARE @entityID AS INT
	DECLARE @currentBalance AS MONEY
	DECLARE @newBalance AS MONEY
	DECLARE @newAmount AS MONEY
	DECLARE @pctGain AS NUMERIC(4,2)
	DECLARE @newBalanceGain AS MONEY
	
	SET @newBalance = 0
	SET @newBalanceGain = 0
	
	SELECT @entityID=ent_id, @currentBalance=ent_saldo, @pctGain=ent_comision_global FROM Entidad WHERE ent_comercio_JNTS= @merchantID

	IF @entityID IS NOT NULL
	BEGIN

		--Validar Tipo de Movimiento
		IF @movementType = 1							/*Asignaci�n de Saldo*/
			SET @newAmount = ABS(@movementAmount)
		ELSE IF @movementType = 2 OR @movementType = 4	/*Retiro de Saldo, Ajuste Sin Comisi�n*/
			SET @newAmount = ABS(@movementAmount) * -1	

		--Modificar Saldo
		EXEC [sp_webAjustarSaldoComercio] @merchantID, @newAmount, @newBalance OUTPUT

		INSERT INTO MovimientoCuenta 
			VALUES (@entityID, GETDATE(), @movementType, @newAmount, @currentBalance, 
				@newBalance, @movementLog, @movementAccountBank, @movementDescription)

		--Modificar Comisi�n
		IF @movementType = 1 OR @movementType = 2 		/*Asignaci�n de Saldo, Retiro de Saldo*/
		BEGIN 
		
			SET @newBalance = 0
			SET @newBalanceGain = (@newAmount * (@pctGain / 100))
		
			EXEC [sp_webAjustarSaldoComercio] @merchantID, @newBalanceGain, @newBalance OUTPUT

			INSERT INTO MovimientoCuenta 
				VALUES (@entityID, GETDATE(), 3, @newBalanceGain, (@newBalance - @newBalanceGain), 
					@newBalance, @movementLog, NULL, 'Comisi�n al Comercio (' + CAST(@pctGain AS VARCHAR) + '%)')			

		END

		SET @status = 1	/* OK */	
	END
	ELSE
		SET @status = 0	/* ERROR */

	SELECT @status AS STATUS, @newBalance AS NEWBALANCE

END
GO
