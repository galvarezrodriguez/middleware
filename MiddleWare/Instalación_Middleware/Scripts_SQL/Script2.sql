USE [Conred]
GO
/****** Object:  Table [dbo].[Proveedor]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Proveedor](
	[prov_id] [bigint] IDENTITY(1,1) NOT NULL,
	[prov_nombre] [varchar](100) NOT NULL,
	[prov_descripcion] [varchar](200) NULL,
	[prov_isomux_nombre] [varchar](50) NULL,
 CONSTRAINT [PK_Proveedor] PRIMARY KEY CLUSTERED 
(
	[prov_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Proveedor] ON
INSERT [dbo].[Proveedor] ([prov_id], [prov_nombre], [prov_descripcion], [prov_isomux_nombre]) VALUES (3, N'Movilred', N'Red Intermedia Servicios Transaccionales', N'ISOMUX_MOVILRED')
SET IDENTITY_INSERT [dbo].[Proveedor] OFF
/****** Object:  Table [dbo].[Objeto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Objeto](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Objeto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Objeto] ON
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (1, N'Index.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (2, N'Session')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (3, N'GeneralMessage')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (4, N'Security/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (5, N'Account/EditAccount.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (6, N'Account/ChangePassword.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (7, N'Security/AddUser.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (8, N'Security/ListUsers.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (9, N'Security/EditUser.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (10, N'Security/Objects.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (11, N'Security/Methods.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (12, N'Security/Functions.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (13, N'Security/Profiles.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (14, N'Security/AddFunctionsXProfile.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (23, N'Merchants/ActivateMerchant.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (24, N'Merchants/ListMerchants.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (25, N'Merchants/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (26, N'Merchants/EditMerchant.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (27, N'Banks/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (28, N'Banks/ListBanks.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (29, N'Banks/AddAccountsXBank.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (30, N'Merchants/AccountMovementMerchant.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (31, N'Reports/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (32, N'Reports/EntryPoint.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (34, N'Reports/ReportViewer.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (35, N'Transactions/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (36, N'Transactions/CurrentTransactions.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (37, N'Transactions/SearchTopupTransactions.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (39, N'Suppliers/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (40, N'Suppliers/AddSupplier.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (41, N'Suppliers/ListSuppliers.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (42, N'Suppliers/AddProductsXSupplier.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (43, N'Suppliers/PurchaseMovementXSupplier.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (44, N'Settings/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (45, N'Settings/ListBusinessModels.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (46, N'Security/WebModules.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (47, N'Security/AddOptionsXWebModule.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (48, N'Security/AddWebModulesXProfile.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (49, N'Transactions/TransactionsMonitor.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (50, N'Transactions/getStatsData.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (51, N'Conciliations/Manager.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (52, N'Conciliations/GenerateFileTopupMovilred.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (54, N'Merchants/TerminalsXMerchant.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (55, N'Settings/AddProductXBusinessModel.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (56, N'Transactions/SearchBillPaymentTransactions.aspx')
INSERT [dbo].[Objeto] ([id], [nombre]) VALUES (57, N'Suppliers/AddAgreementXProduct.aspx')
SET IDENTITY_INSERT [dbo].[Objeto] OFF
/****** Object:  Table [dbo].[Perfil]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Perfil](
	[perf_id] [bigint] IDENTITY(1,1) NOT NULL,
	[perf_nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Perfil] PRIMARY KEY CLUSTERED 
(
	[perf_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Perfil] ON
INSERT [dbo].[Perfil] ([perf_id], [perf_nombre]) VALUES (5, N'Servidor Transaccional Conred')
INSERT [dbo].[Perfil] ([perf_id], [perf_nombre]) VALUES (6, N'Administrador Servicios Transaccionales')
INSERT [dbo].[Perfil] ([perf_id], [perf_nombre]) VALUES (7, N'Seguridad')
INSERT [dbo].[Perfil] ([perf_id], [perf_nombre]) VALUES (8, N'Operaciones')
INSERT [dbo].[Perfil] ([perf_id], [perf_nombre]) VALUES (16, N'Servicio al Cliente')
INSERT [dbo].[Perfil] ([perf_id], [perf_nombre]) VALUES (17, N'Producci�n')
SET IDENTITY_INSERT [dbo].[Perfil] OFF
/****** Object:  Table [dbo].[LoginInfo]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoginInfo](
	[login_id] [bigint] IDENTITY(1,1) NOT NULL,
	[login_fecha_inicio] [datetime] NOT NULL,
	[login_fecha_fin] [datetime] NULL,
	[login_tipo_id] [bigint] NOT NULL,
	[login_usuario_id] [bigint] NOT NULL,
	[login_ip] [varchar](29) NOT NULL,
	[login_session_id] [varchar](50) NOT NULL,
 CONSTRAINT [PK_LoginInfo] PRIMARY KEY CLUSTERED 
(
	[login_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[LoginInfo] ON
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (1, CAST(0x0000A10F00CB267D AS DateTime), NULL, 3, 97, N'172.22.144.136', N'2wjvuri2ct1msbrxmhmg4z45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (2, CAST(0x0000A10F00CBDB1A AS DateTime), NULL, 3, 95, N'172.22.144.136', N'2wjvuri2ct1msbrxmhmg4z45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (3, CAST(0x0000A10F00CC3469 AS DateTime), NULL, 3, 95, N'172.22.144.136', N'2wjvuri2ct1msbrxmhmg4z45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (4, CAST(0x0000A10F00E60066 AS DateTime), NULL, 3, 94, N'172.22.144.136', N'cywnx345hdzvce45cxxh41i2')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (5, CAST(0x0000A10F00E65FFD AS DateTime), CAST(0x0000A10F00E6F3CC AS DateTime), 1, 94, N'172.22.144.136', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (6, CAST(0x0000A10F00E71B53 AS DateTime), CAST(0x0000A10F00E73218 AS DateTime), 1, 94, N'172.22.144.136', N'nd2a2z45fxtzvbym2oyfafjl')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (7, CAST(0x0000A10F01151324 AS DateTime), CAST(0x0000A10F01152E55 AS DateTime), 1, 97, N'127.0.0.1', N'd7812678a2f8d007cd9f0653cba1183e')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (8, CAST(0x0000A10F01153C97 AS DateTime), CAST(0x0000A10F01155BEB AS DateTime), 1, 97, N'127.0.0.1', N'67851c0f07eb4900114e300070a50497')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (9, CAST(0x0000A10F0119E3FD AS DateTime), CAST(0x0000A10F011AD5D8 AS DateTime), 1, 97, N'127.0.0.1', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (10, CAST(0x0000A11000891F76 AS DateTime), NULL, 3, 93, N'172.22.144.136', N'rvdferaxg5ula245qxskcgrf')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (11, CAST(0x0000A1100089DCD4 AS DateTime), CAST(0x0000A110008ADB37 AS DateTime), 1, 97, N'127.0.0.1', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (12, CAST(0x0000A110008B1B8D AS DateTime), CAST(0x0000A110008BC1F1 AS DateTime), 1, 94, N'172.22.144.136', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (13, CAST(0x0000A1100092EA9C AS DateTime), CAST(0x0000A110009346DF AS DateTime), 1, 94, N'172.22.144.136', N'rgrn2dnui5ekbrqqfqnmy4jv')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (14, CAST(0x0000A11000AA3164 AS DateTime), CAST(0x0000A11000B26D94 AS DateTime), 1, 94, N'172.22.144.136', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (15, CAST(0x0000A11000B9D725 AS DateTime), CAST(0x0000A11000BA3FD5 AS DateTime), 1, 93, N'172.22.144.136', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (16, CAST(0x0000A11000BA5D53 AS DateTime), CAST(0x0000A11000BA7D7E AS DateTime), 1, 95, N'172.22.144.136', N'fjryioilyjcydpvjhxwvhb55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (17, CAST(0x0000A11000BA8836 AS DateTime), CAST(0x0000A11000BB7688 AS DateTime), 1, 95, N'172.22.144.136', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (18, CAST(0x0000A11000BB87E3 AS DateTime), CAST(0x0000A11000BB915D AS DateTime), 1, 94, N'172.22.144.136', N'1guuan454mzr54552fwwflzz')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (19, CAST(0x0000A11000BD2E0E AS DateTime), CAST(0x0000A11000BD8901 AS DateTime), 1, 95, N'172.22.144.136', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (20, CAST(0x0000A11000BD98FA AS DateTime), CAST(0x0000A11000BDAA1D AS DateTime), 1, 94, N'172.22.144.136', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (21, CAST(0x0000A11000BDB7DE AS DateTime), CAST(0x0000A11000BE3EE3 AS DateTime), 1, 95, N'172.22.144.136', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (22, CAST(0x0000A11000C05286 AS DateTime), CAST(0x0000A11000C39ACA AS DateTime), 1, 95, N'172.22.144.136', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (23, CAST(0x0000A11000F0A9E4 AS DateTime), CAST(0x0000A11000F12F15 AS DateTime), 1, 95, N'172.22.144.136', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (24, CAST(0x0000A11000F13B95 AS DateTime), CAST(0x0000A11000F154A7 AS DateTime), 1, 94, N'172.22.144.136', N'wvpdi2yvragtzjqtfwv50k45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (25, CAST(0x0000A11000F1C6C0 AS DateTime), CAST(0x0000A11000F21343 AS DateTime), 1, 95, N'172.22.144.136', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (26, CAST(0x0000A11000F36A4F AS DateTime), CAST(0x0000A11000FA78C8 AS DateTime), 1, 95, N'172.22.144.136', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (27, CAST(0x0000A110010A8DC7 AS DateTime), CAST(0x0000A110010AA5ED AS DateTime), 1, 94, N'172.22.144.136', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (28, CAST(0x0000A110010E616D AS DateTime), CAST(0x0000A110010EA7E4 AS DateTime), 1, 94, N'172.22.144.136', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (29, CAST(0x0000A110011267CC AS DateTime), NULL, 3, 95, N'172.22.144.136', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (30, CAST(0x0000A11001127049 AS DateTime), CAST(0x0000A1100112F45B AS DateTime), 1, 95, N'172.22.144.136', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (31, CAST(0x0000A110011385F9 AS DateTime), CAST(0x0000A110011673F0 AS DateTime), 1, 95, N'172.22.144.136', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (32, CAST(0x0000A111008AB241 AS DateTime), CAST(0x0000A111008AD658 AS DateTime), 1, 94, N'172.19.17.97', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (33, CAST(0x0000A111008AE8CC AS DateTime), CAST(0x0000A111008BB84C AS DateTime), 1, 97, N'172.19.17.97', N'b6dfeb1c61a5e8537be23902d2aaccdd')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (34, CAST(0x0000A111008DE1BF AS DateTime), CAST(0x0000A1110092809E AS DateTime), 1, 97, N'172.19.17.97', N'2642633037fe21235207befb7ccb9ec5')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (35, CAST(0x0000A11100916790 AS DateTime), CAST(0x0000A1110091877D AS DateTime), 1, 95, N'172.22.144.136', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (36, CAST(0x0000A111009BAACC AS DateTime), CAST(0x0000A111009C67C7 AS DateTime), 1, 97, N'172.19.17.97', N'60d4938305de069b0830b44684fe8a58')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (37, CAST(0x0000A11100EBBFD8 AS DateTime), CAST(0x0000A11100EEE5AD AS DateTime), 1, 97, N'172.19.17.97', N'2872430c86bf5cb8c1a9d23b1aff17e8')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (38, CAST(0x0000A11200855762 AS DateTime), CAST(0x0000A1120085C387 AS DateTime), 1, 94, N'172.19.17.97', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (39, CAST(0x0000A112008B8C32 AS DateTime), CAST(0x0000A112008C44F6 AS DateTime), 1, 95, N'172.19.17.97', N'y4xj3o453wqmqlmqtmz4nd55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (40, CAST(0x0000A112008CD49B AS DateTime), CAST(0x0000A112008D415C AS DateTime), 1, 95, N'172.22.144.136', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (41, CAST(0x0000A112008F0038 AS DateTime), CAST(0x0000A1120094C19E AS DateTime), 1, 95, N'172.22.144.136', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (42, CAST(0x0000A1120096150F AS DateTime), CAST(0x0000A1120098D7D5 AS DateTime), 1, 95, N'172.22.144.136', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (43, CAST(0x0000A1120099014D AS DateTime), CAST(0x0000A11200998B5E AS DateTime), 1, 95, N'172.22.144.136', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (44, CAST(0x0000A1120099A7B0 AS DateTime), CAST(0x0000A112009B6F35 AS DateTime), 1, 95, N'172.22.144.136', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (45, CAST(0x0000A112009E10FC AS DateTime), CAST(0x0000A112009E2F24 AS DateTime), 1, 94, N'172.22.144.136', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (46, CAST(0x0000A11200A2D2A3 AS DateTime), CAST(0x0000A11200A8027E AS DateTime), 1, 95, N'172.22.144.136', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (47, CAST(0x0000A11200A876A4 AS DateTime), CAST(0x0000A11200A8EFD5 AS DateTime), 1, 95, N'172.22.144.136', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (48, CAST(0x0000A11200A94ACC AS DateTime), CAST(0x0000A11200A9DEDE AS DateTime), 1, 95, N'172.19.8.61', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (49, CAST(0x0000A11200AAFA2D AS DateTime), CAST(0x0000A11200AB039B AS DateTime), 1, 94, N'172.19.17.97', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (50, CAST(0x0000A11500D4B9F7 AS DateTime), CAST(0x0000A11500D50ADC AS DateTime), 1, 94, N'172.19.17.97', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (51, CAST(0x0000A11500D6DB67 AS DateTime), CAST(0x0000A11500D6F4C6 AS DateTime), 1, 94, N'172.22.144.136', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (52, CAST(0x0000A11500D72755 AS DateTime), CAST(0x0000A11500D73D7E AS DateTime), 1, 94, N'172.19.17.97', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (53, CAST(0x0000A11500EFBB22 AS DateTime), CAST(0x0000A11500F51C20 AS DateTime), 1, 97, N'172.19.17.97', N'052cec81e1cc01808f7f5d43c8bcc574')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (54, CAST(0x0000A11500F6E5F9 AS DateTime), CAST(0x0000A11500F7DECF AS DateTime), 1, 93, N'172.19.17.97', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (55, CAST(0x0000A11500FF24F8 AS DateTime), CAST(0x0000A11501034BC6 AS DateTime), 1, 97, N'172.19.17.97', N'fac26a703007e0cdc5007e1308db20ca')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (56, CAST(0x0000A115010BC690 AS DateTime), CAST(0x0000A115010BD685 AS DateTime), 1, 97, N'172.19.17.97', N'd29e2962d2ebe59b001df836d7ada5cf')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (57, CAST(0x0000A11600790816 AS DateTime), CAST(0x0000A11600793224 AS DateTime), 1, 97, N'172.19.17.97', N'18953fa0315f9492186046696a027830')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (58, CAST(0x0000A116007DC185 AS DateTime), CAST(0x0000A116007E6E2B AS DateTime), 1, 97, N'172.19.17.97', N'538c996cc4d1087798905482abfdc8be')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (59, CAST(0x0000A116007F1282 AS DateTime), CAST(0x0000A116007F2BD3 AS DateTime), 1, 94, N'172.19.17.97', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (60, CAST(0x0000A116008628E8 AS DateTime), CAST(0x0000A11600867BEB AS DateTime), 1, 97, N'172.19.17.97', N'db45ffbf8ae7042a938d4764a0fe2274')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (61, CAST(0x0000A116011363B4 AS DateTime), CAST(0x0000A1160114D8DE AS DateTime), 1, 97, N'172.19.17.97', N'5a00e4bf2de465a8b7d217f265e3c563')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (62, CAST(0x0000A117006D7F73 AS DateTime), CAST(0x0000A117006DB7F5 AS DateTime), 1, 97, N'172.19.17.97', N'35853a0b58b90e6f6ee3cb329a78ce57')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (63, CAST(0x0000A11700A8F1C6 AS DateTime), CAST(0x0000A11700A9008B AS DateTime), 1, 97, N'172.19.17.97', N'c5eb14d02c3f8b8d22e184745bcad1b6')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (64, CAST(0x0000A11700B6FFB2 AS DateTime), CAST(0x0000A11700B708A5 AS DateTime), 1, 97, N'172.19.17.97', N'0b1dc08d3cac07ea0c8fc3e4d23853c8')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (65, CAST(0x0000A11701165A00 AS DateTime), CAST(0x0000A11701166D7D AS DateTime), 1, 97, N'172.19.17.97', N'212f0546406c6d27f28c569d3c0f7253')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (66, CAST(0x0000A118007C90F3 AS DateTime), CAST(0x0000A118007CC1B8 AS DateTime), 1, 97, N'172.19.17.97', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (67, CAST(0x0000A11800F96FDA AS DateTime), CAST(0x0000A11800F98C17 AS DateTime), 1, 97, N'172.19.17.97', N'700fb7e3ca8a33c3f8ecafa1fbaf6c6a')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (68, CAST(0x0000A11800FD35B5 AS DateTime), CAST(0x0000A118010229A5 AS DateTime), 1, 97, N'172.19.17.97', N'087ef1f14436cedf715e92f38930feab')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (69, CAST(0x0000A1190066DC05 AS DateTime), CAST(0x0000A1190067307C AS DateTime), 1, 97, N'172.19.17.97', N'd80cacb2536da73120c52e54c0834445')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (70, CAST(0x0000A119006FDBE7 AS DateTime), CAST(0x0000A119007051ED AS DateTime), 1, 97, N'172.19.17.97', N'472aec42e0786b40224b5119db3920a3')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (71, CAST(0x0000A119007063C7 AS DateTime), CAST(0x0000A11900708EF6 AS DateTime), 1, 97, N'172.19.17.97', N'd813747a937bf8a230637e7d1dee33c1')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (72, CAST(0x0000A1190070A49B AS DateTime), CAST(0x0000A1190071D491 AS DateTime), 1, 97, N'172.19.17.97', N'a910982a90af5a8468a15e76a40af27b')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (73, CAST(0x0000A119007C37DB AS DateTime), CAST(0x0000A119007C8A8F AS DateTime), 1, 97, N'172.19.17.97', N'e00f0ab0f0ab92438e06263ace4f00a4')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (74, CAST(0x0000A11900840420 AS DateTime), CAST(0x0000A11900841449 AS DateTime), 1, 97, N'172.19.17.97', N'8988e0033e2bb2efe3566a6a9d23ea81')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (75, CAST(0x0000A11900842005 AS DateTime), CAST(0x0000A119008D6466 AS DateTime), 1, 97, N'172.19.17.97', N'9ec11a312522ef64c617319adde85632')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (76, CAST(0x0000A11900B82009 AS DateTime), CAST(0x0000A11900BEA4A9 AS DateTime), 1, 93, N'172.19.98.192', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (77, CAST(0x0000A11900B971EB AS DateTime), CAST(0x0000A11900BC68D2 AS DateTime), 1, 97, N'172.19.17.97', N'540521bf19955e3f32e37ce010304e5a')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (78, CAST(0x0000A11900BDF33A AS DateTime), CAST(0x0000A11900C0EED0 AS DateTime), 1, 97, N'172.19.17.97', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (79, CAST(0x0000A11900C1DF27 AS DateTime), CAST(0x0000A11900E1C675 AS DateTime), 1, 97, N'172.19.17.97', N'5a98ece4f52fb91ec6cc5f54ce35c2fe')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (80, CAST(0x0000A11A0092C565 AS DateTime), CAST(0x0000A11A009379BD AS DateTime), 1, 97, N'172.19.17.97', N'5ebb0f98a9156f86860ed662e8fbc23a')
INSERT [dbo].[LoginInfo] ([login_id], [login_fecha_inicio], [login_fecha_fin], [login_tipo_id], [login_usuario_id], [login_ip], [login_session_id]) VALUES (81, CAST(0x0000A11C008BCC47 AS DateTime), CAST(0x0000A11C0096B9F7 AS DateTime), 1, 97, N'172.19.17.97', N'b062e9617804675818c489e8a41c350b')
SET IDENTITY_INSERT [dbo].[LoginInfo] OFF
/****** Object:  Table [dbo].[Metodo]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Metodo](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Metodo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Metodo] ON
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (1, N'Load')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (2, N'Start')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (3, N'End')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (4, N'Save')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (5, N'Update')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (6, N'Delete')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (7, N'Create')
INSERT [dbo].[Metodo] ([id], [nombre]) VALUES (8, N'CloseSession')
SET IDENTITY_INSERT [dbo].[Metodo] OFF
/****** Object:  Table [dbo].[MensajeEstado]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MensajeEstado](
	[cod_estado] [varchar](10) NOT NULL,
	[mensaje] [varchar](99) NOT NULL,
	[cod_respuesta_ISO] [varchar](2) NULL,
 CONSTRAINT [PK_Mensaje_Estado] PRIMARY KEY CLUSTERED 
(
	[cod_estado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000000', N'Transaccion Correcta', N'00')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000001', N'Comercio No Existe', N'11')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000002', N'Punto de Venta No Existe', N'N1')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000003', N'Punto de Venta No Activo en Comercio', N'N2')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000004', N'Comercio Inactivo', N'N3')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000005', N'Punto de Venta Inactivo', N'N4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000006', N'Comercio Sin Saldo', N'P3')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000007', N'Producto No Existe', N'12')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000008', N'Tipo de Transacci�n Err�nea', N'57')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000009', N'Longitud Cod. Barras Errronea', N'N5')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000010', N'ID Inicial Cod. Barras Erroneo', N'N6')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000011', N'Convenio Inactivo', N'N7')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS000012', N'Tipo de Transacci�n Inexistente', N'12')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS500000', N'Consulta de Convenio Correcto', N'00')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS600000', N'Consulta de Recarga Correcta', N'00')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS600001', N'Consulta de Recarga Inexistente', N'N8')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS700000', N'Timeout Base de Datos', N'N9')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS710000', N'Error Conectando Base de Datos', N'N0')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS880000', N'Timeout Operador', N'NA')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS890000', N'Error Conectando con el Operador', N'NB')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS930000', N'Error General SP ProcesarInicializacion', N'NC')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS940000', N'Error General SP Postprocesar', N'ND')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS950000', N'Error General SP Preprocesar', N'NE')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS970000', N'Formato Trx. Erroneo', N'30')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS980000', N'Timeout Formateador', N'23')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS990000', N'Formateador Desconectado', N'NF')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS999000', N'Error General Plataforma', N'NG')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000000', N'Recarga Correcta', N'00')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000001', N'Punto de Venta Inactivo', N'06')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000002', N'Usuario Invalido', N'M1')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000003', N'Usuario Inactivo', N'M2')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000004', N'Cliente Inactivo', N'M3')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000005', N'Monto Invalido', N'13')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000006', N'Consec. Factura No Disponible', N'M4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000007', N'Operador Invalido', N'M5')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000008', N'Long. Producto Invalida', N'M6')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000009', N'Usuario Invalido', N'M7')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV0000-1', N'Timeout Operador', N'M8')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000010', N'Dispositivo Invalido', N'M9')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000011', N'Operador Inactivo', N'M0')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000012', N'Punto No Asociado a Red', N'MA')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000013', N'Estructura Interna Invalida', N'MB')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000014', N'Numero Invalido', N'78')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000015', N'Clave Invalida', N'55')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000016', N'Estructura Trama Invalida', N'31')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000017', N'Estructura Trama Invalida', N'31')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000018', N'Subdist. No Asignado a Red', N'MC')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000019', N'Estructura Trama Invalida', N'30')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000020', N'Celular No es Prepago', N'14')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000022', N'Saldo Insuficiente', N'MD')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000025', N'Abonado con Deuda Pendiente por Adelanto Saldo', N'ME')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000026', N'Transaccion Duplicada', N'MF')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000027', N'Error en Asignacion Transaccion', N'MG')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000028', N'Estructura Trama Invalida', N'30')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000029', N'Producto No Habilitado', N'12')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000030', N'Estructura Interna Invalida', N'31')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000031', N'Trx. Rechazada Pruebas', N'NM')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000032', N'Usuario No distribuidor', N'MH')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000033', N'Cuentas Incompletas en Contabilidad', N'MI')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000034', N'Usuario Invalido', N'M1')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000035', N'Dispositivo Invalido', N'15')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000036', N'Operador Bloqueado', N'MJ')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000037', N'Valor Invalido', N'13')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000038', N'Mapa de Cuentas No Creado', N'MK')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000039', N'Saldo Insuficiente', N'MD')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000040', N'Distribuidor Invalido', N'ML')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000041', N'Consignacion Ya registrada', N'MM')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000042', N'Punto Invalido', N'MN')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000046', N'Error Conectando Operador', N'MO')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000047', N'Error Timeout Movilred', N'MP')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000050', N'Timeout Proveedor', N'MQ')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000051', N'Numero Inactivo o Bloqueado en Operador', N'MR')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000052', N'Tarjeta Activada o en Uso', N'NL')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000054', N'Transaccion en Proceso', N'NK')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000055', N'Valor M�ximo de Venta Alcanzado', N'MS')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000057', N'Celular sin Recarga �ltimo Mes', N'MT')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000058', N'Abonado con Deuda Pendiente por Adelanto Saldo', N'ME')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000079', N'Saldo Incorrecto', N'NJ')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000080', N'Transaccion Duplicada', N'MF')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000081', N'Error Base de Datos Movilred', N'MU')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000082', N'No Existe Modelo de Negocio', N'MV')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000083', N'Venta No Habilitada para Dispositivo', N'NI')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000085', N'Recarga NO Autorizada', N'MW')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000090', N'Inconsistencia Parametros Proveedor', N'MX')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000091', N'Inconsistencia Parametros Proveedor', N'MY')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000092', N'Error Formato Respuesta Recarga', N'MZ')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000098', N'Error General Movilred', N'96')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV000099', N'Error General Movilred', N'NH')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV0000A1', N'Canal Desconectado o en Modo Logoff', N'91')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV0000A2', N'Canal en Modo Logon', N'92')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV0000A3', N'Timeout Alcanzado.', N'68')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV0000A4', N'Trx. Recibida Fuera de Tiempo', N'A4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100000', N'Pago de Factura Correcto', N'00')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV1000A1', N'Canal Desconectado o en Modo Logoff', N'91')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV1000A2', N'Canal en Modo Logon', N'92')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV1000A3', N'Timeout Alcanzado.', N'68')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV1000A4', N'Trx. Recibida Fuera de Tiempo', N'A4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV500000', N'Reverso Correcto', N'00')
/****** Object:  Table [dbo].[ConfiguracionServidor]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfiguracionServidor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[codigo_instancia] [smallint] NOT NULL,
	[nombre_parametro] [varchar](50) NOT NULL,
	[descripcion_parametro] [varchar](100) NULL,
	[dato] [varchar](250) NOT NULL,
 CONSTRAINT [PK_Configuracion_Servidor] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ConfiguracionServidor] ON
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (1, 0, N'LocalPort', N'Puerto de Atenci�n Local para el Servidor', N'1975')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (2, 0, N'LocalTextLog', N'Flag para Activar Log de Texto Plano en el Servidor', N'1')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (3, 0, N'minThreadPool', N'N�mero M�nimo de Hilos Servidor', N'50')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (4, 0, N'maxThreadPool', N'N�mero M�ximo de Hilos Servidor', N'100')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (17, 0, N'IPMovilredFormatter', N'IP Formateador Movistar Solidda', N'127.0.0.1')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (18, 0, N'PortMovilredFormatter', N'Puerto TCP Formateador Movistar Solidda', N'6661')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (19, 0, N'totalTimeoutMovilredFormatter', N'Tiempo de Espera Total para Transacciones de Recargas Movistar Solidda', N'61000')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (20, 0, N'TotalTimeoutJTranServer', N'Tiempo de Espera Total (IDLE) para una Conexi�n atendida por el Servidor', N'62000')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (21, 0, N'LocalXMLLog', N'Flag para Activar Log XML en el Servidor', N'0')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (24, 0, N'isomuxNameMovilredFormatter', N'Nombre del ISOMux Movilred Tigo encargado de administrar la transacci�n', N'ISOMUX_MOVILRED')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (25, 0, N'maxAttempsConnection', N'M�ximo Intento de Conexiones del POS', N'3')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (26, 0, N'minSaleAmount', N'M�nimo Valor de Venta', N'1000')
INSERT [dbo].[ConfiguracionServidor] ([id], [codigo_instancia], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (27, 0, N'maxSaleAmount', N'M�ximo Valor de Venta', N'100000')
SET IDENTITY_INSERT [dbo].[ConfiguracionServidor] OFF
/****** Object:  Table [dbo].[ConfiguracionFormateador]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConfiguracionFormateador](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre_formateador] [varchar](50) NOT NULL,
	[nombre_parametro] [varchar](50) NOT NULL,
	[descripcion_parametro] [varchar](100) NULL,
	[dato] [varchar](250) NOT NULL,
 CONSTRAINT [PK_Configuracion_Formateador] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ConfiguracionFormateador] ON
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (39, N'MOVILRED_FORMATTER', N'LocalPort', N'Puerto de Atenci�n Local para el Formateador', N'6661')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (40, N'MOVILRED_FORMATTER', N'LocalTextLog', N'Flag para Activar Log de Texto del Formateador', N'1')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (41, N'MOVILRED_FORMATTER', N'minThreadPool', N'N�mero M�nimo de Hilos', N'50')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (42, N'MOVILRED_FORMATTER', N'maxThreadPool', N'N�mero M�ximo de Hilos', N'100')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (43, N'MOVILRED_FORMATTER', N'destinationIP1', N'IP Servidor de Recargas Solidda', N'192.168.156.77')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (44, N'MOVILRED_FORMATTER', N'destinationPort1', N'Puerto TCP Servidor de Recargas Solidda', N'49394')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (45, N'MOVILRED_FORMATTER', N'destinationTimeout1', N'Tiempo de Espera x Transacci�n Recargas', N'60000')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (46, N'MOVILRED_FORMATTER', N'NIT', N'Nit Empresa Gestor', N'950226658')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (47, N'MOVILRED_FORMATTER', N'user', N'Usuario de Ventas Asignado por Movilred', N'600111')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (48, N'MOVILRED_FORMATTER', N'password', N'Password de Ventas Asignado por Movilred', N'111600')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (56, N'MOVILRED_FORMATTER', N'isomuxName', N'Nombre asignado al Formateador para efectos de Ruteo', N'ISOMUX_MOVILRED')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (57, N'MOVILRED_FORMATTER', N'destinationIP2', N'IP Servidor de PSP Solidda', N'127.0.0.1')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (58, N'MOVILRED_FORMATTER', N'destinationPort2', N'Puerto TCP Servidor de PSP Solidda', N'9691')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (59, N'MOVILRED_FORMATTER', N'destinationTimeout2', N'Tiempo de Espera x Transacci�n PSP', N'30000')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (60, N'MOVILRED_FORMATTER', N'destinationIP3', N'IP Servidor de Giros Solidda', N'127.0.0.3')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (61, N'MOVILRED_FORMATTER', N'destinationPort3', N'Puerto TCP Servidor de Giros Solidda', N'9692')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (62, N'MOVILRED_FORMATTER', N'destinationTimeout3', N'Tiempo de Espera x Transacci�n Giros', N'30000')
INSERT [dbo].[ConfiguracionFormateador] ([id], [nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (63, N'MOVILRED_FORMATTER', N'LocalXMLLog', N'Flag para Activar Log XML del Formateador', N'0')
SET IDENTITY_INSERT [dbo].[ConfiguracionFormateador] OFF
/****** Object:  Table [dbo].[ConcMovilredINTemp]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConcMovilredINTemp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tra_operador] [varchar](30) NULL,
	[tra_fecha] [varchar](10) NULL,
	[tra_hora] [varchar](8) NULL,
	[tra_id] [bigint] NULL,
	[tra_Auth_Ope] [bigint] NULL,
	[tra_valor] [varchar](8) NULL,
	[tra_celular] [varchar](12) NULL,
	[tra_ExternalReference] [bigint] NULL,
	[tra_Time] [varchar](8) NULL,
 CONSTRAINT [PK_CONC_CONRED_IN_TEMP] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ConcMovilredINTemp] ON
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (1, N'COMCEL', N'09/24/2012', N'08:11:23', 579578153, 30767334, N'1000', N'3102274288', 13302, N'147')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (2, N'COMCEL', N'09/24/2012', N'08:11:33', 579578155, 30767334, N'1000', N'3100937028', 13309, N'149')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (3, N'COMCEL', N'09/24/2012', N'08:11:37', 579578157, 30767334, N'1000', N'3100362296', 13314, N'174')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (4, N'COMCEL', N'09/24/2012', N'08:29:07', 579578159, 30767334, N'1000', N'3100687965', 13316, N'174')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (5, N'COMCEL', N'09/24/2012', N'08:29:10', 579578161, 30767334, N'1000', N'3101183245', 13320, N'171')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (6, N'COMCEL', N'09/24/2012', N'08:29:13', 579578163, 30767334, N'5000', N'3104316498', 13323, N'168')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (7, N'COMCEL', N'09/24/2012', N'09:33:40', 579578168, 30767334, N'1000', N'3103751867', 13330, N'154')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (8, N'COMCEL', N'09/24/2012', N'10:12:50', 579578170, 30767334, N'1000', N'3100955597', 13342, N'149')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (9, N'COMCEL', N'09/24/2012', N'10:13:04', 579578172, 30767334, N'3000', N'3100962929', 13345, N'181')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (10, N'COMCEL', N'09/24/2012', N'10:13:08', 579578174, 30767334, N'2000', N'3100082777', 13350, N'167')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (11, N'COMCEL', N'09/24/2012', N'10:16:24', 579578176, 30767334, N'2000', N'3100069999', 13355, N'153')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (12, N'COMCEL', N'09/24/2012', N'10:16:28', 579578178, 30767334, N'5000', N'3103313290', 13361, N'182')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (13, N'COMCEL', N'09/24/2012', N'14:15:47', 579578875, 30767334, N'5000', N'3102565409', 13368, N'216')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (14, N'COMCEL', N'09/24/2012', N'17:05:57', 579578885, 30767334, N'2000', N'3100825389', 13403, N'311')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (15, N'COMCEL', N'09/24/2012', N'17:06:02', 579578887, 30767334, N'1000', N'3104873780', 13410, N'155')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (16, N'COMCEL', N'09/24/2012', N'17:06:22', 579578889, 30767334, N'5000', N'3104248647', 13414, N'157')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (17, N'COMCEL', N'09/24/2012', N'17:06:24', 579578891, 30767334, N'1000', N'3104792276', 13417, N'178')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (18, N'COMCEL', N'09/24/2012', N'17:06:27', 579578893, 30767334, N'1000', N'3101069372', 13421, N'164')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (19, N'COMCEL', N'09/24/2012', N'17:06:44', 579578895, 30767334, N'10000', N'3103213554', 13423, N'162')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (20, N'COMCEL', N'09/24/2012', N'17:06:50', 579578897, 30767334, N'5000', N'3100864990', 13430, N'160')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (21, N'COMCEL', N'09/24/2012', N'17:07:04', 579578899, 30767334, N'1000', N'3103625974', 13434, N'152')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (22, N'COMCEL', N'09/24/2012', N'17:07:06', 579578901, 30767334, N'1000', N'3103601395', 13437, N'156')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (23, N'COMCEL', N'09/24/2012', N'17:07:26', 579578903, 30767334, N'1000', N'3104068209', 13450, N'174')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (24, N'COMCEL', N'09/24/2012', N'17:07:48', 579578905, 30767334, N'5000', N'3102860393', 13456, N'168')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (25, N'COMCEL', N'09/24/2012', N'17:07:51', 579578907, 30767334, N'3000', N'3101301741', 13461, N'145')
INSERT [dbo].[ConcMovilredINTemp] ([id], [tra_operador], [tra_fecha], [tra_hora], [tra_id], [tra_Auth_Ope], [tra_valor], [tra_celular], [tra_ExternalReference], [tra_Time]) VALUES (26, N'COMCEL', N'09/24/2012', N'17:08:04', 579578909, 30767334, N'5000', N'3101130886', 13464, N'148')
SET IDENTITY_INSERT [dbo].[ConcMovilredINTemp] OFF
/****** Object:  Table [dbo].[ConcConredINTemp]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConcConredINTemp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tra_id] [bigint] NULL,
	[tra_celular] [varchar](12) NULL,
	[tra_fecha] [varchar](10) NULL,
	[tra_hora] [varchar](8) NULL,
	[tra_valor] [varchar](8) NULL,
	[tra_operador] [varchar](30) NULL,
 CONSTRAINT [PK_CONC_CONRED_MOVILRED_IN_TEMP] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstadoUsuario]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoUsuario](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EstadoUsuario] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[EstadoUsuario] ON
INSERT [dbo].[EstadoUsuario] ([id], [descripcion]) VALUES (1, N'Activo')
INSERT [dbo].[EstadoUsuario] ([id], [descripcion]) VALUES (2, N'Inactivo')
SET IDENTITY_INSERT [dbo].[EstadoUsuario] OFF
/****** Object:  Table [dbo].[EstadoSimCard]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoSimCard](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EstadoSimCard] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[EstadoSimCard] ON
INSERT [dbo].[EstadoSimCard] ([id], [descripcion]) VALUES (1, N'Activa')
INSERT [dbo].[EstadoSimCard] ([id], [descripcion]) VALUES (2, N'Da�ada')
INSERT [dbo].[EstadoSimCard] ([id], [descripcion]) VALUES (3, N'Sin Se�al')
SET IDENTITY_INSERT [dbo].[EstadoSimCard] OFF
/****** Object:  Table [dbo].[EstadoPuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoPuntoVenta](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EstadoPuntoVenta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[EstadoPuntoVenta] ON
INSERT [dbo].[EstadoPuntoVenta] ([id], [nombre]) VALUES (1, N'Activo')
INSERT [dbo].[EstadoPuntoVenta] ([id], [nombre]) VALUES (2, N'Inactivo')
SET IDENTITY_INSERT [dbo].[EstadoPuntoVenta] OFF
/****** Object:  Table [dbo].[EstadoProducto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoProducto](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EstadoProducto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[EstadoProducto] ON
INSERT [dbo].[EstadoProducto] ([id], [nombre]) VALUES (1, N'Activo')
INSERT [dbo].[EstadoProducto] ([id], [nombre]) VALUES (2, N'Inactivo')
SET IDENTITY_INSERT [dbo].[EstadoProducto] OFF
/****** Object:  Table [dbo].[EstadoEntidad]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoEntidad](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EstadoEntidad] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[EstadoEntidad] ON
INSERT [dbo].[EstadoEntidad] ([id], [nombre]) VALUES (1, N'Activo')
INSERT [dbo].[EstadoEntidad] ([id], [nombre]) VALUES (2, N'Inactivo')
INSERT [dbo].[EstadoEntidad] ([id], [nombre]) VALUES (3, N'Retirado')
SET IDENTITY_INSERT [dbo].[EstadoEntidad] OFF
/****** Object:  Table [dbo].[EstadoCanalMovilred]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoCanalMovilred](
	[id_canal] [smallint] NOT NULL,
	[estado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_EstadoCanalMovilred] PRIMARY KEY CLUSTERED 
(
	[id_canal] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[EstadoCanalMovilred] ([id_canal], [estado]) VALUES (1, N'TCPUP')
INSERT [dbo].[EstadoCanalMovilred] ([id_canal], [estado]) VALUES (2, N'TCPDOWN')
INSERT [dbo].[EstadoCanalMovilred] ([id_canal], [estado]) VALUES (3, N'TCPDOWN')
/****** Object:  Table [dbo].[EstadoCanalJNTS]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoCanalJNTS](
	[estado] [varchar](50) NOT NULL,
 CONSTRAINT [PK_EstadoCanalJNTS] PRIMARY KEY CLUSTERED 
(
	[estado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[EstadoCanalJNTS] ([estado]) VALUES (N'TCPDOWN')
/****** Object:  Table [dbo].[EstadoAviso]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoAviso](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_EstadoAviso] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[EstadoAviso] ON
INSERT [dbo].[EstadoAviso] ([id], [nombre]) VALUES (1, N'Pendiente')
INSERT [dbo].[EstadoAviso] ([id], [nombre]) VALUES (2, N'Procesado')
INSERT [dbo].[EstadoAviso] ([id], [nombre]) VALUES (3, N'Anulado')
SET IDENTITY_INSERT [dbo].[EstadoAviso] OFF
/****** Object:  Table [dbo].[Banco]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banco](
	[ban_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ban_nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Banco] PRIMARY KEY CLUSTERED 
(
	[ban_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CarrierSimCard]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CarrierSimCard](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_CarrierSimCard] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Departamento]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Departamento](
	[id] [bigint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Departamento] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Departamento] ([id], [nombre]) VALUES (24, N'Cundinamarca')
/****** Object:  Table [dbo].[TipoOpcion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoOpcion](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoOpcion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoOpcion] ON
INSERT [dbo].[TipoOpcion] ([id], [descripcion]) VALUES (1, N'Opci�n Principal')
INSERT [dbo].[TipoOpcion] ([id], [descripcion]) VALUES (2, N'Opci�n Segundo Grado')
INSERT [dbo].[TipoOpcion] ([id], [descripcion]) VALUES (3, N'Opci�n Tercer Grado')
SET IDENTITY_INSERT [dbo].[TipoOpcion] OFF
/****** Object:  Table [dbo].[TipoNivelMensaje]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoNivelMensaje](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoNivelMensaje] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoNivelMensaje] ON
INSERT [dbo].[TipoNivelMensaje] ([id], [descripcion]) VALUES (1, N'INFO')
INSERT [dbo].[TipoNivelMensaje] ([id], [descripcion]) VALUES (2, N'WARNING')
INSERT [dbo].[TipoNivelMensaje] ([id], [descripcion]) VALUES (3, N'ERROR')
SET IDENTITY_INSERT [dbo].[TipoNivelMensaje] OFF
/****** Object:  Table [dbo].[TipoNivelEntidad]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoNivelEntidad](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoNivelEntidad] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoNivelEntidad] ON
INSERT [dbo].[TipoNivelEntidad] ([id], [nombre]) VALUES (1, N'Red de Pagos')
INSERT [dbo].[TipoNivelEntidad] ([id], [nombre]) VALUES (2, N'Comercio Conred')
INSERT [dbo].[TipoNivelEntidad] ([id], [nombre]) VALUES (3, N'Corresponsal')
INSERT [dbo].[TipoNivelEntidad] ([id], [nombre]) VALUES (4, N'Comercio Movilred')
INSERT [dbo].[TipoNivelEntidad] ([id], [nombre]) VALUES (5, N'Comercio TiendaTek')
SET IDENTITY_INSERT [dbo].[TipoNivelEntidad] OFF
/****** Object:  Table [dbo].[TipoMovimientoCuenta]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoMovimientoCuenta](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoMovimientoCuenta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoMovimientoCuenta] ON
INSERT [dbo].[TipoMovimientoCuenta] ([id], [nombre]) VALUES (1, N'Asignaci�n Saldo a Comercio')
INSERT [dbo].[TipoMovimientoCuenta] ([id], [nombre]) VALUES (2, N'Retiro Saldo a Comercio')
INSERT [dbo].[TipoMovimientoCuenta] ([id], [nombre]) VALUES (3, N'Comisi�n Comercio')
INSERT [dbo].[TipoMovimientoCuenta] ([id], [nombre]) VALUES (4, N'Ajuste - Sin Comisi�n')
SET IDENTITY_INSERT [dbo].[TipoMovimientoCuenta] OFF
/****** Object:  Table [dbo].[TipoMensajeISO]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoMensajeISO](
	[tipo_mensaje] [nchar](4) NOT NULL,
	[codigo_proceso] [nchar](6) NOT NULL,
	[descripcion] [varchar](100) NULL,
 CONSTRAINT [PK_TipoMensaje] PRIMARY KEY CLUSTERED 
(
	[tipo_mensaje] ASC,
	[codigo_proceso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'0100', N'360000', N'Transacci�n Consulta de Params. X Convenio')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'0100', N'370000', N'Transacci�n Consulta de Trx. Recarga')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'0200', N'150000', N'Transacci�n Pago de Facturas')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'0200', N'380000', N'Transacci�n Venta de Recarga Electr�nica')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'0420', N'150000', N'Transacci�n de Reverso Pago de Facturas')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'9100', N'360000', N'Error Trama ISO - Transacci�n Consulta de Params. X Convenio')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'9100', N'370000', N'Error Trama ISO - Transacci�n Consulta de Trx. Recarga')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'9200', N'150000', N'Error Trama ISO - Transacci�n Pago de Facturas')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'9200', N'380000', N'Error Trama ISO - Transacci�n Venta de Recarga Electr�nica')
INSERT [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso], [descripcion]) VALUES (N'9420', N'150000', N'Error Trama ISO - Transacci�n Reverso Pago de Facturas')
/****** Object:  Table [dbo].[TipoLoginEstado]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoLoginEstado](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TipoLoginEstado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoLoginEstado] ON
INSERT [dbo].[TipoLoginEstado] ([id], [descripcion]) VALUES (1, N'Inicio de Sesi�n Correcto')
INSERT [dbo].[TipoLoginEstado] ([id], [descripcion]) VALUES (2, N'Error de Nombre Usuario')
INSERT [dbo].[TipoLoginEstado] ([id], [descripcion]) VALUES (3, N'Error de Clave')
INSERT [dbo].[TipoLoginEstado] ([id], [descripcion]) VALUES (4, N'Sesi�n Ya Iniciada')
INSERT [dbo].[TipoLoginEstado] ([id], [descripcion]) VALUES (5, N'Intento Acceso M�dulo Err�neo')
INSERT [dbo].[TipoLoginEstado] ([id], [descripcion]) VALUES (6, N'Usuario Inactivo')
SET IDENTITY_INSERT [dbo].[TipoLoginEstado] OFF
/****** Object:  Table [dbo].[TipoLog]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoLog](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoLog] ON
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (1, N'Error cr�tico')
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (2, N'Error de aplicaci�n')
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (3, N'Acceso denegado')
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (4, N'Mensaje informativo')
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (5, N'Acceso permitido')
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (6, N'Inicio de sesi�n')
INSERT [dbo].[TipoLog] ([id], [descripcion]) VALUES (7, N'Fin de sesi�n')
SET IDENTITY_INSERT [dbo].[TipoLog] OFF
/****** Object:  Table [dbo].[TipoIdentificacion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoIdentificacion](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoIdentificacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoIdentificacion] ON
INSERT [dbo].[TipoIdentificacion] ([id], [nombre]) VALUES (1, N'C�dula de Ciudadan�a')
INSERT [dbo].[TipoIdentificacion] ([id], [nombre]) VALUES (2, N'Tarjeta de Identidad')
INSERT [dbo].[TipoIdentificacion] ([id], [nombre]) VALUES (3, N'C�dula de Extranjer�a')
SET IDENTITY_INSERT [dbo].[TipoIdentificacion] OFF
/****** Object:  Table [dbo].[TipoCuenta]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoCuenta](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoCuenta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoCuenta] ON
INSERT [dbo].[TipoCuenta] ([id], [nombre]) VALUES (1, N'Cuenta Corriente')
INSERT [dbo].[TipoCuenta] ([id], [nombre]) VALUES (2, N'Cuenta de Ahorros')
SET IDENTITY_INSERT [dbo].[TipoCuenta] OFF
/****** Object:  Table [dbo].[TipoComunicacion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoComunicacion](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoComunicacion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoComunicacion] ON
INSERT [dbo].[TipoComunicacion] ([id], [nombre]) VALUES (1, N'GPRS')
INSERT [dbo].[TipoComunicacion] ([id], [nombre]) VALUES (2, N'H2H')
INSERT [dbo].[TipoComunicacion] ([id], [nombre]) VALUES (3, N'WEB')
SET IDENTITY_INSERT [dbo].[TipoComunicacion] OFF
/****** Object:  Table [dbo].[TipoCausal]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoCausal](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoCausal] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoCausal] ON
INSERT [dbo].[TipoCausal] ([id], [nombre]) VALUES (1, N'Baja Productividad')
INSERT [dbo].[TipoCausal] ([id], [nombre]) VALUES (2, N'Da�o')
INSERT [dbo].[TipoCausal] ([id], [nombre]) VALUES (3, N'Robo')
SET IDENTITY_INSERT [dbo].[TipoCausal] OFF
/****** Object:  Table [dbo].[TablaInicializacion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TablaInicializacion](
	[tab_id] [varchar](2) NOT NULL,
	[tab_descripcion] [varchar](100) NULL,
	[tab_max_repeticion] [smallint] NOT NULL,
 CONSTRAINT [PK_TablaInicializacion] PRIMARY KEY CLUSTERED 
(
	[tab_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TablaInicializacion] ([tab_id], [tab_descripcion], [tab_max_repeticion]) VALUES (N'01', N'Tabla de Configuraci�n', 1)
INSERT [dbo].[TablaInicializacion] ([tab_id], [tab_descripcion], [tab_max_repeticion]) VALUES (N'02', N'Tabla de Productos de Recarga', 10)
INSERT [dbo].[TablaInicializacion] ([tab_id], [tab_descripcion], [tab_max_repeticion]) VALUES (N'03', N'Tabla de Productos de Pago de Facturas', 10)
INSERT [dbo].[TablaInicializacion] ([tab_id], [tab_descripcion], [tab_max_repeticion]) VALUES (N'XX', N'Error ', 0)
INSERT [dbo].[TablaInicializacion] ([tab_id], [tab_descripcion], [tab_max_repeticion]) VALUES (N'ZZ', N'Completo', 0)
/****** Object:  Table [dbo].[TransaccionAdministrativa]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransaccionAdministrativa](
	[tra_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tra_fecha_ini] [datetime] NOT NULL,
	[tra_fecha_fin] [datetime] NULL,
	[tra_interfaz_origen] [varchar](20) NOT NULL,
	[tra_interfaz_destino] [varchar](20) NOT NULL,
	[tra_tipo_mensaje_req] [nchar](4) NOT NULL,
	[tra_tipo_mensaje_resp] [nchar](4) NULL,
	[tra_stan] [int] NULL,
	[tra_terminal_id] [varchar](10) NULL,
	[tra_campo_p70] [nchar](3) NULL,
	[tra_cod_respuesta] [varchar](2) NULL,
 CONSTRAINT [PK_TransaccionAdministrativa] PRIMARY KEY CLUSTERED 
(
	[tra_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TransaccionAdministrativa] ON
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (1, CAST(0x0000A111009BC704 AS DateTime), CAST(0x0000A111009BC710 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 414703, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (2, CAST(0x0000A111009BD117 AS DateTime), CAST(0x0000A111009BD119 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 216553, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (3, CAST(0x0000A111009C15C5 AS DateTime), CAST(0x0000A111009C15C6 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 814361, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (4, CAST(0x0000A111009C1B31 AS DateTime), CAST(0x0000A111009C1B33 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 304688, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (5, CAST(0x0000A116007E2604 AS DateTime), CAST(0x0000A116007E2614 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 981719, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (6, CAST(0x0000A1190070E11F AS DateTime), CAST(0x0000A1190070E131 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 720581, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (7, CAST(0x0000A1190070E92E AS DateTime), CAST(0x0000A1190070E949 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 321381, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (8, CAST(0x0000A119008D15DA AS DateTime), CAST(0x0000A119008D15DD AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 428894, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (9, CAST(0x0000A119008D20C6 AS DateTime), CAST(0x0000A119008D20C8 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 493805, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (10, CAST(0x0000A11A0092E234 AS DateTime), CAST(0x0000A11A0092E23D AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 781616, N'        ', N'   ', N'00')
INSERT [dbo].[TransaccionAdministrativa] ([tra_id], [tra_fecha_ini], [tra_fecha_fin], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_stan], [tra_terminal_id], [tra_campo_p70], [tra_cod_respuesta]) VALUES (11, CAST(0x0000A11A0092EE4E AS DateTime), CAST(0x0000A11A0092EE50 AS DateTime), N'CLIENTE', N'CONRED', N'0800', N'0810', 409027, N'        ', N'   ', N'00')
SET IDENTITY_INSERT [dbo].[TransaccionAdministrativa] OFF
/****** Object:  Table [dbo].[TipoRubroEntidad]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoRubroEntidad](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_RubroEntidad] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoRubroEntidad] ON
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (1, N'Otros')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (2, N'Locutorio')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (3, N'Polirubro')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (4, N'Maxikiosco')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (5, N'Kiosco')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (6, N'Kiosco de Diarios y Revistas')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (7, N'Almacen')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (8, N'Kiosco y Fotocopias')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (9, N'Estaci�n de Servicio')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (10, N'Telecomunicaciones')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (11, N'Cyber')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (12, N'Bar')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (13, N'Colegios')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (14, N'Universidades')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (15, N'Institutos de formacion no integral')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (16, N'Cafes de internet')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (17, N'Cabinas telefonicas')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (18, N'Estaciones de transmilenio')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (19, N'Estaciones de servicio o gasolina')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (20, N'Cigarrerias')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (21, N'Edificios administrativos')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (22, N'Conjuntos residenciales')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (23, N'Supermercados pymes')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (24, N'Papelerias')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (25, N'Comidas rapidas')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (26, N'Centros medicos')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (27, N'Clinicas')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (28, N'Hospitales')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (29, N'Parques de recreacion')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (30, N'Empresas de servicio de correo')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (31, N'Droguerias')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (32, N'Eventos')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (33, N'Clubes sociales')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (34, N'Hoteles')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (35, N'Empresas de impuestos')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (36, N'Empresas de servicios publico y privado')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (37, N'Terminal de transporte')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (38, N'Empresas de transporte local y municipal')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (39, N'Grandes superficies')
INSERT [dbo].[TipoRubroEntidad] ([id], [nombre]) VALUES (40, N'Cadena de almacenes')
SET IDENTITY_INSERT [dbo].[TipoRubroEntidad] OFF
/****** Object:  Table [dbo].[TipoRegimen]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoRegimen](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoRegimen] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoRegimen] ON
INSERT [dbo].[TipoRegimen] ([id], [nombre]) VALUES (1, N'R�gimen Com�n')
INSERT [dbo].[TipoRegimen] ([id], [nombre]) VALUES (2, N'R�gimen Simplificado')
INSERT [dbo].[TipoRegimen] ([id], [nombre]) VALUES (3, N'Gran Contribuyente')
SET IDENTITY_INSERT [dbo].[TipoRegimen] OFF
/****** Object:  Table [dbo].[TipoPuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoPuntoVenta](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoPuntoVenta] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoPuntoVenta] ON
INSERT [dbo].[TipoPuntoVenta] ([id], [nombre]) VALUES (1, N'Celular')
INSERT [dbo].[TipoPuntoVenta] ([id], [nombre]) VALUES (2, N'Web')
INSERT [dbo].[TipoPuntoVenta] ([id], [nombre]) VALUES (3, N'POS')
INSERT [dbo].[TipoPuntoVenta] ([id], [nombre]) VALUES (4, N'H2H')
SET IDENTITY_INSERT [dbo].[TipoPuntoVenta] OFF
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
 CONSTRAINT [PK_TipoProducto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TipoProducto] ON
INSERT [dbo].[TipoProducto] ([id], [nombre]) VALUES (1, N'Recargas')
INSERT [dbo].[TipoProducto] ([id], [nombre]) VALUES (2, N'Pago de Facturas')
SET IDENTITY_INSERT [dbo].[TipoProducto] OFF
/****** Object:  Table [dbo].[TransaccionXReverso]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransaccionXReverso](
	[tra_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tra_fecha_inicial] [datetime] NOT NULL,
	[tra_fecha_fin] [datetime] NULL,
	[tra_interfaz_origen] [varchar](20) NOT NULL,
	[tra_interfaz_destino] [varchar](20) NOT NULL,
	[tra_tipo_mensaje_req] [nchar](4) NOT NULL,
	[tra_tipo_mensaje_resp] [nchar](4) NULL,
	[tra_codigo_proceso] [nchar](6) NOT NULL,
	[tra_codigo_respuesta] [varchar](2) NULL,
	[tra_stan] [int] NOT NULL,
	[tra_rrn] [varchar](20) NOT NULL,
	[tra_auth_id_p38] [varchar](6) NULL,
	[tra_codigo_estado_final] [varchar](10) NULL,
	[tra_terminal_id] [varchar](20) NOT NULL,
	[tra_comercio_id] [varchar](20) NOT NULL,
	[tra_comercio_saldo_inicial] [money] NULL,
	[tra_valor] [money] NULL,
	[tra_comercio_saldo_final] [money] NULL,
	[tra_referencia_cliente_1] [varchar](50) NULL,
	[tra_referencia_cliente_2] [varchar](50) NULL,
	[tra_referencia_cliente_3] [varchar](50) NULL,
	[tra_datos_adicionales] [varchar](100) NULL,
	[tra_producto_id] [varchar](10) NULL,
	[tra_ip_destino] [varchar](20) NULL,
	[tra_puerto_atencion_destino] [int] NULL,
	[tra_id_original] [bigint] NULL,
 CONSTRAINT [PK_TransaccionXReverso] PRIMARY KEY CLUSTERED 
(
	[tra_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TransaccionInicializacion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TransaccionInicializacion](
	[tra_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tra_fecha] [datetime] NOT NULL,
	[tra_interfaz_origen] [varchar](20) NOT NULL,
	[tra_interfaz_destino] [varchar](20) NOT NULL,
	[tra_tipo_mensaje_req] [nchar](4) NOT NULL,
	[tra_tipo_mensaje_resp] [nchar](4) NOT NULL,
	[tra_codigo_proceso_req] [nchar](6) NOT NULL,
	[tra_codigo_proceso_resp] [nchar](6) NOT NULL,
	[tra_stan] [int] NOT NULL,
	[tra_terminal_id] [varchar](10) NOT NULL,
	[tra_cod_respuesta] [varchar](2) NOT NULL,
	[tra_codigo_estado_final] [varchar](10) NOT NULL,
	[tra_tabla_inicializacion_id] [varchar](2) NOT NULL,
	[tra_index_registro_enviado] [int] NOT NULL,
	[tra_max_registros_tabla] [int] NOT NULL,
	[tra_ip_cliente] [varchar](20) NULL,
	[tra_puerto_atencion] [int] NULL,
 CONSTRAINT [PK_TransaccionInicializacion] PRIMARY KEY CLUSTERED 
(
	[tra_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TransaccionInicializacion] ON
INSERT [dbo].[TransaccionInicializacion] ([tra_id], [tra_fecha], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_codigo_proceso_req], [tra_codigo_proceso_resp], [tra_stan], [tra_terminal_id], [tra_cod_respuesta], [tra_codigo_estado_final], [tra_tabla_inicializacion_id], [tra_index_registro_enviado], [tra_max_registros_tabla], [tra_ip_cliente], [tra_puerto_atencion]) VALUES (1, CAST(0x0000A110008BB8DF AS DateTime), N'CLIENTE', N'MIDDLEWARE', N'0800', N'0810', N'930000', N'930001', 613, N'64900010', N'N1', N'JTS000002', N'01', 0, 1, N'10.59.180.55', 1965)
INSERT [dbo].[TransaccionInicializacion] ([tra_id], [tra_fecha], [tra_interfaz_origen], [tra_interfaz_destino], [tra_tipo_mensaje_req], [tra_tipo_mensaje_resp], [tra_codigo_proceso_req], [tra_codigo_proceso_resp], [tra_stan], [tra_terminal_id], [tra_cod_respuesta], [tra_codigo_estado_final], [tra_tabla_inicializacion_id], [tra_index_registro_enviado], [tra_max_registros_tabla], [tra_ip_cliente], [tra_puerto_atencion]) VALUES (2, CAST(0x0000A110008C470D AS DateTime), N'CLIENTE', N'MIDDLEWARE', N'0800', N'0810', N'930000', N'930001', 614, N'64900010', N'N1', N'JTS000002', N'01', 0, 1, N'10.59.180.55', 1966)
SET IDENTITY_INSERT [dbo].[TransaccionInicializacion] OFF
/****** Object:  Table [dbo].[TipoPlanSimCard]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoPlanSimCard](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[descripcion] [varchar](100) NOT NULL,
	[tamanho_plan] [varchar](50) NOT NULL,
	[carrier_sim_card_id] [bigint] NOT NULL,
 CONSTRAINT [PK_TipoPlanSimCard] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transaccion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transaccion](
	[tra_id] [bigint] IDENTITY(1,1) NOT NULL,
	[tra_fecha_inicial] [datetime] NOT NULL,
	[tra_fecha_fin] [datetime] NULL,
	[tra_interfaz_origen] [varchar](20) NOT NULL,
	[tra_interfaz_destino] [varchar](20) NOT NULL,
	[tra_tipo_mensaje_req] [nchar](4) NULL,
	[tra_tipo_mensaje_resp] [nchar](4) NULL,
	[tra_codigo_proceso] [nchar](6) NOT NULL,
	[tra_codigo_respuesta] [varchar](2) NULL,
	[tra_codigo_respuestaB24] [varchar](2) NULL,
	[tra_stan] [int] NOT NULL,
	[tra_rrn] [varchar](20) NOT NULL,
	[tra_autorizacion_ope_id] [varchar](20) NULL,
	[tra_auth_id_p38] [varchar](6) NULL,
	[tra_codigo_estado_final] [varchar](10) NULL,
	[tra_terminal_id] [varchar](20) NOT NULL,
	[tra_comercio_id] [varchar](20) NOT NULL,
	[tra_comercio_saldo_inicial] [money] NULL,
	[tra_valor] [money] NULL,
	[tra_comercio_saldo_final] [money] NULL,
	[tra_referencia_cliente_1] [varchar](50) NULL,
	[tra_referencia_cliente_2] [varchar](50) NULL,
	[tra_referencia_cliente_3] [varchar](50) NULL,
	[tra_datos_adicionales] [varchar](100) NULL,
	[tra_producto_id] [varchar](10) NULL,
	[tra_ip_cliente] [varchar](20) NULL,
	[tra_puerto_atencion] [int] NULL,
 CONSTRAINT [PK_Transaccion] PRIMARY KEY CLUSTERED 
(
	[tra_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PuntoVenta](
	[pto_id] [bigint] IDENTITY(1,1) NOT NULL,
	[pto_numero] [varchar](20) NOT NULL,
	[pto_tipo_punto_venta_id] [bigint] NOT NULL,
	[pto_numero_serie] [varchar](50) NULL,
	[pto_estado_punto_venta_id] [bigint] NOT NULL,
	[pto_observacion] [varchar](100) NOT NULL,
	[pto_version_software] [varchar](50) NULL,
	[pto_tipo_comunicacion_id] [bigint] NOT NULL,
	[pto_modelo] [varchar](50) NOT NULL,
	[pto_imei] [varchar](20) NULL,
 CONSTRAINT [PK_PuntoVenta] PRIMARY KEY CLUSTERED 
(
	[pto_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SucursalBanco]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SucursalBanco](
	[suc_id] [bigint] NOT NULL,
	[suc_banco_id] [bigint] NOT NULL,
	[suc_nombre] [varchar](50) NOT NULL,
	[suc_direccion] [varchar](100) NOT NULL,
	[suc_observacion] [varchar](100) NULL,
 CONSTRAINT [PK_SucursalBanco] PRIMARY KEY CLUSTERED 
(
	[suc_id] ASC,
	[suc_banco_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CuentaBanco]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CuentaBanco](
	[cue_id] [bigint] IDENTITY(1,1) NOT NULL,
	[cue_numero] [varchar](50) NOT NULL,
	[cue_tipo_cuenta_id] [bigint] NOT NULL,
	[cue_banco_id] [bigint] NOT NULL,
	[cue_observacion] [varchar](100) NULL,
 CONSTRAINT [PK_CuentaBanco] PRIMARY KEY CLUSTERED 
(
	[cue_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Funcion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Funcion](
	[func_id] [bigint] IDENTITY(1,1) NOT NULL,
	[func_descripcion] [varchar](200) NOT NULL,
	[func_objeto_id] [bigint] NOT NULL,
	[func_metodo_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Funcion] PRIMARY KEY CLUSTERED 
(
	[func_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Funcion] ON
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (1, N'Cargar P�gina Inicial', 1, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (2, N'Iniciar Sesi�n', 2, 2)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (4, N'Cerrar Session', 2, 3)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (5, N'Grabar Mensaje Informativo', 3, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (6, N'Manager de Seguridad', 4, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (7, N'Form. Cuenta Usuario', 5, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (8, N'Form. Cambiar Clave', 6, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (9, N'Form. Cuenta Usuario', 5, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (10, N'Form. Cambiar Clave', 6, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (11, N'Form. Adicionar Usuario', 7, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (12, N'Form. Adicionar Usuario', 7, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (13, N'Form. Listar Usuarios', 8, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (14, N'Form. Editar Usuarios', 9, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (15, N'Form. Editar Usuarios', 9, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (16, N'Form. Listar Usuarios', 8, 8)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (17, N'Form. Objetos', 10, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (18, N'Form. M�todos', 11, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (19, N'Form. Perfiles', 13, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (20, N'Form. Funciones', 12, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (24, N'Form. Adicionar Funciones a Perfil', 14, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (25, N'Form. Adicionar Objetos', 10, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (26, N'Form. Adicionar Funciones a Perfil', 14, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (27, N'Form. Adicionar Funciones a Perfil', 14, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (28, N'Form. Objetos', 10, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (29, N'Form. M�todos', 11, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (30, N'Form. Perfiles', 13, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (31, N'Form. Activar Comercios', 23, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (32, N'Form. Listar Comercios', 24, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (33, N'Manager de Comercios', 25, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (34, N'Form. Activar Comercios', 23, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (35, N'Form. Editar Comercio', 26, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (36, N'Form. Editar Comercio', 26, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (37, N'Manager de Bancos', 27, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (38, N'Form. Listar Bancos', 28, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (39, N'Form. Listar Bancos', 28, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (40, N'Form. Adicionar Cuenta', 29, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (41, N'Form. Adicionar Cuenta', 29, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (42, N'Form. Adicionar Cuenta', 29, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (43, N'Form. Adicionar Cuenta', 29, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (44, N'Form. Movimiento de Cuenta Comercio', 30, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (45, N'Form. Movimiento de Cuenta Comercio', 30, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (46, N'Manager de Reportes', 31, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (49, N'Form. Datos del Reporte', 32, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (50, N'Visor de Reportes', 34, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (51, N'Manager de Transacciones', 35, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (52, N'Vista Actual de Transacciones', 36, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (53, N'Buscar Transacci�n de Recargas', 37, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (54, N'Manager de Proveedores', 39, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (55, N'Form. Adicionar Proveedores', 40, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (56, N'Form. Listar Proveedores', 41, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (57, N'Form. Listar Proveedores', 41, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (58, N'Form. Adicionar Proveedores', 40, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (59, N'Form. Perfiles', 13, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (60, N'Form. Objetos', 11, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (61, N'Form. Adicionar Producto', 42, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (62, N'Form. Adicionar Producto', 42, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (63, N'Form. Adicionar Producto', 42, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (64, N'Form. Compra de Cupo', 43, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (65, N'Form. Compra de Cupo', 43, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (66, N'Manager de Parametrizaci�n', 44, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (67, N'Form. Modelo de Negocio', 45, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (68, N'Form. Modelo de Negocio', 45, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (69, N'Form. Modulos Web', 46, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (70, N'Form. Modulos Web', 46, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (71, N'Form. Modulos Web', 46, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (72, N'Form. Adicionar Opciones', 47, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (73, N'Form. Adicionar Opciones', 47, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (74, N'Form. Adicionar Opciones', 47, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (75, N'Form. Adicionar M�dulo Web', 48, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (76, N'Form. Adicionar M�dulo Web', 48, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (77, N'Form. Adicionar M�dulo Web', 48, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (78, N'Form. Monitor Transaccional', 49, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (79, N'Obtener Estad�sticas Transaccionales', 50, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (80, N'Manager de Conciliaciones', 51, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (81, N'Form. Generar Archivo Movilred', 52, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (83, N'Form. Terminales X Comercio', 54, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (84, N'Form. Terminales X Comercio', 54, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (85, N'Form. Modelo de Negocio', 45, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (86, N'Form. Adicionar Productos X Modelo Negocio', 55, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (87, N'Form. Adicionar Productos X Modelo Negocio', 55, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (88, N'Form. Adicionar Productos X Modelo Negocio', 55, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (89, N'Buscar Transacci�n de Pago Facturas', 56, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (90, N'Form. Adicionar Convenios X Producto', 57, 1)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (91, N'Form. Adicionar Convenios X Producto', 57, 6)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (92, N'Form. Adicionar Convenios X Producto', 57, 4)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (93, N'Form. Adicionar Convenios X Producto', 57, 5)
INSERT [dbo].[Funcion] ([func_id], [func_descripcion], [func_objeto_id], [func_metodo_id]) VALUES (95, N'Form. Generar Archivo Movilred', 52, 7)
SET IDENTITY_INSERT [dbo].[Funcion] OFF
/****** Object:  Table [dbo].[Ciudad]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ciudad](
	[id] [bigint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[departamento_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Ciudad] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Ciudad] ([id], [nombre], [departamento_id]) VALUES (11001, N'BOGOT�', 24)
/****** Object:  Table [dbo].[Opcion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Opcion](
	[opc_id] [bigint] IDENTITY(1,1) NOT NULL,
	[opc_titulo] [varchar](50) NOT NULL,
	[opc_url] [varchar](200) NOT NULL,
	[opc_img_url] [varchar](200) NULL,
	[opc_tipo_opcion_id] [bigint] NOT NULL,
	[opc_opcion_padre_id] [bigint] NULL,
	[opc_orden] [smallint] NOT NULL,
 CONSTRAINT [PK_Opcion] PRIMARY KEY CLUSTERED 
(
	[opc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Opcion] ON
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (1, N'Comercios', N'Merchants/Manager.aspx', N'img/icons/sidemenu/factory.png', 1, NULL, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (3, N'Activar Comercio', N'Merchants/ActivateMerchant.aspx', N'../img/icons/dashbutton/add.png', 2, 1, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (6, N'Listar Comercios', N'Merchants/ListMerchants.aspx', N'../img/icons/dashbutton/list.png', 2, 1, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (31, N'Seguridad', N'Security/Manager.aspx', N'img/icons/sidemenu/security.png', 1, NULL, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (32, N'Listar Usuarios', N'Security/ListUsers.aspx', N'../img/icons/dashbutton/list.png', 2, 31, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (34, N'Crear Usuario', N'Security/AddUser.aspx', N'../img/icons/dashbutton/add.png', 2, 31, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (35, N'Objetos', N'Security/Objects.aspx', N'../img/icons/dashbutton/object.png', 2, 31, 3)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (36, N'M�todos', N'Security/Methods.aspx', N'../img/icons/dashbutton/method.png', 2, 31, 4)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (38, N'Funciones', N'Security/Functions.aspx', N'../img/icons/dashbutton/function.jpg', 2, 31, 5)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (39, N'Perfiles', N'Security/Profiles.aspx', N'../img/icons/dashbutton/profile.png', 2, 31, 6)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (43, N'Bancos', N'Banks/Manager.aspx', N'img/icons/sidemenu/banks.png', 1, NULL, 3)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (44, N'Listar Bancos', N'Banks/ListBanks.aspx', N'../img/icons/dashbutton/list.png', 2, 43, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (45, N'Reportes', N'Reports/Manager.aspx', N'img/icons/sidemenu/reports.png', 1, NULL, 4)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (46, N'Ventas Detalladas X Comercios', N'Reports/EntryPoint.aspx', N'../img/icons/dashbutton/report1.png', 2, 45, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (47, N'Mov. de Cuenta X Comercios', N'Reports/EntryPoint.aspx', N'../img/icons/dashbutton/report2.png', 2, 45, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (48, N'Res�men de Ventas X D�a', N'Reports/EntryPoint.aspx', N'../img/icons/dashbutton/report3.png', 2, 45, 3)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (51, N'Visor Transacciones', N'Transactions/Manager.aspx', N'img/icons/sidemenu/magnify.png', 1, NULL, 5)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (52, N'Vista Transacciones Actuales', N'Transactions/CurrentTransactions.aspx', N'../img/icons/dashbutton/list.png', 2, 51, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (53, N'Buscar Transacciones Recarga', N'Transactions/SearchTopupTransactions.aspx', N'../img/icons/dashbutton/search.png', 2, 51, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (54, N'Log de Auditor�a', N'Reports/EntryPoint.aspx', N'../img/icons/dashbutton/report4.png', 2, 45, 4)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (55, N'Proveedores', N'Suppliers/Manager.aspx', N'img/icons/sidemenu/suppliers.png', 1, NULL, 6)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (56, N'Crear Proveedor', N'Suppliers/AddSupplier.aspx', N'../img/icons/dashbutton/add.png', 2, 55, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (57, N'Listar Proveedores', N'Suppliers/ListSuppliers.aspx', N'../img/icons/dashbutton/list.png', 2, 55, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (58, N'Parametrizaci�n', N'Settings/Manager.aspx', N'img/icons/sidemenu/settings.png', 1, NULL, 7)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (59, N'Listar Modelos de Negocio', N'Settings/ListBusinessModels.aspx', N'../img/icons/dashbutton/list.png', 2, 58, 1)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (61, N'M�dulos Web', N'Security/WebModules.aspx', N'../img/icons/dashbutton/modules.png', 2, 31, 7)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (80, N'Compras de Cupo X Proveedor', N'Reports/EntryPoint.aspx', N'../img/icons/dashbutton/report5.png', 2, 45, 5)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (81, N'Monitor Transaccional', N'Transactions/TransactionsMonitor.aspx', N'../img/icons/dashbutton/trx_monitor.png', 2, 51, 3)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (86, N'Conciliaciones', N'Conciliations/Manager.aspx', N'img/icons/sidemenu/conciliations.jpg', 1, NULL, 8)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (93, N'Generar Archivo Recargas Movilred', N'Conciliations/GenerateFileTopupMovilred.aspx', N'../img/icons/dashbutton/download-file.png', 2, 86, 2)
INSERT [dbo].[Opcion] ([opc_id], [opc_titulo], [opc_url], [opc_img_url], [opc_tipo_opcion_id], [opc_opcion_padre_id], [opc_orden]) VALUES (96, N'Buscar Transacciones Pago de Facturas', N'Transactions/SearchBillPaymentTransactions.aspx', N'../img/icons/dashbutton/search.png', 2, 51, 4)
SET IDENTITY_INSERT [dbo].[Opcion] OFF
/****** Object:  Table [dbo].[Producto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Producto](
	[pro_id] [bigint] IDENTITY(120,10) NOT NULL,
	[pro_nombre] [varchar](100) NOT NULL,
	[pro_estado_producto_id] [bigint] NOT NULL,
	[pro_comision_global] [numeric](9, 6) NOT NULL,
	[pro_tipo_producto_id] [bigint] NOT NULL,
	[pro_proveedor_id] [bigint] NOT NULL,
	[pro_cod_EAN] [varchar](30) NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[pro_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Producto] ON
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (120, N'Claro', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'7707175322809')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (130, N'Tigo', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'7702138005065')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (140, N'Movistar', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'7707176962196')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (150, N'Avantel', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'7707223992381')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (160, N'UFF Movil', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'7709990661880')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (170, N'UNE', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'770731603039')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (180, N'DirecTV', 1, CAST(0.000000 AS Numeric(9, 6)), 1, 3, N'7707198435906')
INSERT [dbo].[Producto] ([pro_id], [pro_nombre], [pro_estado_producto_id], [pro_comision_global], [pro_tipo_producto_id], [pro_proveedor_id], [pro_cod_EAN]) VALUES (200, N'Pago_Facturas', 1, CAST(0.000000 AS Numeric(9, 6)), 2, 3, N'7700000000001')
SET IDENTITY_INSERT [dbo].[Producto] OFF
/****** Object:  Table [dbo].[PerfilXOpcion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerfilXOpcion](
	[perf_id] [bigint] NOT NULL,
	[opc_id] [bigint] NOT NULL,
 CONSTRAINT [PK_PerfilXOpcion] PRIMARY KEY CLUSTERED 
(
	[perf_id] ASC,
	[opc_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 31)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 32)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 34)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 35)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 36)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 38)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 39)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 45)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 54)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 55)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 56)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 57)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 58)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 59)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (7, 61)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 1)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 3)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 6)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 45)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 46)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 48)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 51)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 52)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 53)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 81)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (8, 96)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 1)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 6)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 45)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 46)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 48)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 51)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 52)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 53)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 81)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (16, 96)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 51)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 52)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 53)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 81)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 86)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 93)
INSERT [dbo].[PerfilXOpcion] ([perf_id], [opc_id]) VALUES (17, 96)
/****** Object:  Table [dbo].[PerfilXFuncion]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerfilXFuncion](
	[perf_id] [bigint] NOT NULL,
	[func_id] [bigint] NOT NULL,
 CONSTRAINT [PK_PerfilXFuncion] PRIMARY KEY CLUSTERED 
(
	[perf_id] ASC,
	[func_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (6, 2)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (6, 4)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (6, 5)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 1)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 2)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 4)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 5)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 6)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 7)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 8)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 9)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 10)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 11)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 12)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 13)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 14)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 15)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 16)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 17)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 18)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 19)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 20)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 24)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 25)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 26)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 27)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 28)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 29)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 30)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 46)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 49)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 50)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 54)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 55)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 56)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 57)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 58)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 59)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 60)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 61)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 62)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 63)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 66)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 67)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 68)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 69)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 70)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 71)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 72)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 73)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 74)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 75)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 76)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 77)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 85)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 86)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 87)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 88)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 90)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 91)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 92)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (7, 93)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 1)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 2)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 4)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 5)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 7)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 8)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 9)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 10)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 31)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 32)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 33)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 34)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 35)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 36)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 37)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 38)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 39)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 40)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 41)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 42)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 43)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 44)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 45)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 46)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 49)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 50)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 51)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 52)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 53)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 54)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 56)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 64)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 65)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 66)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 67)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 78)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 79)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 80)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 81)
GO
print 'Processed 100 total records'
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 83)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 84)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 89)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (8, 95)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 1)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 2)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 4)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 5)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 7)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 8)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 9)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 10)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 32)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 33)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 46)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 49)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 50)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 51)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 52)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 53)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 78)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 79)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 83)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (16, 89)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 1)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 2)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 4)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 5)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 7)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 8)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 9)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 10)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 51)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 52)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 53)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 78)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 80)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 81)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 89)
INSERT [dbo].[PerfilXFuncion] ([perf_id], [func_id]) VALUES (17, 95)
/****** Object:  Table [dbo].[Matriz_NivelEntidadXProducto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Matriz_NivelEntidadXProducto](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[tipo_nivel_entidad_id] [bigint] NOT NULL,
	[producto_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Matriz_NivelEntidadXProducto] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Matriz_NivelEntidadXProducto] ON
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (1, 2, 120)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (2, 2, 130)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (3, 2, 140)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (4, 2, 150)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (5, 2, 160)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (6, 2, 170)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (7, 2, 180)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (8, 2, 200)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (9, 3, 120)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (10, 3, 130)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (11, 3, 140)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (12, 3, 150)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (13, 3, 160)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (14, 3, 170)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (15, 3, 180)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (16, 3, 200)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (17, 4, 120)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (18, 4, 130)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (19, 4, 140)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (20, 4, 150)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (21, 4, 160)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (22, 4, 170)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (23, 4, 180)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (24, 4, 200)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (25, 5, 120)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (26, 5, 130)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (27, 5, 140)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (28, 5, 150)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (29, 5, 160)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (30, 5, 170)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (31, 5, 180)
INSERT [dbo].[Matriz_NivelEntidadXProducto] ([id], [tipo_nivel_entidad_id], [producto_id]) VALUES (32, 5, 200)
SET IDENTITY_INSERT [dbo].[Matriz_NivelEntidadXProducto] OFF
/****** Object:  Table [dbo].[Compra]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Compra](
	[comp_id] [bigint] IDENTITY(1,1) NOT NULL,
	[comp_fecha] [datetime] NOT NULL,
	[comp_proveedor_id] [bigint] NOT NULL,
	[comp_saldo_inicial] [money] NOT NULL,
	[comp_saldo_final] [money] NOT NULL,
	[comp_observacion] [varchar](200) NULL,
	[comp_cuenta_banco_id] [bigint] NOT NULL,
	[comp_log_id] [bigint] NOT NULL,
	[comp_valor] [money] NOT NULL,
 CONSTRAINT [PK_Compra] PRIMARY KEY CLUSTERED 
(
	[comp_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConvenioXProducto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConvenioXProducto](
	[conv_id] [bigint] IDENTITY(1,1) NOT NULL,
	[conv_nombre] [varchar](150) NOT NULL,
	[conv_codigo_EAN] [varchar](20) NOT NULL,
	[conv_cod_parcial] [bit] NOT NULL,
	[conv_ref_doble] [bit] NOT NULL,
	[conv_pos_ini_ref1] [smallint] NOT NULL,
	[conv_longitud_ref1] [smallint] NOT NULL,
	[conv_pos_ini_valor] [smallint] NOT NULL,
	[conv_longitud_valor] [smallint] NOT NULL,
	[conv_pos_ini_ref2] [smallint] NOT NULL,
	[conv_longitud_ref2] [smallint] NULL,
	[conv_producto_id] [bigint] NOT NULL,
 CONSTRAINT [PK_ConvenioXProducto] PRIMARY KEY CLUSTERED 
(
	[conv_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ConvenioXProducto] ON
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (1, N'Telefonica Moviles Colombia S', N'7707176960178', 0, 0, 21, 8, 33, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (2, N'Colombia Movil S.A. E S P', N'7702138000022', 0, 0, 21, 10, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (3, N'Gas Natural de Bogota', N'7707208029194', 0, 0, 21, 16, 41, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (4, N'Emp De Telefonos De Bogota', N'7707181500017', 0, 0, 21, 14, 39, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (5, N'Emp Acueducto De Bogota', N'7707200485271', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (6, N'Aseo Bogota Recaudo', N'7709998003514', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (7, N'Emp Telecomunicaciones ETB S.A', N'7709998002524', 0, 0, 21, 14, 39, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (8, N'Hector Fabio Amaya Valencia', N'7709998019959', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (9, N'Electrificadora de Boyaca', N'7709998000483', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (10, N'Electrificadora del Caqueta SA', N'7709998008083', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (11, N'Gases del caribe Barranquilla', N'7707232377896', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (12, N'Galeria Inmobiliaria S.A.S.', N'7709998021969', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (13, N'Fund Col de la Inmacul', N'7709998013148', 1, 0, 21, 8, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (14, N'Colegio la Presentacion', N'7709998005808', 0, 0, 21, 8, 33, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (15, N'Hnas Dominicas De La Presenta', N'7709998004481', 0, 0, 21, 8, 33, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (16, N'Empresa acueducto zipaquira', N'7707278950015', 0, 0, 21, 8, 33, 8, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (17, N'Emp Aguas Gir Ricau y la Regio', N'7709998000728', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (18, N'INMOBILIARIA PARAISO LTDA', N'7709998003217', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (19, N'Emp Metro Aseo de Pasto', N'7707262300024', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (20, N'Transmilenium 20212 Lt', N'7709998009851', 0, 0, 21, 14, 39, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (21, N'Servigenerales', N'7709998001145', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (22, N'Altipal S.A.', N'7707349750018', 1, 0, 21, 14, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (23, N'Avon Colombia Ltda', N'7709998006065', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (24, N'Cooperati Magisterio Tuquerres', N'7709998605558', 0, 0, 21, 18, 43, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (25, N'Colegio San Ana Fontibon', N'7709998018259', 0, 0, 21, 10, 35, 8, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (26, N'Tax Individual SA', N'7709998001299', 1, 0, 21, 4, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (27, N'Envitaxi S.A.', N'7709998017405', 1, 0, 21, 8, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (28, N'Envitaxi SA', N'7709998017399', 1, 0, 21, 4, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (29, N'Citi Taxi S.A.', N'7709998017412', 1, 0, 21, 4, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (30, N'Citi Taxi SA', N'7709998017429', 1, 0, 21, 8, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (31, N'Tax Supremo S.A.', N'7709998018709', 1, 0, 21, 8, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (32, N'Tax Supremo SA', N'7709998018716', 1, 0, 21, 4, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (33, N'Tax Individual S.A.', N'7709998017436', 1, 0, 21, 8, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (34, N'Municipio de Palmira', N'7709998017481', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (35, N'Antraindigo', N'7709998002401', 0, 0, 21, 8, 33, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (36, N'Mpio de Bello Impuesto Predial', N'7709998000117', 0, 0, 21, 8, 33, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (37, N'Liceo Francisco Restrepo Molin', N'7709998006263', 0, 0, 21, 10, 35, 8, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (38, N'Col �Parr Ntra Sra Buen Consej', N'7709998002616', 0, 0, 21, 8, 33, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (39, N'Coomitan', N'7709998000032', 0, 0, 21, 14, 39, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (40, N'Central Elect N Santander', N'7709998001794', 0, 0, 21, 18, 43, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (41, N'Electrificadora del Meta', N'7709998002449', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (42, N'C S del Santisimo de la Carida', N'7709998015364', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (43, N'C S del Santisim y de �Caridad', N'7709998016309', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (44, N'C S del Santisimo y de Caridad', N'7709998016286', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (45, N'Cong Sierv del Santis y de Car', N'7709998016316', 1, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (46, N'Colegio Maria Auxiliadora', N'7709998004283', 1, 0, 21, 6, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (47, N'Copropiedad Central Mayorista', N'7709998012387', 0, 0, 21, 14, 39, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (48, N'Coopteptur', N'7709998004719', 0, 0, 21, 8, 33, 8, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (49, N'Agrup de Vivienda Rafael Nu?ez', N'7709998008915', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (50, N'Conj Res Parques de Sn Nicolas', N'7709998018952', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (51, N'Urb Rafael Nunez', N'7709998008922', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (52, N'Emp Energia de Cund BTA', N'7707197690016', 0, 0, 21, 20, 45, 14, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (53, N'Empresa de Energia de Pereira', N'7709998015692', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (54, N'GAS NATURAL CUNDIBOYACENSE', N'7707208424432', 0, 0, 21, 16, 41, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (55, N'Electrificadora Santander', N'7707266014651', 0, 0, 21, 12, 37, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (56, N'Gas Natural del Oriente S.A. E', N'7709998001015', 0, 0, 21, 16, 41, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (57, N'Prod Nat de la Sabana Alqueria', N'7702177010426', 1, 0, 21, 10, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (58, N'Central Elec De Narino', N'7707246320024', 0, 0, 21, 16, 41, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (59, N'Directv Colombia Ltda', N'7707198435777', 1, 0, 21, 10, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (60, N'Parmalat de Colombia Ltda', N'7700604000118', 1, 0, 21, 16, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (61, N'Procesador Leche Proleche', N'7702130000112', 1, 0, 21, 16, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (62, N'Cerveceria Union S.A.', N'7702051002639', 1, 0, 21, 16, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (63, N'Bavaria S.A.', N'7702004009302', 1, 0, 21, 16, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (64, N'Empresas Publicas Neiva', N'7709998018730', 0, 0, 21, 8, 33, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (65, N'Empr de Energia de Casanare', N'7709998012233', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (66, N'Acueducto de Yopal Recaudo', N'7709998009264', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (67, N'Empresas Publias Neiva', N'7709998004405', 0, 0, 21, 8, 33, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (68, N'Emp telecomunic Pereira', N'7707220676673', 0, 0, 21, 10, 35, 12, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (69, N'Alpina Productos Alimenticios', N'7702001015054', 1, 0, 21, 12, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (70, N'Col bethelmitas De Palmira', N'7709998007932', 0, 0, 21, 8, 33, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (71, N'Parroq Santa Isabel de Hungria', N'7709998018877', 0, 0, 21, 5, 30, 6, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (72, N'Dromayor Bogot? S.A.S.', N'7709998818545', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (73, N'Venus Colombia S.A.', N'7705627500003', 1, 0, 21, 20, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (74, N'Empr Acueducto Alcanta de Vcio', N'7709998000414', 0, 0, 21, 8, 33, 8, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (75, N'Cubicar Grupo Inmobiliario Ltd', N'7709998634244', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (76, N'Copidrogas', N'7707200902297', 0, 0, 21, 20, 45, 14, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (77, N'Municipio de Tulua Impuesto', N'7709998012394', 0, 0, 21, 12, 37, 14, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (78, N'Surtigas SA', N'7709998002777', 0, 0, 21, 10, 35, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (79, N'Cia De Elect �De Tulua', N'7709998008557', 1, 0, 21, 12, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (80, N'Cia de Financiamiento Tuya SA', N'7709998018785', 1, 0, 21, 11, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (81, N'EAMM - Colgio Santa Ana', N'7709998022157', 0, 0, 21, 10, 35, 8, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (82, N'CMR Falabella C.F.C. S.A.', N'7709998006522', 1, 0, 21, 16, 0, 0, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (83, N'Franco y Velez Cesde cia ltda', N'7709998004221', 0, 0, 21, 24, 49, 14, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (84, N'Aguas De Cartagena', N'7709998014633', 0, 0, 21, 14, 39, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (85, N'Acuasur Acueducto del Sur S.A.', N'7709998011137', 0, 0, 21, 20, 45, 9, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (86, N'Droguistas Ipiales SAS', N'7709998415058', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (87, N'Dromayor Medellin SAS', N'7709998860568', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (88, N'Copicredito Coop de Ah y Cred', N'7709998011205', 0, 0, 21, 20, 45, 14, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (89, N'Dromayor Pasto SAS', N'7709998122468', 0, 0, 21, 12, 37, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (90, N'Movistar', N'7701055000054', 0, 0, 21, 16, 41, 10, 0, 0, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (91, N'Funda Universitaria de Popayan', N'7709998009271', 0, 1, 21, 11, 50, 10, 36, 10, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (92, N'Alarmar Ltda', N'7709998005761', 1, 1, 21, 10, 0, 0, 35, 10, 200)
INSERT [dbo].[ConvenioXProducto] ([conv_id], [conv_nombre], [conv_codigo_EAN], [conv_cod_parcial], [conv_ref_doble], [conv_pos_ini_ref1], [conv_longitud_ref1], [conv_pos_ini_valor], [conv_longitud_valor], [conv_pos_ini_ref2], [conv_longitud_ref2], [conv_producto_id]) VALUES (93, N'Invergrupo S.A.', N'7701846000010', 0, 1, 21, 8, 33, 6, 43, 6, 200)
SET IDENTITY_INSERT [dbo].[ConvenioXProducto] OFF
/****** Object:  Table [dbo].[Barrio]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Barrio](
	[id] [bigint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[ciudad_id] [bigint] NOT NULL,
 CONSTRAINT [PK_Barrio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Barrio] ([id], [nombre], [ciudad_id]) VALUES (99784, N'CHIC�', 11001)
/****** Object:  Table [dbo].[SimCard]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SimCard](
	[sim_id] [bigint] IDENTITY(1,1) NOT NULL,
	[sim_serie] [varchar](50) NOT NULL,
	[sim_telefono] [varchar](20) NOT NULL,
	[sim_usuario] [varchar](20) NULL,
	[sim_password] [varchar](20) NULL,
	[sim_tipo_plan_sim_id] [bigint] NOT NULL,
	[sim_estado_sim_card_id] [bigint] NOT NULL,
 CONSTRAINT [PK_SimCard] PRIMARY KEY CLUSTERED 
(
	[sim_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RazonSocial]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RazonSocial](
	[raz_id] [bigint] IDENTITY(1,1) NOT NULL,
	[raz_nro_fiscal] [varchar](20) NOT NULL,
	[raz_tipo_identificacion_id] [bigint] NOT NULL,
	[raz_nombre_personal] [varchar](200) NOT NULL,
	[raz_nombre_razon_social] [varchar](200) NOT NULL,
	[raz_direccion] [varchar](200) NOT NULL,
	[raz_barrio_id] [bigint] NOT NULL,
	[raz_ciudad_id] [bigint] NOT NULL,
	[raz_departamento_id] [bigint] NOT NULL,
	[raz_fax] [varchar](20) NULL,
	[raz_telefono_fijo] [varchar](20) NULL,
	[raz_telefono_movil] [varchar](20) NULL,
	[raz_email] [varchar](100) NULL,
	[raz_contacto1] [varchar](200) NOT NULL,
	[raz_contacto1_tel1] [varchar](20) NULL,
	[raz_contacto2] [varchar](200) NOT NULL,
	[raz_contacto2_tel2] [varchar](20) NULL,
	[raz_tipo_regimen_id] [bigint] NOT NULL,
 CONSTRAINT [PK_RazonSocial] PRIMARY KEY CLUSTERED 
(
	[raz_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[RazonSocial] ON
INSERT [dbo].[RazonSocial] ([raz_id], [raz_nro_fiscal], [raz_tipo_identificacion_id], [raz_nombre_personal], [raz_nombre_razon_social], [raz_direccion], [raz_barrio_id], [raz_ciudad_id], [raz_departamento_id], [raz_fax], [raz_telefono_fijo], [raz_telefono_movil], [raz_email], [raz_contacto1], [raz_contacto1_tel1], [raz_contacto2], [raz_contacto2_tel2], [raz_tipo_regimen_id]) VALUES (1, N'890321567', 1, N'Red de Pagos Conred', N'Conred', N'Calle 26 con Av. Cali', 99784, 11001, 24, N'4300300', N'4300300', N'4300300', N'elkin.beltran@carvajal.com', N'Jhon Jairo Morales', N'4300300', N'Jhon Jairo Morales', N'4300300', 1)
SET IDENTITY_INSERT [dbo].[RazonSocial] OFF
/****** Object:  Table [dbo].[PuntoVentaXSimCard]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PuntoVentaXSimCard](
	[pto_id] [bigint] NOT NULL,
	[sim_id] [bigint] NOT NULL,
	[numero_asignacion] [smallint] NOT NULL,
	[fecha_inicial] [datetime] NOT NULL,
	[fecha_final] [datetime] NULL,
	[tipo_causal_id] [bigint] NOT NULL,
	[observacion] [varchar](100) NULL,
 CONSTRAINT [PK_PuntoVentaXSimCard] PRIMARY KEY CLUSTERED 
(
	[pto_id] ASC,
	[sim_id] ASC,
	[numero_asignacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Entidad]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Entidad](
	[ent_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ent_comercio_JNTS] [varchar](15) NULL,
	[ent_tipo_nivel_entidad_id] [bigint] NOT NULL,
	[ent_estado_entidad_id] [bigint] NOT NULL,
	[ent_maneja_saldo] [bit] NOT NULL,
	[ent_comision_global] [numeric](9, 6) NOT NULL,
	[ent_razon_social_id] [bigint] NOT NULL,
	[ent_saldo] [money] NOT NULL,
	[ent_entidad_padre_id] [bigint] NULL,
	[ent_rubro_id] [bigint] NOT NULL,
	[ent_fecha_activacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Entidad] PRIMARY KEY CLUSTERED 
(
	[ent_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Entidad] ON
INSERT [dbo].[Entidad] ([ent_id], [ent_comercio_JNTS], [ent_tipo_nivel_entidad_id], [ent_estado_entidad_id], [ent_maneja_saldo], [ent_comision_global], [ent_razon_social_id], [ent_saldo], [ent_entidad_padre_id], [ent_rubro_id], [ent_fecha_activacion]) VALUES (0, N'0000000000', 1, 1, 0, CAST(0.000000 AS Numeric(9, 6)), 1, 0.0000, NULL, 1, CAST(0x0000901A00000000 AS DateTime))
SET IDENTITY_INSERT [dbo].[Entidad] OFF
/****** Object:  Table [dbo].[AvisoDeposito]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AvisoDeposito](
	[avi_id] [bigint] IDENTITY(1,1) NOT NULL,
	[avi_fecha] [datetime] NULL,
	[avi_entidad_id] [bigint] NOT NULL,
	[avi_punto_venta_numero] [varchar](20) NOT NULL,
	[avi_cuenta_banco_id] [bigint] NOT NULL,
	[avi_valor] [money] NOT NULL,
	[avi_estado_aviso_id] [bigint] NOT NULL,
	[avi_sucursal_banco_id] [bigint] NOT NULL,
 CONSTRAINT [PK_AvisoDeposito] PRIMARY KEY CLUSTERED 
(
	[avi_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[usu_id] [bigint] IDENTITY(1,1) NOT NULL,
	[usu_nombre] [varchar](200) NOT NULL,
	[usu_email] [varchar](100) NOT NULL,
	[usu_login] [varchar](50) NOT NULL,
	[usu_hash] [varchar](150) NOT NULL,
	[usu_perfil_id] [bigint] NOT NULL,
	[usu_estado_usuario_id] [bigint] NOT NULL,
	[usu_entidad_id] [bigint] NOT NULL,
	[usu_fecha_exp_clave] [datetime] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[usu_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_Usuario] UNIQUE NONCLUSTERED 
(
	[usu_login] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (92, N'Servidor Conred', N'conred@carvajal.com', N'conred', N'0', 5, 1, 0, CAST(0x0000A0BF00000000 AS DateTime))
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (93, N'Diana Alexandra Prieto', N'Diana.Prieto@carvajal.com', N'da.prieto', N'8a0dfa28e6d2498f7fdd06318c8e82e2', 7, 1, 0, CAST(0x0000A14800FC4B8E AS DateTime))
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (94, N'Daisy Madelene Burgos ', N'daisy.burgos@carvajal.com', N'dm.burgos', N'3c6a6a00a84c65c19d5ee6952fde03e2', 8, 1, 0, CAST(0x0000A1400115123D AS DateTime))
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (95, N'John Jairo Escobar', N'john.escobar@carvajal.com', N'jj.escobar', N'c514c95c5bbbf190ad60c366ef567abe', 17, 1, 0, CAST(0x0000A14D00BA7848 AS DateTime))
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (96, N'H�ctor Andres Bejarano ', N'hector.bejarano@carvajal.com', N'ha.bejarano', N'bd3255e3d90c31de3dc74474590e328e', 16, 1, 0, CAST(0x0000A103012015AA AS DateTime))
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (97, N'Elkin David Beltr�n Duque', N'elkin.beltran@carvajal.com', N'ed.beltran', N'a496af8f079782fa4559e03d45a97414', 6, 1, 0, CAST(0x0000A1410089E8BB AS DateTime))
INSERT [dbo].[Usuario] ([usu_id], [usu_nombre], [usu_email], [usu_login], [usu_hash], [usu_perfil_id], [usu_estado_usuario_id], [usu_entidad_id], [usu_fecha_exp_clave]) VALUES (98, N'Lilia Consuelo Arismendi ', N'lilia.arismendi@carvajal.com', N'lc.arismendi', N'3ba1fb17954cf75fe0cbf70ad9220046', 7, 1, 0, CAST(0x0000A11900B89311 AS DateTime))
SET IDENTITY_INSERT [dbo].[Usuario] OFF
/****** Object:  Table [dbo].[PuntoVentaXEntidad]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PuntoVentaXEntidad](
	[pto_id] [bigint] NOT NULL,
	[ent_id] [bigint] NOT NULL,
	[numero_asignacion] [smallint] NOT NULL,
	[fecha_inicial] [datetime] NOT NULL,
	[fecha_final] [datetime] NULL,
	[tipo_causal_id] [bigint] NULL,
	[observacion] [varchar](100) NULL,
 CONSTRAINT [PK_PuntoVentaXEntidad] PRIMARY KEY CLUSTERED 
(
	[pto_id] ASC,
	[ent_id] ASC,
	[numero_asignacion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuarioxRestauracionClave]    Script Date: 12/03/2012 09:37:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsuarioxRestauracionClave](
	[rest_id] [bigint] IDENTITY(1,1) NOT NULL,
	[rest_usuario_id] [bigint] NOT NULL,
	[rest_fecha] [datetime] NOT NULL,
	[rest_rand] [varchar](50) NOT NULL,
	[rest_estado] [bit] NOT NULL,
 CONSTRAINT [PK_UsuarioxRestauracionClave] PRIMARY KEY CLUSTERED 
(
	[rest_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuarioHistoricoClaves]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsuarioHistoricoClaves](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[fecha] [datetime] NOT NULL,
	[usu_id] [bigint] NOT NULL,
	[usu_hash_antiguo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UsuarioHistoricoClaves] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[UsuarioHistoricoClaves] ON
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (1, CAST(0x0000A103010E4854 AS DateTime), 93, N'94ff0f9ccf1ebe28374d57ad4d28802b')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (2, CAST(0x0000A103010E77E1 AS DateTime), 93, N'43fced357bbab7a67f700105dfe0638b')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (3, CAST(0x0000A1030115123D AS DateTime), 94, N'3c6a6a00a84c65c19d5ee6952fde03e2')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (4, CAST(0x0000A1040089E8BB AS DateTime), 97, N'a496af8f079782fa4559e03d45a97414')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (5, CAST(0x0000A10B00E18F05 AS DateTime), 95, N'9848f9152135a0aa8d86b0fe6a69fee8')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (6, CAST(0x0000A10B00FA228E AS DateTime), 93, N'610acf39bc09932f6dddbf8a123807c2')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (7, CAST(0x0000A10B00FC4B8E AS DateTime), 93, N'8a0dfa28e6d2498f7fdd06318c8e82e2')
INSERT [dbo].[UsuarioHistoricoClaves] ([id], [fecha], [usu_id], [usu_hash_antiguo]) VALUES (8, CAST(0x0000A11000BA7848 AS DateTime), 95, N'c514c95c5bbbf190ad60c366ef567abe')
SET IDENTITY_INSERT [dbo].[UsuarioHistoricoClaves] OFF
/****** Object:  Table [dbo].[Mensaje]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mensaje](
	[men_id] [bigint] IDENTITY(1,1) NOT NULL,
	[men_fecha_envio] [datetime] NOT NULL,
	[men_fecha_lectura] [datetime] NULL,
	[men_usuario_id_de] [bigint] NOT NULL,
	[men_usuario_id_para] [bigint] NOT NULL,
	[men_tipo_nivel_mensaje_id] [bigint] NOT NULL,
	[men_texto] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Mensaje] PRIMARY KEY CLUSTERED 
(
	[men_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Log]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Log](
	[log_id] [bigint] IDENTITY(1,1) NOT NULL,
	[log_usuario_id] [bigint] NOT NULL,
	[log_fecha] [datetime] NOT NULL,
	[log_funcion_id] [bigint] NULL,
	[log_tipo_log_id] [bigint] NOT NULL,
	[log_mensaje1] [varchar](250) NULL,
	[log_mensaje2] [varchar](250) NULL,
	[log_sessionID] [varchar](50) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[log_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Log] ON
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (1, 94, CAST(0x0000A10F00E6601F AS DateTime), 2, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (2, 94, CAST(0x0000A10F00E6604A AS DateTime), 1, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (3, 94, CAST(0x0000A10F00E667DA AS DateTime), 33, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (4, 94, CAST(0x0000A10F00E66A2F AS DateTime), 46, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (5, 94, CAST(0x0000A10F00E66BF8 AS DateTime), 51, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (6, 94, CAST(0x0000A10F00E66E9B AS DateTime), 52, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (7, 94, CAST(0x0000A10F00E678B2 AS DateTime), 51, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (8, 94, CAST(0x0000A10F00E679E8 AS DateTime), 53, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (9, 94, CAST(0x0000A10F00E689D1 AS DateTime), 51, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (10, 94, CAST(0x0000A10F00E68B62 AS DateTime), 78, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (11, 94, CAST(0x0000A10F00E68BCC AS DateTime), 79, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (12, 94, CAST(0x0000A10F00E68D64 AS DateTime), 46, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (13, 94, CAST(0x0000A10F00E68F16 AS DateTime), 49, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (14, 94, CAST(0x0000A10F00E6933C AS DateTime), 33, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (15, 94, CAST(0x0000A10F00E694D5 AS DateTime), 32, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (16, 94, CAST(0x0000A10F00E6978A AS DateTime), 33, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (17, 94, CAST(0x0000A10F00E698E9 AS DateTime), 31, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (18, 94, CAST(0x0000A10F00E6E2E4 AS DateTime), 33, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (19, 94, CAST(0x0000A10F00E6E503 AS DateTime), 32, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (20, 94, CAST(0x0000A10F00E6E6CE AS DateTime), 46, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (21, 94, CAST(0x0000A10F00E6EA91 AS DateTime), 49, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (22, 94, CAST(0x0000A10F00E6ECD4 AS DateTime), 51, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (23, 94, CAST(0x0000A10F00E6F3CA AS DateTime), 4, 5, N'', N'', N'dzjs2rjy5njklqjuleypuj55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (24, 94, CAST(0x0000A10F00E71B53 AS DateTime), 2, 5, N'', N'', N'nd2a2z45fxtzvbym2oyfafjl')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (25, 94, CAST(0x0000A10F00E71B56 AS DateTime), 1, 5, N'', N'', N'nd2a2z45fxtzvbym2oyfafjl')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (26, 94, CAST(0x0000A10F00E73214 AS DateTime), 4, 5, N'', N'', N'nd2a2z45fxtzvbym2oyfafjl')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (27, 97, CAST(0x0000A10F01151333 AS DateTime), 2, 5, N'', N'', N'd7812678a2f8d007cd9f0653cba1183e')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (28, 97, CAST(0x0000A10F01152606 AS DateTime), 5, 4, N'Datos de Usuario Modificados. Datos [ Nombre=Elkin David Beltr�n Duque   ::   Correo=elkin.beltran@carvajal.com ]', N'', N'd7812678a2f8d007cd9f0653cba1183e')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (29, 97, CAST(0x0000A10F01152E55 AS DateTime), 4, 5, N'', N'', N'd7812678a2f8d007cd9f0653cba1183e')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (30, 97, CAST(0x0000A10F01153C9A AS DateTime), 2, 5, N'', N'', N'67851c0f07eb4900114e300070a50497')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (31, 97, CAST(0x0000A10F01155BEE AS DateTime), 4, 5, N'', N'', N'67851c0f07eb4900114e300070a50497')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (32, 97, CAST(0x0000A10F0119E402 AS DateTime), 2, 5, N'', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (33, 97, CAST(0x0000A10F011A5883 AS DateTime), 5, 4, N'Par�metros de BD Modificados. Datos [ jdbc:sqlserver://172.22.150.148;database=Conred;integratedSecurity=false   ::   conred   ::   ******* ]', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (34, 97, CAST(0x0000A10F011A86B5 AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Iniciado.', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (35, 97, CAST(0x0000A10F011AA50D AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Iniciado.', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (36, 97, CAST(0x0000A10F011ACFE4 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (37, 97, CAST(0x0000A10F011AD486 AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Detenido.', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (38, 97, CAST(0x0000A10F011AD5D9 AS DateTime), 4, 5, N'', N'', N'cb01a4343cbfd58ed41e623f17ffc0c0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (39, 97, CAST(0x0000A1100089DCE2 AS DateTime), 2, 5, N'', N'', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (40, 97, CAST(0x0000A1100089F73F AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Detenido.', N'', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (41, 97, CAST(0x0000A110008A0116 AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Iniciado.', N'', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (42, 97, CAST(0x0000A110008A0F55 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Iniciado.', N'', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (43, 97, CAST(0x0000A110008A42D5 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (44, 97, CAST(0x0000A110008ADB39 AS DateTime), 4, 5, N'', N'', N'2eba4a2e7d67b66544303a4ea555ff62')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (45, 94, CAST(0x0000A110008B1B90 AS DateTime), 2, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (46, 94, CAST(0x0000A110008B1B9E AS DateTime), 1, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (47, 94, CAST(0x0000A110008B23B5 AS DateTime), 46, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (48, 94, CAST(0x0000A110008B2678 AS DateTime), 1, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (49, 94, CAST(0x0000A110008B3635 AS DateTime), 46, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (50, 94, CAST(0x0000A110008B3E57 AS DateTime), 49, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (51, 94, CAST(0x0000A110008B4DEE AS DateTime), 46, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (52, 94, CAST(0x0000A110008B4F1A AS DateTime), 49, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (53, 94, CAST(0x0000A110008B5CC0 AS DateTime), 51, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (54, 94, CAST(0x0000A110008B5E27 AS DateTime), 52, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (55, 94, CAST(0x0000A110008B609C AS DateTime), 33, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (56, 94, CAST(0x0000A110008B6344 AS DateTime), 51, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (57, 94, CAST(0x0000A110008B650B AS DateTime), 52, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (58, 94, CAST(0x0000A110008B66C9 AS DateTime), 46, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (59, 94, CAST(0x0000A110008B6873 AS DateTime), 49, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (60, 94, CAST(0x0000A110008B7FE1 AS DateTime), 46, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (61, 94, CAST(0x0000A110008B81B3 AS DateTime), 49, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (62, 94, CAST(0x0000A110008BC1EE AS DateTime), 4, 5, N'', N'', N'3apvtdyh23nhdfaltdth2g55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (63, 94, CAST(0x0000A1100092EAA2 AS DateTime), 2, 5, N'', N'', N'rgrn2dnui5ekbrqqfqnmy4jv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (64, 94, CAST(0x0000A1100092EAAD AS DateTime), 1, 5, N'', N'', N'rgrn2dnui5ekbrqqfqnmy4jv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (65, 94, CAST(0x0000A1100092F050 AS DateTime), 46, 5, N'', N'', N'rgrn2dnui5ekbrqqfqnmy4jv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (66, 94, CAST(0x0000A1100092F1C0 AS DateTime), 49, 5, N'', N'', N'rgrn2dnui5ekbrqqfqnmy4jv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (67, 94, CAST(0x0000A110009346B3 AS DateTime), 4, 5, N'', N'', N'rgrn2dnui5ekbrqqfqnmy4jv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (68, 94, CAST(0x0000A11000AA3165 AS DateTime), 2, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (69, 94, CAST(0x0000A11000AA3170 AS DateTime), 1, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (70, 94, CAST(0x0000A11000AA33D4 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (71, 94, CAST(0x0000A11000AA350C AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (72, 94, CAST(0x0000A11000AA371C AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (73, 94, CAST(0x0000A11000AA3BC0 AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (74, 94, CAST(0x0000A11000AA3BCC AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (75, 94, CAST(0x0000A11000AA4CC6 AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (76, 94, CAST(0x0000A11000AA4CC6 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (77, 94, CAST(0x0000A11000AA4F1A AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (78, 94, CAST(0x0000A11000AA5390 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (79, 94, CAST(0x0000A11000AA5513 AS DateTime), 52, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (80, 94, CAST(0x0000A11000AA56BB AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (81, 94, CAST(0x0000A11000AA57EE AS DateTime), 53, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (82, 94, CAST(0x0000A11000AA6070 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (83, 94, CAST(0x0000A11000AA61A1 AS DateTime), 78, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (84, 94, CAST(0x0000A11000AA61FF AS DateTime), 79, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (85, 94, CAST(0x0000A11000AA635F AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (86, 94, CAST(0x0000A11000AA65BD AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (87, 94, CAST(0x0000A11000AA67CD AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (88, 94, CAST(0x0000A11000AA6CC0 AS DateTime), 32, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (89, 94, CAST(0x0000A11000AA6FE5 AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (90, 94, CAST(0x0000A11000AA711C AS DateTime), 31, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (91, 94, CAST(0x0000A11000AA77C1 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (92, 94, CAST(0x0000A11000AA7931 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (93, 94, CAST(0x0000A11000AA7B95 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (94, 94, CAST(0x0000A11000AA7C76 AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (95, 94, CAST(0x0000A11000AA86B9 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (96, 94, CAST(0x0000A11000AA898B AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (97, 94, CAST(0x0000A11000AA8ACA AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (98, 94, CAST(0x0000A11000AA8C1A AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (99, 94, CAST(0x0000A11000AA99FC AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (100, 94, CAST(0x0000A11000AA9CDF AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
GO
print 'Processed 100 total records'
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (101, 94, CAST(0x0000A11000ACE232 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (102, 94, CAST(0x0000A11000ACE39F AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (103, 94, CAST(0x0000A11000ACE5C1 AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (104, 94, CAST(0x0000A11000ACE8BE AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (105, 94, CAST(0x0000A11000ACE99A AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (106, 94, CAST(0x0000A11000ACEB30 AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (107, 94, CAST(0x0000A11000ACEB31 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Res�men de Ventas X D�a, , Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (108, 94, CAST(0x0000A11000ACEEDD AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (109, 94, CAST(0x0000A11000ACF064 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (110, 94, CAST(0x0000A11000ACF217 AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (111, 94, CAST(0x0000A11000ACF4E9 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (112, 94, CAST(0x0000A11000AD0C69 AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (113, 94, CAST(0x0000A11000AD0ECD AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (114, 94, CAST(0x0000A11000AD0ECF AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (115, 94, CAST(0x0000A11000AD2B2E AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (116, 94, CAST(0x0000A11000AD2B36 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (117, 94, CAST(0x0000A11000AD2D6C AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (118, 94, CAST(0x0000A11000AD3158 AS DateTime), 1, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (119, 94, CAST(0x0000A11000AD332C AS DateTime), 8, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (120, 94, CAST(0x0000A11000AD3585 AS DateTime), 1, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (121, 94, CAST(0x0000A11000AD36DC AS DateTime), 7, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (122, 94, CAST(0x0000A11000AD38B0 AS DateTime), 1, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (123, 94, CAST(0x0000A11000AD39F4 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (124, 94, CAST(0x0000A11000AD3B55 AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (125, 94, CAST(0x0000A11000B22ADA AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (126, 94, CAST(0x0000A11000B22ADB AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (127, 94, CAST(0x0000A11000B24865 AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (128, 94, CAST(0x0000A11000B24B86 AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (129, 94, CAST(0x0000A11000B24CF1 AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (130, 94, CAST(0x0000A11000B24EED AS DateTime), 50, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (131, 94, CAST(0x0000A11000B24EEE AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (132, 94, CAST(0x0000A11000B253AA AS DateTime), 49, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (133, 94, CAST(0x0000A11000B25527 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (134, 94, CAST(0x0000A11000B2580D AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (135, 94, CAST(0x0000A11000B2592E AS DateTime), 31, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (136, 94, CAST(0x0000A11000B25B5A AS DateTime), 46, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (137, 94, CAST(0x0000A11000B25C37 AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (138, 94, CAST(0x0000A11000B25D86 AS DateTime), 51, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (139, 94, CAST(0x0000A11000B25F15 AS DateTime), 33, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (140, 94, CAST(0x0000A11000B26D94 AS DateTime), 4, 5, N'', N'', N'ugncww45j5em5m2h3jv1vv45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (141, 93, CAST(0x0000A11000B9D726 AS DateTime), 2, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (142, 93, CAST(0x0000A11000B9D731 AS DateTime), 1, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (143, 93, CAST(0x0000A11000B9DD4F AS DateTime), 6, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (144, 93, CAST(0x0000A11000B9E4FC AS DateTime), 13, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (145, 93, CAST(0x0000A11000B9F390 AS DateTime), 14, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (146, 93, CAST(0x0000A11000BA2E7C AS DateTime), 15, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (147, 93, CAST(0x0000A11000BA2E91 AS DateTime), 15, 4, N'Usuario Editado. Datos[ txtUserName=John Jairo Escobar, txtLogin=jj.escobar, chkNewPassword=True, txtEmail=john.escobar@carvajal.com, ddlUserProfile=Producci�n, ddlUserStatus=Activo,  ]', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (148, 93, CAST(0x0000A11000BA3FD4 AS DateTime), 4, 5, N'', N'', N'40jojq45ueforce0m2q5zpzv')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (149, 95, CAST(0x0000A11000BA5D54 AS DateTime), 2, 5, N'', N'', N'fjryioilyjcydpvjhxwvhb55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (150, 95, CAST(0x0000A11000BA5D66 AS DateTime), 8, 5, N'', N'', N'fjryioilyjcydpvjhxwvhb55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (151, 95, CAST(0x0000A11000BA783D AS DateTime), 10, 5, N'', N'', N'fjryioilyjcydpvjhxwvhb55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (152, 95, CAST(0x0000A11000BA7D7E AS DateTime), 4, 5, N'', N'', N'fjryioilyjcydpvjhxwvhb55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (153, 95, CAST(0x0000A11000BA8837 AS DateTime), 2, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (154, 95, CAST(0x0000A11000BA8838 AS DateTime), 1, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (155, 95, CAST(0x0000A11000BB00D9 AS DateTime), 80, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (156, 95, CAST(0x0000A11000BB0235 AS DateTime), 81, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (157, 95, CAST(0x0000A11000BB0734 AS DateTime), 95, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (158, 95, CAST(0x0000A11000BB2C35 AS DateTime), 95, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (159, 95, CAST(0x0000A11000BB2C3A AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 01/11/2012 ]', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (160, 95, CAST(0x0000A11000BB3AC1 AS DateTime), 95, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (161, 95, CAST(0x0000A11000BB3AC4 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 01/11/2012 ]', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (162, 95, CAST(0x0000A11000BB7688 AS DateTime), 4, 5, N'', N'', N'l0gi4y55upcuqi55wkrwmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (163, 94, CAST(0x0000A11000BB87E4 AS DateTime), 2, 5, N'', N'', N'1guuan454mzr54552fwwflzz')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (164, 94, CAST(0x0000A11000BB87E6 AS DateTime), 1, 5, N'', N'', N'1guuan454mzr54552fwwflzz')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (165, 94, CAST(0x0000A11000BB8AF5 AS DateTime), 46, 5, N'', N'', N'1guuan454mzr54552fwwflzz')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (166, 94, CAST(0x0000A11000BB8BF1 AS DateTime), 49, 5, N'', N'', N'1guuan454mzr54552fwwflzz')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (167, 94, CAST(0x0000A11000BB915C AS DateTime), 4, 5, N'', N'', N'1guuan454mzr54552fwwflzz')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (168, 95, CAST(0x0000A11000BD2E0F AS DateTime), 2, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (169, 95, CAST(0x0000A11000BD2E1A AS DateTime), 1, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (170, 95, CAST(0x0000A11000BD31FC AS DateTime), 80, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (171, 95, CAST(0x0000A11000BD3397 AS DateTime), 81, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (172, 95, CAST(0x0000A11000BD35C3 AS DateTime), 95, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (173, 95, CAST(0x0000A11000BD4897 AS DateTime), 81, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (174, 95, CAST(0x0000A11000BD4A35 AS DateTime), 81, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (175, 95, CAST(0x0000A11000BD8900 AS DateTime), 4, 5, N'', N'', N'5ifbedfkfhg4wg554ruxrnf2')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (176, 94, CAST(0x0000A11000BD98FB AS DateTime), 2, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (177, 94, CAST(0x0000A11000BD9906 AS DateTime), 1, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (178, 94, CAST(0x0000A11000BD9B9C AS DateTime), 46, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (179, 94, CAST(0x0000A11000BD9C9A AS DateTime), 49, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (180, 94, CAST(0x0000A11000BD9EA6 AS DateTime), 50, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (181, 94, CAST(0x0000A11000BD9EA9 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (182, 94, CAST(0x0000A11000BDA79F AS DateTime), 49, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (183, 94, CAST(0x0000A11000BDAA1C AS DateTime), 4, 5, N'', N'', N'dirmql55ruoaznbg5zw0hlf5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (184, 95, CAST(0x0000A11000BDB7DE AS DateTime), 2, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (185, 95, CAST(0x0000A11000BDB7E3 AS DateTime), 1, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (186, 95, CAST(0x0000A11000BDBD56 AS DateTime), 51, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (187, 95, CAST(0x0000A11000BDBE58 AS DateTime), 52, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (188, 95, CAST(0x0000A11000BDC135 AS DateTime), 80, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (189, 95, CAST(0x0000A11000BDC2EB AS DateTime), 81, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (190, 95, CAST(0x0000A11000BDCD58 AS DateTime), 95, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (191, 95, CAST(0x0000A11000BE3EE2 AS DateTime), 4, 5, N'', N'', N'0h3r4z45oerd1zjxrolyjp45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (192, 95, CAST(0x0000A11000C05287 AS DateTime), 2, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (193, 95, CAST(0x0000A11000C0528C AS DateTime), 1, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (194, 95, CAST(0x0000A11000C05430 AS DateTime), 80, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (195, 95, CAST(0x0000A11000C055BC AS DateTime), 82, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (196, 95, CAST(0x0000A11000C05763 AS DateTime), 80, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (197, 95, CAST(0x0000A11000C058BA AS DateTime), 82, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (198, 95, CAST(0x0000A11000C05AB7 AS DateTime), 80, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (199, 95, CAST(0x0000A11000C05BBC AS DateTime), 81, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (200, 95, CAST(0x0000A11000C05E3A AS DateTime), 95, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (201, 95, CAST(0x0000A11000C39AC8 AS DateTime), 4, 5, N'', N'', N'hjkdyv55lak3wayvmrqg5445')
GO
print 'Processed 200 total records'
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (202, 95, CAST(0x0000A11000F0A9ED AS DateTime), 2, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (203, 95, CAST(0x0000A11000F0A9F9 AS DateTime), 1, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (204, 95, CAST(0x0000A11000F0AD40 AS DateTime), 80, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (205, 95, CAST(0x0000A11000F0AF7B AS DateTime), 81, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (206, 95, CAST(0x0000A11000F0B2C4 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (207, 95, CAST(0x0000A11000F0B2DF AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/21/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (208, 95, CAST(0x0000A11000F0BC18 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (209, 95, CAST(0x0000A11000F0C827 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (210, 95, CAST(0x0000A11000F0C82A AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/21/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (211, 95, CAST(0x0000A11000F0CBDF AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (212, 95, CAST(0x0000A11000F0CBE1 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/21/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (213, 95, CAST(0x0000A11000F0D6C6 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (214, 95, CAST(0x0000A11000F10D7C AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (215, 95, CAST(0x0000A11000F10D7D AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 01/11/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (216, 95, CAST(0x0000A11000F11193 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (217, 95, CAST(0x0000A11000F118A1 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (218, 95, CAST(0x0000A11000F118A4 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 01/11/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (219, 95, CAST(0x0000A11000F11D3B AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (220, 95, CAST(0x0000A11000F11D3C AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 01/11/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (221, 95, CAST(0x0000A11000F12605 AS DateTime), 95, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (222, 95, CAST(0x0000A11000F12608 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 01/11/2012 ]', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (223, 95, CAST(0x0000A11000F12F15 AS DateTime), 4, 5, N'', N'', N'cdb4t3vw42kfee55qff4im55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (224, 94, CAST(0x0000A11000F13B96 AS DateTime), 2, 5, N'', N'', N'wvpdi2yvragtzjqtfwv50k45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (225, 94, CAST(0x0000A11000F13BA5 AS DateTime), 1, 5, N'', N'', N'wvpdi2yvragtzjqtfwv50k45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (226, 94, CAST(0x0000A11000F13DAB AS DateTime), 46, 5, N'', N'', N'wvpdi2yvragtzjqtfwv50k45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (227, 94, CAST(0x0000A11000F13EAA AS DateTime), 49, 5, N'', N'', N'wvpdi2yvragtzjqtfwv50k45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (228, 94, CAST(0x0000A11000F154A6 AS DateTime), 4, 5, N'', N'', N'wvpdi2yvragtzjqtfwv50k45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (229, 95, CAST(0x0000A11000F1C6CF AS DateTime), 2, 5, N'', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (230, 95, CAST(0x0000A11000F1C6D2 AS DateTime), 1, 5, N'', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (231, 95, CAST(0x0000A11000F1C989 AS DateTime), 80, 5, N'', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (232, 95, CAST(0x0000A11000F1CA57 AS DateTime), 81, 5, N'', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (233, 95, CAST(0x0000A11000F1CFB3 AS DateTime), 95, 5, N'', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (234, 95, CAST(0x0000A11000F1CFC6 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/21/2012 ]', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (235, 95, CAST(0x0000A11000F21342 AS DateTime), 4, 5, N'', N'', N'zbwjov55uy0zb1552udkdw55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (236, 95, CAST(0x0000A11000F36A50 AS DateTime), 2, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (237, 95, CAST(0x0000A11000F36A52 AS DateTime), 1, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (238, 95, CAST(0x0000A11000F36D76 AS DateTime), 80, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (239, 95, CAST(0x0000A11000F36E99 AS DateTime), 81, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (240, 95, CAST(0x0000A11000F375D8 AS DateTime), 81, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (241, 95, CAST(0x0000A11000F37836 AS DateTime), 81, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (242, 95, CAST(0x0000A11000F37CEC AS DateTime), 95, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (243, 95, CAST(0x0000A11000F37CF0 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/23/2012 ]', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (244, 95, CAST(0x0000A11000F380FE AS DateTime), 95, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (245, 95, CAST(0x0000A11000F38100 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/21/2012 ]', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (246, 95, CAST(0x0000A11000F3881B AS DateTime), 95, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (247, 95, CAST(0x0000A11000F38825 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/22/2012 ]', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (248, 95, CAST(0x0000A11000F39B6D AS DateTime), 95, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (249, 95, CAST(0x0000A11000F39B70 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/19/2012 ]', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (250, 95, CAST(0x0000A11000F39FDB AS DateTime), 51, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (251, 95, CAST(0x0000A11000F3A183 AS DateTime), 52, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (252, 95, CAST(0x0000A11000F3A320 AS DateTime), 51, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (253, 95, CAST(0x0000A11000F3A400 AS DateTime), 53, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (254, 95, CAST(0x0000A11000F3A5DF AS DateTime), 80, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (255, 95, CAST(0x0000A11000F3A7D5 AS DateTime), 1, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (256, 95, CAST(0x0000A11000F3A906 AS DateTime), 80, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (257, 95, CAST(0x0000A11000F3A9FF AS DateTime), 81, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (258, 95, CAST(0x0000A11000F3AE87 AS DateTime), 95, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (259, 95, CAST(0x0000A11000F3AE89 AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/22/2012 ]', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (260, 95, CAST(0x0000A11000F3B1E7 AS DateTime), 80, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (261, 95, CAST(0x0000A11000F4A360 AS DateTime), 82, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (262, 95, CAST(0x0000A11000F4ABCC AS DateTime), 94, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (263, 95, CAST(0x0000A11000FA78C5 AS DateTime), 4, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (264, 94, CAST(0x0000A110010A8DCA AS DateTime), 2, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (265, 94, CAST(0x0000A110010A8DD9 AS DateTime), 1, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (266, 94, CAST(0x0000A110010A9079 AS DateTime), 46, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (267, 94, CAST(0x0000A110010A91B3 AS DateTime), 49, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (268, 94, CAST(0x0000A110010A9D0F AS DateTime), 50, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (269, 94, CAST(0x0000A110010A9D12 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 9/24/2012, Fecha Fin: 9/24/2012 ]', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (270, 94, CAST(0x0000A110010AA0F3 AS DateTime), 49, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (271, 94, CAST(0x0000A110010AA5EB AS DateTime), 4, 5, N'', N'', N'wcypxa45wkpxac45v0bh25f0')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (272, 94, CAST(0x0000A110010E616E AS DateTime), 2, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (273, 94, CAST(0x0000A110010E6179 AS DateTime), 1, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (274, 94, CAST(0x0000A110010E6475 AS DateTime), 46, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (275, 94, CAST(0x0000A110010E6672 AS DateTime), 49, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (276, 94, CAST(0x0000A110010E6892 AS DateTime), 50, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (277, 94, CAST(0x0000A110010E6897 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Res�men de Ventas X D�a, , Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (278, 94, CAST(0x0000A110010E734C AS DateTime), 49, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (279, 94, CAST(0x0000A110010E76A0 AS DateTime), 46, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (280, 94, CAST(0x0000A110010E7810 AS DateTime), 49, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (281, 94, CAST(0x0000A110010E7A2C AS DateTime), 50, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (282, 94, CAST(0x0000A110010E7A2D AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/21/2012, Fecha Fin: 11/21/2012 ]', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (283, 94, CAST(0x0000A110010E7E3F AS DateTime), 49, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (284, 94, CAST(0x0000A110010E7FEF AS DateTime), 51, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (285, 94, CAST(0x0000A110010E819F AS DateTime), 52, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (286, 94, CAST(0x0000A110010E8389 AS DateTime), 51, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (287, 94, CAST(0x0000A110010E846C AS DateTime), 53, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (288, 94, CAST(0x0000A110010E8BB9 AS DateTime), 51, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (289, 94, CAST(0x0000A110010E8CD2 AS DateTime), 46, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (290, 94, CAST(0x0000A110010E8E42 AS DateTime), 33, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (291, 94, CAST(0x0000A110010E8F5E AS DateTime), 32, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (292, 94, CAST(0x0000A110010E9167 AS DateTime), 33, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (293, 94, CAST(0x0000A110010E923C AS DateTime), 31, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (294, 94, CAST(0x0000A110010EA30F AS DateTime), 46, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (295, 94, CAST(0x0000A110010EA429 AS DateTime), 51, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (296, 94, CAST(0x0000A110010EA669 AS DateTime), 46, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (297, 94, CAST(0x0000A110010EA7E3 AS DateTime), 4, 5, N'', N'', N'0sppanbyif54renky04a3qii')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (298, 95, CAST(0x0000A1100112704B AS DateTime), 2, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (299, 95, CAST(0x0000A1100112704E AS DateTime), 1, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (300, 95, CAST(0x0000A110011273EB AS DateTime), 80, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (301, 95, CAST(0x0000A110011275C8 AS DateTime), 82, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (302, 95, CAST(0x0000A11001127E3C AS DateTime), 94, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
GO
print 'Processed 300 total records'
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (303, 95, CAST(0x0000A110011298B3 AS DateTime), 80, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (304, 95, CAST(0x0000A11001129A48 AS DateTime), 81, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (305, 95, CAST(0x0000A11001129BD5 AS DateTime), 95, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (306, 95, CAST(0x0000A11001129BEE AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/21/2012 ]', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (307, 95, CAST(0x0000A11001129DE7 AS DateTime), 80, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (308, 95, CAST(0x0000A11001129EEA AS DateTime), 82, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (309, 95, CAST(0x0000A1100112A4A9 AS DateTime), 94, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (310, 95, CAST(0x0000A1100112F459 AS DateTime), 4, 5, N'', N'', N'2mrknz2qkteana55gfvdjf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (311, 95, CAST(0x0000A110011385FA AS DateTime), 2, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (312, 95, CAST(0x0000A110011385FD AS DateTime), 1, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (313, 95, CAST(0x0000A110011388F6 AS DateTime), 80, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (314, 95, CAST(0x0000A11001138F6B AS DateTime), 82, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (315, 95, CAST(0x0000A110011393EB AS DateTime), 94, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (316, 95, CAST(0x0000A11001166BB8 AS DateTime), 80, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (317, 95, CAST(0x0000A11001166CDD AS DateTime), 51, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (318, 95, CAST(0x0000A11001166E52 AS DateTime), 52, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (319, 95, CAST(0x0000A11001167018 AS DateTime), 51, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (320, 95, CAST(0x0000A11001167151 AS DateTime), 80, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (321, 95, CAST(0x0000A110011673F0 AS DateTime), 4, 5, N'', N'', N'eqc524egvz4hyv454oo1iu45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (322, 94, CAST(0x0000A111008AB250 AS DateTime), 2, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (323, 94, CAST(0x0000A111008AB261 AS DateTime), 1, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (324, 94, CAST(0x0000A111008AB5D4 AS DateTime), 33, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (325, 94, CAST(0x0000A111008AB749 AS DateTime), 46, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (326, 94, CAST(0x0000A111008AB86B AS DateTime), 49, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (327, 94, CAST(0x0000A111008ABA65 AS DateTime), 50, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (328, 94, CAST(0x0000A111008ABA67 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/22/2012, Fecha Fin: 11/22/2012 ]', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (329, 94, CAST(0x0000A111008ABDEA AS DateTime), 49, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (330, 94, CAST(0x0000A111008ABF1E AS DateTime), 51, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (331, 94, CAST(0x0000A111008ABFE5 AS DateTime), 52, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (332, 94, CAST(0x0000A111008AC100 AS DateTime), 51, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (333, 94, CAST(0x0000A111008AC1D8 AS DateTime), 53, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (334, 94, CAST(0x0000A111008AC302 AS DateTime), 46, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (335, 94, CAST(0x0000A111008AC42B AS DateTime), 49, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (336, 94, CAST(0x0000A111008AC559 AS DateTime), 33, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (337, 94, CAST(0x0000A111008AC603 AS DateTime), 32, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (338, 94, CAST(0x0000A111008AC88C AS DateTime), 33, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (339, 94, CAST(0x0000A111008ACA7C AS DateTime), 1, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (340, 94, CAST(0x0000A111008ACBC9 AS DateTime), 7, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (341, 94, CAST(0x0000A111008ACDA0 AS DateTime), 9, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (342, 94, CAST(0x0000A111008ACDC5 AS DateTime), 9, 4, N'Cuenta Editada. Datos[ txtLogin=dm.burgos, txtUserName=Daisy Madelene Burgos , txtEmail=daisy.burgos@carvajal.com, ddlUserProfile=Operaciones,  ]', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (343, 94, CAST(0x0000A111008AD1BB AS DateTime), 1, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (344, 94, CAST(0x0000A111008AD2CE AS DateTime), 8, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (345, 94, CAST(0x0000A111008AD482 AS DateTime), 33, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (346, 94, CAST(0x0000A111008AD657 AS DateTime), 4, 5, N'', N'', N'2y5bsl55it3bnurpqiu0ly2v')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (347, 97, CAST(0x0000A111008AE8D1 AS DateTime), 2, 5, N'', N'', N'b6dfeb1c61a5e8537be23902d2aaccdd')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (348, 97, CAST(0x0000A111008BB84E AS DateTime), 4, 5, N'', N'', N'b6dfeb1c61a5e8537be23902d2aaccdd')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (349, 97, CAST(0x0000A111008DE1C3 AS DateTime), 2, 5, N'', N'', N'2642633037fe21235207befb7ccb9ec5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (350, 95, CAST(0x0000A11100916793 AS DateTime), 2, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (351, 95, CAST(0x0000A111009167D8 AS DateTime), 1, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (352, 95, CAST(0x0000A11100916B3D AS DateTime), 80, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (353, 95, CAST(0x0000A11100916C6E AS DateTime), 82, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (354, 95, CAST(0x0000A11100917329 AS DateTime), 94, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (355, 95, CAST(0x0000A11100917E4D AS DateTime), 80, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (356, 95, CAST(0x0000A11100917F79 AS DateTime), 81, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (357, 95, CAST(0x0000A111009180FD AS DateTime), 95, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (358, 95, CAST(0x0000A1110091811E AS DateTime), 95, 4, N'Proceso de Generaci�n Archivo Movilred Recargas Realizado. Datos[ No hay transacciones de Movilred para la fecha: 11/22/2012 ]', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (359, 95, CAST(0x0000A1110091877A AS DateTime), 4, 5, N'', N'', N'fzmtfb452kwlayuyv23s0u55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (360, 97, CAST(0x0000A11100921BBB AS DateTime), 5, 4, N'Par�metros de MOVILRED_FORMATTER Modificados. Datos [ Puerto Local=6661   ::   Hilos Min.=50   ::   Hilos Max.=100   ::   IP Remota1=192.168.156.77   ::   Puerto Remoto1=49394   ::   Timeout Remoto1=60000 ]', N'Par�metros Adicionales de MOVILRED_FORMATTER Modificados. Datos [ NIT=950226658   ::   Usuario=600111   ::   Password=******   ::   IP Remota2=127.0.0.1   ::   Puerto Remoto2=9691   ::   Timeout Remoto2=30000   ::   IP Remota3=127.0.0.3   ::   Puerto', N'2642633037fe21235207befb7ccb9ec5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (361, 97, CAST(0x0000A11100922EFB AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Iniciado.', N'', N'2642633037fe21235207befb7ccb9ec5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (362, 97, CAST(0x0000A11100924741 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'2642633037fe21235207befb7ccb9ec5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (363, 97, CAST(0x0000A1110092809E AS DateTime), 4, 5, N'', N'', N'2642633037fe21235207befb7ccb9ec5')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (364, 97, CAST(0x0000A111009BAAD1 AS DateTime), 2, 5, N'', N'', N'60d4938305de069b0830b44684fe8a58')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (365, 97, CAST(0x0000A111009C67C8 AS DateTime), 4, 5, N'', N'', N'60d4938305de069b0830b44684fe8a58')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (366, 97, CAST(0x0000A11100EBBFE9 AS DateTime), 2, 5, N'', N'', N'2872430c86bf5cb8c1a9d23b1aff17e8')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (367, 97, CAST(0x0000A11100EEE5AE AS DateTime), 4, 5, N'', N'', N'2872430c86bf5cb8c1a9d23b1aff17e8')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (368, 94, CAST(0x0000A1120085576F AS DateTime), 2, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (369, 94, CAST(0x0000A11200855781 AS DateTime), 1, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (370, 94, CAST(0x0000A1120085617D AS DateTime), 8, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (371, 94, CAST(0x0000A112008562F6 AS DateTime), 46, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (372, 94, CAST(0x0000A112008564D3 AS DateTime), 33, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (373, 94, CAST(0x0000A112008566BC AS DateTime), 32, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (374, 94, CAST(0x0000A11200856906 AS DateTime), 46, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (375, 94, CAST(0x0000A11200856B26 AS DateTime), 33, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (376, 94, CAST(0x0000A11200856C03 AS DateTime), 31, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (377, 94, CAST(0x0000A112008580D3 AS DateTime), 46, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (378, 94, CAST(0x0000A11200858255 AS DateTime), 51, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (379, 94, CAST(0x0000A112008583D8 AS DateTime), 52, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (380, 94, CAST(0x0000A11200858562 AS DateTime), 51, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (381, 94, CAST(0x0000A1120085861A AS DateTime), 53, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (382, 94, CAST(0x0000A112008587CD AS DateTime), 51, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (383, 94, CAST(0x0000A112008588B4 AS DateTime), 78, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (384, 94, CAST(0x0000A112008588D6 AS DateTime), 79, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (385, 94, CAST(0x0000A11200858A01 AS DateTime), 51, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (386, 94, CAST(0x0000A11200858AC7 AS DateTime), 89, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (387, 94, CAST(0x0000A11200858BE8 AS DateTime), 51, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (388, 94, CAST(0x0000A11200858CC4 AS DateTime), 33, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (389, 94, CAST(0x0000A11200858DE9 AS DateTime), 46, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (390, 94, CAST(0x0000A11200858F28 AS DateTime), 33, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (391, 94, CAST(0x0000A1120085AB96 AS DateTime), 46, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (392, 94, CAST(0x0000A1120085AC98 AS DateTime), 49, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (393, 94, CAST(0x0000A1120085AE38 AS DateTime), 50, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (394, 94, CAST(0x0000A1120085AE3B AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/23/2012, Fecha Fin: 11/23/2012 ]', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (395, 94, CAST(0x0000A1120085B16A AS DateTime), 49, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (396, 94, CAST(0x0000A1120085B6FA AS DateTime), 46, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (397, 94, CAST(0x0000A1120085B812 AS DateTime), 49, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (398, 94, CAST(0x0000A1120085B929 AS DateTime), 51, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (399, 94, CAST(0x0000A1120085BA89 AS DateTime), 33, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (400, 94, CAST(0x0000A1120085C386 AS DateTime), 4, 5, N'', N'', N'02m11x45bvadqd55xaspdi45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (401, 95, CAST(0x0000A112008B8C34 AS DateTime), 2, 5, N'', N'', N'y4xj3o453wqmqlmqtmz4nd55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (402, 95, CAST(0x0000A112008B8C41 AS DateTime), 1, 5, N'', N'', N'y4xj3o453wqmqlmqtmz4nd55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (403, 95, CAST(0x0000A112008B8FD3 AS DateTime), 80, 5, N'', N'', N'y4xj3o453wqmqlmqtmz4nd55')
GO
print 'Processed 400 total records'
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (404, 95, CAST(0x0000A112008B9188 AS DateTime), 82, 5, N'', N'', N'y4xj3o453wqmqlmqtmz4nd55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (405, 95, CAST(0x0000A112008BAC41 AS DateTime), 94, 5, N'', N'', N'y4xj3o453wqmqlmqtmz4nd55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (406, 95, CAST(0x0000A112008C44F3 AS DateTime), 4, 5, N'', N'', N'y4xj3o453wqmqlmqtmz4nd55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (407, 95, CAST(0x0000A112008CD4A1 AS DateTime), 2, 5, N'', N'', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (408, 95, CAST(0x0000A112008CD4A8 AS DateTime), 1, 5, N'', N'', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (409, 95, CAST(0x0000A112008CD867 AS DateTime), 80, 5, N'', N'', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (410, 95, CAST(0x0000A112008CD9E0 AS DateTime), 82, 5, N'', N'', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (411, 95, CAST(0x0000A112008CF4CD AS DateTime), 94, 5, N'', N'', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (412, 95, CAST(0x0000A112008D415B AS DateTime), 4, 5, N'', N'', N'ckaot345irstloqwujd5ciue')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (413, 95, CAST(0x0000A112008F0041 AS DateTime), 2, 5, N'', N'', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (414, 95, CAST(0x0000A112008F0055 AS DateTime), 1, 5, N'', N'', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (415, 95, CAST(0x0000A112008F03C8 AS DateTime), 80, 5, N'', N'', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (416, 95, CAST(0x0000A112008F11C2 AS DateTime), 82, 5, N'', N'', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (417, 95, CAST(0x0000A112008F1712 AS DateTime), 94, 5, N'', N'', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (418, 95, CAST(0x0000A1120094C19B AS DateTime), 4, 5, N'', N'', N'h3q1kmz4oqqpp555jhxpq1ei')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (419, 95, CAST(0x0000A11200961511 AS DateTime), 2, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (420, 95, CAST(0x0000A1120096151B AS DateTime), 1, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (421, 95, CAST(0x0000A112009617E4 AS DateTime), 80, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (422, 95, CAST(0x0000A11200961944 AS DateTime), 82, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (423, 95, CAST(0x0000A112009621C2 AS DateTime), 94, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (424, 95, CAST(0x0000A1120098BD14 AS DateTime), 80, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (425, 95, CAST(0x0000A1120098BE36 AS DateTime), 82, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (426, 95, CAST(0x0000A1120098D7D4 AS DateTime), 4, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (427, 95, CAST(0x0000A1120099014F AS DateTime), 2, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (428, 95, CAST(0x0000A1120099015D AS DateTime), 1, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (429, 95, CAST(0x0000A11200990414 AS DateTime), 80, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (430, 95, CAST(0x0000A1120099052D AS DateTime), 82, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (431, 95, CAST(0x0000A11200990AFF AS DateTime), 94, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (432, 95, CAST(0x0000A112009951BB AS DateTime), 80, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (433, 95, CAST(0x0000A112009952E7 AS DateTime), 82, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (434, 95, CAST(0x0000A11200998B5D AS DateTime), 4, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (435, 95, CAST(0x0000A1120099A7B3 AS DateTime), 2, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (436, 95, CAST(0x0000A1120099A7C0 AS DateTime), 1, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (437, 95, CAST(0x0000A1120099A9F1 AS DateTime), 80, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (438, 95, CAST(0x0000A1120099AB67 AS DateTime), 82, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (439, 95, CAST(0x0000A1120099B115 AS DateTime), 94, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (440, 95, CAST(0x0000A112009B6F34 AS DateTime), 4, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (441, 94, CAST(0x0000A112009E10FD AS DateTime), 2, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (442, 94, CAST(0x0000A112009E1107 AS DateTime), 1, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (443, 94, CAST(0x0000A112009E139F AS DateTime), 33, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (444, 94, CAST(0x0000A112009E14ED AS DateTime), 32, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (445, 94, CAST(0x0000A112009E17B6 AS DateTime), 33, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (446, 94, CAST(0x0000A112009E189F AS DateTime), 31, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (447, 94, CAST(0x0000A112009E1B8D AS DateTime), 46, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (448, 94, CAST(0x0000A112009E1CB4 AS DateTime), 49, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (449, 94, CAST(0x0000A112009E1F94 AS DateTime), 51, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (450, 94, CAST(0x0000A112009E21AE AS DateTime), 89, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (451, 94, CAST(0x0000A112009E23E0 AS DateTime), 51, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (452, 94, CAST(0x0000A112009E24C1 AS DateTime), 53, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (453, 94, CAST(0x0000A112009E2664 AS DateTime), 51, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (454, 94, CAST(0x0000A112009E2732 AS DateTime), 33, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (455, 94, CAST(0x0000A112009E2F23 AS DateTime), 4, 5, N'', N'', N'ugfxkn55b2gjp1454ydaq555')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (456, 95, CAST(0x0000A11200A2D2A3 AS DateTime), 2, 5, N'', N'', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (457, 95, CAST(0x0000A11200A2D2A5 AS DateTime), 1, 5, N'', N'', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (458, 95, CAST(0x0000A11200A2D7C7 AS DateTime), 80, 5, N'', N'', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (459, 95, CAST(0x0000A11200A2D901 AS DateTime), 82, 5, N'', N'', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (460, 95, CAST(0x0000A11200A2DDFD AS DateTime), 94, 5, N'', N'', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (461, 95, CAST(0x0000A11200A80277 AS DateTime), 4, 5, N'', N'', N'cbevdg55q2rvix553vck2u45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (462, 95, CAST(0x0000A11200A876A5 AS DateTime), 2, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (463, 95, CAST(0x0000A11200A876B0 AS DateTime), 1, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (464, 95, CAST(0x0000A11200A87A4E AS DateTime), 80, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (465, 95, CAST(0x0000A11200A87C61 AS DateTime), 82, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (466, 95, CAST(0x0000A11200A884A7 AS DateTime), 94, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (467, 95, CAST(0x0000A11200A887E1 AS DateTime), 94, 4, N'Proceso de Conciliaci�n Movilred Realizado. Datos[ Fecha = 9/24/2012. Diferencias Conred->Movilred: Nro. Trx=0; Valor= $$ 0.  ::  Diferencias Movilred->:Conred: Nro. Trx=26; Valor= $$ 70,000 ]', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (468, 95, CAST(0x0000A11200A8D78E AS DateTime), 51, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (469, 95, CAST(0x0000A11200A8D98E AS DateTime), 80, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (470, 95, CAST(0x0000A11200A8DAC8 AS DateTime), 82, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (471, 95, CAST(0x0000A11200A8E17A AS DateTime), 94, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (472, 95, CAST(0x0000A11200A8E2F4 AS DateTime), 94, 4, N'Proceso de Conciliaci�n Movilred Realizado. Datos[ Fecha = 9/24/2012. Diferencias Conred->Movilred: Nro. Trx=0; Valor= $$ 0.  ::  Diferencias Movilred->:Conred: Nro. Trx=26; Valor= $$ 70,000 ]', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (473, 95, CAST(0x0000A11200A8EFD0 AS DateTime), 4, 5, N'', N'', N'4woyxffhpcdwrw45mo3pdp31')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (474, 95, CAST(0x0000A11200A94ACD AS DateTime), 2, 5, N'', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (475, 95, CAST(0x0000A11200A94AD7 AS DateTime), 1, 5, N'', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (476, 95, CAST(0x0000A11200A94F4A AS DateTime), 80, 5, N'', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (477, 95, CAST(0x0000A11200A9505F AS DateTime), 82, 5, N'', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (478, 95, CAST(0x0000A11200A9CD43 AS DateTime), 94, 5, N'', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (479, 95, CAST(0x0000A11200A9D03A AS DateTime), 94, 4, N'Proceso de Conciliaci�n Movilred Realizado. Datos[ Fecha = 9/24/2012. Diferencias Conred->Movilred: Nro. Trx=0; Valor= $$ 0.  ::  Diferencias Movilred->:Conred: Nro. Trx=26; Valor= $$ 70,000 ]', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (480, 95, CAST(0x0000A11200A9DEDD AS DateTime), 4, 5, N'', N'', N'kmimsjzrns4nw3yquj5fmf55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (481, 94, CAST(0x0000A11200AAFA2E AS DateTime), 2, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (482, 94, CAST(0x0000A11200AAFA31 AS DateTime), 1, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (483, 94, CAST(0x0000A11200AAFC6A AS DateTime), 33, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (484, 94, CAST(0x0000A11200AAFD19 AS DateTime), 46, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (485, 94, CAST(0x0000A11200AAFDBE AS DateTime), 51, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (486, 94, CAST(0x0000A11200AAFFAE AS DateTime), 89, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (487, 94, CAST(0x0000A11200AB0118 AS DateTime), 46, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (488, 94, CAST(0x0000A11200AB01B7 AS DateTime), 33, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (489, 94, CAST(0x0000A11200AB039A AS DateTime), 4, 5, N'', N'', N'gcxtnizmzaff0h552meabxrx')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (490, 94, CAST(0x0000A11500D4BA16 AS DateTime), 2, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (491, 94, CAST(0x0000A11500D4BA3D AS DateTime), 1, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (492, 94, CAST(0x0000A11500D4D13E AS DateTime), 51, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (493, 94, CAST(0x0000A11500D4D280 AS DateTime), 53, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (494, 94, CAST(0x0000A11500D4D464 AS DateTime), 46, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (495, 94, CAST(0x0000A11500D4D5E0 AS DateTime), 33, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (496, 94, CAST(0x0000A11500D4D636 AS DateTime), 33, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (497, 94, CAST(0x0000A11500D4D6F9 AS DateTime), 46, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (498, 94, CAST(0x0000A11500D4D7C8 AS DateTime), 49, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (499, 94, CAST(0x0000A11500D4DA1F AS DateTime), 50, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (500, 94, CAST(0x0000A11500D4DA53 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/26/2012, Fecha Fin: 11/26/2012 ]', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (501, 94, CAST(0x0000A11500D4E5AF AS DateTime), 49, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (502, 94, CAST(0x0000A11500D4E736 AS DateTime), 46, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (503, 94, CAST(0x0000A11500D4E7F6 AS DateTime), 49, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (504, 94, CAST(0x0000A11500D4E9B0 AS DateTime), 50, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
GO
print 'Processed 500 total records'
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (505, 94, CAST(0x0000A11500D4E9B1 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Res�men de Ventas X D�a, , Fecha Inicial: 11/26/2012, Fecha Fin: 11/26/2012 ]', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (506, 94, CAST(0x0000A11500D4EDD6 AS DateTime), 49, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (507, 94, CAST(0x0000A11500D4EEDA AS DateTime), 33, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (508, 94, CAST(0x0000A11500D4EFF4 AS DateTime), 32, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (509, 94, CAST(0x0000A11500D4F2E7 AS DateTime), 33, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (510, 94, CAST(0x0000A11500D4F3D9 AS DateTime), 31, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (511, 94, CAST(0x0000A11500D502CC AS DateTime), 46, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (512, 94, CAST(0x0000A11500D50AD7 AS DateTime), 4, 5, N'', N'', N'yejqy545apocw0r41dqrta45')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (513, 94, CAST(0x0000A11500D6DB68 AS DateTime), 2, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (514, 94, CAST(0x0000A11500D6DB6A AS DateTime), 1, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (515, 94, CAST(0x0000A11500D6DDC9 AS DateTime), 46, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (516, 94, CAST(0x0000A11500D6DEFE AS DateTime), 49, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (517, 94, CAST(0x0000A11500D6E1D8 AS DateTime), 50, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (518, 94, CAST(0x0000A11500D6E1D9 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Ventas Detalladas X Comercios, Comercio: Todos los Comercios, Fecha Inicial: 11/26/2012, Fecha Fin: 11/26/2012 ]', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (519, 94, CAST(0x0000A11500D6E86C AS DateTime), 49, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (520, 94, CAST(0x0000A11500D6EAB3 AS DateTime), 46, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (521, 94, CAST(0x0000A11500D6EBA4 AS DateTime), 49, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (522, 94, CAST(0x0000A11500D6ED0E AS DateTime), 50, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (523, 94, CAST(0x0000A11500D6ED1A AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Res�men de Ventas X D�a, , Fecha Inicial: 11/26/2012, Fecha Fin: 11/26/2012 ]', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (524, 94, CAST(0x0000A11500D6F4C5 AS DateTime), 4, 5, N'', N'', N'4ws34p3cwphvdcmt3hvypuix')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (525, 94, CAST(0x0000A11500D72755 AS DateTime), 2, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (526, 94, CAST(0x0000A11500D72758 AS DateTime), 1, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (527, 94, CAST(0x0000A11500D728E9 AS DateTime), 46, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (528, 94, CAST(0x0000A11500D729BF AS DateTime), 49, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (529, 94, CAST(0x0000A11500D72B6B AS DateTime), 50, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (530, 94, CAST(0x0000A11500D72B6C AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Res�men de Ventas X D�a, , Fecha Inicial: 11/26/2012, Fecha Fin: 11/26/2012 ]', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (531, 94, CAST(0x0000A11500D73212 AS DateTime), 49, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (532, 94, CAST(0x0000A11500D7335B AS DateTime), 51, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (533, 94, CAST(0x0000A11500D7346A AS DateTime), 53, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (534, 94, CAST(0x0000A11500D735D0 AS DateTime), 51, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (535, 94, CAST(0x0000A11500D73698 AS DateTime), 52, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (536, 94, CAST(0x0000A11500D737EC AS DateTime), 51, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (537, 94, CAST(0x0000A11500D738B1 AS DateTime), 89, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (538, 94, CAST(0x0000A11500D739D2 AS DateTime), 51, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (539, 94, CAST(0x0000A11500D73A9A AS DateTime), 46, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (540, 94, CAST(0x0000A11500D73B47 AS DateTime), 33, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (541, 94, CAST(0x0000A11500D73D7D AS DateTime), 4, 5, N'', N'', N'f2ck3zejlzukqqy2t2n2l0ma')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (542, 97, CAST(0x0000A11500EFBB4B AS DateTime), 2, 5, N'', N'', N'052cec81e1cc01808f7f5d43c8bcc574')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (543, 97, CAST(0x0000A11500F51C27 AS DateTime), 4, 5, N'', N'', N'052cec81e1cc01808f7f5d43c8bcc574')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (544, 93, CAST(0x0000A11500F6E5FB AS DateTime), 2, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (545, 93, CAST(0x0000A11500F6E606 AS DateTime), 1, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (546, 93, CAST(0x0000A11500F6EB5B AS DateTime), 6, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (547, 93, CAST(0x0000A11500F6EDB1 AS DateTime), 13, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (548, 93, CAST(0x0000A11500F704FC AS DateTime), 54, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (549, 93, CAST(0x0000A11500F70673 AS DateTime), 56, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (550, 93, CAST(0x0000A11500F708CF AS DateTime), 66, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (551, 93, CAST(0x0000A11500F70B3B AS DateTime), 46, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (552, 93, CAST(0x0000A11500F70CDA AS DateTime), 49, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (553, 93, CAST(0x0000A11500F7109C AS DateTime), 50, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (554, 93, CAST(0x0000A11500F710A2 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Log de Auditor�a, Usuario: Todos los Usuarios, Fecha Inicial: 11/26/2012, Fecha Fin: 11/26/2012 ]', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (555, 93, CAST(0x0000A11500F7270C AS DateTime), 49, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (556, 93, CAST(0x0000A11500F736B3 AS DateTime), 50, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (557, 93, CAST(0x0000A11500F736B4 AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Log de Auditor�a, Usuario: ed.beltran - Elkin David Beltr�n Duque, Fecha Inicial: 11/22/2012, Fecha Fin: 11/23/2012 ]', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (558, 93, CAST(0x0000A11500F74A6B AS DateTime), 49, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (559, 93, CAST(0x0000A11500F75699 AS DateTime), 50, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (560, 93, CAST(0x0000A11500F7569A AS DateTime), 50, 4, N'Reporte Generado. Datos[ Reporte: Log de Auditor�a, Usuario: ed.beltran - Elkin David Beltr�n Duque, Fecha Inicial: 11/21/2012, Fecha Fin: 11/26/2012 ]', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (561, 93, CAST(0x0000A11500F768DE AS DateTime), 49, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (562, 93, CAST(0x0000A11500F76A23 AS DateTime), 54, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (563, 93, CAST(0x0000A11500F76C45 AS DateTime), 6, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (564, 93, CAST(0x0000A11500F76D9F AS DateTime), 13, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (565, 93, CAST(0x0000A11500F7AB35 AS DateTime), 46, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (566, 93, CAST(0x0000A11500F7AC9C AS DateTime), 54, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (567, 93, CAST(0x0000A11500F7AD78 AS DateTime), 66, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (568, 93, CAST(0x0000A11500F7AECF AS DateTime), 67, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (569, 93, CAST(0x0000A11500F7AFFC AS DateTime), 54, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (570, 93, CAST(0x0000A11500F7B096 AS DateTime), 46, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (571, 93, CAST(0x0000A11500F7B11E AS DateTime), 6, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (572, 93, CAST(0x0000A11500F7B252 AS DateTime), 1, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (573, 93, CAST(0x0000A11500F7BC44 AS DateTime), 6, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (574, 93, CAST(0x0000A11500F7BD82 AS DateTime), 13, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (575, 93, CAST(0x0000A11500F7DECE AS DateTime), 4, 5, N'', N'', N'g0gwh045wx45y3vmouc4ts55')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (576, 97, CAST(0x0000A11500FF24FC AS DateTime), 2, 5, N'', N'', N'fac26a703007e0cdc5007e1308db20ca')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (577, 97, CAST(0x0000A1150103424D AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Iniciado.', N'', N'fac26a703007e0cdc5007e1308db20ca')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (578, 97, CAST(0x0000A11501034BC7 AS DateTime), 4, 5, N'', N'', N'fac26a703007e0cdc5007e1308db20ca')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (579, 97, CAST(0x0000A115010BC694 AS DateTime), 2, 5, N'', N'', N'd29e2962d2ebe59b001df836d7ada5cf')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (580, 97, CAST(0x0000A115010BD686 AS DateTime), 4, 5, N'', N'', N'd29e2962d2ebe59b001df836d7ada5cf')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (581, 97, CAST(0x0000A11600790827 AS DateTime), 2, 5, N'', N'', N'18953fa0315f9492186046696a027830')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (582, 97, CAST(0x0000A11600793227 AS DateTime), 4, 5, N'', N'', N'18953fa0315f9492186046696a027830')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (583, 97, CAST(0x0000A116007DC18B AS DateTime), 2, 5, N'', N'', N'538c996cc4d1087798905482abfdc8be')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (584, 97, CAST(0x0000A116007DE678 AS DateTime), 5, 4, N'Par�metros de SERVIDOR CONRED Modificados. Datos [ Puerto Local=1975   ::   Hilos Min.=50   ::   Hilos Max.=100   ::   Timeout=62000 ]', N'', N'538c996cc4d1087798905482abfdc8be')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (585, 97, CAST(0x0000A116007DEBA2 AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Detenido.', N'', N'538c996cc4d1087798905482abfdc8be')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (586, 97, CAST(0x0000A116007DF06D AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Iniciado.', N'', N'538c996cc4d1087798905482abfdc8be')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (587, 97, CAST(0x0000A116007E6E2C AS DateTime), 4, 5, N'', N'', N'538c996cc4d1087798905482abfdc8be')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (588, 94, CAST(0x0000A116007F1286 AS DateTime), 2, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (589, 94, CAST(0x0000A116007F1295 AS DateTime), 1, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (590, 94, CAST(0x0000A116007F14DE AS DateTime), 8, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (591, 94, CAST(0x0000A116007F2718 AS DateTime), 46, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (592, 94, CAST(0x0000A116007F2867 AS DateTime), 33, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (593, 94, CAST(0x0000A116007F29E4 AS DateTime), 51, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (594, 94, CAST(0x0000A116007F2BD2 AS DateTime), 4, 5, N'', N'', N'ziuzdh452j0sm12wiknliief')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (595, 97, CAST(0x0000A116008628EE AS DateTime), 2, 5, N'', N'', N'db45ffbf8ae7042a938d4764a0fe2274')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (596, 97, CAST(0x0000A1160086334A AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Iniciado.', N'', N'db45ffbf8ae7042a938d4764a0fe2274')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (597, 97, CAST(0x0000A116008647BD AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'db45ffbf8ae7042a938d4764a0fe2274')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (598, 97, CAST(0x0000A11600867BEC AS DateTime), 4, 5, N'', N'', N'db45ffbf8ae7042a938d4764a0fe2274')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (599, 97, CAST(0x0000A116011363C2 AS DateTime), 2, 5, N'', N'', N'5a00e4bf2de465a8b7d217f265e3c563')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (600, 97, CAST(0x0000A1160114D8DF AS DateTime), 4, 5, N'', N'', N'5a00e4bf2de465a8b7d217f265e3c563')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (601, 97, CAST(0x0000A117006D7F88 AS DateTime), 2, 5, N'', N'', N'35853a0b58b90e6f6ee3cb329a78ce57')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (602, 97, CAST(0x0000A117006DAA67 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'35853a0b58b90e6f6ee3cb329a78ce57')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (603, 97, CAST(0x0000A117006DB7F6 AS DateTime), 4, 5, N'', N'', N'35853a0b58b90e6f6ee3cb329a78ce57')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (604, 97, CAST(0x0000A11700A8F1CA AS DateTime), 2, 5, N'', N'', N'c5eb14d02c3f8b8d22e184745bcad1b6')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (605, 97, CAST(0x0000A11700A9008C AS DateTime), 4, 5, N'', N'', N'c5eb14d02c3f8b8d22e184745bcad1b6')
GO
print 'Processed 600 total records'
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (606, 97, CAST(0x0000A11700B6FFB8 AS DateTime), 2, 5, N'', N'', N'0b1dc08d3cac07ea0c8fc3e4d23853c8')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (607, 97, CAST(0x0000A11700B708A6 AS DateTime), 4, 5, N'', N'', N'0b1dc08d3cac07ea0c8fc3e4d23853c8')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (608, 97, CAST(0x0000A11701165A11 AS DateTime), 2, 5, N'', N'', N'212f0546406c6d27f28c569d3c0f7253')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (609, 97, CAST(0x0000A11701166D7E AS DateTime), 4, 5, N'', N'', N'212f0546406c6d27f28c569d3c0f7253')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (610, 97, CAST(0x0000A118007C9101 AS DateTime), 2, 5, N'', N'', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (611, 97, CAST(0x0000A118007C9AC7 AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Detenido.', N'', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (612, 97, CAST(0x0000A118007CA070 AS DateTime), 5, 4, N'Par�metros de SERVIDOR CONRED Modificados. Datos [ Puerto Local=1975   ::   Hilos Min.=50   ::   Hilos Max.=100   ::   Timeout=62000 ]', N'', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (613, 97, CAST(0x0000A118007CA937 AS DateTime), 5, 4, N'Par�metros de MOVILRED_FORMATTER Modificados. Datos [ Puerto Local=6661   ::   Hilos Min.=50   ::   Hilos Max.=100   ::   IP Remota1=192.168.156.77   ::   Puerto Remoto1=49394   ::   Timeout Remoto1=60000 ]', N'Par�metros Adicionales de MOVILRED_FORMATTER Modificados. Datos [ NIT=950226658   ::   Usuario=600111   ::   Password=******   ::   IP Remota2=127.0.0.1   ::   Puerto Remoto2=9691   ::   Timeout Remoto2=30000   ::   IP Remota3=127.0.0.3   ::   Puerto', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (614, 97, CAST(0x0000A118007CBA23 AS DateTime), 5, 4, N'Servidor CONRED v1.0.0 [TCP 1975] Iniciado.', N'', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (615, 97, CAST(0x0000A118007CC1B9 AS DateTime), 4, 5, N'', N'', N'2c545f6474afdd02746632342e940744')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (616, 97, CAST(0x0000A11800F96FEE AS DateTime), 2, 5, N'', N'', N'700fb7e3ca8a33c3f8ecafa1fbaf6c6a')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (617, 97, CAST(0x0000A11800F98C18 AS DateTime), 4, 5, N'', N'', N'700fb7e3ca8a33c3f8ecafa1fbaf6c6a')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (618, 97, CAST(0x0000A11800FD35BA AS DateTime), 2, 5, N'', N'', N'087ef1f14436cedf715e92f38930feab')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (619, 97, CAST(0x0000A118010229A6 AS DateTime), 4, 5, N'', N'', N'087ef1f14436cedf715e92f38930feab')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (620, 97, CAST(0x0000A1190066DC14 AS DateTime), 2, 5, N'', N'', N'd80cacb2536da73120c52e54c0834445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (621, 97, CAST(0x0000A1190067307E AS DateTime), 4, 5, N'', N'', N'd80cacb2536da73120c52e54c0834445')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (622, 97, CAST(0x0000A119006FDBEA AS DateTime), 2, 5, N'', N'', N'472aec42e0786b40224b5119db3920a3')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (623, 97, CAST(0x0000A119007051EE AS DateTime), 4, 5, N'', N'', N'472aec42e0786b40224b5119db3920a3')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (624, 97, CAST(0x0000A119007063CA AS DateTime), 2, 5, N'', N'', N'd813747a937bf8a230637e7d1dee33c1')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (625, 97, CAST(0x0000A11900708EF7 AS DateTime), 4, 5, N'', N'', N'd813747a937bf8a230637e7d1dee33c1')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (626, 97, CAST(0x0000A1190070A49F AS DateTime), 2, 5, N'', N'', N'a910982a90af5a8468a15e76a40af27b')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (627, 97, CAST(0x0000A1190071D492 AS DateTime), 4, 5, N'', N'', N'a910982a90af5a8468a15e76a40af27b')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (628, 97, CAST(0x0000A119007C37E0 AS DateTime), 2, 5, N'', N'', N'e00f0ab0f0ab92438e06263ace4f00a4')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (629, 97, CAST(0x0000A119007C8A90 AS DateTime), 4, 5, N'', N'', N'e00f0ab0f0ab92438e06263ace4f00a4')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (630, 97, CAST(0x0000A11900840423 AS DateTime), 2, 5, N'', N'', N'8988e0033e2bb2efe3566a6a9d23ea81')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (631, 97, CAST(0x0000A1190084144A AS DateTime), 4, 5, N'', N'', N'8988e0033e2bb2efe3566a6a9d23ea81')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (632, 97, CAST(0x0000A11900842008 AS DateTime), 2, 5, N'', N'', N'9ec11a312522ef64c617319adde85632')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (633, 97, CAST(0x0000A119008D646A AS DateTime), 4, 5, N'', N'', N'9ec11a312522ef64c617319adde85632')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (634, 93, CAST(0x0000A11900B8201A AS DateTime), 2, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (635, 93, CAST(0x0000A11900B8202C AS DateTime), 1, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (636, 93, CAST(0x0000A11900B8237E AS DateTime), 6, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (637, 93, CAST(0x0000A11900B825CE AS DateTime), 13, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (638, 93, CAST(0x0000A11900B85923 AS DateTime), 6, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (639, 93, CAST(0x0000A11900B85A67 AS DateTime), 11, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (640, 93, CAST(0x0000A11900B8930C AS DateTime), 12, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (641, 93, CAST(0x0000A11900B89340 AS DateTime), 12, 4, N'Usuario Creado. Datos[ txtUserName=Lilia Consuelo Arismendi , txtLogin=lc.arismendi, txtEmail=lilia.arismendi@carvajal.com, ddlUserProfile=Seguridad,  ]', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (642, 93, CAST(0x0000A11900B8C90C AS DateTime), 6, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (643, 93, CAST(0x0000A11900B8CA47 AS DateTime), 13, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (644, 97, CAST(0x0000A11900B971EF AS DateTime), 2, 5, N'', N'', N'540521bf19955e3f32e37ce010304e5a')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (645, 97, CAST(0x0000A11900BC68D4 AS DateTime), 4, 5, N'', N'', N'540521bf19955e3f32e37ce010304e5a')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (646, 97, CAST(0x0000A11900BDF344 AS DateTime), 2, 5, N'', N'', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (647, 97, CAST(0x0000A11900BE4F2F AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Iniciado.', N'', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (648, 97, CAST(0x0000A11900BE633B AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (649, 93, CAST(0x0000A11900BEA4A8 AS DateTime), 4, 5, N'', N'', N'3hrhcn55ulijxk55gj1gpwml')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (650, 97, CAST(0x0000A11900BEF5C0 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Iniciado.', N'', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (651, 97, CAST(0x0000A11900BEFCA4 AS DateTime), 5, 4, N'Formateador MOVILRED v1.0.0 [TCP 6661] Detenido.', N'', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (652, 97, CAST(0x0000A11900C0EED2 AS DateTime), 4, 5, N'', N'', N'f0dd59d1028a4ae1bc63824c5d8c893d')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (653, 97, CAST(0x0000A11900C1DF2C AS DateTime), 2, 5, N'', N'', N'5a98ece4f52fb91ec6cc5f54ce35c2fe')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (654, 97, CAST(0x0000A11900E1C67C AS DateTime), 4, 5, N'', N'', N'5a98ece4f52fb91ec6cc5f54ce35c2fe')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (655, 97, CAST(0x0000A11A0092C578 AS DateTime), 2, 5, N'', N'', N'5ebb0f98a9156f86860ed662e8fbc23a')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (656, 97, CAST(0x0000A11A009379BF AS DateTime), 4, 5, N'', N'', N'5ebb0f98a9156f86860ed662e8fbc23a')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (657, 97, CAST(0x0000A11C008BCC65 AS DateTime), 2, 5, N'', N'', N'b062e9617804675818c489e8a41c350b')
INSERT [dbo].[Log] ([log_id], [log_usuario_id], [log_fecha], [log_funcion_id], [log_tipo_log_id], [log_mensaje1], [log_mensaje2], [log_sessionID]) VALUES (658, 97, CAST(0x0000A11C0096B9F9 AS DateTime), 4, 5, N'', N'', N'b062e9617804675818c489e8a41c350b')
SET IDENTITY_INSERT [dbo].[Log] OFF
/****** Object:  Table [dbo].[MovimientoCuenta]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MovimientoCuenta](
	[mov_id] [bigint] IDENTITY(1,1) NOT NULL,
	[mov_entidad_id] [bigint] NOT NULL,
	[mov_fecha] [datetime] NOT NULL,
	[mov_tipo_movimiento_cuenta_id] [bigint] NOT NULL,
	[mov_valor] [money] NOT NULL,
	[mov_entidad_saldo_inicial] [money] NOT NULL,
	[mov_entidad_saldo_final] [money] NOT NULL,
	[mov_log_id] [bigint] NULL,
	[mov_cuenta_banco_id] [bigint] NULL,
	[mov_observacion] [varchar](250) NULL,
 CONSTRAINT [PK_MovimientoCuenta] PRIMARY KEY CLUSTERED 
(
	[mov_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LogXObjeto]    Script Date: 12/03/2012 09:37:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LogXObjeto](
	[log_id] [bigint] NOT NULL,
	[obj_id] [bigint] NOT NULL,
	[mensaje1] [varchar](200) NOT NULL,
	[mensaje2] [varchar](200) NOT NULL,
 CONSTRAINT [PK_LogXObjeto] PRIMARY KEY CLUSTERED 
(
	[log_id] ASC,
	[obj_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Este campo se trabajar� como nua relaci�n Virtual, si el LOG, en su funci�n apunta al Objeto Entidad, entonces este ID corresponder� al Registro Id de la Tabla Entidad.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'LogXObjeto', @level2type=N'COLUMN',@level2name=N'obj_id'
GO
/****** Object:  Default [DF_Producto_pro_estado_producto_id]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Producto] ADD  CONSTRAINT [DF_Producto_pro_estado_producto_id]  DEFAULT ((1)) FOR [pro_estado_producto_id]
GO
/****** Object:  ForeignKey [FK_AvisoDeposito_CuentaBanco]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[AvisoDeposito]  WITH CHECK ADD  CONSTRAINT [FK_AvisoDeposito_CuentaBanco] FOREIGN KEY([avi_cuenta_banco_id])
REFERENCES [dbo].[CuentaBanco] ([cue_id])
GO
ALTER TABLE [dbo].[AvisoDeposito] CHECK CONSTRAINT [FK_AvisoDeposito_CuentaBanco]
GO
/****** Object:  ForeignKey [FK_AvisoDeposito_Entidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[AvisoDeposito]  WITH CHECK ADD  CONSTRAINT [FK_AvisoDeposito_Entidad] FOREIGN KEY([avi_entidad_id])
REFERENCES [dbo].[Entidad] ([ent_id])
GO
ALTER TABLE [dbo].[AvisoDeposito] CHECK CONSTRAINT [FK_AvisoDeposito_Entidad]
GO
/****** Object:  ForeignKey [FK_AvisoDeposito_EstadoAviso]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[AvisoDeposito]  WITH CHECK ADD  CONSTRAINT [FK_AvisoDeposito_EstadoAviso] FOREIGN KEY([avi_estado_aviso_id])
REFERENCES [dbo].[EstadoAviso] ([id])
GO
ALTER TABLE [dbo].[AvisoDeposito] CHECK CONSTRAINT [FK_AvisoDeposito_EstadoAviso]
GO
/****** Object:  ForeignKey [FK_Barrio_Ciudad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Barrio]  WITH CHECK ADD  CONSTRAINT [FK_Barrio_Ciudad] FOREIGN KEY([ciudad_id])
REFERENCES [dbo].[Ciudad] ([id])
GO
ALTER TABLE [dbo].[Barrio] CHECK CONSTRAINT [FK_Barrio_Ciudad]
GO
/****** Object:  ForeignKey [FK_Ciudad_Departamento]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Ciudad]  WITH CHECK ADD  CONSTRAINT [FK_Ciudad_Departamento] FOREIGN KEY([departamento_id])
REFERENCES [dbo].[Departamento] ([id])
GO
ALTER TABLE [dbo].[Ciudad] CHECK CONSTRAINT [FK_Ciudad_Departamento]
GO
/****** Object:  ForeignKey [FK_Compra_CuentaBanco]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Compra]  WITH CHECK ADD  CONSTRAINT [FK_Compra_CuentaBanco] FOREIGN KEY([comp_cuenta_banco_id])
REFERENCES [dbo].[CuentaBanco] ([cue_id])
GO
ALTER TABLE [dbo].[Compra] CHECK CONSTRAINT [FK_Compra_CuentaBanco]
GO
/****** Object:  ForeignKey [FK_Compra_Proveedor]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Compra]  WITH CHECK ADD  CONSTRAINT [FK_Compra_Proveedor] FOREIGN KEY([comp_proveedor_id])
REFERENCES [dbo].[Proveedor] ([prov_id])
GO
ALTER TABLE [dbo].[Compra] CHECK CONSTRAINT [FK_Compra_Proveedor]
GO
/****** Object:  ForeignKey [FK_ConvenioXProducto_Producto]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[ConvenioXProducto]  WITH CHECK ADD  CONSTRAINT [FK_ConvenioXProducto_Producto] FOREIGN KEY([conv_producto_id])
REFERENCES [dbo].[Producto] ([pro_id])
GO
ALTER TABLE [dbo].[ConvenioXProducto] CHECK CONSTRAINT [FK_ConvenioXProducto_Producto]
GO
/****** Object:  ForeignKey [FK_CuentaBanco_Banco]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[CuentaBanco]  WITH CHECK ADD  CONSTRAINT [FK_CuentaBanco_Banco] FOREIGN KEY([cue_banco_id])
REFERENCES [dbo].[Banco] ([ban_id])
GO
ALTER TABLE [dbo].[CuentaBanco] CHECK CONSTRAINT [FK_CuentaBanco_Banco]
GO
/****** Object:  ForeignKey [FK_CuentaBanco_TipoCuenta]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[CuentaBanco]  WITH CHECK ADD  CONSTRAINT [FK_CuentaBanco_TipoCuenta] FOREIGN KEY([cue_tipo_cuenta_id])
REFERENCES [dbo].[TipoCuenta] ([id])
GO
ALTER TABLE [dbo].[CuentaBanco] CHECK CONSTRAINT [FK_CuentaBanco_TipoCuenta]
GO
/****** Object:  ForeignKey [FK_Entidad_EstadoEntidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Entidad]  WITH CHECK ADD  CONSTRAINT [FK_Entidad_EstadoEntidad] FOREIGN KEY([ent_estado_entidad_id])
REFERENCES [dbo].[EstadoEntidad] ([id])
GO
ALTER TABLE [dbo].[Entidad] CHECK CONSTRAINT [FK_Entidad_EstadoEntidad]
GO
/****** Object:  ForeignKey [FK_Entidad_RazonSocial]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Entidad]  WITH CHECK ADD  CONSTRAINT [FK_Entidad_RazonSocial] FOREIGN KEY([ent_razon_social_id])
REFERENCES [dbo].[RazonSocial] ([raz_id])
GO
ALTER TABLE [dbo].[Entidad] CHECK CONSTRAINT [FK_Entidad_RazonSocial]
GO
/****** Object:  ForeignKey [FK_Entidad_RubroEntidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Entidad]  WITH CHECK ADD  CONSTRAINT [FK_Entidad_RubroEntidad] FOREIGN KEY([ent_rubro_id])
REFERENCES [dbo].[TipoRubroEntidad] ([id])
GO
ALTER TABLE [dbo].[Entidad] CHECK CONSTRAINT [FK_Entidad_RubroEntidad]
GO
/****** Object:  ForeignKey [FK_Entidad_TipoNivelEntidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Entidad]  WITH CHECK ADD  CONSTRAINT [FK_Entidad_TipoNivelEntidad] FOREIGN KEY([ent_tipo_nivel_entidad_id])
REFERENCES [dbo].[TipoNivelEntidad] ([id])
GO
ALTER TABLE [dbo].[Entidad] CHECK CONSTRAINT [FK_Entidad_TipoNivelEntidad]
GO
/****** Object:  ForeignKey [FK_Funcion_Metodo]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Funcion]  WITH CHECK ADD  CONSTRAINT [FK_Funcion_Metodo] FOREIGN KEY([func_metodo_id])
REFERENCES [dbo].[Metodo] ([id])
GO
ALTER TABLE [dbo].[Funcion] CHECK CONSTRAINT [FK_Funcion_Metodo]
GO
/****** Object:  ForeignKey [FK_Funcion_Objeto]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Funcion]  WITH CHECK ADD  CONSTRAINT [FK_Funcion_Objeto] FOREIGN KEY([func_objeto_id])
REFERENCES [dbo].[Objeto] ([id])
GO
ALTER TABLE [dbo].[Funcion] CHECK CONSTRAINT [FK_Funcion_Objeto]
GO
/****** Object:  ForeignKey [FK_Log_Log]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Log] FOREIGN KEY([log_tipo_log_id])
REFERENCES [dbo].[TipoLog] ([id])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Log]
GO
/****** Object:  ForeignKey [FK_Log_Usuario]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Usuario] FOREIGN KEY([log_usuario_id])
REFERENCES [dbo].[Usuario] ([usu_id])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Usuario]
GO
/****** Object:  ForeignKey [FK_LogXObjeto_Log]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[LogXObjeto]  WITH CHECK ADD  CONSTRAINT [FK_LogXObjeto_Log] FOREIGN KEY([log_id])
REFERENCES [dbo].[Log] ([log_id])
GO
ALTER TABLE [dbo].[LogXObjeto] CHECK CONSTRAINT [FK_LogXObjeto_Log]
GO
/****** Object:  ForeignKey [FK_Matriz_NivelEntidadXProducto_Producto]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Matriz_NivelEntidadXProducto]  WITH CHECK ADD  CONSTRAINT [FK_Matriz_NivelEntidadXProducto_Producto] FOREIGN KEY([producto_id])
REFERENCES [dbo].[Producto] ([pro_id])
GO
ALTER TABLE [dbo].[Matriz_NivelEntidadXProducto] CHECK CONSTRAINT [FK_Matriz_NivelEntidadXProducto_Producto]
GO
/****** Object:  ForeignKey [FK_Matriz_NivelEntidadXProducto_TipoNivelEntidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Matriz_NivelEntidadXProducto]  WITH CHECK ADD  CONSTRAINT [FK_Matriz_NivelEntidadXProducto_TipoNivelEntidad] FOREIGN KEY([tipo_nivel_entidad_id])
REFERENCES [dbo].[TipoNivelEntidad] ([id])
GO
ALTER TABLE [dbo].[Matriz_NivelEntidadXProducto] CHECK CONSTRAINT [FK_Matriz_NivelEntidadXProducto_TipoNivelEntidad]
GO
/****** Object:  ForeignKey [FK_Mensaje_TipoNivelMensaje]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Mensaje]  WITH CHECK ADD  CONSTRAINT [FK_Mensaje_TipoNivelMensaje] FOREIGN KEY([men_tipo_nivel_mensaje_id])
REFERENCES [dbo].[TipoNivelMensaje] ([id])
GO
ALTER TABLE [dbo].[Mensaje] CHECK CONSTRAINT [FK_Mensaje_TipoNivelMensaje]
GO
/****** Object:  ForeignKey [FK_Mensaje_Usuario]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Mensaje]  WITH CHECK ADD  CONSTRAINT [FK_Mensaje_Usuario] FOREIGN KEY([men_usuario_id_de])
REFERENCES [dbo].[Usuario] ([usu_id])
GO
ALTER TABLE [dbo].[Mensaje] CHECK CONSTRAINT [FK_Mensaje_Usuario]
GO
/****** Object:  ForeignKey [FK_Mensaje_Usuario1]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Mensaje]  WITH CHECK ADD  CONSTRAINT [FK_Mensaje_Usuario1] FOREIGN KEY([men_usuario_id_para])
REFERENCES [dbo].[Usuario] ([usu_id])
GO
ALTER TABLE [dbo].[Mensaje] CHECK CONSTRAINT [FK_Mensaje_Usuario1]
GO
/****** Object:  ForeignKey [FK_MovimientoCuenta_CuentaBanco]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[MovimientoCuenta]  WITH CHECK ADD  CONSTRAINT [FK_MovimientoCuenta_CuentaBanco] FOREIGN KEY([mov_cuenta_banco_id])
REFERENCES [dbo].[CuentaBanco] ([cue_id])
GO
ALTER TABLE [dbo].[MovimientoCuenta] CHECK CONSTRAINT [FK_MovimientoCuenta_CuentaBanco]
GO
/****** Object:  ForeignKey [FK_MovimientoCuenta_Entidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[MovimientoCuenta]  WITH CHECK ADD  CONSTRAINT [FK_MovimientoCuenta_Entidad] FOREIGN KEY([mov_entidad_id])
REFERENCES [dbo].[Entidad] ([ent_id])
GO
ALTER TABLE [dbo].[MovimientoCuenta] CHECK CONSTRAINT [FK_MovimientoCuenta_Entidad]
GO
/****** Object:  ForeignKey [FK_MovimientoCuenta_Log]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[MovimientoCuenta]  WITH CHECK ADD  CONSTRAINT [FK_MovimientoCuenta_Log] FOREIGN KEY([mov_log_id])
REFERENCES [dbo].[Log] ([log_id])
GO
ALTER TABLE [dbo].[MovimientoCuenta] CHECK CONSTRAINT [FK_MovimientoCuenta_Log]
GO
/****** Object:  ForeignKey [FK_MovimientoCuenta_TipoMovimientoCuenta]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[MovimientoCuenta]  WITH CHECK ADD  CONSTRAINT [FK_MovimientoCuenta_TipoMovimientoCuenta] FOREIGN KEY([mov_tipo_movimiento_cuenta_id])
REFERENCES [dbo].[TipoMovimientoCuenta] ([id])
GO
ALTER TABLE [dbo].[MovimientoCuenta] CHECK CONSTRAINT [FK_MovimientoCuenta_TipoMovimientoCuenta]
GO
/****** Object:  ForeignKey [FK_Opcion_TipoOpcion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Opcion]  WITH CHECK ADD  CONSTRAINT [FK_Opcion_TipoOpcion] FOREIGN KEY([opc_tipo_opcion_id])
REFERENCES [dbo].[TipoOpcion] ([id])
GO
ALTER TABLE [dbo].[Opcion] CHECK CONSTRAINT [FK_Opcion_TipoOpcion]
GO
/****** Object:  ForeignKey [FK_PerfilXFuncion_Funcion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PerfilXFuncion]  WITH CHECK ADD  CONSTRAINT [FK_PerfilXFuncion_Funcion] FOREIGN KEY([perf_id])
REFERENCES [dbo].[Perfil] ([perf_id])
GO
ALTER TABLE [dbo].[PerfilXFuncion] CHECK CONSTRAINT [FK_PerfilXFuncion_Funcion]
GO
/****** Object:  ForeignKey [FK_PerfilXFuncion_PerfilXFuncion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PerfilXFuncion]  WITH CHECK ADD  CONSTRAINT [FK_PerfilXFuncion_PerfilXFuncion] FOREIGN KEY([func_id])
REFERENCES [dbo].[Funcion] ([func_id])
GO
ALTER TABLE [dbo].[PerfilXFuncion] CHECK CONSTRAINT [FK_PerfilXFuncion_PerfilXFuncion]
GO
/****** Object:  ForeignKey [FK_PerfilXOpcion_Opcion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PerfilXOpcion]  WITH CHECK ADD  CONSTRAINT [FK_PerfilXOpcion_Opcion] FOREIGN KEY([opc_id])
REFERENCES [dbo].[Opcion] ([opc_id])
GO
ALTER TABLE [dbo].[PerfilXOpcion] CHECK CONSTRAINT [FK_PerfilXOpcion_Opcion]
GO
/****** Object:  ForeignKey [FK_PerfilXOpcion_Perfil]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PerfilXOpcion]  WITH CHECK ADD  CONSTRAINT [FK_PerfilXOpcion_Perfil] FOREIGN KEY([perf_id])
REFERENCES [dbo].[Perfil] ([perf_id])
GO
ALTER TABLE [dbo].[PerfilXOpcion] CHECK CONSTRAINT [FK_PerfilXOpcion_Perfil]
GO
/****** Object:  ForeignKey [FK_Producto_EstadoProducto]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_EstadoProducto] FOREIGN KEY([pro_estado_producto_id])
REFERENCES [dbo].[EstadoProducto] ([id])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_EstadoProducto]
GO
/****** Object:  ForeignKey [FK_Producto_Proveedor]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_Proveedor] FOREIGN KEY([pro_proveedor_id])
REFERENCES [dbo].[Proveedor] ([prov_id])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_Proveedor]
GO
/****** Object:  ForeignKey [FK_Producto_TipoProducto]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_TipoProducto] FOREIGN KEY([pro_tipo_producto_id])
REFERENCES [dbo].[TipoProducto] ([id])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_TipoProducto]
GO
/****** Object:  ForeignKey [FK_PuntoVenta_EstadoPuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVenta]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVenta_EstadoPuntoVenta] FOREIGN KEY([pto_estado_punto_venta_id])
REFERENCES [dbo].[EstadoPuntoVenta] ([id])
GO
ALTER TABLE [dbo].[PuntoVenta] CHECK CONSTRAINT [FK_PuntoVenta_EstadoPuntoVenta]
GO
/****** Object:  ForeignKey [FK_PuntoVenta_TipoComunicacion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVenta]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVenta_TipoComunicacion] FOREIGN KEY([pto_tipo_comunicacion_id])
REFERENCES [dbo].[TipoComunicacion] ([id])
GO
ALTER TABLE [dbo].[PuntoVenta] CHECK CONSTRAINT [FK_PuntoVenta_TipoComunicacion]
GO
/****** Object:  ForeignKey [FK_PuntoVenta_TipoPuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVenta]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVenta_TipoPuntoVenta] FOREIGN KEY([pto_tipo_punto_venta_id])
REFERENCES [dbo].[TipoPuntoVenta] ([id])
GO
ALTER TABLE [dbo].[PuntoVenta] CHECK CONSTRAINT [FK_PuntoVenta_TipoPuntoVenta]
GO
/****** Object:  ForeignKey [FK_PuntoVentaXEntidad_Entidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVentaXEntidad]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVentaXEntidad_Entidad] FOREIGN KEY([ent_id])
REFERENCES [dbo].[Entidad] ([ent_id])
GO
ALTER TABLE [dbo].[PuntoVentaXEntidad] CHECK CONSTRAINT [FK_PuntoVentaXEntidad_Entidad]
GO
/****** Object:  ForeignKey [FK_PuntoVentaXEntidad_PuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVentaXEntidad]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVentaXEntidad_PuntoVenta] FOREIGN KEY([pto_id])
REFERENCES [dbo].[PuntoVenta] ([pto_id])
GO
ALTER TABLE [dbo].[PuntoVentaXEntidad] CHECK CONSTRAINT [FK_PuntoVentaXEntidad_PuntoVenta]
GO
/****** Object:  ForeignKey [FK_PuntoVentaXSimCard_PuntoVenta]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVentaXSimCard]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVentaXSimCard_PuntoVenta] FOREIGN KEY([pto_id])
REFERENCES [dbo].[PuntoVenta] ([pto_id])
GO
ALTER TABLE [dbo].[PuntoVentaXSimCard] CHECK CONSTRAINT [FK_PuntoVentaXSimCard_PuntoVenta]
GO
/****** Object:  ForeignKey [FK_PuntoVentaXSimCard_SimCard]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[PuntoVentaXSimCard]  WITH CHECK ADD  CONSTRAINT [FK_PuntoVentaXSimCard_SimCard] FOREIGN KEY([sim_id])
REFERENCES [dbo].[SimCard] ([sim_id])
GO
ALTER TABLE [dbo].[PuntoVentaXSimCard] CHECK CONSTRAINT [FK_PuntoVentaXSimCard_SimCard]
GO
/****** Object:  ForeignKey [FK_RazonSocial_Barrio]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[RazonSocial]  WITH CHECK ADD  CONSTRAINT [FK_RazonSocial_Barrio] FOREIGN KEY([raz_barrio_id])
REFERENCES [dbo].[Barrio] ([id])
GO
ALTER TABLE [dbo].[RazonSocial] CHECK CONSTRAINT [FK_RazonSocial_Barrio]
GO
/****** Object:  ForeignKey [FK_RazonSocial_Ciudad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[RazonSocial]  WITH CHECK ADD  CONSTRAINT [FK_RazonSocial_Ciudad] FOREIGN KEY([raz_ciudad_id])
REFERENCES [dbo].[Ciudad] ([id])
GO
ALTER TABLE [dbo].[RazonSocial] CHECK CONSTRAINT [FK_RazonSocial_Ciudad]
GO
/****** Object:  ForeignKey [FK_RazonSocial_Departamento]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[RazonSocial]  WITH CHECK ADD  CONSTRAINT [FK_RazonSocial_Departamento] FOREIGN KEY([raz_departamento_id])
REFERENCES [dbo].[Departamento] ([id])
GO
ALTER TABLE [dbo].[RazonSocial] CHECK CONSTRAINT [FK_RazonSocial_Departamento]
GO
/****** Object:  ForeignKey [FK_RazonSocial_TipoIdentificacion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[RazonSocial]  WITH CHECK ADD  CONSTRAINT [FK_RazonSocial_TipoIdentificacion] FOREIGN KEY([raz_tipo_identificacion_id])
REFERENCES [dbo].[TipoIdentificacion] ([id])
GO
ALTER TABLE [dbo].[RazonSocial] CHECK CONSTRAINT [FK_RazonSocial_TipoIdentificacion]
GO
/****** Object:  ForeignKey [FK_RazonSocial_TipoRegimen]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[RazonSocial]  WITH CHECK ADD  CONSTRAINT [FK_RazonSocial_TipoRegimen] FOREIGN KEY([raz_tipo_regimen_id])
REFERENCES [dbo].[TipoRegimen] ([id])
GO
ALTER TABLE [dbo].[RazonSocial] CHECK CONSTRAINT [FK_RazonSocial_TipoRegimen]
GO
/****** Object:  ForeignKey [FK_SimCard_EstadoSimCard]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[SimCard]  WITH CHECK ADD  CONSTRAINT [FK_SimCard_EstadoSimCard] FOREIGN KEY([sim_estado_sim_card_id])
REFERENCES [dbo].[EstadoSimCard] ([id])
GO
ALTER TABLE [dbo].[SimCard] CHECK CONSTRAINT [FK_SimCard_EstadoSimCard]
GO
/****** Object:  ForeignKey [FK_SimCard_TipoPlanSimCard]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[SimCard]  WITH CHECK ADD  CONSTRAINT [FK_SimCard_TipoPlanSimCard] FOREIGN KEY([sim_tipo_plan_sim_id])
REFERENCES [dbo].[TipoPlanSimCard] ([id])
GO
ALTER TABLE [dbo].[SimCard] CHECK CONSTRAINT [FK_SimCard_TipoPlanSimCard]
GO
/****** Object:  ForeignKey [FK_SucursalBanco_Banco]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[SucursalBanco]  WITH CHECK ADD  CONSTRAINT [FK_SucursalBanco_Banco] FOREIGN KEY([suc_banco_id])
REFERENCES [dbo].[Banco] ([ban_id])
GO
ALTER TABLE [dbo].[SucursalBanco] CHECK CONSTRAINT [FK_SucursalBanco_Banco]
GO
/****** Object:  ForeignKey [FK_TipoPlanSimCard_CarrierSimCard]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[TipoPlanSimCard]  WITH CHECK ADD  CONSTRAINT [FK_TipoPlanSimCard_CarrierSimCard] FOREIGN KEY([carrier_sim_card_id])
REFERENCES [dbo].[CarrierSimCard] ([id])
GO
ALTER TABLE [dbo].[TipoPlanSimCard] CHECK CONSTRAINT [FK_TipoPlanSimCard_CarrierSimCard]
GO
/****** Object:  ForeignKey [FK_Transaccion_TipoMensajeISO]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Transaccion]  WITH CHECK ADD  CONSTRAINT [FK_Transaccion_TipoMensajeISO] FOREIGN KEY([tra_tipo_mensaje_req], [tra_codigo_proceso])
REFERENCES [dbo].[TipoMensajeISO] ([tipo_mensaje], [codigo_proceso])
GO
ALTER TABLE [dbo].[Transaccion] CHECK CONSTRAINT [FK_Transaccion_TipoMensajeISO]
GO
/****** Object:  ForeignKey [FK_TransaccionInicializacion_TablaInicializacion]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[TransaccionInicializacion]  WITH CHECK ADD  CONSTRAINT [FK_TransaccionInicializacion_TablaInicializacion] FOREIGN KEY([tra_tabla_inicializacion_id])
REFERENCES [dbo].[TablaInicializacion] ([tab_id])
GO
ALTER TABLE [dbo].[TransaccionInicializacion] CHECK CONSTRAINT [FK_TransaccionInicializacion_TablaInicializacion]
GO
/****** Object:  ForeignKey [FK_Usuario_Entidad]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Entidad] FOREIGN KEY([usu_entidad_id])
REFERENCES [dbo].[Entidad] ([ent_id])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Entidad]
GO
/****** Object:  ForeignKey [FK_Usuario_EstadoUsuario]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_EstadoUsuario] FOREIGN KEY([usu_estado_usuario_id])
REFERENCES [dbo].[EstadoUsuario] ([id])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_EstadoUsuario]
GO
/****** Object:  ForeignKey [FK_Usuario_Perfil]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY([usu_perfil_id])
REFERENCES [dbo].[Perfil] ([perf_id])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Perfil]
GO
/****** Object:  ForeignKey [FK_UsuarioHistoricoClaves_Usuario]    Script Date: 12/03/2012 09:37:49 ******/
ALTER TABLE [dbo].[UsuarioHistoricoClaves]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioHistoricoClaves_Usuario] FOREIGN KEY([usu_id])
REFERENCES [dbo].[Usuario] ([usu_id])
GO
ALTER TABLE [dbo].[UsuarioHistoricoClaves] CHECK CONSTRAINT [FK_UsuarioHistoricoClaves_Usuario]
GO
/****** Object:  ForeignKey [FK_UsuarioxRestauracionClave_Usuario]    Script Date: 12/03/2012 09:37:50 ******/
ALTER TABLE [dbo].[UsuarioxRestauracionClave]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioxRestauracionClave_Usuario] FOREIGN KEY([rest_usuario_id])
REFERENCES [dbo].[Usuario] ([usu_id])
GO
ALTER TABLE [dbo].[UsuarioxRestauracionClave] CHECK CONSTRAINT [FK_UsuarioxRestauracionClave_Usuario]
GO
