<?php

class ServerParamsDTO {

    var $LocalPort				= '';
    var $LocalTextLog				= '';
    var $LocalXMLLog				= '';        
    var $minThreadPool				= '';
    var $maxThreadPool				= '';
    var $TotalTimeoutJTranServer		= '';
    var $maxAttempsConnection                   = '';
    var $minSaleAmount                          = '';
    var $maxSaleAmount                          = '';
    var $IPTigoFormatter			= '';
    var $PortTigoFormatter			= '';
    var $totalTimeoutTigoFormatter		= '';
    var $isomuxNameTigoFormatter		= '';
    var $IPComcelFormatter			= '';
    var $PortComcelFormatter			= '';
    var $totalTimeoutComcelFormatter		= '';
    var $isomuxNameComcelFormatter		= '';
    var $IPMovilredFormatter                    = '';
    var $PortMovilredFormatter                  = '';
    var $totalTimeoutMovilredFormatter          = '';
    var $isomuxNameMovilredFormatter		= '';
    
}

?>
