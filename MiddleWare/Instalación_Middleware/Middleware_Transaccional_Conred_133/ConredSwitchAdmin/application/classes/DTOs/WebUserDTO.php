<?php

class WebUserDTO {
    
    var $loginID            = -1;
    var $userID             = -1;
    var $userType           = -1;
    var $userName           = '';
    var $entityName         = '';
    var $sessionID          = '';
    var $errorCode          = 0;
    var $expDatePassword    = '';
    var $isAuthenticated    = false;
   
}

?>
