<?php

class DBPropertiesDTO {
    
    var $id         = '';
    var $jdbc       = '';
    var $dbURL      = '';
    var $dbUser     = '';
    var $dbPassword = '';
    var $dbURLHelp  = '';
}

?>
