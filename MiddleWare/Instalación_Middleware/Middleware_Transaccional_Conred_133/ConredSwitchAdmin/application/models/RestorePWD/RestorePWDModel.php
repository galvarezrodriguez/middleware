<?php

class RestorePWDModel extends CI_Model {
        
	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
	}

        public function start_password_restore_process(){
            
            $user_login = $this->input->post('u_login_e');
            $email = $this->input->post('u_email_e');
            
            /*SP Name for Password Restoring*/
            $sp_name_PWDRestore = "sp_webIniciarProcesoRestauracionClave";

            $params = array(
                            array(&$user_login, SQLSRV_PARAM_IN),
                            array(&$email, SQLSRV_PARAM_IN)
                        );	

            $restoreResp = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_PWDRestore, $params);          
            
            return $restoreResp[0];
            
            
        }
        
        public function continue_password_restore_process($activation_code = null){
            
            /*SP Name for Password Restore Continue*/
            $sp_name_PWDRestore2 = "sp_webContinuarProcesoRestauracionClave";

            $params = array(
                            array(&$activation_code, SQLSRV_PARAM_IN)
                        );	

            $restoreResp = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_PWDRestore2, $params);          
            
            return $restoreResp[0];
            
            
        }        
        
        public function finish_password_restore_process($activation_code = null){
            
            $new_hash = $this->input->post('passwordID');
            
            /*SP Name for Password Process Finish*/
            $sp_name_PWDRestore3 = "sp_webTerminarProcesoRestauracionClave";

            $params = array(
                            array(&$activation_code, SQLSRV_PARAM_IN),
                            array(&$new_hash, SQLSRV_PARAM_IN)
                        );	

            $restoreResp = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_PWDRestore3, $params);          
            
            return $restoreResp[0];
            
            
        }        
        
}

?>