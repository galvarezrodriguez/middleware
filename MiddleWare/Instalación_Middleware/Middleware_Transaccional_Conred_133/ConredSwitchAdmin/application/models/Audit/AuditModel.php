<?php

class AuditModel extends CI_Model {

	var $logType    	= 4;    /*Log Type*/
    
	function __construct(){
		parent::__construct();
                
		///Llamado a SP usando Librería Propia
		$this->load->library('Sqlsrv_SP_Manager');                
	}
	
	function save_log($user_id = null, $message1 = null, $message2 = null, $session_id = null) {

		$sp_name_saveLog = "sp_webInsertarMensajeInfoLog";

		$params = array(
						array(&$user_id, SQLSRV_PARAM_IN),
						array(&$this->logType, SQLSRV_PARAM_IN),
						array(&$message1, SQLSRV_PARAM_IN),
						array(&$message2, SQLSRV_PARAM_IN),
						array(&$session_id, SQLSRV_PARAM_IN)
						);                
						
		$this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_saveLog, $params);

	}     

	function validate_object_by_method($user_id = null, $object = null, $method = null, $session_id = null) {

		$sp_name_validate = "sp_webValidarObjetoPorMetodo";

		$params = array(
                                array(&$user_id, SQLSRV_PARAM_IN),
                                array(&$object, SQLSRV_PARAM_IN),
                                array(&$method, SQLSRV_PARAM_IN),
                                array(&$session_id, SQLSRV_PARAM_IN)
                                );                
						
		$this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_validate, $params);

	}        
        
}

?>