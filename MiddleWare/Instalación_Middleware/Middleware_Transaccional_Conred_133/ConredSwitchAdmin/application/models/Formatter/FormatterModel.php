<?php

class FormatterModel extends CI_Model {

	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
	}

	function get_formatter_params($service_name = null) {
            
                $sp_name_getFormatterParams = "sp_mtcConsultarParametrosFormateador";
            
                $params = array(
                                array(&$service_name, SQLSRV_PARAM_IN)
                                );                
                
                $paramsFormatterRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getFormatterParams, $params);                
                
                return $paramsFormatterRes;                

	}

	function update_formatter_params($service_name = null) {
                
                $sp_name_updateFormatter = "sp_webActualizarParametrosFormateador";
                
                $arrParams = array();
                
                $arrParams[] = $this->input->post('f_localPort');
                
                $f_textLog = $this->input->post('f_textLog');                
                if (isset($f_textLog)){
                    if (strtolower($f_textLog) == "on")
                        $arrParams[] = "1";
                    else
                        $arrParams[] = "0";
                } else
                    $arrParams[] = "0";

                $f_xmlLog = $this->input->post('f_xmlLog');
                if (isset($f_xmlLog)){
                    if (strtolower($f_xmlLog) == "on")
                        $arrParams[] = "1";
                    else
                        $arrParams[] = "0";
                } else
                    $arrParams[] = "0";                

                $arrParams[] = $this->input->post('f_minThreads');
                $arrParams[] = $this->input->post('f_maxThreads');
                $arrParams[] = $this->input->post('f_remoteIP1');
                $arrParams[] = $this->input->post('f_remotePort1');
                $arrParams[] = $this->input->post('f_remoteTimeout1');
                
                /*Add Extra Data Movilred*/
                if (strcmp($service_name, "MOVILRED_FORMATTER") == 0){
                    $arrParams[] = $this->input->post('f_nit');    
                    $arrParams[] = $this->input->post('f_user');
                    $arrParams[] = $this->input->post('f_password');
                    $arrParams[] = $this->input->post('f_remoteIP2');
                    $arrParams[] = $this->input->post('f_remotePort2');
                    $arrParams[] = $this->input->post('f_remoteTimeout2');
                    $arrParams[] = $this->input->post('f_remoteIP3');
                    $arrParams[] = $this->input->post('f_remotePort3');
                    $arrParams[] = $this->input->post('f_remoteTimeout3');                    
                    
                } else {
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';
                    $arrParams[] = '';                    
                }
          
                $params = array(
                                array(&$service_name, SQLSRV_PARAM_IN),
                                array(&$arrParams[0], SQLSRV_PARAM_IN),
                                array(&$arrParams[1], SQLSRV_PARAM_IN),
                                array(&$arrParams[2], SQLSRV_PARAM_IN),
                                array(&$arrParams[3], SQLSRV_PARAM_IN),
                                array(&$arrParams[4], SQLSRV_PARAM_IN),
                                array(&$arrParams[5], SQLSRV_PARAM_IN),
                                array(&$arrParams[6], SQLSRV_PARAM_IN),
                                array(&$arrParams[7], SQLSRV_PARAM_IN),
                                array(&$arrParams[8], SQLSRV_PARAM_IN),
                                array(&$arrParams[9], SQLSRV_PARAM_IN),
                                array(&$arrParams[10], SQLSRV_PARAM_IN),
                                array(&$arrParams[11], SQLSRV_PARAM_IN),
                                array(&$arrParams[12], SQLSRV_PARAM_IN),
                                array(&$arrParams[13], SQLSRV_PARAM_IN),
                                array(&$arrParams[14], SQLSRV_PARAM_IN),
                                array(&$arrParams[15], SQLSRV_PARAM_IN),
                                array(&$arrParams[16], SQLSRV_PARAM_IN)
                                );	
                
                $updateFormatterRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_updateFormatter, $params);

                return $updateFormatterRes[0]["STATUS"];
                
	}        
       
	function get_formatter_movilred_status($service_name = null, $channelID = null) {
            
                $sp_name_getFormatterMovilredStatus = "sp_webConsultarEstadoFormateadorMovilred";
            
                $params = array(
                                array(&$service_name, SQLSRV_PARAM_IN),
                                array(&$channelID, SQLSRV_PARAM_IN)
                                );
                
                $paramsFormatterStatusRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getFormatterMovilredStatus, $params);                
                
                return $paramsFormatterStatusRes[0]["STATUS"];      

	}        
        
}

?>