<?php

include_once(APPPATH . 'classes/DTOs/WebUserDTO.php');

class LoginModel extends CI_Model {
    
	function __construct(){
            parent::__construct();

            ///Llamado a SP usando Librería Propia
            $this->load->library('Sqlsrv_SP_Manager');
	}

        public function validate_login($userID, $passwordID, $IP, $SessionID){

            $entryLoginType = 1;    //Fixed Data

            /*SP Name - Validate Login*/
            $sp_name_validate = "sp_webValidarLogin";

            $params = array(array(&$userID, SQLSRV_PARAM_IN),
                                            array(&$passwordID, SQLSRV_PARAM_IN),
                                            array(&$IP, SQLSRV_PARAM_IN),
                                            array(&$SessionID, SQLSRV_PARAM_IN),
                                            array(&$entryLoginType, SQLSRV_PARAM_IN));

            $validator = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_validate, $params);          

            //Create User Data Object
            $webUserDTO = new WebUserDTO();
            $webUserDTO->loginID = $validator[0]["LOGINID"];
            $webUserDTO->userID = $validator[0]["USERID"];
            $webUserDTO->userType = $validator[0]["USERTYPE"];
            $webUserDTO->userName = $validator[0]["USERNAME"];
            $webUserDTO->entityName = $validator[0]["ENTITYNAME"];
            $webUserDTO->errorCode = $validator[0]["ERRORCODE"];
            $webUserDTO->expDatePassword = $validator[0]["EXPPASSWORDDATETIME"];
            $webUserDTO->sessionID = $SessionID;

            return $webUserDTO;

        }       
        
        public function close_session($LoginID){

            /*SP Name - Close Session*/
            $sp_name_validate = "sp_webCerrarSesionWeb";

            $params = array(array(&$LoginID, SQLSRV_PARAM_IN));

            $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_validate, $params);          
                
        }
        
}

?>
