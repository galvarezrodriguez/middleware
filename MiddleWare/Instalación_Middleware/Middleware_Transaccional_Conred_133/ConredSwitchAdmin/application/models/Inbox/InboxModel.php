<?php

class InboxModel extends CI_Model {

	var $itemsXPage     = 4;   /*Items per Page*/
	var $currentPage    = 1;    /*Current Page*/
	var $maxPagesTotal  = 10;   /*Max. Pages Total*/
    
	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
                
	}
                
	function get_current_page(){
		
		return $this->currentPage;
		
	}
   
	function get_max_pages_total(){
		
		return $this->maxPagesTotal;
		
	}        

	function get_itemsXpage(){
		
		return $this->itemsXPage;
		
	}
	
	function get_unread_messages_counter($user_id = null) {

			$sp_name_getMessagesUnreadCounter = "sp_webContarMensajesNoLeidos";

			$params = array(
							array(&$user_id, SQLSRV_PARAM_IN)
							);                

			$counterRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getMessagesUnreadCounter, $params);                
			
			return $counterRes[0]["TOTAL"];

	}        
	
	function set_message_read($user_id = null, $message_id = null) {

			$sp_name_setMessageRead = "sp_webMarcarMensajeLeido";

			$params = array(
							array(&$user_id, SQLSRV_PARAM_IN),
							array(&$message_id, SQLSRV_PARAM_IN)
							);                

			$statusRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_setMessageRead, $params);                
			
			return $statusRes[0]["STATUS"];

	}     
	
	function get_pages_messages_counter($user_id = null) {
		
			$sp_name_getPagesMessagesCounter = "sp_webContarPaginasMensajes";

			$params = array(
							array(&$user_id, SQLSRV_PARAM_IN),
							array(&$this->itemsXPage, SQLSRV_PARAM_IN)
							);                

			$counterRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getPagesMessagesCounter, $params);                
			
			return $counterRes[0]["TOTAL_PAGES"];

	}
  
	function get_messages_per_page($user_id = null, $current_page = null) {
            
                //Set Current Page
                $this->currentPage = $current_page;
            
                $sp_name_getMessages = "sp_webConsultarMensajes";
            
                $params = array(
                                array(&$user_id, SQLSRV_PARAM_IN),
                                array(&$current_page, SQLSRV_PARAM_IN),
                                array(&$this->itemsXPage, SQLSRV_PARAM_IN)
                                );                
                
                $messagesRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getMessages, $params);                
                
                return $messagesRes;                

	}        
        
}

?>