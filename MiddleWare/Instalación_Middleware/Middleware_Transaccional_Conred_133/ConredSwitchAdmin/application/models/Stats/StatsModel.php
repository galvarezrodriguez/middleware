<?php

class StatsModel extends CI_Model {

	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
	}

	function get_stats($type = null) {
            
                $sp_name_getStats = "sp_webConsultarEstadisticas";
            
                $params = array(
                                array(&$type, SQLSRV_PARAM_IN)
                                );                
                
                $statsRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getStats, $params);                
                
                return $statsRes;                

	}
       
	function get_stats_trx($type = null) {
            
                $sp_name_getStatsTrx = "sp_webConsultarEstadisticasTrx";
            
                $params = array(
                                array(&$type, SQLSRV_PARAM_IN)
                                );                
                
                $statsTrxRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getStatsTrx, $params);                
                
                return $statsTrxRes;                

	}        
        
	function get_monitor_trx() {
            
                $sp_name_getMonitorTrx = "sp_webConsultarEstadisticasMonitor";
            
                $params = array( );                
                
                $monitorTrxRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getMonitorTrx, $params);                
                
                return $monitorTrxRes;                

	}        
        
}

?>