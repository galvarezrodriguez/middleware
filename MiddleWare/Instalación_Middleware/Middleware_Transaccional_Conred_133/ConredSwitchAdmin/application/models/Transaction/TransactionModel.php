<?php

class TransactionModel extends CI_Model {
    
	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
	}
 
        function get_data_paged(){

                /*SP Names for Pagging*/
                $sp_name = "sp_webConsultarTransacciones";
            
                $maxRegisterDefault = 10;
                $initPageDefault = 1;

                $iDisplayLength = $maxRegisterDefault;
                $iDisplayStart = $initPageDefault;
                $orderBy = $this->input->post("iSortCol_0");
                $sortBy = $this->input->post("sSortDir_0");
                $searchBy = $this->input->post("sSearch");

                if ( $this->input->post("iDisplayLength") > 0 )
                        $iDisplayLength = $this->input->post("iDisplayLength");	

                if ( $this->input->post("iDisplayStart") > 0 ){	
                        if ( ($this->input->post("iDisplayStart") / $iDisplayLength) != 0 ){
                                $iDisplayStart = ($this->input->post("iDisplayStart") / $iDisplayLength) + $initPageDefault;
                        }	
                }

                $Pagina = $iDisplayStart;
                $RegistrosporPagina = $iDisplayLength;
                $OrdenarporColumnaID = $orderBy;
                $TipoOrdenamiento = $sortBy;
                $BuscarPor = $searchBy;	

                $params = array(array(&$Pagina, SQLSRV_PARAM_IN),
                                array(&$RegistrosporPagina, SQLSRV_PARAM_IN),
                                array(&$OrdenarporColumnaID, SQLSRV_PARAM_IN),
                                array(&$TipoOrdenamiento, SQLSRV_PARAM_IN),
                                array(&$BuscarPor, SQLSRV_PARAM_IN));            

                $dataPaginada = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name, $params);

                $regCounter = $this->get_register_count_DB($searchBy);        

                $output = array(
                        "sEcho" => intval($this->input->post('sEcho')),
                        "iTotalRecords" => $regCounter,
                        "iTotalDisplayRecords" => $regCounter,
                        "aaData" => $dataPaginada
                );       

                return json_encode( $output );

        }

        private function get_register_count_DB($searchBy){

                /*SP Names for Pagging*/
                $sp_name_counter = "sp_webConsultarTransaccionesNroRegistros";    
            
                $params = array(array(&$searchBy, SQLSRV_PARAM_IN));	

                $registerCount = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_counter, $params);          

                return $registerCount[0]["COUNTER_DB"];

        }          
        
}

?>