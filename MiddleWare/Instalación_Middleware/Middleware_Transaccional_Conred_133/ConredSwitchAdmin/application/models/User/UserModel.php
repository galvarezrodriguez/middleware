<?php

class UserModel extends CI_Model {

        var $usu_nombre = "";
        var $usu_email = "";
        var $usu_login = "";
        
	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
	}

	function add_user() {
                $this->usu_nombre = $this->input->post('u_name');
                $this->usu_email = $this->input->post('u_email');
                $this->usu_login = $this->input->post('u_login');
                
		$this->db->insert('Usuario', $this);
		return 1;   /*Retorno OK Por Default*/
	}
        
	function delete_user($user_id = null) {
                
                $this->db->delete('Usuario', array('usu_id' => $user_id)); 
                
		return 1;   /*Retorno OK Por Default*/
	}        

	function update_user($user_id = null) {
                $this->usu_nombre = $this->input->post('u_name_e');
                $this->usu_email = $this->input->post('u_email_e');
                $this->usu_login = $this->input->post('u_login_e');
                                
                $this->db->where('usu_id', $user_id);
		$this->db->update('Usuario', $this);
		return 1;   /*Retorno OK Por Default*/
	}        
        
	function update_user_account($user_id = null) {

                $this->usu_nombre = $this->input->post('u_name_e');
                $this->usu_email = $this->input->post('u_email_e');            
            
                $params = array(
                            'usu_nombre' => $this->usu_nombre,
                            'usu_email' => $this->usu_email
                          );
                                
                $this->db->where('usu_id', $user_id);
		$this->db->update('Usuario', $params);
		return 1;   /*Retorno OK Por Default*/
	}         

	function update_user_password($user_id = null) {
                
                $sp_name_changePWD = "sp_webCambiarClave";
                $actual_hash = $this->input->post('u_pass1');
                $new_hash = $this->input->post('u_pass2');
                                
                $params = array(
                                array(&$user_id, SQLSRV_PARAM_IN),
                                array(&$actual_hash, SQLSRV_PARAM_IN),
                                array(&$new_hash, SQLSRV_PARAM_IN)
                                );	

                $changePWDRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_changePWD, $params);          

                return $changePWDRes[0]["STATUS"];                
                
                
	}        
        
	function get_user($user_id = null) {
            
                $userData = null;

                $queryResult = $this->db->get_where('Usuario', array('usu_id' => $user_id));
                
                foreach ($queryResult->result() as $row)
                {
                    $userData = $row;
                }
                
		return $userData;
	}        

}

?>