<?php

class ServerModel extends CI_Model {

	function __construct(){
		parent::__construct();
                
                ///Llamado a SP usando Librería Propia
                $this->load->library('Sqlsrv_SP_Manager');
	}

	function get_server_params($instance_id = null) {
            
                $sp_name_getServerParams = "sp_mtcConsultarParametrosServidor";
            
                $params = array(
                                array(&$instance_id, SQLSRV_PARAM_IN)
                                );                
                
                $paramsServerRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getServerParams, $params);                
                
                return $paramsServerRes;                

	}

	function update_server_params($instance_id = null) {

                $sp_name_updateServer = "sp_webActualizarParametrosServidor";
                
                $arrParams = array();
                
                $arrParams[] = $this->input->post('s_localPort');
                
                $s_textLog = $this->input->post('s_textLog');
                if (isset($s_textLog)){
                    if (strtolower($s_textLog) == "on")
                        $arrParams[] = "1";
                    else
                        $arrParams[] = "0";
                } else
                    $arrParams[] = "0";

                $s_xmlLog = $this->input->post('s_xmlLog');
                if (isset($s_xmlLog)){
                    if (strtolower($s_xmlLog) == "on")
                        $arrParams[] = "1";
                    else
                        $arrParams[] = "0";
                } else
                    $arrParams[] = "0";                

                $arrParams[] = $this->input->post('s_minThreads');
                $arrParams[] = $this->input->post('s_maxThreads');
                $arrParams[] = $this->input->post('s_totalTimeout');
                $arrParams[] = $this->input->post('s_maxAttempsConnection');
                $arrParams[] = $this->input->post('s_minSaleAmount');
                $arrParams[] = $this->input->post('s_maxSaleAmount');
                
                $params = array(
                                array(&$instance_id, SQLSRV_PARAM_IN),
                                array(&$arrParams[0], SQLSRV_PARAM_IN),
                                array(&$arrParams[1], SQLSRV_PARAM_IN),
                                array(&$arrParams[2], SQLSRV_PARAM_IN),
                                array(&$arrParams[3], SQLSRV_PARAM_IN),
                                array(&$arrParams[4], SQLSRV_PARAM_IN),
                                array(&$arrParams[5], SQLSRV_PARAM_IN),
                                array(&$arrParams[6], SQLSRV_PARAM_IN),
                                array(&$arrParams[7], SQLSRV_PARAM_IN),
                                array(&$arrParams[8], SQLSRV_PARAM_IN)
                                );	

                $updateServerRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_updateServer, $params);

                return $updateServerRes[0]["STATUS"];

	}        
       
	function get_server_status($service_name = null) {
            
                $sp_name_getServerStatus = "sp_webConsultarEstadoFormateadorJNTS";
            
                $params = array(array(&$service_name, SQLSRV_PARAM_IN));
                
                $paramsServerStatusRes = $this->sqlsrv_sp_manager->output($this->db->conn_id, $sp_name_getServerStatus, $params);                
                
                return $paramsServerStatusRes[0]["STATUS"];      

	}        
        
}

?>