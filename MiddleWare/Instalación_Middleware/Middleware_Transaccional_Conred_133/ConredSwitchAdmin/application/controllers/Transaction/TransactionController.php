<?php

include_once(APPPATH . 'controllers/Base/BaseController.php');

class TransactionController extends BaseController {

    function __construct() {
        parent::__construct();	
        
        ///Cargar el Modelo Transaction para manipulación de Datos        
        $this->load->model('Transaction/TransactionModel');
        
    }
	
    public function getData(){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
	
            echo $this->TransactionModel->get_data_paged();

        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
        
    }
	
}
	
?>
