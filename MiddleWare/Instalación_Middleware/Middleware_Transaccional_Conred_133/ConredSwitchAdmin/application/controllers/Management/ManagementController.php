<?php

include_once(APPPATH . 'controllers/Base/BaseController.php');
include_once(APPPATH . 'classes/DTOs/FormatterParamsDTO.php');
include_once(APPPATH . 'classes/DTOs/ServerParamsDTO.php');
include_once(APPPATH . 'classes/DTOs/DBPropertiesDTO.php');
include_once(APPPATH . 'classes/DTOs/WebUserDTO.php');

class ManagementController extends BaseController {

    function __construct() {
        parent::__construct();

        ///Cargar el Modelo Formatter para manipulación de Datos        
        $this->load->model('Formatter/FormatterModel');        
        ///Cargar el Modelo Server para manipulación de Datos        
        $this->load->model('Server/ServerModel'); 
        ///Cargar el Modelo Stats para manipulación de Datos        
        $this->load->model('Stats/StatsModel');         
        ///Cargar el Modelo Audit para almacenamiento de logs
        $this->load->model('Audit/AuditModel'); 
    }

    function adminServices(){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            //Each Service we want to Manage
            $data["service0"] = $this->getServiceStatus(0); //Main Server
            //$data["service1"] = $this->getServiceStatus(1); //Comcel Formatter
            $data["service2"] = $this->getServiceStatus(2); //Movilred Formatter
            //$data["service3"] = $this->getServiceStatus(3); //Tigo Formatter

            parent::prepareHTMLData("Administrar Servicios",
                                    "Administrar Servicios - Servidor & Formateadores",
                                    array("<script src='js/ConredPack/Conred_ManagementServiceView.js.php'></script>",
                                            "<script src='js/ConredPack/Conred_ValidateUtils.js.php'></script>",
                                            "<script src='js/ConredPack/ReadyMade.Charts.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.visualize.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.visualize.tooltip.js.php'></script>",
                                            "<script src='js/ConredPack/ReadyMadeStats.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.stepy.js.php'></script>"
                                        ));
            ///Load Content
            $this->template->write_view('content', 'Management/ManagementServiceView', $data);
            $this->template->render();
            
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }
    
    private function tail($lines = 10, $filename = null) {
        $data = '';
        $fp = @fopen($filename, "r");
        $block = 4096;
        $max = @filesize($filename);
 
        for($len = 0; $len < $max; $len += $block) 
        {
            $seekSize = ($max - $len > $block) ? $block : $max - $len;
            fseek($fp, ($len + $seekSize) * -1, SEEK_END);
            $data = fread($fp, $seekSize) . $data;
 
            if(substr_count($data, "\n") >= $lines + 1) 
            {
                /* Make sure that the last line ends with a '\n' */
                if(substr($data, strlen($data)-1, 1) !== "\n") {
                    $data .= "\n";
                }
 
                preg_match("!(.*?\n){". $lines ."}$!", $data, $match);
                fclose($fp);
                return $match[0];
            }
                      
        }
        @fclose($fp);
        return $data; 
    }    
    
    function getLogData($serviceID = null){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
        
            $fileName = "";

            switch($serviceID){
                case 0:	///Main Server
                        $fileName = $this->config->item('server_log_path');
                        break;
                case 1:     //Formateador Comcel
                        $fileName = $this->config->item('comcel_formatter_log_path');
                        break;
                case 2:     //Formateador Movilred
                        $fileName = $this->config->item('movilred_formatter_log_path');
                        break;
                case 3:	//Formateador Tigo
                        $fileName = $this->config->item('tigo_formatter_log_path');
                        break;                        
            }

            //Leer Últimas 100 Líneas del archivo de Log
            $linesStr = $this->tail(100, $fileName);        
            ///Arreglo Inicia Al Revés
            $linesStr = array_reverse(explode("\n", utf8_encode($linesStr)));

            include('./www/staticForms/LogDataForm.php');
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }
    
    function getFormatterUpdateForm($serviceID = null){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            $serviceName = "";

            switch($serviceID){

                case 1: $serviceName = "COMCEL_FORMATTER";
                        break;

                case 2: $serviceName = "MOVILRED_FORMATTER";
                        break;

                case 3: $serviceName = "TIGO_FORMATTER";
                        break;                

            }

            $formatterParams = $this->FormatterModel->get_formatter_params($serviceName);

            $FormatterParamsDTO = new FormatterParamsDTO();

            ///Separar Parámetros        
            foreach($formatterParams as $key=>$val){             
                if ($val["nombre_parametro"] == "LocalPort")
                    $FormatterParamsDTO->LocalPort = $val["dato"];
                else if ($val["nombre_parametro"] == "LocalTextLog")
                    $FormatterParamsDTO->LocalTextLog = $val["dato"];
                else if ($val["nombre_parametro"] == "LocalXMLLog")
                    $FormatterParamsDTO->LocalXMLLog = $val["dato"];
                else if ($val["nombre_parametro"] == "destinationIP1")
                    $FormatterParamsDTO->destinationIP1 = $val["dato"];
                else if ($val["nombre_parametro"] == "destinationPort1")
                    $FormatterParamsDTO->destinationPort1 = $val["dato"];            
                else if ($val["nombre_parametro"] == "destinationTimeout1")
                    $FormatterParamsDTO->destinationTimeout1 = $val["dato"];            
                else if ($val["nombre_parametro"] == "destinationIP2")
                    $FormatterParamsDTO->destinationIP2 = $val["dato"];
                else if ($val["nombre_parametro"] == "destinationPort2")
                    $FormatterParamsDTO->destinationPort2 = $val["dato"];            
                else if ($val["nombre_parametro"] == "destinationTimeout2")
                    $FormatterParamsDTO->destinationTimeout2 = $val["dato"];                
                else if ($val["nombre_parametro"] == "destinationIP3")
                    $FormatterParamsDTO->destinationIP3 = $val["dato"];
                else if ($val["nombre_parametro"] == "destinationPort3")
                    $FormatterParamsDTO->destinationPort3 = $val["dato"];            
                else if ($val["nombre_parametro"] == "destinationTimeout3")
                    $FormatterParamsDTO->destinationTimeout3 = $val["dato"];
                else if ($val["nombre_parametro"] == "maxThreadPool")
                    $FormatterParamsDTO->maxThreadPool = $val["dato"];            
                else if ($val["nombre_parametro"] == "minThreadPool")
                    $FormatterParamsDTO->minThreadPool = $val["dato"]; 
                else if ($val["nombre_parametro"] == "isomuxName")
                    $FormatterParamsDTO->isomuxName = $val["dato"]; 
                
                //Parámetros Adicionales por Formateador
                switch($serviceID){

                    case 1: //Comcel
                            break;

                    case 2: //Movilred
                            if ($val["nombre_parametro"] == "NIT")
                                $FormatterParamsDTO->NIT = $val["dato"];
                            else if ($val["nombre_parametro"] == "user")
                                $FormatterParamsDTO->user = $val["dato"];
                            else if ($val["nombre_parametro"] == "password")
                                $FormatterParamsDTO->password = $val["dato"];
                        
                            break;

                    case 3: //Tigo
                            break;

                }            

            }

            //Retornar Formulario
            switch($serviceID){

                case 1: include('./www/staticForms/FormatterUpdateForm.php');
                        break;

                case 2: include('./www/staticForms/FormatterMovilredUpdateForm.php');
                        break;

                case 3: include('./www/staticForms/FormatterUpdateForm.php');
                        break;

            }            
            
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }            
    }
    
    private function getFormatterTCPPort($serviceID = null){
        $serviceName = "";

        switch($serviceID){

            case 1: $serviceName = "COMCEL_FORMATTER";
                    break;

            case 2: $serviceName = "MOVILRED_FORMATTER";
                    break;

            case 3: $serviceName = "TIGO_FORMATTER";
                    break;                

        }

        $formatterParams = $this->FormatterModel->get_formatter_params($serviceName);

        $FormatterParamsDTO = new FormatterParamsDTO();

        ///Separar Parámetros        
        foreach($formatterParams as $key=>$val){             
            if ($val["nombre_parametro"] == "LocalPort")
                $FormatterParamsDTO->LocalPort = $val["dato"];
        }

        return $FormatterParamsDTO->LocalPort;
    }    
    
    function getServerUpdateForm($instanceID = null){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            $serverParams = $this->ServerModel->get_server_params($instanceID);

            $ServerParamsDTO = new ServerParamsDTO();

            ///Separar Parámetros
            foreach($serverParams as $key=>$val){
                if ($val["nombre_parametro"] == "LocalPort")
                    $ServerParamsDTO->LocalPort = $val["dato"];
                else if ($val["nombre_parametro"] == "LocalTextLog")
                    $ServerParamsDTO->LocalTextLog = $val["dato"];
                else if ($val["nombre_parametro"] == "LocalXMLLog")
                    $ServerParamsDTO->LocalXMLLog = $val["dato"];
                else if ($val["nombre_parametro"] == "maxThreadPool")
                    $ServerParamsDTO->maxThreadPool = $val["dato"];            
                else if ($val["nombre_parametro"] == "minThreadPool")
                    $ServerParamsDTO->minThreadPool = $val["dato"];            
                else if ($val["nombre_parametro"] == "TotalTimeoutJTranServer")
                    $ServerParamsDTO->TotalTimeoutJTranServer = $val["dato"];
                else if ($val["nombre_parametro"] == "maxAttempsConnection")
                    $ServerParamsDTO->maxAttempsConnection = $val["dato"];
                else if ($val["nombre_parametro"] == "minSaleAmount")
                    $ServerParamsDTO->minSaleAmount = $val["dato"];
                else if ($val["nombre_parametro"] == "maxSaleAmount")
                    $ServerParamsDTO->maxSaleAmount = $val["dato"];                
                else if ($val["nombre_parametro"] == "IPTigoFormatter")
                    $ServerParamsDTO->IPTigoFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "PortTigoFormatter")
                    $ServerParamsDTO->PortTigoFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "totalTimeoutTigoFormatter")
                    $ServerParamsDTO->totalTimeoutTigoFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "isomuxNameTigoFormatter")
                    $ServerParamsDTO->isomuxNameTigoFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "IPComcelFormatter")
                    $ServerParamsDTO->IPComcelFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "PortComcelFormatter")
                    $ServerParamsDTO->PortComcelFormatter = $val["dato"];            
                else if ($val["nombre_parametro"] == "PortComcelFormatter")
                    $ServerParamsDTO->totalTimeoutComcelFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "totalTimeoutComcelFormatter")
                    $ServerParamsDTO->totalTimeoutComcelFormatter = $val["dato"];                
                else if ($val["nombre_parametro"] == "isomuxNameComcelFormatter")
                    $ServerParamsDTO->isomuxNameComcelFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "IPMovilredFormatter")
                    $ServerParamsDTO->IPMovilredFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "PortMovilredFormatter")
                    $ServerParamsDTO->PortMovilredFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "totalTimeoutMovilredFormatter")
                    $ServerParamsDTO->totalTimeoutMovilredFormatter = $val["dato"];
                else if ($val["nombre_parametro"] == "isomuxNameMovilredFormatter")
                    $ServerParamsDTO->isomuxNameMovilredFormatter = $val["dato"];                

            }

            include('./www/staticForms/ServerUpdateForm.php');
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }            
    }    
    
    private function getServerTCPPort($instanceID = null){
        
        $formatterParams = $this->ServerModel->get_server_params($instanceID);

        $ServerParamsDTO = new ServerParamsDTO();
        
        ///Separar Parámetros        
        foreach($formatterParams as $key=>$val){             
            if ($val["nombre_parametro"] == "LocalPort")
                $ServerParamsDTO->LocalPort = $val["dato"];
        }
       
        return $ServerParamsDTO->LocalPort;
    }    
    
    function updateFormatterParams($serviceID = null){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
        
            $serviceName = "";
            $message1 	 = "";
            $message2 	 = "";			

            switch($serviceID){

                case 1: $serviceName = "COMCEL_FORMATTER";
                        break;

                case 2: $serviceName = "MOVILRED_FORMATTER";
                        $message2 = "Parámetros Adicionales de " . $serviceName . " Modificados. Datos [ NIT=" . $this->input->post('f_nit') . "   ::   Usuario=" . $this->input->post('f_user') . "   ::   Password=******" . "   ::   IP Remota2=" . $this->input->post('f_remoteIP2') . "   ::   Puerto Remoto2=" . $this->input->post('f_remotePort2') . "   ::   Timeout Remoto2=" . $this->input->post('f_remoteTimeout2') . "   ::   IP Remota3=" . $this->input->post('f_remoteIP3') . "   ::   Puerto Remoto3=" . $this->input->post('f_remotePort3') . "   ::   Timeout Remoto3=" . $this->input->post('f_remoteTimeout3') . " ]";
                        break;

                case 3: $serviceName = "TIGO_FORMATTER";
                        break;                

            }        

            $formatterParams = $this->FormatterModel->update_formatter_params($serviceName);
			
            $message1 = "Parámetros de " . $serviceName . " Modificados. Datos [ Puerto Local=" . $this->input->post('f_localPort') . "   ::   Hilos Min.=" . $this->input->post('f_minThreads') . "   ::   Hilos Max.=" . $this->input->post('f_maxThreads') . "   ::   IP Remota1=" . $this->input->post('f_remoteIP1') . "   ::   Puerto Remoto1=" . $this->input->post('f_remotePort1') . "   ::   Timeout Remoto1=" . $this->input->post('f_remoteTimeout1') . " ]";

            $this->saveLog($message1, $message2);			

        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }        
    }
    
    function updateServerParams($instanceID = null){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){  
            $message1 	 = "";
            $message2 	 = "";

            $serverParams = $this->ServerModel->update_server_params($instanceID);

            $message1 = "Parámetros de SERVIDOR CONRED Modificados. Datos [ Puerto Local=" . $this->input->post('s_localPort') . "   ::   Hilos Min.=" . $this->input->post('s_minThreads') . "   ::   Hilos Max.=" . $this->input->post('s_maxThreads') . "   ::   Timeout=" . $this->input->post('s_totalTimeout') . " ]";

            $this->saveLog($message1, $message2);			
			
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }    
    
    function getDBPropertiesForm($DBType = null){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            ///$DBType = 1-Oracle, 2-SQL Server, 3-PostgreSQL, 4-MySQL

            $JDBC               = "jTranServer_Conred.databaseDRIVER_JDBC";
            $DB_URL             = "jTranServer_Conred.databaseURL";
            $DB_USER            = "jTranServer_Conred.databaseUSER";
            $DB_PASSWORD        = "jTranServer_Conred.databasePASSWORD";        
            $KEY_SEPARATOR      = "=";        
            $DBPropertiesDTO    = new DBPropertiesDTO();
            $jdbc_default       = "";
            $dbURL_default      = "";

            switch($DBType){
                //Oracle
                case 1: $DBPropertiesDTO->dbURLHelp = "jdbc:oracle:thin:@ip_host:puerto_listener:nombre_servicio_sid"; 
                        $jdbc_default   = "oracle.jdbc.driver.OracleDriver";
                        $dbURL_default  = "jdbc:oracle:thin:@192.168.0.245:1521:Conred";
                        break;
                //SQL Server
                case 2: $DBPropertiesDTO->dbURLHelp = "jdbc:sqlserver://ip_host(\\\\nombre_instancia):puerto_listener;database=nombre_basedatos"; 
                        $jdbc_default   = "com.microsoft.sqlserver.jdbc.SQLServerDriver";                
                        $dbURL_default  = "jdbc:sqlserver://192.168.0.245\\SQLEXPRESS;database=Conred;integratedSecurity=false";
                        break;
                //PostgreSQL
                case 3: $DBPropertiesDTO->dbURLHelp = "jdbc:postgresql://ip_host:puerto_listener/nombre_basedatos"; 
                        $jdbc_default   = "org.postgresql.Driver";                
                        $dbURL_default  = "jdbc:postgresql://192.168.0.245:5432/Conred";                    
                        break;
                //MySQL
                case 4: $DBPropertiesDTO->dbURLHelp = "jdbc:mysql://ip_host:puerto_listener/nombre_basedatos"; 
                        $jdbc_default   = "com.mysql.jdbc.Driver";                
                        $dbURL_default  = "jdbc:mysql://192.168.0.245:3306/Conred";                    
                        break;                

            }

            ///File .properties - Main Server
            $fileName = $this->config->item('server_file_path');

            $fp = fopen($fileName,"r");
            $arrayTotal = array();

            while (($buffer = fgets($fp, 4096)) !== false) {
                $arrayTotal[] = $buffer;
            }            

            fclose($fp);

            foreach($arrayTotal as $key => $val){

                if( strpos( $val, $JDBC ) !== false ){
                    $DBPropertiesDTO->jdbc = str_replace("\r\n","",str_replace($JDBC . $KEY_SEPARATOR, "", $val));
                }

                if( strpos( $val, $DB_URL ) !== false ){
                    $DBPropertiesDTO->dbURL = str_replace("\r\n","",str_replace($DB_URL . $KEY_SEPARATOR, "", $val));
                }

                if( strpos( $val, $DB_USER ) !== false ){
                    $DBPropertiesDTO->dbUser = str_replace("\r\n","",str_replace($DB_USER . $KEY_SEPARATOR, "", $val));
                }

                if( strpos( $val, $DB_PASSWORD ) !== false ){
                    $DBPropertiesDTO->dbPassword = str_replace("\r\n","",str_replace($DB_PASSWORD . $KEY_SEPARATOR, "", $val));
                }            
            }

            //Check for the JDBC type to show
            if ($DBPropertiesDTO->jdbc != $jdbc_default){

                ///Send Default Parameters according to JDBC type
                $DBPropertiesDTO->jdbc          = $jdbc_default;            
                $DBPropertiesDTO->dbURL         = $dbURL_default;            
                $DBPropertiesDTO->dbUser        = "";            
                $DBPropertiesDTO->dbPassword    = "";

            }

            include('./www/staticForms/BDPropertiesForm.php');
            
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }        
    }
    
    function getDBPropertiesInit(){    
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            $JDBC               = "jTranServer_Conred.databaseDRIVER_JDBC";
            $DB_URL             = "jTranServer_Conred.databaseURL";
            $DB_USER            = "jTranServer_Conred.databaseUSER";
            $DB_PASSWORD        = "jTranServer_Conred.databasePASSWORD";
            $jdbc_oracle        = "oracle.jdbc.driver.OracleDriver";
            $jdbc_sqlserver     = "com.microsoft.sqlserver.jdbc.SQLServerDriver";                
            $jdbc_postgresql    = "org.postgresql.Driver";                
            $jdbc_mysql         = "com.mysql.jdbc.Driver";                        
            $KEY_SEPARATOR      = "=";        
            $DBPropertiesDTO    = new DBPropertiesDTO();
            $DBTypeID           = 0;
            
            ///File .properties - Main Server
            $fileName = $this->config->item('server_file_path');

            $fp = fopen($fileName,"r");
            $arrayTotal = array();

            while (($buffer = fgets($fp, 4096)) !== false) {
                $arrayTotal[] = $buffer;
            }            

            fclose($fp);

            foreach($arrayTotal as $key => $val){

                if( strpos( $val, $JDBC ) !== false ){
                    $DBPropertiesDTO->jdbc = str_replace("\r\n","",str_replace($JDBC . $KEY_SEPARATOR, "", $val));

                    //Check for the JDBC type that is saved into the file
                    if( strpos( $val, $jdbc_oracle ) !== false ){
                        $DBTypeID = 1;
                    }
                    else if( strpos( $val, $jdbc_sqlserver ) !== false ){
                        $DBTypeID = 2;
                    }
                    else if( strpos( $val, $jdbc_postgresql ) !== false ){
                        $DBTypeID = 3;
                    }
                    else if( strpos( $val, $jdbc_mysql ) !== false ){
                        $DBTypeID = 4;
                    }                
                }

                if( strpos( $val, $DB_URL ) !== false ){
                    $DBPropertiesDTO->dbURL = str_replace("\r\n","",str_replace($DB_URL . $KEY_SEPARATOR, "", $val));
                }

                if( strpos( $val, $DB_USER ) !== false ){
                    $DBPropertiesDTO->dbUser = str_replace("\r\n","",str_replace($DB_USER . $KEY_SEPARATOR, "", $val));
                }

                if( strpos( $val, $DB_PASSWORD ) !== false ){
                    $DBPropertiesDTO->dbPassword = str_replace("\r\n","",str_replace($DB_PASSWORD . $KEY_SEPARATOR, "", $val));
                }            
            }

            $DBPropertiesDTO->id = $DBTypeID;

            switch($DBTypeID){
                //Oracle
                case 1: $DBPropertiesDTO->dbURLHelp = "jdbc:oracle:thin:@ip_host:puerto_listener:nombre_servicio_sid"; 
                        break;
                //SQL Server
                case 2: $DBPropertiesDTO->dbURLHelp = "jdbc:sqlserver://ip_host(\\\\nombre_instancia):puerto_listener;database=nombre_basedatos"; 
                        break;
                //PostgreSQL
                case 3: $DBPropertiesDTO->dbURLHelp = "jdbc:postgresql://ip_host:puerto_listener/nombre_basedatos"; 
                        break;
                //MySQL
                case 4: $DBPropertiesDTO->dbURLHelp = "jdbc:mysql://ip_host:puerto_listener/nombre_basedatos"; 
                        break;                

            }         

            return $DBPropertiesDTO;
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }    
    
    private function updateDBPropertiesFormatter($serviceID = null){

        $JDBC               = "";
        $DB_URL             = "";
        $DB_USER            = "";
        $DB_PASSWORD        = "";        
        $fileName           = "";
        $KEY_SEPARATOR      = "=";
        
        switch($serviceID){            
            case 1: 
                    $JDBC           = "ComcelFormatter.databaseDRIVER_JDBC";
                    $DB_URL         = "ComcelFormatter.databaseURL";
                    $DB_USER        = "ComcelFormatter.databaseUSER";
                    $DB_PASSWORD    = "ComcelFormatter.databasePASSWORD";                
                    $fileName       = $this->config->item('comcel_formatter_file_path');
                    break;
            case 2: 
                    $JDBC           = "MovilredFormatter.databaseDRIVER_JDBC";
                    $DB_URL         = "MovilredFormatter.databaseURL";
                    $DB_USER        = "MovilredFormatter.databaseUSER";
                    $DB_PASSWORD    = "MovilredFormatter.databasePASSWORD";                
                    $fileName       = $this->config->item('movilred_formatter_file_path');
                    break;
            case 3: 
                    $JDBC           = "TigoFormatter.databaseDRIVER_JDBC";
                    $DB_URL         = "TigoFormatter.databaseURL";
                    $DB_USER        = "TigoFormatter.databaseUSER";
                    $DB_PASSWORD    = "TigoFormatter.databasePASSWORD";                
                    $fileName       = $this->config->item('tigo_formatter_file_path');
                    break;           
        }

        $fp = fopen($fileName,"r");
        $arrayTotal = array();

        while (($buffer = fgets($fp, 4096)) !== false) {
            $arrayTotal[] = $buffer;
        }            

        fclose($fp);

        $arrayTotalNew = array();

        foreach($arrayTotal as $key => $val){
            //Find And Replace Data according to input form
            if( strpos( $val, $JDBC ) !== false )
                $arrayTotalNew[] = $JDBC . $KEY_SEPARATOR . $this->input->post('bd_jdbc') . chr(13) . chr(10);
            else if( strpos( $val, $DB_URL ) !== false )
                $arrayTotalNew[] = $DB_URL . $KEY_SEPARATOR . $this->input->post('bd_url') . chr(13) . chr(10);            
            else if( strpos( $val, $DB_USER ) !== false )
                $arrayTotalNew[] = $DB_USER . $KEY_SEPARATOR . $this->input->post('bd_usuario') . chr(13) . chr(10);            
            else if( strpos( $val, $DB_PASSWORD ) !== false )
                $arrayTotalNew[] = $DB_PASSWORD . $KEY_SEPARATOR . $this->input->post('bd_clave') . chr(13) . chr(10);                        
            else 
                $arrayTotalNew[] = $val;

        }

        //Write new data into File
        $fp = fopen($fileName,"w");

        foreach($arrayTotalNew as $keyNew => $valNew){
            fwrite($fp, $valNew);
        }

        fclose($fp);  
		
    }    
    
    function updateDBProperties(){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            $JDBC               = "jTranServer_Conred.databaseDRIVER_JDBC";
            $DB_URL             = "jTranServer_Conred.databaseURL";
            $DB_USER            = "jTranServer_Conred.databaseUSER";
            $DB_PASSWORD        = "jTranServer_Conred.databasePASSWORD";        
            $KEY_SEPARATOR      = "=";   
			$message1 			= "";
			$message2 			= "";

            ///File .properties - Main Server
            $fileName = $this->config->item('server_file_path');

            $fp = fopen($fileName,"r");
            $arrayTotal = array();

            while (($buffer = fgets($fp, 4096)) !== false) {
                $arrayTotal[] = $buffer;
            }            

            fclose($fp);

            $arrayTotalNew = array();

            foreach($arrayTotal as $key => $val){

                ///Buscar y reemplazar , según los datos que digiten en el formulario
                if( strpos( $val, $JDBC ) !== false )
                    $arrayTotalNew[] = $JDBC . $KEY_SEPARATOR . $this->input->post('bd_jdbc') . chr(13) . chr(10);
                else if( strpos( $val, $DB_URL ) !== false )
                    $arrayTotalNew[] = $DB_URL . $KEY_SEPARATOR . $this->input->post('bd_url') . chr(13) . chr(10);            
                else if( strpos( $val, $DB_USER ) !== false )
                    $arrayTotalNew[] = $DB_USER . $KEY_SEPARATOR . $this->input->post('bd_usuario') . chr(13) . chr(10);            
                else if( strpos( $val, $DB_PASSWORD ) !== false )
                    $arrayTotalNew[] = $DB_PASSWORD . $KEY_SEPARATOR . $this->input->post('bd_clave') . chr(13) . chr(10);                        
                else 
                    $arrayTotalNew[] = $val;

            }
			
            //Write New Values into Server File
            $fp = fopen($fileName,"w");

            foreach($arrayTotalNew as $keyNew => $valNew){
                fwrite($fp, $valNew);
            }

            fclose($fp);  

            //Update Formatters File
            $this->updateDBPropertiesFormatter(1);
            $this->updateDBPropertiesFormatter(2);
            $this->updateDBPropertiesFormatter(3);
			
			$message1 = "Parámetros de BD Modificados. Datos [ " . $this->input->post('bd_url') . "   ::   " . $this->input->post('bd_usuario') . "   ::   " . "*******" . " ]";
			
			$this->saveLog($message1, $message2);			
			
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }
    
    function configDBProperties(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            parent::prepareHTMLData("Configuraci&oacute;n Base de Datos",
                                    "Configuraci&oacute;n de Par&aacute;metros - Base de Datos",
                                    array("<script src='js/ConredPack/Conred_BDPropertiesView.js.php'></script>"));            
            
            $data["DBPropertiesDTO"] = $this->getDBPropertiesInit();
            
            ///Load Content
            $this->template->write_view('content', 'Management/BDPropertiesView', $data);            
            
            $this->template->render();    
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }    
    
    function testDBConnection(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            $param1 = $this->input->get('bd_jdbc');
            $param2 = str_replace("\\\\", "\\", $this->input->get('bd_url'));
            $param3 = $this->input->get('bd_usuario');
            $param4 = $this->input->get('bd_clave');
            
            ///Execute Path
            $exePath = $this->config->item('DBTester_path');

            //Execute DB Connection Tester
            $testRes = exec($exePath . " " . "\"$param1\" \"$param2\" \"$param3\" \"$param4\"");

            if( substr($testRes, 0, 2) == "OK" ){
                $textRes = "Conexi&oacute;n Realizada con Exito.<br />";
                $textRes .= "Par&aacute;metros de Configuraci&oacute;n Correctos.<br />";

                $imageRes = "<img src='images/mba/msgAlert_success.png' />";
            } else {
                $textRes = "Error realizando conexi&oacute;n<br/>";
                $textRes .= "<b>Mensaje:</b><br />";
                $textRes .= utf8_encode(substr($testRes, 6));            
                $imageRes = "<img src='images/mba/msgAlert_error.png' />";
            }

            include('./www/staticForms/TestDBResult.php');
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }        
    }
 
    function getServiceStatusFunction($serviceID = null){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            $service_name = '';     
            $response = 0;

            switch($serviceID){                   
                    case 0:	$service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('server_exe_path')))); //'jTranServer_Conred';	
                            break;
                    case 1:	$service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('comcel_formatter_exe_path')))); //'jTranServer_ComcelFormatter';	
                            break;
                    case 2:	$service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('movilred_formatter_exe_path')))); //'jTranServer_MovilredFormatter';	
                            break;
                    case 3:	$service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('tigo_formatter_exe_path')))); //'jTranServer_TigoFormatter';	
                            break;		                    

            }  

            $service = new WSM_Service($service_name);

            if ($service->status() == SERVICE_RUNNING){
                $response = 1;
            } else {
                $response = 0;
            }        

            echo $response;
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }    
    
    private function getServiceStatus($serviceID){
        
        $service_name = '';
        $image = '';
        $text = '';        
        $response = null;
        $TCPport = '';

        if ($serviceID > 0)
            $TCPport = $this->getFormatterTCPPort($serviceID);
        else
            $TCPport = $this->getServerTCPPort(0);
        
        switch($serviceID){                   
            case 0:	$server_name = 'Servidor CONRED ' . $this->getFileVersion($this->config->item('server_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('server_exe_path')))); //'jTranServer_Conred';	
                        break;
            case 1:	$server_name = 'Formateador COMCEL ' . $this->getFileVersion($this->config->item('comcel_formatter_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('comcel_formatter_exe_path')))); //'jTranServer_ComcelFormatter';	
                        break;
            case 2:	$server_name = 'Formateador MOVILRED ' . $this->getFileVersion($this->config->item('movilred_formatter_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('movilred_formatter_exe_path')))); //'jTranServer_MovilredFormatter';	
                        break;
            case 3:	$server_name = 'Formateador TIGO ' . $this->getFileVersion($this->config->item('tigo_formatter_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('tigo_formatter_exe_path')))); //'jTranServer_TigoFormatter';	
                        break;		                    
                    
        }  
        
        $service = new WSM_Service($service_name);

        if ($service->status() == SERVICE_RUNNING){
                $image = '<img src=\'images/start.png\' />';
                $text = 'Iniciado';
        } else {
                $image = '<img src=\'images/stop.png\' />';
                $text = 'Detenido';
        }        
        
        $response = array("serverName" => $server_name, "text" => $text, "image" => $image);
        
        return $response;
        
    }
    
    private function getFileVersion($filePath = null){
        
        exec("WMIC Path CIM_DataFile WHERE Name='" . str_replace('\\', '\\\\', $filePath) . "' Get Version", $versionOut);

        $response = '0';
        
        if (count($versionOut) >= 1 ){
            $responseT = explode('.', $versionOut[1]);
            if (count($responseT) >= 2 )
                $response = $responseT[0] . '.' . $responseT[1] . '.' . $responseT[2];
        }

        return 'v' . $response; ///Salida de Versión del Archivo
    }
    
    /*$action = 1)Start, 2)Stop, 3)Restart*/
    function adminService($action = null, $serviceID = null){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            $service_name = '';
            $server_name = '';
            $TCPport = '';
			
            //Log Stuff
            $message1 = '';
            $message2 = '';

            if ($serviceID > 0)
                $TCPport = $this->getFormatterTCPPort($serviceID);
            else
                $TCPport = $this->getServerTCPPort(0);

            switch($serviceID){
                case 0:	$server_name = 'Servidor CONRED ' . $this->getFileVersion($this->config->item('server_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('server_exe_path')))); //'jTranServer_Conred';	
                        break;
                case 1:	$server_name = 'Formateador COMCEL ' . $this->getFileVersion($this->config->item('comcel_formatter_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('comcel_formatter_exe_path')))); //'jTranServer_ComcelFormatter';	
                        break;
                case 2:	$server_name = 'Formateador MOVILRED ' . $this->getFileVersion($this->config->item('movilred_formatter_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('movilred_formatter_exe_path')))); //'jTranServer_MovilredFormatter';	
                        break;
                case 3:	$server_name = 'Formateador TIGO ' . $this->getFileVersion($this->config->item('tigo_formatter_exe_path')) . ' [TCP ' . $TCPport . ']';
                        $service_name = str_replace('.exe', '', end(explode('\\', $this->config->item('tigo_formatter_exe_path')))); //'jTranServer_TigoFormatter';	
                        break;			
            }

            $service= new WSM_Service($service_name);

            switch($action){
                //Start Service
                case 1: $service->start();
                        sleep(1);
                        $service->start();
                        $message1 = $server_name . ' Iniciado.';
                        break;
                //Stop Service
                case 2: $service->stop();
                        sleep(1);
                        $service->stop();
                        $message1 = $server_name . ' Detenido.';
                        break;                
                //Restart Service
                case 3: $service->stop();
                        sleep(1);
                        $service->start();
                        $service->start();
                        sleep(1);
						$message1 = $server_name . ' Reiniciado.';
                        break;
            }

			if ($action > 0 ){
                            //Log Action, Web Form
                            $this->saveLog($message1, $message2);				
			}
			
            $image = '';
            $text = '';

            if ($service->status() == SERVICE_RUNNING){
                    $image = '<img src=\'images/start.png\' />';
                    $text = 'Iniciado';
            } else {
                    $image = '<img src=\'images/stop.png\' />';
                    $text = 'Detenido';
            }

            echo "<h4>" . $server_name . "</h4>
                    <p>Estado Servicio: " . $text . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" . $image . "</p>
                    <br />
                ";        
        }
        else{
			//Deny Hacking Access
			parent::destroySession();
        }        
    }
    
    private function separateTitlesStats($stats){
        
        $arrTitles = array();
        
        foreach($stats as $key => $val){            
            if (!in_array($val["FECHA"], $arrTitles)) {
                $arrTitles[] = $val["FECHA"];
            }            
        }
        
        return $arrTitles;
        
    }
    
    function updateStats(){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            $stats1 = $this->StatsModel->get_stats(1); //Total Ventas Aprobadas

            $stats2 = $this->StatsModel->get_stats(2); //Total Ventas NO Aprobadas

            $stats3 = $this->StatsModel->get_stats(3); //Total Trx. Aprobadas x Tipo Trx.
            $stats3Titles = $this->separateTitlesStats($stats3);

            $stats4 = $this->StatsModel->get_stats(4); //Total Trx. NO Aprobadas x Tipo Trx.
            $stats4Titles = $this->separateTitlesStats($stats4);

            ///Estadísticas X Transacciones
            $statsTrx = $this->StatsModel->get_stats_trx(0); 
            $statsTrx = $statsTrx[0];          

            include('./www/staticForms/Stats.php');                
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }                
    }
    
    function updateMonitorTrx(){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            $monitorTrx = $this->StatsModel->get_monitor_trx(); //Total Transacciones
            
            include('./www/staticForms/MonitorTrx.php');
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }                
    }    
    
    function viewMonitorTrx(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            parent::prepareHTMLData("Monitor de Transacciones D&iacute;a Actual",
                                    "Monitor de Transacciones D&iacute;a Actual",
                                    array("<script src='js/ConredPack/Conred_MonitorTrxView.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.visualize.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.visualize.tooltip.js.php'></script>",
                                            "<script src='js/ConredPack/ReadyMade.Charts.js.php'></script>",
                                            "<script src='js/ConredPack/ReadyMadeStats.js.php'></script>"));


            //$monitor = $this->StatsModel->get_monitor_trx();    //Total Transacciones           
            //$data["monitorTrx"] = $monitor;

            ///Load Content
            $this->template->write_view('content', 'Management/MonitorTrxView');            
            $this->template->render();    
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }    
    
    function viewStats(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            parent::prepareHTMLData("Estad&iacute;sticas Transaccionales D&iacute;a Actual",
                                    "Estad&iacute;sticas Transaccionales D&iacute;a Actual",
                                    array("<script src='js/ConredPack/Conred_StatsView.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.visualize.js.php'></script>",
                                            "<script src='js/ConredPack/jquery.visualize.tooltip.js.php'></script>",
                                            "<script src='js/ConredPack/ReadyMade.Charts.js.php'></script>",
                                            "<script src='js/ConredPack/ReadyMadeStats.js.php'></script>"));

            $stats1 = $this->StatsModel->get_stats(1); //Total Ventas Aprobadas           
            $data["stats1"] = $stats1;

            $stats2 = $this->StatsModel->get_stats(2); //Total Ventas NO Aprobadas
            $data["stats2"] = $stats2;

            $stats3 = $this->StatsModel->get_stats(3); //Total Trx. Aprobadas x Tipo Trx.
            $stats3Titles = $this->separateTitlesStats($stats3);
            $data["stats3"] = $stats3;
            $data["stats3Titles"] = $stats3Titles;
            
            $stats4 = $this->StatsModel->get_stats(4); //Total Trx. NO Aprobadas x Tipo Trx.
            $stats4Titles = $this->separateTitlesStats($stats4);
            $data["stats4"] = $stats4;  
            $data["stats4Titles"] = $stats4Titles;
            
            ///Estadísticas X Transacciones
            $statsTrx = $this->StatsModel->get_stats_trx(0); 
            $data["statsTrx"] = $statsTrx[0];  

            ///Load Content
            $this->template->write_view('content', 'Management/StatsView', $data);            
            $this->template->render();    
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }      

    function getChannelSelect(){    
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){   
            
            include('./www/staticForms/SelectMovilredChannelForm.php');
            
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }            
    }    
    
    function getMonitorStatus($serviceID = null, $channelID = null){    
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){   
            
            if ($serviceID == 0){
                include('./www/staticForms/MonitorStatusForm.php');
            } else if ($serviceID == 2){            
                include('./www/staticForms/MonitorStatusMovilredForm.php');        
            }
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }            
    }
    
    function getMonitorForm($serviceID = null){    
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
            include('./www/staticForms/MonitorForm.php');        
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }            
    }
    
    function getMonitorData($serviceID = null){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            $service_name = '';     
            $separator = '#';
            $response = '0' . $separator . '0'. $separator . '0'; ///Salida por default

            switch($serviceID){                   
                    case 0:	$service_name = end(explode('\\', $this->config->item('server_exe_path'))); //'jTranServer_Conred.exe';	
                            break;
                    case 1:	$service_name = end(explode('\\', $this->config->item('comcel_formatter_exe_path'))); //'jTranServer_ComcelFormatter.exe';	
                            break;
                    case 2:	$service_name = end(explode('\\', $this->config->item('movilred_formatter_exe_path'))); //'jTranServer_MovilredFormatter.exe';	
                            break;
                    case 3:	$service_name = end(explode('\\', $this->config->item('tigo_formatter_exe_path'))); //'jTranServer_TigoFormatter.exe';	
                            break; 
            }  

            exec("wmic process where (name='" . $service_name . "') list brief /FORMAT:csv", $monitorOut);

            if (count($monitorOut) > 2){
                $dataArr = explode(",",$monitorOut[2]);
                if (count($dataArr) > 6){
                    $response = $dataArr[1] . $separator;   //Handlers
                    $response .= $dataArr[5] . $separator;  //Threads
                    $response .= substr(((float)$dataArr[6] / (1000*1024)), 0, 5);               //Memory in Bytes
                }
            }

            echo $response; ///Salida de Tokens separados por el caracter '#'
        }
        else{
                //Deny Hacking Access
                parent::destroySession();
        }        
    }    
  
    function sendAdminMovilredMessage($type = null, $channelID = null){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            $message1 	 = "";
            $message2 	 = "";
		
            //Return Data
            $rsp_code   = '';
            $trx_time   = '';
            
            //Fixed Data
            $ip         = "127.0.0.1";
            $port       = $this->getServerTCPPort(0);  //Get Port For Instance 0
            $msgType    = "0800";	//Admin. Message
            
            switch($channelID){                
                case 1:
                        $procCode   = "990001"; //Which Channel to Route
                        break;
                case 2:
                        $procCode   = "990002"; //Which Channel to Route
                        break;
                case 3:
                        $procCode   = "990003"; //Which Channel to Route
                        break;
            }
            
            $datetime   = date("mdHis");
            $stan       = str_pad(rand(1, 999999), 6, "0", STR_PAD_LEFT);            
            
            $param1 = $ip;
            $param2 = $port;
            $param3 = $msgType;
            $param4 = $procCode;
            $param5 = $datetime;
            $param6 = $stan;
            $param7 = "";
            $strType = "";
            
            if ($type == 1){
                $param7 = "001";
                $strType = "LOGON";
            } else if ($type == 2){
                $param7 = "002";
                $strType = "LOGOFF";
            } else if ($type == 3){
                $param7 = "301";
                $strType = "ECHO TEST";                
            }
            
            ///Execute Path
            $exePath = $this->config->item('TrxManager_path');

            //Execute Admin. Transaction Manager
            $trxAdminRes = exec($exePath . " " . "\"$param1\" \"$param2\" \"$param3\" \"$param4\" \"$param5\" \"$param6\" \"$param7\"");
            
            //Separate Tokens
            $trxAdminRes    = explode("|", $trxAdminRes);
            $rsp_code       = $trxAdminRes[5];
            $trx_time       = $trxAdminRes[6];            
            
            if (strcmp($rsp_code, "00") == 0)
                $responseFinal = "Mensaje de " . $strType . " Procesado Correctamente en " . $trx_time . " Milisegundos.";
            else
                $responseFinal = "Mensaje de " . $strType . " No Autorizado.";
			
			$message1 = $responseFinal;
			
			$this->saveLog($message1, $message2);
			
            echo $responseFinal;
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }    

    function getStatusConnectionData($serviceID = null, $channelID = null){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        

            $response = "";
            $responseFinal = "";
            
            if ($serviceID == 0){
                //Main Server
                $response = $this->ServerModel->get_server_status();

                if (strcmp($response,"LOGON") == 0){
                        $responseFinal = "3#Conectado V&iacute;a LOGON.";
                } else if (strcmp($response,"LOGOFF") == 0){
                        $responseFinal = "4#Desconectado V&iacute;a LOGOFF.";
                } else {
                        $responseFinal = "2#Canal Inactivo.";
                }
            } else if ($serviceID == 2){
                //Movilred Formatter
                $response = $this->FormatterModel->get_formatter_movilred_status('', $channelID);

                if (strcmp($response,"TCPUP") == 0){
                        $responseFinal = "1#Conectado V&iacute;a TCP/IP.";
                } else if (strcmp($response,"TCPDOWN") == 0){
                        $responseFinal = "2#Conexi&oacute;n TCP/IP No Establecida.";
                } else if (strcmp($response,"LOGON") == 0){
                        $responseFinal = "3#Conectado V&iacute;a LOGON.";
                } else if (strcmp($response,"LOGOFF") == 0){
                        $responseFinal = "4#Desconectado V&iacute;a LOGOFF.";
                } else {
                        $responseFinal = "2#Sin Respuesta de Estado.";
                }                
            }
			
            echo $responseFinal;
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }      
  
    function sendEchoTest(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            parent::prepareHTMLData("Env&iacute;o de Echo Test",
                                    "Env&iacute;o de Echo Test Servidor Conred",
                                    array("<script src='js/ConredPack/Conred_EchoTestView.js.php'></script>",
                                            "<script src='js/ConredPack/Conred_ValidateUtils.js.php'></script>"));
            
            $data["time"]   = date("His");
            $data["date"]   = date("md");
            $data["stan"]       = str_pad(rand(1, 999999), 6, "0", STR_PAD_LEFT);
            
            ///Load Content
            $this->template->write_view('content', 'Management/EchoTestView', $data);
            $this->template->render();    
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }     
    
    function sendEchoTestParams(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            $time       = date("His");
            $date       = date("md");
            $stan       = str_pad(rand(1, 999999), 6, "0", STR_PAD_LEFT);

            ///Return Data
            echo $time . '|' . $date . '|' . $stan;
            
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }    
    
    function sendEchoTestTrx(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            //Return Data
            $rsp_code   = '';
            $rsp_msg    = '';
            $trx_time   = '';
            
            //Fixed Data
            $ip         = "127.0.0.1";
            $puerto     = $this->getServerTCPPort(0);

            //Form Data
            $msg_type   = $this->input->get('echo_msg_type');
            $proc_code  = $this->input->get('echo_proc_code');
            $date       = $this->input->get('echo_date');
            $time       = $this->input->get('echo_time');
            $stan       = $this->input->get('echo_stan');
            $stan       = str_pad($stan, 6, "0", STR_PAD_LEFT);
            $terminalID = '';

            ///Execute Path
            $exePath = $this->config->item('TrxTester_path');            
            
            //Ejecutar Transaction Tester 
            $echoTestRes = exec($exePath . " " . "\"$ip\" \"$puerto\" \"$msg_type\" \"$proc_code\" \"$stan\" \"$time\" \"$date\" \"$terminalID\"");

            //Separar Tokens
            $echoTestRes    = explode("|", $echoTestRes);
            $msg_type       = $echoTestRes[0];
            $proc_code      = $echoTestRes[1];
            $stan           = $echoTestRes[2];
            $time           = $echoTestRes[3];
            $date           = $echoTestRes[4];
            $terminalID     = $echoTestRes[5];            
            $rsp_code       = $echoTestRes[6];
            $rsp_msg        = $echoTestRes[7];
            $trx_time       = $echoTestRes[8];

            include('./www/staticForms/EchoTestResponseForm.php');
            
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }
    
    function decoderISO8583(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            parent::prepareHTMLData("Decodificador ISO8583",
                                    "Decodificador ISO8583",
                                    array("<script src='js/ConredPack/Conred_DecoderISO8583View.js.php'></script>",
                                            "<script src='js/ConredPack/Conred_ValidateUtils.js.php'></script>"));
            
            ///Load Content
            $this->template->write_view('content', 'Management/DecoderISO8583View', null);
            $this->template->render();    
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }    
	
    function decodeISO8583Trx(){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){

            $totalXML = '';
            $msg_arrT = '';
            
            //Form Data
            $msg_arr        = $this->input->get('decoder_trx');
            $bytes_shifted  = $this->input->get('decoder_bytes');
            
            //Con espacios en modo HEX
            $msg_arr        = explode(" ", $msg_arr);
            
            for($i=$bytes_shifted; $i < count($msg_arr); $i++){
                $msg_arrT .= $msg_arr[$i];
                
                if ($i != count($msg_arr) - 1)
                    $msg_arrT .= " ";
                
            }
            
            ///Execute Path
            $exePath = $this->config->item('ISO8583Decoder_path');             
            
            //Execute Transaction Decoder 
            exec( $exePath . " " . "\"" . $msg_arrT . "\"", $decoderRes);
            
            if (count($decoderRes) > 1){

                foreach($decoderRes as $key => $val){
                    if (strlen($val) > 1)
                        $totalXML .= str_replace("#", "&lt;", $val) . chr(13);
                }

            } else {
                $totalXML = $decoderRes[0];
            }
			
            include('./www/staticForms/DecoderISO8583ResponseForm.php');
        
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
        
    }	
	
    function decoderBase24(){
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            parent::prepareHTMLData("Decodificador Base 24",
                                    "Decodificador Base 24",
                                    array("<script src='js/ConredPack/Conred_DecoderBase24View.js.php'></script>",
                                            "<script src='js/ConredPack/Conred_ValidateUtils.js.php'></script>"));
            
            ///Load Content
            $this->template->write_view('content', 'Management/DecoderBase24View', null);
            $this->template->render();    
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }    
    
    function decodeBase24Trx(){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){

            $totalXML = '';
            $msg_arrT = '';
            
            //Form Data
            $msg_arr        = $this->input->get('decoder_trx');
            $bytes_shifted  = $this->input->get('decoder_bytes');
            
            /*//Con espacios en modo HEX
            $msg_arr        = explode(" ", $msg_arr);
            
            for($i=$bytes_shifted; $i < count($msg_arr); $i++){
                $msg_arrT .= $msg_arr[$i];
                
                if ($i != count($msg_arr) - 1)
                    $msg_arrT .= " ";
                
            }
            */
            
            ///Execute Path
            $exePath = $this->config->item('Base24Decoder_path');             
            
            //Execute Transaction Decoder 
            exec( $exePath . " " . "\"" . $msg_arr . "\"" . " " . "\"" . $bytes_shifted . "\"", $decoderRes);
            
            if (count($decoderRes) > 1){

                foreach($decoderRes as $key => $val){
                    if (strlen($val) > 1)
                        $totalXML .= str_replace("#", "&lt;", $val) . chr(13);
                }

            } else {
                $totalXML = $decoderRes[0];
            }
            
            include('./www/staticForms/DecoderBASE24ResponseForm.php');
        
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
        
    }
 
    private function saveLog($message1 = null, $message2 = null){

            //Get UserDTO Session Data
            $webUserDTO = $this->session->userdata('webUserDTO');

            $this->AuditModel->save_log($webUserDTO->userID, $message1, $message2, $webUserDTO->sessionID);

    }
 
}

?>