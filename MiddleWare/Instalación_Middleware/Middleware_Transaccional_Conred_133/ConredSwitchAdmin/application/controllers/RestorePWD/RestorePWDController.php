<?php

class RestorePWDController extends CI_Controller {

    function __construct() {
        parent::__construct();	
        $this->load->model('RestorePWD/RestorePWDModel');
    }

    public function initProcess(){
        
        $response = $this->RestorePWDModel->start_password_restore_process();
                
        if ($response["STATUS"] == 1){

            $email = $this->input->post('u_email_e');
            $subject = "Reactivar su Cuenta en System (X)";
            $mensaje = "<b>Sr. Usuario:</b>" . "<br /><br />" .
                        "Para reactivar su clave, por favor, d&eacute; click sobre el siguiente link: " . "<br /><br />" . 
                        "<a href='http://localhost:90/Conred/RestorePWD/RestorePWDController/continueProcess/" . $response["RNDTXT"] . "'>Para Reactivar su clave - Click Aqu&iacute;</a><br /><br />" .
                        "&lt;&lt;Correo Generado Autom&aacute;ticamente, NO Responder&gt;&gt;";
                        
            $headers = "MIME-Version: 1.0\r\n"; 
            $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
            
            mail($email, $subject, $mensaje, $headers);
            
        }
        
    }
    
    public function continueProcess($activation_code = null){
        
        $response = $this->RestorePWDModel->continue_password_restore_process($activation_code);
        
        if ($response["STATUS"] == 1){           
            ///Show New Password Page
            $data['rndTxt']=$response["RNDTXT"];
            $this->load->view('RestorePWD/RestorePWDView.php', $data);            
            
        } else {
            ///Show Login Page
            $data['error']='99';
            $this->load->view('Login/LoginView.php', $data);
        }
        
    }
    
    public function restoreProcess($activation_code = null){
        
        $response = $this->RestorePWDModel->finish_password_restore_process($activation_code);
        
        if ($response["STATUS"] == 1){        
            ///Show Login Page, With OK Message
            $data['error']='-3';            
        } else {            
            ///Show Login Page, With ERROR Message
            $data['error']='-4';
        }
        
        $this->load->view('Login/LoginView.php', $data);
        
    }
    
}
?>
