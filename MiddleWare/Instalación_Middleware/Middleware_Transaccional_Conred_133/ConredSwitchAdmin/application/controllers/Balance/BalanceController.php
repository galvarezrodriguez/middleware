<?php

//Incluir Clases de Otros Folders
include_once(APPPATH . 'controllers/Base/BaseController.php');

class BalanceController extends BaseController {

    function __construct() {
        parent::__construct();	
        $this->load->model('User/UserModel');
    }
        
    function getBalance(){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
        
        
            $webUserDTO = $this->session->userdata('webUserDTO');

            sleep(1);   //Ejemplo para Carga de Datos Ajax

            $balance = number_format($this->UserModel->get_balance($webUserDTO->userID), 2, ',', '.');

            include('./www/staticForms/GetBalanceForm.php');
      
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }   	        
        
    } 

}

?>