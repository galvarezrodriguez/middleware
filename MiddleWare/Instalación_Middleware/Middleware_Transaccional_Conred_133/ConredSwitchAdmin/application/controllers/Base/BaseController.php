<?php

include_once(APPPATH . 'classes/DTOs/WebUserDTO.php');

class BaseController extends CI_Controller {

    var $platformName       = "";
    var $platformVersion    = "";
    
    function __construct() {
        parent::__construct();	
        $this->load->library('session');
        $this->load->library('Template');
        
        $this->load->model('Login/LoginModel');        
        
        $this->platformVersion = $this->config->item('versionAdminWeb');
        $this->platformName = $this->config->item('platformName');
        
    }

    /*
     * Returns HTML menu according $userType in Session Object
     * Read every Time from Local File
     */    
    public function getMenuHTML(){
     
        $webUserDTO = $this->session->userdata('webUserDTO');
        $userType = $webUserDTO->userType;
        $menuHTML = '';
        
        if (!empty($userType)) {
            //Read Template Menu File, According to UserType        
            if ($userType == 6)
                $templateMenu = "./www/menus/AdministradorMenu.php";        

            $fh = fopen($templateMenu, 'r');

            $menuHTML = fread($fh, filesize($templateMenu));

            fclose($fh);            
        }
        
        return $menuHTML;
        
    }

    /*
     * Returns Welcome Message according $userType in Session Object
     */          
    public function getWelcomeMessage(){

        $webUserDTO = $this->session->userdata('webUserDTO');

        $welcomeMsg = "Bienvenido, " . $webUserDTO->userName . 
                        "<br />" . "Usuario: " . $webUserDTO->entityName;

        $webUserDTO->welcomeMsg = $welcomeMsg;

        $this->session->set_userdata('webUserDTO', $webUserDTO);
        
        return $welcomeMsg;
        
    }
    
    /*
     * Destroy All Session Variables
     */    
    public function destroySession(){
        
        //Unset ALL Variables
        $this->session->unset_userdata('captcha');
        $this->session->unset_userdata('webUserDTO');

        $this->session->sess_destroy();

        ///Show Login Page
        $data['error']='99';
        $this->load->view('Login/LoginView.php', $data);
        
    }
    
    /*
     * Check if the User is Logged In
     */    
    public function allowAccess(){
        
        $webUserDTO = $this->session->userdata('webUserDTO');
        
        if ($webUserDTO != null)
            $logged = $webUserDTO->isAuthenticated;
        else
            $logged = false;
        
        $flagInit = false;
        
        ///Validate if the user has been loggen in
        if (!empty($logged) && $logged == true){
            $flagInit = true;
        }
        
        return $flagInit;
        
    }
    
    /*
     * Prepare Common Template Data
     */
    public function prepareHTMLData($_title, $_subtitle, $_jScriptsV){       
        $this->template->write('title', $_title . ' :: ' . $this->platformName . ' v' . $this->platformVersion);
        $this->template->write('menuHTML', $this->getMenuHTML());
        $this->template->write('welcomeMsg', $this->getWelcomeMessage());
        $this->template->write('subtitle', $_subtitle);
        
        $_jScriptsCad = '';
        
        if ($_jScriptsV != null)        
            foreach($_jScriptsV as $c => $v)
                $_jScriptsCad .= $v;
        
        $this->template->write('jScripts', $_jScriptsCad); 
        
        //Count Unread Messages
        //$this->load->model('Inbox/InboxModel');
        
        $webUserDTO = $this->session->userdata('webUserDTO');

        //Get User Unread Messages Counter
        //$userMessagesUnreadCounter = $this->InboxModel->get_unread_messages_counter($webUserDTO->userID);        
        //$this->template->write('unreadMessagesCounter', $userMessagesUnreadCounter);
    }    
    
}
?>
