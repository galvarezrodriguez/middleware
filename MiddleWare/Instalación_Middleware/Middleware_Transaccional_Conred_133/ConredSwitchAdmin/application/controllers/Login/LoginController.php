<?php

include_once(APPPATH . 'controllers/Base/BaseController.php');
include_once(APPPATH . 'classes/DTOs/WebUserDTO.php');

class LoginController extends BaseController {

    function __construct() {
        parent::__construct();		
	$this->load->helper('url');      
        
        ///Cargar el Modelo Transaction para manipulación de Datos        
        $this->load->model('Login/LoginModel');        
        
        $this->load->model('Audit/AuditModel');
        
    }

    function index(){
        $this->load->view('Login/LoginView.php', null);
    }
	
    function validateLogin(){

        $flagInitSession = 0;

        $captchaTxt = $this->input->post('captchaTxt');
        $captcha = $this->session->userdata('captcha');
        $userID = $this->input->post('userID');
        $passwordID = $this->input->post('passwordID');
        $IP = $_SERVER['REMOTE_ADDR'];
        $SessionID = $this->session->userdata('session_id');
        
        if (!empty($captchaTxt)) {
            if (empty($captcha) || trim(strtolower($captchaTxt)) != $captcha) {
                ///Error en Captcha
                $flagInitSession = 0;
            } else {
                
                $webUserDTO = $this->LoginModel->validate_login($userID, $passwordID, $IP, $SessionID);
                
                if ($webUserDTO->errorCode == 1){
                    //Inicio de Sesión Correcto
                    $webUserDTO->isAuthenticated = true;
                    
                    $this->session->set_userdata('webUserDTO', $webUserDTO);
                    $flagInitSession = 777;                    
                } else {
                    
                    switch($webUserDTO->errorCode){

                        case 4: 
                                ///Usuario con Sesión Iniciada
                                $flagInitSession = 7; break;                        
                        
                        case 6: 
                                ///Usuario Desactivado
                                $flagInitSession = 6; break;
                        default:
                                ///Error de Usuario/Password
                                $flagInitSession = 1;                        
                    }
                    

                }
            }
            $this->session->unset_userdata('captcha');
        }        

        if ($flagInitSession != 777){
            ///Send Error Data To the view
            
            switch($flagInitSession){
                case 0:                
                        $data['error']='-1';
                        break;
                case 1:
                        $data['error']='-2';
                        break;
                case 6:
                        $data['error']='-6';
                        break;   
                case 7:
                        $data['error']='-7';
                        break;                     
                    
            }            
            $this->load->view('Login/LoginView.php', $data);
        } else {

            //Save Session Start in Log
            $this->AuditModel->validate_object_by_method($webUserDTO->userID, 'Session', 'Start', $webUserDTO->sessionID);            
            
            //Get HTML Menu from Base Controller, And Save it into Session Object
            parent::getMenuHTML();          
            
            ///Show Intro View
            redirect('../Index/IndexController', 'location');
            
        }

    }

    /*
     * Destroy Session for This user
     */    
    function logout(){
        
        //Close Session DB
        $webUserDTO = $this->session->userdata('webUserDTO');
        
        if ($webUserDTO != null){
            $LoginID = $webUserDTO->loginID;
            $this->LoginModel->close_session($LoginID);
            
            //Save Session Start in Log
            $this->AuditModel->validate_object_by_method($webUserDTO->userID, 'Session', 'End', $webUserDTO->sessionID);            
            
        }
        
        //Destroy Session And Redirect
        parent::destroySession();
    }

}

?>