<?php

//Incluir Clases de Otros Folders
include_once(APPPATH . 'controllers/Base/BaseController.php');

class IndexController extends BaseController {

    function __construct() {
        parent::__construct();	
    }
    
    function index(){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){

            parent::prepareHTMLData("Panel de Control",
                                    "Panel de Control - Transacciones Recientes (10 min.)",
                                    array("<script src='js/ConredPack/Conred_IndexView.js.php'></script>"));
            ///Load Content
            $this->template->write_view('content', 'Index/IndexView');
            $this->template->render();            
            
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }

}

?>