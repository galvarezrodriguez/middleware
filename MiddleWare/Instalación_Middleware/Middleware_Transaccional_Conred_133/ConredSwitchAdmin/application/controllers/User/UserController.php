<?php

include_once(APPPATH . 'controllers/Base/BaseController.php');

class UserController extends BaseController {

    function __construct() {
            parent::__construct();	

            ///Cargar el Modelo User para manipulación de Datos        
            $this->load->model('User/UserModel');
        
    }

    function index(){

            ///Validate if the user has been loggen in
            if (parent::allowAccess()){
                
                parent::prepareHTMLData("Control de Usuarios",
                                        "Control de Usuarios",
                                        array("<script src='js/ConredPack/Conred_UserView.js.php'></script>"));
                ///Load Content
                $this->template->write_view('content', 'User/UserView.php');
                $this->template->render();        
                
            }
            else{
                //Deny Hacking Access
                parent::destroySession();
            }
    }

    public function getData(){
            ///Validate if the user has been loggen in
            if (parent::allowAccess()){
                echo $this->UserModel->get_data_paged();    
            }            
            else{
                //Deny Hacking Access
                parent::destroySession();
            }            
            
    }
    
    public function addUser(){
            ///Validate if the user has been loggen in
            if (parent::allowAccess()){        
                $this->UserModel->add_user(); 
            }
            else{
                //Deny Hacking Access
                parent::destroySession();
            }        
    }

    public function deleteUser($user_id = null){
            ///Validate if the user has been loggen in
            if (parent::allowAccess()){
                $this->UserModel->delete_user($user_id); 
            }
            else{
                //Deny Hacking Access
                parent::destroySession();
            }                  
        
    }    

    public function updateUser($user_id = null){
            ///Validate if the user has been loggen in
            if (parent::allowAccess()){        
                $this->UserModel->update_user($user_id);         
            }
            else{
                //Deny Hacking Access
                parent::destroySession();
            }               
            
    }        
    
    public function getUpdateForm($user_id = null){
            ///Validate if the user has been loggen in
            if (parent::allowAccess()){
                $userData = $this->UserModel->get_user($user_id);

                //En este archivo PHP (usado como Template), se encuentran mapeados los campos
                //Con la variable $userData
                include('./www/staticForms/UserUpdateForm.php');
            }
            else{
                //Deny Hacking Access
                parent::destroySession();
            }        
    }
    
}

?>