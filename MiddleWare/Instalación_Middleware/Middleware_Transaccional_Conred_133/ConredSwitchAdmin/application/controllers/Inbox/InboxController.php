<?php

include_once(APPPATH . 'controllers/Base/BaseController.php');
include_once(APPPATH . 'classes/DTOs/WebUserDTO.php');

class InboxController extends BaseController {

    function __construct() {
        parent::__construct();	
        
        ///Cargar el Modelo Inbox para manipulación de Datos        
        $this->load->model('Inbox/InboxModel');
        
    }
    
    function index(){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){

            parent::prepareHTMLData("Bandeja de Entrada",
                                    "Bandeja de Entrada - Mensajes del Sistema",
                                    array("<script src='js/ConredPack/Conred_InboxView.js.php'></script>"));

            $webUserDTO = $this->session->userdata('webUserDTO');

            //Get User Messages
            $data['userMessages'] = $this->InboxModel->get_messages_per_page($webUserDTO->userID, 1); //Init. Page
            
            //Get Max. Pages
            $data['maxPages'] = $this->InboxModel->get_pages_messages_counter($webUserDTO->userID); 
            
            //Get Current Page
            $data['currentPage'] = $this->InboxModel->get_current_page(); 
            
            //Get Max. Pages Total
            $data['maxPagesTotal'] = $this->InboxModel->get_max_pages_total();             

            //Get Items X Page
            $data['itemsXPage'] = $this->InboxModel->get_itemsXpage();             
            
            ///Load Content
            $this->template->write_view('content', 'Inbox/InboxView', $data);
            $this->template->render();            
            
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
    }

    function goToPageMessages($currentPage = null){
        
        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
        
            $webUserDTO = $this->session->userdata('webUserDTO');

            //Get User Messages per Page
            $userMessages = $this->InboxModel->get_messages_per_page($webUserDTO->userID, $currentPage);            

            //Get Max. Pages
            $maxPages = $this->InboxModel->get_pages_messages_counter($webUserDTO->userID); 
            
            //Get Current Page
            $currentPage = $this->InboxModel->get_current_page();            
            
            //Get Max. Pages Total
            $maxPagesTotal = $this->InboxModel->get_max_pages_total();            
            
            include('./www/staticForms/MessagesList.php');

        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }
        
    }
    
    function updateMessagesList(){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
        
            $webUserDTO = $this->session->userdata('webUserDTO');

            //Get User Messages
            $userMessages = $this->InboxModel->get_messages_per_page($webUserDTO->userID, 1);   //Init. Page         

            //Get Max. Pages
            $maxPages = $this->InboxModel->get_pages_messages_counter($webUserDTO->userID); 
            
            //Get Current Page
            $currentPage = $this->InboxModel->get_current_page();            
            
            //Get Max. Pages Total
            $maxPagesTotal = $this->InboxModel->get_max_pages_total();            
            
            include('./www/staticForms/MessagesList.php');

        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }   	            
            
    }    
    
    function updateMessagesCounter(){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){
            
            $webUserDTO = $this->session->userdata('webUserDTO');

            //Get User Unread Messages Counter
            $userUnreadMessagesCounter = $this->InboxModel->get_unread_messages_counter($webUserDTO->userID);            

            echo $userUnreadMessagesCounter;
        
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }   	        
        
    }    
    
    function updateMessageStatus($messageID = null){

        ///Validate if the user has been loggen in
        if (parent::allowAccess()){        
        
            $webUserDTO = $this->session->userdata('webUserDTO');

            //Set User Message as Read
            $userSetMessageRead = $this->InboxModel->set_message_read($webUserDTO->userID, $messageID);            

            //Update Message Unread Counter
            echo $this->updateMessagesCounter();
        }
        else{
            //Deny Hacking Access
            parent::destroySession();
        }        
    }    
    
}

?>