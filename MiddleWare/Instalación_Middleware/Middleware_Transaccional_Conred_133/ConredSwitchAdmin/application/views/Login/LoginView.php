<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml"> 
 
<head> 
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
	<title><?= $this->config->item('platformName') . ' v' . $this->config->item('versionAdminWeb')  ?> :: Inicio de Sesi&oacute;n</title> 
	
	<base href="<?php echo $this->config->item('base_url') ?>www/" />
	
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="icon" type="image/ico" href="images/favicon.ico">
	
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/text.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/buttons.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/login.css" type="text/css" media="screen" title="no title" />
        <link rel="stylesheet" href="css/forms.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/x.msgAlert.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/x.msgGrowl.css" type="text/css" media="screen" title="no title" />
        <link rel="stylesheet" href="css/x.modal.css" type="text/css" media="screen" title="no title" />
	
</head> 
 
<body>

<div id="login">
	<h1>Login</h1>
	<div id="login_panel">
		<form action="../Login/LoginController/validateLogin" method="POST" autocomplete="OFF" accept-charset="utf-8" onSubmit="return validateLoginForm();">		
			<div class="login_fields">				
				<div class="field">
					<label for="userID">Usuario :</label>
					<input type="text" name="userID" value="" id="userID" tabindex="1" />
				</div>
				
				<div class="field">
					<label for="passwordID">Clave :</label>
					<input type="password" name="passwordID" value="" id="passwordID" tabindex="2" />
				</div>

				<div class="field">
					<label for="captcha">C&oacute;digo de Verificaci&oacute;n :</label>
					<img src="../Captcha/CaptchaController/getImage" id="captcha" width="200" height="60"/>
					<label for=""><small><a href="javascript:changeCaptcha()" id="change-image">Cambiar Imagen.</a></small></label>
					<br />
					<input type="text" name="captchaTxt" value="" id="captchaTxt" tabindex="3" maxlength="20" style="width: 100px" />					
				</div>				
				
			</div> <!-- .login_fields -->
			
			<div class="login_actions">
				<button type="submit" class="btn primary" tabindex="4">Iniciar Sesi&oacute;n</button>
			</div>
		</form>
	</div> <!-- #login_panel -->		
</div> <!-- #login -->

<!-- Javascript Include Files -->
<script src="js/ConredPack/ReadyMade.js.php"></script>
<script src="js/ConredPack/ReadyMade.Nav.js.php"></script>
<script src="js/ConredPack/jquery-1.5.2.min.js.php"></script>
<script src="js/ConredPack/jquery.dataTables.min.js.php"></script>
<script src="js/ConredPack/modal.js.php"></script>
<script src="js/ConredPack/msgAlert.js.php"></script>
<script src="js/ConredPack/msgGrowl.js.php"></script>
<script src="js/ConredPack/md5-min.js.php"></script>
<script src="js/ConredPack/sha1-min.js.php"></script>
<script src="js/ConredPack/Conred_ValidateUtils.js.php"></script>
<script src="js/ConredPack/Conred_LoginView.js.php"></script>

<?php

	if (!empty($error)) {
		if ($error == "-1"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'C&oacute;digo de Verificaci&oacute;n No Coincide. !!! '});</script>";
                } else if ($error == "-2"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'Datos de Usuario/Password Err&oacute;neos, Reintente. !!! '});</script>";
                } else if ($error == "-3"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'success',title: 'Mensaje del Sistema',text: 'Nueva Clave Actualizada Exitosamente. !!! '});</script>";
                } else if ($error == "-4"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'Error Actualizando Clave, Reintente. !!! '});</script>";
                } else if ($error == "-6"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'Usuario Desactivado. !!! '});</script>";
                } else if ($error == "-7"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'Usuario con Sesión ya Iniciada !!! '});</script>";
                } else if ($error == "99"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'info',title: 'Mensaje del Sistema',text: 'Sesi&oacute;n Finalizada Correctamente. '});</script>";
		}
	}
?>

</body> 
 
</html>