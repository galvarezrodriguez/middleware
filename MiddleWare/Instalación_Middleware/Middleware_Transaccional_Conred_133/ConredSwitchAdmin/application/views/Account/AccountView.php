<div id="contact_profile" class="grid grid_17 append_1">


        <table>
                <tr>
                        <td class="td_avatar">
                                <img src="images/avatar.jpg" class="avatar" alt="Profile Image" />
                        </td>
                        <td class="td_info">				
                                <h1 class="contact_name"> <?= $userData->usu_nombre; ?> </h1>

                                <p class="contact_company"> 
                                    Login: <b><?= $userData->usu_login; ?> </b>
                                    <br />
                                    Correo Electr&oacute;nico: <a href="mailto:<?= $userData->usu_email; ?>"> <?= $userData->usu_email; ?> </a>
                                    <br />
                                    Expiraci&oacute;n Clave: <?= $userData->usu_fecha_exp_clave; ?>
                                </p>

                                <p class="contact_tags"><span><?= $entityName; ?></span></p>

                        </td>
                </tr>
        </table>

        <hr />

</div> <!-- .grid -->

<div class="grid grid_6">

        <a href="javascript:;" class="btn primary xlarge block" id="form_edit_btn">Editar Cuenta</a>
        <a href="javascript:;" class="btn secondary xlarge block" id="form_changepwd_btn">Cambiar Clave</a>
        <div class="box">
                <h3>Informaci&oacute;n</h3>

                <p style="text-align: justify">Este formulario le permite Editar los datos de su cuenta web. As&iacute; como cambiar su clave de acceso.</p>

                <p style="text-align: center; font-style: italic">
                    Recuerde cambiar su clave peri&oacute;dicamente.                                            
                </p>

        </div> <!-- .box -->

</div>
