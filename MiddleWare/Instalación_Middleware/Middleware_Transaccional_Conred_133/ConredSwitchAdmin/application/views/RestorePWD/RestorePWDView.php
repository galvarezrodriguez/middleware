<?php

    //Hacking Attemp
    if (!isset($rndTxt)){
        ///Go To Login Page
        redirect('../Login/LoginController', 'location');
    }

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 
<html xmlns="http://www.w3.org/1999/xhtml"> 
 
<head> 
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 
	<title>:: System <X> :: Cambio de Clave</title> 
	
	<base href="<?php echo $this->config->item('base_url') ?>www/" />
	
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="icon" type="image/ico" href="images/favicon.ico">
	
	<link rel="stylesheet" href="css/reset.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/text.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/buttons.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/login.css" type="text/css" media="screen" title="no title" />
        <link rel="stylesheet" href="css/forms.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/x.msgAlert.css" type="text/css" media="screen" title="no title" />
	<link rel="stylesheet" href="css/x.msgGrowl.css" type="text/css" media="screen" title="no title" />
        <link rel="stylesheet" href="css/x.modal.css" type="text/css" media="screen" title="no title" />

        <script src="js/ConredPack/ReadyMade.js.php"></script>
        <script src="js/ConredPack/ReadyMade.Nav.js.php"></script>
        <script src="js/ConredPack/jquery-1.5.2.min.js.php"></script>
        <script src="js/ConredPack/jquery.dataTables.min.js.php"></script>
        <script src="js/ConredPack/modal.js.php"></script>
        <script src="js/ConredPack/msgAlert.js.php"></script>
        <script src="js/ConredPack/msgGrowl.js.php"></script>
        <script src="js/ConredPack/md5-min.js.php"></script>
        <script src="js/ConredPack/sha1-min.js.php"></script>                
        <script src="js/ConredPack/Conred_RestorePWDView.js.php"></script> 
        <script src="js/ConredPack/Conred_ValidateUtils.js.php"></script> 
	
</head> 
 
<body>

<div id="login">
	<h1>Cambiar Clave</h1>
	<div id="login_panel">
            <form action="../RestorePWD/RestorePWDController/restoreProcess/<?= $rndTxt; ?>" method="POST" accept-charset="utf-8" onSubmit="return validateRestorePWDForm();">
			<div class="login_fields">				
                            
                                <b>Digite la nueva clave para su cuenta:</b>
                                <br />
                                <br />
				<div class="field">
					<label for="passwordID">Nueva Clave :</label>
					<input type="password" name="passwordID" value="" id="passwordID" tabindex="1" />
				</div>

				<div class="field">
					<label for="passwordIDRetype">Repetir Clave :</label>
					<input type="password" name="passwordIDRetype" value="" id="passwordIDRetype" tabindex="2" />
				</div>                            
                            
			</div> <!-- .login_fields -->
			
			<div class="login_actions">
				<button type="submit" class="btn primary" tabindex="4">Cambiar Clave</button>
			</div>
		</form>
	</div> <!-- #login_panel -->		
</div> <!-- #login -->

<?php

	if (!empty($error)) {
		if ($error == "-1"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'C&oacute;digo de Verificaci&oacute;n No Coincide. !!! '});</script>";
                } else if ($error == "-2"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'error',title: 'Mensaje del Sistema',text: 'Datos de Usuario/Password Err&oacute;neos, Reintente. !!! '});</script>";                        
		} else if ($error == "99"){
			echo "<script language='javascript'>$.msgGrowl ({type: 'info',title: 'Mensaje del Sistema',text: 'Sesi&oacute;n Finalizada Correctamente. '});</script>";
		}
	}
?>

</body> 
 
</html>