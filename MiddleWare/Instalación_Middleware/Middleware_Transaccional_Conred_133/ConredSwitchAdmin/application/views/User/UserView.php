<div class="grid grid_17 append_1">

        <table class="data display datatable">
                <thead> 
                    <tr>
                        <th>USUARIO ID</th>
                        <th>NOMBRE</th>
                        <th>CORREO ELECTR&Oacute;NICO</th>
                        <th>LOGIN</th>
                        <th>Acciones</th>
                    </tr> 
                </thead> 
                <tbody> 
                </tbody> 
        </table> 

</div> <!-- .grid -->

<div class="grid grid_6">

        <a href="javascript:;" class="btn primary xlarge block" id="form_usuario_btn">Crear Nuevo Usuario</a>

        <div class="box">
                <h3>Informaci&oacute;n</h3>

                <p>Este formulario le permite Ver, Crear y Editar los datos pertenecientes a los usuarios del sistema.</p>
        </div> <!-- .box -->

</div>
