<div id="stats_content" class="grid grid_17 append_1">

    <fieldset id="form-stats-0" class="step" style="display: block; text-align: center;">

            <table style="width: 100%; border:1px solid gray;">
                <thead>
                    <tr>
                        <th colspan="3" style="text-align: left; font-size: small"><b>Número Total de Transacciones:</b></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 33%; font-size: small">Ventas</td>
                        <td style="width: 33%; font-size: small">Consultas de Saldo</td>
                        <td style="width: 34%; font-size: small">Echo Tests</td>
                    </tr>
                    <tr>
                        <td style="width: 33%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats1' value='<?= number_format($statsTrx["STATS1"], 0, '', '.') ?> Trx.' id='stats1' size='21' tabindex='1' disabled/>
                            </div>
                        </td>
                        <td style="width: 33%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats2' value='<?= number_format($statsTrx["STATS2"], 0, '', '.') ?> Trx.' id='stats2' size='21' tabindex='1' disabled/>
                            </div>                                                
                        </td>
                        <td style="width: 34%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats3' value='<?= number_format($statsTrx["STATS3"], 0, '', '.') ?> Trx.' id='stats3' size='21' tabindex='1' disabled/>
                            </div>                                                
                        </td>
                    </tr>  
                </tbody>
            </table>
            <br />
            <table style="width: 100%; border:1px solid gray;">
                <thead>
                    <tr>
                        <th colspan="3" style="text-align: left; font-size: small"><b>Ventas Aprobadas:</b></th>
                    </tr>
                </thead>
                <tbody>                
                    <tr>
                        <td style="width: 33%; font-size: small">N&uacute;mero de Trx.</td>
                        <td style="width: 33%; font-size: small">Valor Total</td>
                    </tr>
                    <tr>
                        <td style="width: 33%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats3' value='<?= number_format($statsTrx["STATS4"], 0, '', '.') ?> Trx.' id='stats3' size='21' tabindex='1' disabled/>
                            </div>                        
                        </td>
                        <td style="width: 33%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats4' value='$ <?= number_format($statsTrx["STATS5"], 0, '', '.') ?>' id='stats4' size='21' tabindex='1' disabled/>
                            </div>                                                
                        </td>
                    </tr>  
                </tbody>                
            </table>        
            <br />
            <table style="width: 100%; border:1px solid gray;">
                <thead>
                    <tr>
                        <th colspan="3" style="text-align: left; font-size: small"><b>Ventas NO Autorizadas:</b></th>
                    </tr>
                </thead>
                <tbody>                
                    <tr>
                        <td style="width: 33%; font-size: small">N&uacute;mero de Trx.</td>
                        <td style="width: 33%; font-size: small">Valor Total</td>
                    </tr>
                    <tr>
                        <td style="width: 33%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats5' value='<?= number_format($statsTrx["STATS6"], 0, '', '.') ?> Trx.' id='stats5' size='21' tabindex='1' disabled/>
                            </div>                        
                        </td>
                        <td style="width: 33%;">
                            <div class='fields'>
                                    <input style="text-align: center; font-size: small" type='text' name='stats6' value='$ <?= number_format($statsTrx["STATS7"], 0, '', '.') ?>' id='stats6' size='21' tabindex='1' disabled/>
                            </div>                                                
                        </td>
                    </tr>                
                </tbody>                    
            </table>                               
        
    </fieldset>                
    <br />        
    
    <div class="grid grid_12">
		
        <h2>Ventas Autorizadas X Operador - Día</h2>
			
        <table class="stats" data-chart-type="pie">
            <caption>Autorizadas por Operador</caption>
            <tbody>
                
                <?php
                    //Recorrer Salida y Armar Datos
                    foreach($stats1 as $key=>$val){                
                        echo "<tr>";
                        echo "<th>" . $val["PRO_NOMBRE"] . "</th>";
                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                        echo "</tr>";
                    }
                ?>
									
            </tbody>
        </table>
    </div> <!-- .grid -->
    
    <div class="grid grid_12">

        <h2>Ventas NO Autorizadas X Operador - Día</h2>

        <table class="stats" data-chart-type="pie">
            <caption>NO Autorizadas por Operador</caption>
            <tbody>

                <?php
                    //Recorrer Salida y Armar Datos
                    foreach($stats2 as $key=>$val){                
                        echo "<tr>";
                        echo "<th>" . $val["PRO_NOMBRE"] . "</th>";
                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                        echo "</tr>";
                    }
                ?>

            </tbody>
        </table>
    </div> <!-- .grid -->    

    <div class="grid grid_12">

        <h2>Transacciones Aprobadas - 5 Horas</h2>

        <table class="stats" data-chart-type="line">
            <caption>Autorizadas por Tipo Trx.</caption>

            <thead>
                <tr>
                        <td>&nbsp;</td>
                        <?php
                            //Recorrer Salida y Armar Datos
                            foreach($stats3Titles as $key=>$val){                
                                echo "<th>" . $val . "</th>";
                            }
                        ?>
                </tr>
            </thead>
            <tbody>
                    <tr>
                            <th>Ventas</th>
                            <?php
                                //Recorrer Salida y Armar Datos
                                foreach($stats3 as $key=>$val){  
                                    if (in_array("Ventas", $val, true))
                                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                                }
                            ?>
                    </tr>
                    <tr>
                            <th>Consultas de Saldo</th>
                            <?php
                                //Recorrer Salida y Armar Datos
                                foreach($stats3 as $key=>$val){  
                                    if (in_array("Consultas de Saldo", $val, true)) 
                                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                                }
                            ?>
                    </tr>
                    <tr>
                            <th>Echo Tests</th>
                            <?php
                                //Recorrer Salida y Armar Datos
                                foreach($stats3 as $key=>$val){  
                                    if (in_array("Echo Tests", $val, true)) 
                                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                                }
                            ?>
                    </tr>
            </tbody>
        </table>
    </div> <!-- .grid -->    
    
    <div class="grid grid_12">

        <h2>Transacciones No Aprobadas - 5 Horas</h2>

        <table class="stats" data-chart-type="line">
            <caption>No Autorizadas por Tipo Trx.</caption>

            <thead>
                <tr>
                        <td>&nbsp;</td>
                        <?php
                            //Recorrer Salida y Armar Datos
                            foreach($stats4Titles as $key=>$val){                
                                echo "<th>" . $val . "</th>";
                            }
                        ?>
                </tr>
            </thead>
            <tbody>
                    <tr>
                            <th>Ventas</th>
                            <?php
                                //Recorrer Salida y Armar Datos
                                foreach($stats4 as $key=>$val){  
                                    if (in_array("Ventas", $val, true))
                                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                                }
                            ?>
                    </tr>
                    <tr>
                            <th>Consultas de Saldo</th>
                            <?php
                                //Recorrer Salida y Armar Datos
                                foreach($stats4 as $key=>$val){  
                                    if (in_array("Consultas de Saldo", $val, true)) 
                                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                                }
                            ?>
                    </tr>
                    <tr>
                            <th>Echo Tests</th>
                            <?php
                                //Recorrer Salida y Armar Datos
                                foreach($stats4 as $key=>$val){  
                                    if (in_array("Echo Tests", $val, true)) 
                                        echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                                }
                            ?>
                    </tr>
            </tbody>
        </table>
    </div> <!-- .grid -->       
    
</div>

<div class="grid grid_6" style="position: fixed; left: 72%">
    
    <a href="javascript:;" class="btn primary xlarge block" id="form_updateAjax_btn">Actualizar Ahora</a>
    
    <div class="box">
            <h3>Informaci&oacute;n</h3>

            <p style="text-align: justify">
                Estas estad&iacute;sticas transaccionales permiten ver un panorama 
                general acerca del estado de las transacciones actuales. Número de Ventas, Consultas y Echo Test.
                <br />
                <br />
                <b>Actualizar Estad&iacute;sticas cada:</b>
                <br />
            </p>           

            <div style="text-align: center;">
                
                <a href="javascript:;" id="updateTime1" class="btn small black">15 Segs.</a> 

                <a href="javascript:;" id="updateTime2" class="btn small black">30 Segs.</a> 
                
                <a href="javascript:;" id="updateTime3" class="btn small black">60 Segs.</a>                 

            </div>
            
    </div> <!-- .box -->

    
    
</div>