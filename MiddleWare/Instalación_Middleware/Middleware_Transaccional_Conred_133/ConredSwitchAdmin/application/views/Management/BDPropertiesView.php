<div id="inbox-tabs" class="grid grid_3">
    <ul>        
        <li <?php if ($DBPropertiesDTO->id == "1") echo "class='current'"; ?> id="oracle_params">
            <a href="javascript:;">Oracle</a>
        </li>

        <li <?php if ($DBPropertiesDTO->id == "2") echo "class='current'"; ?> id="sqlserver_params">
            <a href="javascript:;" >SQL Server</a>
        </li>	

        <li <?php if ($DBPropertiesDTO->id == "3") echo "class='current'"; ?> id="postgresql_params">
            <a href="javascript:;" >PostgreSQL</a>
        </li>	

        <li <?php if ($DBPropertiesDTO->id == "4") echo "class='current'"; ?> id="mysql_params">
            <a href="javascript:;" >MySQL</a>
        </li>		
    </ul>
</div> <!-- .grid -->


<div id="DBProperties_content" class="grid grid_14 append_1">
    
        <h2>Par&aacute;metros de Configuraci&oacute;n Servidor Transaccional</h2>
    
        <form action='#' id='form_bd_params' class='form' onsubmit="return validateUpdateBDParamsForm(this);">

            <fieldset id="form-bd-0" class="step" style="display: block; ">

                <div class='field'>
                                <label for='bd_jdbc'>JDBC Driver :</label>
                                <div class='fields'>
                                        <input type='text' name='bd_jdbc' value='<?= $DBPropertiesDTO->jdbc ?>' id='bd_jdbc' size='51' tabindex='1' disabled/>
                                </div>
                </div>     

                <div class='field'>
                                <label for='bd_url'>URL Conexi&oacute;n BD :</label>                                                
                                <div class='fields'>
                                        <div class="error"><span><?= $DBPropertiesDTO->dbURLHelp ?></span></div>
                                        <textarea name='bd_url' value='' id='bd_url' style='width: 100%' rows='2' tabindex='2'><?= $DBPropertiesDTO->dbURL ?></textarea>
                                </div>
                </div>	

                <div class='field'>
                                <label for='bd_usuario'>Usuario BD :</label>
                                <div class='fields'>
                                        <input type='text' name='bd_usuario' value='<?= $DBPropertiesDTO->dbUser ?>' id='bd_usuario' size='20' tabindex='3'/>
                                </div>
                </div>

                <div class='field'>
                                <label for='bd_clave'>Clave BD :</label>
                                <div class='fields'>
                                        <input type='password' name='bd_clave' value='<?= $DBPropertiesDTO->dbPassword ?>' id='bd_clave' size='20' tabindex='4'/>
                                </div>
                </div>     
            </fieldset>                
        </form>

        <script type='text/javascript'>	
                //Establecer el Foco al elemento del formulario requerido
                setTimeout("$('#bd_url').focus()", 500);
        </script>
        
        <hr />

</div> <!-- .grid -->

<div class="grid grid_6">

    <a href="javascript:;" class="btn primary xlarge block" id="form_save_btn">Guardar Configuraci&oacute;n</a>
    <a href="javascript:;" class="btn secondary xlarge block" id="form_test_btn">Test de Conexi&oacute;n BD</a>
    
    <div class="box">
            <h3>Informaci&oacute;n</h3>

            <p style="text-align: justify">Este formulario le permite Editar los par&aacute;metros de conexi&oacute;n hacia un Servidor de Base de Datos. Los Gestores m&aacute;s reconocidos son:
                <br />
                <ul>
                    <li>Oracle</li>
                    <li>SQL Server</li>
                    <li>PostgreSQL</li>
                    <li>MySQL</li>
                </ul>
            </p>

            <p style="text-align: center; font-style: italic">
                Debe reiniciar el Servidor y los Formateadores para que los cambios tengan efecto.
            </p>                

    </div> <!-- .box -->

</div>
