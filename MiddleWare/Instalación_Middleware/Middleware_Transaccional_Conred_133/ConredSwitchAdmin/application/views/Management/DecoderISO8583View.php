<div id="Decoder_content" class="grid grid_17 append_1">

        <form action='#' id='form_decoder' class='form'>
            
            <fieldset id="form-decoder-0" class="step" style="display: block; ">

                <div class='field'>
                                <label for='decoder_bytes'><b>Corrimiento Bytes:</b></label>
                                <div class='fields'>
                                    
                                    <select name='decoder_bytes' id='decoder_bytes'  tabindex='3'>
                                        <option>0</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>                                 
                                        <option selected>7</option>
                                    </select>

                                </div>
                </div>                
                
                <div class='field'>
                                <label for='decoder_trx'><b>Trama ISO8583:</b></label>
                                <div class='fields'>
                                        <textarea name='decoder_trx' value='' id='decoder_trx' style="width: 800px" rows="10" tabindex='1'></textarea>
                                </div>
                </div>     
                
            </fieldset>                
        </form>

        <script type='text/javascript'>	
                //Establecer el Foco al elemento del formulario requerido
                setTimeout("$('#decoder_trx').focus()", 500);
        </script>
        
        <hr />

</div> <!-- .grid -->

<div class="grid grid_6">

    <a href="javascript:;" class="btn primary xlarge block" id="form_send_btn" tabindex="2">Decodificar</a>
     
    <div class="box">
            <h3>Informaci&oacute;n</h3>

            <p style="text-align: justify">Este formulario le permite decodificar transacciones ISO8583 almacenadas en el archivo de LOG del Servidor M.T.C. Copie y Pegue en este formulario la Trama en Modo Binario.

    </div> <!-- .box -->

</div>
