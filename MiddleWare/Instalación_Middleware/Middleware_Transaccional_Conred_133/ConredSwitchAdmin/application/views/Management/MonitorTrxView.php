<div class="grid_24">

    <a href="javascript:;" class="btn primary small" id="form_updateAjax_btn">Actualizar Ahora</a>
    
    <br /><br />
    
    <div id="stats_content">
    
        <h2>Cantidad de Transacciones por Minuto (30 Min. Atrás)</h2>

        <table class="stats" data-chart-type="line">
            <thead>
                <tr>
                        <td>&nbsp;</td>
                </tr>
            </thead>
            <tbody>
                    <tr>
                            <th>Transacciones</th>
                    </tr>
            </tbody>
        </table>
    
    </div>
    
    <div id="updateTime" style="text-align: right;">
        Actualiza cada 60 segundos. [ <b>Restantes 60</b> ]
    </div>
    
    <br />
    
</div> <!-- .grid -->         
