<div id="EchoTest_content" class="grid grid_17 append_1">

        <form action='#' id='form_echotest_params' class='form'>

            <h3>Campos Petici&oacute;n ISO8583:</h3>
            
            <fieldset id="form-echotest-0" class="step" style="display: block; ">
                
                <div class='field'>
                                <label for='echo_header'>Header :</label>
                                <div class='fields'>
                                        <input type='text' name='echo_header' value='60 00 01 00 00' id='echo_header' size='14' tabindex='1' disabled/>
                                </div>
                </div>

                <div class='field'>
                                <label for='echo_msg_type'>Msg. Type :</label>                                                
                                <div class='fields'>
                                        <input type='text' name='echo_msg_type' value='0800' id='echo_msg_type' size='4' tabindex='2' disabled/>
                                </div>
                </div>	

                <div class='field'>
                                <label for='echo_proc_code'>Proc. Code :</label>                                                
                                <div class='fields'>
                                    <input type='text' name='echo_proc_code' value='990000' id='echo_proc_code' size='6' maxlength='6' tabindex='4' disabled/>
                                </div>
                </div>
                
                <div class='field'>
                                <label for='echo_stan'>STAN :</label>                                                
                                <div class='fields'>
                                    <input type='text' name='echo_stan' value='<?= $stan; ?>' id='echo_stan' size='6' maxlength='6' tabindex='5' disabled/>
                                </div>
                </div>
                
                <div class='field'>
                                <label for='echo_date'>Date :</label>                                                
                                <div class='fields'>
                                    <input type='text' name='echo_date' value='<?= $date; ?>' id='echo_date' size='4' maxlength='4' tabindex='6' disabled/>
                                </div>
                </div>              

                <div class='field'>
                                <label for='echo_time'>Time :</label>                                                
                                <div class='fields'>
                                    <input type='text' name='echo_time' value='<?= $time; ?>' id='echo_time' size='6' maxlength='6' tabindex='7' disabled/>
                                </div>
                </div>              
                
            </fieldset>                
        </form>

        <script type='text/javascript'>	
                //Establecer el Foco al elemento del formulario requerido
                setTimeout("$('#echo_stan').focus()", 500);
        </script>
        
        <hr />

</div> <!-- .grid -->

<div class="grid grid_6">

    <a href="javascript:;" class="btn primary xlarge block" id="form_send_btn" tabindex="8">Enviar Echo Test</a>
    <a href="javascript:;" class="btn secondary xlarge block" id="form_restore_btn" tabindex="9">Actualizar Valores</a>
     
    <div class="box">
            <h3>Informaci&oacute;n</h3>

            <p style="text-align: justify">Este formulario le permite enviar pruebas de conexi&oacute;n hacia el Servidor M.T.C, para medir tiempos de respuesta y estado de la conectividad.

    </div> <!-- .box -->

</div>
