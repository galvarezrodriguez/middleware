<div class="grid grid_16 prepend_1">
                
    <fieldset id="custom-step-0" class="step" style="display: block;">
    
        <ul id="messagesPanel" class="message_threads">

            <?php
            
                if (count($userMessages) <= 0){                    
                    echo "No existen Mensajes del Sistema";
                }
            
                foreach($userMessages as $key => $val){        
            ?>

            <li class="<?php if ($val["leido"] == "0") echo "message alt"; else echo "message"; ?>">						
                    <img src="images/avatar.jpg" alt="Avatar" class="avatar" />

                    <div class="message_body" style="width: 85%">
                            <div class="message_text">
                                    <p class="message_author"><b><?= $val["mensaje_de"] ?></b> Dice:</p>

                                    <p><?= $val["texto"] ?></p>

                                    <p class="message_date"><?= $val["fecha_envio"] ?></p>
                                    <input type="hidden" name="msgID" id="msgID" value="<?= $val["id"] ?>" />
                            </div>	
                    </div>				
            </li>

            <?php
                }
            ?>            
			
            <div class="dataTables_paginate paging_full_numbers">
		
				<span>
					<?php
						if ($maxPages <= $maxPagesTotal){                        
							for($i=0; $i<$maxPages; $i++){
								if ($currentPage != ($i+1))
									echo "<span class='paginate_button' onclick='return goToPage(" . ($i+1) . ");'>" . ($i+1) . "</span>";
								else
									echo "<span class='paginate_active'>" . ($i+1) . "</span>";
							}
						} else {
							
							if ($currentPage > 1)
								echo "<span class='paginate_button' onclick='return goToPage(" . ($currentPage-1) . ");'>&lt;</span>";
							else 
								echo "<span class='paginate_button'>&lt;</span>";                                
							
							if ($currentPage <= $maxPagesTotal){
								for($i=0; $i<$maxPagesTotal; $i++){
									if ($currentPage != ($i+1))
										echo "<span class='paginate_button' onclick='return goToPage(" . ($i+1) . ");'>" . ($i+1) . "</span>";
									else
										echo "<span class='paginate_active'>" . ($i+1) . "</span>";
								}
							} else {
								for($i=$currentPage-1; $i<$maxPages; $i++){
									if ($currentPage != ($i+1))
										echo "<span class='paginate_button' onclick='return goToPage(" . ($i+1) . ");'>" . ($i+1) . "</span>";
									else
										echo "<span class='paginate_active'>" . ($i+1) . "</span>";
								}
							}
								
							if ($currentPage < $maxPagesTotal)
								echo "<span class='paginate_button' onclick='return goToPage(" . ($currentPage + 1) . ");'>&gt;</span>";
							else
								echo "<span class='paginate_button' onclick='return goToPage(" . ($maxPagesTotal + 1) . ");'>&gt;</span>";
						}
					?>
				</span>
				
				<div style="text-align: right;">
					<br />
					Mostrando P&aacute;gina <?= $currentPage;?> de <?= $maxPages;?>
				</div>				
				
            </div> <!-- .dataTables_paginate -->
			
        </ul>                
    </fieldset>
	
</div> <!-- .grid -->

<div class="grid grid_6 prepend_1">

        <a href="javascript:;" class="btn primary xlarge block" id="update_btn">Actualizar Mensajes</a>
        <div class="box">
                <h3>Informaci&oacute;n</h3>

                <p>Este formulario le permite visualizar los mensajes de alertas generados por los servicios transaccionales de Conred.</p>

        </div> <!-- .box -->

</div>