<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"> 
 
<head> 
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" /> 

        <title>            
            <?= $title; ?>            
        </title> 

	<base href="<?php echo $this->config->item('base_url') ?>www/" />
	
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="icon" type="image/ico" href="images/favicon.ico">        
        
        <link rel="stylesheet" href="css/all.css.php" type="text/css" media="screen" title="no title" />
        
</head> 
 
<body> 

<div id="wrapper" class="">
	
	<div id="header">		
		<div class="container">
			<div class="grid grid_24">
				
				<ul>
                                    
                                        <a href="../Index/IndexController" alt="Conred"><img class="nav left" src="images/logo.png" width="135" height="45" alt="Conred"/></a>
					
					<li class="bar"></li>

                                        <?= $menuHTML; ?>
                                        
					<li class="nav profile dropdown right">						
						<a href="javascript:;" class="item"><img src="./images/layout/nav_profile_image.png" alt="Profile Image" /></a>	
						
						<div class="menu">	
							<h3>Mi Cuenta</h3>
							
							<ul>
								<li><a href="../Account/AccountController/getAccountDetails">Configuración General</a></li>
							</ul>
							
							<hr />
							
							<ul>
								<li><a href="../Login/LoginController/logout">Cerrar Sesión</a></li>
							</ul>
						</div>
					</li>
					
                                        <!--
					<li class="nav dropdown right">						
						<a href="javascript:;" class="item icon email"><span>Bandeja de Entrada</span></a>	
						
						<div id="notify" class="notify"><?//= $unreadMessagesCounter; ?> </div>	
						
						<div class="menu">	
							<h3>Bandeja de Entrada</h3>
							
							<ul>
								<li><a href="../Inbox/InboxController">Mensajes del Sistema</a></li>
							</ul>							
					
						</div>				
					</li> 
                                        -->
                                        
					<li class="bar right"></li>			
					
					<div class="nav right">
						<table>
						<tr>
							<td align="right" style="color:white;">
								<b>
                                                                        <?= $welcomeMsg; ?>
								</b>							
							</td>
						</tr>
						</table>
					</div>	
					
				</ul>	
			</div> <!-- .grid -->
		</div> <!-- .container -->
	</div> <!-- #header -->
	
	
	<div id="sub-header" class="">		
		<div class="container">
			
			<div class="grid grid_24">
				<h2>

                                <?= $subtitle; ?>
                                    
                                </h2>	
                            
			</div> <!-- .grid -->			
		</div> <!-- .container -->
	</div> <!-- #sub-header -->
	
	<div id="content" class="">				
		<div class="container">

                    <?= $content; ?>
                        
		</div> <!-- .container -->
	</div> <!-- #content -->
	

	<div id="footer" class="cf">		
		<div class="container">

                        <div id="copyright" class="grid grid_24">
				<div class="grid grid_12 left">
                                    Copyright &copy; 2012 <?= $this->config->item('platformName') . ' v' . $this->config->item('versionAdminWeb'); ?>, Todos los derechos reservados.
				</div> <!-- .grid -->
				
			</div> <!-- .grid -->
			
		</div> <!-- .container -->		
	</div> <!-- #footer -->


</div> <!-- #wrapper -->

<script src="js/ConredPack/ReadyMade.js.php"></script>
<script src="js/ConredPack/ReadyMade.Nav.js.php"></script>
<script src="js/ConredPack/jquery-1.5.2.min.js.php"></script>
<script src="js/ConredPack/jquery.dataTables.min.js.php"></script>
<script src="js/ConredPack/modal.js.php"></script>
<script src="js/ConredPack/msgAlert.js.php"></script>
<script src="js/ConredPack/msgGrowl.js.php"></script>

<?= $jScripts; ?>

</body> 
 
</html>