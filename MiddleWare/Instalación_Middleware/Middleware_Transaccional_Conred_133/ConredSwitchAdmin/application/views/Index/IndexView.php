<div class="grid grid_24">

        <b>Refrescar Vista cada:</b>
    
        <a href="javascript:;" id="updateTime1" class="btn small black">15 Segs.</a> 

        <a href="javascript:;" id="updateTime2" class="btn small black">30 Segs.</a> 

        <a href="javascript:;" id="updateTime3" class="btn small black">60 Segs.</a>                     
    
        <br /><br />
        
        <table class="data display datatable" style="border: 1px gray solid;">
                <thead> 
                    <tr>
                        <th>FECHA/HORA TRX.</th>
                        <th>TIPO MSG. REQ.</th>
                        <th>TIPO MSG. RESP.</th>
                        <th>COD. PROC.</th>
                        <th>COD. RTA.</th>
						<th>COMERCIO</th>
                        <th>STAN</th>
                        <th>REF. CLIENTE</th>
                        <th>AUTH. ID P38</th>
                        <th>AUTH. ID OPE.</th>
                        <th>COD. BARRAS</th>
                        <th>MENSAJE DE ESTADO FINAL</th>
                    </tr> 
                </thead> 
                <tbody> 
                </tbody> 
        </table> 

</div> <!-- .grid -->
