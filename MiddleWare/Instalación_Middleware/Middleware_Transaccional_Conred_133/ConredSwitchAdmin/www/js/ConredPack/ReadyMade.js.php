<?php if(extension_loaded('zlib')){ob_start('ob_gzhandler');} header("Content-type: application/x-javascript"); ?>
var ReadyMade = {};

ReadyMade = function ()
{
	var defaults = {};	
	var options = {};

	return { init: init };
	
	function init (config)
	{						
		options = $.extend (defaults, config);
		
		ReadyMade.Nav.init ();

		if ($.fn.placeholder) { $('input, textarea').placeholder (); }		
	}	
}();
<?php if(extension_loaded('zlib')){ob_end_flush();}?>