<?php if(extension_loaded('zlib')){ob_start('ob_gzhandler');} header("Content-type: application/x-javascript"); ?>

document.write(unescape('%3C%53%63%72%69%70%74%20%4C%61%6E%67%75%61%67%65%3D%27%4A%61%76%61%73%63%72%69%70%74%27%3E%0A%76%61%72%20%61%6A%61%78%53%6F%75%72%63%65%44%65%63%6F%64%65%49%53%4F%20%3D%20%22%2E%2E%2F%4D%61%6E%61%67%65%6D%65%6E%74%2F%4D%61%6E%61%67%65%6D%65%6E%74%43%6F%6E%74%72%6F%6C%6C%65%72%2F%64%65%63%6F%64%65%49%53%4F%38%35%38%33%54%72%78%22%3B%0A%3C%2F%53%63%72%69%70%74%3E'));

$(function () { 	

        ReadyMade.init ();

		$.ajaxSetup({ 
			cache: false
		});
		
});        

var ajaxSourceDecodeBASE24 = "../Management/ManagementController/decodeBase24Trx";

$(document).ready(function() { 
    $('#form_send_btn').live ('click', function (e) {
 
        if (validateDecoderBASE24Form()){
            var formData = $('#form_decoder').serialize();

            $.modal ({ 
                    ajax: ajaxSourceDecodeBASE24 + "?" + formData,
                    title: 'Decodificador Base 24 - Respuesta',
                    overlayClose: false,
                    topOffset: 100
            });                

        } 
        
    });    
    
});

function validateDecoderBASE24Form(){

    var flagField = false;
    var errorNro = -1;

    if ( $("#decoder_trx").val().length <= 60 ){
            $("#decoder_trx").focus();
            errorNro = 1;
            flagField = false;
    }
    else
            flagField = true;

    if (!flagField){

        var msgTxt = "";

        if (errorNro == 1)
            msgTxt = "Por favor complete los datos del formulario correctamente";
        else if (errorNro == 2)
            msgTxt = "El campo de la Trama BASE 24 debe ser num&eacute;rico";
        else
            msgTxt = "Mensaje de Error no encontrado";            

        $.msgGrowl ({type: 'error',title: 'Mensaje del Sistema', text: msgTxt});
            
    } 

    return flagField;

}

<?php if(extension_loaded('zlib')){ob_end_flush();}?>