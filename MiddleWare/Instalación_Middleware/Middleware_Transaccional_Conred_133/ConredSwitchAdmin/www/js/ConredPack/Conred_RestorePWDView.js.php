<?php if(extension_loaded('zlib')){ob_start('ob_gzhandler');} header("Content-type: application/x-javascript"); ?>
$(function () { 	
    ReadyMade.init ();
    
    $("#passwordID").focus();
    
});

function validateRestorePWDForm (){

        var flagField = false;
        var errorNro = -1;
    
        if ( $("#passwordID").val().length <= 0 ){
                $("#passwordID").focus();
                errorNro = 1;
                flagField = false;
        }
        else if ( $("#passwordIDRetype").val().length <= 0 ){
                $("#passwordIDRetype").focus();
                errorNro = 1;
                flagField = false;				
        }
        else if ( $("#passwordID").val() != $("#passwordIDRetype").val() ){
                $("#passwordIDRetype").focus();
                errorNro = 2;
                flagField = false;
        }
        else if ( !validateNewPassword($("#passwordID").val())){
                $("#passwordID").focus();
                errorNro = 3;
                flagField = false;
        }         
        else
                flagField = true;

        if (!flagField){
            
            var msgTxt = "";

            if (errorNro == 1)
                msgTxt = "Por favor complete los datos del formulario correctamente";
            else if (errorNro == 2)
                msgTxt = "Los campos Nueva Clave y Repetir Clave, deben ser iguales";
            else if (errorNro == 3)
                msgTxt = "La nueva clave debe tener mínimo 6 caracteres, entre letras y números";            
            else
                msgTxt = "Mensaje de Error no encontrado";            
            
                $.msgAlert ({
                        type: 'error', 
                        title: 'Mensaje del Sistema', 
                        text: msgTxt,
                        callback: function () { 
                                $.msgAlert.close (); 

                                if ( $("#passwordID").val().length <= 0 )
                                        $("#passwordID").focus();
                                else if ( $("#passwordIDRetype").val().length <= 0 )
                                        $("#passwordIDRetype").focus();
									
                        }
                });						
        } else {
            //Hash/Salt Password
            $("#passwordID").val(hex_md5($("#passwordID").val() + hex_sha1(hex_md5($("#passwordID").val()))));
            
            $("#passwordIDRetype").val(hex_md5($("#passwordIDRetype").val() + hex_sha1(hex_md5($("#passwordIDRetype").val()))));
            
        }

        return flagField;

}

