<?php if(extension_loaded('zlib')){ob_start('ob_gzhandler');} header("Content-type: application/x-javascript"); ?>

document.write(unescape('%3C%53%63%72%69%70%74%20%4C%61%6E%67%75%61%67%65%3D%27%4A%61%76%61%73%63%72%69%70%74%27%3E%0A%76%61%72%20%61%6A%61%78%53%6F%75%72%63%65%54%72%78%20%3D%20%22%2E%2E%2F%54%72%61%6E%73%61%63%74%69%6F%6E%2F%54%72%61%6E%73%61%63%74%69%6F%6E%43%6F%6E%74%72%6F%6C%6C%65%72%2F%67%65%74%44%61%74%61%22%3B%0A%3C%2F%53%63%72%69%70%74%3E'));

//Show Processing Div
jQuery.fn.dataTableExt.oApi.fnProcessingIndicator = function ( oSettings, onoff )
{
        if( typeof(onoff) == 'undefined' )
        {
                onoff=true;
        }
        this.oApi._fnProcessingDisplay( oSettings, onoff );
}

$(function () { 	

        ReadyMade.init ();

		$.ajaxSetup({ 
			cache: false
		});
		
		
});

jQuery.fn.dataTableExt.oApi.fnDisplayStart = function ( oSettings, iStart, bRedraw )
{
        if ( typeof bRedraw == 'undefined' )
        {
                bRedraw = true;
        }

        oSettings._iDisplayStart = iStart;
        oSettings.oApi._fnCalculateEnd( oSettings );

        if ( bRedraw )
        {
                oSettings.oApi._fnDraw( oSettings );
        }
}       

//Reload Data	
function reloadData(type){	
        oTable.fnDisplayStart( oTable.fnSettings()._iDisplayStart );
        
        if (type == 1)
            $.msgGrowl ({type: 'success',title: 'Mensaje del Sistema',text: 'Vista de Transacciones Actualizada.'});        
        
}

$(document).ready(function() {

        oTable = $('.datatable').dataTable({
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': ajaxSourceTrx,
                'sPaginationType': 'full_numbers',
                'sDom': '<T><"clear"><"H"lf><"refreshBTN">rt<"F"ip>',
                'oLanguage': {
                        'sProcessing':   "<b><img src='images/loading.gif' border=0> Procesando...</b>",
                        'sLengthMenu':   'Mostrar _MENU_ registros',
                        'sZeroRecords':  'No se encontraron resultados',
                        'sInfo':         'Mostrando desde _START_ hasta _END_ de _TOTAL_ registros',
                        'sInfoEmpty':    'Mostrando desde 0 hasta 0 de 0 registros',
                        'sInfoFiltered': '(filtrado de _MAX_ registros en total)',
                        'sInfoPostFix':  '',
                        'sSearch':       'Buscar Trx.:',
                        'sUrl':          '',
                        'oPaginate': {
                                'sFirst':    'Primero',
                                'sPrevious': '&lt;',
                                'sNext':     '&gt;',
                                'sLast':     '&Uacute;ltimo'
                        }					
                },
                'bFilter': false,
                'bInfo': false,
                'aaSorting': [[0,'desc']],
                'fnServerData': function ( sSource, aoData, fnCallback ) {			
                        $.ajax( {
                                "dataType": 'json', 
                                "type": "POST", 
                                "url": sSource, 
                                "data": aoData, 
                                "success": fnCallback
                        } );
                }, 
                'aoColumns' : [   
                        { sWidth: '12%'},  /*FECHA/HORA TRX.*/
                        { sWidth: '7%'},   /*TIPO MSG. REQ.*/
                        { sWidth: '7%'},   /*TIPO MSG. RESP.*/
                        { sWidth: '8%'},   /*COD. PROC.*/
                        { sWidth: '7%'},   /*COD. RTA.*/
                        { sWidth: '9%'},   /*COMERCIO*/
                        { sWidth: '5%'},   /*STAN*/
                        { sWidth: '9%'},   /*REF. CLIENTE*/
                        { sWidth: '6%'},   /*AUTH. ID P38*/
                        { sWidth: '7%'},   /*AUTH. ID OPE.*/
                        { sWidth: '10%'},  /*COD. BARRAS*/
                        { sWidth: '13%'}   /*MENSAJE DE ESTADO FINAL*/
                ]
        }); 	

        $('div.refreshBTN').html('<a href=\'javascript:reloadData(0)\'><img src=\'images/refresh.png\'> Refrescar</a>');
        
        $('#updateTime1').live ('click', function () {
            if ($(this).hasClass('primary')){
                timeoutFlag = -1;
                $(this).removeClass("primary");
            } else {    
                $(this).addClass("primary");
                $('#updateTime2').removeClass("primary");
                $('#updateTime3').removeClass("primary");

                timeoutFlag = 15;

                setTimeout('updateStats15()', timeoutUpdateStats15);        
            }
        });     

        $('#updateTime2').live ('click', function () {
            if ($(this).hasClass('primary')){
                timeoutFlag = -1;
                $(this).removeClass("primary");
            } else {    
                $(this).addClass("primary");
                $('#updateTime1').removeClass("primary");
                $('#updateTime3').removeClass("primary");

                timeoutFlag = 30;        

                setTimeout('updateStats30()', timeoutUpdateStats30);        
            }
        });     

        $('#updateTime3').live ('click', function () {
            if ($(this).hasClass('primary')){
                timeoutFlag = -1;
                $(this).removeClass("primary");
            } else {    
                $(this).addClass("primary");
                $('#updateTime1').removeClass("primary");
                $('#updateTime2').removeClass("primary");    

                timeoutFlag = 60;

                setTimeout('updateStats60()', timeoutUpdateStats60);        
            }
        });        
        
} );

function updateStats15(){
    if (timeoutFlag == 15){
        reloadData(1); 

        setTimeout('updateStats15()', timeoutUpdateStats15);
    }
}

function updateStats30(){
    if (timeoutFlag == 30){
        reloadData(1);

        setTimeout('updateStats30()', timeoutUpdateStats30);
    }
}

function updateStats60(){
    if (timeoutFlag == 60){
        reloadData(1); 

        setTimeout('updateStats60()', timeoutUpdateStats60);
    }
}

var timeoutUpdateStats15 = 15000;
var timeoutUpdateStats30 = 30000;
var timeoutUpdateStats60 = 60000;
var timeoutFlag = -1;

<?php if(extension_loaded('zlib')){ob_end_flush();}?>