<?php if(extension_loaded('zlib')){ob_start('ob_gzhandler');} header("Content-type: application/x-javascript"); ?>
var ReadyMadeStats = {};

ReadyMadeStats = function ()
{
	var defaults = {};	
	var options = {};

	return { init: init };
	
	function init (config)
	{		
                ReadyMade.Nav.init ();
                ReadyMade.Charts.visualize ({ el: $('table.stats'), theme: options.theme });

	}	
}();
<?php if(extension_loaded('zlib')){ob_end_flush();}?>