<?php if(extension_loaded('zlib')){ob_start('ob_gzhandler');} header("Content-type: application/x-javascript"); ?>

function validateEmail(mailValue){  
    //Solo direcciones de correo correctas
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
    return emailPattern.test(mailValue);  
}

function validateNewPassword(passValue){

    var flagValidate = false;

    if (passValue.length >= 8)
        if (isAlfaNum(passValue))
            flagValidate = true;

    return flagValidate;
}

function isAlfaNum(dataValue) {

    //Solo Caracteres a-z, A-Z, Dígitos 0-9
    var patron = new RegExp("\\w+$", "i");

    if(!patron.test(dataValue))
        return false;
    else
        return true;
}

function IsNumeric(sText){
   var ValidChars = "0123456789";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){ 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
   }
      
   return IsNumber;
   
}

function IsHexNumeric(sText){
   var ValidChars = "0123456789ABCDEFabcdef ";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){ 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
   }
      
   return IsNumber;
   
}

function IsNumericIP(sText){
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

   for (i = 0; i < sText.length && IsNumber == true; i++){ 
        Char = sText.charAt(i); 
        if (ValidChars.indexOf(Char) == -1) {
            IsNumber = false;
        }
   }
      
   return IsNumber;
   
}

function isValidIPAddress(ipaddr) {
   var re = /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
   if (re.test(ipaddr)) {
      var parts = ipaddr.split(".");
      if (parseInt(parseFloat(parts[0])) == 0) { return false; }
      for (var i=0; i<parts.length; i++) {
         if (parseInt(parseFloat(parts[i])) > 255) { return false; }
      }
      return true;
   } else {
      return false;
   }
}

<?php if(extension_loaded('zlib')){ob_end_flush();}?>