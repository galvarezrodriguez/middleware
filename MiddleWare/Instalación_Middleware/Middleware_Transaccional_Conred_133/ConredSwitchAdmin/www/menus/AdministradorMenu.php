
<li class="nav">						
        <a href="../Index/IndexController" class="item icon"><span>Inicio</span></a>					
</li>	

<li class="nav dropdown">
        <a href="javascript:;" class="item">Switch</a>

        <div class="menu">							
            
                <h3>Opciones</h3>	             
                
                <ul>
                        <li><a href="../Management/ManagementController/adminServices">Admin. Servicios</a></li>
                </ul>
                
                <ul>
                        <li><a href="../Management/ManagementController/configDBProperties">Config. Base de Datos</a></li>
                </ul>                                       
                
                <ul>
                        <li><a href="../Management/ManagementController/viewMonitorTrx">Monitor Transacciones</a></li>
                </ul>                       

        </div>						
</li>

<li class="nav dropdown">
        <a href="javascript:;" class="item">Herramientas</a>

        <div class="menu">							
            
                <h3>Opciones</h3>	

                <ul>
                        <li><a href="../Management/ManagementController/sendEchoTest">Enviar Echo Test</a></li>
                </ul>                

                <ul>
                        <li><a href="../Management/ManagementController/decoderISO8583">Decodificador ISO8583</a></li>
                </ul> 				
				
                <ul>
                        <li><a href="../Management/ManagementController/decoderBase24">Decodificador BASE 24</a></li>
                </ul>                  
                
        </div>						
</li>