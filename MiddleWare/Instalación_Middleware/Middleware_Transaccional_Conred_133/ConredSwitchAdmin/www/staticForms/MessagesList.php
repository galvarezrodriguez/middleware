<?php
        
    if (count($userMessages) <= 0){
        echo "No existen Mensajes del Sistema";
    }

    foreach($userMessages as $key => $val){        
?>

<li class="<?php if ($val["leido"] == "0") echo "message alt"; else echo "message"; ?>">						
        <img src="images/avatar.jpg" alt="Avatar" class="avatar" />

        <div class="message_body" style="width: 85%">							
                <div class="message_text">
                        <p class="message_author"><b><?= $val["mensaje_de"] ?></b> Dice:</p>

                        <p><?= $val["texto"] ?></p>

                        <p class="message_date"><?= $val["fecha_envio"] ?></p>
                        <input type="hidden" name="msgID" id="msgID" value="<?= $val["id"] ?>" />
                </div>	
        </div>				
</li>

<?php
    }
?> 

<div class="dataTables_paginate paging_full_numbers">
        <span>

            <?php
                if ($maxPages <= $maxPagesTotal){                        
                    for($i=0; $i<$maxPages; $i++){
                        if ($currentPage != ($i+1))
                            echo "<span class='paginate_button' onclick='return goToPage(" . ($i+1) . ");'>" . ($i+1) . "</span>";
                        else
                            echo "<span class='paginate_active'>" . ($i+1) . "</span>";
                    }
                } else {

                    if ($currentPage > 1)
                        echo "<span class='paginate_button' onclick='return goToPage(" . ($currentPage-1) . ");'>&lt;</span>";
                    else 
                        echo "<span class='paginate_button'>&lt;</span>";                                    

                    if ($currentPage <= $maxPagesTotal){
                        for($i=0; $i<$maxPagesTotal; $i++){
                            if ($currentPage != ($i+1))
                                echo "<span class='paginate_button' onclick='return goToPage(" . ($i+1) . ");'>" . ($i+1) . "</span>";
                            else
                                echo "<span class='paginate_active'>" . ($i+1) . "</span>";
                        }
                        
                        echo "<span class='paginate_button' onclick='return goToPage(" . ($currentPage + 1) . ");'>&gt;</span>";
                        
                    } else {
                        
                        $maxPagesWindow = ceil($currentPage / $maxPagesTotal) * $maxPagesTotal;
                        $maxPagesWindowIni = (ceil($currentPage / $maxPagesTotal) - 1) * $maxPagesTotal;
                        
                        if ($maxPagesWindow > $maxPages)
                            $maxPagesWindow = $maxPages;
                        
                        for($i=$maxPagesWindowIni; $i<$maxPagesWindow; $i++){
                            if ($currentPage != ($i+1))
                                echo "<span class='paginate_button' onclick='return goToPage(" . ($i+1) . ");'>" . ($i+1) . "</span>";
                            else
                                echo "<span class='paginate_active'>" . ($i+1) . "</span>";
                        }
                        
                        if ($currentPage < $maxPages)
                            echo "<span class='paginate_button' onclick='return goToPage(" . ($currentPage + 1) . ");'>&gt;</span>";
                        else
                            echo "<span class='paginate_button'>&gt;</span>";                        
                    }
                }
            ?>

        </span>
		
		<div style="text-align: right;">
			<br />
			Mostrando P&aacute;gina <?= $currentPage;?> de <?= $maxPages;?>
		</div>						
		
</div> <!-- .dataTables_paginate -->