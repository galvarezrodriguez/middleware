<div style="width:450px;">

<form action='#' id='form_formatter' class='form' onsubmit="return validateUpdateFormatterForm(this);">

        <table>
            <tr>
                <td style="width:250px;">
                    
                    <div class='field'>
                                    <label for='f_isomuxName'>- Nombre ISOMux :</label>
                                    <div class='fields'>
                                            <input type='text' name='f_isomuxName' value='<?= $FormatterParamsDTO->isomuxName ?>' id='f_isomuxName' size='25' maxlength='20' tabindex='0' disabled/>
                                    </div>
                    </div>                    
                    
                    <div class='field'>
                                    <label for='f_localPort'>- Puerto TCP Local :</label>
                                    <div class='fields'>
                                            <input type='text' name='f_localPort' value='<?= $FormatterParamsDTO->LocalPort ?>' id='f_localPort' size='10' maxlength='5' tabindex='1'/>
                                    </div>
                    </div>	        

                    <div class='field'>
                                    <label for='f_minThreads'>- M&iacute;nimo Hilos(Trx.) :</label>
                                    <div class='fields'>
                                            <input type='text' name='f_minThreads' value='<?= $FormatterParamsDTO->minThreadPool ?>' id='f_minThreads' size='10' maxlength='3' tabindex='2' />
                                    </div>
                    </div>	

                    <div class='field'>
                                    <label for='f_maxThreads'>- M&aacute;ximo Hilos(Trx.) :</label>
                                    <div class='fields'>
                                                    <input type='text' name='f_maxThreads' value='<?= $FormatterParamsDTO->maxThreadPool ?>' id='f_maxThreads' size='10' maxlength='3' tabindex='3'/>
                                    </div>
                    </div>
                </td>                    
                <td>
                    <div class='field'>
                                    <label for='f_textLog'>- Activar Log Texto? :</label>
                                    <div class='fields'>
                                            <input type='checkbox' name='f_textLog' <? if ($FormatterParamsDTO->LocalTextLog == "1") echo "checked"; ?> id='f_textLog' tabindex='4'/>
                                    </div>
                    </div>

                    <div class='field'>
                                    <label for='f_xmlLog'>- Activar Log XML? :</label>
                                    <div class='fields'>
                                            <input type='checkbox' name='f_xmlLog' <? if ($FormatterParamsDTO->LocalXMLLog == "1") echo "checked"; ?> id='f_xmlLog' tabindex='5'/>
                                    </div>
                    </div>                    
                    
                </td>
            </tr>
        </table>
    

        <hr />
    
        <b>Datos Autorizador 
        <?php 
            switch($serviceID){                
                case 1: echo "COMCEL"; break;
                case 2: echo "MOVILRED"; break;
                case 3: echo "TIGO"; break;                
            }
        ?>
        :</b>
        <br /><br />
        
	<div class='field'>
			<label for='f_remoteIP1'>- IP Remota :</label>
			<div class='fields'>
					<input type='text' name='f_remoteIP1' value='<?= $FormatterParamsDTO->destinationIP1 ?>' id='f_remoteIP1' size='41' maxlength='15' tabindex='6' />
			</div>
	</div>                                

	<div class='field'>
			<label for='f_remotePort1'>- Puerto TCP Remoto :</label>
			<div class='fields'>
					<input type='text' name='f_remotePort1' value='<?= $FormatterParamsDTO->destinationPort1 ?>' id='f_remotePort1' size='10' maxlength='5' tabindex='7' />
			</div>
	</div>        
        
	<div class='field'>
			<label for='f_remoteTimeout1'>- Tiempo de Espera :</label>
			<div class='fields'>
					<input type='text' name='f_remoteTimeout1' value='<?= $FormatterParamsDTO->destinationTimeout1 ?>' id='f_remoteTimeout1' size='10' maxlength='6' tabindex='8' />
			</div>
                        &nbsp;&nbsp;Milisegundos.
	</div>
        
        <div style='text-align: right'>
            <button type='submit' class='btn primary' tabindex='9' id='f_submit'>Editar</button>
	</div>
        
	<div class='field'>

			<div class='fields'>
                                        <input type='hidden' name='f_serviceID' value='<?= $serviceID ?>' id='f_serviceID' />
                        </div>
        </div>
</form>

</div>

<script type='text/javascript'>	
	//Establecer el Foco al elemento del formulario requerido
	setTimeout("$('#f_localPort').focus()", 500);
</script>