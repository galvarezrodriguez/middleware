<h2>Cantidad de Transacciones por Minuto (30 Min. Atrás)</h2>

<table class="stats" data-chart-type="line">
    <thead>
        <tr>
                <td>&nbsp;</td>
                <?php
                    //Recorrer Salida y Armar Datos
                    foreach($monitorTrx as $key=>$val){
                        echo "<th>" . $val["HORA"] . ":" . $val["MINUTO"] . "</th>";
                    }
                ?>
        </tr>
    </thead>
    <tbody>
            <tr>
                    <th>Transacciones</th>
                    <?php
                        //Recorrer Salida y Armar Datos
                        foreach($monitorTrx as $key=>$val){  
                            echo "<td>" . $val["TOTAL_TRX"] . "</td>";
                        }
                    ?>
            </tr>
    </tbody>
</table>