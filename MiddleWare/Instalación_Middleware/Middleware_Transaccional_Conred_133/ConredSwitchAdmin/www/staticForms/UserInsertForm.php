<form action="#" id="form_usuario_id" class="form" onsubmit="return validateCreateForm(this);">	

        <div class="field">
                <label for="u_name">Nombre :</label>
                <div class="fields">
                    <input type="text" name="u_name" value="" id="u_name" size="41" tabindex="1" />
                </div>
        </div>	

        <div class="field">
                <label for="u_email">Correo Electr&oacute;nico :</label>
                <div class="fields">
                        <input type="text" name="u_email" value="" id="u_email" size="41" tabindex="2"/>
                </div>
        </div>

        <div class="field">
                <label for="u_login">Login :</label>
                <div class="fields">
                        <input type="text" name="u_login" value="" id="u_login" size="41" tabindex="3" />
                </div>
        </div>                                

        <div class="actions">
                <button type="submit" class="btn primary" tabindex="4" id="u_submit">Crear</button>
                
                <button type="reset" class="btn secondary" tabindex="5" id="u_reset">Limpiar</button>
                
        </div>

</form>


<script type="text/javascript">	
    //Establecer el Foco al elemento del formulario requerido
    setTimeout("$('#u_name').focus()", 500); 
                    
    $('#u_reset').live ('click', function () {
        $('#u_name').focus();
    });     
    
</script>