<h2>Par&aacute;metros de Configuraci&oacute;n Servidor Transaccional</h2>
    
<form action='#' id='form_bd_params' class='form' onsubmit="return validateUpdateBDParamsForm(this);">

    <fieldset id="form-bd-0" class="step" style="display: block; ">

        <div class='field'>
                        <label for='bd_jdbc'>JDBC Driver :</label>
                        <div class='fields'>
                                <input type='text' name='bd_jdbc' value='<?= $DBPropertiesDTO->jdbc ?>' id='bd_jdbc' size='51' tabindex='1' disabled/>
                        </div>
        </div>     

        <div class='field'>
                        <label for='bd_url'>URL Conexi&oacute;n BD :</label>                                                
                        <div class='fields'>
                                <div class="error"><span><?= $DBPropertiesDTO->dbURLHelp ?></span></div>
                                <textarea name='bd_url' value='' id='bd_url' style='width: 100%' rows='2' tabindex='2'><?= $DBPropertiesDTO->dbURL ?></textarea>
                        </div>
        </div>	

        <div class='field'>
                        <label for='bd_usuario'>Usuario BD :</label>
                        <div class='fields'>
                                <input type='text' name='bd_usuario' value='<?= $DBPropertiesDTO->dbUser ?>' id='bd_usuario' size='20' tabindex='3'/>
                        </div>
        </div>

        <div class='field'>
                        <label for='bd_clave'>Clave BD :</label>
                        <div class='fields'>
                                <input type='password' name='bd_clave' value='<?= $DBPropertiesDTO->dbPassword ?>' id='bd_clave' size='20' tabindex='4'/>
                        </div>
        </div>     
    </fieldset>                
</form>

<script type='text/javascript'>	
        //Establecer el Foco al elemento del formulario requerido
        setTimeout("$('#bd_url').focus()", 500);
</script>

<hr />