<div style="width: 500px">
    
    <table width="100%">
        <tr>
            <td><h3>Seleccione el Tipo de Canal a Monitorear:</h3></td>
        </tr>
    </table>    
    
    <form action='#' id='form_channel_select' class='form'>

        <fieldset id="form-channelSelect-0" class="step" style="display: block; ">

            <table width="100%">
                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_headerR'>Canal X Servicio :</label></td>
                    <td style="vertical-align: middle;">
                        <div class='fields'>
                            <select id="optChannels" style="width: 170px">
                                <option value="1" selected>Recargas</option>
                                <option value="2">Pago de Facturas</option>
                                <option value="3">Giros</option>
                            </select>                        
                        </div>
                    </td>
                    <td>
                        <a href="javascript:getStatusConnection(2)" class="btn blue">Verificar Estado</a>
                    </td>
                </tr>
            </table>
  
        </fieldset>                
    </form>
</div>