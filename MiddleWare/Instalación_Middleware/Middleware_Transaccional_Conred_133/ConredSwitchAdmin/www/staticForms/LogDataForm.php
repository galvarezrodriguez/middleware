<div id="logData" style="width:1200px; height:500px; overflow: auto; border: 1px black solid;">    
    <table class='data display' style="width: 1150px;">        
        <thead>
            <tr>
                <th style="width: 120px;">Fecha/Hora <img src='images/down_arrow_333333.png'/> </th>
                <th>Nivel</th>
                <th>Data</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach($linesStr as $c=>$v){
                    echo "<tr>";

                    ///Fecha/Hora
                    echo "<td>" . substr($v, 0, 24) . "</td>";

                    ///Nivel
                    echo "<td>" . substr($v, 25, 7) . "</td>";

                    //Data
                    if (strlen(substr($v, 34)) <= 250){
                        echo "<td>" . substr($v, 34) . "</td>";
                    } else {
                        echo "<td><textarea style='width: 98%;' rows='2' disable>" . substr($v, 34) . "</textarea></td>";
                    }

                    echo "</tr>";
                }
            ?>
        </tbody>
    </table>    
</div>
<br />
<a href="javascript:updateLogFileData()" class="btn small black">Actualizar</a>