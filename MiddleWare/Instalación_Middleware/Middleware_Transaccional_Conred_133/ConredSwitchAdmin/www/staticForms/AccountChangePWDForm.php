<div style="width:450px;">
<form action="#" id="form_password_id" class="form" onsubmit="return validateChangePWDForm(this);">

        <div class="field">
                <label for="u_pass1">Clave Actual:</label>
                <div class="fields">
                    <input type="password" name="u_pass1" value="" id="u_pass1" size="41" tabindex="1" />
                </div>
        </div>	

        <div class="field">
                <label for="u_pass2">Nueva Clave:</label>
                <div class="fields">
                    <input type="password" name="u_pass2" value="" id="u_pass2" size="41" tabindex="2" />
                    <img id="img_pass2" src='images/cancel.png'>
                </div>
        </div>    
    
        <div class="field">
                <label for="u_pass3">Repetir Nueva:</label>
                <div class="fields">
                    <input type="password" name="u_pass3" value="" id="u_pass3" size="41" tabindex="3" />
                    <img id="img_pass3" src='images/cancel.png'>
                </div>
        </div>    
    

        <div style='text-align: right'>
                <button type="submit" class="btn primary" tabindex="4" id="u_submit">Cambiar</button>                
                <button type="reset" class="btn secondary" tabindex="5" id="u_reset">Limpiar</button>
        </div>

</form>
</div>

<script type="text/javascript">	
    //Establecer el Foco al elemento del formulario requerido
    setTimeout("$('#u_pass1').focus()", 500); 
                    
    $('#u_reset').live ('click', function () {
        $('#u_pass1').focus();        
    });     
    
    $(function() {

        $("#u_pass2").keyup(function() {
            //Reglas para Contraseña, (Mínimo 1 Mayúscula, Mínimo 1 minúscula, Mínimo 1 dígito, Mínimo 1 Caracter Especial, y Mínimo 8 Caracteres)
            var passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[|°!\"#$%&\/\\(\)=\?'\¿¡*~{}\[\]^\-_.:,;<>+@]).{8,}$/;
            var result = passwordPattern.test($("#u_pass2").val());

            if (result == true) {
                $("#img_pass2").attr("src", "images/ok.png");
            } else {
                $("#img_pass2").attr("src", "images/cancel.png");
            }
        });
        
        $("#u_pass3").keyup(function() {
            if ($("#u_pass2").val() == $("#u_pass3").val()) {
                $("#img_pass3").attr("src", "images/ok.png");
            } else {
                $("#img_pass3").attr("src", "images/cancel.png");
            }
        });        
        
    });

</script>