<div style="width:450px;">
<form action='#' id='form_account_id_e' class='form' onsubmit="return validateUpdateAccountForm(this);">

	<div class='field'>
			<label for='u_login_e'>Login :</label>
			<div class='fields'>
                                <input type='text' name='u_login_e' value='<?= $userData->usu_login ?>' id='u_login_e' size='41' tabindex='3' disabled />
			</div>
	</div>     
    
	<div class='field'>
			<label for='u_name_e'>Nombre :</label>
			<div class='fields'>
				<input type='text' name='u_name_e' value='<?= $userData->usu_nombre ?>' id='u_name_e' size='41' tabindex='1'/>
			</div>
	</div>	

	<div class='field'>
			<label for='u_email_e'>Correo Electr&oacute;nico :</label>
			<div class='fields'>
                                <input type='text' name='u_email_e' value='<?= $userData->usu_email ?>' id='u_email_e' size='41' tabindex='2'/>
			</div>
	</div>

	<div style='text-align: right'>
            <button type='submit' class='btn primary' tabindex='4' id='u_submit_e'>Editar</button>
	</div>

</form>
</div>

<script type='text/javascript'>	
	//Establecer el Foco al elemento del formulario requerido
	setTimeout("$('#u_name_e').focus()", 500);
</script>