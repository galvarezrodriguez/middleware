<div style="width: 500px">
    
    <table width="100%">
        <tr>
            <td><h3>Campos Respuesta ISO8583:</h3></td>
            <td style='text-align: right; color: red'>Trx. Procesada en [<?= $trx_time ?>] milisegs.</td>
        </tr>
    </table>    
    
    <form action='#' id='form_echotest_paramsR' class='form'>

        <fieldset id="form-echotestR-0" class="step" style="display: block; ">

            <table width="100%">
                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_headerR'>Header :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_headerR' value='60 00 00 00 01' id='echo_headerR' size='14' tabindex='1' disabled/></td>
                </tr>

                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_msg_typeR'>Msg. Type :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_msg_typeR' value='<?= $msg_type ?>' id='echo_msg_typeR' size='4' tabindex='2' disabled/></td>
                </tr>

                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_proc_codeR'>Proc. Code :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_proc_codeR' value='<?= $proc_code ?>' id='echo_proc_codeR' size='6' tabindex='3' disabled/></td>
                </tr>                
                
                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_dateR'>Date :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_dateR' value='<?= $date; ?>' id='echo_dateR' size='4' maxlength='12' tabindex='4' disabled/></td>
                </tr>

                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_timeR'>Time :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_timeR' value='<?= $time; ?>' id='echo_timeR' size='6' maxlength='12' tabindex='4' disabled/></td>
                </tr>
                
                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_stanR'>STAN :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_stanR' value='<?= $stan; ?>' id='echo_stanR' size='6' maxlength='6' tabindex='5' disabled/></td>
                </tr>

                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_rspCodeR'>Resp. Code :</label></td>
                    <td style="vertical-align: middle;"><input type='text' name='echo_rspCodeR' value='<?= $rsp_code; ?>' id='echo_rspCodeR' size='2' maxlength='2' tabindex='8' disabled/></td>
                </tr>

                <tr style="height: 30px">
                    <td style="vertical-align: middle; width: 100px"><label for='echo_rspMsgR'>Resp. Message :</label></td>
                    <td style="vertical-align: middle;"><input type='text' style="background-color: #FAF4F4; color: red" name='echo_rspMsgR' value='<?= $rsp_msg; ?>' id='echo_rspMsgR' size='40' maxlength='40' tabindex='9' disabled/></td>
                </tr>            

            </table>
  
        </fieldset>                
    </form>
</div>