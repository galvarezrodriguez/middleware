<div style="width:750px;">

<form action='#' id='form_formatter' class='form' onsubmit="return validateUpdateFormatterMovilredForm(this);">

    <div class='field'>
        <div class='fields'>
            <input type='hidden' name='f_serviceID' value='<?= $serviceID ?>' id='f_serviceID' />
        </div>
    </div>
    
    <fieldset title="Config. General" id="custom-step-0" class="step" style="display: block; ">
        <legend></legend>

        <table style="font-size:12px;">
            <tr>
                <td style="width:350px;">

                    <div class='field'>
                        - Nombre ISOMux :
                        <div class='fields'>
                                <input type='text' name='f_isomuxName' value='<?= $FormatterParamsDTO->isomuxName ?>' id='f_isomuxName' size='25' maxlength='20' tabindex='1' disabled/>
                        </div>
                    </div>   
                </td>                    
                <td>
                    <div class='field'>
                        - Log Texto? :<input type='checkbox' name='f_textLog' <? if ($FormatterParamsDTO->LocalTextLog == "1") echo "checked"; ?> id='f_textLog' tabindex='2'/>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        - Log XML? : <input type='checkbox' name='f_xmlLog' <? if ($FormatterParamsDTO->LocalXMLLog == "1") echo "checked"; ?> id='f_xmlLog' tabindex='3'/>
                    </div>                    
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <table>
                        <tr>
                            <td style="width:200px;">- Puerto TCP Local :</td>
                            <td style="width:200px;">- M&iacute;nimo Hilos(Trx.) :</td>
                            <td style="width:200px;">- M&aacute;ximo Hilos(Trx.) :</td>
                        </tr>
                        <tr>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='f_localPort' value='<?= $FormatterParamsDTO->LocalPort ?>' id='f_localPort' size='10' maxlength='5' tabindex='4'/>
                                </div>
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='f_minThreads' value='<?= $FormatterParamsDTO->minThreadPool ?>' id='f_minThreads' size='10' maxlength='3' tabindex='5' />
                                </div>
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='f_maxThreads' value='<?= $FormatterParamsDTO->maxThreadPool ?>' id='f_maxThreads' size='10' maxlength='3' tabindex='6'/>
                                </div>    
                            </td>
                        </tr> 
                        
                    </table>
                    
                    <br/>
                    
                    <table>
                        <tr>
                            <td style="width:200px;">- NIT Conred :</td>
                            <td style="width:200px;">- Usuario Conred :</td>
                            <td style="width:200px;">- Clave Conred :</td>
                        </tr>                        
                        <tr>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='f_nit' value='<?= $FormatterParamsDTO->NIT ?>' id='f_nit' size='21' maxlength='15' tabindex='7' />
                                </div>                                
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='f_user' value='<?= $FormatterParamsDTO->user ?>' id='f_user' size='21' maxlength='20' tabindex='8' />
                                </div>                                
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='password' name='f_password' value='<?= $FormatterParamsDTO->password ?>' id='f_password' size='21' maxlength='20' tabindex='9' />
                                </div>                                
                            </td>
                        </tr>                                                
                    </table>
                    
                </td>
            </tr>
        </table>

    </fieldset>

    <fieldset title="Recargas" id="custom-step-1" class="step" style="display: none; ">
        <legend></legend>

        <table style="font-size:12px;">
            <tr>
                
                <td style="width:200px;">- IP Remota :</td>
                <td style="width:200px;">- Puerto TCP Remoto :</td>
                <td style="width:200px;">- Tiempo de Espera :</td>
            </tr>
            <tr>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remoteIP1' value='<?= $FormatterParamsDTO->destinationIP1 ?>' id='f_remoteIP1' size='21' maxlength='15' tabindex='10' />
                    </div>                    
                </td>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remotePort1' value='<?= $FormatterParamsDTO->destinationPort1 ?>' id='f_remotePort1' size='10' maxlength='5' tabindex='11' />
                    </div>                    
                </td>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remoteTimeout1' value='<?= $FormatterParamsDTO->destinationTimeout1 ?>' id='f_remoteTimeout1' size='10' maxlength='6' tabindex='12' />
                    </div>
                    &nbsp;Milisegs.                    
                </td>                
                
            </tr>
        </table>
    
    </fieldset>    
    
    <fieldset title="Pago Facturas" id="custom-step-2" class="step" style="display: none; ">
        <legend></legend>

        <table style="font-size:12px;">
            <tr>
                
                <td style="width:200px;">- IP Remota :</td>
                <td style="width:200px;">- Puerto TCP Remoto :</td>
                <td style="width:200px;">- Tiempo de Espera :</td>
            </tr>
            <tr>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remoteIP2' value='<?= $FormatterParamsDTO->destinationIP2 ?>' id='f_remoteIP2' size='21' maxlength='15' tabindex='13' />
                    </div>                    
                </td>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remotePort2' value='<?= $FormatterParamsDTO->destinationPort2 ?>' id='f_remotePort2' size='10' maxlength='5' tabindex='14' />
                    </div>                    
                </td>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remoteTimeout2' value='<?= $FormatterParamsDTO->destinationTimeout2 ?>' id='f_remoteTimeout2' size='10' maxlength='6' tabindex='15' />
                    </div>
                    &nbsp;Milisegs.                    
                </td>                
                
            </tr>
        </table>
        
    </fieldset>    
    
    <fieldset title="Giros" id="custom-step-3" class="step" style="display: none; ">
        <legend></legend>

        <table style="font-size:12px;">
            <tr>
                
                <td style="width:200px;">- IP Remota :</td>
                <td style="width:200px;">- Puerto TCP Remoto :</td>
                <td style="width:200px;">- Tiempo de Espera :</td>
            </tr>
            <tr>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remoteIP3' value='<?= $FormatterParamsDTO->destinationIP3 ?>' id='f_remoteIP3' size='21' maxlength='15' tabindex='16' />
                    </div>                    
                </td>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remotePort3' value='<?= $FormatterParamsDTO->destinationPort3 ?>' id='f_remotePort3' size='10' maxlength='5' tabindex='17' />
                    </div>                    
                </td>
                <td>
                    <div class='fields'>
                        <input type='text' name='f_remoteTimeout3' value='<?= $FormatterParamsDTO->destinationTimeout3 ?>' id='f_remoteTimeout3' size='10' maxlength='6' tabindex='18' />
                    </div>
                    &nbsp;Milisegs.                    
                </td>                
                
            </tr>
        </table>

        <br />
        <br />
        
        <div style='text-align: right'>
            <button type='submit' class='btn primary' tabindex='19' id='f_submit'>Guardar Configuraci&oacute;n</button>
        </div>
        
    </fieldset>     
    
</form>
    
</div>

<script type='text/javascript'>	
	//Establecer el Foco al elemento del formulario requerido
	setTimeout("$('#f_localPort').focus()", 500);                
        
        $('#form_formatter').stepy({
	  backLabel:      '<< Atrás',
	  block:          true,
	  nextLabel:      'Siguiente >>',
	  titleClick:     true
	});        
        
</script>