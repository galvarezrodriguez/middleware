<div style="width:600px;">

<form action='#' id='form_server' class='form' onsubmit="return validateUpdateServerForm(this);">

        <table>
            <tr>
                <td>
                    <div class='field'>
                        - Activar Log Texto? : <input type='checkbox' name='s_textLog' <? if ($ServerParamsDTO->LocalTextLog == "1") echo "checked"; ?> id='s_textLog' tabindex='1'/>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        - Activar Log XML? : <input type='checkbox' name='s_xmlLog' <? if ($ServerParamsDTO->LocalXMLLog == "1") echo "checked"; ?> id='s_xmlLog' tabindex='2'/>
                    </div>                    
                </td>                
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td style="width:200px;">- Puerto TCP Local :</td>
                            <td style="width:200px;">- M&iacute;nimo Hilos(Trx.) :</td>
                            <td style="width:200px;">- M&aacute;ximo Hilos(Trx.) :</td>
                        </tr>
                        <tr>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_localPort' value='<?= $ServerParamsDTO->LocalPort ?>' id='s_localPort' size='10' maxlength='5' tabindex='3'/>
                                </div>                                
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_minThreads' value='<?= $ServerParamsDTO->minThreadPool ?>' id='s_minThreads' size='10' maxlength='3' tabindex='4' />
                                </div>                                
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_maxThreads' value='<?= $ServerParamsDTO->maxThreadPool ?>' id='s_maxThreads' size='10' maxlength='3' tabindex='5'/>
                                </div>                                
                            </td>                            
                        </tr>
                        
                        <tr>
                            <td>- Tiempo de Espera :</td>
                            <td>- N&uacute;mero Intentos POS:</td>
                            <td>- Valor M&iacute;n. Venta(Trx) :</td>
                        </tr> 
                        <tr>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_totalTimeout' value='<?= $ServerParamsDTO->TotalTimeoutJTranServer ?>' id='s_totalTimeout' size='10' maxlength='6' tabindex='6' />
                                    &nbsp;Miliseg.
                                </div>                                
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_maxAttempsConnection' value='<?= $ServerParamsDTO->maxAttempsConnection ?>' id='s_maxAttempsConnection' size='10' maxlength='1' tabindex='7' />
                                </div>                                
                            </td>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_minSaleAmount' value='<?= $ServerParamsDTO->minSaleAmount ?>' id='s_minSaleAmount' size='10' maxlength='6' tabindex='8' />
                                </div>                                
                            </td>                            
                        </tr>
                        
                        <tr>
                            <td>- Valor M&aacute;x. Venta(Trx) :</td>
                            <td></td>
                            <td></td>
                        </tr> 
                        
                        <tr>
                            <td>
                                <div class='fields'>
                                    <input type='text' name='s_maxSaleAmount' value='<?= $ServerParamsDTO->maxSaleAmount ?>' id='s_maxSaleAmount' size='10' maxlength='6' tabindex='9' />
                                </div>                                
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                
                            </td>                            
                        </tr>                        
                        
                    </table>
                    
                </td>
            </tr>
        </table>
    
        <div style='text-align: right'>
            <button type='submit' class='btn primary' tabindex='10' id='s_submit'>Editar</button>
	</div>
        
	<div class='field'>
            <div class='fields'>
                <input type='hidden' name='s_instanceID' value='<?= $instanceID ?>' id='s_instanceID' />
            </div>
        </div>    
    
</form>

<hr />    
    
<table class='data display'>
    <thead>
        <tr>
            <th style="width: 100px; vertical-align: middle">Formateador</th>
            <th style="width: 100px; vertical-align: middle">Nombre ISOMux</th>
            <th style="width: 100px; vertical-align: middle">IP Destino</th>
            <th style="vertical-align: middle">Puerto TCP Destino</th>
            <th style="vertical-align: middle">Tiempo de Espera Total (Miliseg.)</th>
        </tr>
    </thead>
    <tbody>
        <!--
        <tr>
            <td>+ COMCEL</td>
            <td><?//= $ServerParamsDTO->isomuxNameComcelFormatter ?></td>
            <td><?//= $ServerParamsDTO->IPComcelFormatter ?></td>
            <td><?//= $ServerParamsDTO->PortComcelFormatter ?></td>
            <td><?//= $ServerParamsDTO->totalTimeoutComcelFormatter ?></td>
        </tr>
        -->
        <tr>
            <td>+ MOVILRED</td>
            <td><?= $ServerParamsDTO->isomuxNameMovilredFormatter ?></td>
            <td><?= $ServerParamsDTO->IPMovilredFormatter ?></td>
            <td><?= $ServerParamsDTO->PortMovilredFormatter ?></td>
            <td><?= $ServerParamsDTO->totalTimeoutMovilredFormatter ?></td>
        </tr>

        <!--
        <tr>
            <td>+ TIGO</td>
            <td><?//= $ServerParamsDTO->isomuxNameTigoFormatter ?></td>
            <td><?//= $ServerParamsDTO->IPTigoFormatter ?></td>
            <td><?//= $ServerParamsDTO->PortTigoFormatter ?></td>
            <td><?//= $ServerParamsDTO->totalTimeoutTigoFormatter ?></td>
        </tr>                
        -->
    </tbody>
</table>             
    
</div>

<script type='text/javascript'>	
	//Establecer el Foco al elemento del formulario requerido
	setTimeout("$('#s_localPort').focus()", 500);
</script>