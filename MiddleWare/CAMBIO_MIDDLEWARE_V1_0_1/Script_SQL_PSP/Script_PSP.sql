USE [Conred]
GO

/*SENTENCIAS INSERT*/

INSERT [dbo].[ConfiguracionFormateador] ([nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (N'MOVILRED_FORMATTER', N'DistributorID', N'C�d. de Distribuidor entregado por Movilred', N'130594')
INSERT [dbo].[ConfiguracionFormateador] ([nombre_formateador], [nombre_parametro], [descripcion_parametro], [dato]) VALUES (N'MOVILRED_FORMATTER', N'DistributorName', N'Nombre de Distribuidor entregado por Movilred', N'CARVAJAL')

GO

INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'JTS400000', N'Reverso POS Procesado', N'00')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100001', N'Ya existe una transaccion con las mismas condiciones que el recaudo actual', N'MF')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100002', N'El distribuidor no tiene configurado el atributo 5', N'T9')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100003', N'El punto de venta tiene el credito bloqueado', N'O7')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100004', N'Distribuidor no se encuentra habilitado', N'N3')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100005', N'El punto de venta no se encuentra habilitado', N'O6')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100006', N'El dispositivo no se encuentra habilitado para el punto de venta', N'N2')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100007', N'El dispositivo se encuentra inactivo', N'N4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100008', N'El punto de venta no tiene el producto  o no es un punto multiproducto', N'M2')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100009', N'Ya existen pagos para la misma factura', N'72')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100010', N'No existe el convenio con este codigo o el convenio no esta habilitado', N'N7')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100011', N'Se encontraron cuentas tal que su saldo es insuficiente para realizar la transaccion', N'NJ')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100012', N'La cuenta sobre la cual se valida el saldo diario y la l�nea de cr�dito no se encuentra configurada', N'T3')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100013', N'No se encuentra la cuenta configurada en validacioncuentas para ese tercero', N'T4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100014', N'El punto no tiene credito', N'P3')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100015', N'Ocurrio un error al insertar la transaccion y el servicio no monetario', N'T5')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100017', N'El dispositivo no se encuentra habilitado para el punto', N'N2')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100018', N'Error procesando el codigo de barras', N'N5')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100019', N'El valor enviado en la trama no corresponde al del c�digo de barras', N'MY')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100022', N'Punto sin saldo', N'P4')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100023', N'Error de comunicaciones', N'T6')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100024', N'Error de saldo', N'T7')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100025', N'Transaccion rechazada', N'NM')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100026', N'Sin autorizacion banco', N'T8')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100027', N'Intentos alcanzados', N'TA')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100028', N'Cliente erroneo', N'TB')
INSERT [dbo].[MensajeEstado] ([cod_estado], [mensaje], [cod_respuesta_ISO]) VALUES (N'MOV100099', N'Error General Movilred', N'NH')

GO

/*SENTENCIAS STORE PROCEDURES*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_webConsultarTrxMovilredPagoFacturas](
	@fechaConsultaTxt AS VARCHAR(10)
)
AS
BEGIN

	SET NOCOUNT ON;
	SET DATEFORMAT MDY
	
	DECLARE @fechaConsultaIni AS DATETIME
	DECLARE @fechaConsultaFin AS DATETIME
	DECLARE @distributorID AS VARCHAR(20)
	DECLARE @distributorName AS VARCHAR(50)
	DECLARE @reversosTMP1 AS TABLE(tra_id BIGINT, tra_auth_id_p38 VARCHAR(6))
	DECLARE @trxTMP AS TABLE(tra_id BIGINT)
	DECLARE @reversosTMP2 AS TABLE(tra_id BIGINT, tra_auth_id_p38 VARCHAR(6))
	
	SET @fechaConsultaIni = CAST(@fechaConsultaTxt AS DATETIME)
	SET @fechaConsultaFin = DATEADD(DAY,1,@fechaConsultaIni)

	--CONSULTAR REVERSOS ENVIADOS Y PROCESADOS CORRECTAMENTE
	INSERT INTO @reversosTMP1
		SELECT T.tra_id, TXR.tra_auth_id_p38
		FROM Transaccion T WITH(NOLOCK) 
			INNER JOIN TransaccionXReverso TXR WITH(NOLOCK) ON (TXR.tra_id_original = T.tra_id AND TXR.tra_codigo_respuesta = '00')
		WHERE T.tra_tipo_mensaje_req IN ('0200') AND T.tra_codigo_proceso IN ('150000') AND
			T.tra_fecha_inicial > @fechaConsultaIni AND T.tra_fecha_fin < @fechaConsultaFin

	INSERT INTO @trxTMP
		SELECT T.tra_id
		FROM Transaccion T WITH(NOLOCK) 
			INNER JOIN TransaccionXReverso TXR WITH(NOLOCK) ON (TXR.tra_id_original = T.tra_id AND TXR.tra_codigo_respuesta = '00')
		WHERE T.tra_tipo_mensaje_req IN ('0400') AND T.tra_codigo_proceso IN ('150000') AND
			T.tra_fecha_inicial > @fechaConsultaIni AND T.tra_fecha_fin < @fechaConsultaFin
	
	INSERT INTO @reversosTMP2	
		SELECT T1.tra_id, T1.tra_auth_id_p38
		FROM Transaccion T1 WITH(NOLOCK)
			INNER JOIN Transaccion T2 WITH(NOLOCK) ON (T1.tra_stan = T2.tra_stan AND T1.tra_datos_adicionales = T2.tra_datos_adicionales AND T1.tra_tipo_mensaje_req = '0200')
		WHERE T1.tra_fecha_inicial > @fechaConsultaIni AND T1.tra_fecha_fin < @fechaConsultaFin AND
			T2.tra_tipo_mensaje_req = '0400' AND
			T2.tra_codigo_respuesta = 'B3' AND 
			T1.tra_codigo_proceso IN ('150000')	AND
			T2.tra_id IN (SELECT tra_id FROM @trxTMP)	--LOS 0400 REVERSADOS

	--DATOS FIJOS
	SELECT @distributorID = dato FROM ConfiguracionFormateador WHERE nombre_formateador = 'MOVILRED_FORMATTER' AND nombre_parametro = 'DistributorID'
	SELECT @distributorName = dato FROM ConfiguracionFormateador WHERE nombre_formateador = 'MOVILRED_FORMATTER' AND nombre_parametro = 'DistributorName'

	SELECT 
		REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 102), '.', '-'),
		UPPER(P.pro_nombre),
		ISNULL(CAST(T.tra_auth_id_p38 AS VARCHAR(12)), '0'),
		CAST(T.tra_rrn AS VARCHAR(12)),
		CAST(CAST(T.tra_valor AS INT) AS VARCHAR),
		CASE WHEN T.tra_codigo_respuestaB24 = '00' THEN 'APROBADO' ELSE 'RECHAZADO' END,
		SUBSTRING(T.tra_datos_adicionales,4,13),
		(SELECT UPPER(conv_nombre) FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(T.tra_datos_adicionales,4,13)),
		@distributorID,
		@distributorName,
		tra_comercio_id,
		'COMERCIO ' + tra_comercio_id,
		T.tra_referencia_cliente_1
	FROM Transaccion T WITH(NOLOCK)
		INNER JOIN Producto P ON (T.tra_producto_id = P.pro_id)
	WHERE T.tra_fecha_inicial > @fechaConsultaIni AND T.tra_fecha_fin < @fechaConsultaFin
		AND T.tra_tipo_mensaje_req = '0200' --AND T.tra_codigo_respuesta = '00'
		AND T.tra_codigo_proceso = '150000'	--SOLO PAGO DE FACTURAS
		AND tra_codigo_respuesta NOT IN ('68', 'B3', 'A4')	--NI REVERSOS POR TO NI REVERSOS DESDE EL POS NI TRX. RECIBIDA FUERA TIEMPO
		AND T.tra_id NOT IN (SELECT tra_id FROM @reversosTMP1)	--NO COLOCAR REVERSOS APROBADOS 1
		AND T.tra_id NOT IN (SELECT tra_id FROM @reversosTMP2)	--NO COLOCAR REVERSOS APROBADOS 2

	UNION ALL
	
	SELECT 
		REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 102), '.', '-'),
		UPPER(P.pro_nombre),
		ISNULL(CAST(R.tra_auth_id_p38 AS VARCHAR(12)), '0'),
		CAST(T.tra_rrn AS VARCHAR(12)),
		CAST(CAST(T.tra_valor AS INT) AS VARCHAR),
		CASE WHEN T.tra_codigo_respuestaB24 = '00' THEN 'APROBADO' ELSE 'REVERSADO' END,
		SUBSTRING(T.tra_datos_adicionales,4,13),
		(SELECT UPPER(conv_nombre) FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(T.tra_datos_adicionales,4,13)),
		@distributorID,
		@distributorName,
		tra_comercio_id,
		'COMERCIO ' + tra_comercio_id,
		T.tra_referencia_cliente_1
	FROM Transaccion T WITH(NOLOCK)
		INNER JOIN Producto P ON (T.tra_producto_id = P.pro_id)
		INNER JOIN @reversosTMP1 R ON (R.tra_id = T.tra_id)	--MOSTRAR REVERSOS APROBADOS
	WHERE T.tra_fecha_inicial > @fechaConsultaIni AND T.tra_fecha_fin < @fechaConsultaFin
		AND T.tra_tipo_mensaje_req = '0200'
		AND T.tra_codigo_proceso = '150000'	--SOLO PAGO DE FACTURAS

	UNION ALL
	
	SELECT 
		REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 102), '.', '-'),
		UPPER(P.pro_nombre),
		ISNULL(CAST(R.tra_auth_id_p38 AS VARCHAR(12)), '0'),
		CAST(T.tra_rrn AS VARCHAR(12)),
		CAST(CAST(T.tra_valor AS INT) AS VARCHAR),
		CASE WHEN T.tra_codigo_respuestaB24 = '00' THEN 'REVERSADO' ELSE 'REVERSADO' END,
		SUBSTRING(T.tra_datos_adicionales,4,13),
		(SELECT UPPER(conv_nombre) FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(T.tra_datos_adicionales,4,13)),
		@distributorID,
		@distributorName,
		tra_comercio_id,
		'COMERCIO ' + tra_comercio_id,
		T.tra_referencia_cliente_1
	FROM Transaccion T WITH(NOLOCK)
		INNER JOIN Producto P ON (T.tra_producto_id = P.pro_id)
		INNER JOIN @reversosTMP2 R ON (R.tra_id = T.tra_id)	--MOSTRAR REVERSOS APROBADOS
	WHERE T.tra_fecha_inicial > @fechaConsultaIni AND T.tra_fecha_fin < @fechaConsultaFin
		AND T.tra_tipo_mensaje_req = '0200'
		AND T.tra_codigo_proceso = '150000'	--SOLO PAGO DE FACTURAS	
	
END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_mtcPreProcesar](
	/*PAR�METROS DE ENTRADA*/
	@tipoMensajeReq AS VARCHAR(10),	
	@interfazOrigen AS VARCHAR(12),
	@codigoProcesamiento AS VARCHAR(10),
	@stan AS INT, 
	@rrn AS VARCHAR(20),
	@ptoVentaID AS VARCHAR(20),
	@comercioID AS VARCHAR(20), 
	@productoEAN AS VARCHAR(20), 
	@refCliente1 AS VARCHAR(50), 
	@datosAdicionales AS VARCHAR(100),
	@valor AS MONEY, 
	@IP AS VARCHAR(20), 
	@puertoTCP AS INT,
	/*PAR�METROS DE SALIDA*/
	@rutearTrx AS SMALLINT OUTPUT, 
	@codRespuesta AS VARCHAR(10) OUTPUT, 
	@msjRespuesta AS VARCHAR(500) OUTPUT, 
	@trxID AS BIGINT OUTPUT,
	@isoMuxName AS VARCHAR(50) OUTPUT,	--A donde rutear la trx. con base en parametrizaci�n tabla: Matriz_NivelEntidadXProducto
	@codRespuestaISO VARCHAR(2) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES LOCALES*/
	DECLARE @tra_comercio_saldo AS MONEY
	DECLARE @validacion_local AS SMALLINT	/*1-SI , 0-NO*/
	DECLARE @productoID_BD AS BIGINT
	DECLARE @refCliente2 AS VARCHAR(50)
	DECLARE @interfazDestino AS VARCHAR(20)
	
	BEGIN TRANSACTION

	SET @interfazDestino = ''

	BEGIN TRY
		/*INICIO VALIDACIONES DEL NEGOCIO*/

		IF NOT EXISTS(SELECT * FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID)))	--EXISTE COMERCIO?

			SELECT @codRespuesta = 'JTS000001', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = '11'

		ELSE IF NOT EXISTS(SELECT * FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID)		--EXISTE PUNTO DE VENTA?

			SELECT @codRespuesta = 'JTS000002', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N1'

		ELSE IF NOT EXISTS(SELECT * FROM PuntoVentaXEntidad WHERE PTO_ID = (SELECT PTO_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) AND ENT_ID = (SELECT ent_id FROM ENTIDAD WHERE ent_comercio_JNTS = @comercioID) AND FECHA_FINAL IS NULL)	--PUNTO DE VENTA RELACIONADO AL COMERCIO

			SELECT @codRespuesta = 'JTS000003', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N2'

		ELSE IF (SELECT ENT_ESTADO_ENTIDAD_ID FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID))) > 1	--COMERCIO ACTIVO?

			SELECT @codRespuesta = 'JTS000004', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N3'

		ELSE IF (SELECT PTO_ESTADO_PUNTO_VENTA_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) > 1	--PUNTO DE VENTA ACTIVO?

			SELECT @codRespuesta = 'JTS000005', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'N4'

		ELSE IF DBO.[ValidarSaldo](LTRIM(RTRIM(@comercioID)), @valor) = 0	--COMERCIO CON SALDO?

			SELECT @codRespuesta = 'JTS000006', @validacion_local = 0, @tra_comercio_saldo = 0, @codRespuestaISO = 'P3'

		ELSE
			--TODO OK, CONTINUAR CON TRANSACCI�N
			SELECT @codRespuesta = 'JTS000000', @validacion_local = 1, @codRespuestaISO = NULL, @tra_comercio_saldo = DBO.[ObtenerSaldo](@comercioID, 0 /*NO TRANSACCIONAL*/)

		/*FIN VALIDACIONES DEL NEGOCIO*/

		IF @validacion_local = 1
		BEGIN
			/*	REGLAS DE NEGOCIO CUMPLIDAS, @validacion_local = 1	*/

			/*VENTA DE RECARGAS A CELULAR*/
			IF (@tipoMensajeReq = '0200' AND @codigoProcesamiento = '380000')
				/*RECAUDO DE FACTURAS*/
				OR (@tipoMensajeReq = '0200' AND @codigoProcesamiento = '150000')
			BEGIN

				/*OBTENER COD. DE PRODUCTO E ISOMUX_NAME SEG�N EAN*/
				SELECT 
					@productoID_BD = MNEP.producto_id,
					@isoMuxName = PR.prov_isomux_nombre
				FROM Matriz_NivelEntidadXProducto MNEP
					INNER JOIN Producto P ON (MNEP.producto_id = P.pro_id)
					INNER JOIN Proveedor PR ON (P.pro_proveedor_id = PR.prov_id)
					INNER JOIN TipoNivelEntidad TNE ON (MNEP.tipo_nivel_entidad_id = TNE.id)
					INNER JOIN Entidad E ON (E.ent_tipo_nivel_entidad_id = TNE.id)
				WHERE
					E.ent_comercio_JNTS = LTRIM(RTRIM(@comercioID)) AND P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))

				/*VALIDAR EL C�DIGO DE PRODUCTO*/
				IF @productoID_BD IS NULL	--EXISTE PRODUCTO?
					SELECT @rutearTrx = 0, @interfazDestino = '', @isoMuxName = '', @codRespuesta = 'JTS000007', @codRespuestaISO = '12'
				ELSE
					SELECT @rutearTrx = 1, @interfazDestino = @isoMuxName, @codRespuestaISO = '00'
				
				/*ACCIONES ESPEC�FICAS POR PRODUCTO*/
				IF @rutearTrx = 1 
				BEGIN
					--RECAUDO DE FACTURAS
					IF @tipoMensajeReq = '0200' AND @codigoProcesamiento = '150000'
					BEGIN
					
						IF LEN(@datosAdicionales) > 16
						BEGIN 
							
							IF SUBSTRING(@datosAdicionales, 1, 3) = '415'	--ID INICIAL C�DIGO DE BARRAS COLOMBIA
							BEGIN
							
								IF EXISTS(SELECT * FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD)
								BEGIN
									--SEPARAR CAMPOS DEL C�DIGO DE BARRAS
									SELECT @refCliente1=referencia1, @refCliente2=referencia2 FROM dbo.[SepararCamposCodigoBarras](@datosAdicionales, @productoID_BD)
								END
								ELSE
								BEGIN
									--CONVENIO NO ACTIVO
									SELECT @rutearTrx = 0, @codRespuesta = 'JTS000011', @codRespuestaISO = 'N7'									
								END
							END
							ELSE
							BEGIN 
								--ID INICIAL C�DIGO DE BARRAS ERR�NEO
								SELECT @rutearTrx = 0, @codRespuesta = 'JTS000010', @codRespuestaISO = 'N6'
							END								
						END 
						ELSE
						BEGIN
							--LONGITUD C�DIGO DE BARRAS ERR�NEA
							SELECT @rutearTrx = 0, @codRespuesta = 'JTS000009', @codRespuestaISO = 'N5'
						END

					END				
				END

				/*PAR�METROS DE SALIDA OK*/
				SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)

			END			
			ELSE IF (@tipoMensajeReq = '0100' AND @codigoProcesamiento = '370000')
			BEGIN
				/*CONSULTA DE TRANSACCI�N RECARGA*/
				
				DECLARE 
					@tra_codigo_respuestaConsulta AS VARCHAR(5),
					@tra_rrnConsulta AS VARCHAR(20),
					@tra_autorizacion_ope_idConsulta AS VARCHAR(20),
					@tra_auth_id_p38Consulta AS VARCHAR(10),
					@tra_idConsulta AS BIGINT
				
				SELECT @rutearTrx = 0, @isoMuxName = 'MIDDLEWARE'				
				SET @interfazDestino = @isoMuxName
				
				SELECT
					@tra_idConsulta = tra_id,
					@tra_codigo_respuestaConsulta = tra_codigo_respuesta,
					@tra_rrnConsulta = ISNULL(tra_rrn, ''),
					@tra_autorizacion_ope_idConsulta = ISNULL(tra_autorizacion_ope_id, ''),
					@tra_auth_id_p38Consulta = ISNULL(tra_auth_id_p38, '')
				FROM Transaccion WITH(NOLOCK)
				WHERE 
					tra_comercio_id = @comercioID AND
					tra_terminal_id = @ptoVentaID AND
					tra_valor = @valor AND
					tra_stan = SUBSTRING(@datosAdicionales, 5, 10) AND
					CONVERT(VARCHAR, tra_fecha_inicial, 112) = CAST(YEAR(GETDATE()) AS VARCHAR(4)) + SUBSTRING(@datosAdicionales, 1, 4)
								
				IF NOT @tra_idConsulta IS NULL
				BEGIN
					--Consulta OK, Retornar Par�metros
					SELECT @codRespuestaISO = @tra_codigo_respuestaConsulta, 
							@msjRespuesta = RIGHT(REPLICATE('0',12) + CAST( @tra_rrnConsulta AS VARCHAR(12)),12) +
											RIGHT(REPLICATE('0',6) + CAST( @tra_auth_id_p38Consulta AS VARCHAR(6)),6) +
											RIGHT(REPLICATE('0',20) + CAST( @tra_autorizacion_ope_idConsulta AS VARCHAR(20)),20), 
							@codRespuesta = 'JTS600000'
				END
				ELSE
				BEGIN
					--Consulta Err�nea
					SELECT @codRespuestaISO = 'N8', @msjRespuesta = '', @codRespuesta = 'JTS600001'
				END
												
				/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
				SELECT 
					@productoID_BD = P.pro_id 
				FROM Producto P 
				WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))
				
			END	
			ELSE IF (@tipoMensajeReq = '0400' AND @codigoProcesamiento = '150000')
			BEGIN
				/*REVERSO RECAUDO DE FACTURAS*/
				
				DECLARE 
					@tra_codigo_respuestaReverso AS VARCHAR(5),
					@tra_auth_id_p38Reverso AS VARCHAR(10)
				
				SELECT @rutearTrx = 0, @isoMuxName = 'MIDDLEWARE'				
				SET @interfazDestino = @isoMuxName
				
				SELECT
					@tra_codigo_respuestaReverso = tra_codigo_respuesta,
					@tra_auth_id_p38Reverso = tra_auth_id_p38
				FROM Transaccion WITH(NOLOCK)
				WHERE 
					tra_comercio_id = @comercioID AND				--EL MISMO COMERCIO
					tra_terminal_id = @ptoVentaID AND				--EL MISMO PUNTO DE VENTA
					tra_valor = @valor AND							--EL MISMO VALOR
					tra_datos_adicionales = @datosAdicionales AND	--EL MISMO C�DIGO DE BARRAS
					tra_stan = @stan AND							--EL MISMO STAN
					tra_tipo_mensaje_req = '0200' AND
					tra_codigo_proceso = '150000'
								
				IF NOT @tra_codigo_respuestaReverso IS NULL
				BEGIN
				
					IF @tra_codigo_respuestaReverso = '00' AND NOT @tra_auth_id_p38Reverso IS NULL AND @tra_auth_id_p38Reverso <> ''
					BEGIN
						--VENTA ORIGINAL AUTORIZADA, MARCAR PARA ENV�O REVERSO H2H
						SELECT @codRespuestaISO = 'B3',		--C�DIGO ESPECIAL PARA ENVIAR REVERSO H2H
								@codRespuesta = 'JTS400000'
					END
					ELSE
					BEGIN
						--TRANSACCI�N NO AUTORIZADA, MARCAR REVERSO PROCESADO HACIA EL POS
						SELECT @codRespuestaISO = '00',	@codRespuesta = 'JTS400000'
					END

				END
				ELSE
				BEGIN
					--TRANSACCI�N NO ENCONTRADA, MARCAR REVERSO PROCESADO HACIA EL POS
					SELECT @codRespuestaISO = '00',	@codRespuesta = 'JTS400000'					
				END
												
				/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
				SELECT 
					@productoID_BD = P.pro_id 
				FROM Producto P 
				WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))
				
				/*OBTENER REF. DE PAGO DEL C�DIGO DE BARRAS*/
				IF EXISTS(SELECT * FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD)
				BEGIN
					--SEPARAR CAMPOS DEL C�DIGO DE BARRAS
					SELECT @refCliente1=referencia1, @refCliente2=referencia2 FROM dbo.[SepararCamposCodigoBarras](@datosAdicionales, @productoID_BD)
				END
				
				/*PAR�METROS DE SALIDA OK*/
				SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)				
				
			END				
			ELSE IF (@tipoMensajeReq = '0100' AND @codigoProcesamiento = '360000')
			BEGIN
				/*CONSULTA DE PAR�METROS DE LECTURA X CONVENIO*/
					
				SELECT @rutearTrx = 0, @isoMuxName = 'MIDDLEWARE'				
				SET @interfazDestino = @isoMuxName					
					
				/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
				SELECT 
					@productoID_BD = P.pro_id 
				FROM Producto P 
				WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))					
					
				/*VALIDAR EL C�DIGO DE PRODUCTO*/
				IF @productoID_BD IS NULL	--EXISTE PRODUCTO?
					SELECT @rutearTrx = 0, @codRespuesta = 'JTS000007', @codRespuestaISO = '12'
				ELSE
					SELECT @rutearTrx = 0, @codRespuestaISO = '00'					
					
				IF @codRespuestaISO = '00'
				BEGIN	
					
					--SEPARAR C�DIGO DE EAN DEL BARCODE ENVIADO
					IF LEN(@datosAdicionales) > 16
					BEGIN 
						
						IF SUBSTRING(@datosAdicionales, 1, 3) = '415'	--ID INICIAL C�DIGO DE BARRAS COLOMBIA
						BEGIN
						
							IF EXISTS(SELECT * FROM ConvenioXProducto WHERE conv_codigo_EAN=SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD)
							BEGIN
								--DEVOLVER PAR�METROS DE LECTURA DEL C�DIGO DE BARRAS PARA EL CONVENIO, EN LA VARIABLE MENSAJE DE RESPUESTA
								SELECT 
									@msjRespuesta = 
									CAST(CxP.conv_cod_parcial AS VARCHAR)  +
									CAST(CxP.conv_ref_doble AS VARCHAR) + 
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_pos_ini_ref1 AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_longitud_ref1 AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_pos_ini_valor AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_longitud_valor AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_pos_ini_ref2 AS VARCHAR),3) +
									RIGHT(REPLICATE('0',3) + CAST(CxP.conv_longitud_ref2 AS VARCHAR),3)
								FROM ConvenioXProducto CxP
								WHERE conv_codigo_EAN = SUBSTRING(@datosAdicionales,4,13) AND conv_producto_id=@productoID_BD								
								
								SET @codRespuesta = 'JTS500000'
								
							END
							ELSE
							BEGIN
								--CONVENIO NO ACTIVO
								SELECT @rutearTrx = 0, @codRespuesta = 'JTS000011', @codRespuestaISO = 'N7'									
							END
						END
						ELSE
						BEGIN 
							--ID INICIAL C�DIGO DE BARRAS ERR�NEO
							SELECT @rutearTrx = 0, @codRespuesta = 'JTS000010', @codRespuestaISO = 'N6'
						END								
					END 
					ELSE
					BEGIN
						--LONGITUD C�DIGO DE BARRAS ERR�NEA
						SELECT @rutearTrx = 0, @codRespuesta = 'JTS000009', @codRespuestaISO = 'N5'
					END
				END
				
			END					
			ELSE
			BEGIN
				/*TIPO DE TRANSACCI�N INEXISTENTE*/
				SELECT @rutearTrx = 0, @codRespuesta = 'JTS000012', @codRespuestaISO = '12'
				SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)
			END
			
		END
		ELSE
		BEGIN
			/*	REGLAS DE NEGOCIO NO CUMPLIDAS, @validacion_local = 0	*/
			SELECT @rutearTrx = 0, @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta), @isoMuxName = ''
			
			/*OBTENER COD. DE PRODUCTO PARA ALMACENAMIENTO*/
			SELECT 
				@productoID_BD = P.pro_id 
			FROM Producto P 
			WHERE P.pro_cod_EAN = LTRIM(RTRIM(@productoEAN))
			
		END

		/*REGISTRAR TRANSACCI�N*/
		INSERT INTO Transaccion(tra_fecha_inicial, tra_tipo_mensaje_req, tra_interfaz_origen, tra_interfaz_destino, tra_codigo_respuesta, tra_codigo_proceso, tra_stan, tra_rrn, tra_terminal_id, tra_comercio_id,
								tra_comercio_saldo_inicial, tra_referencia_cliente_1, tra_referencia_cliente_2, tra_datos_adicionales, tra_valor, tra_producto_id, tra_ip_cliente, tra_puerto_atencion)	
		VALUES (GETDATE(), @tipoMensajeReq, @interfazOrigen, @interfazDestino, @codRespuestaISO, @codigoProcesamiento, @stan, @rrn, @ptoVentaID, LTRIM(RTRIM(@comercioID)),
				@tra_comercio_saldo, @refCliente1, @refCliente2, @datosAdicionales, @valor, @productoID_BD, @IP, @puertoTCP )

		SELECT @trxID = SCOPE_IDENTITY()

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1, @codRespuesta = 'JTS950000', @rutearTrx = 0, @isoMuxName = ''
		SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[sp_mtcPostProcesar](
	/*PAR�METROS DE ENTRADA*/
	@trxID AS BIGINT, 
	@tipoMensajeResp AS VARCHAR(10),	
	@authID AS VARCHAR(6),
	@operadorAuthID AS VARCHAR(20),
	@codRespuestaFormateador AS VARCHAR(10),
	@codRespuestaISO AS VARCHAR(2),
	/*PAR�METROS DE SALIDA*/
	@codRespuesta AS VARCHAR(10) OUTPUT, 
	@msjRespuesta AS VARCHAR(500) OUTPUT, 
	@saldoComercio AS MONEY OUTPUT,
	@codRespuestaISOReal AS VARCHAR(2) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES*/
	DECLARE @retorno_sp AS SMALLINT
	DECLARE @comercio_id AS VARCHAR(20)
	DECLARE @valor_venta AS MONEY
	DECLARE @tipoMensaje AS VARCHAR(10)
	DECLARE @codigoProcesamiento AS VARCHAR(10)
	
	BEGIN TRANSACTION;

	BEGIN TRY
		/*L�GICA DEL NEGOCIO*/

		--CONSULTAR DATOS DE LA TRANSACCI�N EN CURSO
		SELECT 
			@comercio_id			= TRA_COMERCIO_ID,
			@valor_venta			= TRA_VALOR,
			@tipoMensaje			= TRA_TIPO_MENSAJE_REQ,
			@codigoProcesamiento	= TRA_CODIGO_PROCESO
		FROM TRANSACCION WITH(NOLOCK)	--NO BLOQUEANTE
		WHERE TRA_ID = @trxID
		

		/*VENTA DE RECARGAS A CELULAR*/
		IF @tipoMensaje = '0200' AND @codigoProcesamiento = '380000'
		BEGIN

			---MANEJO LOCAL DE SALDO
			--VENTA EXITOSA
			IF @authID <> '0' AND @codRespuestaISO = '00'
			BEGIN

				--DISMINUIR SALDO DEL COMERCIO
				EXEC @retorno_sp=DBO.[sp_mtcDisminuirSaldo] @comercio_id, @valor_venta, @saldoComercio OUTPUT

			END
			ELSE
			BEGIN
				--VENTA NO EXITOSA

				--CONSULTAR SALDO ACTUAL
				SET @saldoComercio = DBO.[ObtenerSaldo](@comercio_id, 0 /*NO TRANSACCIONAL*/)
			END
			
			--SOLO GATEWAY SIN MANEJO DE SALDO
			SET @saldoComercio = DBO.[ObtenerSaldo](@comercio_id, 0 /*NO TRANSACCIONAL*/)			
			
		END
		ELSE
		BEGIN
			--CONSULTAR SALDO ACTUAL
			SET @saldoComercio = 0
		END
		
		/*OBTENER RSP. CODE REAL, MAPEADO*/
		SELECT @codRespuestaISOReal = dbo.[ObtenerMensajeISOMapeado](@codRespuestaFormateador, @codRespuestaISO)

		/*ACTUALIZAR REGISTRO DE TRANSACCION*/
		UPDATE Transaccion
		SET		tra_fecha_fin				= GETDATE(),
				tra_tipo_mensaje_resp		= @tipoMensajeResp, 
				tra_codigo_respuesta		= (CASE WHEN @tipoMensajeResp IS NULL THEN @codRespuestaISOReal WHEN @tipoMensajeResp <> '0410' THEN @codRespuestaISOReal ELSE tra_codigo_respuesta END),	/*Respuesta Enviada al POS*/
				tra_codigo_respuestaB24		= @codRespuestaISO,		/*Respuesta Obtenida del Servidor Externo*/
				tra_auth_id_p38				= (CASE WHEN @authID='0' THEN NULL ELSE @authID END),
				tra_autorizacion_ope_id		= (CASE WHEN @operadorAuthID='0' THEN NULL ELSE @operadorAuthID END),
				tra_codigo_estado_final		= @codRespuestaFormateador,
				tra_comercio_saldo_final	= @saldoComercio,
				tra_rrn						= RIGHT(REPLICATE('0',12) + CAST(tra_id AS VARCHAR(12)),12)
		WHERE tra_id = @trxID

		/*PAR�METROS DE SALIDA OK*/
		SELECT @codRespuesta = 'JTS000000', @msjRespuesta = DBO.[ObtenerMensaje](@codRespuestaFormateador)
		IF @msjRespuesta IS NULL 
			SET @msjRespuesta = 'Mensaje ''' + @codRespuesta + ''' No Encontrado'

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
	
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @codRespuesta = 'JTS940000', @saldoComercio = 0
		SELECT @msjRespuesta = DBO.[ObtenerMensaje](@codRespuesta)

		PRINT(ERROR_MESSAGE())

	END CATCH;

END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_mtcConsultarReversosPendientes]
AS
BEGIN

	DECLARE @REVTMP1 AS TABLE (tra_id BIGINT)
	DECLARE @REVTMP3 AS TABLE (tra_id BIGINT)
	DECLARE @REVTMP4 AS TABLE (tra_id BIGINT)
	DECLARE @REVTMP5 AS TABLE (tra_id BIGINT)
	DECLARE @REVTMP6 AS TABLE (tra_id BIGINT)

	SET NOCOUNT ON

	---TIPO 1, Transacciones '0200' con estado '68' y que s� aparecen en Tabla TransaccionXReverso con estado = '00'
	INSERT INTO @REVTMP1
		SELECT T.tra_id
		FROM Transaccion T
			INNER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_respuesta = '68' AND T.tra_codigo_proceso IN ('150000') AND TXR.tra_codigo_respuesta = '00'

	---TIPO 3, Transacciones '0200' con estado '68' y que s� aparecen en Tabla TransaccionXReverso PERO con estado <> '00'
	INSERT INTO @REVTMP3
		SELECT DISTINCT(R3.tra_id) FROM (
			SELECT T.* 
			FROM Transaccion T
				INNER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
			WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_respuesta = '68' AND T.tra_codigo_proceso IN ('150000') AND TXR.tra_codigo_respuesta <> '00'
				AND T.tra_id NOT IN (SELECT tra_id FROM @REVTMP1)
		) AS R3

	---TIPO 4, Transacciones '0400' con estado 'B3' y que s� aparecen en Tabla TransaccionXReverso con estado = '00'
	INSERT INTO @REVTMP4
		SELECT T.tra_id
		FROM Transaccion T
			INNER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T.tra_tipo_mensaje_req = '0400' AND T.tra_codigo_respuesta = 'B3' AND T.tra_codigo_proceso IN ('150000') AND TXR.tra_codigo_respuesta = '00'

	---TIPO 5, Transacciones '0400' con estado 'B3' y que no aparecen en Tabla TransaccionXReverso
	INSERT INTO @REVTMP5
		SELECT T.tra_id 
		FROM Transaccion T
			LEFT OUTER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T.tra_tipo_mensaje_req = '0400' AND T.tra_codigo_respuesta = 'B3' AND T.tra_codigo_proceso IN ('150000') AND TXR.tra_id IS NULL

	---TIPO 6, Transacciones '0400' con estado 'B3' y que s� aparecen en Tabla TransaccionXReverso PERO con estado <> '00'
	INSERT INTO @REVTMP6
		SELECT DISTINCT(R6.tra_id) FROM (
			SELECT T.* 
			FROM Transaccion T
				INNER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
			WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T.tra_tipo_mensaje_req = '0400' AND T.tra_codigo_respuesta = 'B3' AND T.tra_codigo_proceso IN ('150000') AND TXR.tra_codigo_respuesta <> '00'
				AND T.tra_id NOT IN (SELECT tra_id FROM @REVTMP4)
		) AS R6

	--SELECT COMPLETO FINAL: REVERSOS PENDIENTES

	SET NOCOUNT OFF

	SELECT TOP 50 * FROM (

		---TIPO 2, Transacciones '0200' con estado '68' y que no aparecen en Tabla TransaccionXReverso
		SELECT T.tra_id, T.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T.tra_tipo_mensaje_req, T.tra_tipo_mensaje_resp, T.tra_codigo_proceso, T.tra_codigo_respuesta,
				T.tra_stan, T.tra_rrn, T.tra_codigo_estado_final, T.tra_terminal_id, T.tra_comercio_id, CAST(T.tra_valor AS NUMERIC(18,0)) AS tra_valor, T.tra_referencia_cliente_1, T.tra_referencia_cliente_2, T.tra_datos_adicionales,
				T.tra_producto_id 
		FROM Transaccion T
			LEFT OUTER JOIN TransaccionXReverso TXR ON (TXR.tra_id_original = T.tra_id)
		WHERE T.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T.tra_tipo_mensaje_req = '0200' AND T.tra_codigo_respuesta = '68' AND T.tra_codigo_proceso IN ('150000') AND TXR.tra_id IS NULL	

		UNION ALL

		SELECT T1.tra_id, T1.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T1.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T1.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T1.tra_tipo_mensaje_req, T1.tra_tipo_mensaje_resp, T1.tra_codigo_proceso, T1.tra_codigo_respuesta,
				T1.tra_stan, T1.tra_rrn, T1.tra_codigo_estado_final, T1.tra_terminal_id, T1.tra_comercio_id, CAST(T1.tra_valor AS NUMERIC(18,0)) AS tra_valor, T1.tra_referencia_cliente_1, T1.tra_referencia_cliente_2, T1.tra_datos_adicionales,
				T1.tra_producto_id
		FROM Transaccion T1
			INNER JOIN @REVTMP3 T2 ON (T1.tra_id = T2.tra_id)

		UNION ALL	

		---Select Adicional para consultar los datos reales '0200' de transacciones '0400' del POS, que no estan en tabla TransaccionXReverso
		SELECT T2.tra_id, T1.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T1.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T1.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T1.tra_tipo_mensaje_req, T1.tra_tipo_mensaje_resp, T1.tra_codigo_proceso, T1.tra_codigo_respuesta,
				T1.tra_stan, T1.tra_rrn, T1.tra_codigo_estado_final, T1.tra_terminal_id, T1.tra_comercio_id, CAST(T1.tra_valor AS NUMERIC(18,0)) AS tra_valor, T1.tra_referencia_cliente_1, T1.tra_referencia_cliente_2, T1.tra_datos_adicionales,
				T1.tra_producto_id
		FROM Transaccion T1 WITH(NOLOCK)
			INNER JOIN Transaccion T2 WITH(NOLOCK) ON (T1.tra_stan = T2.tra_stan AND T1.tra_datos_adicionales = T2.tra_datos_adicionales AND T1.tra_tipo_mensaje_req = '0200')
		WHERE T1.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T2.tra_tipo_mensaje_req = '0400' AND T2.tra_codigo_respuesta = 'B3' AND T1.tra_codigo_proceso IN ('150000') AND
			T2.tra_id IN (SELECT tra_id FROM @REVTMP5) --LOS 0400 DEL POS NO PROCESADOS

		UNION ALL

		---Select Adicional para consultar los datos reales '0200' de transacciones '0400' del POS PERO con estado <> '00' en tabla TransaccionXReverso
		SELECT T2.tra_id, T1.tra_fecha_inicial, SUBSTRING(CONVERT(VARCHAR, T1.tra_fecha_inicial, 112),5,4) AS tra_fecha_original_rev, REPLACE(CONVERT(VARCHAR, T1.tra_fecha_inicial, 108), ':','') AS tra_hora_original_rev, T1.tra_tipo_mensaje_req, T1.tra_tipo_mensaje_resp, T1.tra_codigo_proceso, T1.tra_codigo_respuesta,
				T1.tra_stan, T1.tra_rrn, T1.tra_codigo_estado_final, T1.tra_terminal_id, T1.tra_comercio_id, CAST(T1.tra_valor AS NUMERIC(18,0)) AS tra_valor, T1.tra_referencia_cliente_1, T1.tra_referencia_cliente_2, T1.tra_datos_adicionales,
				T1.tra_producto_id
		FROM Transaccion T1 WITH(NOLOCK)
			INNER JOIN Transaccion T2 WITH(NOLOCK) ON (T1.tra_stan = T2.tra_stan AND T1.tra_datos_adicionales = T2.tra_datos_adicionales AND T1.tra_tipo_mensaje_req = '0200')
		WHERE T1.tra_fecha_inicial > CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),103) AND T2.tra_tipo_mensaje_req = '0400' AND T2.tra_codigo_respuesta = 'B3' AND T1.tra_codigo_proceso IN ('150000') AND
			T2.tra_id IN (SELECT tra_id FROM @REVTMP6) --LOS 0400 DEL POS NO APROBADOS
			
	) AS RES
	ORDER BY 1 ASC

END

GO

USE [Conred]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[sp_mtcProcesarInicializacion](
	/*PAR�METROS DE ENTRADA*/
	@tipoMensajeReq AS VARCHAR(10),
	@tipoMensajeResp AS VARCHAR(10),
	@codigoProcesamientoReq AS VARCHAR(10),
	@stan AS INT, 
	@ptoVentaID AS VARCHAR(20),
	@IP AS VARCHAR(20), 
	@puertoTCP AS INT,
	/*PAR�METROS DE SALIDA*/
	@trxID AS BIGINT OUTPUT,
	@codRespuestaISO VARCHAR(2) OUTPUT,
	@codigoProcesamientoResp AS VARCHAR(10) OUTPUT,
	@tablaIDResp AS VARCHAR(2) OUTPUT
)
AS
BEGIN

	/*DECLARACI�N DE VARIABLES LOCALES*/
	DECLARE @validacion_local AS SMALLINT
	DECLARE @codRespuesta AS VARCHAR(10)
	DECLARE @interfazOrigen AS VARCHAR(20)
	DECLARE @interfazDestino AS VARCHAR(20)
	DECLARE @comercioID AS VARCHAR(20)
	DECLARE @tablaIndexReg AS SMALLINT
	DECLARE @tablaTotalReg AS SMALLINT
	--VARIABLE TIPO TABLA PARA RETORNAR DATOS DE CONFIGURACI�N
	DECLARE @tablaConf AS TABLE(F1 VARCHAR(30), F2 VARCHAR(30), F3 VARCHAR(30), F4 VARCHAR(30), F5 VARCHAR(30), F6 VARCHAR(30),
								F7 VARCHAR(30), F8 VARCHAR(30), F9 VARCHAR(30), F10 VARCHAR(30), F11 VARCHAR(30), F12 VARCHAR(30))
	
	SET NOCOUNT ON	
	
	BEGIN TRANSACTION

	SELECT @interfazOrigen = 'CLIENTE', @interfazDestino = 'MIDDLEWARE'

	BEGIN TRY
		/*INICIO VALIDACIONES DEL NEGOCIO*/

		--VALIDAR SI EXISTE UN COMERCIO ASOCIADO A LA TERMINAL
		SELECT @comercioID = E.ent_comercio_JNTS
		FROM Entidad E
			INNER JOIN PuntoVentaXEntidad PVE ON (E.ent_id = PVE.ent_id)
			INNER JOIN PuntoVenta PV ON (PVE.pto_id = PV.pto_id)
		WHERE PV.pto_numero = @ptoVentaID

		IF NOT EXISTS(SELECT * FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID)		--EXISTE PUNTO DE VENTA?

			SELECT @codRespuesta = 'JTS000002', @validacion_local = 0, @codRespuestaISO = 'N1'

		ELSE IF NOT EXISTS(SELECT * FROM PuntoVentaXEntidad WHERE PTO_ID = (SELECT PTO_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) AND ENT_ID = (SELECT ent_id FROM ENTIDAD WHERE ent_comercio_JNTS = @comercioID) AND FECHA_FINAL IS NULL)	--PUNTO DE VENTA RELACIONADO AL COMERCIO

			SELECT @codRespuesta = 'JTS000003', @validacion_local = 0, @codRespuestaISO = 'N2'

		ELSE IF (SELECT ENT_ESTADO_ENTIDAD_ID FROM ENTIDAD WHERE ENT_COMERCIO_JNTS = LTRIM(RTRIM(@comercioID))) > 1	--COMERCIO ACTIVO?

			SELECT @codRespuesta = 'JTS000004', @validacion_local = 0, @codRespuestaISO = 'N3'

		ELSE IF (SELECT PTO_ESTADO_PUNTO_VENTA_ID FROM PUNTOVENTA WHERE PTO_NUMERO = @ptoVentaID) > 1	--PUNTO DE VENTA ACTIVO?

			SELECT @codRespuesta = 'JTS000005', @validacion_local = 0, @codRespuestaISO = 'N4'
		ELSE
			--TODO OK, CONTINUAR CON TRANSACCI�N
			SELECT @codRespuesta = 'JTS000000', @validacion_local = 1, @codRespuestaISO = '00'

		/*FIN VALIDACIONES DEL NEGOCIO*/

		IF @validacion_local = 1
		BEGIN
			/*	REGLAS DE NEGOCIO CUMPLIDAS, @validacion_local = 1	*/

			IF @codigoProcesamientoReq = '930000'
			BEGIN 			
				--PRIMERA TABLA DE INICIALIZACION = CONFIGURACI�N
				DECLARE @TICKET_DATA AS TABLE(T1 VARCHAR(2))
				DECLARE @COMERCIO_DATA AS TABLE(C1 VARCHAR(30), C2 VARCHAR(30), C3 VARCHAR(30))
				DECLARE @CONEXION_DATA AS TABLE(CO1 VARCHAR(30), CO2 VARCHAR(30), CO3 VARCHAR(30))
				DECLARE @PARAMS_DATA AS TABLE(PAR1 VARCHAR(30), PAR2 VARCHAR(30), PAR3 VARCHAR(30), PAR4 VARCHAR(30), PAR5 VARCHAR(30))
				
				--VARIABLES DE SALIDA
				SELECT @tablaIDResp = '01', @tablaIndexReg = 1, @tablaTotalReg = 1, @codigoProcesamientoResp = CAST( (CAST(@codigoProcesamientoReq AS INT) + 1) AS VARCHAR)

				--C�DIGO DE TICKET				
				INSERT @TICKET_DATA
					SELECT CASE WHEN ent_tipo_nivel_entidad_id <> 3 THEN '1' ELSE '2' END FROM Entidad WHERE ent_comercio_JNTS = @comercioID
	
				---DATOS DEL COMERCIO
				INSERT @COMERCIO_DATA
					SELECT 
						RIGHT(REPLICATE('0',10) + CAST(C.IDCOMERCIO AS VARCHAR(10)),10) AS IDCOMERCIO,
						LEFT(CAST(C.NOMBRE AS VARCHAR(23)) + REPLICATE(' ', 23),23) AS NOMBRE,
						LEFT(CAST(C.DIRECCION AS VARCHAR(23)) + REPLICATE(' ', 23),23) AS DIRECCION
					FROM assenda.dbo.COMERCIOS C
						INNER JOIN assenda.dbo.TERMINALES_ASSENDA TA ON (C.IDCOMERCIO = TA.COMERCIO_ID)
						INNER JOIN Conred.dbo.Entidad E ON (C.IDCOMERCIO COLLATE SQL_Latin1_General_CP1_CI_AS = E.ent_comercio_JNTS)
					WHERE TA.TERMINAL_ID = @ptoVentaID
					
				---DATOS DE CONECTIVIDAD
				INSERT @CONEXION_DATA
					SELECT
						A.TEL_PRIMARIO, 
						A.TEL_SECUNDARIO,
						A.IP_PRIMARIA
					FROM assenda.dbo.TERMINALES_ASSENDA TA
					INNER JOIN assenda.dbo.TIPOS_TERMINALES TT ON (TA.TIPO_TERMINAL_ID = TT.ID)
					INNER JOIN assenda.dbo.ADQUIRENTE A ON (TT.ADQUIRENTE_ID = A.ID_ADQUIRENTE)
					WHERE TA.TERMINAL_ID = @ptoVentaID
					
				---OTROS DATOS PARAMETRIZABLES POR CONRED		
				INSERT INTO @PARAMS_DATA
					SELECT
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'LocalPort') AS PUERTO,
						(SELECT CAST((CAST(CS.dato AS INT) / 1000 ) AS VARCHAR(10)) FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'TotalTimeoutJTranServer') AS TIMEOUT_RECARGAS,
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'maxAttempsConnection') AS INTENTOS_CONEXION,
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'minSaleAmount') AS VALOR_VENTA_MINIMO,
						(SELECT CS.dato FROM ConfiguracionServidor CS WHERE CS.codigo_instancia = 0 AND CS.nombre_parametro = 'maxSaleAmount') AS VALOR_VENTA_MAXIMO
				
				---RESULTADO FINAL PARA LA INICIALIZACI�N
				INSERT INTO @tablaConf
					SELECT 
					(SELECT T1 FROM @TICKET_DATA),
					(SELECT C1 FROM @COMERCIO_DATA),
					(SELECT C2 FROM @COMERCIO_DATA),
					(SELECT C3 FROM @COMERCIO_DATA),
					(SELECT CO1 FROM @CONEXION_DATA),
					(SELECT CO2 FROM @CONEXION_DATA),
					(SELECT CO3 FROM @CONEXION_DATA),
					(SELECT PAR1 FROM @PARAMS_DATA),
					(SELECT PAR2 FROM @PARAMS_DATA),
					(SELECT PAR3 FROM @PARAMS_DATA),
					(SELECT PAR4 FROM @PARAMS_DATA),
					(SELECT PAR5 FROM @PARAMS_DATA)
				
				SELECT * FROM @tablaConf
				
			END
			ELSE IF @codigoProcesamientoReq = '930001'
			BEGIN
				--ENVIAR DATOS DE LA SIGUIENTE TABLA DE INICIALIZACION DE ACUERDO A LA �LTIMA TABLE_ID ENVIADA
				DECLARE @LastTableID AS VARCHAR(2)
				DECLARE @LastRecIndex AS SMALLINT
				DECLARE @LastTablaTotalRec AS SMALLINT
				DECLARE @NextRecIndex AS SMALLINT
				DECLARE @MaxNextIndexXTable AS SMALLINT
				DECLARE @NextTableID AS VARCHAR(2)

				SELECT TOP 1
					@LastTableID		= TI.tra_tabla_inicializacion_id,
					@LastRecIndex		= TI.tra_index_registro_enviado,
					@LastTablaTotalRec	= TI.tra_max_registros_tabla					
				FROM TransaccionInicializacion TI
				WHERE TI.tra_terminal_id = @ptoVentaID AND TI.tra_cod_respuesta = '00' AND TI.tra_tabla_inicializacion_id NOT IN ('ZZ', 'XX')
				ORDER BY TI.tra_fecha DESC

				--VERIFICAR SI HACEN FALTA REGISTROS A ENVIAR POR LA �LTIMA TABLA
				IF @LastRecIndex >= @LastTablaTotalRec
				BEGIN
					--�LTIMA TABLA FINALIZADA, CONTINUAR CON LA PR�XIMA
					--CALCULAR SIGUIENTE TABLA A ENVIAR CON BASE EN LA ANTERIOR
					SET @NextTableID = RIGHT(REPLICATE('0',2) + CAST((CAST(@LastTableID AS SMALLINT) + 1) AS VARCHAR(2)),2)
					
					--INICIAR DESDE EL PRIMER REGISTRO
					SET @NextRecIndex = 1
				END
				ELSE
				BEGIN
					--CONTINUAR ENVIANDO REGISTROS DE LA �LTIMA TABLA
					SET @NextTableID = @LastTableID
					
					--CONTINUAR CON EL �LTIMO REGISTRO ENVIADO M�S 1
					SET @NextRecIndex = @LastRecIndex + 1				
				END

				--VALIDAR SI EXISTE LA TABLA A ENVIAR
				IF EXISTS(SELECT * FROM TablaInicializacion WHERE tab_id = @NextTableID)
				BEGIN
				
					--OBTENER EL M�XIMO DE REPETICIONES DE LA TABLA A ENVIAR
					SELECT @MaxNextIndexXTable = tab_max_repeticion FROM TablaInicializacion WHERE tab_id = @NextTableID				
				
					IF @NextTableID = '02'
					BEGIN
						--PRODUCTOS DE RECARGA
						
						--ESTABLECER EL TOTAL DE REGISTROS DE LA TABLA
						SELECT @tablaTotalReg = COUNT(*)
						FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 1/*SOLO RECARGAS*/ 
																				
						--ESTABLECER EL �LTIMO ID DE REGISTRO ENVIADO
						SELECT @tablaIndexReg = MAX(Row) FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 1/*SOLO RECARGAS*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						IF @tablaIndexReg IS NULL
							SET @tablaIndexReg = -1
												
						--SELECT FINAL PRODUCTOS DE RECARGA
						SELECT * FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 1/*SOLO RECARGAS*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						--VALIDAR FIN DE ENV�O REGISTROS
						IF @tablaIndexReg > 0 AND @tablaIndexReg < @tablaTotalReg
							SET @codigoProcesamientoResp = '930001'	--FALTAN M�S REGISTROS
						ELSE
							SET @codigoProcesamientoResp = '930001'	--TERMIN� EL PROCESO, CONTINUAR TABLA 03

					END
					ELSE IF @NextTableID = '03'
					BEGIN
						--PRODUCTOS DE PSP
						
						--ESTABLECER EL TOTAL DE REGISTROS DE LA TABLA
						SELECT @tablaTotalReg = COUNT(*)
						FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 2/*SOLO PSP*/ 
																				
						--ESTABLECER EL �LTIMO ID DE REGISTRO ENVIADO
						SELECT @tablaIndexReg = MAX(Row) FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 2/*SOLO PSP*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						IF @tablaIndexReg IS NULL
							SET @tablaIndexReg = -1
												
						--SELECT FINAL PRODUCTOS DE PSP
						SELECT * FROM 
						(
							SELECT 
								ROW_NUMBER() OVER(ORDER BY P.pro_id) AS Row,
								CAST(P.pro_cod_EAN AS VARCHAR(16)) AS pro_cod_EAN,
								RIGHT(REPLICATE(' ',23) + CAST(P.pro_nombre AS VARCHAR(23)),23) AS pro_nombre
							FROM Entidad E 
								INNER JOIN TipoNivelEntidad TNE ON (E.ent_tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Matriz_NivelEntidadXProducto MNEP ON (MNEP.tipo_nivel_entidad_id = TNE.id)
								INNER JOIN Producto P ON (P.pro_id = MNEP.producto_id)
							WHERE E.ent_comercio_JNTS = @comercioID AND P.pro_tipo_producto_id = 2/*SOLO PSP*/ 
						) AS Result
							WHERE Row BETWEEN @NextRecIndex AND ( ( ((@NextRecIndex - 1)/@MaxNextIndexXTable) * @MaxNextIndexXTable ) + @MaxNextIndexXTable)
						
						--VALIDAR FIN DE ENV�O REGISTROS
						IF @tablaIndexReg > 0 AND @tablaIndexReg < @tablaTotalReg
							SET @codigoProcesamientoResp = '930001'	--FALTAN M�S REGISTROS
						ELSE
							SET @codigoProcesamientoResp = '930000'	--TERMIN� EL PROCESO

					END
					ELSE IF @NextTableID = '03'
					BEGIN
						--PRODUCTOS DE GIROS
						SELECT 3
					END	
					
					--VARIABLES DE SALIDA
					SELECT @tablaIDResp = @NextTableID
									
				END
				ELSE 
				BEGIN
				
					SELECT '', '', '', '', '', '', '', '', '', '', '', ''
				
					--SIGUIENTE TABLA NO EXISTE, FIN DEL PROCESO
					--VARIABLES DE SALIDA
					SELECT @tablaIDResp = 'ZZ', @tablaIndexReg = 0, @codigoProcesamientoResp = '930000'					
				END
			END
			ELSE
			BEGIN
				SELECT '', '', '', '', '', '', '', '', '', '', '', ''
				
				--ERROR DE PROCESSING CODE
				SELECT @tablaIDResp = 'XX', @tablaIndexReg = 0, @codigoProcesamientoResp = @codigoProcesamientoReq
			END
		END
		ELSE
		BEGIN
		
			SELECT '', '', '', '', '', '', '', '', '', '', '', ''
			
			--REGLAS DE NEGOCIO NO CUMPLIDAS
			SELECT @tablaIDResp = '01', @tablaIndexReg = 0, @tablaTotalReg = 1, @codigoProcesamientoResp = CAST( (CAST(@codigoProcesamientoReq AS INT) + 1) AS VARCHAR)		
		END

		/*REGISTRAR TRANSACCI�N*/
		INSERT INTO TransaccionInicializacion(tra_fecha, tra_interfaz_origen, tra_interfaz_destino, tra_tipo_mensaje_req, tra_tipo_mensaje_resp, tra_codigo_proceso_req, tra_codigo_proceso_resp, tra_stan, tra_terminal_id, tra_cod_respuesta, 
								tra_codigo_estado_final, tra_tabla_inicializacion_id, tra_index_registro_enviado, tra_max_registros_tabla, tra_ip_cliente, tra_puerto_atencion)	
		VALUES (GETDATE(), @interfazOrigen, @interfazDestino, @tipoMensajeReq, @tipoMensajeResp, @codigoProcesamientoReq, @codigoProcesamientoResp, @stan, @ptoVentaID, @codRespuestaISO, @codRespuesta,
				@tablaIDResp, @tablaIndexReg, @tablaTotalReg, @IP, @puertoTCP )

		SELECT @trxID = SCOPE_IDENTITY()

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH

		/*PAR�METROS DE SALIDA ERROR*/
		SELECT @trxID = -1, @codRespuesta = 'JTS930000'

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		PRINT(ERROR_MESSAGE())

	END CATCH;

END




