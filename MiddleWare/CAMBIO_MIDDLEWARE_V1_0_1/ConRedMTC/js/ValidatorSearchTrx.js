﻿function validateSearchTopup() {

    var flagField = false;

    if ($("#ctl00_MainContent_txtMobileNumber").val().length <= 0 &&
           $("#ctl00_MainContent_txtAuthIDP38").val().length <= 0 &&
           $("#ctl00_MainContent_txtAuthIDOpe").val().length <= 0 &&
           $("#ctl00_MainContent_txtSTAN").val().length <= 0) {
        flagField = false;
    }
    else if (!validateOnlyDigits($("#ctl00_MainContent_txtMobileNumber").val()) ||
                !validateOnlyDigits($("#ctl00_MainContent_txtAuthIDP38").val()) ||
                !validateOnlyDigits($("#ctl00_MainContent_txtAuthIDOpe").val()) ||
                !validateOnlyDigits($("#ctl00_MainContent_txtSTAN").val())) {
        flagField = false;
    }    
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateSearchBillPayment() {

    var flagField = false;

    if ($("#ctl00_MainContent_txtPaymentRef").val().length <= 0 &&
           $("#ctl00_MainContent_txtAuthIDP38").val().length <= 0 &&
           $("#ctl00_MainContent_txtBillAmount").val().length <= 0 &&
           $("#ctl00_MainContent_txtSTAN").val().length <= 0) {
        flagField = false;
    }
    else if (!validateOnlyDigits($("#ctl00_MainContent_txtPaymentRef").val()) ||
                !validateOnlyDigits($("#ctl00_MainContent_txtAuthIDP38").val()) ||
                !validateOnlyDigits($("#ctl00_MainContent_txtBillAmount").val()) ||
                !validateOnlyDigits($("#ctl00_MainContent_txtSTAN").val())) {
        flagField = false;
    }
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}