﻿function validateAddSupplier() {

    var flagField = false;

    if ($("#ctl00_MainContent_txtSupplierName").val().length <= 0) {
        $("#ctl00_MainContent_txtSupplierName").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_txtDesc").val().length <= 0) {
        $("#ctl00_MainContent_txtDesc").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_ddlSupplierDest").val() == ' ') {
        $("#ctl00_MainContent_ddlSupplierDest").focus();
        flagField = false;
    }
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateAddProduct() {

    var flagField = false;

    if ($("#ctl00_MainContent_txtProductName").val().length <= 5) {
        $("#ctl00_MainContent_txtProductName").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_txtEANCode").val().length <= 5) {
        $("#ctl00_MainContent_txtEANCode").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_ddlProductsType").val() < 0) {
        $("#ctl00_MainContent_ddlProductsType").focus();
        flagField = false;
    }
    else if (!validateDecimal($("#ctl00_MainContent_txtGlobalPctGain").val())) {
        $("#ctl00_MainContent_txtGlobalPctGain").focus();
        flagField = false;
    }
    else if (!validateOnlyDigits($("#ctl00_MainContent_txtEANCode").val())) {
        $("#ctl00_MainContent_txtEANCode").focus();
        flagField = false;
    }      
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}

function validateApplyMovement() {

    var flagField = false;

    if ($("#ctl00_MainContent_ddlBankAccount").val() <= 0) {
        $("#ctl00_MainContent_ddlBankAccount").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_txtMovementAmount").val().length <= 0
            || $("#ctl00_MainContent_txtFinalBalance").val().substring(0, 1) == '0'    
            || !validateOnlyDigits($("#ctl00_MainContent_txtMovementAmount").val())) {
        $("#ctl00_MainContent_txtMovementAmount").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_txtCurrentBalance").val().length <= 0
            || !validateOnlyDigits($("#ctl00_MainContent_txtCurrentBalance").val())) {
            $("#ctl00_MainContent_txtCurrentBalance").focus();
        flagField = false;
    }
    else if ($("#ctl00_MainContent_txtFinalBalance").val().length <= 0
            || !validateOnlyDigits($("#ctl00_MainContent_txtFinalBalance").val())) {
        $("#ctl00_MainContent_txtFinalBalance").focus();
        flagField = false;
    }    
    else
        flagField = true;

    if (!flagField) {
        $.msgAlert({
            type: 'error',
            title: 'Mensaje del Sistema',
            text: 'Por favor complete los datos del formulario correctamente',
            callback: function() {
                $.msgAlert.close();
            }
        });
    }

    return flagField;

}